package promauto.jroboplc.core;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import promauto.jroboplc.core.api.*;
import promauto.jroboplc.core.tags.TagBool;
import promauto.jroboplc.core.tags.TagDouble;
import promauto.jroboplc.core.tags.TagInt;
import promauto.jroboplc.core.tags.TagString;


public class TagTableImplTest {
	SampleTag tag1;
	SampleTag tag2;
	SampleTag tag3;
	SampleTag tag4;
	TagTable tt;
	Map<String,Tag> tagmap;
	
	@SuppressWarnings("unchecked")
	@Before
	public void setUp() throws Exception {
		Environment env = new SampleEnvironment();
		EnvironmentInst.set(env);

		tt = new TagTable();
		Field f = tt.getClass().getDeclaredField("tagmap");
		f.setAccessible(true);
		tagmap = (Map<String,Tag>) f.get(tt); 
		
		tag1 = new SampleTag("tag1");
		
		tag2 = new SampleTag("tag2");
		tag2.hidden = true;
		tag2.readonly = true;
		tag2.autosave = true;

		tag3 = new SampleTag("output");
		tag3.hidden = true;
		
		tag4 = new SampleTag("input");
		tag4.readonly = true;

	}

	private void addFourTags() {
		tt.add(tag1);
		tt.add(tag2);
		tt.add(tag3);
		tt.add(tag4);
	}

	
	

	
	@Test
	public void testRemove() {
		addFourTags();
		tt.remove(tag1);
		tt.remove(tag4);
		assertEquals("Must return two tags", 2, tagmap.size());
		assertNotNull("Must contains tag2", tagmap.get("tag2"));
		assertNotNull("Must contains tag3", tagmap.get("output"));
	}
	

	@Test
	public void testClear() {
		assertSame(tag1, tt.add(tag1));
		assertEquals("Gotta have a tag", 1, tagmap.size());
		tt.removeAll();
		assertEquals("Gotta have no tags", 0, tagmap.size());
	}

	
	@Test
	public void testGetTags() {
		tt.add(tag2);
		tt.add(tag1);
		List<Tag> list = tt.getTags("", true);
		assertEquals("Must return two tags", 2, list.size());
		assertSame("First must be tag1", list.get(0), tag1);
		assertSame("Second must be tag2", list.get(1), tag2);

		tt.add(tag3);
		list = tt.getTags("", true);
		assertSame("First must be tag3", list.get(0), tag3);
		assertSame("Second must be tag1", list.get(1), tag1);
		assertSame("Third must be tag2", list.get(2), tag2);
	}
	
	
	@Test
	public void testGetTagsWithFilter() {
		addFourTags();
		
		String[] emptyflags = {};
		List<Tag> list = tt.getTags(".*tag.*", true);
		assertEquals("Must return two tags", 2, list.size());
		assertSame("First must be tag1", list.get(0), tag1);
		assertSame("Second must be tag2", list.get(1), tag2);
	}


	@Test
	public void testCreateBool() {
		Tag tag = tt.createBool("tagbool", false);
		assertEquals("Tagmap has one tag", 1, tagmap.size());
		assertSame("The tag must be TagBool", TagBool.class, tag.getClass());
		assertEquals("Must have correct name", "tagbool", tag.getName());
		assertEquals("Must have initial value", false, tag.getBool());
	}

	
	@Test
	public void testCreateBoolWithFlags() {
		Tag tag = tt.createBool("tagbool", true, Flags.HIDDEN);
		assertTrue( tag.hasFlags( Flags.HIDDEN) );
		assertEquals("Must have initial value", true, tag.getBool());
	}
	

	@Test
	public void testCreateInt() {
		Tag tag = tt.createInt("tagint", 0);
		assertEquals("Tagmap has one tag", 1, tagmap.size());
		assertSame("The tag must be TagInt", TagInt.class, tag.getClass());
		assertEquals("Must have correct name", "tagint", tag.getName());
		assertEquals("Must have initial value", 0, tag.getInt());
	}
	

	@Test
	public void testCreateIntWithFlags() {
		Tag tag = tt.createInt("tagint", -5, Flags.EXTERNAL);
		assertTrue( tag.hasFlags( Flags.EXTERNAL) );
		assertEquals("Must have initial value", -5, tag.getInt());
	}

	
	@Test
	public void testCreateDouble() {
		Tag tag = tt.createDouble("tagdbl", 0);
		assertEquals("Tagmap has one tag", 1, tagmap.size());
		assertSame("The tag must be TagDouble", TagDouble.class, tag.getClass());
		assertEquals("Must have correct name", "tagdbl", tag.getName());
		assertEquals("Must have initial value", 0, tag.getDouble(), 0);
	}

	
	@Test
	public void testCreateDoubleWithFlags() {
		Tag tag = tt.createDouble("tagdbl", -5.5, Flags.EXTERNAL);
		assertTrue( tag.hasFlags( Flags.EXTERNAL) );
		assertFalse( tag.hasFlags( Flags.AUTOSAVE) );
		assertEquals("Must have initial value", -5.5, tag.getDouble(), 0);
	}
	

	@Test
	public void testCreateString() {
		Tag tag = tt.createString("tagstr", "");
		assertEquals("Tagmap has one tag", 1, tagmap.size());
		assertSame("The tag must be TagString", TagString.class, tag.getClass());
		assertEquals("Must have correct name", "tagstr", tag.getName());
		assertEquals("Must have initial value", "", tag.getString());
	}

	
	@Test
	public void testCreateStringWithFlags() {
		Tag tag = tt.createString("tagstr", "abc", Flags.AUTOSAVE);
		assertTrue( tag.hasFlags( Flags.AUTOSAVE) );
		assertEquals("Must have initial value", "abc", tag.getString());
	}

	
	@Test
	public void testCreateTwice() {
		assertEquals("Tagmap is empty", 0, tagmap.size());
		
		tt.createString("onetag", "");
		assertEquals("Tagmap has one tag", 1, tagmap.size());
		
		// overlap tag3 with new one
		tt.createString("onetag", "");
		assertEquals("It still has one tag", 1, tagmap.size());
		
	}


}
