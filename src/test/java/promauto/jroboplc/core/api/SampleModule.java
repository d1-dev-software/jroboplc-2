package promauto.jroboplc.core.api;

import java.util.Map;
import java.util.function.Consumer;

import promauto.jroboplc.core.State;
import promauto.jroboplc.core.api.Environment;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.Module;
import promauto.jroboplc.core.api.Plugin;
import promauto.jroboplc.core.TagTable;

public class SampleModule implements Module {
	public Environment env = null;
	public Plugin plg;
	public String name = "simmod";
	public String descr = "the simmod descr";
	public int cntPrepare = 0;
	public int cntExecute = 0;
	public int cntClosedown = 0;
	public TagTable tagtable = new TagTable();
	
	public SampleModule() {
		env = EnvironmentInst.get();
	}
	
	public SampleModule(String name) {
		this();
		this.name = name;
	}


	@Override
	public boolean prepare() {
		cntPrepare++;
		return true;
	}

	@Override
	public boolean execute() {
//		System.out.println("executing module " + name);
		cntExecute++;
		return true;
	}

	@Override
	public boolean closedown() {
		cntClosedown++;
		return true;
	}

	@Override
	public Plugin getPlugin() {
		return plg;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getInfo() {
		return descr;
	}

	@Override
	public TagTable getTagTable() {
		return tagtable;
	}

	@Override
	public void requestReload(Consumer<Boolean> onComplete) {
	}

	@Override
	public String getFlag(String flag) {
		return flag;
	}

	@Override
	public boolean isTaskable() {
		return true;
	}

	@Override
	public boolean canHaveExternalTags() {
		return false;
	}

//	@Override
//	public void release() {
//	}

	@Override
	public void loadState(State state) {

	}
	
	@Override
	public void saveState(State state) {

	}

	@Override
	public boolean isEnabled() {
		return false;
	}

	@Override
	public String check() {
		return "";
	}

	@Override
	public boolean isFlagCompatibleWith(String flag, Module module) {
		return false;
	}

	@Override
	public boolean isSuspended() {
		return false;
	}

	@Override
	public void setSuspended(boolean value) {
	}

	@Override
	public void postCommand(Command cmd, Console console, Module module, String args) {
	}

	@Override
	public void addSignalListener(Signal.Listener signalListener) {

	}

	@Override
	public void removeSignalListener(Signal.Listener signalListener) {

	}

	@Override
	public void postSignal(Signal.SignalType id) {

	}

	@Override
	public void postSignal(Signal.SignalType id, Object data) {

	}


}
