package promauto.jroboplc.core.api;

public class SampleTag implements Tag {
	public String name;
	public Status status;
	public boolean readonly = false;
	public boolean hidden = false;
	public boolean autosave = false;
	
	public boolean valbool = false;
	public int valint = 0;
	public long vallong = 0;
	public double valdbl = 0.0;
	public String valstr = "";
	public boolean valupd = false;
	public Object object = null;
	
	public SampleTag(String name){
		this.name = name; 
	}


	
	
	@Override
	public String getName() {
		return name;
	}


	@Override
	public boolean getBool() {
		return valbool;
	}

	@Override
	public int getInt() {
		return valint;
	}

	@Override
	public long getLong() {
		return vallong;
	}

	@Override
	public double getDouble() {
		return valdbl;
	}

	@Override
	public String getString() {
		return valstr;
	}

	@Override
	public void setBool(boolean value) {
		valbool = value;
	}

	@Override
	public void setInt(int value) {
		valint = value;
	}

	@Override
	public void setLong(long value) {
		vallong = value;
	}

	@Override
	public void setDouble(double value) {
		valdbl = value;
	}

	@Override
	public void setString(String value) {
		valstr = value;
	}


	@Override
	public Type getType() {
		return Type.INT;
	}







	@Override
	public boolean equalsValue(Tag tag) {
		// TODO Auto-generated method stub
		return false;
	}



	public void setReadValue(int value) {
		
	}
	
	public boolean hasWriteValue() {
		return false;
	}
	
	public int getWriteValueInt() {
		return 0;
	}




	@Override
	public Status getStatus() {
		return status;
	}




	@Override
	public void setStatus(Status status) {
		this.status = status;
	}

	@Override
	public int getFlags() {
		return 0;
	}

	@Override
	public void setFlags(int flags) {

	}

	@Override
	public void addFlag(int flag) {

	}

	@Override
	public boolean hasFlags(int flag) {
		return false;
	}


	@Override
	public void copyValueTo(Tag tag) {
	}





	@Override
	@SuppressWarnings("unchecked")
	public <T> T getObject() {
		return (T)object;
	}

	@Override
	public void setObject(Object object) {
		this.object = object;
	}




	@Override
	public void copyFlagsFrom(Tag tag) {
	}





}
