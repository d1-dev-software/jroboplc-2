package promauto.jroboplc.core.api;

import java.net.URLClassLoader;
import java.nio.file.Path;
import java.util.*;

import promauto.jroboplc.core.api.Module;
import promauto.jroboplc.core.api.ModuleManager;
import promauto.jroboplc.core.api.Plugin;

public class SampleModuleManager implements ModuleManager {
	public Map<String,Module> modules = new HashMap<>();

	@Override
	public List<Plugin> getPlugins() {
		return null;
	}

	@Override
	public boolean loadModules() {
		return false;
	}

	@Override
	public Set<Module> getModules() {
		return new HashSet<>( modules.values() );
	}

	@Override
	public List<Module> getModulesWithExternalTags() {
		return new ArrayList<>(modules.values());
	}

	@Override
	public Module getModule(String name) {
		return modules.get(name);
	}

	@Override
	public <T> T getModule(String name, Class<? extends T> cls) {
		Module module = modules.get(name);
		return cls.isInstance(module)? cls.cast(module): null;
	}

	@Override
	public Tag searchTag(String name) {
		return null;
	}

	@Override
	public Tag searchExternalTag(String name) {
		return null;
	}

	@Override
	public boolean appendModule(String plgname, String modname) {
		return false;
	}

//	@Override
//	public boolean removeModule(String modname) {
//		return false;
//	}

	@Override
	public boolean saveState(Set<Module> modules, Path path) {
		return false;
	}

	@Override
	public boolean loadState(Set<Module> modules, Path path) {
		return false;
	}

}
