package promauto.jroboplc.core.api;

import static javax.xml.stream.XMLInputFactory.IS_COALESCING;
import static javax.xml.stream.XMLInputFactory.IS_REPLACING_ENTITY_REFERENCES;
import static javax.xml.stream.XMLInputFactory.IS_SUPPORTING_EXTERNAL_ENTITIES;
import static org.junit.Assert.assertEquals;

import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collection;

import javax.xml.stream.XMLInputFactory;

import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

public class InitUtils {

	private static Logger logger = null;

	public static void setupLogger() {
		if (logger == null) {
			logger = Logger.getRootLogger();
			logger.setLevel(Level.OFF);
			logger.addAppender(new ConsoleAppender(new PatternLayout(
					"%-6r [%p] %c - %m%n")));
		}
	}

	public static String getResourcesDir(String subdir, Class<?> cls) {

		String dir = "src/test/resources/" + cls.getName() + '/';
		if( Files.exists( Paths.get(dir) ) )
			return dir;

		dir = "src/" + subdir + "/src/test/resources/" + cls.getName() + '/';
		if( Files.exists( Paths.get(dir) ) )
			return dir;

		System.out.println("Resource dir doesn't exist: ");
		System.out.println(System.getProperty("user.dir") + "  ----  " + dir);
		return "";
	}

	public static XMLInputFactory getXMLIF() {
		XMLInputFactory xmlif = XMLInputFactory.newInstance();
		xmlif.setProperty(IS_REPLACING_ENTITY_REFERENCES, true);
		xmlif.setProperty(IS_SUPPORTING_EXTERNAL_ENTITIES, true);
		xmlif.setProperty(IS_COALESCING, false);
		return xmlif;
	}


}

