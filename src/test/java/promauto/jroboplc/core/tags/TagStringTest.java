package promauto.jroboplc.core.tags;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class TagStringTest {
	TagString tag;
	TagString tag1;
	TagString tag2;

	@Before
	public void setUp() {
		tag = new TagString("strname", "10");
		tag1 = new TagString("strname1", "11");
		tag2 = new TagString("strname2", "12");
	}

	@Test
	public void testSetName() {
		assertEquals("strname", tag.getName());
	}



	@Test
	public void testSetInitValues() {
		String testname = "initial";
		assertEquals(testname + " getBool",  	false, 		tag.getBool());
		assertEquals(testname + " getInt",		10, 		tag.getInt());
		assertEquals(testname + " getDouble",	10.0, 		tag.getDouble(), 0);
		assertEquals(testname + " getString", 	"10", 		tag.getString());
	}

	@Test
	public void testSetBool() {
		String testname = "setBool false";
		
		tag.setBool(false);
		assertEquals(testname + " getBool",  	false, 		tag.getBool());
		assertEquals(testname + " getString", 	"off",	 	tag.getString());

		testname = "setBool true";
		tag.setBool(true);
		assertEquals(testname + " getBool",  	true, 		tag.getBool());
		assertEquals(testname + " getString", 	"on", 		tag.getString());
	}

	@Test
	public void testSetInt() {
		String testname = "setInt 0";
		tag.setInt(0);
		assertEquals(testname + " getBool",  	false, 	tag.getBool());
		assertEquals(testname + " getInt",  	0, 		tag.getInt());
		assertEquals(testname + " getDouble",  	0.0, 	tag.getDouble(), 0);
		assertEquals(testname + " getString", 	"0", 	tag.getString());

		testname = "setBool true";
		tag.setInt(-100);
		assertEquals(testname + " getBool",  	false, 	tag.getBool());
		assertEquals(testname + " getInt",  	-100, 	tag.getInt());
		assertEquals(testname + " getDouble",  	-100.0, tag.getDouble(), 0);
		assertEquals(testname + " getString", 	"-100", tag.getString());
	}

	@Test
	public void testSetDouble() {
		String testname = "setDouble 0.0";
		tag.setDouble(0.0);
		assertEquals(testname + " getBool",  	false, 	tag.getBool());
		assertEquals(testname + " getDouble",  	0.0, 	tag.getDouble(), 0);
		assertEquals(testname + " getString", 	"0.0",	tag.getString());

		testname = "setDouble 10.0";
		tag.setDouble(10.0);
		assertEquals(testname + " getBool",  	false, 	tag.getBool());
		assertEquals(testname + " getDouble",  	10.0, 	tag.getDouble(), 0);
		assertEquals(testname + " getString", 	"10.0", tag.getString());
	}


	@Test
	public void testSetString() {
		String testname = "setString -0";
		tag.setString("-0");
		assertEquals(testname + " getBool",  	false, 	tag.getBool());
		assertEquals(testname + " getInt",  	0, 		tag.getInt());
		assertEquals(testname + " getDouble",  	0.0, 	tag.getDouble(), 0);
		assertEquals(testname + " getString", 	"-0",	tag.getString());

		testname = "setString -0.123";
		tag.setString("-0.123");
		assertEquals(testname + " getBool",  	false, 	 	tag.getBool());
		assertEquals(testname + " getDouble",  	-0.123, 	tag.getDouble(), 0);
		assertEquals(testname + " getString", 	"-0.123",	tag.getString());

		testname = "setString abc";
		tag.setString("abc");
		assertEquals(testname + " getBool",  	false, 	 	tag.getBool());
		assertEquals(testname + " getString", 	"abc",		tag.getString());

		testname = "setString empty";
		tag.setString("");
		assertEquals(testname + " getBool",  	false, 	 	tag.getBool());
		assertEquals(testname + " getString", 	"",		tag.getString());
	}



	@Test
	public void testEqualsValue() {
		assertFalse(tag1.equalsValue(tag2));
		tag1.setString( tag2.getString() );
		assertTrue(tag1.equalsValue(tag2));
	}



	

}
