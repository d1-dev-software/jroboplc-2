package promauto.jroboplc.core.tags;

import org.junit.Before;
import org.junit.Test;
import promauto.jroboplc.core.api.Tag;

import java.lang.reflect.Field;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.*;

public class RefGroupTest {
    RefGroup rg;
    RefItem item1;
    RefItem item2;
    RefItem item3;

    Tag tag1;
    Tag tag2;
    Tag val1;
    Tag val2;

    Tag tag3;
    Tag val3;

    @SuppressWarnings("unchecked")
    @Before
    public void setUp() throws Exception {
        rg = new RefGroup();
        item1 = new RefItem(rg, "mod", "tag1");
        item2 = new RefItem(rg, "mod", "tag2");
        item3 = new RefItem(rg, "mod", "tag3");

        Field f = rg.getClass().getDeclaredField("items");
        f.setAccessible(true);
        List<RefItem> rg_items = (List<RefItem>) f.get(rg);
        rg_items.add(item1);
        rg_items.add(item2);

        f = rg.getClass().getDeclaredField("itemsCrc");
        f.setAccessible(true);
        List<RefItem> rg_itemsCrc = new LinkedList<>();
        f.set(rg, rg_itemsCrc);
        rg_itemsCrc.add(item1);
        rg_itemsCrc.add(item2);

        f = rg.getClass().getDeclaredField("itemCrcSum");
        f.setAccessible(true);
        f.set(rg, item3);





        tag1 = new TagInt("", 0);
        tag2 = new TagInt("", 0);
        tag3 = new TagInt("", 0);

        val1 = new TagInt("", 0);
        val2 = new TagInt("", 0);
        val3 = new TagInt("", 0);

        init_item(item1, tag1, val1);
        init_item(item2, tag2, val2);
        init_item(item3, tag3, val3);



    }

    private void init_item(RefItem item, Tag tag, Tag val) throws NoSuchFieldException, IllegalAccessException {
        Field f = item.getClass().getDeclaredField("tag");
        f.setAccessible(true);
        f.set(item, tag);

        f = item.getClass().getDeclaredField("value");
        f.setAccessible(true);
        f.set(item, val);
    }



    @Test
    public void calcCrc8() throws Exception {



        val1.setInt(0x1122);
        val2.setInt(0x3344);
        val3.setInt(51);

        assertTrue("check crc8", rg.checkCrc8() );
    }

}