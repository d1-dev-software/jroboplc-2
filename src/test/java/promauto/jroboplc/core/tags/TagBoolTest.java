package promauto.jroboplc.core.tags;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class TagBoolTest {
	TagBool tag1;
	TagBool tag2;

	@Before
	public void setUp() {
		tag1 = new TagBool("name1", false);
		tag2 = new TagBool("name2", true);
	}

	@Test
	public void testSetName() {
		assertEquals("name1", tag1.getName());
		assertEquals("name2", tag2.getName());
	}



	@Test
	public void testSetInitValues() {
		testTag("tag1 initial", tag1, false, 0, 0.0, "off");
		testTag("tag2 initial", tag2, true, 1, 1.0, "on");
	}

	@Test
	public void testSetBool() {
		tag1.setBool(true);
		tag2.setBool(false);
		testTag("tag1 setBool", tag1, true, 1, 1.0, "on");
		testTag("tag2 setBool", tag2, false, 0, 0.0, "off");
	}

	@Test
	public void testSetInt() {
		tag1.setInt(0);
		tag2.setInt(7);
		testTag("tag1 setInt", tag1, false, 0, 0.0, "off");
		testTag("tag2 setInt", tag2, true, 1, 1.0, "on");
	}

	@Test
	public void testSetDouble() {
		tag1.setDouble(-10.7);
		tag2.setDouble(0.0);
		testTag("tag1 setDouble", tag1, true, 1, 1.0, "on");
		testTag("tag2 setDouble", tag2, false, 0, 0.0, "off");
	}

	@Test
	public void testSetString() {
		tag1.setString("");
		tag2.setString("abc");
		testTag("tag1 setString", tag1, false, 0, 0.0, "off");
		testTag("tag2 setString", tag2, false, 0, 0.0, "off");

		tag1.setString("true");
		tag2.setString("on");
		testTag("tag1 setString", tag1, false, 0, 0.0, "off");
		testTag("tag2 setString", tag2, true, 1, 1.0, "on");
	}

	private void testTag(String testname, TagBool tag, boolean valBool,
			int valInt, double valDbl, String valStr) {

		assertEquals(testname + " getBool", 	valBool, tag.getBool());
		assertEquals(testname + " getInt", 		valInt, tag.getInt());
		assertEquals(testname + " getDouble", 	valDbl, tag.getDouble(), 0);
		assertEquals(testname + " getString", 	valStr, tag.getString());
	}

	
	

	@Test
	public void testEqualsValue() {
		assertFalse(tag1.equalsValue(tag2));
		tag1.setBool( tag2.getBool() );
		assertTrue(tag1.equalsValue(tag2));
	}

	
	

}
