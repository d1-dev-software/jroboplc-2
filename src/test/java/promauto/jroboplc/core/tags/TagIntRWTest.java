package promauto.jroboplc.core.tags;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class TagIntRWTest {
	TagRWInt tag1;
	TagRWInt tag2;

	@Before
	public void setUp() {
		tag1 = new TagRWInt("intname1", 0, 0);
		tag2 = new TagRWInt("intname2", 10, 0);
	}

	@Test
	public void testSetName() {
		assertEquals("intname1", tag1.getName());
		assertEquals("intname2", tag2.getName());
	}



	@Test
	public void testSetInitValues() {
		testTag("tag1 initial", tag1, false, 0,  0.0,  "0");
		testTag("tag2 initial", tag2, true, 10, 10.0, "10");
	}

	@Test
	public void testSetBool() {
		tag1.setBool(true);
		tag2.setBool(false);
		testTag("tag1 initial", tag1, false, 0,  0.0,  "0");
		testTag("tag2 initial", tag2, true, 10, 10.0, "10");

		assertTrue( tag1.hasWriteValue() );
		assertTrue( tag2.hasWriteValue() );
		
		assertEquals( 1, tag1.getWriteValInt() );
		assertEquals( 0, tag2.getWriteValInt() );

		tag1.setReadValInt(1);
		tag2.setReadValInt(0);

		testTag("tag1 setBool", tag1, true,  1, 1.0, "1");
		testTag("tag2 setBool", tag2, false, 0, 0.0, "0");
	}

	@Test
	public void testSetInt() {
		tag1.setInt(0);
		tag2.setInt(7);
		
		testTag("tag1 initial", tag1, false, 0,  0.0,  "0");
		testTag("tag2 initial", tag2, true, 10, 10.0, "10");

		assertTrue( tag1.hasWriteValue() );
		assertTrue( tag2.hasWriteValue() );
		
		assertEquals( 0, tag1.getWriteValInt() );
		assertEquals( 7, tag2.getWriteValInt() );

		tag1.setReadValInt(0);
		tag2.setReadValInt(7);

		testTag("tag1 setBool", tag1, false,  0, 0.0, "0");
		testTag("tag2 setBool", tag2, true,   7, 7.0, "7");
		
	}

	@Test
	public void testSetDouble() {
		tag1.setDouble(-10.7);
		tag2.setDouble(0.0);
		
		testTag("tag1 initial", tag1, false, 0,  0.0,  "0");
		testTag("tag2 initial", tag2, true, 10, 10.0, "10");

		assertTrue( tag1.hasWriteValue() );
		assertTrue( tag2.hasWriteValue() );
		
		assertEquals( -10, tag1.getWriteValInt() );
		assertEquals( 0, tag2.getWriteValInt() );

		tag1.setReadValInt(-10);
		tag2.setReadValInt(0);
		
		testTag("tag1 setDouble", tag1, true, -10, -10.0, "-10");
		testTag("tag2 setDouble", tag2, false,  0,   0.0,   "0");
	}

	@Test
	public void testSetString() {
		tag1.setString("0");
		tag2.setString("  -255  ");
		
		testTag("tag1 initial", tag1, false, 0,  0.0,  "0");
		testTag("tag2 initial", tag2, true, 10, 10.0, "10");

		assertTrue( tag1.hasWriteValue() );
		assertTrue( tag2.hasWriteValue() );
		
		assertEquals( 0, tag1.getWriteValInt() );
		assertEquals( -255, tag2.getWriteValInt() );

		tag1.setReadValInt(0);
		tag2.setReadValInt(-255);

		testTag("tag1 setString", tag1, false,    0,    0.0,    "0");
		testTag("tag2 setString", tag2, true,  -255, -255.0, "-255");
	}


	private void testTag(String testname, TagRWInt tag, boolean valBool,
			int valInt, double valDbl, String valStr) {

		assertEquals(testname + " getBool", 	valBool, tag.getBool());
		assertEquals(testname + " getInt", 		valInt, tag.getInt());
		assertEquals(testname + " getDouble", 	valDbl, tag.getDouble(), 0);
		assertEquals(testname + " getString", 	valStr, tag.getString());
	}


	@Test
	public void testEqualsValue() {
		assertFalse(tag1.equalsValue(tag2));
		tag1.setReadValInt( tag2.getInt() );
		assertTrue(tag1.equalsValue(tag2));
	}

}
