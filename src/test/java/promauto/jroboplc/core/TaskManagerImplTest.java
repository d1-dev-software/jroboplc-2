package promauto.jroboplc.core;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import promauto.jroboplc.core.api.InitUtils;
import promauto.jroboplc.core.api.SampleEnvironment;
import promauto.jroboplc.core.api.SampleModule;
import promauto.jroboplc.core.api.SampleModuleManager;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.Task;

public class TaskManagerImplTest {
	private TaskManagerImpl tm;
	private SampleEnvironment env;
	private SampleModuleManager mm;
	private SampleModule mod1;
	private SampleTaskModule mod2;
	
	class SampleTaskModule extends SampleModule implements Task {

		@Override
		public boolean isRunning() {
			return false;
		}

		@Override
		public boolean hasModule(String module) {
			return false;
		}

		@Override
		public Set<String> getModules() {
			return null;
		}

        @Override
        public int getPeriod() {
            return 0;
        }
    }

	private static class SampleModulePrepErr extends SampleModule {
		@Override
		public boolean prepare() {
			super.prepare();
			return false;
		}
	}

	@Before
	public void setUp() throws Exception {
		InitUtils.setupLogger();

		env = new SampleEnvironment();
		EnvironmentInst.set(env);
		
		mm = new SampleModuleManager();
		env.mm = mm;
		
		tm = new TaskManagerImpl(env);
		env.tm = tm;
		
		mod1 = new SampleModule("exbot1");
		mod2 = new SampleTaskModule();
		mm.modules.put(mod1.getName(), mod1);
		mm.modules.put(mod2.getName(), mod2);
	}

	@Test
	public void testStart() {
		assertEquals(0, mod1.cntPrepare);
		assertEquals(0, mod1.cntExecute);
		assertEquals(0, mod1.cntClosedown);

		assertEquals(0, mod2.cntPrepare);
		assertEquals(0, mod2.cntExecute);
		assertEquals(0, mod2.cntClosedown);
		
		assertFalse(env.running);
		
		tm.start();
		
		assertTrue(env.running);
		assertEquals(1, mod1.cntPrepare);
		assertEquals(0, mod1.cntExecute);
		assertEquals(0, mod1.cntClosedown);
		assertEquals(1, mod2.cntPrepare);
		assertEquals(1, mod2.cntExecute);
		assertEquals(0, mod2.cntClosedown);
	}

	
	
	@Test
	public void testDoubleStart() {
		tm.start();
		tm.start();
		assertTrue(env.running);
		assertEquals(1, mod1.cntPrepare);
		assertEquals(0, mod1.cntExecute);
		assertEquals(0, mod1.cntClosedown);
		assertEquals(1, mod2.cntPrepare);
		assertEquals(1, mod2.cntExecute);
		assertEquals(0, mod2.cntClosedown);
	}

	
	@Test
	public void testStartWithErrorInPrepare() {
		SampleModule mod3 = new SampleModulePrepErr();
		mm.modules.put("errorbot", mod3);

		tm.start();
		assertFalse(env.running);
		assertEquals(0, mod1.cntExecute);
		assertEquals(1, mod1.cntClosedown);
		assertEquals(0, mod2.cntExecute);
		assertEquals(1, mod2.cntClosedown);
		assertEquals(1, mod3.cntPrepare);
		assertEquals(0, mod3.cntExecute);
		assertEquals(1, mod3.cntClosedown);
	}


	@Test
	public void testStop() {
		tm.start();
		tm.stop();
		assertFalse(env.running);
		assertEquals(1, mod1.cntPrepare);
		assertEquals(0, mod1.cntExecute);
		assertEquals(1, mod1.cntClosedown);
		assertEquals(1, mod2.cntPrepare);
		assertEquals(1, mod2.cntExecute);
		assertEquals(1, mod2.cntClosedown);
	}

	
	@Test
	public void testDoubleStop() {
		tm.start();
		tm.stop();
		tm.stop();
		assertFalse(env.running);
		assertEquals(1, mod1.cntPrepare);
		assertEquals(0, mod1.cntExecute);
		assertEquals(2, mod1.cntClosedown);
		assertEquals(1, mod2.cntPrepare);
		assertEquals(1, mod2.cntExecute);
		assertEquals(2, mod2.cntClosedown);
	}

}




