package promauto.jroboplc.plugin.roboplant;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.nio.file.Paths;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import promauto.jroboplc.core.api.InitUtils;
import promauto.jroboplc.core.tags.TagInt;

public class DeviceZDVATest {

	private DeviceZDVA d;

	@Mock RoboplantModule module;


	@Before
	public void setUp() throws Exception {
		InitUtils.setupLogger();
		
		d = new DeviceZDVA();
		d.module = module;

		d.inpEnable = new Input();
		d.inpDoOpen = new Input();

		d.chnl1in 	= new Channel();
		d.chnl2in 	= new Channel();
		d.chnl1out 	= new Channel();
		d.chnl2out 	= new Channel();
		
		d.chnl1in .state = Channel.State.Ok;
		d.chnl2in .state = Channel.State.Ok;
		d.chnl1out.state = Channel.State.Ok;
		d.chnl2out.state = Channel.State.Ok;

		d.tagOutput   			= new TagInt("", 0);
		d.tagSost     			= new TagInt("", 0);
		d.tagOpened   			= new TagInt("", 0);
		d.tagClosed   			= new TagInt("", 0);
		d.tagControl  			= new TagInt("", 0);
		d.tagBlok     			= new TagInt("", 0);
		d.tagFlags    			= new TagInt("", 0);
		d.tagTimeOut  			= new TagInt("", 0);
		d.tagDlyAlarm 			= new TagInt("", 0);
		d.chnl1out.tagAddrNum  	= new TagInt("", 0);
		d.chnl1out.tagValue	  	= new TagInt("", -1);
		d.chnl2out.tagAddrNum	= new TagInt("", 0);
		d.chnl2out.tagValue	  	= new TagInt("", -1);
		d.chnl1in.tagAddrNum	= new TagInt("", 0);
		d.chnl1in.tagValue	  	= new TagInt("", -1);
		d.chnl2in.tagAddrNum	= new TagInt("", 0);
		d.chnl2in.tagValue	  	= new TagInt("", -1);
		d.tagAlarm    			= new TagInt("", 0);
	    
		d.inpEnable.tag   		= new TagInt("", 0);
		d.inpDoOpen.tag   		= new TagInt("", 0);

		d.resetState();
	}
	
	private void exec(
			int output	,       
			int sost	,         
			int opened	,          
			int closed	,        
			int chnl1out,
			int chnl2out,     
			int chnl1in	,
			int chnl2in	,     
			int alarm        
			) {
		
		d.execute();
		assertEquals("output"	, output	, d.tagOutput.getInt());     
		assertEquals("sost"		, sost		, d.tagSost.getInt());     
		assertEquals("opened"	, opened	, d.tagOpened.getInt());     
		assertEquals("closed"	, closed	, d.tagClosed.getInt());     
		assertEquals("chnl1out"	, chnl1out	, d.chnl1out.tagValue.getInt());     
		assertEquals("chnl2out"	, chnl2out	, d.chnl2out.tagValue.getInt());     
		assertEquals("chnl1in"	, chnl1in	, d.chnl1in.tagValue.getInt());     
		assertEquals("chnl2in"	, chnl2in	, d.chnl2in.tagValue.getInt());     
		assertEquals("alarm"   	, alarm   	, d.tagAlarm.getInt());
		
	}

	private void setOpenedClosed(int op, int cl) {
		d.chnl1in.tagValue.setInt(op);
		d.chnl2in.tagValue.setInt(cl);
	}

	private void close() {
		d.inpDoOpen.tag.setInt(0);
	}

	private void open() {
		d.inpDoOpen.tag.setInt(1);
	}

	
	
	@Test
	public void testLinkChannel_False() {
		d.chnl1in.state = Channel.State.Error;
		assertFalse(d.execute());
		assertEquals(8, d.tagSost.getInt());
	}


	@Test
	public void testFlags_0() {
		d.chnl1in.tagValue.setInt(0);
		d.chnl2in.tagValue.setInt(0);
		exec( 1,3, 1,1,  0,0, 0,0,  0 );
	}
	
	
	
	// Flags_0x80
	@Test
	public void testFlags_0x80_enable_0() {
		d.tagFlags.setInt(0x80);
		d.inpEnable.tag.setInt(0);
		close();
		
		setOpenedClosed(0,0);
		exec( 0,0, 0,0,  0,0, 0,0,  0 );

		setOpenedClosed( 1,0 );
		exec( 0,2, 1,0,  0,0, 1,0,  0 );

		setOpenedClosed( 0,1);
		exec( 1,1, 0,1,  0,0, 0,1,  0 );

		setOpenedClosed( 1,1);
		exec( 1,3, 1,1,  0,0, 1,1,  0 );
	}

	@Test
	public void testFlags_0x80_enable_1_close() {
		d.tagFlags.setInt(0x80);
		d.inpEnable.tag.setInt(1);
		close();
		
		setOpenedClosed( 0,0 );
		exec( 0,0, 0,0,  0,1, 0,0,  1 );

		setOpenedClosed( 1,0 );
		exec( 0,2, 1,0,  0,1, 1,0,  1 );

		
		setOpenedClosed( 0,1 );
		exec( 1,1, 0,1,  0,0, 0,1,  0 );

		setOpenedClosed( 1,1 );
		exec( 1,3, 1,1,  0,0, 1,1,  0 );
	}

	@Test
	public void testFlags_0x80_enable_1_open() {
		d.tagFlags.setInt(0x80);
		d.inpEnable.tag.setInt(1);
		open();
		
		setOpenedClosed( 0,0 );
		exec( 0,4, 0,0,  1,0, 0,0,  1 );
		

		setOpenedClosed( 0,1 );
		exec( 0,5, 0,1,  1,0, 0,1,  1 );

		
		setOpenedClosed( 1,0 );
		exec( 1,6, 1,0,  0,0, 1,0,  0 );

		setOpenedClosed( 1,1 );
		exec( 1,7, 1,1,  0,0, 1,1,  0 );
	}

	@Test
	public void testFlags_0x80_close_open() {
		d.tagFlags.setInt(0x80);
		d.inpEnable.tag.setInt(1);

		close();							// close - middle
		setOpenedClosed( 0,0 );			
		exec( 0,0, 0,0,  0,1, 0,0,  1 );

		setOpenedClosed( 0,1 );				// close - closed
		exec( 1,1, 0,1,  0,0, 0,1,  0 );

		open();								// open - closed
		setOpenedClosed( 0,1 );
		exec( 0,5, 0,1,  1,0, 0,1,  1 );

		setOpenedClosed( 0,0 );				// open - middle
		exec( 0,4, 0,0,  1,0, 0,0,  1 );
		
		setOpenedClosed( 1,0 );				// open - opened
		exec( 1,6, 1,0,  0,0, 1,0,  0 );

		close();							// close - opened
		setOpenedClosed( 1,0 );			
		exec( 0,2, 1,0,  0,1, 1,0,  1 );
	}
	
	@Test
	public void testFlags_0x80_close_lost() {
		d.tagFlags.setInt(0x80);
		d.inpEnable.tag.setInt(1);

		close();							// close - closed
		setOpenedClosed( 0,1 );			
		exec( 1,1, 0,1,  0,0, 0,1,  0 );
		
		setOpenedClosed( 0,0 );				// close - middle
		exec( 0,0, 0,0,  0,0, 0,0,  1 );
		exec( 0,0, 0,0,  0,0, 0,0,  1 );
	}

	@Test
	public void testFlags_0x80_open_lost() {
		d.tagFlags.setInt(0x80);
		d.inpEnable.tag.setInt(1);

		open();								// open - opened
		setOpenedClosed( 1,0 );			
		exec( 1,6, 1,0,  0,0, 1,0,  0 );
		
		// ??? why we need this ???
		exec( 1,6, 1,0,  0,0, 1,0,  0 ); 	
		
		setOpenedClosed( 0,0 );				// open - middle
		exec( 0,4, 0,0,  0,0, 0,0,  1 );
		exec( 0,4, 0,0,  0,0, 0,0,  1 );
	}



	
	// Flags_0x81
	@Test
	public void testFlags_0x81_enable_0() {
		d.tagFlags.setInt(0x81);
		d.inpEnable.tag.setInt(0);
		close();
		
		setOpenedClosed(0,0);
		exec( 0,0, 0,0,  0,0, 0,0,  0 );

		setOpenedClosed( 1,0 );
		exec( 0,2, 1,0,  0,0, 1,0,  0 );

		setOpenedClosed( 0,1);
		exec( 1,1, 0,1,  0,0, 0,1,  0 );

		setOpenedClosed( 1,1);
		exec( 1,3, 1,1,  0,0, 1,1,  0 );
	}

	@Test
	public void testFlags_0x81_enable_1_close() {
		d.tagFlags.setInt(0x81);
		d.inpEnable.tag.setInt(1);
		close();
		
		setOpenedClosed( 0,0 );
		exec( 0,0, 0,0,  0,1, 0,0,  1 );

		setOpenedClosed( 1,0 );
		exec( 0,2, 1,0,  0,1, 1,0,  1 );

		
		setOpenedClosed( 0,1 );
		exec( 1,1, 0,1,  0,0, 0,1,  0 );

		setOpenedClosed( 1,1 );
		exec( 1,3, 1,1,  0,0, 1,1,  0 );
	}

	@Test
	public void testFlags_0x81_enable_1_open() {
		d.tagFlags.setInt(0x81);
		d.inpEnable.tag.setInt(1);
		open();
		
		setOpenedClosed( 0,0 );
		exec( 0,4, 0,0,  1,0, 0,0,  1 );
		

		setOpenedClosed( 0,1 );
		exec( 0,5, 0,1,  1,0, 0,1,  1 );

		
		setOpenedClosed( 1,0 );
		exec( 1,6, 1,0,  0,0, 1,0,  0 );

		setOpenedClosed( 1,1 );
		exec( 1,7, 1,1,  0,0, 1,1,  0 );
	}

	@Test
	public void testFlags_0x81_close_open() {
		d.tagFlags.setInt(0x81);
		d.inpEnable.tag.setInt(1);

		close();							// close - middle
		setOpenedClosed( 0,0 );			
		exec( 0,0, 0,0,  0,1, 0,0,  1 );

		setOpenedClosed( 0,1 );				// close - closed
		exec( 1,1, 0,1,  0,0, 0,1,  0 );

		open();								// open - closed
		setOpenedClosed( 0,1 );
		exec( 0,5, 0,1,  1,0, 0,1,  1 );

		setOpenedClosed( 0,0 );				// open - middle
		exec( 0,4, 0,0,  1,0, 0,0,  1 );
		
		setOpenedClosed( 1,0 );				// open - opened
		exec( 1,6, 1,0,  0,0, 1,0,  0 );

		close();							// close - opened
		setOpenedClosed( 1,0 );			
		exec( 0,2, 1,0,  0,1, 1,0,  1 );
	}
	
	@Test
	public void testFlags_0x81_close_lost() {
		d.tagFlags.setInt(0x81);
		d.inpEnable.tag.setInt(1);

		close();							
		setOpenedClosed( 0,1 );				// close - closed
		exec( 1,1, 0,1,  0,0, 0,1,  0 );
		
		setOpenedClosed( 0,0 );				// close - middle
		exec( 0,0, 0,0,  0,1, 0,0,  1 );
		exec( 0,0, 0,0,  0,1, 0,0,  1 );

		setOpenedClosed( 0,1 );				// close - closed
		exec( 1,1, 0,1,  0,0, 0,1,  0 );

		setOpenedClosed( 0,0 );				// close - middle
		exec( 0,0, 0,0,  0,1, 0,0,  1 );
	}

	@Test
	public void testFlags_0x81_open_lost() {
		d.tagFlags.setInt(0x81);
		d.inpEnable.tag.setInt(1);

		open();								
		setOpenedClosed( 1,0 );				// open - opened
		exec( 1,6, 1,0,  0,0, 1,0,  0 );
		
		setOpenedClosed( 0,0 );				// open - middle
		exec( 0,4, 0,0,  1,0, 0,0,  1 );
		exec( 0,4, 0,0,  1,0, 0,0,  1 );
		
		setOpenedClosed( 1,0 );				// open - opened
		exec( 1,6, 1,0,  0,0, 1,0,  0 );
		
		setOpenedClosed( 0,0 );				// open - middle
		exec( 0,4, 0,0,  1,0, 0,0,  1 );
		exec( 0,4, 0,0,  1,0, 0,0,  1 );
	}



	
	// Flags_0x81
	@Test
	public void testFlags_0x82_enable_0() {
		d.tagFlags.setInt(0x82);
		d.inpEnable.tag.setInt(0);
		close();
		
		setOpenedClosed(0,0);
		exec( 0,0, 0,0,  0,0, 0,0,  0 );

		setOpenedClosed( 1,0 );
		exec( 0,2, 1,0,  0,0, 1,0,  0 );

		setOpenedClosed( 0,1);
		exec( 1,1, 0,1,  0,0, 0,1,  0 );

		setOpenedClosed( 1,1);
		exec( 1,3, 1,1,  0,0, 1,1,  0 );
	}

	@Test
	public void testFlags_0x82_enable_1_close() {
		d.tagFlags.setInt(0x82);
		d.inpEnable.tag.setInt(1);
		close();
		
		setOpenedClosed( 0,0 );
		exec( 0,0, 0,0,  0,1, 0,0,  1 );

		setOpenedClosed( 1,0 );
		exec( 0,2, 1,0,  0,1, 1,0,  1 );

		
		setOpenedClosed( 0,1 );
		exec( 1,1, 0,1,  0,1, 0,1,  0 );

		setOpenedClosed( 1,1 );
		exec( 1,3, 1,1,  0,1, 1,1,  0 );
	}

	@Test
	public void testFlags_0x82_enable_1_open() {
		d.tagFlags.setInt(0x82);
		d.inpEnable.tag.setInt(1);
		open();
		
		setOpenedClosed( 0,0 );
		exec( 0,4, 0,0,  1,0, 0,0,  1 );
		

		setOpenedClosed( 0,1 );
		exec( 0,5, 0,1,  1,0, 0,1,  1 );

		
		setOpenedClosed( 1,0 );
		exec( 1,6, 1,0,  1,0, 1,0,  0 );

		setOpenedClosed( 1,1 );
		exec( 1,7, 1,1,  1,0, 1,1,  0 );
	}

	@Test
	public void testFlags_0x82_close_open() {
		d.tagFlags.setInt(0x82);
		d.inpEnable.tag.setInt(1);

		close();							// close - middle
		setOpenedClosed( 0,0 );			
		exec( 0,0, 0,0,  0,1, 0,0,  1 );

		setOpenedClosed( 0,1 );				// close - closed
		exec( 1,1, 0,1,  0,1, 0,1,  0 );

		open();								// open - closed
		setOpenedClosed( 0,1 );
		exec( 0,5, 0,1,  1,0, 0,1,  1 );

		setOpenedClosed( 0,0 );				// open - middle
		exec( 0,4, 0,0,  1,0, 0,0,  1 );
		
		setOpenedClosed( 1,0 );				// open - opened
		exec( 1,6, 1,0,  1,0, 1,0,  0 );

		close();							// close - opened
		setOpenedClosed( 1,0 );			
		exec( 0,2, 1,0,  0,1, 1,0,  1 );
	}
	
	@Test
	public void testFlags_0x82_close_lost() {
		d.tagFlags.setInt(0x82);
		d.inpEnable.tag.setInt(1);

		close();							
		setOpenedClosed( 0,1 );				// close - closed
		exec( 1,1, 0,1,  0,1, 0,1,  0 );
		
		setOpenedClosed( 0,0 );				// close - middle
		exec( 0,0, 0,0,  0,1, 0,0,  1 );
		exec( 0,0, 0,0,  0,1, 0,0,  1 );

		setOpenedClosed( 0,1 );				// close - closed
		exec( 1,1, 0,1,  0,1, 0,1,  0 );

		setOpenedClosed( 0,0 );				// close - middle
		exec( 0,0, 0,0,  0,1, 0,0,  1 );
	}

	@Test
	public void testFlags_0x82_open_lost() {
		d.tagFlags.setInt(0x82);
		d.inpEnable.tag.setInt(1);

		open();								
		setOpenedClosed( 1,0 );				// open - opened
		exec( 1,6, 1,0,  1,0, 1,0,  0 );
		
		setOpenedClosed( 0,0 );				// open - middle
		exec( 0,4, 0,0,  1,0, 0,0,  1 );
		exec( 0,4, 0,0,  1,0, 0,0,  1 );
		
		setOpenedClosed( 1,0 );				// open - opened
		exec( 1,6, 1,0,  1,0, 1,0,  0 );
		
		setOpenedClosed( 0,0 );				// open - middle
		exec( 0,4, 0,0,  1,0, 0,0,  1 );
		exec( 0,4, 0,0,  1,0, 0,0,  1 );
	}



	
	
	@Test
	public void testFlags_0x84() {
		d.tagFlags.setInt(0x84);
		d.inpEnable.tag.setInt(1);

		setOpenedClosed( 0,0 );
		
		close();						
		exec( 0,0, 0,0,  0,-1, 0,0,  1 );
		
		open();						
		exec( 0,4, 0,0,  1,-1, 0,0,  1 );
		
		close();						
		exec( 0,0, 0,0,  0,-1, 0,0,  1 );
		
		open();						
		exec( 0,4, 0,0,  1,-1, 0,0,  1 );
	}

	
	@Test
	public void testFlags_0x88() {
		d.tagFlags.setInt(0x88);
		d.inpEnable.tag.setInt(1);

		setOpenedClosed( 0,0 );
		
		close();						
		exec( 0,0, 0,0,  -1,-1, 0,0,  1 );
		
		open();						
		exec( 0,4, 0,0,  -1,-1, 0,0,  1 );
		
		close();						
		exec( 0,0, 0,0,  -1,-1, 0,0,  1 );
		
		open();						
		exec( 0,4, 0,0,  -1,-1, 0,0,  1 );
	}
	
	
	@Test
	public void testFlags_0x92() {
		d.tagFlags.setInt(0x92);
		d.inpEnable.tag.setInt(1);
		close();						

		setOpenedClosed( 0,0 );
		exec( 1,1, 0,1,  0,1, 0,0,  0 );

		setOpenedClosed( 0,1 );
		exec( 0,0, 0,0,  0,1, 0,1,  1 );

		setOpenedClosed( 1,0 );
		exec( 0,0, 0,0,  0,1, 1,0,  1 );

		setOpenedClosed( 1,1 );
		exec( 0,2, 1,0,  0,1, 1,1,  1 );
	}
	
	@Test
	public void testFlags_0xA2() {
		d.tagFlags.setInt(0xA2);
		d.inpEnable.tag.setInt(1);
		close();						

		setOpenedClosed( 0,0 );
		exec( 1,1, 0,1,  0,1, 0,0,  0 );

		setOpenedClosed( 0,1 );
		exec( 1,1, 0,1,  0,1, 0,1,  0 );

		setOpenedClosed( 1,0 );
		exec( 0,2, 1,0,  0,1, 1,0,  1 );

		setOpenedClosed( 1,1 );
		exec( 0,2, 1,0,  0,1, 1,1,  1 );
	}
	
	
	
	@Test
	public void testFlags_0x80_timeout_1() {
		d.tagFlags.setInt(0x80);
		d.tagTimeOut.setInt(1);
		d.inpEnable.tag.setInt(1);

		close();								
		setOpenedClosed( 0,0 );				// close - middle
		exec( 0,0, 0,0,  0,1, 0,0,  1 );
		exec( 0,0, 0,0,  0,0, 0,0,  1 );
		exec( 0,0, 0,0,  0,0, 0,0,  1 );

		open();								
		setOpenedClosed( 0,0 );				// open - middle
		exec( 0,4, 0,0,  1,0, 0,0,  1 );
		exec( 0,4, 0,0,  0,0, 0,0,  1 );
		exec( 0,4, 0,0,  0,0, 0,0,  1 );

		close();								
		setOpenedClosed( 0,0 );				// close - middle
		exec( 0,0, 0,0,  0,1, 0,0,  1 );
		setOpenedClosed( 0,1 );				// close - closed
		exec( 1,1, 0,1,  0,0, 0,1,  0 );
		
		open();								
		setOpenedClosed( 0,0 );				// open - middle
		exec( 0,4, 0,0,  1,0, 0,0,  1 );
		setOpenedClosed( 1,0 );				// open - opened
		exec( 1,6, 1,0,  0,0, 1,0,  0 );
	}	
	
	
	@Test
	public void testFlags_0x81_timeout_1() {
		d.tagFlags.setInt(0x81);
		d.tagTimeOut.setInt(1);
		d.inpEnable.tag.setInt(1);

		close();								
		setOpenedClosed( 0,0 );				// close - middle
		exec( 0,0, 0,0,  0,1, 0,0,  1 );
		exec( 0,0, 0,0,  0,0, 0,0,  1 );
		exec( 0,0, 0,0,  0,0, 0,0,  1 );

		open();								
		setOpenedClosed( 0,0 );				// open - middle
		exec( 0,4, 0,0,  1,0, 0,0,  1 );
		exec( 0,4, 0,0,  0,0, 0,0,  1 );
		exec( 0,4, 0,0,  0,0, 0,0,  1 );

		close();								
		setOpenedClosed( 0,0 );				// close - middle
		exec( 0,0, 0,0,  0,1, 0,0,  1 );
		setOpenedClosed( 0,1 );				// close - closed
		exec( 1,1, 0,1,  0,0, 0,1,  0 );
		
		open();								
		setOpenedClosed( 0,0 );				// open - middle
		exec( 0,4, 0,0,  1,0, 0,0,  1 );
		setOpenedClosed( 1,0 );				// open - opened
		exec( 1,6, 1,0,  0,0, 1,0,  0 );
	}	
	
	
	@Test
	public void testFlags_0x82_timeout_1() {
		d.tagFlags.setInt(0x82);
		d.tagTimeOut.setInt(1);
		d.inpEnable.tag.setInt(1);

		close();								
		setOpenedClosed( 0,0 );				// close - middle
		exec( 0,0, 0,0,  0,1, 0,0,  1 );
		exec( 0,0, 0,0,  0,1, 0,0,  1 );
		exec( 0,0, 0,0,  0,1, 0,0,  1 );

		open();								
		setOpenedClosed( 0,0 );				// open - middle
		exec( 0,4, 0,0,  1,0, 0,0,  1 );
		exec( 0,4, 0,0,  1,0, 0,0,  1 );
		exec( 0,4, 0,0,  1,0, 0,0,  1 );
	}
	
	@Test
	public void testFlags_0x80_control_b3b0() {
		d.tagFlags.setInt(0x80);
		d.inpEnable.tag.setInt(1);

		close();								
		d.tagControl.setInt(1);				
		setOpenedClosed( 0,0 );				// disable - middle
		exec( 0,0, 0,0,  0,0, 0,0,  1 );

		open();								
		d.tagControl.setInt(2);				
		setOpenedClosed( 0,0 );				// close - middle
		exec( 0,0, 0,0,  0,1, 0,0,  1 );
		setOpenedClosed( 0,1 );				// close - closed
		exec( 1,1, 0,1,  0,0, 0,1,  0 );

		d.tagControl.setInt(1);								
		setOpenedClosed( 0,0 );				// disable - middle
		exec( 0,0, 0,0,  0,0, 0,0,  1 );

		close();							
		d.tagControl.setInt(3);				
		setOpenedClosed( 0,0 );				// open - middle
		exec( 0,4, 0,0,  1,0, 0,0,  1 );
		setOpenedClosed( 1,0 );				// open - opened
		exec( 1,6, 1,0,  0,0, 1,0,  0 );

	}
	
	@Test
	public void testFlags_0x81_control_b3b0() {
		d.tagFlags.setInt(0x81);
		d.inpEnable.tag.setInt(1);

		close();								
		d.tagControl.setInt(1);				
		setOpenedClosed( 0,0 );				// disable - middle
		exec( 0,0, 0,0,  0,0, 0,0,  1 );

		open();								
		d.tagControl.setInt(2);				
		setOpenedClosed( 0,0 );				// close - middle
		exec( 0,0, 0,0,  0,1, 0,0,  1 );
		setOpenedClosed( 0,1 );				// close - closed
		exec( 1,1, 0,1,  0,0, 0,1,  0 );

		d.tagControl.setInt(1);								
		setOpenedClosed( 0,0 );				// disable - middle
		exec( 0,0, 0,0,  0,0, 0,0,  1 );

		close();							
		d.tagControl.setInt(3);				
		setOpenedClosed( 0,0 );				// open - middle
		exec( 0,4, 0,0,  1,0, 0,0,  1 );
		setOpenedClosed( 1,0 );				// open - opened
		exec( 1,6, 1,0,  0,0, 1,0,  0 );

	}
	
	
	@Test
	public void testFlags_0x80_control_b4b7() {
		d.tagFlags.setInt(0x80);
		d.inpEnable.tag.setInt(1);

		close();								
		d.tagControl.setInt(0x10);				
		setOpenedClosed( 0,0 );				// disable - middle
		exec( 0,0, 0,0,  0,0, 0,0,  1 );

		open();								
		d.tagControl.setInt(0x20);				
		setOpenedClosed( 0,0 );				// close - middle
		exec( 0,0, 0,0,  0,1, 0,0,  1 );
		setOpenedClosed( 0,1 );				// close - closed
		exec( 1,1, 0,1,  0,0, 0,1,  0 );

		d.tagControl.setInt(0x10);								
		setOpenedClosed( 0,0 );				// disable - middle
		exec( 0,0, 0,0,  0,0, 0,0,  1 );

		close();							
		d.tagControl.setInt(0x30);				
		setOpenedClosed( 0,0 );				// open - middle
		exec( 0,4, 0,0,  1,0, 0,0,  1 );
		setOpenedClosed( 1,0 );				// open - opened
		exec( 1,6, 1,0,  0,0, 1,0,  0 );

	}
	
	@Test
	public void testFlags_0x81_control_b4b7() {
		d.tagFlags.setInt(0x81);
		d.inpEnable.tag.setInt(1);

		close();								
		d.tagControl.setInt(0x10);				
		setOpenedClosed( 0,0 );				// disable - middle
		exec( 0,0, 0,0,  0,0, 0,0,  1 );

		open();								
		d.tagControl.setInt(0x20);				
		setOpenedClosed( 0,0 );				// close - middle
		exec( 0,0, 0,0,  0,1, 0,0,  1 );
		setOpenedClosed( 0,1 );				// close - closed
		exec( 1,1, 0,1,  0,0, 0,1,  0 );

		d.tagControl.setInt(0x10);								
		setOpenedClosed( 0,0 );				// disable - middle
		exec( 0,0, 0,0,  0,0, 0,0,  1 );

		close();							
		d.tagControl.setInt(0x30);				
		setOpenedClosed( 0,0 );				// open - middle
		exec( 0,4, 0,0,  1,0, 0,0,  1 );
		setOpenedClosed( 1,0 );				// open - opened
		exec( 1,6, 1,0,  0,0, 1,0,  0 );

	}
	
	@Test
	public void testFlags_b6_1() {
		d.tagFlags.setInt(0x80);
		d.inpEnable.tag.setInt(1);
		setOpenedClosed( 0,0 );				

		d.tagControl.setInt(0x23);				
		exec( 0,4, 0,0,  1,0, 0,0,  1 );

		d.tagControl.setInt(0x32);				
		exec( 0,0, 0,0,  0,1, 0,0,  1 );

		
		d.tagFlags.setInt(0xC0);
		d.tagControl.setInt(0x23);				
		exec( 0,0, 0,0,  0,1, 0,0,  1 );

		d.tagControl.setInt(0x32);				
		exec( 0,4, 0,0,  1,0, 0,0,  1 );
	}
	

	

	@Test
	public void testLoad() throws Exception {
		
		RoboplantModule robo = AuxProcs.createRoboplantModule();
		assertTrue( robo.loadProject( Paths.get( 
				InitUtils.getResourcesDir("roboplant", RoboplantModuleTest.class) + "zdva.xml" )));

		assertEquals(1, robo.devicesByAddr.length);
		DeviceZDVA d = (DeviceZDVA)robo.devicesByAddr[0];
		
		assertEquals( "ZDVA", d.devtype );
		assertEquals( "824", d.tagname );
		
		assertEquals(2, d.inputs.size());
		assertEquals(16, d.outputs.size());
		
		assertTrue( d.init(robo) );
		assertTrue( d.prepare() );

	}
	
	
	
	
	@Test
	public void testFlags_0x81_close_open_blok() {
		d.tagFlags.setInt(0x81);
		d.inpEnable.tag.setInt(1);
		d.tagBlok.setInt(1);

		close();							// close - middle
		setOpenedClosed( 0,0 );			
		exec( 1,1, 0,1,  0,0, 0,0,  0 );

		setOpenedClosed( 0,1 );				// close - closed
		exec( 1,1, 0,1,  0,0, 0,1,  0 );

		open();								// open - closed
		setOpenedClosed( 0,1 );
		exec( 1,6, 1,0,  0,0, 0,1,  0 );

		setOpenedClosed( 0,0 );				// open - middle
		exec( 1,6, 1,0,  0,0, 0,0,  0 );
		
		setOpenedClosed( 1,0 );				// open - opened
		exec( 1,6, 1,0,  0,0, 1,0,  0 );

		close();							// close - opened
		setOpenedClosed( 1,0 );			
		exec( 1,1, 0,1,  0,0, 1,0,  0 );
	}
	
	
	@Test
	public void testFlags_0x82_close_open_blok() {
		d.tagFlags.setInt(0x82);
		d.inpEnable.tag.setInt(1);
		d.tagBlok.setInt(1);

		close();							// close - middle
		setOpenedClosed( 0,0 );			
		exec( 1,1, 0,1,  0,1, 0,0,  0 );

		setOpenedClosed( 0,1 );				// close - closed
		exec( 1,1, 0,1,  0,1, 0,1,  0 );

		open();								// open - closed
		setOpenedClosed( 0,1 );
		exec( 1,6, 1,0,  1,0, 0,1,  0 );

		setOpenedClosed( 0,0 );				// open - middle
		exec( 1,6, 1,0,  1,0, 0,0,  0 );
		
		setOpenedClosed( 1,0 );				// open - opened
		exec( 1,6, 1,0,  1,0, 1,0,  0 );

		close();							// close - opened
		setOpenedClosed( 1,0 );			
		exec( 1,1, 0,1,  0,1, 1,0,  0 );
	}
	
	
	
	
	
	
	
	
}