package promauto.jroboplc.plugin.roboplant;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static promauto.jroboplc.plugin.roboplant.AuxProcs.newInputAndTag;

import java.nio.file.Paths;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import org.mockito.MockitoAnnotations;
import promauto.jroboplc.core.api.InitUtils;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.TagInt;

public class DeviceZCTATest {
	private DeviceZCTA d;
	
	Tag tagSeqc;
	Tag tagAddress0;
	Tag tagAddress1;
	Tag tagAddress2;
	Tag tagAddress3;
	Tag tagAddress4;
	Tag tagMode0;
	Tag tagMode1;
	Tag tagMode2;
	Tag tagMode3;
	Tag tagMode4;

	@Mock RoboplantModule module;

	int zdvcount = 5;
	Device[] zdv = new Device[zdvcount];
	Tag[] zdvOutput = new Tag[zdvcount];
	Tag[] zdvControl = new Tag[zdvcount];


	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.openMocks(this);
		InitUtils.setupLogger();
		
		d = new DeviceZCTA();
		d.module = module;
	
		d.inpStart 		= newInputAndTag();
		d.inpStop 		= newInputAndTag();
		
		d.tagSost    		= new TagInt("", 0);
		d.tagAlarm          = new TagInt("", 0);
		d.tagStopFlow       = new TagInt("", 0);
		d.tagStopRout       = new TagInt("", 0);
		d.tagStepSize       = new TagInt("", 1);
		d.tagStepTime       = new TagInt("", 1);
		d.tagDlyAlarm       = new TagInt("", 0);
		d.tagDlyStopFlow    = new TagInt("", 0);
		d.tagDlyStopRout    = new TagInt("", 0);
		d.tagCode           = new TagInt("", 0);
		d.tagEnable         = new TagInt("", 1);


		d.initZdv();
		d.zdv[0].tagAddress = tagAddress0 = new TagInt("", 0);
		d.zdv[1].tagAddress = tagAddress1 = new TagInt("", 1);
		d.zdv[2].tagAddress = tagAddress2 = new TagInt("", 2);
		d.zdv[3].tagAddress = tagAddress3 = new TagInt("", 3);
		d.zdv[4].tagAddress = tagAddress4 = new TagInt("", 0xffff);
		d.zdv[0].tagMode = tagMode0 = new TagInt("", 0x11);
		d.zdv[1].tagMode = tagMode1 = new TagInt("", 0x12);
		d.zdv[2].tagMode = tagMode2 = new TagInt("", 0x11);
		d.zdv[3].tagMode = tagMode3 = new TagInt("", 0x12);
		d.zdv[4].tagMode = tagMode4 = new TagInt("", 0);

		d.resetState();
		
//		Output output; 
		for (int i=0; i<zdvcount; i++) {
			zdv[i] = mock(Device.class);
			when(module.getDevice(i)).thenReturn(zdv[i]);
			
			mock1(i, "Output", zdvOutput);
			mock1(i, "Control", zdvControl);
		}
		
		d.tagCode.setInt(5);
	}


	private void mock1(int i, String tname, Tag[] tags) {
		tags[i] = new TagInt("tag_"+i+"_"+tname, 0);
		Output output = new Output();
		output.tag = tags[i];
		when(zdv[i].getOutput(tname)).thenReturn(output);
	}

	
	

	
	@Test
	public void testLoad() throws Exception {
		
		RoboplantModule robo = AuxProcs.createRoboplantModule();
		assertTrue( robo.loadProject( Paths.get( 
				InitUtils.getResourcesDir("roboplant", RoboplantModuleTest.class) + "zcta.xml" )));

		assertEquals(1, robo.devicesByAddr.length);
		DeviceZCTA d = (DeviceZCTA)robo.devicesByAddr[0];
		
		assertEquals( "ZCTA", d.devtype );
		assertEquals( "SZ01", d.tagname );
		
		assertTrue( d.init(robo) );
		assertTrue( d.prepare() );

		assertEquals(100, d.zdv.length );
		assertEquals(0, d.zdv[0].state);
		assertEquals(-1, d.zdv[0].address);
		assertEquals(0, d.zdv[0].getOutputValue());
		assertEquals(0, d.zdv[0].getControlValue());

	}

	
	
	private void exec(
			int sost     ,
			int alarm    ,       
			int stopFlow , 
			int stopRout 
			) {
		
		d.execute();	
		assertEquals("Sost    ", 	sost    ,   d.tagSost    .getInt());       
		assertEquals("Alarm   ", 	alarm   ,   d.tagAlarm   .getInt());   
		assertEquals("StopFlow", 	stopFlow,   d.tagStopFlow.getInt());      
		assertEquals("StopRout", 	stopRout,   d.tagStopRout.getInt());        
	}

	private void ctrl(int...values) {
		for (int i=0; i<values.length; i++)
			assertEquals( "zdvControl["+i+"]", values[i], zdvControl[i].getInt());
	}
	
	private void output(int...values) {
		for (int i=0; i<values.length; i++)
			zdvOutput[i].setInt(values[i]);
	}
	

	@Test
	public void testInit() {
		assertEquals(100, d.zdv.length);
		assertEquals(0, d.zdv[0].state);
		assertEquals(-1, d.zdv[0].address);
		assertEquals(0, d.zdv[0].getControlValue());
		assertEquals(0, d.zdv[0].getOutputValue());
		
	}

	
	@Test
	public void testLinkMchbItems_bad() {
		when(module.getDevice(0)).thenReturn(null);
		when(module.getDevice(2)).thenReturn(null);
		when(zdv[3].getOutput("Control")).thenReturn(null);
		d.linkZdvItems();
		
		assertEquals(			 0, 	d.zdv[0].address);
		assertSame(            0, 	d.zdv[0].getOutputValue());
		assertSame(            0, 	d.zdv[0].getControlValue());
		
		assertEquals(			 1, 	d.zdv[1].address);
		assertSame(	   zdvOutput[1].getInt(), 	d.zdv[1].getOutputValue());
		assertSame(   zdvControl[1].getInt(), 	d.zdv[1].getControlValue());
		
		assertEquals(			 2, 	d.zdv[2].address);
		assertSame(            0, 	d.zdv[2].getOutputValue());
		assertSame(            0, 	d.zdv[2].getControlValue());
		
		assertEquals(			 3, 	d.zdv[3].address);
		assertSame(    zdvOutput[3].getInt(), 	d.zdv[3].getOutputValue());
		assertSame(  		   0, 	d.zdv[3].getControlValue());

		assertEquals(	     0xffff, 	d.zdv[4].tagAddress.getInt());
		assertEquals(	     -1, 	d.zdv[4].address);
		assertSame(            0, 	d.zdv[4].getOutputValue());
		assertSame(            0, 	d.zdv[4].getControlValue());
	}

	
	@Test
	public void testLinkMchbItems_good() {
		d.linkZdvItems();
		
		assertEquals(			 0, 	d.zdv[0].address);
		assertSame(   zdvOutput[0].getInt(), 	d.zdv[0].getOutputValue());
		assertSame(  zdvControl[0].getInt(), 	d.zdv[0].getControlValue());
		
		assertEquals(			 1, 	d.zdv[1].address);
		assertSame(   zdvOutput[1].getInt(), 	d.zdv[1].getOutputValue());
		assertSame(  zdvControl[1].getInt(), 	d.zdv[1].getControlValue());
		
		assertEquals(			 2, 	d.zdv[2].address);
		assertSame(   zdvOutput[2].getInt(), 	d.zdv[2].getOutputValue());
		assertSame(  zdvControl[2].getInt(), 	d.zdv[2].getControlValue());
		
		assertEquals(			 3, 	d.zdv[3].address);
		assertSame(   zdvOutput[3].getInt(), 	d.zdv[3].getOutputValue());
		assertSame(  zdvControl[3].getInt(), 	d.zdv[3].getControlValue());

		assertEquals(		 0xffff, 	d.zdv[4].tagAddress.getInt());
		assertEquals(		 -1, 	d.zdv[4].address);
		assertSame(            0, 	d.zdv[4].getOutputValue());
		assertSame(            0, 	d.zdv[4].getControlValue());
	}

	
	@Test
	public void testEnable_0() {
		d.tagEnable.setInt(0);
		output(0,0,0,0,0);   exec(0,0,0,0); ctrl( 0, 0, 0, 0, 0);

	}
	
	@Test
	public void testEnable_1() {
		output(0,0,0,0,0);   exec(0,0,1,1); ctrl( 0, 0, 0, 0, 0);
	}
	
	@Test
	public void testStart() {
		d.inpStart.tag.setInt(1);
		output(0,0,0,0);   exec(2,0,1,1);   ctrl(0x20,    0,    0,    0);
		d.inpStart.tag.setInt(0);
		output(0,0,0,0);   exec(2,0,1,1);   ctrl(0x20,    0,    0,    0);
		output(0,0,0,0);   exec(2,0,1,1);   ctrl(0x20, 0x30,    0,    0);
		output(0,0,0,0);   exec(2,0,1,1);   ctrl(0x20, 0x30,    0,    0);
		output(0,0,0,0);   exec(2,0,1,1);   ctrl(0x20, 0x30, 0x20,    0);
		output(0,0,0,0);   exec(2,0,1,1);   ctrl(0x20, 0x30, 0x20,    0);
		output(0,0,0,0);   exec(2,0,1,1);   ctrl(0x20, 0x30, 0x20, 0x30);
		output(0,0,0,0);   exec(2,0,1,1);   ctrl(0x20, 0x30, 0x20, 0x30);
		output(0,0,0,0);   exec(3,0,1,1);   ctrl(0x20, 0x30, 0x20, 0x30);
		                                                               
		output(0,0,0,0);   exec(3,0,1,1);   ctrl(0x20, 0x30, 0x20, 0x30);
		output(0,0,0,0);   exec(3,0,1,1);   ctrl(0x20, 0x30, 0x20, 0x30);
		output(0,0,0,0);   exec(3,0,1,1);   ctrl(0x20, 0x30, 0x20, 0x30);

		output(1,0,0,0);   exec(3,0,1,1);   ctrl(0x20, 0x30, 0x20, 0x30);
		output(1,1,0,0);   exec(3,0,1,1);   ctrl(0x20, 0x30, 0x20, 0x30);
		output(1,1,1,0);   exec(3,0,1,1);   ctrl(0x20, 0x30, 0x20, 0x30);
		output(1,1,1,1);   exec(1,0,1,1);   ctrl(0x20, 0x30, 0x20, 0x30);
		
		output(1,1,1,1);   exec(1,0,0,0);   ctrl(0x20, 0x30, 0x20, 0x30);
		output(1,1,1,1);   exec(1,0,0,0);   ctrl(0x20, 0x30, 0x20, 0x30);
		output(1,1,1,1);   exec(1,0,0,0);   ctrl(0x20, 0x30, 0x20, 0x30);

		d.inpStop.tag.setInt(1);
		output(1,1,1,1);   exec(0,0,1,1);   ctrl(0x00, 0x00, 0x00, 0x00);
	}
	
	@Test
	public void testStepTime_0() {
		d.tagStepSize.setInt(1);
		d.tagStepTime.setInt(0);
		d.inpStart.tag.setInt(1);
		output(1,1,1,1);   exec(2,0,1,1);   ctrl(0x20, 0x00, 0x00, 0x00);
		output(1,1,1,1);   exec(2,0,1,1);   ctrl(0x20, 0x30, 0x00, 0x00);
		output(1,1,1,1);   exec(2,0,1,1);   ctrl(0x20, 0x30, 0x20, 0x00);
		output(1,1,1,1);   exec(2,0,1,1);   ctrl(0x20, 0x30, 0x20, 0x30);

	}

	@Test
	public void testStepSize_0() {
		d.tagStepSize.setInt(0);
		d.tagStepTime.setInt(1);
		d.inpStart.tag.setInt(1);
		output(1,1,1,1);   exec(2,0,1,1);   ctrl(0x00, 0x00, 0x00, 0x00);
		output(1,1,1,1);   exec(2,0,1,1);   ctrl(0x00, 0x00, 0x00, 0x00);
		output(1,1,1,1);   exec(2,0,1,1);   ctrl(0x00, 0x00, 0x00, 0x00);
		output(1,1,1,1);   exec(2,0,1,1);   ctrl(0x00, 0x00, 0x00, 0x00);
	}

	
	
	public void quickstart() {
		d.tagStepSize.setInt(5);
		d.tagStepTime.setInt(0);
		d.inpStart.tag.setInt(1);
		output(1,1,1,1);   exec(1,0,1,1);   ctrl(0x20, 0x30, 0x20, 0x30);
		d.inpStart.tag.setInt(0);
		output(1,1,1,1);   exec(1,0,0,0);   ctrl(0x20, 0x30, 0x20, 0x30);
		d.tagStepSize.setInt(1);
		d.tagStepTime.setInt(1);
	}

	@Test
	public void testAlarm() {
		quickstart();
		tagMode1.setInt(0x0112);
		output(1,1,0,1);   exec(4,0,0,0);   ctrl(0x20, 0x30, 0x20, 0x30);
		output(1,0,1,1);   exec(4,1,0,0);   ctrl(0x20, 0x30, 0x20, 0x30);
		output(1,1,1,1);   exec(1,0,0,0);   ctrl(0x20, 0x30, 0x20, 0x30);
	}
	
	@Test
	public void testStopFlow() {
		quickstart();
		tagMode1.setInt(0x0212);
		output(1,1,0,1);   exec(4,0,0,0);   ctrl(0x20, 0x30, 0x20, 0x30);
		output(1,0,1,1);   exec(4,0,1,0);   ctrl(0x20, 0x30, 0x20, 0x30);
		output(1,1,1,1);   exec(1,0,0,0);   ctrl(0x20, 0x30, 0x20, 0x30);
	}
	
	@Test
	public void testStopRout() {
		quickstart();
		tagMode1.setInt(0x0412);
		output(1,1,0,1);   exec(4,0,0,0);   ctrl(0x20, 0x30, 0x20, 0x30);
		output(1,0,1,1);   exec(4,0,0,1);   ctrl(0x20, 0x30, 0x20, 0x30);
		output(1,1,1,1);   exec(1,0,0,0);   ctrl(0x20, 0x30, 0x20, 0x30);
	}
	
	@Test
	public void testDlyAlarm() {
		quickstart();
		d.tagDlyAlarm.setInt(2);
		tagMode1.setInt(0x0112);
		output(1,1,0,1);   exec(4,0,0,0);   ctrl(0x20, 0x30, 0x20, 0x30);
		output(1,0,1,1);   exec(4,0,0,0);   ctrl(0x20, 0x30, 0x20, 0x30);
		output(1,0,1,1);   exec(4,0,0,0);   ctrl(0x20, 0x30, 0x20, 0x30);
		output(1,0,1,1);   exec(4,1,0,0);   ctrl(0x20, 0x30, 0x20, 0x30);
		output(1,1,1,1);   exec(1,0,0,0);   ctrl(0x20, 0x30, 0x20, 0x30);
	}
	
	@Test
	public void testDlyStopFlow() {
		quickstart();
		d.tagDlyStopFlow.setInt(2);
		tagMode1.setInt(0x0212);
		output(1,1,0,1);   exec(4,0,0,0);   ctrl(0x20, 0x30, 0x20, 0x30);
		output(1,0,1,1);   exec(4,0,0,0);   ctrl(0x20, 0x30, 0x20, 0x30);
		output(1,0,1,1);   exec(4,0,0,0);   ctrl(0x20, 0x30, 0x20, 0x30);
		output(1,0,1,1);   exec(4,0,1,0);   ctrl(0x20, 0x30, 0x20, 0x30);
		output(1,1,1,1);   exec(1,0,0,0);   ctrl(0x20, 0x30, 0x20, 0x30);
	}
	
	@Test
	public void testDlyStopRout() {
		quickstart();
		d.tagDlyStopRout.setInt(2);
		tagMode1.setInt(0x0412);
		output(1,1,0,1);   exec(4,0,0,0);   ctrl(0x20, 0x30, 0x20, 0x30);
		output(1,0,1,1);   exec(4,0,0,0);   ctrl(0x20, 0x30, 0x20, 0x30);
		output(1,0,1,1);   exec(4,0,0,0);   ctrl(0x20, 0x30, 0x20, 0x30);
		output(1,0,1,1);   exec(4,0,0,1);   ctrl(0x20, 0x30, 0x20, 0x30);
		output(1,1,1,1);   exec(1,0,0,0);   ctrl(0x20, 0x30, 0x20, 0x30);
	}
	

	@Test
	public void testStart_zdvt() {
		tagMode0.setInt(0x0021);
		tagMode1.setInt(0x0022);
		tagMode2.setInt(0x0023);
		tagMode3.setInt(0x0024);
		
		d.tagStepSize.setInt(5);
		d.tagStepTime.setInt(0);

		d.inpStart.tag.setInt(1);
		output(0,0,0,0);   exec(1,0,1,1);   ctrl(0x100, 0x200, 0x300, 0x400);
	}

	@Test
	public void testStart_unknown() {
		tagMode0.setInt(0x0011);
		tagMode1.setInt(0x0022);
		tagMode2.setInt(0x0033);
		tagMode3.setInt(0x0044);
		
		d.tagStepSize.setInt(5);
		d.tagStepTime.setInt(0);

		d.inpStart.tag.setInt(1);
		output(0,0,0,0);   exec(1,0,1,1);   ctrl(0x20, 0x200, 0, 0);
		output(1,1,0,0);   exec(4,0,0,0);   ctrl(0x20, 0x200, 0, 0);
		output(1,1,1,1);   exec(1,0,0,0);   ctrl(0x20, 0x200, 0, 0);
	}


}
