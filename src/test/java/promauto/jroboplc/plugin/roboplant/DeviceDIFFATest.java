package promauto.jroboplc.plugin.roboplant;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static promauto.jroboplc.plugin.roboplant.AuxProcs.newInputAndTag;

import java.nio.file.Paths;

import org.junit.Before;
import org.junit.Test;

import promauto.jroboplc.core.api.InitUtils;
import promauto.jroboplc.core.tags.TagInt;

public class DeviceDIFFATest {
	private DeviceDIFFA d;
	
	@Before
	public void setUp() throws Exception {
		InitUtils.setupLogger();

		d = new DeviceDIFFA();
		d.inpInpVal 	= newInputAndTag();
		d.inpSetVal 	= newInputAndTag();
		
		d.tagOutVal		= new TagInt("", 0);
		d.tagInpVal		= new TagInt("", 0);
		d.tagSetVal		= new TagInt("", 0);
		d.tagCycleTime	= new TagInt("", 3);
		d.tagDlyStart	= new TagInt("", 5);
		d.tagGain		= new TagInt("", 110);
		d.tagMaxOutVal	= new TagInt("", 0);
		d.tagEnable		= new TagInt("", 1);

	}

	@Test
	public void testLoad() throws Exception {
		RoboplantModule robo = AuxProcs.createRoboplantModule();
		assertTrue( robo.loadProject( Paths.get( 
				InitUtils.getResourcesDir("roboplant", RoboplantModuleTest.class) + "diffa.xml" )));

		assertEquals(1, robo.devicesByAddr.length);
		d = (DeviceDIFFA)robo.devicesByAddr[0];
		assertTrue( d.init(robo) );
		assertTrue( d.prepare() );

		assertEquals( "DIFFA", d.devtype );
		assertEquals( "S02_FC", d.name );
		assertEquals( "S02_FC", d.tagname );
	}
	
	private void set(
			int inpval,
			int setval
			) {
		d.inpInpVal.tag.setInt(inpval);
		d.inpSetVal.tag.setInt(setval);
	}

	
	private void exec(
			int outVal	  
			) {
		d.execute();
		
		assertEquals("outVal"	, outVal	 , d.tagOutVal	 .getInt());     
	}


	@Test
	public void testExecute() {
		
		set(	0,	0); 		exec( 0 ); 
		set( 	0,	0); 		exec( 0 ); 
            	  	        	
		set(	0,	4500); 		exec( 4500 ); 
		set(	0,	4500); 		exec( 4500 ); 
		set(	0,	4500); 		exec( 4500 ); 
		set(	0,	4500); 		exec( 4500 ); 
		set(	0,	4500); 		exec( 4500 ); 
		set(	0,	4500); 		exec( 4500 ); 
		set(	0,	4500); 		exec( 4500 ); 
		set(	0,	4500); 		exec( 4500 ); 
		set(	0,	4500); 		exec( 4500 ); 
		set(	0,	4500); 		exec( 4500 ); 

		set( 1000,	4500); 		exec( 4500 ); 
		set( 1000,	4500); 		exec( 4500 ); 
		set( 1000,	4500); 		exec( 4500 ); 
		set( 1000,	4500); 		exec( 4500 ); 
		set( 1000,	4500); 		exec( 4500 ); 
		set( 1000,	4500); 		exec( 4500 ); 
		set( 1000,	4500); 		exec( 4500 ); 
		set( 1000,	4500); 		exec( 8350 ); 
		set( 1000,	4500); 		exec( 8350 ); 
		set( 1000,	4500); 		exec( 8350 ); 
		set( 1000,	4500); 		exec( 12200 ); 
		set( 1000,	4500); 		exec( 12200 ); 
		set( 1000,	4500); 		exec( 12200 ); 
		set( 1000,	4500); 		exec( 16050 ); 
		set( 1000,	4500); 		exec( 16050 ); 
		set( 1000,	4500); 		exec( 16050 ); 
		set( 1000,	4500); 		exec( 19900 ); 
		set( 1000,	4500); 		exec( 19900 ); 
		set( 1000,	4500); 		exec( 19900 ); 
		set( 1000,	4500); 		exec( 23750 ); 
		set( 1000,	4500); 		exec( 23750 ); 
		set( 1000,	4500); 		exec( 23750 ); 
		set( 1000,	4500); 		exec( 27600 ); 
		set( 1000,	4500); 		exec( 27600 ); 
		set( 1000,	4500); 		exec( 27600 ); 
		set( 1000,	4500); 		exec( 31450 ); 
		set( 1000,	4500); 		exec( 31450 ); 
		set( 1000,	4500); 		exec( 31450 ); 
		set( 1000,	4500); 		exec( 35300 ); 
		set( 1000,	4500); 		exec( 35300 ); 
		set( 1000,	4500); 		exec( 35300 ); 
		set( 1000,	4500); 		exec( 39150 ); 
		set( 1000,	4500); 		exec( 39150 ); 
		set( 1000,	4500); 		exec( 39150 ); 
		set( 1000,	4500); 		exec( 43000 ); 
		set( 1000,	4500); 		exec( 43000 ); 
		set( 1000,	4500); 		exec( 43000 ); 
		set( 1000,	4500); 		exec( 46850 ); 
		set( 1000,	4500); 		exec( 46850 ); 
		set( 1000,	4500); 		exec( 46850 ); 
		set( 1000,	4500); 		exec( 50700 ); 
		set( 1000,	4500); 		exec( 50700 ); 
		set( 1000,	4500); 		exec( 50700 ); 
		set( 1000,	4500); 		exec( 54550 ); 
		set( 1000,	4500); 		exec( 54550 ); 
		set( 1000,	4500); 		exec( 54550 ); 
		set( 1000,	4500); 		exec( 58400 ); 
		set( 1000,	4500); 		exec( 58400 ); 
		set( 1000,	4500); 		exec( 58400 ); 
		set( 1000,	4500); 		exec( 62250 ); 
		set( 1000,	4500); 		exec( 62250 ); 
		set( 1000,	4500); 		exec( 62250 );
		
		set( 1000,	4500); 		exec( 65535 ); 
		set( 1000,	4500); 		exec( 65535 ); 
		set( 1000,	4500); 		exec( 65535 ); 
		set( 1000,	4500); 		exec( 65535 ); 
		set( 1000,	4500); 		exec( 65535 ); 
		set( 1000,	4500); 		exec( 65535 ); 

		set( 20000,	4500); 		exec( 48485 ); 
		set( 20000,	4500); 		exec( 48485 ); 
		set( 20000,	4500); 		exec( 48485 ); 
		set( 20000,	4500); 		exec( 31435 ); 
		set( 20000,	4500); 		exec( 31435 ); 
		set( 20000,	4500); 		exec( 31435 ); 
		set( 20000,	4500); 		exec( 14385 ); 
		set( 20000,	4500); 		exec( 14385 ); 
		set( 20000,	4500); 		exec( 14385 ); 

		set( 20000,	4500); 		exec( 0 ); 
		set( 20000,	4500); 		exec( 0 ); 
		set( 20000,	4500); 		exec( 0 ); 
		set( 20000,	4500); 		exec( 0 ); 
		set( 20000,	4500); 		exec( 0 ); 

		set( 2000,	4500); 		exec( 0 );
		set( 2000,	4500); 		exec( 2750 ); 
		set( 2000,	4500); 		exec( 2750 ); 
		set( 2000,	4500); 		exec( 2750 ); 
		set( 2000,	4500); 		exec( 5500 ); 
		set( 2000,	4500); 		exec( 5500 ); 
		set( 2000,	4500); 		exec( 5500 ); 
		set( 2000,	4500); 		exec( 8250 ); 
		set( 2000,	4500); 		exec( 8250 ); 
		set( 2000,	4500); 		exec( 8250 ); 
		set( 2000,	4500); 		exec( 11000 ); 

		set( 	0,	4500); 		exec( 4500 ); 
		set( 	0,	4500); 		exec( 4500 ); 
		set( 	0,	4500); 		exec( 4500 ); 
		set( 	0,	4500); 		exec( 4500 ); 

		set( 2000,	4500); 		exec( 4500 ); 
		set( 2000,	4500); 		exec( 4500 ); 
		set( 2000,	4500); 		exec( 4500 ); 
		set( 2000,	4500); 		exec( 4500 ); 
		set( 2000,	4500); 		exec( 4500 ); 
		set( 2000,	4500); 		exec( 4500 ); 
		set( 2000,	4500); 		exec( 4500 ); 
		set( 2000,	4500); 		exec( 7250 ); 
		set( 2000,	4500); 		exec( 7250 ); 
		set( 2000,	4500); 		exec( 7250 ); 
		
		assertEquals( 2000, d.tagInpVal.getInt());
		assertEquals( 4500, d.tagSetVal.getInt());
	}

	
	@Test
	public void testMaxOutVal() {
		
		d.tagMaxOutVal.setInt(20000);
		
		set( 1000,	4500); 		exec( 4500 ); 
		set( 1000,	4500); 		exec( 4500 ); 
		set( 1000,	4500); 		exec( 4500 ); 
		set( 1000,	4500); 		exec( 4500 ); 
		set( 1000,	4500); 		exec( 4500 ); 
		set( 1000,	4500); 		exec( 4500 ); 
		set( 1000,	4500); 		exec( 4500 ); 
		set( 1000,	4500); 		exec( 8350 ); 
		set( 1000,	4500); 		exec( 8350 ); 
		set( 1000,	4500); 		exec( 8350 ); 
		set( 1000,	4500); 		exec( 12200 ); 
		set( 1000,	4500); 		exec( 12200 ); 
		set( 1000,	4500); 		exec( 12200 ); 
		set( 1000,	4500); 		exec( 16050 ); 
		set( 1000,	4500); 		exec( 16050 ); 
		set( 1000,	4500); 		exec( 16050 ); 
		set( 1000,	4500); 		exec( 19900 ); 
		set( 1000,	4500); 		exec( 19900 ); 
		set( 1000,	4500); 		exec( 19900 ); 
		set( 1000,	4500); 		exec( 20000 ); 
		set( 1000,	4500); 		exec( 20000 ); 
		set( 1000,	4500); 		exec( 20000 ); 
		set( 1000,	4500); 		exec( 20000 ); 
		set( 1000,	4500); 		exec( 20000 ); 
		set( 1000,	4500); 		exec( 20000 ); 
	}


	@Test
	public void testGain() {
		
		d.tagCycleTime.setInt(1);
		d.tagDlyStart.setInt(1);
		
		d.tagGain.setInt(90);
		set( 	0,	4500); 		exec( 4500 ); 
		set( 1000,	4500); 		exec( 4500 ); 
		set( 1000,	4500); 		exec( 7650 ); 
		set( 1000,	4500); 		exec( 10800 ); 
		set( 1000,	4500); 		exec( 13950 );
		
		d.tagGain.setInt(10);
		set( 	0,	4500); 		exec( 4500 ); 
		set( 1000,	4500); 		exec( 4500 ); 
		set( 1000,	4500); 		exec( 4850 ); 
		set( 1000,	4500); 		exec( 5200 ); 
		set( 1000,	4500); 		exec( 5550 );
		
		d.tagGain.setInt(0);
		set( 	0,	4500); 		exec( 4500 ); 
		set( 1000,	4500); 		exec( 4500 ); 
		set( 1000,	4500); 		exec( 4500 ); 
		set( 1000,	4500); 		exec( 4500 ); 
		set( 1000,	4500); 		exec( 4500 );
		
		d.tagGain.setInt(-10);
		set( 	0,	4500); 		exec( 4500 ); 
		set( 1000,	4500); 		exec( 4500 ); 
		set( 1000,	4500); 		exec( 4150 ); 
		set( 1000,	4500); 		exec( 3800 ); 
		set( 1000,	4500); 		exec( 3450 );
		
		
	}


	@Test
	public void testEnable() {

		set( 1000,	4500); 		exec( 4500 ); 
		set( 1000,	4500); 		exec( 4500 ); 
		set( 1000,	4500); 		exec( 4500 ); 
		set( 1000,	4500); 		exec( 4500 ); 
		set( 1000,	4500); 		exec( 4500 ); 
		set( 1000,	4500); 		exec( 4500 ); 
		set( 1000,	4500); 		exec( 4500 ); 
		set( 1000,	4500); 		exec( 8350 ); 
		set( 1000,	4500); 		exec( 8350 ); 
		set( 1000,	4500); 		exec( 8350 ); 

		d.tagEnable.setInt(0);
		
		set( 1000,	4500); 		exec( 4500 ); 
		set( 1000,	4500); 		exec( 4500 ); 
		set( 1000,	4500); 		exec( 4500 ); 
		set( 1000,	4500); 		exec( 4500 ); 
		set( 1000,	4500); 		exec( 4500 ); 
		set( 1000,	4500); 		exec( 4500 ); 
		set( 1000,	4500); 		exec( 4500 ); 
		set( 1000,	4500); 		exec( 4500 ); 
		set( 1000,	4500); 		exec( 4500 ); 
		set( 1000,	4500); 		exec( 4500 ); 
		set( 1000,	4500); 		exec( 4500 ); 
		set( 1000,	4500); 		exec( 4500 ); 
		set( 1000,	4500); 		exec( 4500 ); 
		set( 1000,	4500); 		exec( 4500 ); 
		set( 1000,	4500); 		exec( 4500 ); 

		d.tagEnable.setInt(1);
		set( 1000,	4500); 		exec( 4500 ); 
		set( 1000,	4500); 		exec( 4500 ); 
		set( 1000,	4500); 		exec( 4500 ); 
		set( 1000,	4500); 		exec( 4500 ); 
		set( 1000,	4500); 		exec( 4500 ); 
		set( 1000,	4500); 		exec( 4500 ); 
		set( 1000,	4500); 		exec( 4500 ); 
		set( 1000,	4500); 		exec( 8350 ); 
		set( 1000,	4500); 		exec( 8350 ); 
		set( 1000,	4500); 		exec( 8350 ); 
}

}
