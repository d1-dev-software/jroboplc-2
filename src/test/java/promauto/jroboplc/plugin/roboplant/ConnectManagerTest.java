package promauto.jroboplc.plugin.roboplant;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import promauto.jroboplc.core.ConfigurationYaml;
import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.Environment;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.InitUtils;
import promauto.jroboplc.core.api.Module;
import promauto.jroboplc.core.api.ModuleManager;

public class ConnectManagerTest {
	private static String CFG_DIR = InitUtils.getResourcesDir("roboplant", RoboplantModuleTest.class);

	private ConnectManager conman;
	Environment env;
	
	Module mod1;
	Module mod2;
	RoboplantModule robo1;
	RoboplantModule robo2;
	Device dev1;
	Device dev2;
	Device dev3;
	Device dev4;
	
	@Before
	public void setUp() throws Exception {
		InitUtils.setupLogger();
		

		mod1 = mock(Module.class);
		when(mod1.getName()).thenReturn("mod1");

		mod2 = mock(Module.class);
		when(mod2.getName()).thenReturn("mod2");

		robo1 = mock(RoboplantModule.class);
		when(robo1.getName()).thenReturn("robo1");

		robo2 = mock(RoboplantModule.class);
		when(robo2.getName()).thenReturn("robo2");

		dev1 = mock(Device.class);
		dev1.devtype = "PLCI";
		dev1.tagname = "M01";

		dev2 = mock(Device.class);
		dev2.devtype = "PLCO";
		dev2.tagname = "M01";

		dev3 = mock(Device.class);
		dev3.devtype = "PLRKS";
		dev3.tagname = "M02";

		dev4 = mock(Device.class);
		dev4.devtype = "PLDAT";
		dev4.tagname = "M04";

		
		ModuleManager mm = mock(ModuleManager.class);
		when(mm.getModule("mod1")).thenReturn(mod1);
		when(mm.getModule("mod2")).thenReturn(mod2);
		when(mm.getModule("robo1")).thenReturn(robo1);
		when(mm.getModule("robo2")).thenReturn(robo2);

		env = mock(Environment.class);
		EnvironmentInst.set(env);
		when(env.getModuleManager()).thenReturn(mm);

		Configuration conf = new ConfigurationYaml( CFG_DIR );
		if( !conf.load() ) 
			fail();
		when(env.getConfiguration()).thenReturn(conf);

		conman = new ConnectManager(env);
		
	}

	@Test
	public void testLoad() throws Exception {
		
		
		Map<String, String> conf = new HashMap<>();
		Map<String, Object> conf2 = new HashMap<>();
		conf.put("PLCI.M01", "mod1");
		conf.put("PLCO.M01", "mod2");
		conf.put("PLRKS.M02", "mod3");
		conf2.put("connect", conf);
		
		conman.loadConnectors(robo1, conf2);
		assertEquals(mod1, conman.getConnectedModule( robo1, dev1));
		assertEquals(mod2, conman.getConnectedModule( robo1, dev2));
		assertEquals(null, conman.getConnectedModule( robo1, dev3));
		
		conman.loadConnectors(robo1, conf2);
		assertEquals(mod1, conman.getConnectedModule( robo1, dev1));
		assertEquals(mod2, conman.getConnectedModule( robo1, dev2));
		assertEquals(null, conman.getConnectedModule( robo1, dev3));
		

		conf.clear();
		conf.put("PLDAT.M04", "mod1");

		conman.loadConnectors(robo2, conf2);
		assertEquals(mod1, conman.getConnectedModule( robo1, dev1));
		assertEquals(mod2, conman.getConnectedModule( robo1, dev2));
		assertEquals(null, conman.getConnectedModule( robo1, dev3));
		assertEquals(mod1, conman.getConnectedModule( robo2, dev4));
	}

}
