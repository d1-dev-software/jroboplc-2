package promauto.jroboplc.plugin.roboplant;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static promauto.jroboplc.plugin.roboplant.AuxProcs.newInputAndTag;

import java.nio.file.Paths;

import org.junit.Before;
import org.junit.Test;

import promauto.jroboplc.core.api.InitUtils;
import promauto.jroboplc.core.tags.TagInt;

public class DeviceGNRTest {
	private DeviceGNR d;
	
	@Before
	public void setUp() throws Exception {
		InitUtils.setupLogger();

		d = new DeviceGNR();
		d.inpEnabled 	= newInputAndTag();
		
		d.tagOutput		= new TagInt("", 0);
		d.tagLowTime	= new TagInt("", 2);
		d.tagHighTime	= new TagInt("", 3);
		d.tagCnt		= new TagInt("", 0);
		
	}

	@Test
	public void testLoad() throws Exception {
		RoboplantModule robo = AuxProcs.createRoboplantModule();
		assertTrue( robo.loadProject( Paths.get( 
				InitUtils.getResourcesDir("roboplant", RoboplantModuleTest.class) + "gnr.xml" )));

		assertEquals(1, robo.devicesByAddr.length);
		d = (DeviceGNR)robo.devicesByAddr[0];
		assertTrue( d.init(robo) );
		assertTrue( d.prepare() );

		assertEquals( "GNR", d.devtype );
		assertEquals( "Beep1", d.name );
		assertEquals( "Beep1", d.tagname );
	}
	
	private void set(
			int input
			) {
		d.inpEnabled.tag.setInt(input);
	}

	
	private void exec(
			int output ,
			int cnt
			) {
		d.execute();
		assertEquals("output"	, output	 , d.tagOutput.getInt());     
		assertEquals("cnt"		, cnt 		 , d.tagCnt.getInt());     
	}


	@Test
	public void testExecute() {
		set(0); 	exec(0,0);
		
		set(1); 	exec(0,1);
		set(1); 	exec(1,0);
		set(1); 	exec(1,1);
		set(1); 	exec(1,2);
		set(1); 	exec(0,0);
		set(1); 	exec(0,1);
		set(1); 	exec(1,0);
		set(1); 	exec(1,1);
		set(1); 	exec(1,2);
		set(1); 	exec(0,0);
		set(1); 	exec(0,1);
	}

}
