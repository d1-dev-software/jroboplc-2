package promauto.jroboplc.plugin.roboplant;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static promauto.jroboplc.plugin.roboplant.AuxProcs.newInputAndTag;

import java.nio.file.Paths;

import org.junit.Before;
import org.junit.Test;

import promauto.jroboplc.core.api.InitUtils;
import promauto.jroboplc.core.tags.TagInt;

public class DeviceGLSATest {
	private DeviceGLSA d;
	
	@Before
	public void setUp() throws Exception {
		InitUtils.setupLogger();

		d = new DeviceGLSA();
		d.inpInput 			= newInputAndTag();
		d.inpSuspend 		= newInputAndTag();
		d.inpAlarmEnable 	= newInputAndTag();
		
		d.tagSetCmd			= new TagInt("", 0);
		d.tagSost			= new TagInt("", 0);
		d.tagAlarm			= new TagInt("", 0);
		d.tagState			= new TagInt("", 0);
		d.tagControl		= new TagInt("", 0);
		d.tagSuspend		= new TagInt("", 0);
		d.tagBlok			= new TagInt("", 0);
	}

	@Test
	public void testLoad() throws Exception {
		RoboplantModule robo = AuxProcs.createRoboplantModule();
		assertTrue( robo.loadProject( Paths.get( 
				InitUtils.getResourcesDir("roboplant", RoboplantModuleTest.class) + "glsa.xml" )));

		assertEquals(1, robo.devicesByAddr.length);
		d = (DeviceGLSA)robo.devicesByAddr[0];
		assertTrue( d.init(robo) );
		assertTrue( d.prepare() );

		assertEquals( "GLSA", d.devtype );
		assertEquals( "WZRN", d.name );
		assertEquals( "WZRN", d.tagname );
	}
	
	private void set(
			int input		,
			int suspendInp 	,
			int alarmEnable ,
			int state		,
			int control	    ,
			int suspend	    
			
			) {
		d.inpInput		.tag.setInt(input);
		d.inpSuspend	.tag.setInt(suspendInp );
		d.inpAlarmEnable.tag.setInt(alarmEnable);
		d.tagState			.setInt(state);
		d.tagControl		.setInt(control);
		d.tagSuspend		.setInt(suspend);
		
	}

	
	private void exec(
			int setCmd	,
			int sost	,
			int alarm	
			) {
		d.execute();
		assertEquals("setCmd"	, 	 setCmd	, d.tagSetCmd	.getInt());     
		assertEquals("sost"		, 	 sost	, d.tagSost		.getInt());     
		assertEquals("alarm"	, 	 alarm	, d.tagAlarm	.getInt());     
	}


	@Test
	public void testExecute_blok_b7_0() {
		set(0,0,0, 0x00, 0,0); 	exec(2,3,0);
		set(1,0,0, 0x00, 0,0); 	exec(1,2,0);
		set(1,0,0, 0x2E, 0,0); 	exec(0,1,0);
		set(0,0,0, 0x2E, 0,0); 	exec(2,3,0);
		set(0,0,0, 0x01, 0,0); 	exec(0,0,0);
		set(0,0,0, 0x40, 0,0); 	exec(0,4,0);
		set(0,0,1, 0x40, 0,0); 	exec(0,4,1);
		set(0,0,1, 0x01, 0,0); 	exec(0,0,0);
		set(0,1,1, 0x01, 0,0); 	exec(0,5,0);
		set(1,1,1, 0x01, 0,0); 	exec(0,5,0);
		set(0,0,0, 0x01, 1,0); 	exec(0,0,0);
		set(1,0,0, 0x01, 1,0); 	exec(0,0,0);
		set(0,0,0, 0x01, 3,0); 	exec(1,2,0);
		set(0,0,0, 0x2e, 3,0); 	exec(0,1,0);
		set(0,0,0, 0x2e, 3,1); 	exec(2,3,0);
		set(0,0,0, 0x01, 3,1); 	exec(0,5,0);
	}

	@Test
	public void testExecute_blok_b7_1() {
		d.tagBlok.setInt(0x80);
		set(0,0,0, 0, 0,0); 	exec(0,0,0);
		set(1,0,0, 0, 0,0); 	exec(1,2,0);
		set(1,0,0, 1, 0,0); 	exec(0,1,0);
		set(0,0,0, 1, 0,0); 	exec(2,3,0);
		set(0,0,0, 0, 0,0); 	exec(0,0,0);
		set(0,0,0, 2, 0,0); 	exec(0,4,0);
		set(0,0,1, 2, 0,0); 	exec(0,4,1);
		set(0,0,1, 0, 0,0); 	exec(0,0,0);
		set(0,1,1, 0, 0,0); 	exec(0,5,0);
		set(1,1,1, 0, 0,0); 	exec(0,5,0);
		set(0,0,0, 0, 1,0); 	exec(0,0,0);
		set(1,0,0, 0, 1,0); 	exec(0,0,0);
		set(0,0,0, 0, 3,0); 	exec(1,2,0);
		set(0,0,0, 1, 3,0); 	exec(0,1,0);
		set(0,0,0, 1, 3,1); 	exec(2,3,0);
		set(0,0,0, 0, 3,1); 	exec(0,5,0);
	}

	
}
