package promauto.jroboplc.plugin.roboplant;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static promauto.jroboplc.plugin.roboplant.AuxProcs.addTagToList;
import static promauto.jroboplc.plugin.roboplant.AuxProcs.newInputAndTag;

import java.nio.file.Paths;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import org.mockito.MockitoAnnotations;
import promauto.jroboplc.core.api.InitUtils;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.TagInt;

public class DeviceSEQCTest {
	private DeviceSEQC d;
	
	Tag tagSeqc;
	Tag tagAddress0;
	Tag tagAddress1;
	Tag tagAddress2;
	Tag tagAddress3;
	Tag tagAddress4;
	Tag tagDelays0;
	Tag tagDelays1;
	Tag tagDelays2;
	Tag tagDelays3;
	Tag tagDelays4;
	Tag tagModeFlags0;
	Tag tagModeFlags1;
	Tag tagModeFlags2;
	Tag tagModeFlags3;
	Tag tagModeFlags4;

	@Mock RoboplantModule module;
	int mchbcount = 5;
	Device[] mchb = new Device[mchbcount];
	Tag[] mchbMasterOut = new Tag[mchbcount];
	Tag[] mchbSost = new Tag[mchbcount];
	Tag[] mchbControl = new Tag[mchbcount];

	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.openMocks(this);
		InitUtils.setupLogger();
		
		d = new DeviceSEQC();
		d.module = module;
		
		d.inpStart 		= newInputAndTag();
		d.inpStop 		= newInputAndTag();
		d.inpStopEmrg 	= newInputAndTag();
		d.inpStopFlow 	= newInputAndTag();
		
		d.tagSost    = new TagInt("Sost"	, 0);
		d.tagCurrentAddr = new TagInt("Current"	, 0);
		d.tagAlarm   = new TagInt("Alarm"	, 0);
		d.tagCode    = new TagInt("Code"	, 0);
		d.tagCheck   = new TagInt("Check"	, 0);
		d.tagFlags   = new TagInt("Flags"	, 0);

		
		
		tagAddress0	  = addTagToList( d.tagAddress	, 0 );	
		tagAddress1	  = addTagToList( d.tagAddress	, 1 );	
		tagAddress2	  = addTagToList( d.tagAddress	, 2 );	
		tagAddress3	  = addTagToList( d.tagAddress	, 3 );	
		tagAddress4	  = addTagToList( d.tagAddress	, 0xffff );	
		tagDelays0	  = addTagToList( d.tagDelays 	, 0 );	
		tagDelays1	  = addTagToList( d.tagDelays 	, 0 );	
		tagDelays2	  = addTagToList( d.tagDelays 	, 0 );	
		tagDelays3	  = addTagToList( d.tagDelays 	, 0 );	
		tagDelays4	  = addTagToList( d.tagDelays 	, 0 );	
		tagModeFlags0 = addTagToList( d.tagModeFlags, 0 );	
		tagModeFlags1 = addTagToList( d.tagModeFlags, 0 );	
		tagModeFlags2 = addTagToList( d.tagModeFlags, 0 );	
		tagModeFlags3 = addTagToList( d.tagModeFlags, 0 );	
		tagModeFlags4 = addTagToList( d.tagModeFlags, 0 );	

		d.addressCount = 5;
		d.initMchbItems();
		d.resetState();
		
		for (int i=0; i<mchbcount; i++) {
			mchb[i] 			= mock(Device.class);
			when(module.getDevice(i)).thenReturn(mchb[i]);
			
			mock1(i, 0, mchbMasterOut);
			mock1(i, 1, mchbSost);
			mock1(i, 6, mchbControl);
		}
		
		d.tagCode.setInt(5);
	}


	private void mock1(int i, int num, Tag[] tags) {
		tags[i] = new TagInt("tag_"+i+"_"+num, 0);
		Output output = new Output();
		output.tag = tags[i];
		when(mchb[i].getOutput(num)).thenReturn(output);
	}

	
	

	
	@Test
	public void testLoad() throws Exception {
		
		RoboplantModule robo = AuxProcs.createRoboplantModule();
		assertTrue( robo.loadProject( Paths.get( 
				InitUtils.getResourcesDir("roboplant", RoboplantModuleTest.class) + "seqc.xml" )));

		assertEquals(1, robo.devicesByAddr.length);
		DeviceSEQC d = (DeviceSEQC)robo.devicesByAddr[0];
		
		assertEquals( "SEQC", d.devtype );
		assertEquals( "SC01", d.tagname );
		
		assertTrue( d.init(robo) );
		assertTrue( d.prepare() );

		assertEquals(80, d.tagAddress.size());
		assertEquals(80, d.tagDelays.size());
		assertEquals(80, d.tagModeFlags.size());
		assertEquals(80, d.mchb.length);
		assertEquals(0, d.mchb[0].state);
		assertEquals(-1, d.mchb[0].address);
		assertEquals(0, d.mchb[0].getMasteroutValue());
		assertEquals(0, d.mchb[0].getSostValue());
		assertEquals(0, d.mchb[0].getControlValue());

	}

	
	
	private void exec(
			int sost,       
			int currentAddr,       
			int alarm,       
			int code,       
			int check,       
			int flags       
			) {
		
		d.execute();	
		assertEquals("sost",        	sost,        d.tagSost.getInt());       
		assertEquals("currentAddr", 	currentAddr, d.tagCurrentAddr.getInt());   
		assertEquals("alarm",       	alarm,       d.tagAlarm.getInt());      
		assertEquals("code",        	code,        d.tagCode.getInt());        
		assertEquals("check",       	check,       d.tagCheck.getInt());        
		assertEquals("flags",        	flags,       d.tagFlags.getInt());           
	}

	private void ctrl(int...values) {
		for (int i=0; i<values.length; i++)
			assertEquals( "mchbControl["+i+"]", values[i], mchbControl[i].getInt());
	}
	
	private void mast(int...values) {
		for (int i=0; i<values.length; i++)
			mchbMasterOut[i].setInt(values[i]);
	}
	
	private void sost(int...values) {
		for (int i=0; i<values.length; i++)
			mchbSost[i].setInt(values[i]);
	}
	
	@Test
	public void testInit() {
		assertEquals(5, d.mchb.length);
		assertEquals(0, d.mchb[0].state);
		assertEquals(-1, d.mchb[0].address);
		assertEquals(0, d.mchb[0].getMasteroutValue());
		assertEquals(0, d.mchb[0].getSostValue());
		assertEquals(0, d.mchb[0].getControlValue());
		
	}

	
	@Test
	public void testLinkMchbItems_bad() {
		when(module.getDevice(0)).thenReturn(null);
		when(module.getDevice(2)).thenReturn(null);
		when(mchb[3].getOutput(6)).thenReturn(null);
		d.linkMchbItems();
		
		assertEquals(			65535, 	d.mchb[0].address);
		assertSame(            0, 	d.mchb[0].getMasteroutValue());
		assertSame(            0, 	d.mchb[0].getSostValue());
		assertSame(            0, 	d.mchb[0].getControlValue());
		
		assertEquals(			 1, 	d.mchb[1].address);
		assertSame(mchbMasterOut[1].getInt(), 	d.mchb[1].getMasteroutValue());
		assertSame(     mchbSost[1].getInt(), 	d.mchb[1].getSostValue());
		assertSame(  mchbControl[1].getInt(), 	d.mchb[1].getControlValue());
		
		assertEquals(			 65535,	d.mchb[2].address);
		assertSame(            0, 	d.mchb[2].getMasteroutValue());
		assertSame(            0, 	d.mchb[2].getSostValue());
		assertSame(            0, 	d.mchb[2].getControlValue());
		
		assertEquals(			 3, 	d.mchb[3].address);
		assertSame(mchbMasterOut[3].getInt(), 	d.mchb[3].getMasteroutValue());
		assertSame(     mchbSost[3].getInt(), 	d.mchb[3].getSostValue());
		assertSame(  		   0, 		d.mchb[3].getControlValue());
		
		assertEquals(	     0xffff, 	d.mchb[4].address);
		assertSame(            0, 	d.mchb[4].getMasteroutValue());
		assertSame(            0, 	d.mchb[4].getSostValue());
		assertSame(            0, 	d.mchb[4].getControlValue());
	}
	
	@Test
	public void testLinkMchbItems_good() {
		d.linkMchbItems();
		
		assertEquals(			 0, 	d.mchb[0].address);
		assertSame(mchbMasterOut[0].getInt(), 	d.mchb[0].getMasteroutValue());
		assertSame(     mchbSost[0].getInt(), 	d.mchb[0].getSostValue());
		assertSame(  mchbControl[0].getInt(), 	d.mchb[0].getControlValue());
		
		assertEquals(			 1, 	d.mchb[1].address);
		assertSame(mchbMasterOut[1].getInt(), 	d.mchb[1].getMasteroutValue());
		assertSame(     mchbSost[1].getInt(), 	d.mchb[1].getSostValue());
		assertSame(  mchbControl[1].getInt(), 	d.mchb[1].getControlValue());
		
		assertEquals(			 2, 	d.mchb[2].address);
		assertSame(mchbMasterOut[2].getInt(), 	d.mchb[2].getMasteroutValue());
		assertSame(     mchbSost[2].getInt(), 	d.mchb[2].getSostValue());
		assertSame(  mchbControl[2].getInt(), 	d.mchb[2].getControlValue());
		
		assertEquals(			 3, 	d.mchb[3].address);
		assertSame(mchbMasterOut[3].getInt(), 	d.mchb[3].getMasteroutValue());
		assertSame(     mchbSost[3].getInt(), 	d.mchb[3].getSostValue());
		assertSame(  mchbControl[3].getInt(), 	d.mchb[3].getControlValue());
		
		assertEquals(		 0xffff, 	d.mchb[4].address);
		assertSame(            0, 	d.mchb[4].getMasteroutValue());
		assertSame(            0, 	d.mchb[4].getSostValue());
		assertSame(            0, 	d.mchb[4].getControlValue());
	}

	@Test
	public void testStart_fast() {
		tagDelays0.setInt(0x0000);
		tagDelays1.setInt(0x0100);
		tagDelays2.setInt(0x0200);
		tagDelays3.setInt(0x0300);
		
		mast(0,0,0,0,0); sost(0,0,0,0,0);  exec(0,65535,0,5,0,0); ctrl( 0, 0, 0, 0, 0);

		d.inpStart.tag.setInt(1);
		//1
		mast(1,1,1,1,1); sost(1,1,1,1,1);  exec(1,    1,0,5,0,0); ctrl(16, 0, 0, 0, 0);
		
		//2
		mast(1,1,1,1,1); sost(1,1,1,1,1);  exec(1,    2,0,5,0,0); ctrl(16,16, 0, 0, 0);
		
		//3
		mast(1,1,1,1,1); sost(1,1,1,1,1);  exec(1,    2,0,5,0,0); ctrl(16,16,16, 0, 0);
		mast(1,1,1,1,1); sost(1,1,1,1,1);  exec(1,    3,0,5,0,0); ctrl(16,16,16, 0, 0);
		
		//4
		mast(1,1,1,1,1); sost(1,1,1,1,1);  exec(1,    3,0,5,0,0); ctrl(16,16,16,16, 0);
		mast(1,1,1,1,1); sost(1,1,1,1,1);  exec(1,    3,0,5,0,0); ctrl(16,16,16,16, 0);

		// run
		mast(1,1,1,1,1); sost(1,1,1,1,1);  exec(2,65535,0,5,0,0); ctrl(16,16,16,16, 0);
		
		mast(1,1,1,1,1); sost(1,1,1,1,1);  exec(2,65535,0,5,0,0); ctrl(16,16,16,16, 0);
		mast(1,1,1,1,1); sost(1,1,1,1,1);  exec(2,65535,0,5,0,0); ctrl(16,16,16,16, 0);
	}
	
	@Test
	public void testStart_slow() {
		tagDelays0.setInt(0x0000);
		tagDelays1.setInt(0x0100);
		tagDelays2.setInt(0x0200);
		tagDelays3.setInt(0x0500);
		
		mast(0,0,0,0,0); sost(0,0,0,0,0);  exec(0,65535,0,5,0,0); ctrl( 0, 0, 0, 0, 0);

		d.inpStart.tag.setInt(1);
		//1
		mast(0,0,0,0,0); sost(0,0,0,0,0);  exec(1,    0,0,5,0,0); ctrl(16, 0, 0, 0, 0);
		mast(0,0,0,0,0); sost(2,0,0,0,0);  exec(1,    0,0,5,0,0); ctrl(16, 0, 0, 0, 0);
		mast(1,0,0,0,0); sost(1,0,0,0,0);  exec(1,    1,0,5,0,0); ctrl(16, 0, 0, 0, 0);
		
		//2
		mast(1,0,0,0,0); sost(1,0,0,0,0);  exec(1,    1,0,5,0,0); ctrl(16,16, 0, 0, 0);
		mast(1,0,0,0,0); sost(1,2,0,0,0);  exec(1,    1,0,5,0,0); ctrl(16,16, 0, 0, 0);
		mast(1,1,0,0,0); sost(1,1,0,0,0);  exec(1,    2,0,5,0,0); ctrl(16,16, 0, 0, 0);
		
		//3
		mast(1,1,0,0,0); sost(1,1,0,0,0);  exec(1,    2,0,5,0,0); ctrl(16,16,16, 0, 0);
		mast(1,1,0,0,0); sost(1,1,2,0,0);  exec(1,    2,0,5,0,0); ctrl(16,16,16, 0, 0);
		mast(1,1,1,0,0); sost(1,1,1,0,0);  exec(1,    3,0,5,0,0); ctrl(16,16,16, 0, 0);
		
		//4
		mast(1,1,1,0,0); sost(1,1,1,0,0);  exec(1,    3,0,5,0,0); ctrl(16,16,16,16, 0);
		mast(1,1,1,0,0); sost(1,1,1,2,0);  exec(1,    3,0,5,0,0); ctrl(16,16,16,16, 0);
		mast(1,1,1,1,0); sost(1,1,1,1,0);  exec(1,    3,0,5,0,0); ctrl(16,16,16,16, 0);
		mast(1,1,1,1,0); sost(1,1,1,1,0);  exec(1,    3,0,5,0,0); ctrl(16,16,16,16, 0);

		// run
		mast(1,1,1,1,1); sost(1,1,1,1,1);  exec(2,65535,0,5,0,0); ctrl(16,16,16,16, 0);
	}


	@Test
	public void testStart_fail() {
		tagDelays0.setInt(0x0000);
		tagDelays1.setInt(0x0100);
		tagDelays2.setInt(0x0200);
		tagDelays3.setInt(0x0500);
		
		mast(0,0,0,0,0); sost(0,0,0,0,0);  exec(0,65535,0,5,0,0); ctrl( 0, 0, 0, 0, 0);

		d.inpStart.tag.setInt(1);
		//1
		mast(0,0,0,0,0); sost(0,0,0,0,0);  exec(1,    0,0,5,0,0); ctrl(16, 0, 0, 0, 0);
		mast(0,0,0,0,0); sost(2,0,0,0,0);  exec(1,    0,0,5,0,0); ctrl(16, 0, 0, 0, 0);
		mast(1,0,0,0,0); sost(1,0,0,0,0);  exec(1,    1,0,5,0,0); ctrl(16, 0, 0, 0, 0);
		
		//2
		mast(1,0,0,0,0); sost(1,0,0,0,0);  exec(1,    1,0,5,0,0); ctrl(16,16, 0, 0, 0);
		mast(1,0,0,0,0); sost(1,2,0,0,0);  exec(1,    1,0,5,0,0); ctrl(16,16, 0, 0, 0);
		mast(1,1,0,0,0); sost(1,1,0,0,0);  exec(1,    2,0,5,0,0); ctrl(16,16, 0, 0, 0);
		
		//3
		mast(1,1,0,0,0); sost(1,1,0,0,0);  exec(1,    2,0,5,0,0); ctrl(16,16,16, 0, 0);
		// fail
		mast(1,0,0,0,0); sost(1,4,2,0,0);  exec(4,    1,1,5,0,0); ctrl(16,16, 0, 0, 0);

		//2
		mast(1,0,0,0,0); sost(1,0,0,0,0);  exec(1,    1,0,5,0,0); ctrl(16,16, 0, 0, 0);
		mast(1,0,0,0,0); sost(1,0,0,0,0);  exec(1,    1,0,5,0,0); ctrl(16,16, 0, 0, 0);
		mast(1,0,0,0,0); sost(1,2,0,0,0);  exec(1,    1,0,5,0,0); ctrl(16,16, 0, 0, 0);
		mast(1,1,0,0,0); sost(1,1,0,0,0);  exec(1,    2,0,5,0,0); ctrl(16,16, 0, 0, 0);

		//3
		mast(1,1,0,0,0); sost(1,1,0,0,0);  exec(1,    2,0,5,0,0); ctrl(16,16,16, 0, 0);
		
		// ...and so on
	}


	public void quickstart() {
		tagDelays0.setInt(0x0000);
		tagDelays1.setInt(0x0000);
		tagDelays2.setInt(0x0000);
		tagDelays3.setInt(0x0000);
		
		d.inpStart.tag.setInt(1);
		mast(1,1,1,1,1); sost(1,1,1,1,1);
		exec(1,    1,0,5,0,0); ctrl(16, 0, 0, 0, 0);
		exec(1,    2,0,5,0,0); ctrl(16,16, 0, 0, 0);
		exec(1,    3,0,5,0,0); ctrl(16,16,16, 0, 0);
		exec(2,65535,0,5,0,0); ctrl(16,16,16,16, 0);
		d.inpStart.tag.setInt(0);
	}

	@Test
	public void testStop() {
		quickstart();

		tagDelays0.setInt(0x0000);
		tagDelays1.setInt(0x0001);
		tagDelays2.setInt(0x0002);
		tagDelays3.setInt(0x0003);
		
		mast(1,1,1,1,1); sost(1,1,1,1,1);  exec(2,65535,0,5,0,0); ctrl(16,16,16,16, 0);

		d.inpStop.tag.setInt(1);
		mast(1,1,1,1,1); sost(1,1,1,1,1);  exec(3,    3,0,5,0,0); ctrl(16,16,16,16, 0);
		
		// 3
		mast(1,1,1,1,1); sost(1,1,1,1,1);  exec(3,    3,0,5,0,0); ctrl(16,16,16, 0, 0);
		mast(1,1,1,1,1); sost(1,1,1,1,1);  exec(3,    3,0,5,0,0); ctrl(16,16,16, 0, 0);
		mast(1,1,1,1,1); sost(1,1,1,1,1);  exec(3,    2,0,5,0,0); ctrl(16,16,16, 0, 0);
		
		// 2
		mast(1,1,1,1,1); sost(1,1,1,1,1);  exec(3,    2,0,5,0,0); ctrl(16,16, 0, 0, 0);
		mast(1,1,1,1,1); sost(1,1,1,1,1);  exec(3,    1,0,5,0,0); ctrl(16,16, 0, 0, 0);
		
		// 1
		mast(1,1,1,1,1); sost(1,1,1,1,1);  exec(3,    0,0,5,0,0); ctrl(16, 0, 0, 0, 0);
		
		// 0, off
		mast(1,1,1,1,1); sost(1,1,1,1,1);  exec(0,65535,0,5,0,0); ctrl( 0, 0, 0, 0, 0);
	}
	
	
	@Test
	public void testStop_fast() {
		quickstart();
		
		d.inpStop.tag.setInt(1);
		mast(1,1,1,1,1); sost(1,1,1,1,1);  exec(3,    3,0,5,0,0); ctrl(16,16,16,16, 0);
		mast(1,1,1,1,1); sost(1,1,1,1,1);  exec(3,    2,0,5,0,0); ctrl(16,16,16, 0, 0);
		mast(1,1,1,1,1); sost(1,1,1,1,1);  exec(3,    1,0,5,0,0); ctrl(16,16, 0, 0, 0);
		mast(1,1,1,1,1); sost(1,1,1,1,1);  exec(3,    0,0,5,0,0); ctrl(16, 0, 0, 0, 0);
		mast(1,1,1,1,1); sost(1,1,1,1,1);  exec(0,65535,0,5,0,0); ctrl( 0, 0, 0, 0, 0);
	}

	
	@Test
	public void testStop_otherSeqc() {
		DeviceSEQC d1 = mock(DeviceSEQC.class);
		DeviceSEQC d2 = mock(DeviceSEQC.class);
		when(d1.checkMchbState(1)).thenReturn(true);
		when(d1.checkMchbState(2)).thenReturn(true);
		when(d2.checkMchbState(1)).thenReturn(true);
		d.otherSeqc.add(d1);
		d.otherSeqc.add(d2);
		
		quickstart();
		
		d.inpStop.tag.setInt(1);
		mast(1,1,1,1,1); sost(1,1,1,1,1);  exec(3,    3,0,5,0,0); ctrl(16,16,16,16, 0);
		mast(1,1,1,1,1); sost(1,1,1,1,1);  exec(3,    2,0,5,0,0); ctrl(16,16,16, 0, 0);
		mast(1,1,1,1,1); sost(1,1,1,1,1);  exec(3,    1,0,5,0,0); ctrl(16,16,16, 0, 0);
		mast(1,1,1,1,1); sost(1,1,1,1,1);  exec(3,    0,0,5,0,0); ctrl(16,16,16, 0, 0);
		mast(1,1,1,1,1); sost(1,1,1,1,1);  exec(0,65535,0,5,0,0); ctrl( 0,16,16, 0, 0);
	}


	@Test
	public void testAlarm() {
		quickstart();
		
		tagDelays0.setInt(0x0505);
		tagDelays1.setInt(0x0505);
		tagDelays2.setInt(0x0505);
		tagDelays3.setInt(0x0505);

		// run 
		mast(1,1,1,1); sost(1,1,1,1);  exec(2,65535,0,5,0,0); ctrl(16,16,16,16);

		// fail 1
		mast(1,0,1,1); sost(1,4,1,1);  exec(4,    1,1,5,0,0); ctrl(16,16, 0, 0);
		mast(1,1,0,0); sost(1,1,0,0);  exec(4,    1,1,5,0,0); ctrl(16,16, 0, 0);
		
		// restart 
		d.inpStart.tag.setInt(1);
		mast(1,1,0,0); sost(1,1,0,0);  exec(1,    1,0,5,0,0); ctrl(16,16, 0, 0);
		d.inpStart.tag.setInt(0);
		
		mast(1,1,0,0); sost(1,1,0,0);  exec(1,    2,0,5,0,0); ctrl(16,16, 0, 0);
		
		// 2
		mast(1,1,0,0); sost(1,1,0,0);  exec(1,    2,0,5,0,0); ctrl(16,16,16, 0);
		mast(1,1,1,0); sost(1,1,1,0);  exec(1,    2,0,5,0,0); ctrl(16,16,16, 0);
		mast(1,1,1,0); sost(1,1,1,0);  exec(1,    2,0,5,0,0); ctrl(16,16,16, 0);
		mast(1,1,1,0); sost(1,1,1,0);  exec(1,    2,0,5,0,0); ctrl(16,16,16, 0);
		mast(1,1,1,0); sost(1,1,1,0);  exec(1,    3,0,5,0,0); ctrl(16,16,16, 0);
		
		//3
		mast(1,1,1,0); sost(1,1,1,0);  exec(1,    3,0,5,0,0); ctrl(16,16,16,16);
		mast(1,1,1,1); sost(1,1,1,1);  exec(1,    3,0,5,0,0); ctrl(16,16,16,16);
		mast(1,1,1,1); sost(1,1,1,1);  exec(1,    3,0,5,0,0); ctrl(16,16,16,16);
		mast(1,1,1,1); sost(1,1,1,1);  exec(1,    3,0,5,0,0); ctrl(16,16,16,16);
		mast(1,1,1,1); sost(1,1,1,1);  exec(2,65535,0,5,0,0); ctrl(16,16,16,16);
	}


	@Test
	public void testStopEmrg() {
		quickstart();
		
		tagDelays0.setInt(0x0505);
		tagDelays1.setInt(0x0505);
		tagDelays2.setInt(0x0505);
		tagDelays3.setInt(0x0505);

		d.inpStopEmrg.tag.setInt(1);
		mast(1,1,1,1); sost(1,1,1,1);  exec(0,65535,0,5,0,0); ctrl( 0, 0, 0, 0);
	}

	@Test
	public void testStopFlow() {
		quickstart();

		tagModeFlags3.setInt(0b0_0001_0000);
		mast(1,1,1,1); sost(1,1,1,1);  exec(2,65535,0,5,0,0); ctrl(16,16,16,16);

		d.inpStopFlow.tag.setInt(1);
		mast(1,1,1,1); sost(1,1,1,1);  exec(1,    3,0,5,0,0); ctrl(16,16,16, 0);

		d.inpStopFlow.tag.setInt(0);
		mast(1,1,1,1); sost(1,1,1,1);  exec(2,65535,0,5,0,0); ctrl(16,16,16,16);
	}

	@Test
	public void testStopFlow_no_effect() {
		quickstart();

		d.inpStopFlow.tag.setInt(1);
		mast(1,1,1,1); sost(1,1,1,1);  exec(2,65535,0,5,0,0); ctrl(16,16,16,16);
	}

	@Test
	public void testModeFlags_b0() {
		d.inpStart.tag.setInt(1);
		mast(0,0,0,0); sost(0,0,0,0);  exec(1,    0,0,5,0,0); ctrl(16, 0, 0, 0);

		quickstart();

		tagModeFlags3.setInt(0b0_0000_0001);
		mast(1,0,1,1); sost(1,4,1,1);  exec(4,    1,1,5,0,0); ctrl(16,16, 0,16);
		// restart 
		d.inpStart.tag.setInt(1);
		mast(1,1,1,1); sost(1,1,1,1);  exec(1,    1,0,5,0,0); ctrl(16,16, 0,16);
		mast(1,1,1,1); sost(1,1,1,1);  exec(1,    2,0,5,0,0); ctrl(16,16, 0,16);
		mast(1,1,1,1); sost(1,1,1,1);  exec(1,    3,0,5,0,0); ctrl(16,16,16,16);
		mast(1,1,1,1); sost(1,1,1,1);  exec(2,65535,0,5,0,0); ctrl(16,16,16,16);

	}

	@Test
	public void testModeFlags_b1b2_1_start() {
		tagModeFlags1.setInt(0b0_0000_0100);
		d.inpStart.tag.setInt(1);
		mast(0,0,0,0); sost(0,0,0,0);  exec(1,    0,0,5,0,0); ctrl(16, 0, 0, 0);
		mast(1,0,0,0); sost(1,0,0,0);  exec(1,    1,0,5,0,0); ctrl(16, 0, 0, 0);
		mast(1,0,0,0); sost(1,0,0,0);  exec(1,    1,0,5,0,0); ctrl(16,16, 0, 0);
		mast(1,0,0,0); sost(1,0,0,0);  exec(1,    1,0,5,0,0); ctrl(16,16, 0, 0);
	}

	@Test
	public void testModeFlags_b1b2_1_run() {
		quickstart();

		tagModeFlags1.setInt(0b0_0000_0110);
		mast(1,0,1,1); sost(1,4,1,1);  exec(2,65535,0,5,0,0); ctrl(16,16,16,16);
		mast(1,1,1,1); sost(1,1,1,1);  exec(2,65535,0,5,0,0); ctrl(16,16,16,16);
	}

	@Test
	public void testModeFlags_b1b2_2_start() {
		tagModeFlags1.setInt(0b0_0000_0110);
		d.inpStart.tag.setInt(1);
		// 0
		mast(0,0,0,0); sost(0,0,0,0);  exec(1,    0,0,5,0,0); ctrl(16, 0, 0, 0);
		mast(1,0,0,0); sost(1,0,0,0);  exec(1,    1,0,5,0,0); ctrl(16, 0, 0, 0);
		// 1
		mast(1,0,0,0); sost(1,0,0,0);  exec(1,    2,0,5,0,0); ctrl(16,16, 0, 0);
		// 2
		mast(1,0,0,0); sost(1,0,0,0);  exec(1,    2,0,5,0,0); ctrl(16,16,16, 0);
		mast(1,0,1,0); sost(1,0,1,0);  exec(1,    3,0,5,0,0); ctrl(16,16,16, 0);
		// 3
		mast(1,0,1,0); sost(1,0,1,0);  exec(1,    3,0,5,0,0); ctrl(16,16,16,16);
		mast(1,0,1,0); sost(1,0,1,0);  exec(1,    3,0,5,0,0); ctrl(16,16,16,16);
		mast(1,0,1,0); sost(1,0,1,0);  exec(1,    3,0,5,0,0); ctrl(16,16,16,16);
		mast(1,0,1,1); sost(1,0,1,1);  exec(2,65535,0,5,0,0); ctrl(16,16,16,16);
	}

	@Test
	public void testModeFlags_b1b2_2_run() {
		quickstart();

		tagModeFlags1.setInt(0b0_0000_0100);
		mast(1,0,1,1); sost(1,4,1,1);  exec(2,65535,0,5,0,0); ctrl(16,16,16,16);
		mast(1,1,1,1); sost(1,1,1,1);  exec(2,65535,0,5,0,0); ctrl(16,16,16,16);
	}


	@Test
	public void testModeFlags_b3() {
		quickstart();

		tagModeFlags1.setInt(0b0_0000_1100);
		tagModeFlags3.setInt(0b0_0001_0000);
		mast(1,1,1,1); sost(1,1,1,1);  exec(2,65535,0,5,0,0); ctrl(16,16,16,16);

		mast(1,0,1,1); sost(1,0,1,1);  exec(1,    3,0,5,0,0); ctrl(16,16,16, 0);
		mast(1,0,1,1); sost(1,0,1,1);  exec(1,    3,0,5,0,0); ctrl(16,16,16, 0);
		
		mast(1,1,1,1); sost(1,1,1,1);  exec(2,65535,0,5,0,0); ctrl(16,16,16,16);
	}

	
	@Test
	public void testModeFlags_b5() {
		tagModeFlags1.setInt(0b0_0010_0000);
		d.inpStart.tag.setInt(1);
		// 0
		mast(0,0,0,0); sost(0,0,0,0);  exec(1,    0,0,5,0,0); ctrl(16, 0, 0, 0);
		mast(1,0,0,0); sost(1,0,0,0);  exec(1,    1,0,5,0,0); ctrl(16, 0, 0, 0);
		// 1
		mast(1,0,0,0); sost(1,0,0,0);  exec(1,    1,0,5,0,0); ctrl(16, 0, 0, 0);
		mast(1,0,0,0); sost(1,0,0,0);  exec(1,    1,0,5,0,0); ctrl(16, 0, 0, 0);
		mast(1,0,0,0); sost(1,0,0,0);  exec(1,    1,0,5,0,0); ctrl(16, 0, 0, 0);
		mast(1,0,0,0); sost(1,0,0,0);  exec(1,    1,0,5,0,0); ctrl(16, 0, 0, 0);
		mast(1,1,0,0); sost(1,1,0,0);  exec(1,    2,0,5,0,0); ctrl(16, 0, 0, 0);
		// 2
		mast(1,1,0,0); sost(1,0,0,0);  exec(1,    2,0,5,0,0); ctrl(16, 0,16, 0);
		mast(1,1,1,0); sost(1,0,1,0);  exec(1,    3,0,5,0,0); ctrl(16, 0,16, 0);
		// 3
		mast(1,1,1,0); sost(1,0,1,0);  exec(1,    3,0,5,0,0); ctrl(16, 0,16,16);
		mast(1,1,1,0); sost(1,0,1,0);  exec(1,    3,0,5,0,0); ctrl(16, 0,16,16);
		mast(1,1,1,0); sost(1,0,1,0);  exec(1,    3,0,5,0,0); ctrl(16, 0,16,16);
		mast(1,1,1,1); sost(1,0,1,1);  exec(2,65535,0,5,0,0); ctrl(16, 0,16,16);
	}

	@Test
	public void testModeFlags_b15b8() {
		quickstart();

		tagModeFlags1.setInt(0b0_0000__0000_1100);
		tagModeFlags3.setInt(0b0_0111__0001_0000);
		mast(1,1,1,1); sost(1,1,1,1);  exec(2,65535,0,5,0,0); ctrl(16,16,16,16);

		mast(1,0,1,1); sost(1,0,1,1);  exec(2,65535,0,5,0,0); ctrl(16,16,16,16);
		mast(1,0,1,1); sost(1,0,1,1);  exec(2,65535,0,5,0,0); ctrl(16,16,16,16);
		mast(1,0,1,1); sost(1,0,1,1);  exec(2,65535,0,5,0,0); ctrl(16,16,16,16);
		mast(1,0,1,1); sost(1,0,1,1);  exec(2,65535,0,5,0,0); ctrl(16,16,16,16);
		mast(1,0,1,1); sost(1,0,1,1);  exec(2,65535,0,5,0,0); ctrl(16,16,16,16);
		mast(1,0,1,1); sost(1,0,1,1);  exec(2,65535,0,5,0,0); ctrl(16,16,16,16);
		mast(1,0,1,1); sost(1,0,1,1);  exec(2,65535,0,5,0,0); ctrl(16,16,16,16);

		mast(1,0,1,1); sost(1,0,1,1);  exec(1,    3,0,5,0,0); ctrl(16,16,16, 0);
		mast(1,0,1,1); sost(1,0,1,1);  exec(1,    3,0,5,0,0); ctrl(16,16,16, 0);
		
		mast(1,1,1,1); sost(1,1,1,1);  exec(2,65535,0,5,0,0); ctrl(16,16,16,16);
	}


	@Test
	public void testCheckMchbState() {
		assertFalse( d.checkMchbState(0));
		assertFalse( d.checkMchbState(1));
		assertFalse( d.checkMchbState(2));
		assertFalse( d.checkMchbState(3));
		assertFalse( d.checkMchbState(4));
		assertFalse( d.checkMchbState(5));

		quickstart();
		assertTrue(  d.checkMchbState(0));
		assertTrue(  d.checkMchbState(1));
		assertTrue(  d.checkMchbState(2));
		assertTrue(  d.checkMchbState(3));
		assertFalse( d.checkMchbState(4));
		assertFalse( d.checkMchbState(5));

		mast(1,0,1,1); sost(1,0,1,1);  exec(1,    1,0,5,0,0); ctrl(16,16, 0, 0);
		assertTrue(  d.checkMchbState(0));
		assertTrue(  d.checkMchbState(1));
		assertFalse( d.checkMchbState(2));
		assertFalse( d.checkMchbState(3));
		assertFalse( d.checkMchbState(4));
		assertFalse( d.checkMchbState(5));
	}

}
