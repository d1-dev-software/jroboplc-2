package promauto.jroboplc.plugin.roboplant;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.nio.file.Paths;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import promauto.jroboplc.core.api.InitUtils;
import promauto.jroboplc.core.tags.TagInt;


public class DeviceMDTATest {

	private DeviceMDTA d;

	@Mock RoboplantModule module;


	@Before
	public void setUp() throws Exception {
		InitUtils.setupLogger();
		
		d = new DeviceMDTA();
		d.module = module;

		d.inpTrigRes = new Input();
		d.inpMchaSost = new Input();

		d.channel 	= mock(Channel.class);
		when( d.channel.isOk() ).thenReturn(true);

		d.tagOutput        = new TagInt("", 0);
		d.tagSost          = new TagInt("", 0);
		d.tagCnt           = new TagInt("", 0);
		d.tagValue         = new TagInt("", 0);
		d.tagLow           = new TagInt("", 0);
		d.tagHigh          = new TagInt("", 0);
		d.tagBlok          = new TagInt("", 0);
		d.tagDlyDrebezg    = new TagInt("", 0);
		d.tagDlyOutputOff = new TagInt("", 0);
		d.tagDlyOutputOn = new TagInt("", 0);
		d.channel.tagAddrNum  = new TagInt("", 0);
		d.channel.tagValue = new TagInt("", -1);
		d.tagFlags         = new TagInt("", 0);
		d.tagDebugValue   = new TagInt("", 0);
		d.tagPointV        = new TagInt("", 0);
		d.tagPointP        = new TagInt("", 0);
		d.tagPointV1       = new TagInt("", 0);
		d.tagPointP1       = new TagInt("", 0);
		d.tagPrimeval      = new TagInt("", 0);
		d.tagAlarm         = new TagInt("", 0);
		d.tagTrigRes       = new TagInt("", 0);
		d.inpTrigRes.tag   = new TagInt("", 0);
		d.inpMchaSost.tag  = new TagInt("", 0);

		d.resetState();
	}
	
	private void exec(
			int output,       
			int sost,         
			int cnt,          
			int value,        
			int channel,
			int primeval,     
			int alarm        
			) {
		
		d.execute();
		assertEquals("output",      output,      d.tagOutput.getInt());     
		assertEquals("sost",        sost,        d.tagSost.getInt());       
		assertEquals("cnt",         cnt,         d.tagCnt.getInt());        
		assertEquals("value",       value,       d.tagValue.getInt());      
		assertEquals("channel",     channel,     d.channel.tagValue.getInt());    
		assertEquals("primeval",    primeval,    d.tagPrimeval.getInt());   
		assertEquals("alarm",       alarm,       d.tagAlarm.getInt());       
	}



	@Test
	public void testLinkChannelFalse() {
		when( d.channel.isOk() ).thenReturn( false );
		exec( 0,0xFE, 0, 0,-1,0, 0 );
	}

	@Test
	public void testFlags_0() {
		d.channel.tagValue.setInt(0);
		exec( 0,0, 0, 0,0,0, 0 );
		
		d.channel.tagValue.setInt(1);
		exec( 1,3, 0, 1,1,1, 1 );

		d.channel.tagValue.setInt(0);
		exec( 0,0, 0, 0,0,0, 0 );
	}
	
	@Test
	public void testFlags_0_LowHigh() {
		d.channel.tagValue.setInt(0);
		d.tagLow.setInt(1);
		d.tagHigh.setInt(1);
		exec( 1,3, 0, 0,0,0, 1 );
		
		d.channel.tagValue.setInt(1);
		exec( 0,0, 0, 1,1,1, 0 );

		d.tagLow.setInt(2);
		d.tagHigh.setInt(3);
		exec( 1,3, 0, 1,1,1, 1 );

		d.channel.tagValue.setInt(2);
		exec( 0,0, 0, 2,2,2, 0 );

		d.channel.tagValue.setInt(3);
		exec( 0,0, 0, 3,3,3, 0 );
	}
	
	
	@Test
	public void testFlags_0_Blok() {
		d.tagBlok.setInt(1);

		d.channel.tagValue.setInt(0);
		exec( 0,0, 0, 0,0,0, 0 );

		d.channel.tagValue.setInt(1);
		exec( 0,4, 0, 1,1,1, 0 );

		d.tagBlok.setInt(0);
		exec( 1,3, 0, 1,1,1, 1 );
		exec( 1,3, 0, 1,1,1, 1 );
		exec( 1,3, 0, 1,1,1, 1 );

		d.tagBlok.setInt(1);
		exec( 0,4, 0, 1,1,1, 0 );
	}
	
	@Test
	public void testFlags_0_Blok1() {
		d.tagBlok         .setInt(1);
		d.tagLow          .setInt(1);
		d.tagHigh         .setInt(100);
		d.tagBlok         .setInt(0);
		d.tagDlyDrebezg   .setInt(1);
		d.tagDlyOutputOff.setInt(0);
		d.tagDlyOutputOn.setInt(1);
		d.tagFlags        .setInt(3);
		d.tagDebugValue  .setInt(65535);
		d.inpMchaSost.tag.setInt(1);
		d.channel.tagValue.setInt(0);

		exec( 0,0, 1, 0,0,0, 0 );
		exec( 0,1, 1, 0,0,0, 1 );
		exec( 1,3, 0, 0,0,0, 1 );
		
		d.inpMchaSost.tag.setInt(5);

		// TODO 
		d.tagBlok.setInt(1);
		exec( 1,4, 0, 0,0,0, 0 );
	}
	
	
	@Test
	public void testFlags_0_DlyDrebezg_1() {
		d.tagDlyDrebezg.setInt(1);
		d.channel.tagValue.setInt(1);
		exec( 0,0, 1, 1,1,1, 0 );
		exec( 1,3, 0, 1,1,1, 1 );
		exec( 1,3, 0, 1,1,1, 1 );
	}

	@Test
	public void testFlags_0_DlyDrebezg_2() {
		d.tagDlyDrebezg.setInt(2);
		d.channel.tagValue.setInt(1);
		exec( 0,0, 1, 1,1,1, 0 );
		exec( 0,0, 2, 1,1,1, 0 );
		exec( 1,3, 0, 1,1,1, 1 );
	}

	@Test
	public void testFlags_0_DlyDrebezg_3() {
		d.tagDlyDrebezg.setInt(3);
		d.channel.tagValue.setInt(1);
		exec( 0,0, 1, 1,1,1, 0 );
		exec( 0,0, 2, 1,1,1, 0 );
		exec( 0,0, 3, 1,1,1, 0 );
		exec( 1,3, 0, 1,1,1, 1 );
	}

	@Test
	public void testFlags_35_DlyDrebezg_5() {
		d.tagFlags.setInt(0x23);
		d.tagDlyOutputOn.setInt(5);
		d.tagDlyDrebezg.setInt(5);
		d.channel.tagValue.setInt(1);
		d.inpMchaSost.tag.setInt(0); // mchb off
		exec( 0,0, 1, 1,1,1, 0 );
		exec( 0,0, 2, 1,1,1, 0 );
		exec( 0,0, 3, 1,1,1, 0 );
		exec( 0,0, 4, 1,1,1, 0 );
		exec( 0,0, 5, 1,1,1, 0 );
		exec( 0,1, 0, 1,1,1, 0 );
		exec( 0,1, 0, 1,1,1, 0 );
		d.inpMchaSost.tag.setInt(2); // starting
		exec( 0,1, 0, 1,1,1, 0 );
		exec( 0,1, 0, 1,1,1, 0 );
		exec( 0,1, 0, 1,1,1, 0 );
		exec( 0,1, 0, 1,1,1, 0 );
		exec( 0,1, 0, 1,1,1, 0 );
		d.inpMchaSost.tag.setInt(1); // running
		exec( 0,1, 1, 1,1,1, 1 );
		exec( 0,1, 2, 1,1,1, 1 );
		exec( 0,1, 3, 1,1,1, 1 );
		exec( 0,1, 4, 1,1,1, 1 );
		exec( 0,1, 5, 1,1,1, 1 );
		exec( 1,3, 0, 1,1,1, 1 );

		d.tagFlags.setInt(0x13);
		d.tagDlyOutputOn.setInt(5);
		d.tagDlyDrebezg.setInt(5);
		d.channel.tagValue.setInt(1);
		d.inpMchaSost.tag.setInt(0); // mchb off
		exec( 0,1, 0, 1,1,1, 0 );
		exec( 0,1, 0, 1,1,1, 0 );
		d.inpMchaSost.tag.setInt(2); // starting
		exec( 0,1, 0, 1,1,1, 0 );
		exec( 0,1, 0, 1,1,1, 0 );
		exec( 0,1, 0, 1,1,1, 0 );
		exec( 0,1, 0, 1,1,1, 0 );
		exec( 0,1, 0, 1,1,1, 0 );
		d.inpMchaSost.tag.setInt(1); // running
		exec( 0,1, 1, 1,1,1, 1 );
		exec( 0,1, 2, 1,1,1, 1 );
		exec( 0,1, 3, 1,1,1, 1 );
		exec( 0,1, 4, 1,1,1, 1 );
		exec( 0,1, 5, 1,1,1, 1 );
		exec( 1,3, 0, 1,1,1, 1 );

		d.tagFlags.setInt(0x33);
		d.tagDlyOutputOn.setInt(5);
		d.tagDlyDrebezg.setInt(5);
		d.channel.tagValue.setInt(1);
		d.inpMchaSost.tag.setInt(0); // mchb off
		exec( 0,1, 0, 1,1,1, 0 );
		exec( 0,1, 0, 1,1,1, 0 );
		d.inpMchaSost.tag.setInt(2); // starting
		exec( 0,1, 0, 1,1,1, 0 );
		exec( 0,1, 0, 1,1,1, 0 );
		exec( 0,1, 0, 1,1,1, 0 );
		exec( 0,1, 0, 1,1,1, 0 );
		exec( 0,1, 0, 1,1,1, 0 );
		d.inpMchaSost.tag.setInt(1); // running
		exec( 0,1, 1, 1,1,1, 1 );
		exec( 0,1, 2, 1,1,1, 1 );
		exec( 0,1, 3, 1,1,1, 1 );
		exec( 0,1, 4, 1,1,1, 1 );
		exec( 0,1, 5, 1,1,1, 1 );
		exec( 1,3, 0, 1,1,1, 1 );

		d.tagFlags.setInt(0x03);
		d.tagDlyOutputOn.setInt(5);
		d.tagDlyDrebezg.setInt(5);
		d.channel.tagValue.setInt(1);
		d.inpMchaSost.tag.setInt(0); // mchb off
		exec( 0,1, 0, 1,1,1, 0 );
		exec( 0,1, 0, 1,1,1, 0 );
		d.inpMchaSost.tag.setInt(2); // starting
		exec( 0,1, 0, 1,1,1, 0 );
		exec( 0,1, 0, 1,1,1, 0 );
		exec( 0,1, 0, 1,1,1, 0 );
		exec( 0,1, 0, 1,1,1, 0 );
		exec( 0,1, 0, 1,1,1, 0 );
		d.inpMchaSost.tag.setInt(1); // running
		exec( 0,1, 1, 1,1,1, 1 );
		exec( 0,1, 2, 1,1,1, 1 );
		exec( 0,1, 3, 1,1,1, 1 );
		exec( 0,1, 4, 1,1,1, 1 );
		exec( 0,1, 5, 1,1,1, 1 );
		exec( 1,3, 0, 1,1,1, 1 );

	}

    @Test
    public void testFlags_35_DlyDrebezg_dr1() {
        d.tagFlags.setInt(0x23);
        d.tagDlyOutputOn.setInt(5);
        d.tagDlyDrebezg.setInt(5);
        d.inpMchaSost.tag.setInt(1); // mchb running

        d.channel.tagValue.setInt(0); // value OK
        exec(0, 0, 0, 0, 0, 0, 0);
        exec(0, 0, 0, 0, 0, 0, 0);
        exec(0, 0, 0, 0, 0, 0, 0);

        d.channel.tagValue.setInt(1); // value BAD
        exec(0, 0, 1, 1, 1, 1, 0);
        exec(0, 0, 2, 1, 1, 1, 0);
        exec(0, 0, 3, 1, 1, 1, 0);
        exec(0, 0, 4, 1, 1, 1, 0);
        exec(0, 0, 5, 1, 1, 1, 0);
        exec(0, 1, 1, 1, 1, 1, 1); // alarm!!!
        exec(0, 1, 2, 1, 1, 1, 1);
        exec(0, 1, 3, 1, 1, 1, 1);
        exec(0, 1, 4, 1, 1, 1, 1);
        exec(0, 1, 5, 1, 1, 1, 1);
        exec(1, 3, 0, 1, 1, 1, 1);
    }

    @Test
    public void testFlags_35_DlyDrebezg_dr2() {
        d.tagFlags.setInt(0x23);
        d.tagDlyOutputOn.setInt(5);
        d.tagDlyDrebezg.setInt(5);
        d.inpMchaSost.tag.setInt(1); // mchb running

        d.channel.tagValue.setInt(0); // value OK
        exec(0, 0, 0, 0, 0, 0, 0);
        exec(0, 0, 0, 0, 0, 0, 0);
        exec(0, 0, 0, 0, 0, 0, 0);

        d.channel.tagValue.setInt(1); // value BAD
        exec(0, 0, 1, 1, 1, 1, 0);
        exec(0, 0, 2, 1, 1, 1, 0);
        d.channel.tagValue.setInt(0); // value OK
        exec(0, 0, 0, 0, 0, 0, 0);
        d.channel.tagValue.setInt(1); // value BAD
        exec(0, 0, 1, 1, 1, 1, 0);
        exec(0, 0, 2, 1, 1, 1, 0);
        exec(0, 0, 3, 1, 1, 1, 0);
        exec(0, 0, 4, 1, 1, 1, 0);
        exec(0, 0, 5, 1, 1, 1, 0);
        exec(0, 1, 1, 1, 1, 1, 1); // alarm!!!
        exec(0, 1, 2, 1, 1, 1, 1);
        exec(0, 1, 3, 1, 1, 1, 1);
        exec(0, 1, 4, 1, 1, 1, 1);
        exec(0, 1, 5, 1, 1, 1, 1);
        exec(1, 3, 0, 1, 1, 1, 1);
    }

    @Test
	public void testFlags_0_DlyOutput_1() {
		d.tagDlyOutputOn.setInt(1);
		d.channel.tagValue.setInt(1);
		exec( 0,1, 1, 1,1,1, 1 );
		exec( 1,3, 0, 1,1,1, 1 );
		exec( 1,3, 0, 1,1,1, 1 );
	}

	@Test
	public void testFlags_0_DlyOutput_2() {
		d.tagDlyOutputOn.setInt(2);
		d.channel.tagValue.setInt(1);
		exec( 0,1, 1, 1,1,1, 1 );
		exec( 0,1, 2, 1,1,1, 1 );
		exec( 1,3, 0, 1,1,1, 1 );
	}

	@Test
	public void testFlags_0_DlyStart_1() {
		d.tagDlyOutputOff.setInt(2);
		d.channel.tagValue.setInt(1);
		exec(1, 3, 0, 1, 1, 1, 1);
		d.channel.tagValue.setInt(0);
		exec(1, 2, 0, 0, 0, 0, 0);
		exec(1, 2, 0, 0, 0, 0, 0);
		exec(0, 0, 0, 0, 0, 0, 0);
	}

	@Test
	public void testFlags_0_DlyStart_2() {
		d.tagDlyOutputOff.setInt(2);
		d.channel.tagValue.setInt(1);
		exec( 1,3, 0, 1,1,1, 1 );
		d.channel.tagValue.setInt(0);
		exec( 1,2, 0, 0,0,0, 0 );
		d.channel.tagValue.setInt(1);
		exec( 1,3, 0, 1,1,1, 1 );
		d.channel.tagValue.setInt(0);
		exec( 1,2, 0, 0,0,0, 0 );
		exec( 1,2, 0, 0,0,0, 0 );
		exec( 0,0, 0, 0,0,0, 0 );
	}

	@Test
	public void testFlags_0_DlyStart_DlyOutput() {
		d.tagDlyOutputOff.setInt(2);
		d.tagDlyOutputOn.setInt(2);
		d.channel.tagValue.setInt(1);
		exec( 0,1, 1, 1,1,1, 1 );
		exec( 0,1, 2, 1,1,1, 1 );
		exec( 1,3, 0, 1,1,1, 1 );
		d.channel.tagValue.setInt(0);
		exec( 1,2, 0, 0,0,0, 0 );
		exec( 1,2, 0, 0,0,0, 0 );
		exec( 0,0, 0, 0,0,0, 0 );

		d.channel.tagValue.setInt(1);
		exec( 0,1, 1, 1,1,1, 1 );
		d.channel.tagValue.setInt(0);
		exec( 0,0, 0, 0,0,0, 0 );

		d.channel.tagValue.setInt(1);
		exec( 0,1, 1, 1,1,1, 1 );
		exec( 0,1, 2, 1,1,1, 1 );
		exec( 1,3, 0, 1,1,1, 1 );
		d.channel.tagValue.setInt(0);
		exec( 1,2, 0, 0,0,0, 0 );
		d.channel.tagValue.setInt(1);
		exec( 1,3, 0, 1,1,1, 1 );
	}

	@Test
	public void testFlags_b0_1() {
		d.tagFlags.setInt(1);
		
		d.channel.tagValue.setInt(0);
		exec( 0,0, 0, 0,0,0, 0 ); 
		
		d.channel.tagValue.setInt(1);
		exec( 1,3, 0, 1,1,1, 1 );

		d.channel.tagValue.setInt(0);
		exec( 1,2, 0, 0,0,0, 0 );

		d.channel.tagValue.setInt(1);
		exec( 1,3, 0, 1,1,1, 1 );

		d.channel.tagValue.setInt(0);
		exec( 1,2, 0, 0,0,0, 0 );

		d.inpTrigRes.tag.setInt(1);
		exec( 0,0, 0, 0,0,0, 0 );

		d.inpTrigRes.tag.setInt(0);
		exec( 0,0, 0, 0,0,0, 0 );
	}

	@Test
	public void testFlags_b0_1_TrigRes_input() {
		d.tagFlags.setInt(1);

		d.channel.tagValue.setInt(1);
		exec( 1,3, 0, 1,1,1, 1 );

		d.inpTrigRes.tag.setInt(1);
		exec( 0,1, 0, 1,1,1, 1 );
		exec( 0,1, 0, 1,1,1, 1 );
		exec( 0,1, 0, 1,1,1, 1 );

		d.inpTrigRes.tag.setInt(0);
		d.channel.tagValue.setInt(0);
		exec( 0,0, 0, 0,0,0, 0 );
	}

	@Test
	public void testFlags_b0_1_TrigRes_output() {
		d.tagFlags.setInt(1);

		d.channel.tagValue.setInt(1);
		exec( 1,3, 0, 1,1,1, 1 );

		d.tagTrigRes.setInt(1);
		exec( 0,1, 0, 1,1,1, 1 );
		exec( 1,3, 0, 1,1,1, 1 );
	}

	@Test
	public void testFlags_b2b1_1() {
		d.tagFlags.setInt(2);
		
		d.inpMchaSost.tag.setInt(0);
		d.channel.tagValue.setInt(1);
		exec( 0,1, 0, 1,1,1, 0 );
		
		d.inpMchaSost.tag.setInt(2);
		exec( 0,1, 0, 1,1,1, 0 );

		d.inpMchaSost.tag.setInt(1);
		exec( 1,3, 0, 1,1,1, 1 );
	}


	@Test
	public void testFlags_3() {
		d.tagFlags.setInt(3);

		d.channel.tagValue.setInt(1);
		d.inpMchaSost.tag.setInt(1);
		exec( 1,3, 0, 1,1,1, 1 );

		d.inpMchaSost.tag.setInt(5);
		exec( 1,3, 0, 1,1,1, 1 );
		exec( 1,3, 0, 1,1,1, 1 );

		d.channel.tagValue.setInt(0);
		exec( 1,2, 0, 0,0,0, 0 );
	}


	@Test
	public void testFlags_0x203() {
		d.tagFlags.setInt(0x203);

		d.channel.tagValue.setInt(1);
		d.inpMchaSost.tag.setInt(1);
		exec( 1,3, 0, 1,1,1, 1 );

		d.inpMchaSost.tag.setInt(5);
		exec( 1,3, 0, 1,1,1, 1 );
		exec( 1,3, 0, 1,1,1, 1 );

		d.channel.tagValue.setInt(0);
		exec( 1,2, 0, 0,0,0, 1 );
	}


	@Test
	public void testFlags_b2b1_2() {
		d.tagFlags.setInt(4);
		
		d.inpMchaSost.tag.setInt(0);
		d.channel.tagValue.setInt(1);
		exec( 0,1, 0, 1,1,1, 0 );
		
		d.inpMchaSost.tag.setInt(2);
		exec( 1,3, 0, 1,1,1, 1 );

		d.inpMchaSost.tag.setInt(1);
		exec( 1,3, 0, 1,1,1, 1 );
	}
	
	@Test
	public void testFlags_b3b0_0x0d() {
		d.tagFlags.setInt(0x0d);
		d.tagDlyDrebezg.setInt(3);
		d.tagDlyOutputOn.setInt(3);
		
		d.channel.tagValue.setInt(0);
		exec( 0,0, 0, 0,0,0, 0 );

		d.channel.tagValue.setInt(1);
		exec( 0,0, 1, 1,1,1, 0 );
		exec( 0,0, 2, 1,1,1, 0 );
		exec( 0,0, 3, 1,1,1, 0 );
		exec( 0,1, 0, 1,1,1, 0 );
		
		d.inpMchaSost.tag.setInt(2);
		exec( 1,3, 0, 1,1,1, 1 );
	}
	
	@Test
	public void testFlags_b5b4_1_DlyDrebezg_1() {
		d.tagFlags.setInt(0x10);
		d.tagDlyDrebezg.setInt(1);
		
		d.channel.tagValue.setInt(1);
		exec( 1,3, 0, 1,1,1, 1 );

		d.channel.tagValue.setInt(0);
		exec( 1,3, 1, 0,0,0, 1 );
		exec( 0,0, 0, 0,0,0, 0 );
	}
	
	@Test
	public void testFlags_b5b4_1_DlyDrebezg_2() {
		d.tagFlags.setInt(0x20);
		d.tagDlyDrebezg.setInt(1);
		
		d.channel.tagValue.setInt(1);
		exec( 0,0, 1, 1,1,1, 0 );
		exec( 1,3, 0, 1,1,1, 1 );

		d.channel.tagValue.setInt(0);
		exec( 1,3, 1, 0,0,0, 1 );
		exec( 0,0, 0, 0,0,0, 0 );
	}
	
	@Test
	public void testFlags_b5b4_1_DlyDrebezg_3() {
		d.tagFlags.setInt(0x30);
		d.tagDlyDrebezg.setInt(1);
		
		d.channel.tagValue.setInt(1);
		exec( 1,3, 0, 1,1,1, 1 );

		d.channel.tagValue.setInt(0);
		exec( 0,0, 0, 0,0,0, 0 );
	}
	
	@Test
	public void testFlags_b6_1() {
		d.tagFlags.setInt(0x40);
		
		d.channel.tagValue.setInt(1);
		exec( 0,1, 0, 1,1,1, 1 );
	}
	
	@Test
	public void testFlags_b7_1() {
		d.tagFlags.setInt(0x80);
		
		d.channel.tagValue.setInt(1);
		exec( 1,3, 0, 1,1,1, 0 );
	}
	
	@Test
	public void testFlags_b8_1() {
		d.tagFlags.setInt(0x104);
		
		d.channel.tagValue.setInt(0);
		exec( 0,0, 0, 0,0,0, 0 );

		d.inpMchaSost.tag.setInt(2);
		exec( 0,0, 0, 0,0,0, 1 );

		d.channel.tagValue.setInt(1);
		exec( 1,3, 0, 1,1,1, 0 );

		d.channel.tagValue.setInt(0);
		exec( 0,0, 0, 0,0,0, 1 );
	}
	
	@Test
	public void testFlags_NoTransform() {
		d.tagDebugValue.setInt(7);
		
		exec( 1,3, 0, 7,-1,-1, 1 );
	}

	@Test
	public void testFlags_Scaling1() {
		d.tagPointP.setInt(100);
		d.tagPointV.setInt(200);
		
		d.channel.tagValue.setInt(0);
		exec( 0,0, 0, 0,0,0, 0 );

		d.channel.tagValue.setInt(10);
		exec( 1,3, 0, 20,10,10, 1 );

		d.channel.tagValue.setInt(100);
		exec( 1,3, 0, 200,100,100, 1 );

		d.channel.tagValue.setInt(-100);
		exec( 1,3, 0, -200,-100,-100, 1 );
	}

	@Test
	public void testFlags_Scaling2() {
		d.tagPointP.setInt(100);
		d.tagPointV.setInt(0);
		d.tagPointP1.setInt(200);
		d.tagPointV1.setInt(100);
		
		d.channel.tagValue.setInt(0);
		exec( 1,3, 0, -100,0,0, 1 );

		d.channel.tagValue.setInt(100);
		exec( 0,0, 0, 0,100,100, 0 );

		d.channel.tagValue.setInt(150);
		exec( 1,3, 0, 50,150,150, 1 );

		d.channel.tagValue.setInt(200);
		exec( 1,3, 0, 100,200,200, 1 );

		d.channel.tagValue.setInt(-100);
		exec( 1,3, 0, -200,-100,-100, 1 );
	}

	
	@Test
	public void testLoad() throws Exception {
		
		RoboplantModule robo = AuxProcs.createRoboplantModule();
		assertTrue( robo.loadProject( Paths.get( 
				InitUtils.getResourcesDir("roboplant", RoboplantModuleTest.class) + "mdta.xml" )));

		assertEquals(1, robo.devicesByAddr.length);
		DeviceMDTA d = (DeviceMDTA)robo.devicesByAddr[0];
		
		assertEquals( "MDTA", d.devtype );
		assertEquals( "602_RKS", d.tagname );
		
		assertEquals(2, d.inputs.size());
		assertEquals(19, d.outputs.size());
		
		assertTrue( d.init(robo) );
		assertTrue( d.prepare() );
	}
	
	
	
}
