package promauto.jroboplc.plugin.roboplant;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static promauto.jroboplc.plugin.roboplant.AuxProcs.newInputAndTag;

import java.nio.file.Paths;

import org.junit.Before;
import org.junit.Test;

import promauto.jroboplc.core.api.InitUtils;
import promauto.jroboplc.core.tags.TagInt;

public class DeviceLOADATest {
	private DeviceLOADA d;
	
	@Before
	public void setUp() throws Exception {
		InitUtils.setupLogger();

		d = new DeviceLOADA();
		d.inpEnable 	= newInputAndTag();
		
		d.tagCurrent	= new TagInt("", 0);
		d.tagNoOutput	= new TagInt("", 0);
		d.tagChanging	= new TagInt("", 0);
		d.tagDlyChange	= new TagInt("", 0);
		
		d.loadANum = 16;
		
		for (int i = 0; i<d.loadANum; i++) {
			d.inpInput.add( newInputAndTag() );
			d.tagOutput.add( new TagInt("", 0) );
			d.tagOrder.add( new TagInt("", 0) );
		}
    	d.order = new int[ d.loadANum ];
    	d.fl = new int[ d.loadANum ];
    	d.idx = new int[ d.loadANum ];
		
		d.resetState();
	}

	@Test
	public void testLoad() throws Exception {
		RoboplantModule robo = AuxProcs.createRoboplantModule();
		assertTrue( robo.loadProject( Paths.get( 
				InitUtils.getResourcesDir("roboplant", RoboplantModuleTest.class) + "loada.xml" )));

		assertEquals(1, robo.devicesByAddr.length);
		d = (DeviceLOADA)robo.devicesByAddr[0];
		assertTrue( d.init(robo) );
		assertTrue( d.prepare() );

		assertEquals( "LOADA", d.devtype );
		assertEquals( "SZ_607", d.name );
		assertEquals( "SZ_607", d.tagname );
	}
	
	private void set(int... inputs) {
		for (int i=0; i<inputs.length; i++)
			d.inpInput.get(i).tag.setInt(inputs[i]);
	}

	
	private void exec(
			int current	 ,
			int noOutput ,
			int changing ,
			int... outputs 
			) {
		d.execute();
		assertEquals("current"		, current	 , d.tagCurrent	 .getInt());     
		assertEquals("noOutput"		, noOutput 	 , d.tagNoOutput .getInt());     
		assertEquals("changing" 	, changing 	 , d.tagChanging .getInt());     

		for (int i=0; i<outputs.length; i++)
			assertEquals("output_" + i, outputs[i], d.tagOutput.get(i).getInt());     
	}

	private void order(int... orders) {
		d.rebuildOrder();
		for (int i=0; i<orders.length; i++)
			assertEquals("order_" + i, orders[i], d.tagOrder.get(i).getInt());     
	}


	@Test
	public void testRebuildOrder() {
		order(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
		
		d.tagOrder.get(0).setInt(1);
		order(1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);

		d.tagOrder.get(1).setInt(1);
		order(2,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0);

		d.tagOrder.get(1).setInt(0);
		order(1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);

		d.tagOrder.get(0).setInt(0);
		order(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);

		d.tagOrder.get(0).setInt(1);
		d.tagOrder.get(1).setInt(1);
		order(1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
		
		d.tagOrder.get(15).setInt(1);
		order(2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1);

		d.tagOrder.get(0).setInt(1);
		order(1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2);

		d.tagOrder.get(1).setInt(3);
		order(1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,2);

		d.tagOrder.get(0).setInt(3);
		order(3,2,0,0,0,0,0,0,0,0,0,0,0,0,0,1);
		
		d.tagOrder.get(0).setInt(0);
		d.tagOrder.get(1).setInt(0);
		d.tagOrder.get(15).setInt(0);
		order(0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,1);

		d.tagOrder.get(0).setInt(5);
		order(5,2,0,0,0,0,0,0,0,0,0,0,0,0,0,1);

		d.tagOrder.get(15).setInt(0);
		order(4,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0);

		d.tagOrder.get(15).setInt(1);
		order(5,2,0,0,0,0,0,0,0,0,0,0,0,0,0,1);

		d.tagOrder.get(0).setInt(6);
		order(6,2,0,0,0,0,0,0,0,0,0,0,0,0,0,1);

		d.tagOrder.get(0).setInt(100);
		order(100,2,0,0,0,0,0,0,0,0,0,0,0,0,0,1);
	}

	@Test
	public void testExecute() {
		d.inpEnable.tag.setInt(0);
		set(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);	exec(0,0,0,  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);

		d.inpEnable.tag.setInt(1);
		set(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);	exec(0,1,0,  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);

		d.inpEnable.tag.setInt(0);
		d.tagOrder.get(2).setInt(1);
		order(0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0);

		d.tagOrder.get(0).setInt(1);
		order(1,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0);

		d.tagOrder.get(1).setInt(1);
		order(2,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0);

		set(0,0,0,0,0);	exec(1,0,0,  0,0,0,0,0);

		d.inpEnable.tag.setInt(1);
		set(0,0,0,0,0);	exec(1,0,0,  0,1,0,0,0);
		set(0,1,0,0,0);	exec(0,0,0,  1,0,0,0,0);
		set(0,0,0,0,0);	exec(0,0,0,  1,0,0,0,0);
		set(1,0,0,0,0);	exec(2,0,0,  0,0,1,0,0);
		set(0,0,1,0,0);	exec(1,0,0,  0,1,0,0,0);
		set(0,1,0,0,0);	exec(0,0,0,  1,0,0,0,0);
		set(1,1,0,0,0);	exec(2,0,0,  0,0,1,0,0);
		set(1,1,1,0,0);	exec(2,1,0,  0,0,1,0,0);

		set(1,1,0,0,0);	exec(2,0,0,  0,0,1,0,0);
		set(1,1,1,0,0);	exec(2,1,0,  0,0,1,0,0);
		set(0,1,1,0,0);	exec(0,0,0,  1,0,0,0,0);
		set(1,1,0,0,0);	exec(2,0,0,  0,0,1,0,0);
		set(0,1,0,0,0);	exec(2,0,0,  0,0,1,0,0);

		d.inpEnable.tag.setInt(0);
		set(0,1,0,0,0);	exec(1,0,0,  0,0,0,0,0);
		
		d.inpEnable.tag.setInt(1);
		set(0,1,0,0,0);	exec(0,0,0,  1,0,0,0,0);
		
		d.tagDlyChange.setInt(3);
		set(0,1,0,0,0);	exec(0,0,0,  1,0,0,0,0);
		set(1,1,0,0,0);	exec(0,0,1,  1,0,0,0,0);
		set(1,1,0,0,0);	exec(0,0,1,  1,0,0,0,0);
		set(1,1,0,0,0);	exec(0,0,1,  1,0,0,0,0);
		set(1,1,0,0,0);	exec(2,0,0,  0,0,1,0,0);

		set(1,1,1,0,0);	exec(2,1,0,  0,0,1,0,0);	// no changing delay!

		set(0,1,1,0,0);	exec(0,0,0,  1,0,0,0,0);
	}

	
	@Test
	public void testExecute_current() {
		d.inpEnable.tag.setInt(1);
		set(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);	

		d.tagOrder.get(2).setInt(1);
		order(0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0);	
		
		exec(2,0,0,  0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0);

	}



	@Test
	public void testExecute_all_orders_zero() {
		d.inpEnable.tag.setInt(1);

		d.tagOrder.get(2).setInt(1);
		order(0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0);
		exec(2,0,0,  0,0,1,0,0);

		d.tagOrder.get(2).setInt(0);
		order(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
		exec(0,1,0,  0,0,0,0,0);

	}


}
