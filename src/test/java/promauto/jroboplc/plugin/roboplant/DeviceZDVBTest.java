package promauto.jroboplc.plugin.roboplant;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import promauto.jroboplc.core.api.InitUtils;
import promauto.jroboplc.core.tags.TagInt;

public class DeviceZDVBTest {

	private DeviceZDVB d;

	@Mock RoboplantModule module;


	@Before
	public void setUp() throws Exception {
		InitUtils.setupLogger();
		
		d = new DeviceZDVB();
		d.module = module;

		d.inpEnable     = new Input();
		d.inpDoOpen     = new Input();
		d.inpImp        = new Input();
		d.inpIsFullOpen = new Input();
		d.inpCurVal     = new Input();
		d.inpInProc     = new Input();

		d.chnl1in 	= new Channel();
		d.chnl2in 	= new Channel();
		d.chnl1out 	= new Channel();
		d.chnl2out 	= new Channel();
		
		d.chnl1in .state = Channel.State.Ok;
		d.chnl2in .state = Channel.State.Ok;
		d.chnl1out.state = Channel.State.Ok;
		d.chnl2out.state = Channel.State.Ok;

		d.tagOutput   			= new TagInt("", 0);
		d.tagOutput             = new TagInt("", 0);
		d.tagSost               = new TagInt("", 0);
		d.tagOpened             = new TagInt("", 0);
		d.tagClosed             = new TagInt("", 0);
		d.tagAlarm              = new TagInt("", 0);
		d.tagControl            = new TagInt("", 0);
		d.tagBlok               = new TagInt("", 0);
		d.tagFlags              = new TagInt("", 0x70);
		d.tagTimeOpen           = new TagInt("", 0);
		d.tagMaxCloseTime       = new TagInt("", 0);
		d.tagMaxOpenTime        = new TagInt("", 0);
		d.tagTime100Proc        = new TagInt("", 0);
		d.tagProc               = new TagInt("", 0);
		d.tagRealProc           = new TagInt("", 0);
		d.tagCloseVal           = new TagInt("", 0);
		d.tagOpenVal            = new TagInt("", 100);
		d.tagValue              = new TagInt("", 0);
		
		d.chnl1out.tagAddrNum  	= new TagInt("", 0);
		d.chnl1out.tagValue	  	= new TagInt("", -1);
		d.chnl2out.tagAddrNum	= new TagInt("", 0);
		d.chnl2out.tagValue	  	= new TagInt("", -1);
		d.chnl1in.tagAddrNum	= new TagInt("", 0);
		d.chnl1in.tagValue	  	= new TagInt("", -1);
		d.chnl2in.tagAddrNum	= new TagInt("", 0);
		d.chnl2in.tagValue	  	= new TagInt("", -1);
		
	    
		d.inpEnable.tag   		= new TagInt("", 0);
		d.inpDoOpen.tag   		= new TagInt("", 0);
		d.inpImp.tag   			= new TagInt("", 0);
		d.inpIsFullOpen.tag 	= new TagInt("", 0);
		d.inpCurVal.tag   		= new TagInt("", 0);
		d.inpInProc.tag   		= new TagInt("", 0);
		
		
		d.resetState();
	}
	
	private void exec(
			int output	,       
			int sost	,         
			int alarm	,        
			int closed	,
			int opened	,          
			int proc	,
			int realproc,
			int value   ,
			int chnl1out,
			int chnl2out,     
			int chnl1in	,
			int chnl2in	     
			) {
		
		d.execute();
		assertEquals("output"	, output	, d.tagOutput.getInt());     
		assertEquals("sost"		, sost		, d.tagSost.getInt());     
		assertEquals("alarm"   	, alarm   	, d.tagAlarm.getInt());
		assertEquals("closed"	, closed	, d.tagClosed.getInt());     
		assertEquals("opened"	, opened	, d.tagOpened.getInt());     
		assertEquals("proc"   	, proc   	, d.tagProc.getInt());
		assertEquals("realproc"	, realproc 	, d.tagRealProc.getInt());
		assertEquals("value"   	, value   	, d.tagValue.getInt());
		assertEquals("chnl1out"	, chnl1out	, d.chnl1out.tagValue.getInt());     
		assertEquals("chnl2out"	, chnl2out	, d.chnl2out.tagValue.getInt());     
		assertEquals("chnl1in"	, chnl1in	, d.chnl1in.tagValue.getInt());     
		assertEquals("chnl2in"	, chnl2in	, d.chnl2in.tagValue.getInt());     
		
	}

	private void setClosedOpened(int cl, int op) {
		d.chnl1in.tagValue.setInt(cl);
		d.chnl2in.tagValue.setInt(op);
	}

	
	@Test
	public void testLinkChannel_False() {
		d.chnl1in.state = Channel.State.Error;
		assertTrue(d.execute());
		assertEquals(88, d.tagSost.getInt());
	}


	@Test
	public void testFlags_0() {
		setClosedOpened(0, 0);
		assertEquals(30, d.tagSost.getInt());
		
		d.tagFlags.setInt(0);
		exec( 0,0,0, 1,1, 0,0,0,  1,1,0,0 );
		
		d.tagFlags.setInt(0x20);
		exec( 0,0,0, 0,0, 0,0,0,  1,1,0,0 );
		
		d.tagFlags.setInt(0x30);
		exec( 0,0,0, 0,0, 0,0,0,  0,0,0,0 );
		
		d.tagFlags.setInt(0x70);
		d.tagOpenVal.setInt(0);
		exec( 0,88,1, 0,0, 0,0,0,  0,0,0,0 );

	}


	@Test
	public void test_close_fullopen() {
		
		d.tagFlags.setInt(0x70);
		
		setClosedOpened(0, 0);
		exec( 0,0,0, 0,0, 0,0,0,  0,0,0,0 );

		setClosedOpened(1, 0);
		exec( 0,0,0, 1,0, 0,0,0,  0,0,1,0 );
		
		d.inpEnable.tag.setInt(1);
		exec( 1,17,0, 1,0, 0,0,0,  0,0,1,0 );

		d.inpDoOpen.tag.setInt(1);
		exec( 1,16,0, 1,0, 0,0,0,  0,0,1,0 );

		d.inpIsFullOpen.tag.setInt(1);
		exec( 0,12,0, 1,0, 0,0,0,  0,1,1,0 );

		setClosedOpened(0, 0);
		exec( 0,12,0, 0,0, 0,0,0,  0,1,0,0 );

		setClosedOpened(0, 1);
		exec( 1,14,0, 0,1, 0,0,0,  0,0,0,1 );

		d.inpIsFullOpen.tag.setInt(0);
		exec( 1,16,0, 0,1, 0,0,0,  0,0,0,1 );

		d.inpDoOpen.tag.setInt(0);
		exec( 0,15,0, 0,1, 0,0,0,  1,0,0,1 );
		
		setClosedOpened(0, 0);
		exec( 0,15,0, 0,0, 0,0,0,  1,0,0,0 );
		
		setClosedOpened(1, 0);
		exec( 1,17,0, 1,0, 0,0,0,  0,0,1,0 );
		
		setClosedOpened(0, 0);
		exec( 0,15,0, 0,0, 0,0,0,  1,0,0,0 );
	}

	
	@Test
	public void test_enable() {
		
		d.inpEnable.tag.setInt(1);
		setClosedOpened(0, 0);
		exec( 0,0,0, 0,0, 0,0,0,  0,0,0,0 );
		exec( 0,15,0, 0,0, 0,0,0,  1,0,0,0 );
		
		d.inpEnable.tag.setInt(0);
		exec( 0,0,0, 0,0, 0,0,0,  0,0,0,0 );

		d.inpEnable.tag.setInt(1);
		exec( 0,15,0, 0,0, 0,0,0,  1,0,0,0 );
	}


	@Test
	public void test_control_1() {
		
		d.inpEnable.tag.setInt(1);
		setClosedOpened(0, 0);
		d.execute();
		
		d.tagControl.setInt(1);
		exec( 1,25,0, 0,0, 0,0,0,  1,0,0,0 );
		
		d.inpDoOpen.tag.setInt(1);
		exec( 1,25,0, 0,0, 0,0,0,  1,0,0,0 );

		d.inpEnable.tag.setInt(0);
		exec( 1,25,0, 0,0, 0,0,0,  1,0,0,0 );
	}


	@Test
	public void test_control_2() {
		
		d.inpEnable.tag.setInt(1);
		setClosedOpened(0, 0);
		d.execute();
		
		d.tagControl.setInt(2);
		exec( 1,26,0, 0,0, 0,0,0,  0,0,0,0 );
		
		d.tagProc.setInt(30);
		exec( 1,21,0, 0,0, 30,0,0,  0,1,0,0 );
		
		d.inpEnable.tag.setInt(0);
		exec( 1,21,0, 0,0, 30,0,0,  0,1,0,0 );

		d.inpIsFullOpen.tag.setInt(1);
		exec( 1,21,0, 0,0, 30,0,0,  0,1,0,0 );
	}


	@Test
	public void test_control_3() {
		
		d.inpEnable.tag.setInt(1);
		setClosedOpened(0, 0);
		d.execute();
		
		d.tagControl.setInt(3);
		exec( 1,22,0, 0,0, 0,0,0,  0,1,0,0 );
		
		d.tagProc.setInt(30);
		exec( 1,22,0, 0,0, 30,0,0,  0,1,0,0 );
		
		d.inpEnable.tag.setInt(0);
		exec( 1,22,0, 0,0, 30,0,0,  0,1,0,0 );

	}


	
	@Test
	public void test_control_4() {
		
		d.inpEnable.tag.setInt(1);
		setClosedOpened(0, 0);
		exec( 0,0,0, 0,0, 0,0,0,  0,0,0,0 );
		exec( 0,15,0, 0,0, 0,0,0,  1,0,0,0 );
		
		d.tagControl.setInt(4);
		exec( 1,0,0, 0,0, 0,0,0,  0,0,0,0 );
		
		d.inpEnable.tag.setInt(0);
		exec( 1,0,0, 0,0, 0,0,0,  0,0,0,0 );
		
		d.inpEnable.tag.setInt(1);
		exec( 1,0,0, 0,0, 0,0,0,  0,0,0,0 );
	}
	
	
	
	
	
}