package promauto.jroboplc.plugin.roboplant;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.nio.file.Paths;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import promauto.jroboplc.core.api.InitUtils;
import promauto.jroboplc.core.tags.TagInt;

public class DeviceZDVSTest {

	private DeviceZDVS d;

	@Mock RoboplantModule module;
	

	@Before
	public void setUp() throws Exception {
		InitUtils.setupLogger();
		
		d = new DeviceZDVS();
		d.module = module;

		d.chnlClIn 		= new Channel();
		d.chnlClOut 	= new Channel();
		d.chnlOpIn 		= new Channel();
		d.chnlOpOut 	= new Channel();
		d.chnlMiIn 		= new Channel();
		d.chnlMiOut 	= new Channel();
		
		d.chnlClIn .state = Channel.State.Ok;
		d.chnlClOut.state = Channel.State.Ok;
		d.chnlOpIn .state = Channel.State.Ok;
		d.chnlOpOut.state = Channel.State.Ok;
		d.chnlMiIn .state = Channel.State.Ok;
		d.chnlMiOut.state = Channel.State.Ok;
		

		d.inpInput  = new Input();
		d.inpEnable = new Input();
		d.inpDoOpenFull = new Input();
		d.inpDoOpenMid = new Input();

		d.inpInput.tag   		= new TagInt("", 0);
		d.inpEnable.tag   		= new TagInt("", 0);
		d.inpDoOpenFull.tag   	= new TagInt("", 0);
		d.inpDoOpenMid.tag   	= new TagInt("", 0);

		d.tagOutput   			= new TagInt("", 0);
		d.tagSost     			= new TagInt("", 0);
		d.tagInOut   			= new TagInt("", 0);
		d.tagControl  			= new TagInt("", 0);
		d.tagBlok     			= new TagInt("", 0);
		d.tagFlags    			= new TagInt("", 0);
		
		d.chnlClIn.tagAddrNum	  	= new TagInt("", 0);
		d.chnlClIn.tagValue	  	= new TagInt("", -1);
		d.chnlClOut.tagAddrNum	  	= new TagInt("", 0);
		d.chnlClOut.tagValue	= new TagInt("", -1);
		
		d.chnlOpIn.tagAddrNum	  	= new TagInt("", 0);
		d.chnlOpIn.tagValue	  	= new TagInt("", -1);
		d.chnlOpOut.tagAddrNum	  	= new TagInt("", 0);
		d.chnlOpOut.tagValue	= new TagInt("", -1);
		
		d.chnlMiIn.tagAddrNum	  	= new TagInt("", 0);
		d.chnlMiIn.tagValue	  	= new TagInt("", -1);
		d.chnlMiOut.tagAddrNum	  	= new TagInt("", 0);
		d.chnlMiOut.tagValue	= new TagInt("", -1);

		d.resetState();
	}
	
	private void exec(
			int output	,       
			int sost	,         
			int inout	,          
			int chnlClOut,
			int chnlOpOut,
			int chnlMiOut
			) {
		
		d.execute();
		assertEquals("output"	, output	, d.tagOutput.getInt());     
		assertEquals("sost"		, sost		, d.tagSost.getInt());     
		assertEquals("inout"	, inout		, d.tagInOut.getInt());     
		assertEquals("chnlClOut", chnlClOut	, d.chnlClOut.tagValue.getInt());     
		assertEquals("chnlOpOut", chnlOpOut	, d.chnlOpOut.tagValue.getInt());     
		assertEquals("chnlMiOut", chnlMiOut	, d.chnlMiOut.tagValue.getInt());     
		
	}

	private void set(int inp, int opfull, int opmid, int chCl, int chOp, int chMi) {
		d.inpInput.tag.setInt(inp);
		d.inpDoOpenFull.tag.setInt(opfull);
		d.inpDoOpenMid.tag.setInt(opmid);

		d.chnlClIn.tagValue.setInt(chCl);
		d.chnlOpIn.tagValue.setInt(chOp);
		d.chnlMiIn.tagValue.setInt(chMi);
	}

	
	@Test
	public void testLoad() throws Exception {
		
		RoboplantModule robo = AuxProcs.createRoboplantModule();
		assertTrue( robo.loadProject( Paths.get( 
				InitUtils.getResourcesDir("roboplant", RoboplantModuleTest.class) + "zdvs.xml" )));

		assertEquals(1, robo.devicesByAddr.length);
		DeviceZDVS d = (DeviceZDVS)robo.devicesByAddr[0];
		
		assertEquals( "ZDVS", d.devtype );
		assertEquals( "606", d.tagname );
		
		assertEquals(4, d.inputs.size());
		assertEquals(12, d.outputs.size());
		
		assertTrue( d.init(robo) );
		assertTrue( d.prepare() );

	}
	
	

	
	@Test
	public void testLinkChannel_False() {
		d.chnlClIn.state = Channel.State.Error;
		assertFalse(d.execute());
		assertEquals(8, d.tagSost.getInt());
	}

	@Test
	public void testFlags_0() {
		d.tagFlags.setInt(0);
		d.inpEnable.tag.setInt(0);

		set( 0,  0,0,  0,0,0 );
		exec( 0,7,  7,  0,0,0 );

		set( 0,  1,1,  0,0,0 );
		exec( 0,7,  7,  0,0,0 );
	}

	@Test
	public void testFlags_80_close_fullopen_close() {
		d.tagFlags.setInt(0x80);
		d.inpEnable.tag.setInt(1);

// Sost:		
// 0 - закр              
// 1 - откр              
// 2 - сред              
// 3 - неопред           
// 4 - закр-сред         
// 5 - откр-сред         
// 6 -                   
// 7 - ошибка            
// 8 - неправильный адрес

		//    I    F M          C O M
		//    O S     MOC  MOC  C O M
		
		set(  0,   0,0,  		0,0,0 ); // close, cl-mi
		exec( 0,4, 0b0001_0000, 1,0,0 );

		set(  0,   0,0,  		1,0,0 ); // close, cl
		exec( 0,0, 0b0000_0001, 0,0,0 );

		set(  0,   1,0,  		1,0,0 ); // open - cl 
		exec( 0,0, 0b0010_0001, 0,1,0 );

		set(  0,   1,0,  		0,0,0 ); // open - cl-mi 
		exec( 0,4, 0b0010_0000, 0,1,0 );

		set(  0,   1,0,  		0,0,1 ); // open - mi 
		exec( 0,2, 0b0010_0100, 0,1,0 );

		set(  0,   1,0,  		0,0,0 ); // open - mi-op 
		exec( 0,5, 0b0010_0000, 0,1,0 );

		set(  0,   1,0,  		0,1,0 ); // open - op 
		exec( 0,1, 0b0000_0010, 0,0,0 );

		set(  0,   0,0,  		0,1,0 ); // close - op 
		exec( 0,1, 0b0001_0010, 1,0,0 );

		set(  0,   0,0,  		0,0,0 ); // close - op-mi 
		exec( 0,5, 0b0001_0000, 1,0,0 );

		set(  0,   0,0,  		0,0,1 ); // close - mi 
		exec( 0,2, 0b0001_0100, 1,0,0 );

		set(  0,   0,0,  		0,0,0 ); // close - mi-cl 
		exec( 0,4, 0b0001_0000, 1,0,0 );

		set(  0,   0,0,  		1,0,0 ); // close - cl 
		exec( 0,0, 0b0000_0001, 0,0,0 );
	}


	@Test
	public void testFlags_80_input() {
		d.tagFlags.setInt(0x80);
		d.inpEnable.tag.setInt(0);

		set(  0,   0,0,  		0,0,0 ); // en=0, inp=0, close, ?
		exec( 0,4, 0b0000_0000, 0,0,0 );

		set(  1,   0,0,  		0,0,0 ); // en=0, inp=1, close, ?
		exec( 0,4, 0b0000_0000, 0,0,0 );
		
		d.inpEnable.tag.setInt(1);
		set(  1,   0,0,  		0,0,0 ); // en=1, inp=1, close, ?
		exec( 0,4, 0b0001_0000, 1,0,0 );

		set(  1,   0,0,  		1,0,0 ); // en=1, inp=1, close, cl
		exec( 1,0, 0b0000_0001, 0,0,0 );

		set(  1,   0,0,  		0,0,0 ); // en=1, inp=1, close, ?
		exec( 0,4, 0b0001_0000, 1,0,0 );
	}
	
	

	
//	b3 reset - ignore 2 sensors off
//  b3 set - ignore 2 sensors on		
	@Test
	public void testFlags_b3reset_closeToOpen() {
		d.tagFlags.setInt(0x80);
		d.inpEnable.tag.setInt(1);
		// Sost:		
		// 0 - закр              
		// 1 - откр              
		// 2 - сред              
		// 3 - неопред           
		// 4 - закр-сред         
		// 5 - откр-сред         
		// 6 -                   
		// 7 - ошибка            
		// 8 - неправильный адрес

		//    I    F M          C O M
		//    O S     MOC  MOC  C O M
		set(  0,   0,0,  		1,0,0 ); // close, clsd

		exec( 0,0, 0b0000_0001, 0,0,0 );
		
		set(  0,   1,0,  		1,0,0 ); // open, clsd

		exec( 0,0, 0b0010_0001, 0,1,0 );		
		
		set(  0,   1,0,  		0,0,1 ); // open, midl

		exec( 0,2, 0b0010_0100, 0,1,0 );
		
		set(  0,   1,0,  		0,1,0 ); // open, opened

		exec( 0,1, 0b0000_0010, 0,0,0 );	

	}
	
	@Test
	public void testFlags_b3reset_openClose() {
		d.tagFlags.setInt(0x80);
		d.inpEnable.tag.setInt(1);
		// Sost:		
		// 0 - закр              
		// 1 - откр              
		// 2 - сред              
		// 3 - неопред           
		// 4 - закр-сред         
		// 5 - откр-сред         
		// 6 -                   
		// 7 - ошибка            
		// 8 - неправильный адрес

		//    I    F M          C O M
		//    O S     MOC  MOC  C O M
		set(  0,   0,0,  		1,0,0 ); // close, clsd

		exec( 0,0, 0b0000_0001, 0,0,0 );
		
		set(  0,   1,0,  		1,0,0 ); // open, clsd

		exec( 0,0, 0b0010_0001, 0,1,0 );		
		
		set(  0,   1,0,  		0,0,1 ); // open, midl
		exec( 0,2, 0b0010_0100, 0,1,0 );
		
		set(  0,   1,0,  		0,1,0 ); // open, opened
		exec( 0,1, 0b0000_0010, 0,0,0 );	

		//---------------close----------------//
		set(  0,   0,0,  		0,1,0 ); // close, opened
		exec( 0,1, 0b0001_0010, 1,0,0 );		
		
		set(  0,   0,0,  		0,0,1 ); // close, midle
		exec( 0,5, 0b0001_0100, 1,0,0 );
		
		set(  0,   0,0,  		0,0,0 ); // close, close
		exec( 0,4, 0b0001_0000, 1,0,0 );

		set(  0,   0,0,  		1,0,0 ); // close, close
		exec( 0,0, 0b0000_0001, 0,0,0 );

	}	
	
	
	@Test
	public void testFlags_b3reset_closeToMidle() {
		d.tagFlags.setInt(0x80);
		d.inpEnable.tag.setInt(1);
		// Sost:		
		// 0 - закр              
		// 1 - откр              
		// 2 - сред              
		// 3 - неопред           
		// 4 - закр-сред         
		// 5 - откр-сред         
		// 6 -                   
		// 7 - ошибка            
		// 8 - неправильный адрес

		//    I    F M          C O M
		//    O S     MOC  MOC  C O M
		set(  0,   0,0,  		1,0,0 ); // close, clsd

		exec( 0,0, 0b0000_0001, 0,0,0 );
		
		set(  0,   0,1,  		1,0,0 ); // midle, clsd

		exec( 0,0, 0b0010_0001, 0,1,0 );		
		
		set(  0,   0,1,  		0,0,1 ); // midle, midl

		exec( 0,2, 0b0000_0100, 0,0,0 );
		
				//---------------close----------------//
		set(  0,   0,0,  		0,0,1 ); // close, midle

		exec( 0,2, 0b0001_0100, 1,0,0 );		
				
		set(  0,   0,0,  		1,0,0 ); // close, close

		exec( 0,0, 0b0000_0001, 0,0,0 );

	}	
	
	@Test
	public void testFlags_b3reset_openeToMidle() {
		d.tagFlags.setInt(0x80);
		d.inpEnable.tag.setInt(1);
		// Sost:		
		// 0 - закр              
		// 1 - откр              
		// 2 - сред              
		// 3 - неопред           
		// 4 - закр-сред         
		// 5 - откр-сред         
		// 6 -                   
		// 7 - ошибка            
		// 8 - неправильный адрес

		//    I    F M          C O M
		//    O S     MOC  MOC  C O M
		set(  0,   0,0,  		1,0,0 ); // close, clsd

		exec( 0,0, 0b0000_0001, 0,0,0 );
		
		set(  0,   1,0,  		1,0,0 ); // open, clsd

		exec( 0,0, 0b0010_0001, 0,1,0 );		
		
		set(  0,   1,0,  		0,0,1 ); // open, midl

		exec( 0,2, 0b0010_0100, 0,1,0 );
		
		set(  0,   1,0,  		0,1,0 ); // open, opened

		exec( 0,1, 0b0000_0010, 0,0,0 );	
		
		//---------------midles----------------//
		//	  I    F M          C O M
		//    O S     MOC  MOC  C O M
		set(  0,   0,1,  		0,1,0 ); // midle, opened

		exec( 0,1, 0b0001_0010, 1,0,0 ); // 		

		set(  0,   0,1,  		0,0,1 ); // midle, midle

		//---????? close must be 1!!!
		exec( 0,5, 0b0001_0100, 1,0,0 ); // midle, midle		
		
		set(  0,   0,1,  		0,0,0 ); // midle, none
		
		//---

		exec( 0,4, 0b0010_0000, 0,1,0 ); // midle, none		
		
		set(  0,   0,1,  		0,0,1 ); // midle, midle

		exec( 0,2, 0b0000_0100, 0,0,0 ); // midle, midle		
		
	}	
	
}


