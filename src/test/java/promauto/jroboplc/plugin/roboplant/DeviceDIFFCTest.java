package promauto.jroboplc.plugin.roboplant;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static promauto.jroboplc.plugin.roboplant.AuxProcs.newInputAndTag;

import java.nio.file.Paths;

import org.junit.Before;
import org.junit.Test;

import promauto.jroboplc.core.api.InitUtils;
import promauto.jroboplc.core.tags.TagInt;

public class DeviceDIFFCTest {
	private DeviceDIFFC d;
	
	@Before
	public void setUp() throws Exception {
		InitUtils.setupLogger();

		d = new DeviceDIFFC();
		d.inpEnable		 = newInputAndTag();
		d.inpSusp		 = newInputAndTag();	
		d.inpCurTmp		 = newInputAndTag();
		d.inpMaxTmp		 = newInputAndTag();
		d.inpMinTmp		 = newInputAndTag();
		d.inpAlrmTmpHigh = newInputAndTag();	
		d.inpAlrmTmpLow	 = newInputAndTag();
		d.inpError		 = newInputAndTag();
		
		d.tagOutVal		    = new TagInt("", 0);
		d.tagState		    = new TagInt("", 0);
		d.tagMinOut		    = new TagInt("", 0);
		d.tagMaxOut         = new TagInt("", 0);
		d.tagStartPwr       = new TagInt("", 0);
		d.tagPwrStep        = new TagInt("", 0);
		d.tagPwrMult        = new TagInt("", 0);
		d.tagTimePreHeat    = new TagInt("", 0);
		d.tagTimeWrkHigh    = new TagInt("", 0);
		d.tagTimeWrkLow     = new TagInt("", 0);
		d.tagOldTmp	        = new TagInt("", 0);
		d.tagFlSetHigh      = new TagInt("", 0);
		d.tagTimeWrkLowCnt  = new TagInt("", 0);
		d.tagTimeWrkHighCnt = new TagInt("", 0);
		d.tagTimePreCnt     = new TagInt("", 0); 
		
		
		d.tagMinOut.setInt(4);
		d.tagMaxOut.setInt(20);
		d.tagStartPwr.setInt(10);
		d.tagPwrStep.setInt(10);
		d.tagTimePreHeat.setInt(6);
		d.tagTimeWrkHigh.setInt(3);
		d.tagTimeWrkLow.setInt(2);

		d.resetState();

	}

	@Test
	public void testLoad() throws Exception {
		RoboplantModule robo = AuxProcs.createRoboplantModule();
		assertTrue( robo.loadProject( Paths.get( 
				InitUtils.getResourcesDir("roboplant", RoboplantModuleTest.class) + "diffc.xml" )));

		assertEquals(1, robo.devicesByAddr.length);
		d = (DeviceDIFFC)robo.devicesByAddr[0];
		assertTrue( d.init(robo) );
		assertTrue( d.prepare() );

		assertEquals( "DIFFC", d.devtype );
		assertEquals( "432", d.name );
		assertEquals( "432", d.tagname );
	}
	
	private void set(
			int inpEnable,
			int inpSusp,
			int inpCurTmp,
			int inpMaxTmp,
			int inpMinTmp,
			int inpAlrmTmpHigh,
			int inpAlrmTmpLow,
			int inpError
			) {
		d.inpEnable     .tag.setInt(inpEnable     );
		d.inpSusp       .tag.setInt(inpSusp       );
		d.inpCurTmp     .tag.setInt(inpCurTmp     );
		d.inpMaxTmp     .tag.setInt(inpMaxTmp     );
		d.inpMinTmp     .tag.setInt(inpMinTmp     );
		d.inpAlrmTmpHigh.tag.setInt(inpAlrmTmpHigh);
		d.inpAlrmTmpLow .tag.setInt(inpAlrmTmpLow );
		d.inpError      .tag.setInt(inpError      );
	}

	
	private void exec(
			int tagOutVal,
			int tagState,
			int tagPwrMult,
			int tagOldTmp,
			int tagTimePreCnt,
			int tagTimeWrkHighCnt, 	
			int tagTimeWrkLowCnt, 	
			int tagFlSetHigh
			) {
		d.execute();
		
		assertEquals("tagOutVal"			, tagOutVal,         d.tagOutVal        .getInt());     
		assertEquals("tagState"				, tagState,          d.tagState         .getInt());     
		assertEquals("tagPwrMult"			, tagPwrMult,        d.tagPwrMult       .getInt());     
		assertEquals("tagOldTmp"			, tagOldTmp,         d.tagOldTmp        .getInt());     
		assertEquals("tagTimePreCnt"		, tagTimePreCnt,     d.tagTimePreCnt    .getInt());     
		assertEquals("tagTimeWrkHighCnt"	, tagTimeWrkHighCnt, d.tagTimeWrkHighCnt.getInt());     
		assertEquals("tagTimeWrkLowCnt"		, tagTimeWrkLowCnt,  d.tagTimeWrkLowCnt .getInt());     
		assertEquals("tagFlSetHigh"			, tagFlSetHigh,      d.tagFlSetHigh     .getInt());     
	}

	@Test
	public void testExecute1() {
		
		
		//   Enable   Susp  CurTmp   MaxTmp  MinTmp AlrmTmpH AlrmTmpL Error      OutVal  State  PwrMult  OldTmp TPreCnt TWrkHCnt TWrkLCnt FlSetHigh      
		set(	0,		0,		0,		50,		30,		55,		25,		0);	exec(	4,		0,		1,		0,		6,		0,		0,		0 );
		set(	1,		0,		0,		50,		30,		55,		25,		0);	exec(	5,		1,		1,		0,		5,		0,		0,		0 );
		set(	1,		0,		0,		50,		30,		55,		25,		0);	exec(	5,		1,		1,		0,		4,		0,		0,		0 );
		set(	1,		0,		0,		50,		30,		55,		25,		0);	exec(	5,		1,		1,		0,		3,		0,		0,		0 );
		set(	1,		0,		0,		50,		30,		55,		25,		0);	exec(	5,		1,		1,		0,		2,		0,		0,		0 );
		set(	1,		0,		0,		50,		30,		55,		25,		0);	exec(	5,		1,		1,		0,		1,		0,		0,		0 );
		set(	1,		0,		0,		50,		30,		55,		25,		0);	exec(	5,		1,		1,		0,		0,		0,		0,		0 );
		//8
		set(	1,		0,		0,		50,		30,		55,		25,		0);	exec(	8,		5,		1,		0,		0,		3,		0,		1 );
		set(	1,		0,		0,		50,		30,		55,		25,		0);	exec(	8,		5,		1,		0,		0,		2,		0,		1 );
		set(	1,		0,		0,		50,		30,		55,		25,		0);	exec(	8,		5,		1,		0,		0,		1,		0,		1 );
		set(	1,		0,		0,		50,		30,		55,		25,		0);	exec(	8,		5,		1,		0,		0,		0,		0,		1 );
		//11
		set(	1,		0,		0,		50,		30,		55,		25,		0);	exec(	11,		5,		2,		0,		0,		3,		0,		1 );
		set(	1,		0,		0,		50,		30,		55,		25,		0);	exec(	11,		5,		2,		0,		0,		2,		0,		1 );
		set(	1,		0,		0,		50,		30,		55,		25,		0);	exec(	11,		5,		2,		0,		0,		1,		0,		1 );
		set(	1,		0,		0,		50,		30,		55,		25,		0);	exec(	11,		5,		2,		0,		0,		0,		0,		1 );
		//15
		set(	1,		0,		0,		50,		30,		55,		25,		0);	exec(	15,		5,		3,		0,		0,		3,		0,		1 );
		set(	1,		0,		0,		50,		30,		55,		25,		0);	exec(	15,		5,		3,		0,		0,		2,		0,		1 );
		set(	1,		0,		0,		50,		30,		55,		25,		0);	exec(	15,		5,		3,		0,		0,		1,		0,		1 );
		set(	1,		0,		0,		50,		30,		55,		25,		0);	exec(	15,		5,		3,		0,		0,		0,		0,		1 );
		//19
		set(	1,		0,		0,		50,		30,		55,		25,		0);	exec(	15,		5,		0,		0,		0,		3,		0,		1 );
		set(	1,		0,		0,		50,		30,		55,		25,		0);	exec(	15,		5,		0,		0,		0,		2,		0,		1 );
		set(	1,		0,		0,		50,		30,		55,		25,		0);	exec(	15,		5,		0,		0,		0,		1,		0,		1 );
		set(	1,		0,		0,		50,		30,		55,		25,		0);	exec(	15,		5,		0,		0,		0,		0,		0,		1 );
		//23
		set(	1,		0,		0,		50,		30,		55,		25,		0);	exec(	16,		5,		1,		0,		0,		3,		0,		1 );
		set(	1,		0,		5,		50,		30,		55,		25,		0);	exec(	16,		5,		1,		5,		0,		2,		0,		1 );
		set(	1,		0,		10,		50,		30,		55,		25,		0);	exec(	16,		5,		1,		10,		0,		1,		0,		1 );
		set(	1,		0,		15,		50,		30,		55,		25,		0);	exec(	16,		5,		1,		15,		0,		0,		0,		1 );
		//27
		set(	1,		0,		20,		50,		30,		55,		25,		0);	exec(	16,		5,		1,		20,		0,		3,		0,		1 );
		set(	1,		0,		25,		50,		30,		55,		25,		0);	exec(	16,		5,		1,		25,		0,		2,		0,		1 );
		set(	1,		0,		30,		50,		30,		55,		25,		0);	exec(	16,		7,		1,		30,		0,		0,		0,		1 );
		set(	1,		0,		35,		50,		30,		55,		25,		0);	exec(	16,		7,		1,		35,		0,		0,		0,		1 );
		set(	1,		0,		40,		50,		30,		55,		25,		0);	exec(	16,		7,		1,		40,		0,		0,		0,		1 );
		set(	1,		0,		45,		50,		30,		55,		25,		0);	exec(	16,		7,		1,		45,		0,		0,		0,		1 );
		set(	1,		0,		50,		50,		30,		55,		25,		0);	exec(	16,		7,		1,		50,		0,		0,		0,		1 );
		//34
		set(	1,		0,		55,		50,		30,		55,		25,		0);	exec(	15,		6,		1,		55,		0,		0,		2,		0 );
		set(	1,		0,		60,		50,		30,		55,		25,		0);	exec(	15,		6,		1,		60,		0,		0,		1,		0 );
		set(	1,		0,		75,		50,		30,		55,		25,		0);	exec(	15,		6,		1,		75,		0,		0,		0,		0 );
	}

	
	@Test
	public void testExecute2() {
		//   Enable   Susp  CurTmp   MaxTmp  MinTmp AlrmTmpH AlrmTmpL Error       
		set(	1,		0,		0,		50,		30,		55,		25,		0);	
		for(int i=0; i<24; i++)
			d.execute();
		
		//   Enable   Susp  CurTmp   MaxTmp  MinTmp AlrmTmpH AlrmTmpL Error      OutVal  State  PwrMult  OldTmp TPreCnt TWrkHCnt TWrkLCnt FlSetHigh      
		//25
		set(	1,		0,		0,		50,		30,		55,		25,		0);	exec(	16,		5,		1,		0,		0,		1,		0,		1 );
		set(	1,		0,		75,		50,		30,		55,		25,		0);	exec(	13,		6,		1,		75,		0,		1,		2,		0 );
		d.execute();
		d.execute();
		d.execute();
		set(	1,		0,		75,		50,		30,		55,		25,		0);	exec(	10,		6,		1,		75,		0,		1,		1,		0 );
		d.execute();
		d.execute();
		d.execute();
		set(	1,		0,		75,		50,		30,		55,		25,		0);	exec(	7,		6,		1,		75,		0,		1,		0,		0 );
	}
	

	@Test
	public void testExecuteError() {
		//   Enable   Susp  CurTmp   MaxTmp  MinTmp AlrmTmpH AlrmTmpL Error       
		set(	1,		0,		0,		50,		30,		55,		25,		0);	
		for(int i=0; i<24; i++)
			d.execute();
		
		//   Enable   Susp  CurTmp   MaxTmp  MinTmp AlrmTmpH AlrmTmpL Error      OutVal  State  PwrMult  OldTmp TPreCnt TWrkHCnt TWrkLCnt FlSetHigh      
		//25
		set(	1,		0,		0,		50,		30,		55,		25,		0);	exec(	16,		5,		1,		0,		0,		1,		0,		1 );
		set(	1,		0,		0,		50,		30,		55,		25,		1);	exec(	4,		3,		1,		0,		6,		1,		0,		1 );
		set(	1,		0,		0,		50,		30,		55,		25,		0);	exec(	5,		1,		1,		0,		5,		1,		0,		1 );
		for(int i=0; i<7; i++)	d.execute();
		//35
		set(	1,		0,		0,		50,		30,		55,		25,		0);	exec(	8,		5,		2,		0,		0,		2,		0,		1 );
		set(	1,		0,		0,		50,		30,		55,		25,		1);	exec(	4,		3,		2,		0,		6,		2,		0,		1 );
		set(	1,		0,		100,	50,		30,		55,		25,		1);	exec(	4,		3,		2,		0,		6,		2,		0,		1 );
		set(	1,		0,		100,	50,		30,		55,		25,		0);	exec(	4,		6,		1,		100,	0,		2,		2,		0 );
		set(	1,		0,		100,	50,		30,		55,		25,		0);	exec(	4,		6,		1,		100,	0,		2,		1,		0 );
		set(	1,		0,		100,	50,		30,		55,		25,		0);	exec(	4,		6,		1,		100,	0,		2,		0,		0 );
		set(	1,		0,		100,	50,		30,		55,		25,		0);	exec(	4,		6,		1,		100,	0,		2,		2,		0 );
	}
	
}





























