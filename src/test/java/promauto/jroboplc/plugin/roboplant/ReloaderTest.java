package promauto.jroboplc.plugin.roboplant;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import promauto.jroboplc.core.api.*;
import promauto.jroboplc.core.RealEnvironment;

public class ReloaderTest {
	
	RoboplantModule mod;
	RoboplantModule tmp;
	SampleEnvironment env;
	SampleModuleManager mm;

	private static final String CFGDIR = InitUtils.getResourcesDir("roboplant", ReloaderTest.class);

	@Before
	public void setUp() throws Exception {
		InitUtils.setupLogger();
	
		env = new SampleEnvironment();
		EnvironmentInst.set(env);



		
	}


	private void load(String testnum) throws Exception {
		
		Map<String,Object> conf = new HashMap<>();
		conf.put("enable", true);
		
		mod = AuxProcs.createRoboplantModule();
		conf.put("project", "reload/" + testnum + "/old.xml");
		assertTrue( mod.load( conf ));
		assertTrue( mod.prepare() );
		
		tmp = AuxProcs.createRoboplantModule();
		conf.put("project", "reload/" + testnum + "/new.xml");
		assertTrue( tmp.load( conf ));
	}

	
	@Test
	public void testTransferTags() throws Exception {

		load("1");
		
		Reloader r = new Reloader(mod);
		r.tmp = tmp;
		
		String tagname = "MCHB_618_Control";
		Tag taga1 = mod.getTagTable().get(tagname);
		Tag tagb1 = tmp.getTagTable().get(tagname);
		taga1.setInt(3);
		tagb1.setInt(4);
		
		r.transferTags();
		
		assertEquals( 3, taga1.getInt());
	}
	
	
	@Test
	public void testPrepare() throws Exception {
		
		load("1");
		
		assertEquals("618 Нория", mod.getDevice(0).name );
		Tag tag1 = mod.getTagTable().get("MCHB_618_MasterOut");
		tag1.setInt(3);
		
		Tag tagInputA = mod.getDevice(0).getInput("InputA").tag;
		Tag tagInputB = mod.getDevice(0).getInput("InputB").tag;
		Tag tagTrigRes = mod.getDevice(0).getInput("TrigRes").tag;
		
		assertEquals("MCHB_618@InputA_1", tagInputA.getName());
		assertEquals("MCHB_618@Suspend_0", mod.getDevice(0).getInput("Suspend").tag.getName());
		assertEquals("MCHB_618@InputB_2", tagInputB.getName());
		assertEquals("MCHB_618@TrigRes_4", tagTrigRes.getName());
		assertEquals("ZDVA_607_Sost", mod.getDevice(0).getInput("InputC").tag.getName());
		assertNotNull( mod.getTagTable().get("ZDVA_607_Alarm") );
		assertNull( mod.getTagTable().get("BIT_S01_Dst_35"));

		assertEquals(0, tagInputA.getInt());
		assertEquals(7, tagInputB.getInt());
		assertSame(tagInputB, mod.getDevice(0).getInput("InputB").tag);

		
		Reloader r = new Reloader(mod);
		r.tmp = tmp;
		
		r.saveState();
		r.transferTags();
		assertTrue( tmp.prepare() );

		r.assignNewDevices();
		assertEquals(0, tag1.getInt());

		r.loadState();

		Tag tagInputAnew = mod.getDevice(0).getInput("InputA").tag;
		Tag tagInputBnew = mod.getDevice(0).getInput("InputB").tag;
		Tag tagTrigResnew = mod.getDevice(0).getInput("TrigRes").tag;
		
		assertNotSame(tagInputA, tagInputAnew);
		assertEquals("MCHB_618@InputA_1", tagInputA.getName());
		assertEquals("MCHB_618@InputA_0", tagInputAnew.getName());

		assertNotSame(tagInputB, tagInputBnew);
		assertEquals("MCHB_618@InputB_2", tagInputB.getName());
		assertEquals("MCHB_618@InputB_1", tagInputBnew.getName());

		assertSame(tagTrigRes, tagTrigResnew);

		assertEquals("618 Нория SSS", mod.getDevice(0).name );
		assertEquals("PLCI_M00_Channel_0", mod.getDevice(0).getInput("Suspend").tag.getName());
		assertEquals("MCHB_618@BADREF_InputC", mod.getDevice(0).getInput("InputC").tag.getName());
		assertNull( mod.getTagTable().get("ZDVA_607_Alarm") );
		assertNotNull( mod.getTagTable().get("BIT_S01_Dst_35"));

		assertEquals(3, tag1.getInt());
		assertEquals(0, tagInputA.getInt());
		assertEquals(7, tagInputB.getInt());
		assertEquals(1, tagInputAnew.getInt());
		assertEquals(8, tagInputBnew.getInt());
	}
	
	
	@Test
	public void testReload() throws Exception {
		
		assertTrue( RealEnvironment.create(CFGDIR) );

		
		Object confrobo1 = EnvironmentInst.get().getConfiguration().getModuleConf("roboplant", "robo1");
		
		RoboplantPlugin plg = new RoboplantPlugin();
		plg.initialize();
		RoboplantModule m = (RoboplantModule)plg.createModule("robo1", confrobo1);
		assertNotNull(m);
		
		List<Tag> tags1 = m.getTagTable().getTags("", true);
		
		assertTrue(m.reload());

		List<Tag> tags2 = m.getTagTable().getTags("", true);
		assertNotSame(tags1, tags2);
		assertEquals(tags1, tags2);

		assertTrue(m.reload());
		
	}

	

}
