package promauto.jroboplc.plugin.roboplant;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.lang.reflect.Field;
import java.util.List;

import promauto.jroboplc.core.api.*;
import promauto.jroboplc.core.ConfigurationYaml;
import promauto.jroboplc.core.tags.TagInt;

public class AuxProcs {
	private static final String CFG_DIR = InitUtils.getResourcesDir("roboplant", RoboplantModuleTest.class);


	public static Input createInput(Device d, String name, int refaddr,
			int refnum) {
		Input input = new Input();
		input.name = name;
		input.refaddr = refaddr;
		input.refnum = refnum;
		d.inputs.add(input);
		return input;
	}

	public static Output createOutput(Device d, String name, int value, int num) {
		Output output = new Output();
		output.name = name;
		output.inival = value;
		output.num = num;
		d.outputs.add(output);
		d.outputsByNum.add(output);
		return output;
	}

	public static Tag createTagWithOutput(Device d, String name, int num, int value) {
		Output output = new Output();
		output.name = name;
		output.inival = value;
		output.num = num;
		output.tag = new TagInt(d.devtype + "_" + d.tagname + "_" + name, value); 
		d.outputs.add(output);
		d.outputsByNum.add(output);
		return output.tag;
	}

	public static Tag addTagToList(List<Tag> list, int inival) {
		Tag tag = new TagInt("", inival);
		list.add(tag);
		return tag;
	}

	
	public static Input newInputAndTag(int value) {
		Input inp = new Input();
		inp.tag = new TagInt("", value);
		return inp;
	}

	public static Input newInputAndTag() {
		return newInputAndTag(0);
	}


	
	public static RoboplantModule createRoboplantModule() throws Exception {
		
		Environment env = mock( Environment.class );
		EnvironmentInst.set(env);
		
		Configuration cm = new ConfigurationYaml( CFG_DIR );
		cm.load();

		RoboplantPlugin plugin = mock( RoboplantPlugin.class );
		when(plugin.getConnectManager()).thenReturn(new ConnectManager(env));

		when(env.getXMLInputFactory()).thenReturn(InitUtils.getXMLIF());
		when(env.getConfiguration()).thenReturn(cm);

		SampleModuleManager mm = new SampleModuleManager();
		when(env.getModuleManager()).thenReturn(mm);


		return new RoboplantModule(plugin, "robo");

	}
	
	public static void createInputsOutputs(Device d) throws IllegalArgumentException, IllegalAccessException {
		createInputsOutputs(d, d.getClass());
	}

	public static void createInputsOutputs(Device d, Class<?> clz) throws IllegalArgumentException, IllegalAccessException {
		for (Field field : clz.getDeclaredFields()) {
			if( field.getName().startsWith("tag")) {
				field.setAccessible(true);
				field.set(d, new TagInt("", 0));
			}

			if( field.getName().startsWith("inp")) {
				field.setAccessible(true);
				Input inp = new Input();
				inp.tag = new TagInt("", 0);
				field.set(d, inp);
			}
		}
	}


}
