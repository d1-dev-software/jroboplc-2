package promauto.jroboplc.plugin.roboplant;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import promauto.jroboplc.core.api.InitUtils;
import promauto.jroboplc.core.tags.TagInt;

import static org.junit.Assert.assertEquals;


public class DeviceLOADBTest {


	private DeviceLOADB d;

	@Mock RoboplantModule module;


	@Before
	public void setUp() throws Exception {
		InitUtils.setupLogger();
		
		d = new DeviceLOADB();
		d.module = module;
		AuxProcs.createInputsOutputs(d);

        for(int i=1; i<=16; ++i) {
            DeviceLOADB.Item it = new DeviceLOADB.Item();
            it.tagOutput = new TagInt("Output" + i, 0);
            it.tagSelect = new TagInt("Select" + i, 0);
            it.index = i;
            it.select = false;
            d.items.add(it);
        }

	}



    private void exec(
            int current,
            int changing,
            int output0,
            int output1,
            int output2,
            int output16,
            int select1,
            int select2,
            int select16
    ) {
        d.execute();
        assertEquals("current",    current,   d.tagCurrent.getInt());
        assertEquals("changing",   changing,  d.tagChanging.getInt());
        assertEquals("output0",    output0,   d.tagOutput0.getInt());
        assertEquals("output1",    output1,   d.items.get(0).tagOutput.getInt());
        assertEquals("output2",    output2,   d.items.get(1).tagOutput.getInt());
        assertEquals("output16",   output16,  d.items.get(15).tagOutput.getInt());
        assertEquals("select1",    select1,   d.items.get(0).tagSelect.getInt());
        assertEquals("select2",    select2,   d.items.get(1).tagSelect.getInt());
        assertEquals("select16",   select16,  d.items.get(15).tagSelect.getInt());
    }


    @Test
	public void test_disabled() {
        exec(0,0, 0, 0, 0, 0, 0, 0, 0);
    }


    @Test
    public void test_select() {
        d.inpEnable.tag.setInt(1);
        d.tagDlyChange.setInt(0);

        exec(0,0, 1, 0, 0, 0, 0, 0, 0);

        select(1);
        exec(1,0, 0, 1, 0, 0, 1, 0, 0);

        select(2);
        exec(2,0, 0, 0, 1, 0, 0, 1, 0);

        select(16);
        exec(16,0, 0, 0, 0, 1, 0, 0, 1);

        unselect(16);
        exec(0,0, 1, 0, 0, 0, 0, 0, 0);
    }

    @Test
    public void test_current() {
        d.inpEnable.tag.setInt(1);
        d.tagDlyChange.setInt(0);

        exec(0,0, 1, 0, 0, 0, 0, 0, 0);

        d.tagCurrent.setInt(1);
        exec(1,0, 0, 1, 0, 0, 1, 0, 0);

        d.tagCurrent.setInt(2);
        exec(2,0, 0, 0, 1, 0, 0, 1, 0);

        d.tagCurrent.setInt(16);
        exec(16,0, 0, 0, 0, 1, 0, 0, 1);

        d.tagCurrent.setInt(0);
        exec(0,0, 1, 0, 0, 0, 0, 0, 0);
    }


    @Test
    public void test_changing() {
        d.inpEnable.tag.setInt(1);
        d.tagDlyChange.setInt(2);

        exec(0,0, 1, 0, 0, 0, 0, 0, 0);

        select(1);
        exec(1,1, 1, 0, 0, 0, 1, 0, 0);
        exec(1,1, 1, 0, 0, 0, 1, 0, 0);
        exec(1,0, 0, 1, 0, 0, 1, 0, 0);

        select(2);
        exec(2,1, 0, 1, 0, 0, 0, 1, 0);
        exec(2,1, 0, 1, 0, 0, 0, 1, 0);
        select(16);
        exec(16,1, 0, 1, 0, 0, 0, 0, 1);
        exec(16,1, 0, 1, 0, 0, 0, 0, 1);
        exec(16,0, 0, 0, 0, 1, 0, 0, 1);

        unselect(16);
        exec(0,0, 1, 0, 0, 0, 0, 0, 0);
    }



    private void select(int index) {
        d.items.get(index-1).tagSelect.setInt(1);
    }

    private void unselect(int index) {
        d.items.get(index-1).tagSelect.setInt(0);
    }

}

