package promauto.jroboplc.plugin.roboplant;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import promauto.jroboplc.core.api.InitUtils;

import static org.junit.Assert.assertEquals;
import static promauto.jroboplc.plugin.roboplant.DeviceSTMD.*;


public class DeviceSTMDTest {

	private DeviceSTMD d;

	@Mock RoboplantModule module;


	@Before
	public void setUp() throws Exception {
		InitUtils.setupLogger();
		
		d = new DeviceSTMD();
		d.module = module;
		
		AuxProcs.createInputsOutputs(d);
		
		d.tagCycleQnt	 .setInt(2);
		d.tagPAlarm		 .setInt(80);
		d.tagPWork		 .setInt(70);
		d.tagPWorkMax	 .setInt(70);
		d.tagPDelta		 .setInt(5);
		d.tagPDeltaMax	 .setInt(5);
		d.tagPOpenK5	 .setInt(60);
		d.tagPLoad		 .setInt(10);
		d.tagPUnload	 .setInt(20);
		d.tagPFan		 .setInt(30);
		d.tagTimeStartDly.setInt(3);
		d.tagTimeLoad	 .setInt(3);
		d.tagTimeWarm	 .setInt(0);
		d.tagTimeInflMax .setInt(7);
		d.tagTimeDeflMax .setInt(5);
		d.tagTimeExp	 .setInt(3);
		d.tagTimeExpMax	 .setInt(3);
		d.tagTimeStopFan .setInt(2);
		d.tagTimeUnload	 .setInt(3);
		d.tagTimeVibr	 .setInt(3);
		d.tagTimeCycDly	 .setInt(3);
		d.tagTimeLoadVibr.setInt(3);
        d.tagPBlow       .setInt(5);






		    
	}

	
	private void exec(
			int State    ,
			int Alarm    ,
			int Timer    ,
			
			int CycleNum ,

			int OpenK1   ,
			int OpenK2   ,
			int OpenK3   ,
			int OpenK4   ,
			int OpenK5   ,
			int OpenK6   ,
			int stopfan  ,
			int vibr     ,

			int WaitCl   ,
			int WaitOp   ,
			int WaitEx   
			) {
		
		d.execute();
		assertEquals("State   "	, State   , d.tagState   .getInt());     
		assertEquals("Alarm   "	, Alarm   , d.tagAlarm   .getInt());     
		assertEquals("Timer   "	, Timer   , d.tagTimer   .getInt());     
		assertEquals("CycleNum"	, CycleNum, d.tagCycleNum.getInt());     
		assertEquals("OpenK1  "	, OpenK1  , d.tagOpenK1  .getInt());
		assertEquals("OpenK2  "	, OpenK2  , d.tagOpenK2  .getInt());     
		assertEquals("OpenK3  "	, OpenK3  , d.tagOpenK3  .getInt());     
		assertEquals("OpenK4  "	, OpenK4  , d.tagOpenK4  .getInt());     
		assertEquals("OpenK5  "	, OpenK5  , d.tagOpenK5  .getInt());     
		assertEquals("OpenK6  "	, OpenK6  , d.tagOpenK6  .getInt());     
		assertEquals("stopfan "	, stopfan , d.tagStopFan .getInt());
		assertEquals("vibr    "	, vibr    , d.tagVibr    .getInt());

		assertEquals("WaitCl  "	, WaitCl  , d.tagWaitCl  .getInt());
		assertEquals("WaitOp  "	, WaitOp  , d.tagWaitOp  .getInt());     
		assertEquals("WaitEx  "	, WaitEx  , d.tagWaitEx  .getInt());     
	}

	private void nextState(int state) {
		d.tagNextState.setInt(state);
		d.execute();
		assertEquals(state, d.tagState.getInt());
	}

	private void nextState(int stateSet, int stateGet) {
		d.tagNextState.setInt(stateSet);
		d.execute();
		assertEquals(stateGet, d.tagState.getInt());
	}

	

	@Test
	public void test1() {
		exec(0, 0, -1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0);

		// STATE_OFF
		d.tagBtnStart.setInt(1);
		exec(1, 0, 3, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, WAIT_TIMER);
		exec(1, 0, 2, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, WAIT_TIMER);
		exec(1, 0, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, WAIT_TIMER);

		// STATE_BEFORE_LOAD
		exec(2, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0b00001111_00000000, 0, WAIT_PLINE_OK + WAIT_PAIR_OK + WAIT_PSENSOR_OK + WAIT_ENERGY_OK + WAIT_DVU1);
		exec(2, 0, -1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0b00001111_00000000, 0, WAIT_PLINE_OK + WAIT_PAIR_OK + WAIT_PSENSOR_OK + WAIT_ENERGY_OK + WAIT_DVU1);

		d.inpClosedK1.tag.setInt(1);
		exec(2, 0, -1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0b00001110_00000000, 0, WAIT_PLINE_OK + WAIT_PAIR_OK + WAIT_PSENSOR_OK + WAIT_ENERGY_OK + WAIT_DVU1);

		d.inpClosedK2.tag.setInt(1);
		exec(2, 0, -1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0b00001100_00000000, 0, WAIT_PLINE_OK + WAIT_PAIR_OK + WAIT_PSENSOR_OK + WAIT_ENERGY_OK + WAIT_DVU1);

		d.inpClosedK3.tag.setInt(1);
		exec(2, 0, -1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0b00001000_00000000, 0, WAIT_PLINE_OK + WAIT_PAIR_OK + WAIT_PSENSOR_OK + WAIT_ENERGY_OK + WAIT_DVU1);

		d.inpClosedK4.tag.setInt(1);
		exec(2, 0, -1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0b00000000_00000000, 0, WAIT_PLINE_OK + WAIT_PAIR_OK + WAIT_PSENSOR_OK + WAIT_ENERGY_OK + WAIT_DVU1);

		d.inpDVU1.tag.setInt(1);
		exec(2, 0, -1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0b00000000_00000000, 0, WAIT_PLINE_OK + WAIT_PAIR_OK + WAIT_PSENSOR_OK + WAIT_ENERGY_OK);

		d.inpPSensorOk.tag.setInt(1);
		exec(2, 0, -1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0b00000000_00000000, 0, WAIT_PLINE_OK + WAIT_PAIR_OK + WAIT_ENERGY_OK);

		d.inpEnergyOk.tag.setInt(1);
		exec(2, 0, -1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0b00000000_00000000, 0, WAIT_PLINE_OK + WAIT_PAIR_OK);

		d.inpPAirOk.tag.setInt(1);
		exec(2, 0, -1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0b00000000_00000000, 0, WAIT_PLINE_OK);


		// STATE_LOAD_OPEN
		d.inpPLineOk.tag.setInt(1);
		exec(3, 0, -1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0b00000000_00000000, 0b00000001_00000000, 0);

		// STATE_LOAD_VIBRATE
		d.inpClosedK1.tag.setInt(0);
		d.inpOpenedK1.tag.setInt(1);
		exec(4, 0, 3, 0, 1, 0, 0, 0, 1, 1, 0, 1, 0b00000000_00000000, 0b00000000_00000000, WAIT_TIMER);
		exec(4, 0, 2, 0, 1, 0, 0, 0, 1, 1, 0, 1, 0b00000000_00000000, 0b00000000_00000000, WAIT_TIMER);
		exec(4, 0, 1, 0, 1, 0, 0, 0, 1, 1, 0, 1, 0b00000000_00000000, 0b00000000_00000000, WAIT_TIMER);


		// STATE_LOAD_DLY
		exec(5, 0, 3, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0b00000000_00000000, 0b00000000_00000000, WAIT_TIMER);
		exec(5, 0, 2, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0b00000000_00000000, 0b00000000_00000000, WAIT_TIMER);
		exec(5, 0, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0b00000000_00000000, 0b00000000_00000000, WAIT_TIMER);

		// STATE_LOAD_FINISH
		exec(6, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0b00100001_00000000, 0b00000000_00000000, 0);

		// STATE_BEFORE_INFLATE
		d.inpClosedK1.tag.setInt(1);
		d.inpOpenedK1.tag.setInt(0);
		d.inpClosedK6.tag.setInt(1);
		exec(7, 0, -1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0b00000000_00000000, 0b00000000_00000000, WAIT_CAN_INFL);


		//STATE_INFLATE_BLOW1
		d.inpClosedK5.tag.setInt(1);
		d.inpCanInfl.tag.setInt(1);
		exec(9, 0, 7, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0b00000000_00010000, 0b00000000_00000000, 0);

		d.inpClosedK5.tag.setInt(0);


		//STATE_INFLATE_BLOW2
		exec(10, 0, 6, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0b00000000_00000000, 0b00000000_00000000, WAIT_PBLOW);

		exec(10, 0, 5, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0b00000000_00000000, 0b00000000_00000000, WAIT_PBLOW);
		d.inpP.tag.setInt(20);

		// STATE_INFLATE
		exec(11, 0, 4, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0b00000000_00000000, 0b00000000_00000000, WAIT_PWORK);

		d.inpClosedK3.tag.setInt(0);
		d.inpP.tag.setInt(20);
		exec(11, 0, 3, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0b00000000_00000000, 0b00000000_00000000, WAIT_PWORK);

		d.inpP.tag.setInt(30);
		exec(11, 0, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0b00000000_00000000, 0b00000000_00000000, WAIT_PWORK);

		d.inpP.tag.setInt(40);
		exec(11, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0b00000000_00000000, 0b00000000_00000000, WAIT_PWORK);

		d.inpP.tag.setInt(50);
		exec(11, 2, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0b00000000_00000000, 0b00000000_00000000, WAIT_PWORK);

		d.inpP.tag.setInt(60);
		exec(11, 2, -1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0b00000000_00000000, 0b00000000_00000000, WAIT_PWORK);

		// STATE_INFLATE_FINISH
		d.inpP.tag.setInt(70);
		exec(12, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0b00010100_00000000, 0b00000000_00000000, 0);

		d.inpClosedK5.tag.setInt(1);

		// STATE_EXPOSITION
		d.inpClosedK3.tag.setInt(1);
		exec(13, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0b00000000_00000000, 0b00000000_00000000, WAIT_TIMER);
		d.inpP.tag.setInt(65);
		exec(13, 0, 2, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0b00000000_00000000, 0b00000000_00000000, WAIT_TIMER);
		d.inpP.tag.setInt(75);
		exec(13, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0b00000000_00000000, 0b00000000_00000000, WAIT_TIMER);

		// STATE_DEFLATE
		exec(14, 0, 5, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0b00000000_00000000, 0b00000000_00000000, WAIT_PFAN);
		d.inpClosedK6.tag.setInt(0);
		exec(14, 0, 4, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0b00000000_00000000, 0b00000000_00000000, WAIT_PFAN);
		d.inpP.tag.setInt(65);
		exec(14, 0, 3, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0b00000000_00000000, 0b00000000_00000000, WAIT_PFAN);
		d.inpP.tag.setInt(60);
		exec(14, 0, 2, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0b00000000_00000000, 0b00000000_00000000, WAIT_PFAN);
		d.inpClosedK5.tag.setInt(0);
		d.inpP.tag.setInt(50);
		exec(14, 0, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0b00000000_00000000, 0b00000000_00000000, WAIT_PFAN);
		d.inpP.tag.setInt(40);
		exec(14, 3, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0b00000000_00000000, 0b00000000_00000000, WAIT_PFAN);
		d.inpP.tag.setInt(35);
		exec(14, 3, -1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0b00000000_00000000, 0b00000000_00000000, WAIT_PFAN);
		d.inpP.tag.setInt(30);

		// STATE_STOP_FUN
		exec(15, 3, -1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0b00000000_00000000, 0b00000000_00000000, WAIT_PUNLOAD+WAIT_TIMEFAN);
		exec(15, 3, -1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0b00000000_00000000, 0b00000000_00000000, WAIT_PUNLOAD+WAIT_TIMEFAN);
		exec(15, 3, -1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0b00000000_00000000, 0b00000000_00000000, WAIT_PUNLOAD);
		exec(15, 3, -1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0b00000000_00000000, 0b00000000_00000000, WAIT_PUNLOAD);

		d.inpP.tag.setInt(20);

		// STATE_UNLOAD_OPEN
		exec(16, 0, -1, 0, 0, 1, 0, 0, 1, 1, 1, 0, 0b00000000_00000000, 0b00000010_00000000, 0);

		// STATE_UNLOAD_DLY
		d.inpClosedK2.tag.setInt(0);
		d.inpOpenedK2.tag.setInt(1);
		exec(17, 0, 3, 0, 0, 1, 0, 0, 1, 1, 1, 0, 0b00000000_00000000, 0b00000000_00000000, WAIT_TIMER);
		d.inpDVU2.tag.setInt(1);
		exec(17, 0, 2, 0, 0, 1, 0, 0, 1, 1, 1, 0, 0b00000000_00000000, 0b00000000_00000000, WAIT_TIMER);
		exec(17, 0, 1, 0, 0, 1, 0, 0, 1, 1, 1, 0, 0b00000000_00000000, 0b00000000_00000000, WAIT_TIMER);

		// STATE_UNLOAD_EMPTY
//		d.inpDVU2.tag.setInt(0);
		exec(18, 4,-1, 0, 0, 1, 0, 0, 1, 1, 1, 0, 0b00000000_00000000, 0b00000000_00000000, WAIT_DVU2);

		// STATE_UNLOAD_VIBRATE
		d.inpDVU2.tag.setInt(0);
		exec(19, 0, 3, 0, 0, 1, 0, 0, 1, 1, 1, 1, 0b00000000_00000000, 0b00000000_00000000, WAIT_TIMER);
		exec(19, 0, 2, 0, 0, 1, 0, 0, 1, 1, 1, 1, 0b00000000_00000000, 0b00000000_00000000, WAIT_TIMER);
		exec(19, 0, 1, 0, 0, 1, 0, 0, 1, 1, 1, 1, 0b00000000_00000000, 0b00000000_00000000, WAIT_TIMER);


		d.inpClosedK1.tag.setInt(0);
		d.inpClosedK3.tag.setInt(0);
		d.inpClosedK4.tag.setInt(0);
		d.inpClosedK5.tag.setInt(1);
		d.inpClosedK6.tag.setInt(1);

		// STATE_UNLOAD_FINISH
		exec(20, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0b00001111_00110000, 0b00000000_00000000, 0);

		// STATE_CYCLE_PAUSE
		d.inpClosedK1.tag.setInt(1);

		d.inpOpenedK2.tag.setInt(0);
		d.inpClosedK2.tag.setInt(1);

		d.inpClosedK3.tag.setInt(1);
		d.inpClosedK4.tag.setInt(1);

		d.inpClosedK5.tag.setInt(0);
		d.inpClosedK6.tag.setInt(0);

		exec(21, 0, 3, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0b0000000_00000000, 0b00000000_00000000, WAIT_TIMER);
		exec(21, 0, 2, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0b0000000_00000000, 0b00000000_00000000, WAIT_TIMER);
		exec(21, 0, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0b0000000_00000000, 0b00000000_00000000, WAIT_TIMER);


		// new cycle starts
		exec(2, 0, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0b0000000_00000000, 0b00000000_00000000, 0);


	}



	@Test
	public void test_no_vibr() {

		d.tagBtnStart.setInt(1);
		nextState(1);
		nextState(2);
		nextState(3);
		nextState(4);
		nextState(5);
		nextState(6);
		nextState(7);
		nextState(8,9);

		nextState(10);
		nextState(11);

		nextState(12);
		nextState(13);
		nextState(14,15);
		nextState(16);

		// STATE_UNLOAD_DLY
		d.inpClosedK2.tag.setInt(0);
		d.inpOpenedK2.tag.setInt(1);
		exec(17, 0, 3, 0, 0, 1, 0, 0, 1, 1, 1, 0, 0b00000000_00000000, 0b00000000_00000000, WAIT_TIMER);
		d.inpDVU2.tag.setInt(1);
		exec(17, 0, 2, 0, 0, 1, 0, 0, 1, 1, 1, 0, 0b00000000_00000000, 0b00000000_00000000, WAIT_TIMER);
		exec(17, 0, 1, 0, 0, 1, 0, 0, 1, 1, 1, 0, 0b00000000_00000000, 0b00000000_00000000, WAIT_TIMER);

		// STATE_UNLOAD_EMPTY
		d.inpDVU2.tag.setInt(0);

		// STATE_UNLOAD_VIBRATE
		d.inpDVU2.tag.setInt(0);
		exec(19, 0, 3, 0, 0, 1, 0, 0, 1, 1, 1, 1, 0b00000000_00000000, 0b00000000_00000000, WAIT_TIMER);
		exec(19, 0, 2, 0, 0, 1, 0, 0, 1, 1, 1, 1, 0b00000000_00000000, 0b00000000_00000000, WAIT_TIMER);
		exec(19, 0, 1, 0, 0, 1, 0, 0, 1, 1, 1, 1, 0b00000000_00000000, 0b00000000_00000000, WAIT_TIMER);

		// STATE_UNLOAD_FINISH
		exec(20, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0b00001111_00000000, 0b00000000_00000000, 0);
	}



	@Test
	public void test_warm() {

		d.tagBtnStart.setInt(1);
		nextState(1);
		nextState(2);
		nextState(3);
		nextState(4);
		nextState(5);

		d.inpP.tag.setInt(5);
		d.inpPSensorOk.tag.setInt(1);
		d.tagTimeWarm.setInt(5);
		d.tagPBlow.setInt(20);
		exec(5, 0,2,  0,1,0,0,0,1,1,  0,0, 0b00000000_00000000, 0b00000000_00000000, WAIT_TIMER);
		exec(5, 0,1,  0,1,0,0,0,1,1,  0,0, 0b00000000_00000000, 0b00000000_00000000, WAIT_TIMER);
//		exec(5, 0,0,  0,1,0,0,0,1,1,  0,0, 0b00000000_00000000, 0b00000000_00000000, WAIT_TIMER);


		// STATE_LOAD_FINISH
		exec(6, 0,0,  0,0,0,0,0,1,0,  0,0, 0b00101111_00000000, 0b00000000_00000000, 0);
		exec(6, 0,-1,  0,0,0,0,0,1,0,  0,0, 0b00101111_00000000, 0b00000000_00000000, 0);

		// STATE_BEFORE_INFLATE
		d.inpClosedK1.tag.setInt(1);
		d.inpClosedK2.tag.setInt(1);
		d.inpClosedK3.tag.setInt(1);
		d.inpClosedK4.tag.setInt(1);
		d.inpClosedK6.tag.setInt(1);

		d.inpOpenedK1.tag.setInt(0);
		d.inpOpenedK2.tag.setInt(0);

		exec(7, 0,-1,  0, 0,0,0,0,1,0,  0,0, 0b00000000_00000000, 0b00000000_00000000, WAIT_CAN_INFL + WAIT_PLINE_OK);
		d.inpPLineOk.tag.setInt(1);
		exec(7, 0,-1,  0, 0,0,0,0,1,0,  0,0, 0b00000000_00000000, 0b00000000_00000000, WAIT_CAN_INFL);

		// STATE_INFLATE_WARM
		d.inpCanInfl.tag.setInt(1);
		exec(8, 0, 5,  0, 0,0,1,0,1,0,  0,0, 0b00000000_00000000, 0b00000000_00000000, WAIT_TIMER);
		exec(8, 0, 4,  0, 0,0,1,0,1,0,  0,0, 0b00000000_00000000, 0b00000000_00000000, WAIT_TIMER);
		exec(8, 0, 3,  0, 0,0,1,0,1,0,  0,0, 0b00000000_00000000, 0b00000000_00000000, WAIT_TIMER);
		exec(8, 0, 2,  0, 0,0,1,0,1,0,  0,0, 0b00000000_00000000, 0b00000000_00000000, WAIT_TIMER);
		exec(8, 0, 1,  0, 0,0,1,0,1,0,  0,0, 0b00000000_00000000, 0b00000000_00000000, WAIT_TIMER);

		// STATE_INFLATE
		exec(10, 0, 7,  0, 0,0,1,0,1,0,  0,0, 0b00000000_00000000, 0b00000000_00000000, WAIT_PBLOW);
	}




}



















