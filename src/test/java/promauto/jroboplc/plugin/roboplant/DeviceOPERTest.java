package promauto.jroboplc.plugin.roboplant;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static promauto.jroboplc.plugin.roboplant.AuxProcs.newInputAndTag;

import java.nio.file.Paths;

import org.junit.Before;
import org.junit.Test;

import promauto.jroboplc.core.api.InitUtils;
import promauto.jroboplc.core.tags.TagInt;

public class DeviceOPERTest {
	private DeviceOPER d;
	
	@Before
	public void setUp() throws Exception {
		InitUtils.setupLogger();

		d = new DeviceOPER();
		d.inpInput1 	= newInputAndTag();
		d.inpInput2 	= newInputAndTag();
		
		d.tagOutput		= new TagInt("", 0);
		d.tagOper		= new TagInt("", 0);
	}

	@Test
	public void testLoad() throws Exception {
		RoboplantModule robo = AuxProcs.createRoboplantModule();
		assertTrue( robo.loadProject( Paths.get( 
				InitUtils.getResourcesDir("roboplant", RoboplantModuleTest.class) + "oper.xml" )));

		assertEquals(1, robo.devicesByAddr.length);
		d = (DeviceOPER)robo.devicesByAddr[0];
		assertTrue( d.init(robo) );
		assertTrue( d.prepare() );

		assertEquals( "OPER", d.devtype );
		assertEquals( "S05_501A", d.name );
		assertEquals( "S05_501A", d.tagname );
	}
	
	private void set(
			int oper,
			int input1,
			int input2
			) {
		d.tagOper.setInt(oper);
		d.inpInput1.tag.setInt(input1);
		d.inpInput2.tag.setInt(input2);
	}

	
	private void exec(
			int output 
			) {
		d.execute();
		assertEquals("output"	, output	 , d.tagOutput.getInt());     
	}


	@Test
	public void testExecute() {
		set(0, 0,0); 	exec(1); // =
		set(0, 1,0); 	exec(0); // =
		set(0, 0,2); 	exec(0); // =

		set(1, 2,2); 	exec(1); // >=
		set(1, 2,3); 	exec(0); // >=
		set(1, 3,2); 	exec(1); // >=
		
		set(2, 1,1); 	exec(1); // <=
		set(2, 2,3); 	exec(1); // <=
		set(2, 3,2); 	exec(0); // <=

		set(3, 1,1); 	exec(0); // >
		set(3, 2,3); 	exec(0); // >
		set(3, 3,2); 	exec(1); // >

		set(4, 1,1); 	exec(0); // <
		set(4, 2,3); 	exec(1); // <
		set(4, 3,2); 	exec(0); // <

		set(5, 1,1); 	exec(2); // sum
		set(5, 2,3); 	exec(5); // sum
		set(5, 3,2); 	exec(5); // sum

		set(6, 1,1); 	exec(0); // sub
		set(6, 2,3); 	exec(0);// sub
		set(6, 3,2); 	exec(1); // sub

		set(7, 1,1); 	exec(0); // xor
		set(7, 2,3); 	exec(1); // xor
		set(7, 3,2); 	exec(1); // xor

		set(8, 1,1); 	exec(1); // mul
		set(8, 2,3); 	exec(6); // mul
		set(8, 4,2); 	exec(8); // mul

		set(9, 1,1); 	exec(1); // div
		set(9, 6,3); 	exec(2); // div
		set(9, 3,2); 	exec(1); // div
		set(9, 4,3); 	exec(1); // div

		set(10, 1,1); 	exec(0); // mod
		set(10, 5,3); 	exec(2); // mod
		set(10, 3,2); 	exec(1); // mod

		set(11, 1,0); 	exec(0); // and
		set(11, 2,3); 	exec(2); // and
		set(11, 3,3); 	exec(3); // and

		set(12, 1,0); 	exec(1); // or
		set(12, 2,3); 	exec(3); // or
		set(12, 4,2); 	exec(6); // or

		set(13, 1,1); 	exec(0); // %
		set(13, 2,3); 	exec(0); // %
		set(13, 3,2); 	exec(0); // %

		set(14, 1,1); 	exec(1); // (inp1&inp2)==inp2
		set(14, 2,3); 	exec(0); // (inp1&inp2)==inp2
		set(14, 7,3); 	exec(1); // (inp1&inp2)==inp2
	}

}
