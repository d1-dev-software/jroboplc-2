package promauto.jroboplc.plugin.roboplant;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static promauto.jroboplc.plugin.roboplant.AuxProcs.createInput;
import static promauto.jroboplc.plugin.roboplant.AuxProcs.createOutput;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import org.mockito.MockitoAnnotations;
import promauto.jroboplc.core.api.InitUtils;
import promauto.jroboplc.core.api.SampleEnvironment;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.Flags;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.TagTable;
import promauto.jroboplc.plugin.roboplant.Device.RefBool;

public class DeviceTest {
	private Device d;
	
	@Mock private RoboplantModule robo;
	@Mock private TagTable ttbl; 
	@Mock private Tag tagOutput;
	@Mock private Tag tagOutput_1;
	@Mock private Tag tagValue;
	@Mock private Tag tagConst1;
	@Mock private Tag tagConst2;
	@Mock private Tag tagEmpty;
	@Mock private Tag tagUnknown;
	

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.openMocks(this);
		InitUtils.setupLogger();
		
		SampleEnvironment env = new SampleEnvironment();
		EnvironmentInst.set(env);

		d = new Device();
		
		d.module = mock(RoboplantModule.class);
		when(d.module.getPlugin()).thenReturn( mock(RoboplantPlugin.class) );
		
		when(robo.getTagTable()).thenReturn(ttbl);

		mockTag(tagOutput, 	"MDTA_508_Output", 	0);
		mockTag(tagOutput_1,"MDTA_508_Output_1",10);
		mockTag(tagValue, 	"MDTA_508_Value", 	10);
		mockTag(tagConst1, 	"MDTA_508@Const1_1", 20);
		mockTag(tagConst2, 	"MDTA_508@Const2_2", 30);
		mockTag(tagEmpty, 	"MDTA_508@Empty_3", 	0);
		mockTag(tagUnknown, "MDTA_508@BADREF_Unknown",	0);

	}
	
	private void mockTag(Tag tag, String name, int value) {
		when(tag.getName()).thenReturn(name);
		when(tag.getInt()).thenReturn(value);
		when(ttbl.createInt(tag.getName(), tag.getInt() )).thenReturn(tag);
		when(ttbl.createInt(tag.getName(), tag.getInt(), 0 )).thenReturn(tag);
		when(ttbl.createInt(tag.getName(), tag.getInt(), Flags.HIDDEN )).thenReturn(tag);
		when(ttbl.createTag(Tag.Type.INT, tag.getName() )).thenReturn(tag);
	}
	
	
	

	@Test
	public void testSetAttribute() {
		assertEquals(null, d.devtype);
		assertEquals(null, d.tagname);
		assertEquals(null, d.name);
		assertEquals(0, d.addr);
		assertEquals(0, d.order);
		
		d.setAttribute("DevType", 	"MDTA");
		d.setAttribute("TagName", 	"508_Tok");
		d.setAttribute("Name", 	"508 Нория Ток");
		d.setAttribute("Addr", 	"0123");
		d.setAttribute("Order", 	"ABCD");

		assertEquals("MDTA", d.devtype);
		assertEquals("508_Tok", d.tagname);
		assertEquals("508 Нория Ток", d.name);
		assertEquals(0x0123, d.addr);
		assertEquals(0xABCD, d.order);
	}

	
	
	@Test
	public void testAddComp() {
		assertEquals(0, d.inputs.size());
		assertEquals(0, d.outputs.size());

		Comp comp = d.addComp(false);
		
		assertSame( Output.class, comp.getClass());
		assertEquals(0, d.inputs.size());
		assertEquals(1, d.outputs.size());
		assertSame( comp, d.outputs.get(0) );
	}

	
	
	@Test
	public void testInit_tagname() {
		d.addr = 0xFF;
		assertEquals(null, d.tagname);
		
		d.init(robo);
		
		assertEquals("T00FF", d.tagname);
	}


	
	
	
	@Test
	public void testInit_outputs() {
		d.devtype = "MDTA";
		d.tagname = "508";
		
		Output output1 = createOutput(d, "Output", 0, 0);
		Output output2 = createOutput(d, "Value", 10, 0);

		
		d.init(robo);
		
		assertSame( tagOutput, output1.tag);
		assertEquals( "MDTA_508_Output", output1.tag.getName() );
		
		assertSame( tagValue, output2.tag);
		assertEquals( "MDTA_508_Value", output2.tag.getName() );
	}


	@Test
	public void testInit_outputs_dblnames() {
		when(ttbl.get(anyString())).thenReturn(null).thenReturn(tagOutput).thenReturn(null);

		d.devtype = "MDTA";
		d.tagname = "508";
		
		Output output1 = createOutput(d, "Output", 0, 0);
		Output output2 = createOutput(d, "Output", 10, 0);
		
		
		d.init(robo);
		
		assertSame( tagOutput, output1.tag);
		assertEquals( "MDTA_508_Output", output1.tag.getName() );
		
		assertSame( tagOutput_1, output2.tag);
		assertEquals( "MDTA_508_Output_1", output2.tag.getName() );
		
	}

	

	@Test
	public void testInit_inputs() {
		d.devtype = "MDTA";
		d.tagname = "508";
		
		Input input1 = createInput(d, "Input", 255, 64);
		Input input2 = createInput(d, "Const1", 0xFFFF, 20);
		Input input3 = createInput(d, "Const2", 0xFFFF, 30);
		Input input4 = createInput(d, "Empty",  0x7000, 40);
		
		
		d.init(robo);
		
		assertSame( null, input1.tag);

		assertEquals( "MDTA_508@Const1_1", input2.tag.getName() );
		assertSame( tagConst1, input2.tag);
		
		assertEquals( "MDTA_508@Const2_2", input3.tag.getName() );
		assertSame( tagConst2, input3.tag);
		
		assertEquals( "MDTA_508@Empty_3", input4.tag.getName() );
		assertEquals( 0, input4.tag.getInt() );
		assertSame( tagEmpty, input4.tag);
	}

	
	
	
	
	
	@Test
	public void testPrepareRefs() {
		d.devtype = "MDTA";
		d.tagname = "508";
		d.addr = 255;
		d.module = robo;
		
		Output output1 = createOutput(d, "Output", 0, 3);
		Output output2 = createOutput(d, "Value", 10, 5);
		output1.tag = tagOutput; 
	    output2.tag = tagValue;

		Input input1 = createInput(d, "Input", 255, 3);
		Input input2 = createInput(d, "Unknown", 0, 0);
		Input input3 = createInput(d, "Const2", 0xFFFF, 30);
		Input input4 = createInput(d, "Empty",  0x7000, 40);
		input3.tag = tagConst2;
		input4.tag = tagEmpty;
		
		when(robo.getDevice(255)).thenReturn(d);
		when(robo.getDevice(0)).thenReturn(null);

		assertSame( null, input1.tag);
		assertSame( null, input2.tag);
		

		assertTrue(d.prepare());
		
		assertSame( tagOutput, 	input1.tag);
		assertSame( tagUnknown, input2.tag);
	}
	
	@Test
	public void testInputSetAttribute() {
		Input input = new Input();
		assertEquals(null, input.name);
		assertEquals(0, input.refaddr);
		assertEquals(0, input.refnum);
		assertEquals(false, input.invertor);
		
		input.setAttribute("Name", 	"Input 1.1-2 A");
		input.setAttribute("RefAddr", 	"10");
		input.setAttribute("RefNum", 	"20");
		input.setAttribute("Invertor", 	"1");

		assertEquals("Input_1.1-2_A", input.name);
		assertEquals(0x10, input.refaddr);
		assertEquals(0x20, input.refnum);
		assertEquals(true, input.invertor);
	}
	
	@Test
	public void testOutputSetAttribute() {
		Output output = new Output();
		assertEquals(null, output.name);
		assertEquals(0, output.num);
		assertEquals(0, output.inival);
		assertEquals(false, output.initiated);
		assertEquals(false, output.istag);
		assertEquals(false, output.saveable);
		
		output.setAttribute("Name", 	"Out put 1.1-2 A");
		output.setAttribute("Num",	 	"10");
		output.setAttribute("IniVal", 	"20");
		output.setAttribute("Initiated","1");
		output.setAttribute("IsTag", 	"1");
		output.setAttribute("TagSaveable", "1");

		assertEquals("Out_put_1.1-2_A", output.name);
		assertEquals(0x10, output.num);
		assertEquals(32, output.inival);
		assertEquals(true, output.initiated);
		assertEquals(true, output.istag);
		assertEquals(true, output.saveable);
	}
	
	
	@Test
	public void testInputIsConst() {
		Input inp = new Input();
		
		inp.refaddr = 0;
		inp.refnum = 0;
		assertFalse( inp.isConst() );
		
		inp.refaddr = 100;
		inp.refnum = 7;
		assertFalse( inp.isConst() );
		
		inp.refaddr = 0x7000;
		inp.refnum = 0;
		assertTrue( inp.isConst() );
		
		inp.refaddr = 0xFFFF;
		inp.refnum = 10;
		assertTrue( inp.isConst() );
		
	}

	
	
	@Test
	public void testInputGetInt() {
		Input inp = new Input();
		
		inp.invertor = false;
		inp.tag = tagValue;
		assertEquals( 10, inp.getInt() );
		
		inp.invertor = true;
		inp.tag = tagValue;
		assertEquals( 0, inp.getInt() );
		
		inp.invertor = false;
		inp.tag = tagOutput;
		assertEquals( 0, inp.getInt() );
		
		inp.invertor = true;
		inp.tag = tagOutput;
		assertEquals( 1, inp.getInt() );
	}
	
	@Test
	public void testSortInputByName() {
		Input inp1 = new Input("aaa");
		Input inp2 = new Input("bbb");
		Input inp3 = new Input("ccc");
		
		d.inputs.add(inp2);
		d.inputs.add(inp3);
		d.inputs.add(inp1);
		
		d.sortInputOutputArrays();
		
		assertSame(inp1, d.inputs.get(0));
		assertSame(inp2, d.inputs.get(1));
		assertSame(inp3, d.inputs.get(2));
	}
	
	@Test
	public void testSortOutputByName() {
		Output out1 = new Output("aaa");
		Output out2 = new Output("bbb");
		Output out3 = new Output("ccc");
		
		d.outputs.add(out2);
		d.outputs.add(out3);
		d.outputs.add(out1);
		
		d.sortInputOutputArrays();
		
		assertSame(out1, d.outputs.get(0));
		assertSame(out2, d.outputs.get(1));
		assertSame(out3, d.outputs.get(2));
	}

	@Test
	public void testSortOutputByNum() {
		Output out1 = new Output(1);
		Output out2 = new Output(2);
		Output out3 = new Output(3);
		
		d.outputs.add(out2);
		d.outputs.add(out3);
		d.outputs.add(out1);
		
		d.sortInputOutputArrays();
		
		assertSame(out1, d.outputsByNum.get(0));
		assertSame(out2, d.outputsByNum.get(1));
		assertSame(out3, d.outputsByNum.get(2));
	}
	
	@Test
	public void testGetInputByName() {
		d.inputs.add(new Input("aaa"));
		d.inputs.add(new Input("bbb"));
		d.inputs.add(new Input("ccc"));

		assertSame(d.inputs.get(0), d.getInput("aaa"));
		assertSame(d.inputs.get(1), d.getInput("bbb"));
		assertSame(d.inputs.get(2), d.getInput("ccc"));
	}	

	@Test
	public void testGetOutputByName() {
		d.outputs.add(new Output("aaa"));
		d.outputs.add(new Output("bbb"));
		d.outputs.add(new Output("ccc"));

		assertSame(d.outputs.get(0), d.getOutput("aaa"));
		assertSame(d.outputs.get(1), d.getOutput("bbb"));
		assertSame(d.outputs.get(2), d.getOutput("ccc"));
	}	

	@Test
	public void testGetOutputByNum() {
		d.outputsByNum.add(new Output(11));
		d.outputsByNum.add(new Output(22));
		d.outputsByNum.add(new Output(33));

		assertSame(d.outputsByNum.get(0), d.getOutput(11) );
		assertSame(d.outputsByNum.get(1), d.getOutput(22) );
		assertSame(d.outputsByNum.get(2), d.getOutput(33) );
	}
	
	
	
	@Test
	public void testGetOutputTagByName() {
		d.outputs.add(new Output("aaa"));
		d.outputs.add(new Output("bbb"));
		d.outputs.add(new Output("ccc"));
		
		d.outputs.get(0).tag = tagOutput;
		d.outputs.get(1).tag = tagValue;
		
		RefBool res = new RefBool(true);

		assertSame(tagOutput, d.getOutputTag("aaa", res));
		assertTrue(res.value);

		assertSame(null, d.getOutputTag("qqq", res));
		assertFalse(res.value);
		
		assertSame(tagValue, d.getOutputTag("bbb", res));
		assertFalse(res.value);
	}


	@Test
	public void testGetOutputTagByNum() {
		d.outputsByNum.add(new Output(11));
		d.outputsByNum.add(new Output(22));
		d.outputsByNum.add(new Output(33));
		
		d.outputsByNum.get(0).tag = tagOutput;
		d.outputsByNum.get(1).tag = tagValue;
		
		RefBool res = new RefBool(true);

		assertSame(tagOutput, d.getOutputTag(11, res));
		assertTrue(res.value);

		assertSame(null, d.getOutputTag(12, res));
		assertFalse(res.value);
		
		assertSame(tagValue, d.getOutputTag(22, res));
		assertFalse(res.value);
	}


}
