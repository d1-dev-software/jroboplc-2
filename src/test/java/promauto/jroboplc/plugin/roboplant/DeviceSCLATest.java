package promauto.jroboplc.plugin.roboplant;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static promauto.jroboplc.plugin.roboplant.AuxProcs.newInputAndTag;

import java.nio.file.Paths;

import org.junit.Before;
import org.junit.Test;

import promauto.jroboplc.core.api.InitUtils;
import promauto.jroboplc.core.tags.TagInt;

public class DeviceSCLATest {
	private DeviceSCLA d;
	
	@Before
	public void setUp() throws Exception {
		InitUtils.setupLogger();

		d = new DeviceSCLA();
		d.inpValue 	= newInputAndTag();
		
		d.tagValue		= new TagInt("", 0);
		d.tagPrimeval	= new TagInt("", 0);
		d.tagPercent	= new TagInt("", 0);
		d.tagMax		= new TagInt("", 0);
		d.tagMin		= new TagInt("", 0);
		d.tagPointV1  	= new TagInt("", 0);
		d.tagPointP1  	= new TagInt("", 0);
		d.tagPointV2  	= new TagInt("", 0);
		d.tagPointP2  	= new TagInt("", 0);
		d.tagSPmax    	= new TagInt("", 0);
		d.tagSPmin    	= new TagInt("", 0);
		d.tagBlok     	= new TagInt("", 0);
		d.tagBlokValue	= new TagInt("", 0);
		d.tagDlyMax   	= new TagInt("", 0);
		d.tagDlyMin   	= new TagInt("", 0);
		
		d.resetState();
	}

	@Test
	public void testLoad() throws Exception {
		RoboplantModule robo = AuxProcs.createRoboplantModule();
		assertTrue( robo.loadProject( Paths.get( 
				InitUtils.getResourcesDir("roboplant", RoboplantModuleTest.class) + "scla.xml" )));

		assertEquals(1, robo.devicesByAddr.length);
		d = (DeviceSCLA)robo.devicesByAddr[0];
		assertTrue( d.init(robo) );
		assertTrue( d.prepare() );

		assertEquals( "SCLA", d.devtype );
		assertEquals( "301", d.name );
		assertEquals( "301", d.tagname );
	}
	
	private void set(
			int value
			) {
		d.inpValue.tag.setInt(value);
	}

	
	private void exec(
			int value		,
			int primeval	,
			int percent	    ,
			int max		    ,
			int min		
			) {
		d.execute();
		assertEquals("value"		, value		 , d.tagValue	.getInt());     
		assertEquals("primeval"		, primeval	 , d.tagPrimeval.getInt());     
		assertEquals("percent"		, percent	 , d.tagPercent	.getInt());     
		assertEquals("max"			, max		 , d.tagMax		.getInt());     
		assertEquals("min"			, min		 , d.tagMin		.getInt());     
	}


	@Test
	public void testExecute() {
		
		d.tagPointV1 .setInt(0);
		d.tagPointP1 .setInt(100);
		d.tagPointV2 .setInt(50);
		d.tagPointP2 .setInt(200);
		d.tagSPmin   .setInt(0);
		d.tagSPmax   .setInt(50);

		set(50); 	exec(	0,		50,		0,  	0,1);
		set(100); 	exec(	0,		100,	0,  	0,1);
		set(150); 	exec(	25,		150,	50,  	0,0);
		set(200); 	exec(	50,		200,	100,  	1,0);
		set(250); 	exec(	50,		250,	100,  	1,0);
		
		d.tagBlok.setInt( 1 );
		set(250); 	exec(	0,		250,	0,  	0,1);
		d.tagBlokValue.setInt( 25 );
		set(250); 	exec(	25,		250,	50,  	0,0);
		
		d.tagBlok.setInt( 2 );
		d.tagBlokValue.setInt( 150 );
		set(250); 	exec(	25,		150,	50,  	0,0);
	}

}
