package promauto.jroboplc.plugin.roboplant;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static promauto.jroboplc.plugin.roboplant.AuxProcs.newInputAndTag;

import java.nio.file.Paths;

import org.junit.Before;
import org.junit.Test;

import promauto.jroboplc.core.api.InitUtils;
import promauto.jroboplc.core.tags.TagInt;

public class DeviceTRADTest {
	private DeviceTRAD d;
	
	@Before
	public void setUp() throws Exception {
		InitUtils.setupLogger();

		d = new DeviceTRAD();
		d.inpMasterIn 	= newInputAndTag();
		d.inpTrigSet 	= newInputAndTag();
		d.inpEnable.add( newInputAndTag() );
		d.inpEnable.add( newInputAndTag() );
		
		d.tagOutput		= new TagInt("", 0);
		d.tagReady 		= new TagInt("", 0);
	}

	@Test
	public void testLoad() throws Exception {
		RoboplantModule robo = AuxProcs.createRoboplantModule();
		assertTrue( robo.loadProject( Paths.get( 
				InitUtils.getResourcesDir("roboplant", RoboplantModuleTest.class) + "trad.xml" )));

		assertEquals(1, robo.devicesByAddr.length);
		d = (DeviceTRAD)robo.devicesByAddr[0];
		assertTrue( d.init(robo) );
		assertTrue( d.prepare() );

		assertEquals( "TRAD", d.devtype );
		assertEquals( "S01_on", d.name );
		assertEquals( "S01_on", d.tagname );
	}
	
	private void set(
			int masterin , 
			int trigset  ,
			int enab1    ,
			int enab2
			) {
		d.inpMasterIn.tag.setInt(		masterin);
		d.inpTrigSet.tag.setInt(		trigset );
		d.inpEnable.get(0).tag.setInt(  enab1   );
		d.inpEnable.get(1).tag.setInt(  enab2   );
	}

	
	private void exec(
			int output , 
			int ready 
			) {
		d.execute();
		assertEquals("output"	, output	 , d.tagOutput.getInt());     
		assertEquals("ready"	, ready		 , d.tagReady.getInt());     
	}


	@Test
	public void testExecute() {
		set(0,0, 0,0); 	exec(0,0);
		set(0,0, 1,1); 	exec(0,0);
		set(0,1, 1,1); 	exec(0,0);
		set(1,0, 1,1); 	exec(1,0);
		set(0,0, 1,1); 	exec(0,0);
		set(1,0, 1,1); 	exec(1,0);
		set(1,0, 1,0); 	exec(0,0);
		set(1,0, 1,1); 	exec(0,1);
		set(0,0, 1,1); 	exec(0,0);
		set(1,0, 1,1); 	exec(0,1);
		set(1,1, 1,1); 	exec(1,0);
		set(1,0, 1,1); 	exec(1,0);
		set(0,0, 0,0); 	exec(0,0);
	}
}
