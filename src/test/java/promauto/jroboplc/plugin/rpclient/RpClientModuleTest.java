package promauto.jroboplc.plugin.rpclient;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.SocketException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;

import org.junit.Before;
import org.junit.Test;

import promauto.jroboplc.core.api.InitUtils;
import promauto.jroboplc.core.api.SampleEnvironment;
import promauto.jroboplc.core.api.SamplePlugin;
import promauto.jroboplc.core.CmdDispatcherImpl;
import promauto.jroboplc.core.ConfigurationYaml;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.Plugin;
import promauto.jroboplc.core.api.Tag;

public class RpClientModuleTest {
	private final static Charset charset = Charset.forName("UTF-8");
	
	public static class SampleRpClientModule extends RpClientModule {
		public boolean socketConnected = false;
		public ByteArrayOutputStream out = new ByteArrayOutputStream();
		public Queue<String> in = new LinkedList<>();

		public SampleRpClientModule(Plugin plugin, String name) {
			super(plugin, name);
	    	socketWriter = new BufferedWriter(new OutputStreamWriter(out, charset));
	    	streamReader = new ByteArrayInputStream( new byte[0] );
		}
		
		public String getOutput() {
			return new String(out.toByteArray(), charset);
		}

		@Override
		protected void connectSocket() throws SocketException, IOException {
			socketConnected = true;
		}

		@Override
		protected void disconnectSocket() throws IOException {
			socketConnected = false;
		}
		
		protected boolean isConnected() {
			return socketConnected;
		}

		
		protected void clearSockerReader() throws IOException {
		}

		@Override
		protected boolean readBuffin() throws IOException {
			String s = in.poll();
			if( s == null ) 
				return false;

			buffinBeg = 0;
			buffinEnd = s.length();
			
			byte[] src = s.getBytes();
			for(int i=0; i<buffinEnd; i++)
				buffin[i] = src[i];
				
			return true;
		}

		

		
	}
	
	private SampleRpClientModule m;
	

	@Before
	public void setUp() throws Exception {
		InitUtils.setupLogger();

		SampleEnvironment env = new SampleEnvironment();
		EnvironmentInst.set(env);
		
		env.setConfiguration( new ConfigurationYaml("") );
		env.setCmdDispatcher( new CmdDispatcherImpl() );

		SamplePlugin plug = new SamplePlugin();

		m = new SampleRpClientModule(plug, "rpcl");

		HashMap<String,String> conf = new HashMap<String,String>();
		m.load(conf);

	}
	

	@Test
	public void testExecute() throws Exception {
		m.in.add("100-Roboplant OPC Server (TCP)");
		m.in.add("101 OK");
		m.in.add("103 B");
		m.in.add("104 B#0!TRM1.LvoP.STAT;TRM1.LvoP.PV1;TRM1.LvoP.PV2;TRM1.LvoP.LUPV1;TRM1.LvoP.LUPV2;TRM1.SYSTEM.ErrorCount;TRM1.SYSTEM.ErrorFlag;TRM1.SYSTEM.NetAddr;TRM1.SYSTEM.TimePeriod;TRM1.SYSTEM.UpdateDate;TRM1.SYSTEM.UpdateTime=DBDD");
		m.in.add("105 4");
		m.in.add("118 983583A1");
		m.in.add("106 B#0!;1;;;;;;;1D7;1337AF4;22FD8=8938");
		m.in.add("116 B#0!;1;;;;;;;;;=EE36");
		m.in.add("105 2");
		m.in.add("118 5074eb81");
		m.in.add("107 2#8!16D#A!23034=82DC");
		m.in.add("119 ");
		m.in.add("117 !");
		m.in.add("105 2");
		m.in.add("118 5074eb81");
		m.in.add("107 2#8!16D#A!23034=82DC");
		
		m.prepare();
		assertTrue( m.connected );
		assertEquals(11, m.tags.length);
		
		Tag tag = m.getTagTable().get("TRM1.LvoP.PV1");
		assertNotNull(tag);
		assertEquals(1, tag.getInt());
		Tag auxtag = tag.getObject();
		assertNotNull( auxtag );
		assertEquals("IRR", auxtag.getName() );
		
		tag = m.getTagTable().get("TRM1.SYSTEM.UpdateTime");
		assertNotNull(tag);
		assertEquals(0x22FD8, tag.getInt());
		
		
		
		
		m.execute();
		assertTrue( m.connected );
		assertEquals(0x23034, tag.getInt());

		auxtag.setInt(1);
		m.execute();
		assertTrue( m.connected );

		
		String[] out = m.getOutput().split("\r\n");
		int i=0;
		assertEquals("SETFILTER *", 	out[i++]);
		assertEquals("CREATETAGLIST", 	out[i++]);
		assertEquals("GETTAGLIST 0", 	out[i++]);
		assertEquals("FIXALL", 			out[i++]);
		assertEquals("GETCRC", 			out[i++]);
		assertEquals("GETALL 0", 		out[i++]);
		assertEquals("GETPROPS 0", 		out[i++]);
		assertEquals("FIXALL", 			out[i++]);
		assertEquals("GETCRC", 			out[i++]);
		assertEquals("GETCHG 0", 		out[i++]);
		assertEquals("GETMSG", 			out[i++]);
		assertEquals("SETFLAG 1 1 1BF8",out[i++]);
		assertEquals("FIXALL", 			out[i++]);
		assertEquals("GETCRC", 			out[i++]);
		assertEquals("GETCHG 0", 		out[i++]);
		
	}

}
