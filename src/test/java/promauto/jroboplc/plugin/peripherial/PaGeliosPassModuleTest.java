package promauto.jroboplc.plugin.peripherial;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;

import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;

import promauto.jroboplc.core.api.InitUtils;
import promauto.jroboplc.core.api.SampleEnvironment;
import promauto.jroboplc.core.api.SamplePlugin;
import promauto.jroboplc.core.ConfigurationYaml;
import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.EnvironmentInst;

public class PaGeliosPassModuleTest {
	private static String CFG_DIR = InitUtils.getResourcesDir("peripherial", PeripherialModule.class);

	PaGeliosPassModule m;
	SampleProtocolAA55 prot;


	@Before
	public void setUp() throws Exception {
		InitUtils.setupLogger();

		SampleEnvironment env = new SampleEnvironment();
		EnvironmentInst.set(env);

		Configuration conf = new ConfigurationYaml(CFG_DIR);
		if (!conf.load())
			fail();
		env.setConfiguration(conf);

		SamplePlugin plug = new SamplePlugin();

		m = new PaGeliosPassModule(plug, "gelios");
		m.load(new HashMap<String, String>());
		m.retrial = 3;
		m.netaddr = 1;

		prot = new SampleProtocolAA55(m);
		m.protocol = prot;
	}


	@Test
	public void testExecute() {
		prot.buffin.push(new int[]{
				0x00, 0x00, 0x00, 0x20, // SumNum
				0x00, 0x00, 0xFF, 0xFF, // SumWeight
				0x00, 0x01, 0x00, 0x00, // CurWeight
				0xFF, 0xFF, 0xFF, 0xFF, // LastWeight
				0x7F, 0xFF, 0xFF, 0xFF, // LastTime
				0x00,                    // State
				0x80,                    // ErrorCode
				0xFF});

		m.execute();

		assertFalse(m.tagError.getBool());
		assertEquals(0, m.tagErrorCnt.getInt());


		int[] expout = new int[]{0x55, 0x21, 0x7D};
		int[] actout = prot.buffout.pollLast();
		assertArrayEquals("Check data to send", expout, actout);

		assertEquals(0, m.tagSumNumHigh1.getInt());
		assertEquals(32, m.tagSumNumLow1.getInt());

		assertEquals(0, m.tagSumWeightHigh1.getInt());
		assertEquals(65535, m.tagSumWeightLow1.getInt());

		assertEquals(1, m.tagCurWeightHigh.getInt());
		assertEquals(0, m.tagCurWeightLow.getInt());

		assertEquals(65535, m.tagLastWeightHigh.getInt());
		assertEquals(65535, m.tagLastWeightLow.getInt());

		assertEquals(32767, m.tagLastTimeHigh.getInt());
		assertEquals(65535, m.tagLastTimeLow.getInt());

		assertEquals(0x00, m.tagState.getInt());
		assertEquals(0x80, m.tagErrorCode.getInt());


		m.tagSetCmd.setInt(1);
		prot.buffin.push(new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0x00, 0, 0});
		m.execute();
		assertArrayEquals(new int[]{0x55, 0x21, 0x7D}, prot.buffout.pollLast());

		m.tagSetCmd.setInt(1);
		m.tagSetCmdCtrl.setInt(1);
		prot.buffin.push(new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0x00, 0, 0});
		m.execute();
		assertArrayEquals(new int[]{0x55, 0x01, 0x01, 154}, prot.buffout.pollLast());
		assertArrayEquals(new int[]{0x55, 0x21, 0x7D}, prot.buffout.pollLast());

		prot.buffin.push(new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0x00, 0, 0});
		m.execute();
		assertArrayEquals(new int[]{0x55, 0x21, 0x7D}, prot.buffout.pollLast());

		m.tagSetCmd.setInt(0);
		prot.buffin.push(new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0x00, 0, 0});
		m.execute();
		assertArrayEquals(new int[]{0x55, 0x01, 0x00, 196}, prot.buffout.pollLast());
		assertArrayEquals(new int[]{0x55, 0x21, 0x7D}, prot.buffout.pollLast());

		prot.buffin.push(new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0x00, 0, 0});
		m.execute();
		assertArrayEquals(new int[]{0x55, 0x21, 0x7D}, prot.buffout.pollLast());


		m.tagSetCmdCtrl.setInt(3);
		prot.buffin.push(new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0x00, 0, 0});
		m.execute();
		assertArrayEquals(new int[]{0x55, 0x01, 0x01, 154}, prot.buffout.pollLast());
		assertArrayEquals(new int[]{0x55, 0x21, 0x7D}, prot.buffout.pollLast());

		prot.buffin.push(new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0x01, 0, 0});
		m.execute();
		assertArrayEquals(new int[]{0x55, 0x01, 0x01, 154}, prot.buffout.pollLast());
		assertArrayEquals(new int[]{0x55, 0x21, 0x7D}, prot.buffout.pollLast());

		prot.buffin.push(new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0x01, 0, 0});
		m.execute();
		assertArrayEquals(new int[]{0x55, 0x21, 0x7D}, prot.buffout.pollLast());

		prot.buffin.push(new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0x00, 0, 0});
		m.execute();
		assertArrayEquals(new int[]{0x55, 0x21, 0x7D}, prot.buffout.pollLast());

		prot.buffin.push(new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0x01, 0, 0});
		m.execute();
		assertArrayEquals(new int[]{0x55, 0x01, 0x01, 154}, prot.buffout.pollLast());
		assertArrayEquals(new int[]{0x55, 0x21, 0x7D}, prot.buffout.pollLast());

		prot.buffin.push(new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0x01, 0, 0});
		m.execute();
		assertArrayEquals(new int[]{0x55, 0x21, 0x7D}, prot.buffout.pollLast());


		m.tagSetCmdCtrl.setInt(2);
		prot.buffin.push(new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0x00, 0, 0});
		m.execute();
		assertArrayEquals(new int[]{0x55, 0x01, 0x02, 120}, prot.buffout.pollLast());
		assertArrayEquals(new int[]{0x55, 0x21, 0x7D}, prot.buffout.pollLast());

		prot.buffin.push(new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0x00, 0, 0});
		m.execute();
		assertArrayEquals(new int[]{0x55, 0x21, 0x7D}, prot.buffout.pollLast());

	}


	@Test
	public void testExecuteExtra() {

		m.addExtraTag("NumLock", 7);
		m.addExtraTag("StopHold", 32);

		prot.buffin.push(new int[]{0x00, 0x00, 0x00, 0x21, 0xFF});
		prot.buffin.push(new int[]{0x00, 0x01, 0x00, 0x02, 0xFF});
		prot.buffin.push(new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0});
		m.execute();
		assertArrayEquals(new int[]{0x55, 0x61, 0x00, 7, 141}, prot.buffout.pollLast());
		assertArrayEquals(new int[]{0x55, 0x61, 0x00, 32, 45}, prot.buffout.pollLast());
		assertArrayEquals(new int[]{0x55, 0x21, 0x7D}, prot.buffout.pollLast());

		assertEquals(0x21, m.extraTags.get(0).tag.getInt());
		assertEquals(0x10002, m.extraTags.get(1).tag.getInt());

		m.extraTags.get(0).tag.setInt(10);
		prot.buffin.push(new int[]{0, 0, 0, 33, 0});
		prot.buffin.push(new int[]{0, 1, 0, 2, 0});
		prot.buffin.push(new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0});
		m.execute();
		assertArrayEquals(new int[]{0x55, 0x81, 0, 7, 0, 0, 0, 10, 133}, prot.buffout.pollLast());
		assertArrayEquals(new int[]{0x55, 0x61, 0, 7, 141}, prot.buffout.pollLast());
		assertArrayEquals(new int[]{0x55, 0x61, 0, 32, 45}, prot.buffout.pollLast());
		assertArrayEquals(new int[]{0x55, 0x21, 0x7D}, prot.buffout.pollLast());

		prot.buffin.push(new int[]{0, 0, 0, 10, 0});
		prot.buffin.push(new int[]{0, 1, 0, 2, 0});
		prot.buffin.push(new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0});
		m.execute();
		assertArrayEquals(new int[]{0x55, 0x81, 0, 7, 0, 0, 0, 10, 133}, prot.buffout.pollLast());
		assertArrayEquals(new int[]{0x55, 0x61, 0, 7, 141}, prot.buffout.pollLast());
		assertArrayEquals(new int[]{0x55, 0x61, 0, 32, 45}, prot.buffout.pollLast());
		assertArrayEquals(new int[]{0x55, 0x21, 125}, prot.buffout.pollLast());

		prot.buffin.push(new int[]{0, 0, 0, 10, 0});
		prot.buffin.push(new int[]{0, 1, 0, 2, 0});
		prot.buffin.push(new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0});
		m.execute();
		assertArrayEquals(new int[]{0x55, 0x61, 0, 7, 141}, prot.buffout.pollLast());
		assertArrayEquals(new int[]{0x55, 0x61, 0, 32, 45}, prot.buffout.pollLast());
		assertArrayEquals(new int[]{0x55, 0x21, 125}, prot.buffout.pollLast());


	}


	@Test
	public void testTagCrc() {
		prot.buffin.push(new int[]{
				0, 3, 0, 4, // SumNum
				0, 1, 0, 2, // SumWeight
				0, 5, 0, 6, // CurWeight
				0, 7, 0, 8, // LastWeight
				0, 9, 0, 0, // LastTime
				0x00,        // State
				0x80,        // ErrorCode
				0xFF});

		m.execute();

		assertEquals(52, m.tagCrc.getInt());
	}


	@Test
	public void testTagOutput() {
		try {
			m.prepare();
			// 1
			prot.buffin.push(new int[]{
					0, 0, 0, 0,
					0, 0, 0, 10000, // SumWeight
					0, 0, 0, 0,
					0, 0, 0, 50000,
					0, 0, 0, 1000,
					0, 0, 0});
			m.execute();
			assertEquals(0, m.tagOutput.getInt());
			assertEquals(0, m.tagOutputKg.getInt());

			Thread.sleep(1000);

			// 2
			prot.buffin.push(new int[]{
					0, 0, 0, 0,
					0, 0, 0, 60000, // SumWeight
					0, 0, 0, 0,
					0, 0, 0, 50000,
					0, 0, 0, 1000,
					0, 0, 0});
			m.execute();

			assertEquals(180000000, m.tagOutput.getInt());
			assertEquals(180000, m.tagOutputKg.getInt());

			Thread.sleep(1000);

			// 3
			prot.buffin.push(new int[]{
					0, 0, 0, 0,
					0, 0, 0, 60000, // SumWeight
					0, 0, 0, 0,
					0, 0, 0, 50000,
					0, 0, 0, 1000,
					0, 0, 0});
			m.execute();

			assertEquals(180000000, m.tagOutput.getInt());
			assertEquals(180000, m.tagOutputKg.getInt());

			Thread.sleep(1000);

			// 4
			prot.buffin.push(new int[]{
					0, 0, 0, 0,
					0, 0, 0, 60000, // SumWeight
					0, 0, 0, 0,
					0, 0, 0, 50000,
					0, 0, 0, 1000,
					0, 0, 0});
			m.execute();

			assertEquals(180000000, m.tagOutput.getInt());
			assertEquals(180000, m.tagOutputKg.getInt());

			Thread.sleep(1000);

			// 5
			prot.buffin.push(new int[]{
					0, 0, 0, 0,
					0, 0, 0, 60000, // SumWeight
					0, 0, 0, 0,
					0, 0, 0, 50000,
					0, 0, 0, 1000,
					0, 0, 0});
			m.execute();

			assertEquals(0, m.tagOutput.getInt());
			assertEquals(0, m.tagOutputKg.getInt());

			Thread.sleep(1000);

			// 6
			prot.buffin.push(new int[]{
					0, 0, 0, 0,
					0, 0, 0, 60000, // SumWeight
					0, 0, 0, 0,
					0, 0, 0, 50000,
					0, 0, 0, 1000,
					0, 0, 0});
			m.execute();

			assertEquals(0, m.tagOutput.getInt());
			assertEquals(0, m.tagOutputKg.getInt());


		} catch (InterruptedException e) {
		}
	}


	@Test
	public void testExecuteShad() {
		prot.buffin.push(new int[]{
				0x06, 0x2B, 0x98, 0x4E,
				0x37, 0xF0, 0x43, 0xE8,
				0x00, 0x00, 0x03, 0xCA,
				0x00, 0x00, 0x00, 0x00,
				0x00, 0x00, 0x00, 0x00,
				0x01,
				0x00,
				0x22});

		m.netaddr = 7;

		m.execute();


		int[] expout = new int[]{0x55, 0x27, 0xA0};
		int[] actout = prot.buffout.pollLast();
		assertArrayEquals("Check data to send", expout, actout);
	}


	@Test
	public void testMultireq() {
		m.multireq = true;
		prot.buffin.push(new int[]{
				0, 0, 0, 32, // SumNum
				255, 255, 255, 255, // SumWeight
				0x00, 0x01, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0xFF, 0x7F, 0xFF, 0xFF, 0xFF, 0x00, 0x80,	0xFF});

		prot.buffin.push(new int[]{
				0, 0, 0, 32, // SumNum
				255, 255, 255, 255, // SumWeight
				0x00, 0x01, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0xFF, 0x7F, 0xFF, 0xFF, 0xFF, 0x00, 0x80,	0xFF});

		m.execute();

		assertFalse(m.tagError.getBool());
		assertEquals(0, m.tagErrorCnt.getInt());


		int[] expout = new int[]{0x55, 0x21, 0x7D};
		int[] actout = prot.buffout.pollLast();
		assertArrayEquals("Check data to send", expout, actout);

		assertEquals(0, m.tagSumNumHigh1.getInt());
		assertEquals(32, m.tagSumNumLow1.getInt());

		assertEquals(65535, m.tagSumWeightHigh1.getInt());
		assertEquals(65535, m.tagSumWeightLow1.getInt());

		assertEquals(1, m.tagCurWeightHigh.getInt());
		assertEquals(0, m.tagCurWeightLow.getInt());

		assertEquals(65535, m.tagLastWeightHigh.getInt());
		assertEquals(65535, m.tagLastWeightLow.getInt());

		assertEquals(32767, m.tagLastTimeHigh.getInt());
		assertEquals(65535, m.tagLastTimeLow.getInt());

		assertEquals(0x00, m.tagState.getInt());
		assertEquals(0x80, m.tagErrorCode.getInt());

	}


}