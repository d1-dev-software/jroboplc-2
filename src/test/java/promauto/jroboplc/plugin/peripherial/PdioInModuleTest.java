package promauto.jroboplc.plugin.peripherial;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;

import promauto.jroboplc.core.api.InitUtils;
import promauto.jroboplc.core.api.SampleEnvironment;
import promauto.jroboplc.core.api.SamplePlugin;
import promauto.jroboplc.core.ConfigurationYaml;
import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.EnvironmentInst;

public class PdioInModuleTest {
	private static String CFG_DIR = InitUtils.getResourcesDir("peripherial", PeripherialModule.class);

	PaPdioModule m;
	SampleProtocolAA55 prot;


	@Before
	public void setUp() throws Exception {
		InitUtils.setupLogger();

		SampleEnvironment env = new SampleEnvironment();
		EnvironmentInst.set(env);
		
		Configuration cm = new ConfigurationYaml( CFG_DIR );
		if( !cm.load() ) 
			fail();
		env.setConfiguration(cm);

		SamplePlugin plug = new SamplePlugin();

		m = new PaPdioModule(plug, "di");
		HashMap<String,String> conf = new HashMap<String,String>();
		conf.put("mode", "readonly");
		m.load(conf);
		m.retrial = 3;
		m.netaddr = 1;

		prot = new SampleProtocolAA55(m);
		m.protocol = prot; 
	}

	
	
	@Test
	public void testExecuteGood() {
		prot.buffin.push( new int[]{0xFF,0x00, 0x00,0xFF, 0x55,0xAA, 0x11,0xEE, 0xFD} );
		m.execute();
		
		assertFalse( m.tagError.getBool());
		assertEquals( 0, m.tagErrorCnt.getInt());
		
		
		int[] expout = new int[]{0x55, 0x41};
		int[] actout = prot.buffout.pollLast();
		assertArrayEquals("Check data to send", expout, actout);
		
		
		
		int[] inp = new int[4]; 
		int[] brd = new int[4]; 
		Arrays.fill(inp, 0);
		for (int i=0; i<4; i++) { 
			for (int j=0; j<16; j++)
				inp[i] += m.inputs[i*16+j].getInt() << j;
			brd[i] = m.boards[i].getInt();
		}

		int[] expin = new int[]{0x00FF, 0xFF00, 0xAA55, 0xEE11};
		assertArrayEquals("Check data in the inputs tags", expin, inp);
		assertArrayEquals("Check data in the boards tags", expin, brd);
	}
	
	
	@Test
	public void testExecuteBadCrc1() {
		prot.buffin.push( new int[]{0xFF,0x00, 0x00,0xFF, 0x55,0xAA, 0x11,0xEE, 0xFF} );
		prot.buffin.push( new int[]{0xFF,0x00, 0x00,0xFF, 0x55,0xAA, 0x11,0xEE, 0xFD} );
		m.execute();
		
		assertFalse("Having 1 error answer with retrial=3", m.tagError.getBool());
		assertEquals("Errorcnt after 1 error answer", 0, m.tagErrorCnt.getInt());
	}

	
	@Test
	public void testExecuteBadCrc2() {
		// bad answers
		prot.buffin.push( new int[]{0xFF,0x00, 0x00,0xFF, 0x55,0xAA, 0x14,0xEE, 0xFD} );
		prot.buffin.push( new int[]{0xFF,0x00, 0x00,0xFF, 0x55,0xAA, 0x13,0xEE, 0xFD} );
		prot.buffin.push( new int[]{0xFF,0x00, 0x00,0xFF, 0x55,0xAA, 0x12,0xEE, 0xFD} );
		// good answer
		prot.buffin.push( new int[]{0xFF,0x00, 0x00,0xFF, 0x55,0xAA, 0x11,0xEE, 0xFD} );
		m.execute();
	}

	
	@Test
	public void testExecuteBadNoAnswer() {
		// empty answers
		prot.buffin.push( null );
		prot.buffin.push( null );
		prot.buffin.push( null );
		m.execute();
		
		assertTrue("Having no answers with retrial=3", m.tagError.getBool());
	}

	

	@Test
	public void testEnable_0() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {

		Field f = m.getClass().getSuperclass().getSuperclass().getDeclaredField("enable");
		f.setAccessible(true);
		f.setBoolean(m, Boolean.FALSE);

		prot.buffin.push(new int[] { 0xFF, 0x00, 0x00, 0xFF, 0x55, 0xAA, 0x11, 0xEE, 0xFD });
		m.execute();

		assertFalse(m.tagError.getBool());
		assertEquals(0, m.tagErrorCnt.getInt());

		int[] actout = prot.buffout.pollLast();
		assertArrayEquals("Check data to send", null, actout);
	}

	
}
