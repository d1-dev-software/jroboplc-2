package promauto.jroboplc.plugin.peripherial;

import java.util.Arrays;
import java.util.LinkedList;

public class SampleProtocolAA55 extends ProtocolAA55 {
	public LinkedList<int[]> buffin = new LinkedList<>();
	public LinkedList<int[]> buffout = new LinkedList<>();
	
	public SampleProtocolAA55(PeripherialModule module) {
		super(module);
	}

	@Override
	public void writeBytes(int[] data, int size) throws Exception {
		buffout.push( Arrays.copyOfRange(data, 0, size));		
	}

	@Override
	public boolean readBytes(int[] data, int size) throws Exception {
		int[] buff = buffin.pollLast();
		if (buff==null)
			return false;
		
		for(int i=0; (i<size) && (i<buff.length); i++)
			data[i] = buff[i];
		return size <= buff.length;
	}


	@Override
    public boolean request(int[] buffout, int sizeout, int[] buffin, int sizein, int crctype) throws Exception {
    	writeBytes(buffout, sizeout);
    	return readBytes(buffin, sizein);
    }

    

}
