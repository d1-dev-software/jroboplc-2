package promauto.jroboplc.plugin.peripherial;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;

import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;

import promauto.jroboplc.core.api.InitUtils;
import promauto.jroboplc.core.api.SampleEnvironment;
import promauto.jroboplc.core.api.SamplePlugin;
import promauto.jroboplc.core.ConfigurationYaml;
import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.EnvironmentInst;

public class I7024ModuleTest {
	private static String CFG_DIR = InitUtils.getResourcesDir("peripherial", PeripherialModule.class);
	
	I7024Module m;
	SampleProtocolIcpcon prot;


	@Before
	public void setUp() throws Exception {
		InitUtils.setupLogger();

		SampleEnvironment env = new SampleEnvironment();
		EnvironmentInst.set(env);
		
		Configuration conf = new ConfigurationYaml( CFG_DIR );
		if( !conf.load() ) 
			fail();
		env.setConfiguration(conf);

		
		SamplePlugin plug = new SamplePlugin();

		m = new I7024Module(plug, "do");
		m.load(new HashMap<String,String>());
		m.retrial = 3;
		m.netaddr = 2;
//		m.tagEnable.setInt(1);
		
		prot = new SampleProtocolIcpcon(m);
		m.protocol = prot; 
	}

	
	
	@Test
	public void testExecute() {
		// read
		// $0280EE
		// !02+05.000D1
		
		prot.pushIn( "!02+05.000__\r" );
		prot.pushIn( "!02+06.000__\r" );
		prot.pushIn( "!02+07.000__\r" );
		prot.pushIn( "!02+08.000__\r" );
		m.execute();
		
		assertFalse( m.tagError.getBool());
		assertEquals( 0, m.tagErrorCnt.getInt());
		
		assertEquals("$0280", prot.pollOut());
		assertEquals("$0281", prot.pollOut());
		assertEquals("$0282", prot.pollOut());
		assertEquals("$0283", prot.pollOut());
		
		assertEquals(5000, m.outputs[0].value.getInt() );
		assertEquals(5000, m.outputs[0].valsp.getInt() );
		assertEquals(6000, m.outputs[1].value.getInt() );
		assertEquals(6000, m.outputs[1].valsp.getInt() );
		assertEquals(7000, m.outputs[2].value.getInt() );
		assertEquals(7000, m.outputs[2].valsp.getInt() );
		assertEquals(8000, m.outputs[3].value.getInt() );
		assertEquals(8000, m.outputs[3].valsp.getInt() );

		
		
		prot.pushIn( "!02+05.000__\r" );
		prot.pushIn( "!02+06.000__\r" );
		prot.pushIn( "!02+07.000__\r" );
		prot.pushIn( "!02+08.000__\r" );
		m.execute();
		assertEquals(5000, m.outputs[0].value.getInt() );
		assertEquals(5000, m.outputs[0].valsp.getInt() );

		
		
		prot.pushIn( ">__\r" );
		prot.pushIn( "!02+05.000__\r" );
		prot.pushIn( "!02+06.000__\r" );
		prot.pushIn( "!02+07.000__\r" );
		prot.pushIn( "!02+08.000__\r" );
		prot.out.clear();
		m.outputs[0].valsp.setInt(5500);
		m.execute();
		
		assertEquals("#020+05.500", prot.pollOut());
		assertEquals("$0280", 		prot.pollOut());
		assertEquals("$0281", 		prot.pollOut());
		assertEquals("$0282", 		prot.pollOut());
		assertEquals("$0283", 		prot.pollOut());

		assertEquals(5000, m.outputs[0].value.getInt() );
		assertEquals(5500, m.outputs[0].valsp.getInt() );
		
		
		
		prot.pushIn( ">__\r" );
		prot.pushIn( "!02+05.500__\r" );
		prot.pushIn( "!02+06.000__\r" );
		prot.pushIn( "!02+07.000__\r" );
		prot.pushIn( "!02+08.000__\r" );
		m.execute();
		assertEquals(5500, m.outputs[0].value.getInt() );
		assertEquals(5500, m.outputs[0].valsp.getInt() );

		
		
		prot.pushIn( "!02+05.500__\r" );
		prot.pushIn( "!02+06.000__\r" );
		prot.pushIn( "!02+07.000__\r" );
		prot.pushIn( "!02+08.000__\r" );
		m.execute();

}
	

	
}

