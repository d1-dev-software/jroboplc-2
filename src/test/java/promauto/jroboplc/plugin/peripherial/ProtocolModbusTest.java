package promauto.jroboplc.plugin.peripherial;

import static org.junit.Assert.*;

import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;

import promauto.jroboplc.core.api.InitUtils;
import promauto.jroboplc.core.api.SampleEnvironment;
import promauto.jroboplc.core.api.SamplePlugin;
import promauto.jroboplc.core.api.SampleSerialManager;
import promauto.jroboplc.core.api.SampleSerialPort;
import promauto.jroboplc.core.ConfigurationYaml;
import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.EnvironmentInst;

public class ProtocolModbusTest {
	private static String CFG_DIR = InitUtils.getResourcesDir("peripherial", PeripherialModule.class);

	ProtocolModbus prot;
	PeripherialModule mod;
	SampleSerialPort ser;
	SampleSerialManager sm;
	

	@Before
	public void setUp() throws Exception {
		InitUtils.setupLogger();
		
		SampleEnvironment env = new SampleEnvironment();
		EnvironmentInst.set(env);
		
		Configuration conf = new ConfigurationYaml( CFG_DIR );
		if( !conf.load() ) 
			fail();
		env.setConfiguration(conf);

		sm = new SampleSerialManager();
    	ser = sm.port;
		env.setSerialManager(sm);

		SamplePlugin plug = new SamplePlugin();
		mod = new PeripherialModule(plug, "");
		mod.load(new HashMap<String,String>());
		mod.netaddr = 0;
		mod.port = ser;
		
		prot = new ProtocolModbus(mod); 

	}

	
	
	@Test
	public void testCmd3() throws Exception {
		
		ser.setBuffRead(new int[]{
				0x00, 0x03, 0x22, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x14, 0xA5, 
				0x28, 0x00, 0x01, 0xC3, 0x5E, 0x00, 0x00, 0x27, 0x10, 0x00, 0x00, 0x00, 
				0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
				0x00, 0x44, 0xFC
		});
		assertTrue( prot.requestCmd3(0, 0x11) );
		assertArrayEquals(new int[]{0, 3, 0, 0, 0, 0x11, 0x84, 0x17}, ser.getBuffWrite());

		
		mod.netaddr = 0x11;
		ser.setBuffRead(new int[]{0x11, 0x03, 0x06, 0xAE, 0x41, 0x56, 0x52, 0x43, 0x40, 0x49, 0xAD});
		assertTrue( prot.requestCmd3(0x6B, 3) );
		assertArrayEquals(new int[]{0x11, 0x03, 0x00, 0x6B, 0x00, 0x03, 0x76, 0x87}, ser.getBuffWrite());
	
	}
	
	@Test
	public void testCmd3Error() throws Exception {
		mod.netaddr = 0x0A;
		ser.setBuffRead(new int[]{0x0A, 0x83, 0x02, 177, 51});
		assertFalse( prot.requestCmd3(0x04A1, 1) );
		assertArrayEquals(new int[]{0x0A, 0x03, 0x04, 0xA1, 0x00, 0x01, 213, 163}, ser.getBuffWrite());
	}


	@Test
	public void testCmd6() throws Exception {
		mod.netaddr = 0x11;
		ser.setBuffRead(new int[]{0x11, 0x06, 0x00, 0x01, 0x00, 0x03, 0x9A, 0x9B});
		assertTrue( prot.requestCmd6(1, 3) );
		assertArrayEquals(new int[]{0x11, 0x06, 0x00, 0x01, 0x00, 0x03, 0x9A, 0x9B}, ser.getBuffWrite());
	}

	@Test
	public void testCmd6Error() throws Exception {
		mod.netaddr = 0x0A;
		ser.setBuffRead(new int[]{0x0A, 0x83, 0x02, 177, 51});
		assertTrue( prot.requestCmd6(1, 3) );
		assertArrayEquals(new int[]{0x0A, 0x06, 0x00, 0x01, 0x00, 0x03, 153, 112}, ser.getBuffWrite());
	}


	@Test
	public void testCmd10() throws Exception {
		mod.netaddr = 0x11;
		ser.setBuffRead(new int[]{0x11, 0x10, 0x00, 0x01, 0x00, 0x02, 0x12, 0x98});
		
		assertTrue( prot.requestCmd10(1, 2, 0x000A, 0x0102 ));
		
		assertArrayEquals(new int[]{
				0x11, 0x10, 0x00, 0x01, 0x00, 0x02, 0x04, 0x00, 0x0A, 0x01, 0x02, 0xC6, 0xF0}, ser.getBuffWrite());
	}
	
	@Test
	public void testCmd0F() throws Exception {
		mod.netaddr = 0x11;
		ser.setBuffRead(new int[]{0x11, 0x0F, 0x00, 0x01, 0x00, 0x02, 0x87, 0x5A});
		
		assertTrue( prot.requestCmd0F(1, 29, 1,0,1,0,1,0,1,0,  0,1,0,1,0,1,0,1,  0,0,0,0,0,0,0,0,  1,1,1,1,1 ));
		
		assertArrayEquals(new int[]{
				0x11, 0x0F,  0,1, 0,29, 4, 0x55, 0xAA, 0, 0x1F, 37, 77}, ser.getBuffWrite());
	}
	
	@Test
	public void testCmd5() throws Exception {
		mod.netaddr = 0x11;
		ser.setBuffRead(new int[]{0x11, 0x06, 0x00, 0x01, 0x00, 0x03, 0x9A, 0x9B});
		assertTrue( prot.requestCmd5(1, 1) );
		assertArrayEquals(new int[]{0x11, 0x05, 0x00, 0x01, 0xff, 0x00, 223, 106}, ser.getBuffWrite());
		
		ser.setBuffRead(new int[]{0x11, 0x06, 0x00, 0x01, 0x00, 0x03, 0x9A, 0x9B});
		assertTrue( prot.requestCmd5(1, 0) );
		assertArrayEquals(new int[]{0x11, 0x05, 0x00, 0x01, 0x00, 0x00, 158, 154}, ser.getBuffWrite());
		
	}
	
	

}
