package promauto.jroboplc.plugin.peripherial;

import org.junit.Before;
import org.junit.Test;
import promauto.jroboplc.core.ConfigurationYaml;
import promauto.jroboplc.core.api.*;

import java.util.HashMap;

import static org.junit.Assert.*;

public class KorennPassModuleTest {
	private static String CFG_DIR = InitUtils.getResourcesDir("peripherial", PeripherialModule.class);

	KorennPassModule mod;
	SampleSerialPort ser;
	SampleSerialManager sm;

	@Before
	public void setUp() throws Exception {
		InitUtils.setupLogger();
		
		SampleEnvironment env = new SampleEnvironment();
		EnvironmentInst.set(env);
		
		Configuration conf = new ConfigurationYaml( CFG_DIR );
		if( !conf.load() ) 
			fail();
		env.setConfiguration(conf);

		sm = new SampleSerialManager();
    	ser = sm.port;
		env.setSerialManager(sm);

		SamplePlugin plug = new SamplePlugin();
		mod = new KorennPassModule(plug, "");
		mod.load(new HashMap<String,String>());
		mod.netaddr = 1;
		mod.port = ser;
	}


	@Test
	public void testRequest1() throws Exception {
		String request = "#010542\r\n";
		String answer = "@01012345672F\r\n";

		ser.setBuffRead( answer );

		assertTrue( mod.request(0x42) );
		assertEquals( request, ser.getBuffWriteString() );
	}

	@Test
	public void testRequest2() throws Exception {
		String request = "#010522\r\n";
		String answer = "@01007B60FD27\r\n";

		ser.setBuffRead( answer );

		assertTrue( mod.request(0x22) );
		assertEquals( request, ser.getBuffWriteString() );
	}


	@Test
	public void testExecute() throws Exception {
		String request =
				"#010522\r\n" +
				"#010524\r\n" +
				"#010531\r\n" +
				"#01052F\r\n" +
				"#01052D\r\n";

		String answer = "@01012345672F\r\n";

		for(int i=0; i<mod.tas.size(); ++i)
			ser.setBuffRead( answer );

		assertTrue( mod.executePeripherialModule() );
		assertEquals( request, ser.getBuffWriteString() );
		assertEquals( 0x01234567, mod.tagSumWeight.getLong());
		assertEquals( 0x4567, mod.tagSumNum.getLong());
		assertEquals( 0x4567, mod.tagLastTime.getLong());
		assertEquals( 0x4567, mod.tagReqOutput.getLong());
		assertEquals( 0x4567, mod.tagCurOutput.getLong());
	}



}
