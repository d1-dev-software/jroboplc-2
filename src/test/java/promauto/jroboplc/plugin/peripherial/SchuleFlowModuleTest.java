package promauto.jroboplc.plugin.peripherial;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;

import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;

import promauto.jroboplc.core.api.InitUtils;
import promauto.jroboplc.core.api.SampleEnvironment;
import promauto.jroboplc.core.api.SamplePlugin;
import promauto.jroboplc.core.api.SampleSerialManager;
import promauto.jroboplc.core.api.SampleSerialPort;
import promauto.jroboplc.core.ConfigurationYaml;
import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.EnvironmentInst;

public class SchuleFlowModuleTest {
	private static String CFG_DIR = InitUtils.getResourcesDir("peripherial", PeripherialModule.class);

	SchuleFlowModule m;
	SampleSerialPort ser;
	SampleSerialManager sm;


	@Before
	public void setUp() throws Exception {
		InitUtils.setupLogger();

		SampleEnvironment env = new SampleEnvironment();
		EnvironmentInst.set(env);
		
		Configuration conf = new ConfigurationYaml( CFG_DIR );
		if( !conf.load() ) 
			fail();
		env.setConfiguration(conf);

		SamplePlugin plug = new SamplePlugin();

		env.setSerialManager( sm = new SampleSerialManager() );
    	ser = sm.port;

		
		m = new SchuleFlowModule(plug, "shule");
		m.load(new HashMap<String,String>());
		m.retrial = 3;
		m.netaddr = 1;
//		m.tagEnable.setInt(1);
		
	}

	
	
	@Test
	public void testExecuteRead() {
		m.netaddr = 7;
		ser.setBuffRead(new int[]
			{0xDD,0xDD,0x3C,0x07, 0,0,0,0, 
				1,2,		//parState
				3,4,5,6,	//parActFlow
				7,8,
				1,0,1,2,	//parTrgFlow
				3,4,
				5,6,		//parTotalSumH
				7,8,		//parTotalSumL
				9,0,
				1,2,		//parProdType
				3,4,		//parAlarmCode
				5,6,7,8,
				2,0,1,2,	//parTrgPresel
				3,4,5,6,	//parRestPresel
				7,8,		//parPreselExpir
				9,0,		//parLenActive
				1,2,		//parActualLen
				3,4,
				5,6,		//parLenCtrl
				7,8,
				9,1,		//parTrgLen
				0x20,0x20}
		);
		
		
		m.tagSetState     .setInt(0x123);
		m.tagSetTrgFlow   .setInt(0x12345678);
		m.tagSetProdType  .setInt(0x234);
		m.tagSetTrgPresel .setInt(0x23456789);
		m.tagSetLenCtrl   .setInt(0x345);
		m.tagSetTrgLen    .setInt(0x567);
		
		
		m.execute();
		
		assertFalse( m.tagError.getBool());
		assertEquals( 0, m.tagErrorCnt.getInt());

		assertArrayEquals(new int[]	{0xDD,0xDD,0x3C,0x07,0,0,
			0,1,1,0x23, //state
			0,0,0,0, 
			0,1,0x12,0x34,0x56,0x78, //trgflow
			0,0,0,0,0,0,
			0,1,2,0x34, //prodtype
			0,0,0,0,
			0,1,0x23,0x45,0x67,0x89, //trgpresel
			0,0,0,0,0,0,0,0,0,0,
			0,1,3,0x45, //lenctrl
			0,1,5,0x67, //trglen
			0x20,0x20}, ser.getBuffWrite());
		
//DD DD 3C 7 0 0 0 1 1 23	0 0 0 0 0 1 12 34 56 78 0 0 0 0 0 0 0 1 2 34 0 0 0 0 0 1 23 45 67 89 0 0 0 0 0 0 0 0 0 0 0 1 3 45 0 1 5 67 20 20		

		assertEquals(0x102, m.tagState.getInt());
		assertEquals(0x3040506, m.tagActFlow.getInt());
		assertEquals(0x1000102, m.tagTrgFlow.getInt());
		assertEquals(0x506, m.tagTotalSumH.getInt());
		assertEquals(0x708, m.tagTotalSumL.getInt());
		assertEquals(0x102, m.tagProdType.getInt());
		assertEquals(0x304, m.tagAlarmCode.getInt());
		assertEquals(0x2000102, m.tagTrgPresel.getInt());
		assertEquals(0x3040506, m.tagRestPresel.getInt());
		assertEquals(0x708, m.tagPreselExpir.getInt());
		assertEquals(0x900, m.tagLenActive.getInt());
		assertEquals(0x102, m.tagActualLen.getInt());
		assertEquals(0x506, m.tagLenCtrl.getInt());
		assertEquals(0x901, m.tagTrgLen.getInt());
		
	}
	
	

}


//out:
//DD DD 3C 7 0 0 0 1 1 23 0 0 0 0 0 1 12 34 56 78 0 0 0 0 0 0 0 1 2 34 0 0 0 0 0 1 23 45 67 89 0 0 0 0 0 0 0 0 0 0 0 1 3 45 0 1 5 67 20 20
//DD DD 3C 7 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 20 20
//
//in:
//DD DD 3C 07 0 0 0 0 1 2 3 4 5 6 7 8 1 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 2 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 1 20 20













