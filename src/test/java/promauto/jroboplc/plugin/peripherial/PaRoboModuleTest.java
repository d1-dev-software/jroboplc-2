package promauto.jroboplc.plugin.peripherial;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import promauto.jroboplc.core.api.InitUtils;
import promauto.jroboplc.core.api.SampleSerialManager;
import promauto.jroboplc.core.api.SampleSerialPort;
import promauto.jroboplc.core.RealEnvironment;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.utils.CRC;

public class PaRoboModuleTest {

	private static String CFGDIR = InitUtils.getResourcesDir("peripherial", PaRoboModuleTest.class);

	SampleSerialPort ser;
	SampleSerialManager sm;
	

	@Before
	public void setUp() throws Exception {
		InitUtils.setupLogger();
		
		sm = new SampleSerialManager();
    	ser = sm.port;
	}

	
	
	@Test
	public void testLoad() throws Exception {
		
		/*
		ser.setBuffRead(new int[]{
				0x00, 0x03, 0x22, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x14, 0xA5, 
				0x28, 0x00, 0x01, 0xC3, 0x5E, 0x00, 0x00, 0x27, 0x10, 0x00, 0x00, 0x00, 
				0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
				0x00, 0x44, 0xFC
		});
		assertTrue( prot.requestCmd3(0, 0x11) );
		assertArrayEquals(new int[]{0, 3, 0, 0, 0, 0x11, 0x84, 0x17}, ser.getBuffWrite());

		
		mod.netaddr = 0x11;
		ser.setBuffRead(new int[]{0x11, 0x03, 0x06, 0xAE, 0x41, 0x56, 0x52, 0x43, 0x40, 0x49, 0xAD});
		assertTrue( prot.requestCmd3(0x6B, 3) );
		assertArrayEquals(new int[]{0x11, 0x03, 0x00, 0x6B, 0x00, 0x03, 0x76, 0x87}, ser.getBuffWrite());
	*/
	}
	
	
	
	@Test
	public void testExecute() throws Exception {
		
		String s = "+0;ffff;;;;;;;;;;;;;;;;;;";
//		String s = "1>;;;;;;;;;;;;;;;;;;;;;;;;;17c;17c;17c;17c;17c;17c;17c;17c;17c;17c;17c;17c;32;32;32;32;32;32;32;32;32;32;32;32;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;4;;1;4;;1;;f;1;1;;1;4;;1;683;14b0;;;7e0;1;14;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;;;1;4;;1;4;;1;;;1;4;;1;;;1;4;;1;;;1;4;;1;;;1;4;;1;;;1;4;;1;1;1;;1;;;1;4;;1;;;1;4;;1;4;;1;4;;1;;;1;4;;1;1;1;;1;;;1;4;;1;;;1;4;;1;;;1;4;;1;;4;;1;4;;1;;4;;1;4;" + 
//				";1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;1;;7;7;;1;;113;112;114;814;813;812;;;1;4;1;;7;7;;1;;116;115;117;817;816;815;;;1;;1;;1;;1;;1;;1;4;1;"; 
		
		int crc = CRC.getCrc16(s.getBytes(), 0, s.length()-1 );
		
//		assertEquals(0x21b2, crc);
		assertEquals(0xba59, crc);
		
		/*
robo1 --> %1
robo1 <-- 1>;;;;;;;;;;;;;;;;;;;;;;;;;17c;17c;17c;17c;17c;17c;17c;17c;17c;17c;17c;17c;32;32;32;32;32;32;32;32;32;32;32;32;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;4;;1;4;;1;;f;1;1;;1;4;;1;683;14b0;;;7e0;1;14;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;;;1;4;;1;4;;1;;;1;4;;1;;;1;4;;1;;;1;4;;1;;;1;4;;1;;;1;4;;1;1;1;;1;;;1;4;;1;;;1;4;;1;4;;1;4;;1;;;1;4;;1;1;1;;1;;;1;4;;1;;;1;4;;1;;;1;4;;1;;4;;1;4;;1;;4;;1;4;
robo1 *** RETRIAL: 0 *** 
robo1 --> %1
robo1 <-- ;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;1;;7;7;;1;;113;112;114;814;813;812;;;1;4;1;;7;7;;1;;116;115;117;817;816;815;;;1;;1;;1;;1;;1;;1;4;1;|21b2
robo1 *** RETRIAL: 1 *** 
		 
		 */
		
		
		
		assertTrue( RealEnvironment.create(CFGDIR) );
		EnvironmentInst.get().setSerialManager(sm);
		
		Object confrobo1 = EnvironmentInst.get().getConfiguration().getModuleConf("peripherial", "robo1");
		
		PeripherialPlugin plg = new PeripherialPlugin();
		plg.initialize();
		PaRoboModule m = (PaRoboModule)plg.createModule("robo1", confrobo1);
		assertNotNull(m);
		assertTrue( m.prepare() );
		
		
		ser.setBuffRead(
				"0>;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;1;1d1f;3;1;1;;3;1;;;1;1d1f;;1;1;67d;8;1;;;;;;;;;;;;;;;;;;;;;;1;67d;9;1;1;67d;a;1;1;67d;f;1;1;67d;d;1;1;67d;c;1;1;1d1f;1;1;1;1d1f;2;1;1;1d1f;4;1;1;1d1f;5;1;1;;1;1;1;;2;1;1;;4;1;1;;5;1;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;1;;;1;1;67d;e;1;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;1;1d1f;6;1;1;;6;1;1;67e;7;1;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;ffff;ffff;;ffff;;;;;;;;;;;;;;;;;;|d294\r" +
				"1>;;;;;;;;;;;;;;;;;;;;;;;;;17c;17c;17c;17c;17c;17c;17c;17c;17c;17c;17c;17c;32;32;32;32;32;32;32;32;32;32;32;32;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;4;;1;4;;1;;f;1;1;;1;4;;1;46e;135b;;;7df;c;8;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;;;1;4;;1;4;;1;;;1;4;;1;;;1;4;;1;;;1;4;;1;;;1;4;;1;;;1;4;;1;1;1;;1;;;1;4;;1;;;1;4;;1;4;;1;4;;1;;;1;4;;1;1;1;;1;;;1;4;;1;;;1;4;;1;;;1;4;;1;;4;;1;4;;1;;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;1;;7;7;;1;;113;112;114;814;813;812;;;1;4;1;;7;7;;1;;116;115;117;817;816;815;;;1;;1;;1;;1;;1;;1;4;1;|c78a\r" +
				"2>4;1;4;1;4;1;4;1;4;1;4;1;4;1;;;;;1;4;1;4;1;4;1;4;1;4;1;4;1;4;1;4;1;;;;;1;4;1;4;1;4;1;4;1;4;1;4;1;4;1;4;1;;;;;1;4;1;4;1;4;1;4;1;4;1;4;1;4;1;4;1;;;;;1;4;1;4;1;4;1;4;1;4;1;4;1;4;1;4;1;;;;;1;4;1;4;1;4;1;;1;4;1;4;1;4;1;4;1;;;;;1;4;;1;4;1;;1;4;;1;4;1;;1;;;1;4;1;;1;4;1;;1;4;1;4;1;;1;;1;4;1;;1;4;1;;1;4;1;;1;4;1;;1;4;1;;1;4;1;;1;4;1;;1;4;1;;1;4;1;;1;4;1;;1;4;1;;1;;1;;1;;1;;1;;1;;1;4;1;;;1;4;1;;;1;4;1;;;;1;;;;1;28;5;;;;1;;;;1;;;;1;;;;1;;;;1;;;;1;;;;1;;;;1;;;;1;;;;1;;;;1;;;;1;;;;1;;;;1;;;;1;;;;1;;;;1;;;;1;;;;;1;;4;;;1;80;;;;;;;4;1;;1;1;1;;1;4;;1;;;;;;;;;;;;;;1;;7;7;;1;;101;100;102;802;801;800;;7;7;;1;;104;103;105;805;804;803;;7;7;;1;;107;106;108;808;807;806;;7;7;;1;;10a;109;10b;80b;80a;809;;7;7;|f556\r" +
				"3>;1;;10d;10c;10e;80e;80d;80c;;7;7;;1;;110;10f;111;811;810;80f;1;1;;1;1;7;7;;1;;134;133;135;835;834;833;1;7;7;;1;;13a;139;13b;83b;83a;839;1;7;7;;1;;207;206;208;905;904;903;1;7;7;;1;;20d;20c;20e;90b;90a;909;1;7;7;;1;;213;212;214;911;910;90f;1;7;7;;1;;201;200;202;914;913;912;1;7;7;;1;;12b;12a;12c;82c;82b;82a;1;7;7;;1;;131;130;132;832;831;830;1;7;7;;1;;137;136;138;838;837;836;1;7;7;;1;;204;203;205;902;901;900;1;7;7;;1;;20a;209;20b;908;907;906;1;7;7;;1;;210;20f;211;90e;90d;90c;1;7;7;;1;;13d;13c;13e;83e;83d;83c;1;7;7;;1;;216;215;217;917;916;915;1;7;7;;1;;219;218;21a;91a;919;918;1;7;7;;1;;21c;21b;21d;91d;91c;91b;1;7;7;;1;;21f;21e;220;920;91f;91e;;;;1;;;;1;;;;1;;;;1;;1;;;;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;4;1;;1;;1;;1;;1;;1;;1;;1;4;1;;1;;7;7;;1;;119;118;11a;81a;819;818;;7;7;;1;;125;124;126;826;825;824;;7;7;;1;;128;127;129;829;828;827;;7;7;;1;;11c;11b;11d;81d;81c;81b;;|9a61\r" +
				"4>7;7;;1;;11f;11e;120;820;81f;81e;;7;7;;1;;122;121;123;823;822;821;;1;4;1;;4;;1;;4;;1;;4;;1;;;;;1;;;;;1;4;;1;4;;1;1;;2;;;3c;a;1;1;;1;4;;1;4;;1;1;67e;b;1;;1;;;;;;;;;;;4;;1;;;;;;;;;;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;1;;1;;1;4;1;;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;;1;4;1;;1;4;1;;1;4;1;;1;;;1;;;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;1;7;7;;1;;63e;63f;63d;f02;f01;f00;1;7;7;;1;;63b;63c;63a;f05;f04;f03;1;7;7;;1;;638;639;637;f08;f07;f06;1;7;7;;1;1;62c;62d;62b;f14;f13;f12;1;7;7;;1;;635;636;634;f0b;f0a;f09;1;7;7;;1;;632;633;631;f0e;f0d;f0c;708;5dc;1f4;7d0;5dc;384;60e;5dc;1f4;5dc;44c;1f4;7d0;708;1f4;7d0;5dc;1f4;7d0;5dc;1f4;c80;b54;1f4;a28;834;12c;7d0;5dc;1f4;;;4;1;c8;c8;c8;1f4;c8;c8;c8;c8;1f4;1f4;4;1;;1;4;;1;;;;;4;1;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;4;|b6c9\r" +
				"5>1;;1;4;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;4;;1;4;;1;4;;1;4;;1;4;;1;5;;;;;;1;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;708;7d0;1;7;7;;1;1;62f;630;62e;f11;f10;f0f;1;7;7;;1;;629;62a;628;f17;f16;f15;;4;;;1;80;;;;;;;1;1;;1;1;1;;1;1;1;;1;1;1;;1;;60e;1;1;;1;1;1;;1;1;1;;1;1;1;;1;4;;1;4;1;;1;4;1;4;1;4;1;;1;4;1;4;1;;1;4;1;4;1;;1;4;1;4;1;;1;4;1;4;1;;1;4;1;4;1;;1;4;1;4;1;;1;4;1;4;1;;1;4;1;4;1;;1;;1;4;1;4;1;4;;1;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;1;;;;;;;;;;;;;;;;;;;;;1;1;;1;1;1;;1;1;1;;1;;;;;;;;;;;;;;;;;;;;;;;4;;1;;;;;;;;;;;;;;;4;;1;;;32;;;;12c;;4;;1;;1;4;1;;1;;4;;1;4;;1;4;;1;4;;1;4;;1;|acc6\r" +
				"6>4;;1;4;;1;4;;1;;;1;;1;1;;1;;1;;1;;1;;1;;1;;1;4;1;4;1;;1;4;1;4;1;;1;;;4;;1;c8;c8;c8;c8;c8;c8;c8;c8;c8;c8;c8;c8;a0;a0;a0;a0;a0;a0;a0;a0;a0;a0;a0;a0;1;1;;1;1;1;;1;1;1;;1;1;1;;1;;;;;;;;;1;;;;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;;;12c;4;;1;;12c;32;32;12c;32;32;12c;;;;;;;;;;;;;;;;;;1;4;1;;;1;1;1;;1;1;1;;1;4;1;4;1;;1;4;;1;4;;1;4;;1;4;;1;4;;1;;;;1;;;;1;4;1;4;1;;1;1;1;;1;4;1;4;1;;1;4;1;4;1;;1;4;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;;;4;;1;4;1;4;1;4;1;;1;;1;;;;;;;1;1;;1;;;;;;1;4;;1;1e;;23;5;1e;5;2d;5;1b;5;1e;5;;1;1;;1;1;1;;1;1;1;;1;1;1;;1;;4;;1;1;;;;;;;7d0;8;;;32;;1;4;1;;;1;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;1;4;1;;;|d585\r" +
				"7>1;1;1;8;d08;1;1;2;d32;41;25;24;729;728;1;1;8;d0b;1;1;2;d35;1;1;8;d07;1;1;2;d31;1;1;8;d0a;1;1;2;d34;1;1;8;d0d;1;1;2;d37;1;1;8;d04;1;1;2;d2e;1;1;2c;d02;1;1;2;d2c;1;1;8;d05;1;1;2;d2f;1;1;8;d01;1;1;2;d2b;1;1;2c;d03;1;1;2;d2d;1;1;8;d00;1;1;2;d2a;1;1;8;d0c;1;1;2;d36;1;1;8;d09;1;1;2;d33;1;1;8;d06;1;1;2;d30;1;1;58;d15;1;1;a;d24;;;2;;1;1;a;d25;1;1;2c;d17;;;2;;1;1;2c;d18;;;2;;1;1;2c;d19;;;2;;1;1;58;d1a;;;2;;1;1;58;d1b;;;2;;1;1;58;d0e;c2;224;225;926;925;;;2;;1;1;2c;d0f;;;2;;1;1;58;d10;1;1;2;d3a;1;1;58;d11;;;2;;1;1;2c;d12;c2;221;222;923;922;;;2;;1;1;58;d13;;;2;;1;1;58;d14;;;2;;1;1;8;d26;1;1;2;d28;1;1;8;d27;1;1;2;d29;1;1;8;c36;1;;2;c32;1;1;8;c37;1;;2;c33;1;1;8;c38;1;;2;c34;1;;8;c39;1;;2;c35;1;1;8;d1c;1;1;2;d1e;1;1;8;d1d;1;1;2;d1f;1;1;f;b2d;1;1;2;b2a;1;1;8;b2b;1;1;2;b2c;1;32;1;2;c16;4;a;322;81;1;32;1;2;c17;4;;321;81;a;19;337;81;5;28;336;81;5;19;335;81;4;1e;312;81;4;f;311;81;1;1;1;2;b18;1;1;1;2;b17;1;32;1;3;c8c;1;1;1;2;c0d;1;1;1;1;c0e;1;1;1;2;c0d;1;1;1;2;c0e;1;88;1;8;b84;1388;a7;|840a\r" +
				"8>;5;5;338;81;1;1;1;2;b10;1;1;1;2;b0f;1;96;1;3;c80;1;1;1;2;c01;1;1;1;1;c02;1;1;1;2;c01;1;1;1;2;c02;1;30;1;5;b80;3778;ea;;5;5;33c;81;1;1;1;2;b12;1;1;1;2;b11;1;64;1;3;c83;1;1;1;2;c04;1;1;1;1;c05;1;1;1;2;c04;1;1;1;2;c05;1;30;1;5;b81;364c;e6;;5;5;33b;81;1;1;1;2;b16;1;1;1;2;b15;1;96;1;3;c89;1;1;1;2;c0a;1;1;1;1;c0b;1;1;1;2;c0a;1;1;1;2;c0b;1;f0;1;5;b83;35e8;10a;;5;f;320;81;1;1;1;2;b14;1;1;1;2;b13;1;64;1;3;c86;1;1;1;2;c07;1;1;1;1;c08;1;1;1;2;c07;1;1;1;2;c08;1;ac;1;;;87a;86;;5;5;33a;81;1;1;1;2;b1a;1;1;1;2;b19;1;64;1;3;c91;;;1;2;c12;1;1;1;1;c13;1;1;1;2;c12;1;1;1;2;c13;1;20;1;5;b85;1194;96;;5;9;324;81;1;;937;1;80;4;14;235;81;1;;935;1;80;4;14;236;81;;;e13;1;80;4;14;237;81;1;c95;2;;323;81;1;ca2;1;ca1;4;fa;30d;81;4;fa;30c;81;1;ca3;4;fa;30a;81;1;ca4;4;fa;308;81;1;ca5;4;fa;306;81;1;ca7;4;fa;302;81;1;ca6;4;fa;304;81;5;c9c;2;5;319;81;5;c9d;2;4;318;81;3;c9e;2;4;317;81;4;9bb;4;;33f;81;1;9bc;4;;33e;81;4;9bd;4;;33d;81;4;;238;81;4;;239;81;4;;23a;81;4;;23b;81;4;;31f;81;4;;31e;81;1;32;1;2;c98;4;;320;81;1;32;1;2;c99;4;;31c;81;1;1;1;5;;5;c;|969a\r" +
				"9>30f;89;5;;332;81;5;;331;81;5;;330;81;5;;327;81;5;;326;81;5;;325;81;5;;32c;81;5;;32f;81;5;;32e;81;5;;32d;81;5;;32a;81;5;;329;81;5;;328;81;5;;32b;81;5;8;400;81;5;8;402;81;5;;404;81;5;;406;81;5;;408;81;5;6;31d;81;1;80;4;14;333;81;41;50d;50e;a30;a31;1;;939;;;;c14;c2;;;;;5;7;401;81;5;7;403;81;5;;405;81;5;;407;81;2;;23d;81;5;;409;81;c2;231;232;932;931;c2;233;234;934;933;41;28;29;b29;b2a;41;50f;510;a2e;a2f;41;22d;22e;92d;92e;41;50b;50c;a33;a32;41;53f;53e;a01;a00;41;53c;53d;a02;a03;41;53a;53b;a04;a05;41;536;537;92c;92b;41;521;520;a08;a09;41;535;534;a0a;a0b;41;533;532;a0d;a0c;41;530;531;a0f;a0e;41;52f;52e;a10;a11;41;52c;52d;a12;a13;41;52b;52a;a14;a15;41;529;528;a17;a16;41;526;527;a18;a19;41;525;524;a1b;a1a;1;c8f;4;fa;30e;81;4;fa;30b;81;4;fa;309;81;4;fa;307;81;4;fa;305;81;4;fa;301;81;4;fa;303;81;5;c9b;4;78;31a;81;4;64;310;81;1;1;5;c28;1;1;;f3c;1;1;2;d3e;1;1;2;d3f;5;5;614;81;5;5;613;81;1;1;8;d20;1;1;8;938;c2;;;;;1;1;2;d21;1;1;2;e0f;1;1;2;e10;1;1;2;e11;1;1;2;e12;1;1;8;f33;1;;2;f2b;1;1;8;f32;1;;2;f2a;1;1;8;f31;1;;2;f29;1;1;8;f30;1;;2;f28;1;1;8;f37;1;;2;f2f;1;1;8;f36;1;;2;f2e;1;1;8;f35;1;;2;f2d;1;1;8;f34;1;;2;f2c;1;;5;c29;;;5;c2a;;;|bbe\r" +
				"a>5;c2d;1;1;5;c30;;;5;c31;c2;627;626;f19;f18;c2;625;624;f1b;f1a;c2;623;622;f1d;f1c;c2;61b;61a;f25;f24;c2;621;620;f1f;f1e;c2;61f;61e;f21;f20;4;f;334;81;1;ca0;4;f;314;81;1;c9f;4;d;313;81;1;;4;;;81;;;58;d16;;;2;;c2;619;618;f27;f26;c2;51d;51c;a22;a23;c2;51a;51b;a24;a25;c2;519;518;a26;a27;c2;516;517;a28;a29;c2;515;514;a2a;a2b;c2;513;512;a2c;a2d;1;e98;5;e19;4;f;412;81;1;1;f;b28;1;1;5;c2b;1;1;5;c2c;;;5;c2f;1;1;5;c2e;41;521;520;a1e;a1f;41;51f;51e;a20;a21;41;5;4;705;704;41;7;6;707;706;41;;;;;41;9;8;709;708;41;;;;;41;a;b;70b;70a;1;1;8;c3a;1;1;8;c3b;1;1;8;c3c;1;1;8;c3d;1;1;8;c3e;41;500;511;a3e;a3f;41;505;503;a3d;a3c;41;50a;509;a34;a35;41;508;507;a36;a37;41;538;539;a06;a07;41;505;506;a38;a39;41;503;504;a3a;a3b;41;2;3;702;703;1;2;;1;e14;4;;23d;81;5;b23;5;b24;1;e88;4;14;40a;81;1;b1c;1;e89;4;f;40b;81;1;b1d;1;e8b;4;f;40d;81;1;b1f;1;e8d;4;f;40f;81;1;b21;1;e8e;4;f;410;81;5;b22;1;e96;4;2;411;81;1;b25;1;e8a;4;f;40c;81;1;b1e;1;e97;4;f;316;81;1;b26;1;e8c;4;;2b;3;4;f;40e;81;1;b20;5;b1b;1;1;1;c3f;4;;3;41;d;c;70c;70d;41;e;f;70e;70f;41;11;10;711;710;1;1;1;93f;1;1;1;93e;1;1;2;;4;;;81;1;32;1;2;80;4;4;315;81;1;1;8;d3d;1;1;2;d3c;1;1;8;d3b;1;1;2;d3a;1;1;8;d39;1;1;8;d38;1;1;2;d37;1;|1cae\r" +
				"b>1;8;d36;;;;c13;4;;ff;81;4;;602;81;4;;603;81;4;;604;81;4;;600;81;4;;605;81;4;;606;81;4;;607;81;8;e9c;5;e1d;4;f;611;81;1;1;5;e1a;1;1;5;e1b;a;19;612;81;1;1;8;e20;c2;2b;2a;72d;72c;c2;2d;2c;72e;72f;c2;2e;2f;731;730;c2;30;31;733;732;5;;;81;1;1;2;936;1;1;8;c10;1;1;2;93a;1;1;8;928;1;1;2;927;1;1;8;92a;1;1;2;929;1;1;;f3b;4;68;61c;81;1;32;1;2;e85;4;68;615;81;41;;;e1d;e1c;41;13;12;713;712;1;e87;1;e06;4;f;610;81;1;1;2;d35;1;1;a;d34;1;1;2;d33;1;1;8;d32;1;1;2;d31;5;;2c;81;5;;414;81;1;e81;5;d30;4;19;60e;81;41;26;27;72a;72b;1;e82;5;d2f;4;;60d;81;1;1;5;e03;1;1;5;e04;a;32;60f;81;1;e80;c2;523;;a1d;a1c;c2;522;;a1f;a1e;c2;14;;715;714;c2;15;;717;716;c2;16;;719;718;c2;17;;71b;71a;41;;;;;41;1b;1a;71f;71e;41;19;18;71d;71c;1;2;9b9;1;e9e;1;1;5;;1;1;5;;4;f;227;81;a;19;22a;81;41;;;;;4;68;;81;1;1;8;;41;1d;1c;721;720;41;1e;1f;722;723;41;21;20;724;725;41;23;22;727;726;1;1;1;703;4;;;81;1;32;1;2;80;4;68;;81;4;;;81;1;32;1;2;80;4;5;;81;|b1f\r" +
				"!770\r"+
				"0>;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;1;1d21;3;1;1;;3;1;;;1;1d21;;1;1;67e;8;1;;;;;;;;;;;;;;;;;;;;;;1;67e;9;1;1;67e;a;1;1;67e;f;1;1;67e;d;1;1;67e;c;1;1;1d21;1;1;1;1d21;2;1;1;1d21;4;1;1;1d21;5;1;1;;1;1;1;;2;1;1;;4;1;1;;5;1;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;1;;;1;1;67e;e;1;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;1;1d21;6;1;1;;6;1;1;67e;7;1;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;ffff;ffff;;ffff;;;;;;;;;;;;;;;;;;|1006\r"+
				"1>;;;;;;;;;;;;;;;;;;;;;;;;;17c;17c;17c;17c;17c;17c;17c;17c;17c;17c;17c;17c;32;32;32;32;32;32;32;32;32;32;32;32;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;|fd03\r"+
				":N\r"+
				"!697\r"+
				"0>;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;1;1d22;3;1;1;;3;1;;;1;1d22;;1;1;67f;8;1;;;;;;;;;;;;;;;;;;;;;;1;67e;9;1;1;67e;a;1;1;67e;f;1;1;67e;d;1;1;67e;c;1;1;1d22;1;1;1;1d22;2;1;1;1d22;4;1;1;1d21;5;1;1;;1;1;1;;2;1;1;;4;1;1;;5;1;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;1;;;1;1;67e;e;1;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;1;1d21;6;1;1;;6;1;1;67f;7;1;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;ffff;ffff;;ffff;;;;;;;;;;;;;;;;;;|4b73\r"+
				"1>;;;;;;;;;;;;;;;;;;;;;;;;;17c;17c;17c;17c;17c;17c;17c;17c;17c;17c;17c;17c;32;32;32;32;32;32;32;32;32;32;32;32;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;|fd03\r"+
				"f:4d6;10:1357;|1ecb\r"+
				"\r"+
				"!4c6\r"+
				"0>;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;1;1d21;3;1;1;;3;1;;;1;1d21;;1;1;67e;8;1;;;;;;;;;;;;;;;;;;;;;;1;67e;9;1;1;67e;a;1;1;67e;f;1;1;67e;d;1;1;67e;c;1;1;1d21;1;1;1;1d21;2;1;1;1d21;4;1;1;1d21;5;1;1;;1;1;1;;2;1;1;;4;1;1;;5;1;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;1;;;1;1;67e;e;1;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;1;1d21;6;1;1;;6;1;1;67e;7;1;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;ffff;ffff;;ffff;;;;;;;;;;;;;;;;;;|1006\r"+
				"1>;;;;;;;;;;;;;;;;;;;;;;;;;17c;17c;17c;17c;17c;17c;17c;17c;17c;17c;17c;17c;32;32;32;32;32;32;32;32;32;32;32;32;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;|fd03\r"+
				":RQDB\r"+
				"0>;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;1;1d1f;3;1;1;;3;1;;;1;1d1f;;1;1;67d;8;1;;;;;;;;;;;;;;;;;;;;;;1;67d;9;1;1;67d;a;1;1;67d;f;1;1;67d;d;1;1;67d;c;1;1;1d1f;1;1;1;1d1f;2;1;1;1d1f;4;1;1;1d1f;5;1;1;;1;1;1;;2;1;1;;4;1;1;;5;1;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;1;;;1;1;67d;e;1;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;1;1d1f;6;1;1;;6;1;1;67e;7;1;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;ffff;ffff;;ffff;;;;;;;;;;;;;;;;;;|d294\r" +
				"1>;;;;;;;;;;;;;;;;;;;;;;;;;17c;17c;17c;17c;17c;17c;17c;17c;17c;17c;17c;17c;32;32;32;32;32;32;32;32;32;32;32;32;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;4;;1;4;;1;;f;1;1;;1;4;;1;46e;135b;;;7df;c;8;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;;;1;4;;1;4;;1;;;1;4;;1;;;1;4;;1;;;1;4;;1;;;1;4;;1;;;1;4;;1;1;1;;1;;;1;4;;1;;;1;4;;1;4;;1;4;;1;;;1;4;;1;1;1;;1;;;1;4;;1;;;1;4;;1;;;1;4;;1;;4;;1;4;;1;;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;1;;7;7;;1;;113;112;114;814;813;812;;;1;4;1;;7;7;;1;;116;115;117;817;816;815;;;1;;1;;1;;1;;1;;1;4;1;|c78a\r" +
				"2>4;1;4;1;4;1;4;1;4;1;4;1;4;1;;;;;1;4;1;4;1;4;1;4;1;4;1;4;1;4;1;4;1;;;;;1;4;1;4;1;4;1;4;1;4;1;4;1;4;1;4;1;;;;;1;4;1;4;1;4;1;4;1;4;1;4;1;4;1;4;1;;;;;1;4;1;4;1;4;1;4;1;4;1;4;1;4;1;4;1;;;;;1;4;1;4;1;4;1;;1;4;1;4;1;4;1;4;1;;;;;1;4;;1;4;1;;1;4;;1;4;1;;1;;;1;4;1;;1;4;1;;1;4;1;4;1;;1;;1;4;1;;1;4;1;;1;4;1;;1;4;1;;1;4;1;;1;4;1;;1;4;1;;1;4;1;;1;4;1;;1;4;1;;1;4;1;;1;;1;;1;;1;;1;;1;;1;4;1;;;1;4;1;;;1;4;1;;;;1;;;;1;28;5;;;;1;;;;1;;;;1;;;;1;;;;1;;;;1;;;;1;;;;1;;;;1;;;;1;;;;1;;;;1;;;;1;;;;1;;;;1;;;;1;;;;1;;;;1;;;;;1;;4;;;1;80;;;;;;;4;1;;1;1;1;;1;4;;1;;;;;;;;;;;;;;1;;7;7;;1;;101;100;102;802;801;800;;7;7;;1;;104;103;105;805;804;803;;7;7;;1;;107;106;108;808;807;806;;7;7;;1;;10a;109;10b;80b;80a;809;;7;7;|f556\r" +
				"3>;1;;10d;10c;10e;80e;80d;80c;;7;7;;1;;110;10f;111;811;810;80f;1;1;;1;1;7;7;;1;;134;133;135;835;834;833;1;7;7;;1;;13a;139;13b;83b;83a;839;1;7;7;;1;;207;206;208;905;904;903;1;7;7;;1;;20d;20c;20e;90b;90a;909;1;7;7;;1;;213;212;214;911;910;90f;1;7;7;;1;;201;200;202;914;913;912;1;7;7;;1;;12b;12a;12c;82c;82b;82a;1;7;7;;1;;131;130;132;832;831;830;1;7;7;;1;;137;136;138;838;837;836;1;7;7;;1;;204;203;205;902;901;900;1;7;7;;1;;20a;209;20b;908;907;906;1;7;7;;1;;210;20f;211;90e;90d;90c;1;7;7;;1;;13d;13c;13e;83e;83d;83c;1;7;7;;1;;216;215;217;917;916;915;1;7;7;;1;;219;218;21a;91a;919;918;1;7;7;;1;;21c;21b;21d;91d;91c;91b;1;7;7;;1;;21f;21e;220;920;91f;91e;;;;1;;;;1;;;;1;;;;1;;1;;;;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;4;1;;1;;1;;1;;1;;1;;1;;1;4;1;;1;;7;7;;1;;119;118;11a;81a;819;818;;7;7;;1;;125;124;126;826;825;824;;7;7;;1;;128;127;129;829;828;827;;7;7;;1;;11c;11b;11d;81d;81c;81b;;|9a61\r" +
				"4>7;7;;1;;11f;11e;120;820;81f;81e;;7;7;;1;;122;121;123;823;822;821;;1;4;1;;4;;1;;4;;1;;4;;1;;;;;1;;;;;1;4;;1;4;;1;1;;2;;;3c;a;1;1;;1;4;;1;4;;1;1;67e;b;1;;1;;;;;;;;;;;4;;1;;;;;;;;;;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;1;;1;;1;4;1;;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;;1;4;1;;1;4;1;;1;4;1;;1;;;1;;;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;1;7;7;;1;;63e;63f;63d;f02;f01;f00;1;7;7;;1;;63b;63c;63a;f05;f04;f03;1;7;7;;1;;638;639;637;f08;f07;f06;1;7;7;;1;1;62c;62d;62b;f14;f13;f12;1;7;7;;1;;635;636;634;f0b;f0a;f09;1;7;7;;1;;632;633;631;f0e;f0d;f0c;708;5dc;1f4;7d0;5dc;384;60e;5dc;1f4;5dc;44c;1f4;7d0;708;1f4;7d0;5dc;1f4;7d0;5dc;1f4;c80;b54;1f4;a28;834;12c;7d0;5dc;1f4;;;4;1;c8;c8;c8;1f4;c8;c8;c8;c8;1f4;1f4;4;1;;1;4;;1;;;;;4;1;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;4;|b6c9\r" +
				"5>1;;1;4;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;4;;1;4;;1;4;;1;4;;1;4;;1;5;;;;;;1;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;708;7d0;1;7;7;;1;1;62f;630;62e;f11;f10;f0f;1;7;7;;1;;629;62a;628;f17;f16;f15;;4;;;1;80;;;;;;;1;1;;1;1;1;;1;1;1;;1;1;1;;1;;60e;1;1;;1;1;1;;1;1;1;;1;1;1;;1;4;;1;4;1;;1;4;1;4;1;4;1;;1;4;1;4;1;;1;4;1;4;1;;1;4;1;4;1;;1;4;1;4;1;;1;4;1;4;1;;1;4;1;4;1;;1;4;1;4;1;;1;4;1;4;1;;1;;1;4;1;4;1;4;;1;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;1;;;;;;;;;;;;;;;;;;;;;1;1;;1;1;1;;1;1;1;;1;;;;;;;;;;;;;;;;;;;;;;;4;;1;;;;;;;;;;;;;;;4;;1;;;32;;;;12c;;4;;1;;1;4;1;;1;;4;;1;4;;1;4;;1;4;;1;4;;1;|acc6\r" +
				"6>4;;1;4;;1;4;;1;;;1;;1;1;;1;;1;;1;;1;;1;;1;;1;4;1;4;1;;1;4;1;4;1;;1;;;4;;1;c8;c8;c8;c8;c8;c8;c8;c8;c8;c8;c8;c8;a0;a0;a0;a0;a0;a0;a0;a0;a0;a0;a0;a0;1;1;;1;1;1;;1;1;1;;1;1;1;;1;;;;;;;;;1;;;;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;4;;1;;;12c;4;;1;;12c;32;32;12c;32;32;12c;;;;;;;;;;;;;;;;;;1;4;1;;;1;1;1;;1;1;1;;1;4;1;4;1;;1;4;;1;4;;1;4;;1;4;;1;4;;1;;;;1;;;;1;4;1;4;1;;1;1;1;;1;4;1;4;1;;1;4;1;4;1;;1;4;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;1;1;;1;;;4;;1;4;1;4;1;4;1;;1;;1;;;;;;;1;1;;1;;;;;;1;4;;1;1e;;23;5;1e;5;2d;5;1b;5;1e;5;;1;1;;1;1;1;;1;1;1;;1;1;1;;1;;4;;1;1;;;;;;;7d0;8;;;32;;1;4;1;;;1;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;1;4;1;;;|d585\r" +
				"7>1;1;1;8;d08;1;1;2;d32;41;25;24;729;728;1;1;8;d0b;1;1;2;d35;1;1;8;d07;1;1;2;d31;1;1;8;d0a;1;1;2;d34;1;1;8;d0d;1;1;2;d37;1;1;8;d04;1;1;2;d2e;1;1;2c;d02;1;1;2;d2c;1;1;8;d05;1;1;2;d2f;1;1;8;d01;1;1;2;d2b;1;1;2c;d03;1;1;2;d2d;1;1;8;d00;1;1;2;d2a;1;1;8;d0c;1;1;2;d36;1;1;8;d09;1;1;2;d33;1;1;8;d06;1;1;2;d30;1;1;58;d15;1;1;a;d24;;;2;;1;1;a;d25;1;1;2c;d17;;;2;;1;1;2c;d18;;;2;;1;1;2c;d19;;;2;;1;1;58;d1a;;;2;;1;1;58;d1b;;;2;;1;1;58;d0e;c2;224;225;926;925;;;2;;1;1;2c;d0f;;;2;;1;1;58;d10;1;1;2;d3a;1;1;58;d11;;;2;;1;1;2c;d12;c2;221;222;923;922;;;2;;1;1;58;d13;;;2;;1;1;58;d14;;;2;;1;1;8;d26;1;1;2;d28;1;1;8;d27;1;1;2;d29;1;1;8;c36;1;;2;c32;1;1;8;c37;1;;2;c33;1;1;8;c38;1;;2;c34;1;;8;c39;1;;2;c35;1;1;8;d1c;1;1;2;d1e;1;1;8;d1d;1;1;2;d1f;1;1;f;b2d;1;1;2;b2a;1;1;8;b2b;1;1;2;b2c;1;32;1;2;c16;4;a;322;81;1;32;1;2;c17;4;;321;81;a;19;337;81;5;28;336;81;5;19;335;81;4;1e;312;81;4;f;311;81;1;1;1;2;b18;1;1;1;2;b17;1;32;1;3;c8c;1;1;1;2;c0d;1;1;1;1;c0e;1;1;1;2;c0d;1;1;1;2;c0e;1;88;1;8;b84;1388;a7;|840a\r" +
				"8>;5;5;338;81;1;1;1;2;b10;1;1;1;2;b0f;1;96;1;3;c80;1;1;1;2;c01;1;1;1;1;c02;1;1;1;2;c01;1;1;1;2;c02;1;30;1;5;b80;3778;ea;;5;5;33c;81;1;1;1;2;b12;1;1;1;2;b11;1;64;1;3;c83;1;1;1;2;c04;1;1;1;1;c05;1;1;1;2;c04;1;1;1;2;c05;1;30;1;5;b81;364c;e6;;5;5;33b;81;1;1;1;2;b16;1;1;1;2;b15;1;96;1;3;c89;1;1;1;2;c0a;1;1;1;1;c0b;1;1;1;2;c0a;1;1;1;2;c0b;1;f0;1;5;b83;35e8;10a;;5;f;320;81;1;1;1;2;b14;1;1;1;2;b13;1;64;1;3;c86;1;1;1;2;c07;1;1;1;1;c08;1;1;1;2;c07;1;1;1;2;c08;1;ac;1;;;87a;86;;5;5;33a;81;1;1;1;2;b1a;1;1;1;2;b19;1;64;1;3;c91;;;1;2;c12;1;1;1;1;c13;1;1;1;2;c12;1;1;1;2;c13;1;20;1;5;b85;1194;96;;5;9;324;81;1;;937;1;80;4;14;235;81;1;;935;1;80;4;14;236;81;;;e13;1;80;4;14;237;81;1;c95;2;;323;81;1;ca2;1;ca1;4;fa;30d;81;4;fa;30c;81;1;ca3;4;fa;30a;81;1;ca4;4;fa;308;81;1;ca5;4;fa;306;81;1;ca7;4;fa;302;81;1;ca6;4;fa;304;81;5;c9c;2;5;319;81;5;c9d;2;4;318;81;3;c9e;2;4;317;81;4;9bb;4;;33f;81;1;9bc;4;;33e;81;4;9bd;4;;33d;81;4;;238;81;4;;239;81;4;;23a;81;4;;23b;81;4;;31f;81;4;;31e;81;1;32;1;2;c98;4;;320;81;1;32;1;2;c99;4;;31c;81;1;1;1;5;;5;c;|969a\r" +
				"9>30f;89;5;;332;81;5;;331;81;5;;330;81;5;;327;81;5;;326;81;5;;325;81;5;;32c;81;5;;32f;81;5;;32e;81;5;;32d;81;5;;32a;81;5;;329;81;5;;328;81;5;;32b;81;5;8;400;81;5;8;402;81;5;;404;81;5;;406;81;5;;408;81;5;6;31d;81;1;80;4;14;333;81;41;50d;50e;a30;a31;1;;939;;;;c14;c2;;;;;5;7;401;81;5;7;403;81;5;;405;81;5;;407;81;2;;23d;81;5;;409;81;c2;231;232;932;931;c2;233;234;934;933;41;28;29;b29;b2a;41;50f;510;a2e;a2f;41;22d;22e;92d;92e;41;50b;50c;a33;a32;41;53f;53e;a01;a00;41;53c;53d;a02;a03;41;53a;53b;a04;a05;41;536;537;92c;92b;41;521;520;a08;a09;41;535;534;a0a;a0b;41;533;532;a0d;a0c;41;530;531;a0f;a0e;41;52f;52e;a10;a11;41;52c;52d;a12;a13;41;52b;52a;a14;a15;41;529;528;a17;a16;41;526;527;a18;a19;41;525;524;a1b;a1a;1;c8f;4;fa;30e;81;4;fa;30b;81;4;fa;309;81;4;fa;307;81;4;fa;305;81;4;fa;301;81;4;fa;303;81;5;c9b;4;78;31a;81;4;64;310;81;1;1;5;c28;1;1;;f3c;1;1;2;d3e;1;1;2;d3f;5;5;614;81;5;5;613;81;1;1;8;d20;1;1;8;938;c2;;;;;1;1;2;d21;1;1;2;e0f;1;1;2;e10;1;1;2;e11;1;1;2;e12;1;1;8;f33;1;;2;f2b;1;1;8;f32;1;;2;f2a;1;1;8;f31;1;;2;f29;1;1;8;f30;1;;2;f28;1;1;8;f37;1;;2;f2f;1;1;8;f36;1;;2;f2e;1;1;8;f35;1;;2;f2d;1;1;8;f34;1;;2;f2c;1;;5;c29;;;5;c2a;;;|bbe\r" +
				"a>5;c2d;1;1;5;c30;;;5;c31;c2;627;626;f19;f18;c2;625;624;f1b;f1a;c2;623;622;f1d;f1c;c2;61b;61a;f25;f24;c2;621;620;f1f;f1e;c2;61f;61e;f21;f20;4;f;334;81;1;ca0;4;f;314;81;1;c9f;4;d;313;81;1;;4;;;81;;;58;d16;;;2;;c2;619;618;f27;f26;c2;51d;51c;a22;a23;c2;51a;51b;a24;a25;c2;519;518;a26;a27;c2;516;517;a28;a29;c2;515;514;a2a;a2b;c2;513;512;a2c;a2d;1;e98;5;e19;4;f;412;81;1;1;f;b28;1;1;5;c2b;1;1;5;c2c;;;5;c2f;1;1;5;c2e;41;521;520;a1e;a1f;41;51f;51e;a20;a21;41;5;4;705;704;41;7;6;707;706;41;;;;;41;9;8;709;708;41;;;;;41;a;b;70b;70a;1;1;8;c3a;1;1;8;c3b;1;1;8;c3c;1;1;8;c3d;1;1;8;c3e;41;500;511;a3e;a3f;41;505;503;a3d;a3c;41;50a;509;a34;a35;41;508;507;a36;a37;41;538;539;a06;a07;41;505;506;a38;a39;41;503;504;a3a;a3b;41;2;3;702;703;1;2;;1;e14;4;;23d;81;5;b23;5;b24;1;e88;4;14;40a;81;1;b1c;1;e89;4;f;40b;81;1;b1d;1;e8b;4;f;40d;81;1;b1f;1;e8d;4;f;40f;81;1;b21;1;e8e;4;f;410;81;5;b22;1;e96;4;2;411;81;1;b25;1;e8a;4;f;40c;81;1;b1e;1;e97;4;f;316;81;1;b26;1;e8c;4;;2b;3;4;f;40e;81;1;b20;5;b1b;1;1;1;c3f;4;;3;41;d;c;70c;70d;41;e;f;70e;70f;41;11;10;711;710;1;1;1;93f;1;1;1;93e;1;1;2;;4;;;81;1;32;1;2;80;4;4;315;81;1;1;8;d3d;1;1;2;d3c;1;1;8;d3b;1;1;2;d3a;1;1;8;d39;1;1;8;d38;1;1;2;d37;1;|1cae\r" +
				"b>1;8;d36;;;;c13;4;;ff;81;4;;602;81;4;;603;81;4;;604;81;4;;600;81;4;;605;81;4;;606;81;4;;607;81;8;e9c;5;e1d;4;f;611;81;1;1;5;e1a;1;1;5;e1b;a;19;612;81;1;1;8;e20;c2;2b;2a;72d;72c;c2;2d;2c;72e;72f;c2;2e;2f;731;730;c2;30;31;733;732;5;;;81;1;1;2;936;1;1;8;c10;1;1;2;93a;1;1;8;928;1;1;2;927;1;1;8;92a;1;1;2;929;1;1;;f3b;4;68;61c;81;1;32;1;2;e85;4;68;615;81;41;;;e1d;e1c;41;13;12;713;712;1;e87;1;e06;4;f;610;81;1;1;2;d35;1;1;a;d34;1;1;2;d33;1;1;8;d32;1;1;2;d31;5;;2c;81;5;;414;81;1;e81;5;d30;4;19;60e;81;41;26;27;72a;72b;1;e82;5;d2f;4;;60d;81;1;1;5;e03;1;1;5;e04;a;32;60f;81;1;e80;c2;523;;a1d;a1c;c2;522;;a1f;a1e;c2;14;;715;714;c2;15;;717;716;c2;16;;719;718;c2;17;;71b;71a;41;;;;;41;1b;1a;71f;71e;41;19;18;71d;71c;1;2;9b9;1;e9e;1;1;5;;1;1;5;;4;f;227;81;a;19;22a;81;41;;;;;4;68;;81;1;1;8;;41;1d;1c;721;720;41;1e;1f;722;723;41;21;20;724;725;41;23;22;727;726;1;1;1;703;4;;;81;1;32;1;2;80;4;68;;81;4;;;81;1;32;1;2;80;4;5;;81;|b1f\r" +
				"!770\r"+
				"\r"+
				"\r"+
				"\r"+
				"\r");
		
//0>;;;;;;;;;;;;;;1;1;;1;1;1;1;255;1;;;1;1;1;1;;1;1;;1;1;1;1;;1;;;1;1;1;1;53;1;1;20;1;1;1;1;;1;1;;1;;1;;;;;;;;;;;;;;;;1;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;3;1;;;3;1;;;;;;1;;1;8;1;;;;;;;;;;;;;;;;;;;;;1;;2;9;1;;2;a;1;;1;f;1;;2;d;1;;2;c;1;;;1;1;;;2;1;;;4;1;;;5;1;;;1;1;;;2;1;;;4;1;;;5;1;1;;;;;;;;;;;;;;1;;;;1;;;;1;;;;1;;;;1;;1;1;;;;;;;;;1;;1;e;1;;1;;;;;;;;;;;;;;;;41;fffc;13;1;1b;60;b;c;fff6;;1;;;1;;;;;1;;1;;;6;1;;;6;1;;1;7;1;;;;;;;1;;1;;;1;1;1;;;1;;;1;1;;;;1;;;;1;;;;1;;;;1;;;;1;;;;1;;;;;;;1;1;;;;;;ffff;ffff;;ffff;;;;;;;;;;;;;;;;;;|daca	
		
		assertTrue( m.execute() );
		String out = "%0\r%1\r%2\r%3\r%4\r%5\r%6\r%7\r%8\r%9\r%A\r%B\r;RQEN\r*\r";
		assertEquals( out, ser.getBuffWriteString());
		
		assertTrue( m.execute() );
		out = "+0\r+1\r:\r*\r";
		assertEquals( out, ser.getBuffWriteString());
		
		assertTrue( m.execute() );
		out = "+0\r+1\r:\r;OK\r*\r";
		assertEquals( out, ser.getBuffWriteString());

		assertTrue( m.execute() );
		out = "+0\r+1\r:\r%0\r%1\r%2\r%3\r%4\r%5\r%6\r%7\r%8\r%9\r%A\r%B\r;RQEN\r*\r";
		assertEquals( out, ser.getBuffWriteString());
		
	}


}

/*
















<-- 
--> +0
<-- 0>;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;1;1d24;3;1;1;;3;1;;;1;1d24;;1;1;67f;8;1;;;;;;;;;;;;;;;;;;;;;;1;67f;9;1;1;67f;a;1;1;67e;f;1;1;67f;d;1;1;67f;c;1;1;1d24;1;1;1;1d24;2;1;1;1d24;4;1;1;1d24;5;1;1;;1;1;1;;2;1;1;;4;1;1;;5;1;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;1;;;1;1;67e;e;1;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;1;1d24;6;1;1;;6;1;1;67f;7;1;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;ffff;ffff;;ffff;;;;;;;;;;;;;;;;;;|548d
--> +1
<-- 1>;;;;;;;;;;;;;;;;;;;;;;;;;17c;17c;17c;17c;17c;17c;17c;17c;17c;17c;17c;17c;32;32;32;32;32;32;32;32;32;32;32;32;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;|fd03
--> :
<-- 492:67f;|cde3
--> ;OK
<-- 
--> *
<-- !4c6
--> +0
<-- 0>;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;1;1d25;3;1;1;;3;1;;;1;1d25;;1;1;67f;8;1;;;;;;;;;;;;;;;;;;;;;;1;67f;9;1;1;67f;a;1;1;67f;f;1;1;67f;d;1;1;67f;c;1;1;1d25;1;1;1;1d25;2;1;1;1d25;4;1;1;1d25;5;1;1;;1;1;1;;2;1;1;;4;1;1;;5;1;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;1;;;1;1;67f;e;1;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;1;1d25;6;1;1;;6;1;1;67f;7;1;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;ffff;ffff;;ffff;;;;;;;;;;;;;;;;;;|7920
--> +1
<-- 1>;;;;;;;;;;;;;;;;;;;;;;;;;17c;17c;17c;17c;17c;17c;17c;17c;17c;17c;17c;17c;32;32;32;32;32;32;32;32;32;32;32;32;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;|fd03
--> :
<-- f:55d;10:1167;|1360
--> ;OK
<-- 
--> *
<-- !55d
--> +0
<-- 0>;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;1;1d26;3;1;1;;3;1;;;1;1d26;;1;1;680;8;1;;;;;;;;;;;;;;;;;;;;;;1;680;9;1;1;67f;a;1;1;67f;f;1;1;67f;d;1;1;67f;c;1;1;1d26;1;1;1;1d26;2;1;1;1d25;4;1;1;1d25;5;1;1;;1;1;1;;2;1;1;;4;1;1;;5;1;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;1;;;1;1;67f;e;1;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;1;1d25;6;1;1;;6;1;1;680;7;1;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;ffff;ffff;;ffff;;;;;;;;;;;;;;;;;;|8ee7
--> +1
<-- 1>;;;;;;;;;;;;;;;;;;;;;;;;;17c;17c;17c;17c;17c;17c;17c;17c;17c;17c;17c;17c;32;32;32;32;32;32;32;32;32;32;32;32;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;|fd03
--> :
<-- :N
--> *
<-- !4fb
--> +0
<-- 0>;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;1;1d26;3;1;1;;3;1;;;1;1d27;;1;1;680;8;1;;;;;;;;;;;;;;;;;;;;;;1;680;9;1;1;680;a;1;1;67f;f;1;1;67f;d;1;1;67f;c;1;1;1d27;1;1;1;1d26;2;1;1;1d26;4;1;1;1d26;5;1;1;;1;1;1;;2;1;1;;4;1;1;;5;1;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;1;;;1;1;67f;e;1;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;1;1d26;6;1;1;;6;1;1;680;7;1;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;ffff;ffff;;ffff;;;;;;;;;;;;;;;;;;|aad7
--> +1
<-- 1>;;;;;;;;;;;;;;;;;;;;;;;;;17c;17c;17c;17c;17c;17c;17c;17c;17c;17c;17c;17c;32;32;32;32;32;32;32;32;32;32;32;32;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;|fd03
--> :
<-- :N
--> *
<-- !4cf
*/