package promauto.jroboplc.plugin.peripherial;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;

import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;

import promauto.jroboplc.core.api.InitUtils;
import promauto.jroboplc.core.api.SampleEnvironment;
import promauto.jroboplc.core.api.SamplePlugin;
import promauto.jroboplc.core.api.SampleSerialManager;
import promauto.jroboplc.core.api.SampleSerialPort;
import promauto.jroboplc.core.ConfigurationYaml;
import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.EnvironmentInst;

public class NotisA100ModuleTest {
	private static String CFG_DIR = InitUtils.getResourcesDir("peripherial", PeripherialModule.class);

	NotisA100Module m;
	SampleSerialPort ser;
	SampleSerialManager sm;


	@Before
	public void setUp() throws Exception {
		InitUtils.setupLogger();

		SampleEnvironment env = new SampleEnvironment();
		EnvironmentInst.set(env);
		
		Configuration conf = new ConfigurationYaml( CFG_DIR );
		if( !conf.load() ) 
			fail();
		env.setConfiguration(conf);

		SamplePlugin plug = new SamplePlugin();

		env.setSerialManager( sm = new SampleSerialManager() );
    	ser = sm.port;

		
		m = new NotisA100Module(plug, "notis");
		m.load(new HashMap<String,String>());
		m.retrial = 3;
		m.netaddr = 1;
	}

	
	
//  Out: 81 00 18 14 00 00 00 00 00 00 00 2A
//  In:  80 01 18 14 22 04 63 19 1D 2F 00 2E

//  Out: 81 00 18 13 00 00 00 00 00 00 00 63
//  In : 80 01 18 13 20 08 54 00 00 00 02 45

	@Test
	public void testExecuteRead() {
		ser.setBuffRead(new int[] {
			0x80, 0x01, 0x18, 0x14, 0x22, 0x04, 0x63, 0x19, 0x1D, 0x2F, 0x00, 0x2E,
			0x80, 0x01, 0x18, 0x13, 0x20, 0x08, 0x54, 0x00, 0x00, 0x00, 0x02, 0x45
		});
		
		
		m.execute();
		
		assertFalse( m.tagError.getBool());
		assertEquals( 0, m.tagErrorCnt.getInt());

		assertEquals( 130, 			m.tagCrc.getInt());
		assertEquals( 790436195, 	m.tagSumWeight.getInt());
		assertEquals( 84, 			m.tagSumNum.getInt());
		assertEquals( 34, 			m.tagState.getInt());
		assertEquals( 4, 			m.tagCode.getInt());
	}

}


//out:
//DD DD 3C 7 0 0 0 1 1 23 0 0 0 0 0 1 12 34 56 78 0 0 0 0 0 0 0 1 2 34 0 0 0 0 0 1 23 45 67 89 0 0 0 0 0 0 0 0 0 0 0 1 3 45 0 1 5 67 20 20
//DD DD 3C 7 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 20 20
//
//in:
//DD DD 3C 07 0 0 0 0 1 2 3 4 5 6 7 8 1 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 2 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 1 20 20













