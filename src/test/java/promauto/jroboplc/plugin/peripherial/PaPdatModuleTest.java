package promauto.jroboplc.plugin.peripherial;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;

import java.util.Arrays;
import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;

import promauto.jroboplc.core.api.InitUtils;
import promauto.jroboplc.core.api.SampleEnvironment;
import promauto.jroboplc.core.api.SamplePlugin;
import promauto.jroboplc.core.ConfigurationYaml;
import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.EnvironmentInst;

public class PaPdatModuleTest {
	private static String CFG_DIR = InitUtils.getResourcesDir("peripherial", PeripherialModule.class);

	PaPdatModule m;
	SampleProtocolAA55 prot;


	@Before
	public void setUp() throws Exception {
		InitUtils.setupLogger();

		SampleEnvironment env = new SampleEnvironment();
		EnvironmentInst.set(env);
		
		Configuration conf = new ConfigurationYaml( CFG_DIR );
		if( !conf.load() ) 
			fail();
		env.setConfiguration(conf);

		SamplePlugin plug = new SamplePlugin();

		m = new PaPdatModule(plug, "dat");
		m.load(new HashMap<String,String>());
		m.retrial = 3;
		m.netaddr = 9;

		prot = new SampleProtocolAA55(m);
		m.protocol = prot; 
	}

	
	
	@Test
	public void testExecuteGood() {
		
		prot.buffin.push( new int[]{0xFF,0x00, 0x00,0xFF, 0x55,0xAA, 0x11,0xEE, 0xFD} );

		prot.buffin.push( new int[]{0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x09} );
		prot.buffin.push( new int[]{0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x09} );
		prot.buffin.push( new int[]{0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x09} );
		prot.buffin.push( new int[]{0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x09} );
		m.execute();
		
		assertFalse( m.tagError.getBool());
		assertEquals( 0, m.tagErrorCnt.getInt());
		
		
		int[] expout1 = new int[]{0x55, 0xA9};
		int[] expout2 = new int[]{0x55, 0x29};
		int[] expout3 = new int[]{0x55, 0x49};
		int[] expout4 = new int[]{0x55, 0x69};
		int[] expout5 = new int[]{0x55, 0x89};
		
		int[] actout5 = prot.buffout.pollFirst();		
		int[] actout4 = prot.buffout.pollFirst();		
		int[] actout3 = prot.buffout.pollFirst();
		int[] actout2 = prot.buffout.pollFirst();
		int[] actout1 = prot.buffout.pollFirst();
		assertArrayEquals(expout1, actout1);
		assertArrayEquals(expout2, actout2);
		assertArrayEquals(expout3, actout3);
		assertArrayEquals(expout4, actout4);		
		assertArrayEquals(expout5, actout5);
		
		
		for(int i=0; i<8; i++)
			assertEquals( 1, m.inps[ ProtocolAA55.pRksDatConv[i] ].getInt());
		
		for(int i=8; i<16; i++)
			assertEquals( 0, m.inps[ ProtocolAA55.pRksDatConv[i] ].getInt());
		
		for(int i=32; i<40; i+=2) {
			assertEquals( 0, m.inps[ ProtocolAA55.pRksDatConv[i] ].getInt());
			assertEquals( 0, m.inps[ ProtocolAA55.pRksDatConv[i+1] ].getInt());
		}
		
		
		int[] trueInp=new int[64];
		int[] expin = new int[64];
		
		Arrays.fill(expin, 0);
		for(int i=0;i<64;i++){
			trueInp[i]=m.frqs[i].getInt();
			expin[i]=0xb5;
		}
		assertArrayEquals( expin, trueInp);
		
	}
	
	
	
}

