package promauto.jroboplc.plugin.peripherial;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;

import java.util.Arrays;
import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;

import promauto.jroboplc.core.api.InitUtils;
import promauto.jroboplc.core.api.SampleEnvironment;
import promauto.jroboplc.core.api.SamplePlugin;
import promauto.jroboplc.core.ConfigurationYaml;
import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.EnvironmentInst;

public class PaPrksModuleTest {
	private static String CFG_DIR = InitUtils.getResourcesDir("peripherial", PeripherialModule.class);

	PaPrksModule m;
	SampleProtocolAA55 prot;


	@Before
	public void setUp() throws Exception {
		InitUtils.setupLogger();

		SampleEnvironment env = new SampleEnvironment();
		EnvironmentInst.set(env);
		
		Configuration conf = new ConfigurationYaml( CFG_DIR );
		if( !conf.load() ) 
			fail();
		env.setConfiguration(conf);

		SamplePlugin plug = new SamplePlugin();

		m = new PaPrksModule(plug, "rks");
		m.load(new HashMap<String,String>());
		m.retrial = 3;
		m.netaddr = 9;

		prot = new SampleProtocolAA55(m);
		m.protocol = prot; 
	}

	
	
	@Test
	public void testExecuteGood() {
		
		prot.buffin.push( new int[]{0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x09} );
		prot.buffin.push( new int[]{0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x09} );
		prot.buffin.push( new int[]{0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x09} );
		prot.buffin.push( new int[]{0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x09} );
		m.execute();
		
		assertFalse( m.tagError.getBool());
		assertEquals( 0, m.tagErrorCnt.getInt());
		
		
		int[] expout1 = new int[]{0x55, 0x29};
		int[] expout2 = new int[]{0x55, 0x49};
		int[] expout3 = new int[]{0x55, 0x69};
		int[] expout4 = new int[]{0x55, 0x89};
		
		int[] actout4 = prot.buffout.pollFirst();		
		int[] actout3 = prot.buffout.pollFirst();
		int[] actout2 = prot.buffout.pollFirst();
		int[] actout1 = prot.buffout.pollFirst();
		assertArrayEquals("Check data to send", expout1, actout1);
		assertArrayEquals("Check data to send", expout2, actout2);
		assertArrayEquals("Check data to send", expout3, actout3);
		assertArrayEquals("Check data to send", expout4, actout4);		
		
		int[] inp = new int[4]; 
		
		Arrays.fill(inp, 0);
		for (int i=0; i<4; i++) { 
			for (int j=0; j<16; j++)
				inp[i] += m.inps[i*16+j].getInt() << j;			
		}
		int[] expin = new int[]{0xFFFF, 0xFFFF,0xFFFF,0xFFFF};
		assertArrayEquals("Check data in the inputs tags", expin, inp);		
	}
	

	@Test
	public void testExecuteGood2() {
		
		prot.buffin.push( new int[]{0x80,0x81,0x82,0x83,0x84,0x85,0x86,0x87,0x88,0x89,0x8A,0x8B,0x8C,0x8D,0x8E,0x8F,0x81} );
		prot.buffin.push( new int[]{0x80,0x81,0x82,0x83,0x84,0x85,0x86,0x87,0x88,0x89,0x8A,0x8B,0x8C,0x8D,0x8E,0x8F,0x81} );
		prot.buffin.push( new int[]{0x80,0x81,0x82,0x83,0x84,0x85,0x86,0x87,0x88,0x89,0x8A,0x8B,0x8C,0x8D,0x8E,0x8F,0x81} );
		prot.buffin.push( new int[]{0x80,0x81,0x82,0x83,0x84,0x85,0x86,0x87,0x88,0x89,0x8A,0x8B,0x8C,0x8D,0x8E,0x8F,0x81} );
		m.execute();
		
		assertFalse( m.tagError.getBool());
		assertEquals( 0, m.tagErrorCnt.getInt());
		
		
		int[] freq = new int[] {0,0,1,1,1,1,2,2,2,2,3,3,3,3,4,4};
		
		for (int i=0; i<64; i++) {
			assertEquals( "freq " + i, freq[i%16], m.frqs[getRksConvReverted(i)].getInt());
		}

	}



	private int getRksConvReverted(int value) {
		for (int i=0; i<64; i++)
			if (ProtocolAA55.pRksDatConv[i] == value)
				return i;
				
		return 0;
	}
	
	

	
	
}


