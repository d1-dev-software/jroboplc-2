package promauto.jroboplc.plugin.peripherial;

import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.LinkedList;

public class SampleProtocolIcpcon extends ProtocolIcpcon {
	private final static Charset charset = Charset.forName("UTF-8");
	
	public LinkedList<int[]> in = new LinkedList<>();
	public LinkedList<int[]> out = new LinkedList<>();
	
	public SampleProtocolIcpcon(PeripherialModule module) {
		super(module);
	}

	@Override
    public int request(int[] buffout, int sizeout, int[] buffin, int sizein) throws Exception {
		out.push( Arrays.copyOfRange(buffout, 0, sizeout) );
		
		int[] buff = in.pollLast();
		if (buff==null)
			return -1;
		
		for(int i=0; (i<sizein) && (i<buff.length); i++)
			buffin[i] = buff[i];
		return buff.length;
	}
	
	public void pushIn(String data) {
		int n = data.length();
		int[] a = new int[n];
		byte[] b = data.getBytes(charset);
		for(int i=0; i<n; ++i)
			a[i] = b[i];
		
		in.push( a );
	}

	public String pollOut() {
		int[] a = out.pollLast();
		String s = new String(a, 0, a.length);
		return s;
	}


}
