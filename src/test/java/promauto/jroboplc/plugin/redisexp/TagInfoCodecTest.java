package promauto.jroboplc.plugin.redisexp;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class TagInfoCodecTest {

    public static final String NAME  = "123465789012345";
    public static final String VALUE = "!S0000000FFFF Hello";

    @Before
    public void setUp() throws Exception {

    }

    @Test
    public void test_encodeName() {
        assertEquals("0F"+ NAME, TagInfoCodec.encodeName(NAME));
    }

    @Test
    public void test_encodeValue() {
        assertEquals("=D0000000FFFF " + VALUE, TagInfoCodec.encodeData(VALUE, 65535, 'D', true));
        assertEquals("!D0000000FFFF " + VALUE, TagInfoCodec.encodeData(VALUE, 65535, 'D', false));
    }

    @Test
    public void test_encodeMessageUpdate() {
        assertEquals(NAME + VALUE, TagInfoCodec.encodeMessageUpdate(NAME, VALUE));
    }

    @Test
    public void test_encodeDelete() {
        assertEquals("0000000FFFF " + NAME, TagInfoCodec.encodeMessageDelete(NAME, 65535));
    }

    @Test
    public void test_encodeWrite() {
        assertEquals("0F" + NAME + " " + VALUE, TagInfoCodec.encodeMessageWrite(NAME, VALUE));
    }

    @Test
    public void test_decodeWrite() {
        TagInfo result = TagInfoCodec.decodeMessageWrite("0F" + NAME + " " + VALUE);
        assertEquals(NAME, result.name);
        assertEquals(VALUE, result.value);
    }

    @Test
    public void test_decodeUpdate() {
        TagInfo result = TagInfoCodec.decodeMessageUpdate("0F" + NAME + VALUE);
        assertEquals(NAME, result.name);
        assertEquals(VALUE, result.data);
        assertEquals("Hello", result.value);
        assertEquals('S', result.type);
        assertEquals(65535, result.time);
        assertFalse(result.good);
    }

    @Test
    public void test_decodeDelete() {
        TagInfo result = TagInfoCodec.decodeMessageDelete("0000000FFFF " + NAME);
        assertEquals(NAME, result.name);
        assertEquals(65535, result.time);
    }

}