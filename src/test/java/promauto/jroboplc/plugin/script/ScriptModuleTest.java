package promauto.jroboplc.plugin.script;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;

import promauto.jroboplc.core.api.InitUtils;
import promauto.jroboplc.core.api.SamplePlugin;
import promauto.jroboplc.core.ConfigurationYaml;
import promauto.jroboplc.core.api.Environment;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.Module;
import promauto.jroboplc.core.api.ModuleManager;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.TagTable;

public class ScriptModuleTest {

	private static String CFG_DIR = InitUtils.getResourcesDir("script", ScriptModuleTest.class);

	Environment env;
	ScriptModule mod;
	
	private Tag tag_glsa301state;

	@Before
	public void setUp() {
		
		InitUtils.setupLogger();
		
		TagTable tt1 = new TagTable();
		Module gelios = mock(Module.class);
		when(gelios.getName()).thenReturn("gelios1");
		when(gelios.getTagTable()).thenReturn(tt1);
		tt1.createInt("State", 10); 

		TagTable tt2 = new TagTable();
		Module shdsh = mock(Module.class);
		when(shdsh.getName()).thenReturn("SHDSH");
		when(shdsh.getTagTable()).thenReturn(tt2);
		tag_glsa301state = tt2.createInt("GLSA_301_State", 20); 
		tt2.createInt("GLSA_301_Sost", 30); 

		ModuleManager mm = mock(ModuleManager.class);
		when(mm.getModule("gelios1")).thenReturn(gelios);
		when(mm.getModule("SHDSH")).thenReturn(shdsh);
		
		TagTable tt = new TagTable();

		env = mock(Environment.class);
		EnvironmentInst.set(env);
		when(env.getModuleManager()).thenReturn(mm);

		SamplePlugin plugin = new SamplePlugin();
		plugin.name = "script";
		mod = new ScriptModule(plugin, "SHDSH1");
		
		when(mm.getModule("SHDSH1")).thenReturn(mod);
	}

	
	@Test
	public void testExecute() {
		when(env.getConfiguration()).thenReturn( new ConfigurationYaml( CFG_DIR + "1" ) );
		env.getConfiguration().load();
		Object conf = env.getConfiguration().getModuleConf("script", "SHDSH1");
		assertTrue( mod.load(conf) );
		assertTrue( mod.prepare() );
		
		Tag res1 = mod.getTagTable().get("301_StateSost");
		Tag res2 = mod.getTagTable().get("301_StateSost_1x");
		
		assertNotNull(res1);
		assertNotNull(res2);
		
		assertEquals( Tag.Type.DOUBLE, res1.getType() );
		assertEquals( Tag.Type.DOUBLE, res2.getType() );
		
		assertEquals(0d, res1.getDouble(), 0.001);
		assertEquals(0d, res2.getDouble(), 0.001);
		
		assertEquals(20, tag_glsa301state.getInt() );
		
		mod.execute();
		
		assertEquals(10, tag_glsa301state.getInt() );
		assertEquals(137.3, res1.getDouble(), 0.001 );
		assertEquals(68.65, res2.getDouble(), 0.001 );
		
	}

	
	@Test
	public void testReload() {
		when(env.getConfiguration()).thenReturn( new ConfigurationYaml( CFG_DIR + "1" ) );
		env.getConfiguration().load();
		Object conf = env.getConfiguration().getModuleConf("script", "SHDSH1");
		assertTrue( mod.load(conf) );
		assertTrue( mod.prepare() );
		
		Tag res1 = mod.getTagTable().get("301_StateSost");
		Tag res2 = mod.getTagTable().get("301_StateSost_1x");
		
		mod.execute();
		assertEquals(137.3, res1.getDouble(), 0.001 );
		assertEquals(68.65, res2.getDouble(), 0.001 );
		
		
		when(env.getConfiguration()).thenReturn( new ConfigurationYaml( CFG_DIR + "2" ) );
		env.getConfiguration().load();
		
		mod.requestReload(null);

		Tag new1 = mod.getTagTable().get("301_StateSost");
		Tag new2 = mod.getTagTable().get("301_StateSost_1x");
		Tag new3 = mod.getTagTable().get("301_StateSost_1y");
		
		assertSame(res1, new1);
		assertNull(new2);
		assertNotNull(new3);
		assertEquals(Tag.Status.Deleted, res2.getStatus());
		
		mod.execute();
		assertEquals(137.4, new1.getDouble(), 0.001 );
		assertEquals(168.7, new3.getDouble(), 0.001 );

	}

	
	@Test
	public void testEnable() {
		when(env.getConfiguration()).thenReturn( new ConfigurationYaml( CFG_DIR + "3" ) );
		env.getConfiguration().load();
		Object conf = env.getConfiguration().getModuleConf("script", "SHDSH1");
		assertTrue( mod.load(conf) );
		assertTrue( mod.prepare() );
		
		mod.execute();
		
		assertEquals(20, tag_glsa301state.getInt() );
		Tag res1 = mod.getTagTable().get("301_StateSost");
		Tag res2 = mod.getTagTable().get("301_StateSost_1y");

		assertEquals(237.4, res1.getDouble(), 0.001 );
		assertEquals(0, res2.getDouble(), 0.001 );
	}

}
