package promauto.jroboplc.plugin.script;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;

import promauto.jroboplc.core.tags.TagInt;
import promauto.jroboplc.core.api.Module;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.Ref;

public class ActionCopyTest {

	ActionCopy ac;

	ScriptModule mod;
	Module mod1;
	Module mod2;
	Tag tag1;
	Tag tag2;

	
	@Before
	public void setUp() {
		mod = mock(ScriptModule.class);
		ac = new ActionCopy(mod);

		tag1 = new TagInt("tag1", 1);
		tag2 = new TagInt("tag2", 2);

		ac.src = mock(Ref.class);
		ac.dst = mock(Ref.class);
		
		when(ac.src.getTag()).thenReturn(tag1);
		when(ac.dst.getTag()).thenReturn(tag2);
		
		when(ac.src.linkIfNotValid()).thenReturn(true);
		when(ac.dst.linkIfNotValid()).thenReturn(true);
		
	}

	
	@Test
	public void testExecute() {
		assertEquals(1, tag1.getInt() );
		assertEquals(2, tag2.getInt() );
		ac.execute();
		assertEquals(1, tag1.getInt() );
		assertEquals(1, tag2.getInt() );
	}

	
	@Test
	public void testExecuteDelay() throws InterruptedException {
		ac.delay_ms = 60;
		
		assertEquals(2, tag2.getInt() );

		tag1.setInt(1);
		ac.execute();
		assertEquals(2, tag2.getInt() );
		Thread.sleep(30);
		ac.execute();
		assertEquals(2, tag2.getInt() );
		Thread.sleep(40);
		ac.execute();
		assertEquals(1, tag2.getInt() );
		Thread.sleep(40);
		ac.execute();
		assertEquals(1, tag2.getInt() );
		
		
		tag1.setInt(3);
		ac.execute();
		assertEquals(1, tag2.getInt() );
		Thread.sleep(30);
		ac.execute();
		assertEquals(1, tag2.getInt() );
		Thread.sleep(40);
		ac.execute();
		assertEquals(3, tag2.getInt() );

		
		tag1.setInt(7);
		ac.execute();
		assertEquals(3, tag2.getInt() );
		Thread.sleep(30);
		ac.execute();
		assertEquals(3, tag2.getInt() );
		Thread.sleep(30);
		tag1.setInt(3);
		ac.execute();
		assertEquals(3, tag2.getInt() );
		Thread.sleep(30);
		ac.execute();
		assertEquals(3, tag2.getInt() );
	}

}
