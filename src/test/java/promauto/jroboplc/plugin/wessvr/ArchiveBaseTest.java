package promauto.jroboplc.plugin.wessvr;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

import java.time.LocalDateTime;
import promauto.jroboplc.core.api.Database;

public class ArchiveBaseTest {

    WessvrModule module;
    DeviceBase device;
    ArchiveConfig.Item cfg;

    @Before
    public void setUp() {
        Database database = mock(Database.class);
        when(database.getTimestampFormatter()).thenReturn(null);

        module = mock(WessvrModule.class);
        when(module.getDatabase()).thenReturn(database);

        device = mock(DeviceBase.class, withSettings().useConstructor(module));

        device.shiftset = new Shiftset();
        device.shiftset.addShift(1, 800);
        device.shiftset.addShift(2, 2000);

        cfg = new ArchiveConfig.Item("MAIN", "STAT", 0);

    }

    private LocalDateTime time(int hhmm) {
        return LocalDateTime.of(2021, 12, 10, hhmm/100, hhmm%100);
    }


    @Test
    public void testCalcPeriod_interval_0() {
        cfg.interval = 0;
        ArchiveBase ab = new ArchiveBase(device, cfg);

        assertEquals(202112092, ab.calcPeriod(time(0)));
        assertEquals(202112092, ab.calcPeriod(time(759)));
        assertEquals(202112101, ab.calcPeriod(time(800)));
        assertEquals(202112101, ab.calcPeriod(time(1959)));
        assertEquals(202112102, ab.calcPeriod(time(2000)));
        assertEquals(202112102, ab.calcPeriod(time(2359)));
    }


    @Test
    public void testCalcPeriod_interval_5() {
        cfg.interval = 5;
        ArchiveBase ab = new ArchiveBase(device, cfg);

        assertEquals(2112100000, ab.calcPeriod(time(0)));
        assertEquals(2112100755, ab.calcPeriod(time(759)));
        assertEquals(2112100800, ab.calcPeriod(time(800)));
        assertEquals(2112101955, ab.calcPeriod(time(1959)));
        assertEquals(2112102000, ab.calcPeriod(time(2000)));
        assertEquals(2112102355, ab.calcPeriod(time(2359)));
    }

    @Test
    public void testCalcPeriod_interval_30() {
        cfg.interval = 30;
        ArchiveBase ab = new ArchiveBase(device, cfg);

        assertEquals(2112100000, ab.calcPeriod(time(0)));
        assertEquals(2112100730, ab.calcPeriod(time(759)));
        assertEquals(2112100800, ab.calcPeriod(time(800)));
        assertEquals(2112101930, ab.calcPeriod(time(1959)));
        assertEquals(2112102000, ab.calcPeriod(time(2000)));
        assertEquals(2112102330, ab.calcPeriod(time(2359)));
    }

    @Test
    public void testCalcPeriod_interval_60() {
        cfg.interval = 60;
        ArchiveBase ab = new ArchiveBase(device, cfg);

        assertEquals(2021121000, ab.calcPeriod(time(0)));
        assertEquals(2021121007, ab.calcPeriod(time(759)));
        assertEquals(2021121008, ab.calcPeriod(time(800)));
        assertEquals(2021121019, ab.calcPeriod(time(1959)));
        assertEquals(2021121020, ab.calcPeriod(time(2000)));
        assertEquals(2021121023, ab.calcPeriod(time(2359)));
    }

    @Test
    public void testCalcPeriod_interval_120() {
        cfg.interval = 120;
        ArchiveBase ab = new ArchiveBase(device, cfg);

        assertEquals(2021121000, ab.calcPeriod(time(0)));
        assertEquals(2021121006, ab.calcPeriod(time(759)));
        assertEquals(2021121008, ab.calcPeriod(time(800)));
        assertEquals(2021121018, ab.calcPeriod(time(1959)));
        assertEquals(2021121020, ab.calcPeriod(time(2000)));
        assertEquals(2021121022, ab.calcPeriod(time(2359)));
    }

    @Test
    public void testCalcPeriod_interval_240() {
        cfg.interval = 240;
        ArchiveBase ab = new ArchiveBase(device, cfg);

        assertEquals(2021121000, ab.calcPeriod(time(0)));
        assertEquals(2021121004, ab.calcPeriod(time(759)));
        assertEquals(2021121008, ab.calcPeriod(time(800)));
        assertEquals(2021121016, ab.calcPeriod(time(1959)));
        assertEquals(2021121020, ab.calcPeriod(time(2000)));
        assertEquals(2021121020, ab.calcPeriod(time(2359)));
    }

    @Test
    public void testCalcPeriod_interval_1440() {
        cfg.interval = 1440;
        ArchiveBase ab = new ArchiveBase(device, cfg);

        assertEquals(20211210, ab.calcPeriod(time(0)));
        assertEquals(20211210, ab.calcPeriod(time(759)));
        assertEquals(20211210, ab.calcPeriod(time(800)));
        assertEquals(20211210, ab.calcPeriod(time(1959)));
        assertEquals(20211210, ab.calcPeriod(time(2000)));
        assertEquals(20211210, ab.calcPeriod(time(2359)));
    }



}