package promauto.jroboplc.plugin.wessvr;

import org.junit.Test;

import java.time.LocalDateTime;

import static org.junit.Assert.*;

public class ShiftsetTest {

    private LocalDateTime time(int hhmm) {
        return LocalDateTime.of(2021, 12, 10, hhmm/100, hhmm%100);
    }

    @Test
    public void test_TwoShifts() {
        Shiftset ss = new Shiftset();
        ss.addShift(1, 800);
        ss.addShift(2, 2000);

        assertEquals(2, ss.getShiftNum( time(0)) );
        assertEquals(2, ss.getShiftNum( time(759)) );
        assertEquals(1, ss.getShiftNum( time(800)) );
        assertEquals(1, ss.getShiftNum( time(1959)) );
        assertEquals(2, ss.getShiftNum( time(2000)) );
        assertEquals(2, ss.getShiftNum( time(2359)) );

        assertTrue( ss.isAfterMidnight( time(0)) );
        assertTrue( ss.isAfterMidnight( time(759)) );
        assertFalse( ss.isAfterMidnight( time(800)) );
        assertFalse( ss.isAfterMidnight( time(1959)) );
        assertFalse( ss.isAfterMidnight( time(2000)) );
        assertFalse( ss.isAfterMidnight( time(2359)) );
    }


    @Test
    public void test_ThreeShifts() {
        Shiftset ss = new Shiftset();
        ss.addShift(2, 1600);
        ss.addShift(1, 800);
        ss.addShift(3, 0);

        assertEquals(3, ss.getShiftNum( time(0)) );
        assertEquals(3, ss.getShiftNum( time(759)) );
        assertEquals(1, ss.getShiftNum( time(800)) );
        assertEquals(1, ss.getShiftNum( time(1559)) );
        assertEquals(2, ss.getShiftNum( time(1600)) );
        assertEquals(2, ss.getShiftNum( time(2359)) );

        assertFalse( ss.isAfterMidnight( time(0)) );
        assertFalse( ss.isAfterMidnight( time(759)) );
        assertFalse( ss.isAfterMidnight( time(800)) );
        assertFalse( ss.isAfterMidnight( time(1559)) );
        assertFalse( ss.isAfterMidnight( time(1600)) );
        assertFalse( ss.isAfterMidnight( time(2359)) );
    }


}