package promauto.jroboplc.plugin.jrbustcp;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufUtil;
import io.netty.buffer.Unpooled;
import io.netty.channel.embedded.EmbeddedChannel;
import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.spi.LoggingEvent;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import promauto.jroboplc.core.api.*;
import promauto.jroboplc.core.tags.*;
import promauto.utils.Strings;

import java.nio.charset.Charset;
import java.util.zip.CRC32;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class JrbustcpClientModuleTest {

    JrbustcpClientModule m;
    EmbeddedChannel ch;

    @Before
    public void setUp() throws Exception {

        BasicConfigurator.configure(new AppenderSkeleton() {
            @Override
            protected void append(LoggingEvent event) {}

            @Override
            public void close() {}

            @Override
            public boolean requiresLayout() {return false;}
        });

        SampleEnvironment env = new SampleEnvironment();
        EnvironmentInst.set(env);

        KeyManager keyManager = mock(KeyManager.class);
        when(keyManager.decryptPrivate(eq("keyname"), eq("encrypted_nonce"))).thenReturn("decrypted_nonce");
        when(keyManager.encryptPublic(eq("badkeyname"), anyString())).thenThrow(new Exception("key_not_found_reply"));
        env.setKeyManager(keyManager);

        m = new JrbustcpClientModule(null, "");
        m.createSystemTags();

        ch = new EmbeddedChannel();
        m.channel = ch;

        m.filter = "";
        m.descr = "";

    }


    @After
    public void tearDown() throws Exception {
        if( m.outbuf != null )
            m.outbuf.release();
    }


    private void answer(int reqid, String s) {
        String ss = addHeaderFooter(reqid, s);
        byte[] bb = Strings.hexStringToBytes(ss);

        ByteBuf buf = Unpooled.buffer();
        buf.writeBytes(bb);
        m.answer.offer(buf);
    }

    private CRC32 crc = new CRC32();
    private String addHeaderFooter(int reqid, String s) {
        s = String.format("%08X", reqid) + s.replace(" ","");
        byte[] bb = Strings.hexStringToBytes(s);
        crc.reset();
        crc.update(bb);
        s = "ABCD" + s + String.format("%08X", crc.getValue());
        return s;
    }

    private static String str(String s) {
        byte[] bb = s.getBytes(Charset.defaultCharset());
        String ss = String.format("%02X ", bb.length) + Strings.bytesToHexString(bb);
        return ss;
    }

    private static String val(String s) {
        byte[] bb = s.getBytes(Charset.defaultCharset());
        String ss = String.format("%04X ", bb.length) + Strings.bytesToHexString(bb);
        return ss;
    }

    private static String val(Double val) {
        return String.format("%016X ", Double.doubleToLongBits(val));
    }



    private void request(int reqid, String s) {
        String exp = addHeaderFooter(reqid, s).toUpperCase();

        ByteBuf buf = ch.readOutbound();
        String act = ByteBufUtil.hexDump(buf).toUpperCase();

        assertEquals(exp, act);
    }


    @Test
    public void testInitList_double_pass() {
        // pass 1 - get 5 tags
        answer(1, "81 000005");
        answer(2, String.format("82 000000 000005 000000 01%s00 02%s00 03%s00 04%s00 05%s00"
                , str("scr.bool")
                , str("scr.int")
                , str("scr.long")
                , str("scr.double")
                , str("scr.string")
        ));

        assertTrue(m.initList());

        request(1, String.format("01 %s %s 0000"
                , str(m.filter)
                , str(m.descr)
        ));
        request(2, "02 000000");

        assertEquals(5, m.tags.length);
        assertEquals("scr.bool",    m.tags[0].getName());
        assertEquals("scr.int",     m.tags[1].getName());
        assertEquals("scr.long",    m.tags[2].getName());
        assertEquals("scr.double",  m.tags[3].getName());
        assertEquals("scr.string",  m.tags[4].getName());

        assertEquals(Tag.Type.BOOL,     m.tags[0].getType());
        assertEquals(Tag.Type.INT,      m.tags[1].getType());
        assertEquals(Tag.Type.LONG,     m.tags[2].getType());
        assertEquals(Tag.Type.DOUBLE,   m.tags[3].getType());
        assertEquals(Tag.Type.STRING,   m.tags[4].getType());

        Tag t1 = m.tags[1];
        Tag t2 = m.tags[2];

        assertEquals(Tag.Status.Good, t1.getStatus());
        assertEquals(Tag.Status.Good, t2.getStatus());



        // pass 2 - get 2 tags, where one tag is old and one is new
        answer(3, "81 000002");
        answer(4, String.format("82 000000 000002 000000 02%s00 02%s00"
                , str("scr.int")
                , str("scr.new")
        ));

        m.filter = "filter";
        m.descr = "descr";
        assertTrue(m.initList());

        request(3, String.format("01 %s %s 0000"
                , str(m.filter)
                , str(m.descr)
        ));
        request(4, "02 000000");

        assertEquals(2, m.tags.length);
        assertEquals("scr.int",     m.tags[0].getName());
        assertEquals("scr.new",     m.tags[1].getName());

        assertEquals(Tag.Type.INT,          m.tags[0].getType());
        assertEquals(Tag.Type.INT,          m.tags[1].getType());

        assertEquals(Tag.Status.Good, t1.getStatus());
        assertEquals(Tag.Status.Deleted, t2.getStatus());

        // old tag
        assertSame(t1, m.tags[0]);
    }


    @Test
    public void testInitList_next() {
        answer(1, "81 000005");
        answer(2, String.format("82 000000 000003 000003 01%s00 02%s00 03%s00"
                , str("scr.bool")
                , str("scr.int")
                , str("scr.long")
        ));
        answer(3, String.format("82 000003 000001 000004 04%s00"
                , str("scr.double")
        ));
        answer(4, String.format("82 000004 000001 000000 05%s00"
                , str("scr.string")
        ));

        assertTrue(m.initList());

        request(1, String.format("01 %s %s 0000"
                , str(m.filter)
                , str(m.descr)
        ));
        request(2, "02 000000");
        request(3, "02 000003");
        request(4, "02 000004");

        assertEquals(5, m.tags.length);
        assertEquals("scr.bool", m.tags[0].getName());
        assertEquals("scr.int", m.tags[1].getName());
        assertEquals("scr.long", m.tags[2].getName());
        assertEquals("scr.double", m.tags[3].getName());
        assertEquals("scr.string", m.tags[4].getName());
    }


    @Test
    public void testInitList_rusdescr_flags() {

        answer(1, "81 000000");
        answer(2, "82 000000 000000 000000");

        m.descr = "русские буквы в описании";
        m.initflags = 0x1234;
        assertTrue(m.initList());

        request(1, String.format("01 %s %s 1234"
                , str(m.filter)
                , str(m.descr)
        ));
        request(2, "02 000000");

        assertEquals(0, m.tags.length);
    }


    @Test
    public void testInitList_toobig_filter_descr() {

        answer(1, "81 000000");
        answer(2, "82 000000 000000 000000");

        m.filter = "";
        m.descr  = "";
        for(int i=0; i<100; ++i) {
            m.filter += "0123456789";
            m.descr  += "руссобуквы";
        }
        assertTrue(m.initList());

        request(1, String.format("01 %s %s 0000"
                , str(m.filter.substring(0, 254))
                , str(m.descr.substring(0, 254/2))
        ));
        request(2, "02 000000");
    }


    @Test
    public void testInitList_invalid_tagnames() {
        answer(1, "81 0002");
        answer(2, String.format("82 000000 000003 000003 01FFFF 02FFFF"
                , str("scr.bool")
                , str("scr.int")
        ));

        assertFalse(m.initList());

        request(1, String.format("01 %s %s 0000"
                , str(m.filter)
                , str(m.descr)
        ));
        request(2, "02 000000");

        assertEquals(0, m.tags.length);

    }


    @Test
    public void testInitList_invalid_tagtypes() {
        answer(1, "81 000002");
        answer(2, String.format("82 000000 000003 000000 01%s00 02%s00 06%s00"
                , str("good.bool")
                , str("good.int")
                , str("badtype")
        ));

        assertFalse(m.initList());

        request(1, String.format("01 %s %s 0000"
                , str(m.filter)
                , str(m.descr)
        ));
        request(2, "02 000000");

        assertEquals(0, m.tags.length);
    }



    @Test
    public void testUpdateRead_bool() {
        m.tags = new TagRW[0x12];
        for(int i=0; i<m.tags.length; ++i)
            m.tags[i] = new TagRWBool("t"+i,false, Flags.EXTERNAL);

        answer(1, "83 000012 000000 00");
        String values = String.format("84 000000 000012 000000   "
            // 0 1    2    3      4      5        6          7          8
            +"F0 F1   F200 F2FF   F30000 F3FFFF   F800000000 F87FFFFFFF F8FFFFFFFF   "
            // 9                 10                 11
            +"F90000000000000000 F97FFFFFFFFFFFFFFF F9FFFFFFFFFFFFFFFF   "
            +"FA%s FA%s FA%s   FB%s FB%s FB%s"
            //    12        13            14
            , val(0.0), val(123.456), val(-123.456)
            //      15          16            17
            , val(""), val("off"), val("on")
        );
        answer(2, values);

        assertTrue(m.doRead());

        request(1, "03");
        request(2, "04 000000");

        assertEquals( false,    m.tags[0].getBool());
        assertEquals( true,     m.tags[1].getBool());
        assertEquals( false,    m.tags[2].getBool());
        assertEquals( true,     m.tags[3].getBool());
        assertEquals( false,    m.tags[4].getBool());
        assertEquals( true,     m.tags[5].getBool());
        assertEquals( false,    m.tags[6].getBool());
        assertEquals( true,     m.tags[7].getBool());
        assertEquals( true,     m.tags[8].getBool());
        assertEquals( false,    m.tags[9].getBool());
        assertEquals( true,     m.tags[10].getBool());
        assertEquals( true,     m.tags[11].getBool());
        assertEquals( false,    m.tags[12].getBool());
        assertEquals( true,     m.tags[13].getBool());
        assertEquals( true,     m.tags[14].getBool());
        assertEquals( false,    m.tags[15].getBool());
        assertEquals( false,    m.tags[16].getBool());
        assertEquals( true,     m.tags[17].getBool());
    }


    @Test
    public void testUpdateRead_int() {
        m.tags = new TagRW[0x12];
        for(int i=0; i<m.tags.length; ++i)
            m.tags[i] = new TagRWInt("t"+i,0, Flags.EXTERNAL);

        answer(1, "83 000012 000000 00");

        String values = String.format("84 000000 000012 000000   "
            // 0 1    2    3      4      5        6          7          8
            +"F0 F1   F200 F2FF   F30000 F3FFFF   F800000000 F87FFFFFFF F8FFFFFFFF   "
            // 9                 10                 11
            +"F90000000000000000 F97FFFFFFFFFFFFFFF F9FFFFFFFFFFFFFFFF   "
            +"FA%s FA%s FA%s   FB%s FB%s FB%s"
            //    12        13            14
            , val(0.0), val(123.456), val(-123.456)
            //      15          16            17
            , val("0"), val("111"), val("-111")
        );
        answer(2, values);

        assertTrue(m.doRead());

        request(1, "03");
        request(2, "04 000000");

        assertEquals( 0,            m.tags[0].getInt());
        assertEquals( 1,            m.tags[1].getInt());
        assertEquals( 0,            m.tags[2].getInt());
        assertEquals( 0xFF,         m.tags[3].getInt());
        assertEquals( 0,            m.tags[4].getInt());
        assertEquals( 0xFFFF,       m.tags[5].getInt());
        assertEquals( 0,            m.tags[6].getInt());
        assertEquals( 0x7FFFFFFF,   m.tags[7].getInt());
        assertEquals( -1,           m.tags[8].getInt());
        assertEquals( 0,            m.tags[9].getInt());
        assertEquals( -1,           m.tags[10].getInt());
        assertEquals( -1,           m.tags[11].getInt());
        assertEquals( 0,            m.tags[12].getInt());
        assertEquals( 123,          m.tags[13].getInt());
        assertEquals( -123,         m.tags[14].getInt());
        assertEquals( 0,            m.tags[15].getInt());
        assertEquals( 111,          m.tags[16].getInt());
        assertEquals( -111,         m.tags[17].getInt());
    }


    @Test
    public void testUpdateRead_long() {
        m.tags = new TagRW[0x12];
        for(int i=0; i<m.tags.length; ++i)
            m.tags[i] = new TagRWLong("t"+i,0L, Flags.EXTERNAL);

        answer(1, "83 000012 000000 00");

        String values = String.format("84 000000 000012 000000   "
                        // 0 1    2    3      4      5        6          7          8
                        +"F0 F1   F200 F2FF   F30000 F3FFFF   F800000000 F87FFFFFFF F8FFFFFFFF   "
                        // 9                 10                 11
                        +"F90000000000000000 F97FFFFFFFFFFFFFFF F9FFFFFFFFFFFFFFFF   "
                        +"FA%s FA%s FA%s   FB%s FB%s FB%s"
                //    12        13            14
                , val(0.0), val(123.456), val(-123.456)
                //      15          16            17
                , val("0"), val("111"), val("-111")
        );
        answer(2, values);

        assertTrue(m.doRead());

        request(1, "03");
        request(2, "04 000000");

        assertEquals( 0,            m.tags[0].getLong());
        assertEquals( 1,            m.tags[1].getLong());
        assertEquals( 0,            m.tags[2].getLong());
        assertEquals( 0xFF,         m.tags[3].getLong());
        assertEquals( 0,            m.tags[4].getLong());
        assertEquals( 0xFFFF,       m.tags[5].getLong());
        assertEquals( 0,            m.tags[6].getLong());
        assertEquals( 0x7FFFFFFF,   m.tags[7].getLong());
        assertEquals( -1,           m.tags[8].getLong());
        assertEquals( 0,            m.tags[9].getLong());
        assertEquals( 0x7FFFFFFFFFFFFFFFL, m.tags[10].getLong());
        assertEquals( -1,           m.tags[11].getLong());
        assertEquals( 0,            m.tags[12].getLong());
        assertEquals( 123,          m.tags[13].getLong());
        assertEquals( -123,         m.tags[14].getLong());
        assertEquals( 0,            m.tags[15].getLong());
        assertEquals( 111,          m.tags[16].getLong());
        assertEquals( -111,         m.tags[17].getLong());
    }


    @Test
    public void testUpdateRead_double() {
        m.tags = new TagRW[0x12];
        for(int i=0; i<m.tags.length; ++i)
            m.tags[i] = new TagRWDouble("t"+i,0, Flags.EXTERNAL);

        answer(1, "83 000012 000000 00");

        String values = String.format("84 000000 000012 000000   "
                        // 0 1    2    3      4      5        6          7          8
                        +"F0 F1   F200 F2FF   F30000 F3FFFF   F800000000 F87FFFFFFF F8FFFFFFFF   "
                        // 9                 10                 11
                        +"F90000000000000000 F97FFFFFFFFFFFFFFF F9FFFFFFFFFFFFFFFF   "
                        +"FA%s FA%s FA%s   FB%s FB%s FB%s"
                //    12        13            14
                , val(0.0), val(123.456), val(-123.456)
                //      15          16            17
                , val("0"), val("111.11"), val("-111.11")
        );
        answer(2, values);

        assertTrue(m.doRead());

        request(1, "03");
        request(2, "04 000000");

        assertEquals( 0,            m.tags[0].getDouble(), 0.0001);
        assertEquals( 1,            m.tags[1].getDouble(), 0.0001);
        assertEquals( 0,            m.tags[2].getDouble(), 0.0001);
        assertEquals( 0xFF,         m.tags[3].getDouble(), 0.0001);
        assertEquals( 0,            m.tags[4].getDouble(), 0.0001);
        assertEquals( 0xFFFF,       m.tags[5].getDouble(), 0.0001);
        assertEquals( 0,            m.tags[6].getDouble(), 0.0001);
        assertEquals( 0x7FFFFFFF,   m.tags[7].getDouble(), 0.0001);
        assertEquals( -1,           m.tags[8].getDouble(), 0.0001);
        assertEquals( 0,            m.tags[9].getDouble(), 0.0001);
        assertEquals( (double)0x7FFFFFFFFFFFFFFFL, m.tags[10].getDouble(), 0.0001);
        assertEquals( -1,           m.tags[11].getDouble(), 0.0001);
        assertEquals( 0,            m.tags[12].getDouble(), 0.0001);
        assertEquals( 123.456,      m.tags[13].getDouble(), 0.0001);
        assertEquals( -123.456,     m.tags[14].getDouble(), 0.0001);
        assertEquals( 0,            m.tags[15].getDouble(), 0.0001);
        assertEquals( 111.11,       m.tags[16].getDouble(), 0.0001);
        assertEquals( -111.11,      m.tags[17].getDouble(), 0.0001);
    }



    @Test
    public void testUpdateRead_string() {
        m.tags = new TagRW[0x12];
        for(int i=0; i<m.tags.length; ++i)
            m.tags[i] = new TagRWString("t"+i,"", Flags.EXTERNAL);

        answer(1, "83 000012 000000 00");

        String values = String.format("84 000000 000012 000000   "
                        // 0 1    2    3      4      5        6          7          8
                        +"F0 F1   F200 F2FF   F30000 F3FFFF   F800000000 F87FFFFFFF F8FFFFFFFF   "
                        // 9                 10                 11
                        +"F90000000000000000 F97FFFFFFFFFFFFFFF F9FFFFFFFFFFFFFFFF   "
                        +"FA%s FA%s FA%s   FB%s FB%s FB%s"
                //    12        13            14
                , val(0.0), val(123.456), val(-123.456)
                //      15          16            17
                , val("0"), val("111"), val("-111")
        );
        answer(2, values);

        assertTrue(m.doRead());

        request(1, "03");
        request(2, "04 000000");

        assertEquals( "0",            m.tags[0].getString());
        assertEquals( "1",            m.tags[1].getString());
        assertEquals( "0",            m.tags[2].getString());
        assertEquals( ""+0xFF,        m.tags[3].getString());
        assertEquals( "0",            m.tags[4].getString());
        assertEquals( ""+0xFFFF,      m.tags[5].getString());
        assertEquals( "0",            m.tags[6].getString());
        assertEquals( ""+0x7FFFFFFF,  m.tags[7].getString());
        assertEquals( "-1",           m.tags[8].getString());
        assertEquals( "0",            m.tags[9].getString());
        assertEquals( ""+0x7FFFFFFFFFFFFFFFL, m.tags[10].getString());
        assertEquals( "-1",           m.tags[11].getString());
        assertEquals( "0.0",          m.tags[12].getString());
        assertEquals( "123.456",      m.tags[13].getString());
        assertEquals( "-123.456",     m.tags[14].getString());
        assertEquals( "0",            m.tags[15].getString());
        assertEquals( "111",          m.tags[16].getString());
        assertEquals( "-111",         m.tags[17].getString());
    }


    @Test
    public void testUpdateRead_invalid_int_value() {
        m.tags = new TagRW[]{
                new TagRWInt("", 0, Flags.EXTERNAL),
                new TagRWInt("", 0, Flags.EXTERNAL)
        };

        answer(1, "83 000002 000000 00");
        answer(2, String.format("84 000000 000002 000000 F1 FB%s", val("not a value")));

        assertTrue(m.doRead());

        request(1, "03");
        request(2, "04 000000");

        assertEquals( 1, m.tags[0].getInt());
        assertEquals( 0, m.tags[1].getInt());
    }

    @Test
    public void testUpdateRead_random() {
        m.tags = new TagRW[5];
        for(int i=0; i<m.tags.length; ++i)
            m.tags[i] = new TagRWInt("",0, Flags.EXTERNAL);

        answer(1, "83 000002 000001 00");
        answer(2, "84 000001 000002 000004 F211 FE0003 F233");
        answer(3, "84 000004 000002 000007 F244 FF000002 F222");
        // doesn't matter what is index, but qnt still matters
        answer(4, "84 000007 000001 000000 FE0000 F277");
        assertTrue(m.doRead());
        request(1, "03");
        request(2, "04 000001");
        request(3, "04 000004");
        request(4, "04 000007");


        assertEquals( 0x77, m.tags[0].getInt());
        assertEquals( 0x11, m.tags[1].getInt());
        assertEquals( 0x22, m.tags[2].getInt());
        assertEquals( 0x33, m.tags[3].getInt());
        assertEquals( 0x44, m.tags[4].getInt());
    }


    @Test
    public void testUpdateRead_invalid_index() {
        m.tags = new TagRW[]{
                new TagRWInt("", 0, Flags.EXTERNAL),
        };

        answer(1, "83 000001 000000 00");
        // 7 is invalid index
        answer(2, "84 000007 000001 000000 F0");
        assertFalse(m.doRead());
        request(1, "03");
        request(2, "04 000000");
    }


    @Test
    public void testUpdateRead_liststatus() {
        m.tags = new TagRW[]{
                new TagRWInt("", 0, Flags.EXTERNAL),
        };

        answer(1, "83 000000 000000 FF");
        answer(2, "81 000005");
        answer(3, String.format("82 000000 000002 000000 01%s00 02%s00"
                , str("bool")
                , str("int")
        ));
        assertFalse(m.doRead());
        request(1, "03");
        request(2, String.format("01 %s %s 0000"
                , str(m.filter)
                , str(m.descr)
        ));
        request(3, "02 000000");

    }


    @Test
    public void testWrite_random() {
        m.tags = new TagRW[5];
        for(int i=0; i<m.tags.length; ++i)
            m.tags[i] = new TagRWInt("",0, Flags.EXTERNAL);

        assertTrue(m.doWrite());

        m.tags[0].setInt(0x77);
        answer(1, "85");
        assertTrue(m.doWrite());
        request(1, "05 000000 000001 F277");

        m.tags[1].setInt(0x11);
        m.tags[3].setInt(0x33);
        answer(2, "85");
        assertTrue(m.doWrite());
        request(2, "05 000001 000002 F211 FE0003 F233");

        m.tags[2].setInt(0x22);
        m.tags[4].setInt(0x7FFFFFFF);
        answer(3, "85");
        assertTrue(m.doWrite());
        request(3, "05 000002 000002 F222 FE0004 F87FFFFFFF");
    }


    @Test
    public void testWrite_all() {
        m.tags = new TagRW[]{
                new TagRWBool(  "", false,  Flags.EXTERNAL),
                new TagRWInt(   "", 0,      Flags.EXTERNAL),
                new TagRWLong(  "", 0L,     Flags.EXTERNAL),
                new TagRWDouble("", 0.0,    Flags.EXTERNAL),
                new TagRWString("", "",     Flags.EXTERNAL),
        };

        assertTrue(m.doWrite());

        m.tags[0].setBool(true);
        m.tags[1].setInt(-1);
        m.tags[2].setLong(0x7FFFFFFF_FFFFFFFFL);
        m.tags[3].setDouble(456.789);
        m.tags[4].setString("hello");

        answer(1, "85");
        assertTrue(m.doWrite());
        request(1, String.format("05 000000 000005 F1 F8FFFFFFFF F97FFFFFFFFFFFFFFF FA%s FB%s"
                , val(456.789)
                , val("hello")
        ));

        assertTrue(m.doWrite());

    }


    @Test
    public void testWrite_noanswer() {
        m.tags = new TagRW[]{
                new TagRWInt(   "", 0,      Flags.EXTERNAL)
        };

        m.tags[0].setInt(1);
//        answer(1, "85"); - server doesn't answer!
        assertFalse(m.doWrite());
        request(1, "05 000000 000001 F1");

        assertTrue(m.doWrite());
    }


    @Test
    public void testAuth_accepted() {
        m.setAuth(true);
        m.setAuthKeyName("keyname");
        answer(1, "87 00 " + val("encrypted_nonce"));
        answer(2, "88 00");

        assertTrue(m.authenticate());

        request(1, "07 " + val("keyname"));
        request(2, "08 " + val("decrypted_nonce"));
    }

    @Test
    public void testAuthInit_failed() {
        m.setAuth(true);
        m.setAuthKeyName("keyname");
        answer(1, "87 01 " + "key_not_found");

        assertFalse(m.authenticate());
        request(1, "07 " + val("keyname"));
    }

    @Test
    public void testAuthInit_disabled() {
        m.setAuth(true);
        m.setAuthKeyName("keyname");
        answer(1, "87 02");

        assertTrue(m.authenticate());
        request(1, "07 " + val("keyname"));
    }

    @Test
    public void testAuthInit_denied() {
        m.setAuth(true);
        m.setAuthKeyName("keyname");
        answer(1, "87 00 " + val("encrypted_nonce"));
        answer(2, "88 01");

        assertFalse(m.authenticate());
    }

    @Test
    public void testUnauthenticated_read() {
        m.setAuth(true);
        m.setAuthKeyName("keyname");
        answer(1, "FE");

        assertFalse(m.doRead());

        assertTrue( m.getTagTable().get("client.last.error").getString().contains("Unauthenticated") );
    }
}