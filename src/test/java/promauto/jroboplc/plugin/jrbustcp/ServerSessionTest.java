package promauto.jroboplc.plugin.jrbustcp;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufUtil;
import io.netty.channel.embedded.EmbeddedChannel;
import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.spi.LoggingEvent;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import promauto.jroboplc.core.api.*;
import promauto.utils.Strings;

import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.zip.CRC32;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static promauto.jroboplc.plugin.jrbustcp.JrbustcpProtocol.STR_LEN_MAX;
import static promauto.jroboplc.plugin.jrbustcp.ServerSession.ClientTagState.OK;

public class ServerSessionTest {

    JrbustcpServerModule m;
    ServerSession sess;
    EmbeddedChannel ch;

    SampleModule m1;
    SampleModule m2;

    Tag t1;
    Tag t2;
    Tag t3;
    Tag t4;
    Tag t5;
    
    @Before
    public void setUp() throws Exception {

        BasicConfigurator.configure(new AppenderSkeleton() {
            @Override
            protected void append(LoggingEvent event) {}

            @Override
            public void close() {}

            @Override
            public boolean requiresLayout() {return false;}
        });


        SampleEnvironment env = new SampleEnvironment();
        EnvironmentInst.set(env);

        KeyManager keyManager = mock(KeyManager.class);
        when(keyManager.encryptPublic(eq("keyname"), anyString())).thenReturn("encrypted_nonce");
        when(keyManager.encryptPublic(eq("badkeyname"), anyString())).thenThrow(new Exception("key_not_found_reply"));
        env.setKeyManager(keyManager);

        SampleModuleManager mm = new SampleModuleManager();
        env.setModuleManager(mm);

        m1 = new SampleModule("m1");
        m2 = new SampleModule("m2");
        mm.modules.put(m1.name, m1);
        mm.modules.put(m2.name, m2);

        t1 = m1.tagtable.createBool("bool", false, Flags.EXTERNAL);
        t2 = m1.tagtable.createInt("int", 0, Flags.EXTERNAL);
        t3 = m1.tagtable.createLong("long", 0L, Flags.EXTERNAL);
        t4 = m2.tagtable.createDouble("double", 0.0);
        t5 = m2.tagtable.createString("string", "");


        m = new JrbustcpServerModule(null, "");
        sess = new ServerSession(m);

        ch = new EmbeddedChannel();

        sess.onConnected(ch);

    }


    @After
    public void tearDown() throws Exception {
        sess.onDisconnected(ch);
    }


    private CRC32 crc = new CRC32();
    private String addHeaderFooter(int reqid, String s) {
        s = String.format("%08X", reqid) + s.replace(" ","");
        byte[] bb = Strings.hexStringToBytes(s);
        crc.reset();
        crc.update(bb);
        s = "ABCD" + s + String.format("%08X", crc.getValue());
        return s;
    }

    private static String str(String s) {
        byte[] bb = s.getBytes(Charset.defaultCharset());
        String ss = String.format("%02X ", bb.length) + Strings.bytesToHexString(bb);
        return ss;
    }

    private static String val(String s) {
        byte[] bb = s.getBytes(Charset.defaultCharset());
        String ss = String.format("%04X ", bb.length) + Strings.bytesToHexString(bb);
        return ss;
    }

    private static String val(Double val) {
        return String.format("%016X ", Double.doubleToLongBits(val));
    }

    private static String val(int val) {
        return String.format("%08X ", val);
    }



    private void request(int reqid, String s) {
        String ss = addHeaderFooter(reqid, s);
        byte[] bb = Strings.hexStringToBytes(ss);

        ByteBuf buf = ch.alloc().buffer();
        buf.writeBytes(bb);

        try {
            sess.onMessageReceived(ch, buf);
        } finally {
            buf.release();
        }
    }

    private void answer(int reqid, String s) {
        String exp = addHeaderFooter(reqid, s).toUpperCase();

        ByteBuf buf = ch.readOutbound();
        String act = ByteBufUtil.hexDump(buf).toUpperCase();
        buf.release();

        assertEquals(exp, act);
    }


    @Test
    public void testInit_duplicates() {

        Tag t6 = m2.tagtable.createInt("int", 111, Flags.EXTERNAL);

        request(1, "01" + str(".*") + str("clientdescr") + "0000");
        answer(1, "81 000005");
        assertEquals(5, sess.cltags.size());
        assertEquals("bool", sess.cltags.get(0).tag.getName());
        assertEquals("int", sess.cltags.get(1).tag.getName());
        assertEquals("long", sess.cltags.get(2).tag.getName());
        assertEquals("double", sess.cltags.get(3).tag.getName());
        assertEquals("string", sess.cltags.get(4).tag.getName());
    }


    @Test
    public void testInit_all() {
        request(1, "01" + str(".*") + str("clientdescr") + "0000");
        answer(1, "81 000005");
        assertEquals(".*", sess.clientFilter);
        assertEquals("clientdescr", sess.clientDescr);
        assertEquals(5, sess.cltags.size());
        assertEquals( "bool",   sess.cltags.get(0).tag.getName());
        assertEquals( "int",    sess.cltags.get(1).tag.getName());
        assertEquals( "long",   sess.cltags.get(2).tag.getName());
        assertEquals( "double", sess.cltags.get(3).tag.getName());
        assertEquals( "string", sess.cltags.get(4).tag.getName());

        ServerSession.ClientTag cltag = sess.cltags.get(0);
        assertSame(m1, cltag.module);
        assertSame(t1, cltag.tag);
        assertEquals( Tag.Type.BOOL,   cltag.fixval.getType());
        assertEquals( Tag.Status.Good, cltag.fixval.getStatus());
        assertTrue(cltag.skipModuleName);
        assertEquals(ServerSession.ClientTagState.NEED_SEND, cltag.state);

        cltag = sess.cltags.get(4);
        assertSame(m2, cltag.module);
        assertSame(t5, cltag.tag);
        assertEquals( Tag.Type.STRING,   cltag.fixval.getType());
        assertEquals( Tag.Status.Good, cltag.fixval.getStatus());
        assertFalse(cltag.skipModuleName);
        assertEquals(ServerSession.ClientTagState.NEED_SEND, cltag.state);
    }

    @Test
    public void testInit_zero() {
        request(1, "01" + str("m1.*") + "00 0000");
        answer(1, "81 000000");
        assertEquals(0, sess.cltags.size());
    }

    @Test
    public void testInit_two() {
        request(1, "01" + str("m2.*") + "00 0000");
        answer(1, "81 000002");
        assertEquals(2, sess.cltags.size());
    }

    @Test
    public void testInit_flags() {
        request(1, "01 00 00 0001");
        answer(1, "81 000005");
        assertEquals(true, sess.useTagDescr);
    }

    @Test
    public void testInit_invalid_strings() {
        assertTrue(ch.isActive());
        request(1, "01 FF FF 0000");
        assertFalse(ch.isActive());
    }

    @Test
    public void testList_without_init() {
        request(1, "02 000000");
        answer(1, "82 000000 000000 000000");
        assertEquals(0, sess.cltags.size());
    }

    @Test
    public void testList_with_init() {
        request(1, "01 00 00 0000");
        answer(1, "81 000005");
        assertEquals(5, sess.cltags.size());

        request(2, "02 000000");
        answer(2, String.format("82 000000 000005 000000 01%s00 02%s00 03%s00 04%s00 05%s00"
                , str("bool")
                , str("int")
                , str("long")
                , str("m2.double")
                , str("m2.string")
        ));

        request(3, "02 000003");
        answer(3, String.format("82 000003 000002 000000 04%s00 05%s00"
                , str("m2.double")
                , str("m2.string")
        ));

        request(4, "02 000005");
        answer(4, "82 000005 000000 000000");

        request(5, "02 FFFFFF");
        answer(5, "82 FFFFFF 000000 000000");

    }


    @Test
    public void testRead_without_init_update() {
        request(1, "04 000000");
        answer(1, "84 000000 000000 000000");
    }


    @Test
    public void testRead_without_init() {
        request(1, "03");
        answer(1, "83 000000 000000 00");

        request(2, "04 000000");
        answer(2, "84 000000 000000 000000");
    }


    @Test
    public void testRead_without_update() {
        request(1, "01 00 00 0000");
        answer(1, "81 000005");

        request(2, "04 000000");
        answer(2, String.format("84 000000 000005 000000 F0 F0 F0 FA%s FB%s"
                , val(0.0)
                , val("")
        ));
    }

    @Test
    public void testRead_random() {
        //init
        request(1, "01 00 00 0000");
        answer(1, "81 000005");

        // read pass 0
        request(2, "03");
        answer(2, "83 000005 000000 00");

        request(3, "04 000000");
        answer(3, String.format("84 000000 000005 000000 F0 F0 F0 FA%s FB%s"
                , val(0.0)
                , val("")
        ));


        // read pass 1
        request(4, "03");
        answer(4, "83 000000 000000 00");

        request(5, "04 000000");
        answer(5, "84 000000 000000 000000");


        // read pass 2
        t2.setInt(1);
        t3.setInt(2);
        request(6, "03");
        answer(6, "83 000002 000001 00");

        request(7, "04 000001");
        answer(7, "84 000001 000002 000000 F1 F202");


        // read pass 3
        request(8, "03");
        answer(8, "83 000000 000000 00");

        request(9, "04 000000");
        answer(9, "84 000000 000000 000000");


        // read pass 4
        t1.setInt(1);
        t3.setInt(3);
        request(6, "03");
        answer(6, "83 000002 000000 00");

        request(7, "04 000000");
        answer(7, "84 000000 000002 000000 F1 FE0002 F203");

    }


    @Test
    public void testRead_all() {
        //init
        request(1, "01 00 00 0000");
        answer(1, "81 000005");

        // read pass 0
        t1.setBool(true);
        t2.setInt(0xFFFF);
        t3.setLong(0xFFFF);
        t4.setDouble(123.456);
        t5.setString("hello");

        request(2, "03");
        answer(2, "83 000005 000000 00");

        request(3, "04 000000");
        answer(3, String.format("84 000000 000005 000000 F1 F3FFFF F3FFFF FA%s FB%s"
                , val(123.456)
                , val("hello")
        ));

        // read pass 1
        t2.setInt(0x7FFFFFFF);
        t3.setLong(0x7FFFFFFFFFFFFFFFL);
        t4.setDouble(-123.456);

        request(2, "03");
        answer(2, "83 000003 000001 00");

        request(3, "04 000001");
        answer(3, String.format("84 000001 000003 000000 F87FFFFFFF F97FFFFFFFFFFFFFFF FA%s"
                , val(-123.456)
        ));

        // read pass 2
        t2.setInt(-1);
        t3.setLong(-1);

        request(2, "03");
        answer(2, "83 000002 000001 00");

        request(3, "04 000001");
        answer(3, "84 000001 000002 000000 F8FFFFFFFF F9FFFFFFFFFFFFFFFF");
    }


    @Test
    public void testRead_toobig_string() {
        //init
        request(1, "01 00 00 0000");
        answer(1, "81 000005");

        sess.cltags.stream().forEach(ct -> ct.state = OK);

        String lat = "";
        String rus = "";
        for(int i=0; i<10000; ++i) {
            lat += "0123456789";
            rus += "руссобуквы";
        }

        // read pass 0
        t5.setString(lat);

        request(2, "03");
        answer(2, "83 000001 000004 00");

        request(3, "04 000004");
        answer(3, String.format("84 000004 000001 000000 FB%s"
                , val(lat.substring(0, STR_LEN_MAX))
        ));

        // read pass 1
        t5.setString(rus);

        request(2, "03");
        answer(2, "83 000001 000004 00");

        request(3, "04 000004");
        answer(3, String.format("84 000004 000001 000000 FB%s"
                , val(rus.substring(0, STR_LEN_MAX/2))
        ));
    }



    @Test
    public void testWrite_values() {
        //init
        request(1, "01 00 00 0000");
        answer(1, "81 000005");

        // pass 1
        request(1, String.format("05 000000 000005 F1 F2FF F3FFFF FA%s FB%s"
                , val(123.456)
                , val("hello")
        ));
        answer(1, "85");

        assertEquals( true, t1.getBool());
        assertEquals( 0xFF, t2.getInt());
        assertEquals( 0XFFFF, t3.getLong());
        assertEquals( 123.456, t4.getDouble(), 0.0001);
        assertEquals( "hello", t5.getString());

        // pass 1
        request(1, String.format("05 000000 000005 F0 F87FFFFFFF F97FFFFFFFFFFFFFFF FA%s FB%s"
                , val(-123.456)
                , val("")
        ));
        answer(1, "85");

        assertEquals( false, t1.getBool());
        assertEquals( 0x7FFFFFFF, t2.getInt());
        assertEquals( 0X7FFFFFFFFFFFFFFFL, t3.getLong());
        assertEquals( -123.456, t4.getDouble(), 0.0001);
        assertEquals( "", t5.getString());

        // pass 2
        request(1, "05 000000 000005 F8FFFFFFFF F8FFFFFFFF F9FFFFFFFFFFFFFFFF F9FFFFFFFFFFFFFFFF F9FFFFFFFFFFFFFFFF");
        answer(1, "85");

        assertEquals( true, t1.getBool());
        assertEquals( -1, t2.getInt());
        assertEquals( -1, t3.getLong());
        assertEquals( -1, t4.getDouble(), 0.0001);
        assertEquals( "-1", t5.getString());
    }



    @Test
    public void testWrite_invalid_string() {
        //init
        request(1, "01 00 00 0000");
        answer(1, "81 000005");

        // pass 1
        assertTrue(ch.isActive());
        request(1, "05 000000 000001 FBFFFF");
        assertFalse(ch.isActive());
    }


    @Test
    public void testUnknown() {
        //init
        request(1, "77");
        answer(1, "FF");
        assertTrue(ch.isActive());

    }


    @Test
    public void testCrc() {
        //init
        request(1, "01 00 00 0000");
        answer(1, "81 000005");

        // read pass 0
        t1.setBool(true);
        t2.setInt(0xFFFF);
        t3.setLong(0x7FFF_FFFF_FFFF_FFFFL);
        t4.setDouble(123.456);
        t5.setString("hello");

        request(2, "03");
        answer(2, "83 000005 000000 00");

//        long d = Double.doubleToLongBits(123.456);
//        System.out.println( Long.toHexString(d));
//        System.out.println( Integer.toHexString("hello".hashCode()));
//        System.out.println( Integer.toHexString("абв".hashCode()));

        int[] bb = new int[]{
                1,
                0,0,255,255,
                0x7f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
                0x40, 0x5e, 0xdd, 0x2f, 0x1a, 0x9f, 0xbe, 0x77,
                0x05, 0xe9, 0x18, 0xd2
        };

        CRC32 crc = new CRC32();
        Arrays.stream(bb).forEach( b -> crc.update(b) );
        int res = (int)crc.getValue();

        request(3, "06");
        answer(3, "86 " + val(res));
    }


    @Test
    public void testAuthInit_disabled() {
        request(1, "07" + val("keyname"));
        answer(1, "87 02");
    }

    @Test
    public void testAuthInit_enabled() {
        m.setAuth(true);
        request(1, "07" + val("keyname"));
        answer(1, "87 00 " + val("encrypted_nonce"));
    }

    @Test
    public void testAuthInit_bad_key() {
        m.setAuth(true);
        request(1, "07" + val("badkeyname"));
        answer(1, "87 01 " + val("key_not_found_reply"));
    }

    @Test
    public void testAuthSubmit_init_and_accept() {
        m.setAuth(true);
        request(1, "07" + val("keyname"));
        answer(1, "87 00 " + val("encrypted_nonce"));

        request(1, "08" + val(sess.getAuthNonce()));
        answer(1, "88 00");
    }

    @Test
    public void testAuthSubmit_deny() {
        request(1, "08" + val("bad_decrypted_nonce"));
        answer(1, "88 ff");
    }

    @Test
    public void testAuthSubmit_unauthenticated() {
        m.setAuth(true);
        request(1, "01"); // existing command
        answer(1, "FE");

        request(1, "33"); // not existing command
        answer(1, "FE");
    }


}