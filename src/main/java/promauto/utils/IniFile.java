package promauto.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class IniFile {
    private final static Charset charset = Charset.forName("windows-1251");

    private Pattern  _section  = Pattern.compile( "\\s*\\[([^]]*)\\]\\s*" );
    private Pattern  _keyValue = Pattern.compile( "\\s*([^=]*)=(.*)" );

    private Map< String, Map<String,String> >  _entries  = new HashMap<>();

    public IniFile( Path path ) throws IOException {
        load( path );
    }

    public void load( Path path ) throws IOException {
        try (BufferedReader br = Files.newBufferedReader(path, charset)) {
            String line;
            String section = null;
            while(( line = br.readLine()) != null ) {
                Matcher m = _section.matcher( line );
                if( m.matches()) {
                    section = m.group( 1 ).trim();
                }
                else if( section != null ) {
                    m = _keyValue.matcher( line );
                    if( m.matches()) {
                        String key   = m.group( 1 ).trim();
                        String value = m.group( 2 ).trim();
                        Map< String, String > kv = _entries.get( section );
                        if( kv == null ) {
                            _entries.put( section, kv = new HashMap<>());
                        }
                        kv.put( key, value );
                    }
                }
            }
        }
    }

    public String getString( String section, String key, String defaultvalue ) {
        Map< String, String > kv = _entries.get( section );
        if( kv == null ) {
            return defaultvalue;
        }
        return kv.getOrDefault( key, defaultvalue );
    }

    public int getInt( String section, String key, int defaultvalue ) {
        Map< String, String > kv = _entries.get( section );
        if( kv == null ) {
            return defaultvalue;
        }
        String val = kv.get( key );
        if( val == null ) {
            return defaultvalue;
        }
        return Integer.parseInt( val );
    }

    public float getFloat( String section, String key, float defaultvalue ) {
        Map< String, String > kv = _entries.get( section );
        if( kv == null ) {
            return defaultvalue;
        }
        String val = kv.get( key );
        if( val == null ) {
            return defaultvalue;
        }
        return Float.parseFloat(val);
    }

    public double getDouble( String section, String key, double defaultvalue ) {
        Map< String, String > kv = _entries.get( section );
        if( kv == null ) {
            return defaultvalue;
        }
        String val = kv.get( key );
        if( val == null ) {
            return defaultvalue;
        }
        return Double.parseDouble(val);
    }
}