package promauto.utils;

import java.util.Arrays;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;


public class Strings {
	
	public static byte[] intsToBytes(int[] data) {
		byte[] b = new byte[data.length];
		for (int i=0; i<data.length; i++)
			b[i] = (byte)(data[i] & 0xFF);
		return b;
	}

	public static byte[] intsToBytes(int[] data, int len) {
		byte[] b = new byte[len];
		for (int i=0; i<len; i++)
			b[i] = (byte)(data[i] & 0xFF);
		return b;
	}

	public static int[] bytesToInts(byte[] data) {
		int[] b = new int[data.length];
		for (int i=0; i<data.length; i++)
			b[i] = data[i] & 0xFF;
		return b;
	}



	/**
	 * Converts array of bytes to a string of hex numbers separated with space
	 * @param data
	 * @return
	 */
	public static String bytesToHexString(byte[] data) {
		StringBuilder sb = new StringBuilder();
		for (int i=0; i<data.length; i++) {
			sb.append( String.format("%02X ", data[i] & 0xff).toUpperCase() );
		}
		if (sb.length()>0)
			sb.deleteCharAt(sb.length()-1);
		return sb.toString();
	}
	
	public static String bytesToHexString(int[] data) {
		return bytesToHexString( intsToBytes(data) );
	}


	public static byte[] hexStringToBytes(String s) {
		s = s.replace(" ", "");
		int len = s.length()/2*2;
		byte[] buf = new byte[len / 2];
		for (int i = 0; i < len; i += 2) {
			buf[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
					+ Character.digit(s.charAt(i+1), 16));
		}
		return buf;
	}


	/**
	 * Converts array of string (where each string is a ascii hex number of byte) to a byte array.
	 * @param str
	 * @param ofs
	 * @return
	 */
//	public static byte[] hexStringToBytes(String[] str, int ofs) {
//		int n = str.length-ofs;
//		if (n<0)
//			n=0;
//		byte[] data = new byte[n];
//		try {
//			for (int i=0; i<n; i++) {
//				data[i] = (byte)(Integer.parseInt(str[i+ofs], 16));
//			}
//		} catch (NumberFormatException e) {
//		}
//
//		return data;
//	}

	
//	public static String parseHexString(String str) {
//		String[] ss = str.split("\\s* \\s*");
//		StringBuilder sb = new StringBuilder();
//		for(String s: ss)
//			sb.append( (char)Integer.parseInt(s, 16) );
//		return sb.toString();
//	}

	
	/**
	 * @param s source string of format "prmName=prmValue"
	 * @param prmName name of parameter
	 * @param separator sign or string between name of parameter and value
	 * @return parameter's string value
	 */
	public static String parsePropertyLine(String s, String prmName, String separator) {
		if (s.startsWith(prmName + separator))
			return s.substring(prmName.length()+1);
		else
			return null;
	}
	
	
	
//	/**
//	 * Converts string to integer
//	 * @param s - string format "number[radix]", where radix is an optional symbol  'h' for hex-numbers or 'b' for bin-numbers.
//	 * Examples: "123" = 123, "80h" = 128, "1111b" = 15.
//	 * @return
//	 */
//	public static Integer parseInt(String s) {
//		if (s.isEmpty())
//			return 0;
//
//		char c = s.charAt(s.length()-1);
//
//		if ((c>='0') && (c<='9'))
//			return Integer.parseInt(s);
//
//		if ((c=='h') || (c=='H'))
//			return Integer.parseInt(s.substring(0, s.length()-1), 16);
//
//		if ((c=='b') || (c=='B'))
//			return Integer.parseInt(s.substring(0, s.length()-1), 2);
//
//		return 0;
//	}


	public static Pattern getFilterPattern(String ipstr) {
		return getFilterPattern(ipstr, "\\s+");
	}


	public static Pattern getFilterPattern(String ipstr, String delim) {
		if ((ipstr==null) || (ipstr.isEmpty()))
			return null;
		
		try {
			String s;
			if( ipstr.startsWith("regex:") ) 
				s = ipstr.substring(6).trim();
			else
				s = "^(" + ipstr.trim()
					.replace(".", "\\.")
					.replace("*", ".*")
					.replace("?", ".")
					.replaceAll(delim, "|") + ")$";
			
			return Pattern.compile(s);
			
		} catch (PatternSyntaxException e) {
		}
		
		return Pattern.compile("");
	}


	public static String removeAnsiColors(String text) {
		return text.replaceAll("\u001B\\[\\d+m", "");
	}

	
	
	public static boolean bytesStartsWith(String substr, byte[] buff, int first, int end) {
		int n = substr.length();
		if( n > (end - first) )
			return false;
		
		for(int i=0; i<n; ++i) 
			if( substr.codePointAt(i) != buff[first + i] )
				return false;
		
		return true;
	}

	
	public static int getPosReverse(int ch, byte[] buff, int first, int end) {
		byte b = (byte)ch;
		while( --end >= first )
			if( buff[end] == b )
				return end;
		return -1;
	}
	
	public static int getPos(int ch, byte[] buff, int first, int end) {
		byte b = (byte)ch;
		while( first < end )
			if( buff[first] == b )
				return first;
			else
				first++;
		return -1;
	}
	

	public static boolean parsePropString() {
		return true;
	}
	
	
	public static String repeat(int length, char charToFill) {
		  if (length > 0) {
		    char[] array = new char[length];
		    Arrays.fill(array, charToFill);
		    return new String(array);
		  }
		  return "";
		}


}



































