package promauto.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CollectionUtils {
    @SuppressWarnings("unchecked")
    public static <K,V> Map<K,V> createMap(Object... items) {
        Map<K,V> map = new HashMap<>();
        for(int i=0; i<items.length/2; ++i)
            map.put((K)items[i*2], (V)items[i*2+1]);
        return map;
    }

    @SuppressWarnings("unchecked")
    public static <T> List<T> createList(Object... items) {
        List<T> list = new ArrayList<>();
        for(int i=0; i<items.length; ++i)
            list.add((T)items[i]);
        return list;
    }
}
