package promauto.jroboplc.plugin.tcpserver;

import java.net.InetSocketAddress;

import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.TcpServerChannel;
import promauto.jroboplc.core.api.TcpServerPort;

public class TcpServerChannelImpl implements TcpServerChannel {
	protected final Logger logger = LoggerFactory.getLogger(TcpServerChannelImpl.class);

	private final Channel channel;
	private final TcpServerPort port;
	private boolean logging = false;
	private Object attachedObject = null;
	private volatile long lastAliveTime;
	private volatile boolean closing;

	public TcpServerChannelImpl(Channel channel, TcpServerPort port) {
		this.channel = channel;
		this.port = port;
		lastAliveTime = System.currentTimeMillis();
		closing = false;
	}
	
	@Override
	public String getId() {
		return channel.id().asShortText();
	}

	@Override
	public TcpServerPort getPort() {
		return port;
	}

	@Override
	public InetSocketAddress getRemoteAddress() {
		return (InetSocketAddress)channel.remoteAddress();
	}

	@Override
	public boolean write(String data) {
		if( !channel.isActive() )
			return false;
		
		channel.writeAndFlush(data);
		return true;
	}


	@Override
	public String toString() {
		String remoteHostAddress = getRemoteAddress().getAddress().getHostAddress();
		String remoteHostName = getRemoteAddress().getAddress().getCanonicalHostName();
		return remoteHostAddress + "/" + remoteHostName + " (" + getId() + ")";
	}
	
	@Override
	public boolean isLogEnable() {
		return logging;
	}
	
	@Override
	public synchronized void setLogEnable(boolean logging) {
		if (this.logging == logging) return;
		
		this.logging = logging;
		if (logging) 
			channel.pipeline().addAfter("encoder", "logger", new LoggingHandler(LogLevel.DEBUG));
		else
			channel.pipeline().remove("logger");
	}

	
	@Override
	public Object getAttachedObject() {
		return attachedObject;
	}

	@Override
	public void setAttachedObject(Object obj) {
		this.attachedObject = obj;
	}

	public boolean close(long timeout) {
		ChannelFuture future = channel.close();
		return future.awaitUninterruptibly(timeout);
	}

	@Override
	public boolean checkAlive() {
		if( (System.currentTimeMillis() - lastAliveTime) < port.getAliveTimeout() ) 
			return true;
		
		if( !closing ) {
			EnvironmentInst.get().printInfo(logger, "Close dead channel: " + toString() );
			channel.disconnect();
			closing = true;
		}
		
		return false;
	}
	
	public void markChannelAsAlive() {
		lastAliveTime = System.currentTimeMillis();
	}
	
	

}
