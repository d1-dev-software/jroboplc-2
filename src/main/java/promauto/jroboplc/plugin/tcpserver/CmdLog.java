package promauto.jroboplc.plugin.tcpserver;

import promauto.jroboplc.core.AbstractCommand;
import promauto.jroboplc.core.api.Console;
import promauto.jroboplc.core.api.Environment;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.Plugin;
import promauto.jroboplc.core.api.TcpServerChannel;
import promauto.jroboplc.core.api.TcpServerPort;

public class CmdLog extends AbstractCommand {
	private TcpServerPort port;
	private boolean value;
	private String valuestr;
	
	


	@Override
	public String getName() {
		return "log";
	}

	@Override
	public String getUsage() {
		return "portnum on|off";
	}

	@Override
	public String getDescription() {
		return "turns on/off logging";
	}

	
	
	protected boolean parseArgs(Environment env, String args) {
		String[] ss = args.split("\\s* \\s*");

		if (ss.length!=2) 
			return false;
			
		int portnum = 0;
		try {
			portnum = Integer.parseInt(ss[0]);
		} catch (NumberFormatException e) {
			return false;
		}
		
		valuestr = ss[1];
		if (valuestr.equals("on"))
			value = true;
		else
			if (valuestr.equals("off"))
				value = false;
			else
				return false;

		port = env.getTcpServer().getPortByNum(portnum);
		if (port==null)
			return false;

		return true;
	}

	
	@Override
	public String execute(Console console, Plugin plugin, String args) {
		Environment env = EnvironmentInst.get();
		if (!parseArgs(env, args))
			return getRefuseAnswer(plugin);

		StringBuilder sb = new StringBuilder("Setting logging ");
		sb.append(valuestr);
		sb.append(" to port ");
		sb.append(port.getPortnum());
		sb.append(":\r\n");
		for (TcpServerChannel ch: port.getChannels()) {
			if (ch.isLogEnable() == value)
				sb.append("already ");
			else {
				sb.append("set to ");
				ch.setLogEnable(value);
			}
			sb.append(valuestr);
			sb.append(" - ");
			sb.append(ch);
			sb.append("\r\n");
		}
		return sb.toString();
	}

}
