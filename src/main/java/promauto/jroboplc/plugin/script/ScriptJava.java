package promauto.jroboplc.plugin.script;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.api.*;
import promauto.jroboplc.core.tags.*;

import javax.xml.crypto.Data;

public class ScriptJava implements ScriptJavaApi {
	final Logger logger = LoggerFactory.getLogger(ScriptJava.class);
	
	
	private Environment env = null;
	private Module parentModule = null;
	private ActionScriptJava action = null;
	private Object conf;

	// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	// !!! Change version every time when you modify this class !!!
	// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	public static final String getVersion() {
		return "20200404";
	}

	public final boolean load(Module parentModule, ActionScriptJava action, Object conf) {
		this.env = EnvironmentInst.get();
		this.parentModule = parentModule;
		this.action = action;
		this.conf = conf;
		return load();
	}

	@Override
	public final String getModuleName() {
		return parentModule.getName();
	}
	
	@Override
	public final String getId() {
		return action.getId();
	}
	
	
	// arguments
	@Override
	public final String getArg(String argname, String defval) {
		return env.getConfiguration().get(conf, argname, defval);
	}
	
	@Override
	public final int getArg(String argname, int defval) {
		return env.getConfiguration().get(conf, argname, defval);
	}
	
	@Override
	public final double getArg(String argname, double defval) {
		return env.getConfiguration().get(conf, argname, defval);
	}
	
	@Override
	public final boolean getArg(String argname, boolean defval) {
		return env.getConfiguration().get(conf, argname, defval);
	}
	
	
	// create var (internal tag, persistent while reloading)
	@Override
	public final Tag createVar(String tagname, boolean value) {
		return action.addVar( new TagPlainBool(makeTagName(tagname), value) );
	}

	@Override
	public final Tag createVar(String tagname, int value) {
		return action.addVar( new TagPlainInt(makeTagName(tagname), value) );
	}

	@Override
	public final Tag createVar(String tagname, long value) {
		return action.addVar( new TagPlainLong(makeTagName(tagname), value) );
	}

	@Override
	public final Tag createVar(String tagname, double value) {
		return action.addVar( new TagPlainDouble(makeTagName(tagname), value) );
	}
	
	@Override
	public final Tag createVar(String tagname, String value) {
		return action.addVar( new TagPlainString(makeTagName(tagname), value) );
	}
	
	
	
	// create tag in tagtable and returns a ref
	// base tags
	@Override
	public final Ref createTag(String tagname, boolean value, int flags) {
		return makeRef( parentModule.getTagTable().createBool(makeTagName(tagname), value, flags) );
	}

	@Override
	public final Ref createTag(String tagname, int value, int flags) {
		return makeRef( parentModule.getTagTable().createInt(makeTagName(tagname), value, flags) );
	}

	@Override
	public final Ref createTag(String tagname, long value, int flags) {
		return makeRef( parentModule.getTagTable().createLong(makeTagName(tagname), value, flags) );
	}

	@Override
	public final Ref createTag(String tagname, double value, int flags) {
		return makeRef( parentModule.getTagTable().createDouble(makeTagName(tagname), value, flags) );
	}
	
	@Override
	public final Ref createTag(String tagname, String value, int flags) {
		return makeRef( parentModule.getTagTable().createString(makeTagName(tagname), value, flags) );
	}


	public final Ref createTag(String tagname, boolean value) {
		return createTag(tagname, value, 0);
	}

	@Override
	public final Ref createTag(String tagname, int value) {
		return createTag(tagname, value, 0);
	}

	@Override
	public final Ref createTag(String tagname, long value) {
		return createTag(tagname, value, 0);
	}

	@Override
	public final Ref createTag(String tagname, double value) {
		return createTag(tagname, value, 0);
	}

	@Override
	public final Ref createTag(String tagname, String value) {
		return createTag(tagname, value, 0);
	}



	// rw tags
	@Override
	public final Ref createRWTag(String tagname, boolean value, int flags) {
		return makeRef( parentModule.getTagTable().createRWBool(makeTagName(tagname), value, flags) );
	}

	@Override
	public final Ref createRWTag(String tagname, int value, int flags) {
		return makeRef( parentModule.getTagTable().createRWInt(makeTagName(tagname), value, flags) );
	}

	@Override
	public final Ref createRWTag(String tagname, long value, int flags) {
		return makeRef( parentModule.getTagTable().createRWLong(makeTagName(tagname), value, flags) );
	}

	@Override
	public final Ref createRWTag(String tagname, double value, int flags) {
		return makeRef( parentModule.getTagTable().createRWDouble(makeTagName(tagname), value, flags) );
	}

	@Override
	public final Ref createRWTag(String tagname, String value, int flags) {
		return makeRef( parentModule.getTagTable().createRWString(makeTagName(tagname), value, flags) );
	}


	public final Ref createRWTag(String tagname, boolean value) {
		return createRWTag(tagname, value, 0);
	}

	@Override
	public final Ref createRWTag(String tagname, int value) {
		return createRWTag(tagname, value, 0);
	}

	@Override
	public final Ref createRWTag(String tagname, long value) {
		return createRWTag(tagname, value, 0);
	}

	@Override
	public final Ref createRWTag(String tagname, double value) {
		return createRWTag(tagname, value, 0);
	}

	@Override
	public final Ref createRWTag(String tagname, String value) {
		return createRWTag(tagname, value, 0);
	}






	private String makeTagName(String tagname) {
		if( getId().isEmpty() )
			return tagname;
		else
			return getId() + "." + tagname;
	}
	
	private Ref makeRef(Tag tag) {
		Ref ref = new Ref();
		ref.init(parentModule.getName(), tag.getName());
		action.addRef(ref);
		return ref;
	}
	
	
	
	
	// create ref
	@Override
	public final Ref createRef(String moduleTagname) {
		int k = moduleTagname.indexOf(':');
		if( k < 0 )
			return createRef(parentModule.getName(), moduleTagname);
		else
			return createRef(moduleTagname.substring(0,k), moduleTagname.substring(k+1));
	}

	@Override
	public final Ref createRef(String module, String tagname) {
		Ref ref = new Ref();
		ref.init(module, tagname);
		action.addRef(ref);
		return ref;
	}
	
	
	// miscellaneous
	@Override
	public final void printInfo(String text) {
		env.printInfo(logger, getModuleName(), getId(), text);
	}
	
	@Override
	public final void printError(String text) {
		env.printError(logger, getModuleName(), getId(), text);
	}

	@Override
	public final void printError(Throwable e, String text) {
		env.printError(logger, e, getModuleName(), "Id:", getId(), "Text:", text);
	}

	
	@Override
	public final boolean isValid() {
		return action.isValid();
	}

	@Override
	public Database getDatabase(String module) {
		Module m = env.getModuleManager().getModule(module);
		return (m instanceof Database)? (Database)m: null;
	}


	// methods for overriding
	@Override
	public boolean load() {
		return true;
	}

	@Override
	public boolean prepare() {
		return true;
	}

	@Override
	public void execute() {
	}


}












