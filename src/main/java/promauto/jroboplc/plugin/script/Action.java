package promauto.jroboplc.plugin.script;

import java.util.Map;

import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.EnvironmentInst;

public abstract class Action {
	private String id = "";
	private boolean enable = true;
	protected long delay_ms = 0;
	protected long timeDelay = 0;

	public boolean load(Object conf) {
		Configuration cm = EnvironmentInst.get().getConfiguration();
		
		id = cm.get(conf, "id", "");
		enable = cm.get(conf, "enable", true);
		delay_ms = Math.max(
				cm.get(conf, "delay_ms", 0),
				cm.get(conf, "delay_s", 0)*1000 );
		
		return true;
	}
	
	public String getId() {
		return id;
	}

	public boolean isEnabled() {
		return enable;
	}
	
	public abstract boolean prepare();
	
	public abstract void execute();
	
	
	public abstract boolean isValid();
	
	public abstract String getError();

	
	public void saveState(Map<String, String> state) {
	}

	public void loadState(Map<String, String> state) {
	}


}
