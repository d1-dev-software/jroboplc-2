package promauto.jroboplc.plugin.script.samples;

import promauto.jroboplc.plugin.script.*;
import promauto.jroboplc.core.tags.*;
import promauto.jroboplc.core.api.*;

public class Sample_PIDMelassa1 extends ScriptJava {
    ///----- paste begin -----


    //refs
    Ref vzlErrorFlag;
    Ref vzlFlow;    // x1 double l/min
    Ref vzlVolInt;
    Ref vzlVolFloat;

    Ref dnfErrorFlag;
    Ref dnfMinRef;     // x1000
    Ref dnfMaxRef;     // x1000
    Ref dnfSource1;
    Ref dnfState;
    Ref dnfFreqOut;
    Ref dnfFreqSet;

    //outputs ro
    Ref FlowInp;       // x1000 int l/min
    Ref FlowInpMh;     // x1000 int m3/hour
    Ref SumVolume;
    Ref FreqSetHz;     // x1000
    Ref FreqOutHz;     // x1000
    Ref PrevError;
    Ref Integral;
    Ref Diff;
    Ref Output;
    Ref Running;
    Ref Connected;

    //outputs rw
    Ref Enable;
    Ref Manual;
    Ref FlowSet;       // x1000 integer
    Ref FreqLowHz;     // x1000
    Ref Kp;            // x1000
    Ref Ki;            // x1000
    Ref Kd;            // x1000



    @Override
    public boolean load() {
        String vzl = getArg("vzl", "");
        String dnf = getArg("dnf", "");

        vzlErrorFlag = createRef(vzl, "SYSTEM.ErrorFlag");
        vzlFlow      = createRef(vzl, "Flow");
        vzlVolInt    = createRef(vzl, "VolPlusInt");
        vzlVolFloat  = createRef(vzl, "VolPlusFloat");

        dnfErrorFlag = createRef(dnf, "SYSTEM.ErrorFlag");
        dnfMinRef    = createRef(dnf, "MinRef");
        dnfMaxRef    = createRef(dnf, "MaxRef");
        dnfSource1   = createRef(dnf, "Source1");
        dnfState     = createRef(dnf, "State");
        dnfFreqOut   = createRef(dnf, "FreqOut");
        dnfFreqSet   = createRef(dnf, "FreqSet");


        FlowInp      = createTag("FlowInp", 0);
        FlowInpMh    = createTag("FlowInpV", 0);
        SumVolume    = createTag("SumVolume", 0L);
        FreqSetHz    = createTag("FreqSetHz", 0.0D);
        FreqOutHz    = createTag("FreqOutHz", 0);
        PrevError    = createTag("Error", 0.0D);
        Integral     = createTag("Integral", 0.0D);
        Diff         = createTag("Diff", 0.0D);
        Output       = createTag("Output", 0.0D);
        Running      = createTag("Running", 0);
        Connected    = createTag("Connected", 0);

        Enable       = createTag("Enable", 0, Flags.AUTOSAVE);
        Manual       = createTag("Manual", 0, Flags.AUTOSAVE);
        FlowSet      = createTag("FlowSet", 0, Flags.AUTOSAVE);
        FreqLowHz    = createTag("FreqLowHz", 0, Flags.AUTOSAVE);
        Kp = createTag("Kp", 0, Flags.AUTOSAVE);
        Kd = createTag("Kd", 0, Flags.AUTOSAVE);
        Ki = createTag("Ki", 0, Flags.AUTOSAVE);


        return true;
    }


    @Override
    public void execute(){
        boolean enable = Enable.getBool();
        boolean manual = Manual.getBool();
        boolean running = (dnfState.getInt() & 0b1000_0000_0000) > 0;
        boolean connected = vzlErrorFlag.getInt() + dnfErrorFlag.getInt() == 0;

        Running.setBool(running);
        Connected.setBool(connected);
        FlowInp.setInt( (int)Math.round( vzlFlow.getDouble() * 1000) );
        FlowInpMh.setInt( (int)Math.round( vzlFlow.getDouble() * 60) );
        SumVolume.setLong( vzlVolInt.getLong()*1000 + Math.round((vzlVolFloat.getDouble()*1000)) );

        // calc FreqOutHz
        double minref = dnfMinRef.getDouble();
        double maxref = dnfMaxRef.getDouble();
        double refrange = maxref - minref;
        int freqOutHz = (int)Math.round(dnfFreqOut.getDouble() / 16384 * refrange  + minref);
        FreqOutHz.setInt( freqOutHz );

        // danfoss control is off and we MUST keep zero in dnfFreqSet
        if( dnfSource1.getInt() != 0 ) {
            if( dnfFreqSet.getInt() != 0 )
                dnfFreqSet.setInt(0);

            FreqSetHz.setInt(0);
            PrevError.setInt(0);
            Integral.setInt(0);
            return;
        }

        if( running  &&  enable  &&  !manual ) {
            if( connected ) {
                double error = FlowSet.getDouble() - vzlFlow.getDouble() * 1000;
                double kp = Kp.getDouble()/1000;
                double kd = Kd.getDouble()/1000;
                double ki = Ki.getDouble()/1000;

                double integral = Ki.getDouble()==0? 0: Integral.getDouble() + error;

                double diff = kd * (error - PrevError.getDouble());

                double output = kp * error + diff + ki * integral;

                double freqSetHz = FreqSetHz.getDouble() + output;

                double freqLowHz = FreqLowHz.getDouble();
                double freqHighHz = dnfMaxRef.getDouble();
                freqSetHz = Math.max(freqSetHz, freqLowHz);
                freqSetHz = Math.min(freqSetHz, freqHighHz);

                if( freqSetHz == freqLowHz  &&  FreqSetHz.getInt() == freqLowHz  &&  output < 0 )
                    integral = 0;

                if( freqSetHz == freqHighHz  &&  FreqSetHz.getInt() == freqHighHz  &&  output > 0 )
                    integral = 0;

                Integral.setDouble( integral );
                Diff.setDouble( diff );
                PrevError.setDouble( error );
                Output.setDouble( output );
                FreqSetHz.setDouble( freqSetHz );
            }
        } else {
            FreqSetHz.setInt(0);
            PrevError.setInt(0);
            Integral.setInt(0);
            Diff.setInt( 0 );
            Output.setInt( 0 );
        }


        // calc FreqSet
        int freqSet = 0;
        if( refrange > 0 )
            freqSet = Math.max(0, (int)Math.round( (FreqSetHz.getDouble() - minref) / refrange * 8192));

        if( !manual )
            dnfFreqSet.setInt( freqSet );
    }


    ///----- paste end -----
}