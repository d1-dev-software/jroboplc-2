package promauto.jroboplc.plugin.script;

import promauto.jroboplc.core.api.Database;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.Ref;

public interface ScriptJavaApi {
	
	String getModuleName();
	String getId();

	
	// arguments
	String getArg(String argname, String defval);

	int getArg(String argname, int defval);
	
	double getArg(String argname, double defval);
	
	boolean getArg(String argname, boolean defval);
	
	
	// creates var (an internal tag, persistent for reloading)
	Tag createVar(String tagname, boolean value);
	Tag createVar(String tagname, int value);
	Tag createVar(String tagname, long value);
	Tag createVar(String tagname, double value);
	Tag createVar(String tagname, String value);
	
		
	// creates tag in the tagtable and returns a reference. Result name of the tag: "id.tagname"
	Ref createTag(String tagname, boolean value, int flags);
	Ref createTag(String tagname, int value, int flags);
	Ref createTag(String tagname, long value, int flags);
	Ref createTag(String tagname, double value, int flags);
	Ref createTag(String tagname, String value, int flags);

	Ref createTag(String tagname, boolean value);
	Ref createTag(String tagname, int value);
	Ref createTag(String tagname, long value);
	Ref createTag(String tagname, double value);
	Ref createTag(String tagname, String value);

	Ref createRWTag(String tagname, boolean value, int flags);
	Ref createRWTag(String tagname, int value, int flags);
	Ref createRWTag(String tagname, long value, int flags);
	Ref createRWTag(String tagname, double value, int flags);
	Ref createRWTag(String tagname, String value, int flags);

	Ref createRWTag(String tagname, boolean value);
	Ref createRWTag(String tagname, int value);
	Ref createRWTag(String tagname, long value);
	Ref createRWTag(String tagname, double value);
	Ref createRWTag(String tagname, String value);



	// Create ref to any tag outside the script
	Ref createRef(String module, String tagname);

	// Create ref to the given "module:tagname" separated by a colon.
	// If the "module:" part is not specified, then the current script module name is used.
	Ref createRef(String moduleTagname);



	// miscellaneous
	void printInfo(String text);
	
	void printError(String text);

	void printError(Throwable e, String text);
	
	boolean isValid();

	Database getDatabase(String module);


	// methods for overriding
	// invoked once on startup
	boolean load();

	// invoked every system start
	boolean prepare();

	// invoked every cycle pass of the task
	void execute();


}

