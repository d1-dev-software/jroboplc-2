package promauto.jroboplc.plugin.peripherial;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.api.Flags;
import promauto.jroboplc.core.api.Plugin;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.TagRW;

/**
 *    Attention! Unverified source code, needs to be refactored
 *
 *    Внимание! Непроверенный исходный код, необходимо провести рефакторинг
 */
public class OwenPVT100Module extends PeripherialModule {

    private final Logger logger = LoggerFactory.getLogger(OwenPVT100Module.class);

    protected ProtocolModbus protocol = new ProtocolModbus(this);

    protected TagRW tag_RegReload; // 423330 to reload
    protected Tag tag_RegTempr;
    protected Tag tag_RegRH;
    protected Tag tag_RegDewPnt;

    public OwenPVT100Module(Plugin plugin, String name) {
        super(plugin, name);
    }


    @Override
    public boolean loadPeripherialModule(Object conf) {
        tag_RegTempr     = tagtable.createInt("Tempr", 0, Flags.STATUS);
        tag_RegRH        = tagtable.createInt("RH", 0, Flags.STATUS);
        tag_RegDewPnt    = tagtable.createInt("DewPnt", 0, Flags.STATUS);
        tag_RegReload    = protocol.addWriteTag( 0x11, tagtable.createRWInt("RLD", 0, Flags.STATUS));
        return true;
    }



    @Override
    public boolean executePeripherialModule() {
        if(emulated) {
            tag_RegReload.acceptWriteValue();
            return true;
        }

        boolean result = true;

        try {

            result = protocol.sendWriteTags(6);
            if( result ) {
                if(result = protocol.requestCmd3(0x0102, 3)) {
                    tag_RegTempr.setInt((short) protocol.getAnswerWord(0));
                    tag_RegRH.setInt(protocol.getAnswerWord(1));
                    tag_RegDewPnt.setInt(protocol.getAnswerWord(2));
                }

                if (result = protocol.requestCmd3(0x11, 1)) {
                    tag_RegReload.setReadValInt(protocol.getAnswerWord(0));
                }
            }


        } catch (Exception e) {
            env.printError(logger, e, name);
            result = false;
        }

        return result;
    }

}
