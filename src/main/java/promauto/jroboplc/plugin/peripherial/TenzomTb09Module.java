package promauto.jroboplc.plugin.peripherial;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.Flags;
import promauto.jroboplc.core.api.Plugin;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.TagRW;

@Deprecated
public class TenzomTb09Module extends PeripherialModule {
    private final Logger logger = LoggerFactory.getLogger(TenzomTb09Module.class);

    protected ProtocolTenzom protocol = new ProtocolTenzom(this);

    protected Tag tagSumWeightHigh;
    protected Tag tagSumWeightLow;
    protected Tag tagSumWeight;

    protected Tag tagSumNumHigh;
    protected Tag tagSumNumLow;
    protected Tag tagSumNum;

    protected Tag tagLastWeightHigh;
    protected Tag tagLastWeightLow;
    protected Tag tagLastWeight;

    protected Tag tagLastTimeHigh;
    protected Tag tagLastTimeLow;
    protected Tag tagLastTime;

    protected Tag tagOutputHigh;
    protected Tag tagOutputLow;
    protected Tag tagOutput;

    protected TagRW tagVersion;
    protected TagRW tagWesSvrState;

    private long wesSvrStateTimer;


    public TenzomTb09Module(Plugin plugin, String name) {
        super(plugin, name);
    }


    @Override
    public boolean loadPeripherialModule(Object conf) {
        Configuration cm = env.getConfiguration();
        int version = cm.get(conf, "version", 0);

        super.initCrc16Tags();
        crc16Tags.add( tagSumWeightHigh  = tagtable.createInt("SumWeightHigh",  0, Flags.STATUS) );
        crc16Tags.add( tagSumWeightLow   = tagtable.createInt("SumWeightLow",   0, Flags.STATUS) );
        crc16Tags.add( tagSumNumHigh     = tagtable.createInt("SumNumHigh",     0, Flags.STATUS) );
        crc16Tags.add( tagSumNumLow      = tagtable.createInt("SumNumLow",      0, Flags.STATUS) );
        crc16Tags.add( tagLastWeightHigh = tagtable.createInt("LastWeightHigh", 0, Flags.STATUS) );
        crc16Tags.add( tagLastWeightLow  = tagtable.createInt("LastWeightLow",  0, Flags.STATUS) );
        crc16Tags.add( tagLastTimeHigh   = tagtable.createInt("LastTimeHigh",   0, Flags.STATUS) );
        crc16Tags.add( tagLastTimeLow    = tagtable.createInt("LastTimeLow",    0, Flags.STATUS) );
        crc16Tags.add( tagOutputHigh     = tagtable.createInt("OutputHigh",     0, Flags.STATUS) );
        crc16Tags.add( tagOutputLow      = tagtable.createInt("OutputLow",      0, Flags.STATUS) );
        crc16Tags.add( tagError );

        tagSumWeight    = tagtable.createInt("SumWeight ",      0, Flags.STATUS);
        tagSumNum       = tagtable.createInt("SumNum",          0, Flags.STATUS);
        tagLastWeight   = tagtable.createInt("LastWeight",      0, Flags.STATUS);
        tagLastTime     = tagtable.createInt("LastTime",        0, Flags.STATUS);
        tagOutput       = tagtable.createInt("Output",          0, Flags.STATUS);

        tagVersion 	      = tagtable.createRWInt("Version", version, Flags.STATUS);
        tagWesSvrState	  = tagtable.createRWInt("WesSvrState", 0);

        return true;
    }



    @Override
    public boolean executePeripherialModule() {
        boolean result = true;

        if( emulated ) {
            tagVersion.acceptWriteValue();
        } else {

            try {

                if (result && (result = protocol.requestA(0xC8, 0x86))) {
                    long w;

                    w = protocol.getBCDValue(8, 5);
                    tagSumWeightHigh.setInt( (int)(w >> 16) );
                    tagSumWeightLow.setInt( (int)(w & 0xFFFF) );

                    w = protocol.getBCDValue( tagVersion.getInt()==0? 13: 18, 5);
                    tagSumNumHigh.setInt( (int)(w >> 16) );
                    tagSumNumLow.setInt( (int)(w & 0xFFFF) );


                    w = tagVersion.getInt()==0? protocol.getBCDValue(18, 5): 0;
                    tagLastWeightHigh.setInt( (int)(w >> 16) );
                    tagLastWeightLow.setInt( (int)(w & 0xFFFF) );

                    w = protocol.getBCDValue(23, 5);
                    tagOutputHigh.setInt( (int)(w >> 16) );
                    tagOutputLow.setInt( (int)(w & 0xFFFF) );

                    w = protocol.getBCDValue(28, 5);
                    tagLastTimeHigh.setInt( (int)(w >> 16) );
                    tagLastTimeLow.setInt( (int)(w & 0xFFFF) );
                }


            } catch (Exception e) {
                env.printError(logger, e, name);
                result = false;
            }
        }

        setDWordTag(tagSumWeight ,  tagSumWeightHigh,   tagSumWeightLow);
        setDWordTag(tagSumNum,      tagSumNumHigh,      tagSumNumLow);
        setDWordTag(tagLastWeight,  tagLastWeightHigh,  tagLastWeightLow);
        setDWordTag(tagLastTime,    tagLastTimeHigh,    tagLastTimeLow);
        setDWordTag(tagOutput,      tagOutputHigh,      tagOutputLow);

        wesSvrStateTimer = updateWesSvrState(tagWesSvrState, wesSvrStateTimer);
        return result;
    }

    private void setDWordTag(Tag tag, Tag high, Tag low) {
        tag.setInt(
                (int)(((((long)high.getInt()) << 16) + low.getInt()) & 0x7FFF_FFFF)
        );
    }


    @Override
    protected boolean reload() {
        TenzomTb09Module tmp = new TenzomTb09Module(plugin, name);
        if( !tmp.load() )
            return false;

        copySettingsFrom(tmp);
        tagVersion.setReadValInt( tmp.tagVersion.getInt() );

        return true;
    }


}
