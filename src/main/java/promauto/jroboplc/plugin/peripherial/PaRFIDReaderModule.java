package promauto.jroboplc.plugin.peripherial;

import java.time.LocalDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.api.Flags;
import promauto.jroboplc.core.api.Plugin;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.TagRW;

public class PaRFIDReaderModule extends PeripherialModule { // implements Database.EventListener {

	private static final int TAGS_IN_RECORD = 5;
	private static final int REGS_IN_RECORD = 4;

	private static final int RECORDS_SIZE = 1; //10;

	private final Logger logger = LoggerFactory.getLogger(PaRFIDReaderModule.class);
	
	protected ProtocolModbus protocol = new ProtocolModbus(this);
	
	private class Record {
		public TagRW tagCode0;
		public TagRW tagCode1;
		public TagRW tagCode2;
		public TagRW tagDate;
		public TagRW tagTime;
	}

    protected Tag tagCrc;
    protected TagRW tagVersion;
	protected TagRW tagLed0;
	protected TagRW tagLed1;
	protected TagRW tagLed2;
    protected Tag tagState;
    protected Tag tagReadCnt;
    protected Tag tagShiftCnt;
    
	protected TagRW tagSvrLive;
	private long wesSvrLiveTimer;

    
    protected Record[] records = new Record[RECORDS_SIZE];
    private int buffer[] = new int[RECORDS_SIZE * TAGS_IN_RECORD];
    
	protected Tag[] crcTags = new Tag[RECORDS_SIZE * TAGS_IN_RECORD + 3];

	private boolean needReadVersion;

	private int counter;     
	
	
	
	public PaRFIDReaderModule(Plugin plugin, String name) {
		super(plugin, name);
	}


	@Override
	public boolean loadPeripherialModule(Object conf) {

		tagCrc		= tagtable.createInt("crc", 0);
		tagVersion	= tagtable.createRWInt("version", 0x100, Flags.STATUS);
		tagLed0		= protocol.addWriteTag( 0x0FFA, tagtable.createRWInt("led0", 0, Flags.STATUS));
		tagLed1		= protocol.addWriteTag( 0x0FFE, tagtable.createRWInt("led1", 0, Flags.STATUS));
		tagLed2		= protocol.addWriteTag( 0x0FFF, tagtable.createRWInt("led2", 0, Flags.STATUS));
		
		int j = 0;
		crcTags[j++] = tagState		= tagtable.createInt("state", 0, Flags.STATUS);
		crcTags[j++] = tagReadCnt	= tagtable.createInt("cnt.read", 0, Flags.STATUS);
		crcTags[j++] = tagShiftCnt	= tagtable.createInt("cnt.shift", 0, Flags.STATUS);
		
		for(int i=0; i<RECORDS_SIZE; ++i) {
			Record r = new Record();
			r.tagCode0 	= createCrcTagIntRW(j++, "rec" + i + ".code0");
			r.tagCode1 	= createCrcTagIntRW(j++, "rec" + i + ".code1");
			r.tagCode2 	= createCrcTagIntRW(j++, "rec" + i + ".code2");
			r.tagDate 	= createCrcTagIntRW(j++, "rec" + i + ".date");
			r.tagTime 	= createCrcTagIntRW(j++, "rec" + i + ".time");
			records[i] = r;
		}

		protocol.adjustBuffers(0, crcTags.length*2);

		tagSvrLive		= tagtable.createRWInt("SvrLive", 0, Flags.STATUS);

		return true;
	}


	
	private TagRW createCrcTagIntRW(int idxCrcTag, String tagname) {
		TagRW tag = tagtable.createRWInt(tagname, 0, Flags.STATUS);
		crcTags[idxCrcTag] = (Tag)tag;
		return tag;
	}


	@Override
	protected boolean preparePeripherialModule() {
		needReadVersion = true;
		counter = -1;
		return true;
	}


	@Override
	public boolean executePeripherialModule() {
		boolean result = true;
		
		if(emulated) {
			tagVersion.acceptWriteValue();
			tagLed0.acceptWriteValue();
			tagLed1.acceptWriteValue();
			tagLed2.acceptWriteValue();
			for(Record rec: records) {
				rec.tagCode0.acceptWriteValue();
				rec.tagCode1.acceptWriteValue();
				rec.tagCode2.acceptWriteValue();
				rec.tagDate.acceptWriteValue();
				rec.tagTime.acceptWriteValue();
			}
			
		} else {
			
			try {
				result = protocol.sendWriteTags(6);
				
				if( result  &&  needReadVersion  &&  (result = protocol.requestCmd3(0x101, 1)) ) {
					tagVersion.setReadValInt(protocol.getAnswerWord(0) );
					needReadVersion = false;
				} 
			
	
				int tmpCounter = counter;
				boolean hasDataInBuffer = false;
				LocalDateTime curdt = null;
				while( result ) 
					if( result = protocol.requestCmd3(0x1000, 2) ) {
						tagState.setInt(protocol.getAnswerWord(0) );
						if( tmpCounter == protocol.getAnswerWord(1) ) 
							break;
						
						tmpCounter = protocol.getAnswerWord(1);
						
						curdt = LocalDateTime.now();
						
						result = result  &&  
								readBuffer(0x1002, 0, REGS_IN_RECORD * RECORDS_SIZE);
						
	//							readBuffer(0x1002, 0, 20)  &&  
	//							readBuffer(0x1016, 20, 20);
						
						hasDataInBuffer = true;
					}
	
				if( result  &&  hasDataInBuffer ) {
					counter = tmpCounter;
					tagReadCnt.setInt(counter & 0xFF);
					tagShiftCnt.setInt((counter >> 8) & 0xFF);
					for(int i=0, j=0; i<RECORDS_SIZE; ++i) {
						if( buffer[j+3] == 0xFFFF ) {
							records[i].tagCode0.setReadValInt( 0 );
							records[i].tagCode1.setReadValInt( 0 );
							records[i].tagCode2.setReadValInt( 0 );
							records[i].tagDate.setReadValInt( 0 );
							records[i].tagTime.setReadValInt( 0 );
							j += 4;
						} else {
							records[i].tagCode0.setReadValInt( buffer[j++] );
							records[i].tagCode1.setReadValInt( buffer[j++] );
							records[i].tagCode2.setReadValInt( buffer[j++] );
							LocalDateTime ldt = curdt.minusSeconds( buffer[j++]);
							records[i].tagDate.setReadValInt(  (ldt.getYear()%100)*10000 + ldt.getMonthValue()*100 + ldt.getDayOfMonth() );
							records[i].tagTime.setReadValInt(  ldt.getHour()*10000 + ldt.getMinute()*100 + ldt.getSecond() );
						}
						
					}
				}
				
				if( !result )
					needReadVersion = true;
	
	
			} catch (Exception e) {
				env.printError(logger, e, name);
				result = false;
			}
		}

		calcTagCrc(crcTags, tagCrc, protocol.buffin);
		wesSvrLiveTimer = updateWesSvrState(tagSvrLive, wesSvrLiveTimer);

		return result;
	}


	private boolean readBuffer(int regaddr, int offset, int size) throws Exception {
		if( !protocol.requestCmd3(regaddr, size) )
			return false;
			
		for(int i=0; i<size; ++i)
			buffer[offset+i] = protocol.getAnswerWord(i);
		
		return true;
	}



}
