package promauto.jroboplc.plugin.peripherial;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.api.Flags;
import promauto.jroboplc.core.api.Plugin;
import promauto.jroboplc.core.tags.TagRW;

import java.util.List;


public class OwenMu110_8iModule extends PeripherialModule {
    private final Logger logger = LoggerFactory.getLogger(OwenMu110_8iModule.class);

    protected ProtocolModbus protocol = new ProtocolModbus(this);


    protected TagRW     tagTOut;
    protected TagRW[] 	tagOut = null;
    protected TagRW[] 	tagAlm = null;

    protected final int size;
    protected final int regOut;
    protected final int regAlm;
    protected final int regTOut;


    public OwenMu110_8iModule(Plugin plugin, String name) {
        super(plugin, name);

        size = 8;
        regOut = 0;
        regAlm = 16;
        regTOut = 48;
    }


    @Override
    public boolean loadPeripherialModule(Object conf) {

        tagTOut	= protocol.addWriteTag( 48, tagtable.createRWInt("TOut", 0, Flags.STATUS));

        if( tagOut == null ) {
            tagOut = new TagRW[size];
            tagAlm = new TagRW[size];
        }

        for(int i = 0; i< size; ++i) {
            tagOut[i] = protocol.addWriteTag(regOut + i,
                    tagtable.createRWInt(String.format("out%02d", i), 0, Flags.STATUS));

            tagAlm[i] = protocol.addWriteTag(regOut + i,
                    tagtable.createRWInt(String.format("alm%02d", i), 0, Flags.AUTOSAVE + Flags.STATUS));
        }

        return true;
    }


    @Override
    protected void initChannelMap(List<String> chtags) {
        for( int i=0; i<size; ++i ) {
            addChannelMapTag(chtags, tagOut[i], "" + i);
        }
    }


    @Override
    public boolean executePeripherialModule() {

        if(emulated) {
            tagTOut.acceptWriteValue();
            for(int i = 0; i< size; ++i) {
                tagOut[i].acceptWriteValue();
                tagAlm[i].acceptWriteValue();
            }
            return true;
        }

        boolean result = true;

        try {
            if( result )
                result = protocol.sendWriteTags(16);

            result &= read(tagOut, regOut);

            if( result  &&  (firstPass  ||  tagError.getBool()) ) {
                if( result = protocol.requestCmd3(regTOut, 1) )
                    tagTOut.setReadValInt( protocol.getAnswerWord(0) );

                result &= read(tagAlm, regAlm);
            }

        } catch (Exception e) {
            env.printError(logger, e, name);
            result = false;
        }

        return result;
    }


    protected boolean read(TagRW[] tags, int addr) throws Exception {
        if( !protocol.requestCmd3(addr, size) )
            return false;

        for (int i = 0; i < size; ++i)
            tags[i].setReadValInt(protocol.getAnswerWord(i));
        return true;
    }



}
