package promauto.jroboplc.plugin.peripherial;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.api.Flags;
import promauto.jroboplc.core.api.Plugin;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.TagRW;
import promauto.utils.Numbers;

public class SchuleFlowModule extends PeripherialModule {
	private final Logger logger = LoggerFactory.getLogger(SchuleFlowModule.class);

	private static int BUFF_SIZE = 60;
	protected int[] buffin = new int[BUFF_SIZE];
	protected int[] buffout = new int[BUFF_SIZE];
	
	protected Tag tagCrc;

	protected Tag tagState;       
	protected Tag tagActFlow;     
	protected Tag tagTrgFlow;     
	              
	protected Tag tagTotalSumH;   
	protected Tag tagTotalSumL;   
	              
	protected Tag tagProdType;  
	protected Tag tagAlarmCode;   
	protected Tag tagTrgPresel;   
	protected Tag tagRestPresel;  
	protected Tag tagPreselExpir;
	protected Tag tagLenActive; 
	protected Tag tagActualLen;  
	protected Tag tagLenCtrl;     
	protected Tag tagTrgLen;      
	              
	protected Tag tagSetState;    
	protected Tag tagSetTrgFlow;  
	protected Tag tagRstTotalSum;
	protected Tag tagSetProdType;
	protected Tag tagSetTrgPresel;
	protected Tag tagSetLenCtrl;  
	protected Tag tagSetTrgLen;   
	              
	protected TagRW tagWesSvrState;
	

	private long wesSvrStateTimer;
	protected Tag[] crcTags;
    
	
	public SchuleFlowModule(Plugin plugin, String name) {
		super(plugin, name);
	}


	@Override
	public boolean loadPeripherialModule(Object conf) {

		tagCrc 			= tagtable.createInt("Crc", 			0);
		tagState		= tagtable.createInt("State", 		0, Flags.STATUS);
		tagActFlow      = tagtable.createInt("ActFlow", 		0, Flags.STATUS);
		tagTrgFlow      = tagtable.createInt("TrgFlow", 		0, Flags.STATUS);
		tagTotalSumH    = tagtable.createInt("TotalSumH", 	0, Flags.STATUS);
		tagTotalSumL    = tagtable.createInt("TotalSumL", 	0, Flags.STATUS);
		tagProdType     = tagtable.createInt("ProdType", 	0, Flags.STATUS);
		tagAlarmCode    = tagtable.createInt("AlarmCode", 	0, Flags.STATUS);
		tagTrgPresel    = tagtable.createInt("TrgPresel", 	0, Flags.STATUS);
		tagRestPresel   = tagtable.createInt("RestPresel", 	0, Flags.STATUS);
		tagPreselExpir  = tagtable.createInt("PreselExpir", 	0, Flags.STATUS);
		tagLenActive    = tagtable.createInt("LenActive", 	0, Flags.STATUS);
		tagActualLen    = tagtable.createInt("ActualLen", 	0, Flags.STATUS);
		tagLenCtrl      = tagtable.createInt("LenCtrl", 		0, Flags.STATUS);
		tagTrgLen       = tagtable.createInt("TrgLen", 		0, Flags.STATUS);
		tagSetState     = tagtable.createInt("SetState", 	0, Flags.STATUS);
		tagSetTrgFlow   = tagtable.createInt("SetTrgFlow", 	0, Flags.STATUS);
		tagRstTotalSum  = tagtable.createInt("RstTotalSum", 	0, Flags.STATUS);
		tagSetProdType  = tagtable.createInt("SetProdType", 	0, Flags.STATUS);
		tagSetTrgPresel = tagtable.createInt("SetTrgPresel", 0, Flags.STATUS);
		tagSetLenCtrl   = tagtable.createInt("SetLenCtrl", 	0, Flags.STATUS);
		tagSetTrgLen    = tagtable.createInt("SetTrgLen", 	0, Flags.STATUS);
		             
		tagWesSvrState  = tagtable.createRWInt("WesSvrState",  0);
		   
		crcTags = new Tag[]{
				tagTotalSumH,
				tagTotalSumL
			};
		   
		return true;
	}


	
	@Override
	public boolean executePeripherialModule() {
		
		boolean result = true;
		
		if( !emulated ) {
			try {
				
				buffout[0] = 0xDD;
				buffout[1] = 0xDD;
				buffout[2] = 60;
				buffout[3] = netaddr;
				buffout[4] = 0;
				buffout[5] = 0;
				
				setCmd(tagState, tagSetState, 6, 2);
				setZero( 10, 4); 
				setCmd(tagTrgFlow, tagSetTrgFlow, 14, 4);
				setZero( 20, 6);
				setCmd(tagProdType, tagSetProdType, 26, 2);
				setZero( 30, 4); 
				setCmd(tagTrgPresel, tagSetTrgPresel, 34, 4);
				setZero( 40, 10); 
				setCmd(tagLenCtrl, tagSetLenCtrl, 50, 2);
				setCmd(tagTrgLen, tagSetTrgLen, 54, 2);
				
				buffout[58] = 0x20;
				buffout[59] = 0x20;
				
				if( result = request() ) {
					tagState		.setInt( Numbers.bytesToWord(buffin, 8) );		
					tagActFlow    	.setInt( Numbers.bytesToInt( buffin, 10) );
					tagTrgFlow    	.setInt( Numbers.bytesToInt( buffin, 16) );
					              
					tagTotalSumH  	.setInt( Numbers.bytesToWord(buffin, 22) );
					tagTotalSumL  	.setInt( Numbers.bytesToWord(buffin, 24) );
					              
					tagProdType   	.setInt( Numbers.bytesToWord(buffin, 28) );
					tagAlarmCode  	.setInt( Numbers.bytesToWord(buffin, 30) );
					tagTrgPresel  	.setInt( Numbers.bytesToInt( buffin, 36) );
					tagRestPresel 	.setInt( Numbers.bytesToInt( buffin, 40) );
					tagPreselExpir	.setInt( Numbers.bytesToWord(buffin, 44) );
					tagLenActive  	.setInt( Numbers.bytesToWord(buffin, 46) );
					tagActualLen  	.setInt( Numbers.bytesToWord(buffin, 48) );
					tagLenCtrl    	.setInt( Numbers.bytesToWord(buffin, 52) );
					tagTrgLen     	.setInt( Numbers.bytesToWord(buffin, 56) );
				}
				
			} catch (Exception e) {
				env.printError(logger, e, name);
				result = false;
			}
		}
		
		calcTagCrc(crcTags, tagCrc, buffin);
		wesSvrStateTimer = updateWesSvrState(tagWesSvrState, wesSvrStateTimer);
		
		return result;
	}
	
	private void setZero(int idx, int size) {
		for(int i=0; i<size; ++i)
			buffin[i] = 0;
	}


	private void setCmd(Tag tagRead, Tag tagWrite, int addr, int size) {
		if( tagRead.getInt() == tagWrite.getInt() )
			setZero(addr, size*2 + 2);
		else {
			buffout[addr] = 0;
			buffout[addr+1] = 1;
			int v = tagWrite.getInt();
			for(int i=0; i<size; ++i)
				buffout[addr + size - i + 1] = (v >> (8*i)) & 0xFF;
		}
	}


	private boolean request() throws Exception {
		if( port == null )
			return false;
		
		for (int trynum = 0; trynum < retrial; trynum++) {

			port.discard();
			port.writeBytes(buffout, BUFF_SIZE);
			int n = port.readBytes(buffin, BUFF_SIZE);

			boolean badcrc = false;
			if( n > 0 ) {
				if( n == BUFF_SIZE  &&
					buffin[0] == 0xDD && 
					buffin[1] == 0xDD && 
					buffin[2] == 60 && 
					buffin[3] == netaddr && 
					buffin[58] == 0x20 && 
					buffin[59] == 0x20 
				) 
					return true;
				
				badcrc = true;
			}

			if( canLogError() ) {
				int cntRead = Math.abs(n);
				logError(trynum, badcrc, buffout, BUFF_SIZE, buffin, cntRead, "");
			}

			try {
				Thread.sleep(20);
			} catch (InterruptedException e) {
			}

		}
		tagErrorCnt.setInt( tagErrorCnt.getInt() + 1 );

    	return false;
	}


}
