package promauto.jroboplc.plugin.peripherial;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.api.Flags;
import promauto.jroboplc.core.api.Plugin;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.TagRW;

import java.util.List;

public class OwenMa110_1tModule extends PeripherialModule {
	private final Logger logger = LoggerFactory.getLogger(OwenMa110_1tModule.class);
	
	protected ProtocolModbus protocol = new ProtocolModbus(this);


	protected Tag	 	tagStatus;
    protected Tag	 	tagMode;
    
    protected TagRW 	tagDecN;
    protected TagRW 	tagDecI;
    protected TagRW 	tagDecF;

    protected Tag	 	tagValueN; 
    protected Tag 		tagValueI; 
    protected Tag 		tagValueF;
    
    protected TagRW 	tagApply;


	
	public OwenMa110_1tModule(Plugin plugin, String name) {
		super(plugin, name);
	}


	@Override
	public boolean loadPeripherialModule(Object conf) {

		tagStatus 	= tagtable.createInt("Status", 0, Flags.STATUS);
		tagMode 	= tagtable.createInt("Mode", 0, Flags.STATUS);
		tagDecN		= protocol.addWriteTag( 18, tagtable.createRWInt("DecN", 0, Flags.STATUS));
		tagDecI		= protocol.addWriteTag( 21, tagtable.createRWInt("DecI", 0, Flags.STATUS));
		tagDecF		= protocol.addWriteTag( 24, tagtable.createRWInt("DecF", 0, Flags.STATUS));
		
		tagValueN	= tagtable.createInt("ValueN", 0, Flags.STATUS);
		tagValueI	= tagtable.createInt("ValueI", 0, Flags.STATUS);
		tagValueF	= tagtable.createInt("ValueF", 0, Flags.STATUS);

		tagApply	= protocol.addWriteTag( 33, tagtable.createRWInt("Apply129", 0, Flags.STATUS));

		return true;
	}


	@Override
	protected void initChannelMap(List<String> chtags) {
		addChannelMapTag(chtags, tagValueF, "0");
	}



	@Override
	public boolean executePeripherialModule() {
		
		if(emulated) {
			tagDecN.acceptWriteValue();
			tagDecI.acceptWriteValue();
			tagDecF.acceptWriteValue();
			tagApply.acceptWriteValue();
			
			return true;
		}
		
		boolean result = true;
		
		try {
			
			if( result )
				result = protocol.sendWriteTags(0x6);
			
			if( result )
				if( result = protocol.requestCmd3(16, 18) ) {
					tagStatus	.setInt( protocol.getAnswerWord(0) ); 
					tagMode		.setInt( protocol.getAnswerWord(1) );
					
					tagDecN		.setReadValInt( protocol.getAnswerWord(2) );
					tagValueN	.setInt( protocol.getAnswerInt32(3) );

					tagDecI		.setReadValInt( protocol.getAnswerWord(5) );
					tagValueI	.setInt( protocol.getAnswerInt32(6) );

					tagDecF		.setReadValInt( protocol.getAnswerWord(8) );
					tagValueF	.setInt( protocol.getAnswerInt32(9) );
					
					//11
					//13
					//15
					
					tagApply	.setReadValInt( protocol.getAnswerWord(17) );
				}

			
		} catch (Exception e) {
			env.printError(logger, e, name);
			result = false;
		}
	
		return result;
	}




}
