package promauto.jroboplc.plugin.peripherial;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.Flags;
import promauto.jroboplc.core.api.Plugin;
import promauto.jroboplc.core.api.Tag;
import promauto.utils.Numbers;

import java.util.zip.CRC32;

public class UpsNanoModule extends PeripherialModule {
	private final Logger logger = LoggerFactory.getLogger(UpsNanoModule.class);


	private static int BUFFOUT_SIZE = 15;
	private static int BUFFIN_SIZE = 8;
	protected int[] buffout = new int[BUFFOUT_SIZE];
	protected int[] buffin = new int[BUFFIN_SIZE];

	protected Tag tagBatTimeout;
	protected Tag tagSerTimeout;
    protected Tag tagOutAux;
    protected Tag tagInpAcok;
    protected Tag tagInpBatlow;
    protected Tag tagInpAux;
    protected Tag tagT;
	protected Tag tagShutdown;
	protected Tag tagBeep;
	protected Tag tagVersion;

	private CRC32 crc = new CRC32();

	public UpsNanoModule(Plugin plugin, String name) {
		super(plugin, name);
	}


	@Override
	public boolean loadPeripherialModule(Object conf) {

		tagBatTimeout 	= tagtable.createInt("BatTimeout",60, Flags.AUTOSAVE);
		tagSerTimeout 	= tagtable.createInt("SerTimeout",3, Flags.AUTOSAVE);
		tagBeep 		= tagtable.createInt("beep", 		0, Flags.AUTOSAVE);

		tagInpAcok 		= tagtable.createInt("InpAcok", 	0, Flags.STATUS);
		tagInpBatlow 	= tagtable.createInt("InpBatlow", 0, Flags.STATUS);
		tagInpAux 		= tagtable.createInt("InpAux", 	0, Flags.STATUS);
		tagOutAux 		= tagtable.createInt("OutAux", 	0, Flags.STATUS);
		tagT 			= tagtable.createInt("T", 			0, Flags.STATUS);
		tagShutdown 	= tagtable.createInt("shutdown", 	0, Flags.STATUS);
		tagVersion		= tagtable.createInt("version", 	0, Flags.STATUS);

		return true;
	}


	
	@Override
	public boolean executePeripherialModule() {
		
		boolean result = true;
		
		if( !emulated ) {
			try {
				
				buffout[0] = tagVersion.getInt();
				Numbers.wordToBytes( tagBatTimeout.getInt(), buffout, 1 );
				Numbers.wordToBytes( tagSerTimeout.getInt(), buffout, 3 );
				buffout[5] = 0;
				buffout[6] = 0;
				buffout[7] = 0;
				buffout[8] = 0;
				buffout[9] = tagOutAux.getInt();
				buffout[10] = (tagBeep.getInt() << 4);

				long cs = getCrc(buffout, BUFFOUT_SIZE-4);
				Numbers.dwordToBytes( cs, buffout, BUFFOUT_SIZE-4 );

				if( result = request() ) {
                    tagInpAcok	.setInt( 	buffin[1] & 0x01 );
                    tagInpBatlow.setBool( (	buffin[1] & 0x02) > 0 );
                    tagInpAux	.setBool( (	buffin[1] & 0x04) > 0 );
					tagT    	.setInt( Numbers.bytesToWord( buffin, 2) );
				}
				
			} catch (Exception e) {
				env.printError(logger, e, name);
				result = false;
			}

			tagShutdown.setBool( result  &&  tagInpAcok.getInt() == 0 );
		}

		return result;
	}

	private long getCrc(int[] buff, int len) {
		crc.reset();
		for(int i=0; i<len; i++)
			crc.update(buff[i]);
		return crc.getValue();
	}


	private boolean request() throws Exception {


		if( port == null )
			return false;
		
		for (int trynum = 0; trynum < retrial; trynum++) {

			port.discard();
			port.writeBytes(buffout, BUFFOUT_SIZE);
			int n = port.readBytes(buffin, BUFFIN_SIZE);

			if( n == BUFFIN_SIZE ) {
				long cs1 = getCrc(buffin, BUFFIN_SIZE-4);
				long cs2 = Numbers.bytesToUInt( buffin, BUFFIN_SIZE-4 );

				if( cs1 == cs2 ) {
					if(buffin[0] == 0xFF)
						changeVersion(buffin[1]);
					else
						return true;
				}

			}

			try {
				Thread.sleep(20);
			} catch (InterruptedException e) {
			}

		}
		tagErrorCnt.setInt( tagErrorCnt.getInt() + 1 );

    	return false;
	}

	private void changeVersion(int new_version) {
		tagVersion.setInt( new_version );
	}


}
