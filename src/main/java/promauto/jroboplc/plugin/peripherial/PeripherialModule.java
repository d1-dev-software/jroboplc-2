package promauto.jroboplc.plugin.peripherial;


import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.AbstractModule;
import promauto.jroboplc.core.State;
import promauto.jroboplc.core.api.*;
import promauto.jroboplc.core.tags.TagRW;
import promauto.utils.CRC;
import promauto.utils.Numbers;

public class PeripherialModule extends AbstractModule {
	private final Logger logger = LoggerFactory.getLogger(PeripherialModule.class);

	static DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HHmmss");
	public static final String TAGNAME_CHANNELMAP = "CHANNELMAP";

	protected TagRW tagPort;
	protected TagRW tagNetAddr;
	protected Tag tagError;
	protected Tag tagErrorCnt;
	protected Tag tagTimePeriod;
	protected Tag tagUpdateDate;
	protected Tag tagUpdateTime;
	protected Tag tagCrc16;
	protected Tag tagCrc32;
	protected List<TagRW> tagChannelMap;

	protected int portnum;
	protected int netaddr;
	protected int retrial;
	protected boolean emulated;
	protected String type;
	protected String descr;
	protected String systagpref;
	protected volatile int logErrorCnt;
	protected int delay;

	protected SerialPort port;

	protected List<Tag> crc16Tags;
	protected List<Tag> crc32Tags;

	protected boolean firstPass;


	public PeripherialModule(Plugin plugin, String name) {
		super(plugin, name);
		env.getCmdDispatcher().addCommand(this, CmdLogError.class);
	}
	
	
	public final boolean loadModule(Object conf) {
		Configuration cm = env.getConfiguration();
		
		portnum 	= cm.get(conf, "portnum", 0);
		netaddr 	= cm.get(conf, "netaddr", 0);
		emulated 	= cm.get(conf, "emulate", false);
		type 		= cm.get(conf, "type", "");
		descr 		= cm.get(conf, "descr", "");
		systagpref	= cm.get(conf, "systag", "SYSTEM.");
		retrial		= Math.max(1, cm.get(conf, "retrial", 1));
		logErrorCnt	= cm.get(conf, "logerror", 0);
		delay    	= cm.get(conf, "delay_ms", 0);

		tagError 	  = 	tagtable.createBool(systagpref + "ErrorFlag",	false);
		tagErrorCnt   = 	tagtable.createInt(systagpref + "ErrorCount", 	0);
		tagTimePeriod = 	tagtable.createInt(systagpref + "TimePeriod", 	0);
		tagUpdateDate = 	tagtable.createInt(systagpref + "UpdateDate", 	0);
		tagUpdateTime = 	tagtable.createInt(systagpref + "UpdateTime", 	0);
		tagPort 	  = 	tagtable.createRWInt(systagpref + "Port", portnum);
		tagNetAddr 	  = 	tagtable.createRWInt(systagpref + "NetAddr", netaddr);

		if( !loadPeripherialModule(conf) )
			return false;

		if( cm.get(conf, "chmap", false) )
			createChannelMap(
					cm.get(conf, "chmap.name", ""),
					cm.get(conf, "chmap.value", "")
					);


		return true;
	}


	protected void initCrc16Tags() {
		tagCrc16 = tagtable.createInt("Crc16", 0);
		crc16Tags = new ArrayList<>();
	}

	protected void initCrc32Tags() {
		tagCrc32 = tagtable.createLong("Crc32", 0);
		crc32Tags = new ArrayList<>();
	}

	/**
		"Tags" argument is an array of either string tag names or tag instances
	*/
	protected void addCrc32Tags(Object... tags) {
		if( crc32Tags == null )
			initCrc32Tags();

		for(Object obj: tags) {
			if(obj instanceof Tag)
				crc32Tags.add((Tag)obj);
			else {
				Tag tag = tagtable.get(obj.toString());
				if (tag == null)
					throw new RuntimeException("Crc32Tag not found: " + obj);

				crc32Tags.add(tag);
			}
		}
	}



	protected boolean loadPeripherialModule(Object conf) {
		return true;
	}


	private void createChannelMap(String chmapName, String chmapValue) {
		List<String> chtags = new ArrayList<>();

		if( chmapValue.isEmpty() ) {
			initChannelMap(chtags);
		} else {
			chtags.addAll(Arrays.asList( chmapValue.trim().split("\\s+") ));
		}

		if( chtags.size() == 0)
			return;

		String header = name  + (chmapName.trim().isEmpty()? "": ":" + chmapName);
		int headerlen = header.length();

		tagChannelMap = new ArrayList<>();
		StringBuilder sb = new StringBuilder();
		int tagidx = 0;

		for(String chtag: chtags) {
			chtag = ' ' + chtag.trim();
			if( headerlen + sb.length() + chtag.length() > Tag.STRING_LENGTH_MAX) {
				createTagChannelMap(tagidx, header + sb.toString());
				sb.setLength(0);
				tagidx++;
			}

			sb.append(chtag);
		}

		if( sb.length() > 0 )
			createTagChannelMap( tagidx, header + sb.toString());
	}


	private void createTagChannelMap(int tagidx, String value) {
		tagChannelMap.add( tagtable.createRWString(systagpref + TAGNAME_CHANNELMAP + '.' + tagidx, value));
	}

	protected void addChannelMapTag(List<String> chtags, Tag tag, String shortTagname) {
		if( tag.getName().equals(shortTagname) ) {
			chtags.add( tag.getName() );
		} else {
			chtags.add( String.format("%s:%s", tag.getName(), shortTagname) );
		}
	}

	protected void addChannelMapTag(List<String> chtags, Tag tag) {
		chtags.add( tag.getName() );
	}

	protected void initChannelMap(List<String> chtags) {
		// to be overriden
	}



	@Override
	public final boolean prepareModule() {
		port = null;
		firstPass = true;

		tagError.setBool(true);
		tagtable.setTagState(Tag.Status.Bad, Flags.STATUS);

		return preparePeripherialModule();
	}

	protected boolean preparePeripherialModule() {
		return true;
	}


	
	@Override
	public final boolean executeModule() {
		long timems = System.currentTimeMillis();
		
		boolean res = true;
		if( !emulated ) {
			if( port == null  ||  !port.isValid() )	{
				port = env.getSerialManager().getPort(portnum);
				if( port == null ) {
					res = false;
				}
			}
		}

		int tmpUpdateDate = Integer.parseInt( LocalDate.now().format( DateTimeFormatter.BASIC_ISO_DATE ));
		int tmpUpdateTime = Integer.parseInt( LocalTime.now().format( timeFormatter ));

		if( res )
			res = executePeripherialModule();

		boolean hasStatusChanged = tagError.getBool() == res;
		if( emulated ) {
			if( firstPass )
				tagError.setBool(false);
		} else {
			tagError.setBool(!res);
		}
		tagUpdateDate.setInt( tmpUpdateDate );
		tagUpdateTime.setInt( tmpUpdateTime );

		if( hasStatusChanged  )
			tagtable.setTagState(
					res? Tag.Status.Good: Tag.Status.Bad,
					Flags.STATUS);

		int tp = (int)(System.currentTimeMillis() - timems);
		if( tagTimePeriod.getInt() == tp )
			tp++;
		tagTimePeriod.setInt( tp );

		calcCrc16();
		calcCrc32();


		if( hasStatusChanged )
			env.logInfo(logger, (res? "Connected": "Disconnected") + ": " + name);

		firstPass = false;
		return res;
	}



	public void calcCrc16() {
		if( crc16Tags != null  &&  tagCrc16 != null )
			tagCrc16.setInt( CRC.getCrc16FromWordStream( crc16Tags.stream().map(a -> a.getInt()) ) );
	}

	public void calcCrc32() {
		if( crc32Tags != null  &&  tagCrc32 != null )
			tagCrc32.setLong( CRC.getOctoCrc32FromTagStream( crc32Tags.stream() ) );
	}



	protected boolean executePeripherialModule() {
		return true;
	}

	

	@Override
	public String getInfo() {
		return enable?
				(emulated? 
					"emulated":
					(tagError.getBool()? ANSI.RED + ANSI.BOLD + "ERROR! " + ANSI.RESET: "") +
						String.format("p%-7s ", portnum + " a" + netaddr) +
						"errcnt=" + tagErrorCnt.getInt() +
						" " + type + 
						(!descr.isEmpty()? " (" + descr + ")": "")
				)
			  : "disabled";
	}

	
	protected void calcTagCrc(Tag[] crcTags, Tag tagCrc, int[] buff) {
		if( buff.length < crcTags.length * 2 ) {
			tagCrc.setInt(-1);
			return;
		}
		int i = 0;
		for(Tag tag: crcTags) {
			int v = tag.getInt();
			buff[i++] = (v >> 8) & 0xFF;
			buff[i++] = v & 0xFF;
		}
		tagCrc.setInt( CRC.getCrc8(buff, 0, crcTags.length*2-1) );
	}
	

	protected static long updateWesSvrState(TagRW tag, long timer) {
		if( tag.hasWriteValue() ) {
			tag.setReadValInt( tag.getWriteValInt() | 1 );
			timer = System.currentTimeMillis();
		} else {
			if( (System.currentTimeMillis() - timer) > 10000 )
				tag.setReadValInt( 0 );
		}
		return timer;
	}

	

	@Override
	public void copySettingsFrom(AbstractModule src) {
		PeripherialModule tmp = (PeripherialModule)src;
		super.copySettingsFrom(tmp);
		portnum    = tmp.portnum;   
		netaddr    = tmp.netaddr;   
		retrial    = tmp.retrial;   
		emulated   = tmp.emulated;  
		type       = tmp.type;      
		descr      = tmp.descr;     
		systagpref = tmp.systagpref;
		logErrorCnt= tmp.logErrorCnt;
		delay      = tmp.delay;

		tagPort.setReadValInt( tmp.portnum );
		tagNetAddr.setReadValInt( tmp.netaddr );
		
		port = null;
		firstPass = true;
	}

	
	@Override
	protected boolean reload() {
		PeripherialModule tmp = new PeripherialModule(plugin, name);
		if( !tmp.load() )
			return false;

		closedown();
		
		copySettingsFrom(tmp);
		
		return prepare();
	}


	
	@Override
	public void saveState(State state) {
		tagtable.values().stream()
			.filter(tag -> tag != tagPort  &&  tag != tagNetAddr)
			.forEach(tag -> state.saveTag(tag));
	}

	
	@Override
	public void loadState(State state) {
		tagtable.values().stream()
				.forEach(tag -> state.loadTag(tag));
	}


	public boolean canLogError() {
		if( logErrorCnt <= 0 )
			return false;
		logErrorCnt--;
		return true;
	}


	public void logError(int trynum, boolean badcrc,  
			int[] buffout, int sizeout, int[] buffin, int sizein, String text) {
				
		StringBuilder sb = new StringBuilder();
		sb.append("error " + (badcrc? "crc": "read"));
		sb.append(", try=" + trynum);
		sb.append("\r\n");

		sb.append(", out:");
		for(int i=0; i<sizeout; ++i)
			sb.append( " " + Numbers.toHexString(buffout[i], 2));
		sb.append(", len=" + sizeout);
		sb.append("\r\n");

		sb.append(", inp:");
		for(int i=0; i<sizein; ++i)
			sb.append( " " + Numbers.toHexString(buffin[i], 2));
		sb.append(", len=" + sizein);
		sb.append("\r\n");

		if( !text.isEmpty())
			sb.append(", " + text);
		sb.append("\r\n");
		sb.append("\r\n");

		logError(sb.toString());
	}

	
	public void logError(String text) {
		EnvironmentInst.get().printError(logger, name, text
				+ (logErrorCnt==0? "\r\nNo more further logging!": "") );
	}


	public void delayBeforeWrite() {
		if( delay > 0 )
			try {
				Thread.sleep(delay);
			} catch (InterruptedException e) {
			}
	}

	public void delayAfterError() {
		try {
			Thread.sleep(20);
		} catch (InterruptedException e) {
		}
	}
}