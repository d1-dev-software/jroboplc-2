package promauto.jroboplc.plugin.peripherial;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.api.Flags;
import promauto.jroboplc.core.api.Plugin;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.TagRW;

public class IdsDriveEczModule extends PeripherialModule {
	private final Logger logger = LoggerFactory.getLogger(IdsDriveEczModule.class);

	protected ProtocolModbus protocol = new ProtocolModbus(this);


	protected Tag	 	tagFreqReq;
	protected Tag	 	tagFreqOut;
	protected Tag	 	tagCurrOut;
	protected Tag	 	tagRotSpd;
	protected Tag	 	tagVoltage;
	protected Tag	 	tagTemper;
	protected Tag	 	tagError1;
	protected Tag	 	tagError2;
	protected Tag	 	tagError3;
	protected Tag	 	tagError4;

	protected TagRW		tagControl;
	protected TagRW		tagFreqSet;



	public IdsDriveEczModule(Plugin plugin, String name) {
		super(plugin, name);
	}


	@Override
	public boolean loadPeripherialModule(Object conf) {

		tagFreqReq	= tagtable.createInt("FreqReq", 0, Flags.STATUS);
		tagFreqOut = tagtable.createInt("FreqOut", 0, Flags.STATUS);
		tagCurrOut	= tagtable.createInt("CurrOut", 0, Flags.STATUS);
		tagRotSpd 	= tagtable.createInt("RotSpd", 0, Flags.STATUS);
		tagVoltage	= tagtable.createInt("Voltage", 0, Flags.STATUS);
		tagTemper 	= tagtable.createInt("Temper", 0, Flags.STATUS);
		tagError1 	= tagtable.createInt("Error1", 0, Flags.STATUS);
		tagError2 	= tagtable.createInt("Error2", 0, Flags.STATUS);
		tagError3 	= tagtable.createInt("Error3", 0, Flags.STATUS);
		tagError4 	= tagtable.createInt("Error4", 0, Flags.STATUS);

		tagControl	= protocol.addWriteTag( 0x2000, tagtable.createRWInt("Control", 0, Flags.STATUS));
		tagFreqSet	= protocol.addWriteTag( 0x2001, tagtable.createRWInt("FreqSet", 0, Flags.STATUS));

		return true;
	}


	


	@Override
	public boolean executePeripherialModule() {
		
		if(emulated) {
			tagFreqSet.acceptWriteValue();
			return true;
		}
		
		boolean result = true;
		
		try {
			
			if( result )
				result = protocol.sendWriteTags(0x6);
			
			if( result )
				if( result = protocol.requestCmd3(1, 13) ) {
					tagFreqReq	.setInt( protocol.getAnswerWord(0) );
					tagFreqOut	.setInt( protocol.getAnswerWord(1) );
					tagCurrOut	.setInt( protocol.getAnswerWord(2) );
					tagRotSpd 	.setInt( protocol.getAnswerWord(3) );
					tagVoltage	.setInt( protocol.getAnswerWord(4) );
					tagTemper 	.setInt( protocol.getAnswerWord(5) );
					tagError1 	.setInt( protocol.getAnswerWord(9) );
					tagError2 	.setInt( protocol.getAnswerWord(10) );
					tagError3 	.setInt( protocol.getAnswerWord(11) );
					tagError4 	.setInt( protocol.getAnswerWord(12) );

					tagFreqSet.setReadValInt( tagFreqReq.getInt() );
				}

		} catch (Exception e) {
			env.printError(logger, e, name);
			result = false;
		}
	
		return result;
	}




}
