package promauto.jroboplc.plugin.peripherial;

import promauto.jroboplc.core.api.Plugin;

public class PaPdioVlvModule extends PaPdioModule {


	static final int[] pOffsetVlv = { 1, 4, 2, 8,   32, 16, 64, 128 };

	public PaPdioVlvModule(Plugin plugin, String name) {
		super(plugin, name);
	}

	
	protected void decodeBuffinToInputs() {
		for (int i = 0; i < INP_SIZE; i++) 
			inputs[i].setBool( (buffin[i / 8] & pOffsetVlv[i % 8]) > 0 );
	}

	protected void encodeOutputsToBuffout() {
		int b = 0;

		for (int i = 0; i < 64; i++) {
			if( outputs[i].getBool() ) 
				b = b + pOffsetVlv[i % 8];
				
			if ((i + 1) % 8 == 0) {
				buffout[i / 8 + 2] = b;
				b = 0;
			}
		}
	}

}


