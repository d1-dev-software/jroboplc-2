package promauto.jroboplc.plugin.peripherial;

import promauto.jroboplc.core.AbstractCommand;
import promauto.jroboplc.core.api.Console;
import promauto.jroboplc.core.api.Module;

public class CmdLogError extends AbstractCommand {
	
	


	@Override
	public String getName() {
		return "logerror";
	}

	@Override
	public String getUsage() {
		return "cnt";
	}

	@Override
	public String getDescription() {
		return "sets logerror counter value";
	}

	
	
	
	@Override
	public String execute(Console console, Module module, String args) {
		
		PeripherialModule m = (PeripherialModule) module;

		if( args.trim().isEmpty() ) {
			return "logerror = " + m.logErrorCnt;
		}

		m.logErrorCnt = Integer.parseInt(args);
		
		return "OK";
	}


}
