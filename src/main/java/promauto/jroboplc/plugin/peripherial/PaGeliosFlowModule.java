package promauto.jroboplc.plugin.peripherial;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.Flags;
import promauto.jroboplc.core.api.Plugin;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.TagRW;
import promauto.utils.Numbers;

public class PaGeliosFlowModule extends PeripherialModule {

	private final Logger logger = LoggerFactory.getLogger(PaGeliosFlowModule.class);
	
	protected ProtocolModbus protocol = new ProtocolModbus(this);
	
	protected Tag tagCrc;
	protected Tag tagState;
	protected Tag tagPrzCur;
	protected Tag tagNeto;
	
	protected Tag tagWeightMainH;
	protected Tag tagWeightMainL;
	
	protected Tag tagCmd;
	protected Tag tagReset;
	protected TagRW tagPrzReq;
	
	protected Tag tagErrorCode;
	protected Tag tagIn;
	protected Tag tagOut;
	protected Tag tagADCInit;
	protected Tag tagADCSetGr;
	protected Tag tagADCCtrl;
	protected Tag tagADCMain;
	protected Tag tagADCRequest;
	
	protected Tag tagAnswer;

	protected TagRW tagWesSvrState;

	protected Tag[] crcTags;
    private int[] buffCalcCrc;
    
	private long wesSvrStateTimer;

    private int curOutputAvgSize = 0;
    private int curOutputAvgIdx = 0;
    private int[] curOutputAvgCache;
    
    

	
	
// Read extra tag:
// Out: 00 03 00 00 00 11 84 17 
// In : 00 03 22 00 00 00 00 00 00 00 14 A5 28 00 01 C3 5E 00 00 27 10 00 00 00 00 00 01 00 00 00 00 00 00 00 00 00 00 44 FC 
	 
	
	public PaGeliosFlowModule(Plugin plugin, String name) {
		super(plugin, name);
	}


	@Override
	public boolean loadPeripherialModule(Object conf) {
		Configuration cm = env.getConfiguration();
		
		curOutputAvgSize = cm.get(conf, "averaging", 0);
		curOutputAvgCache = new int[curOutputAvgSize];

		tagCrc = tagtable.createInt("Crc", 0);
		tagState = tagtable.createInt("State", 0, Flags.STATUS);
		tagPrzCur = tagtable.createInt("CurOutput", 0, Flags.STATUS);
		tagNeto = tagtable.createInt("CurWeight", 0, Flags.STATUS);
		   
		tagWeightMainH = tagtable.createInt("SumWeightHigh", 0, Flags.STATUS);
		tagWeightMainL = tagtable.createInt("SumWeightLow", 0, Flags.STATUS);
		crcTags = new Tag[]{
				tagWeightMainH,
				tagWeightMainL
			};
		buffCalcCrc = new int[crcTags.length*2];
		   
		tagCmd = tagtable.createInt("Cmd", 0, Flags.STATUS);
		tagReset = tagtable.createInt("Reset", 0, Flags.STATUS);
		tagPrzReq = tagtable.createRWInt("ReqOutput", 0, Flags.STATUS);
		   
		tagErrorCode = tagtable.createInt("ErrorCode", 0, Flags.STATUS);
		tagIn = tagtable.createInt("In", 0, Flags.STATUS);
		tagOut = tagtable.createInt("Out", 0, Flags.STATUS);
		tagADCInit = tagtable.createInt("ADCInit", 0, Flags.STATUS);
		tagADCSetGr = tagtable.createInt("ADCSetGr", 0, Flags.STATUS);
		tagADCCtrl = tagtable.createInt("ADCCtrl", 0, Flags.STATUS);
		tagADCMain = tagtable.createInt("ADCMain", 0, Flags.STATUS);
		tagADCRequest = tagtable.createInt("ADCRequest", 0, Flags.STATUS);
		   
		tagAnswer = tagtable.createInt("Answer", 0, Flags.STATUS);
           
		tagWesSvrState = tagtable.createRWInt("WesSvrState", 0);

		return true;
	}

	@Override
	public boolean preparePeripherialModule() {
		curOutputAvgIdx = 0;
		for(int i=0; i<curOutputAvgSize; ++i)
			curOutputAvgCache[i] = -1;
		return true;
	}

	
	@Override
	public boolean executePeripherialModule() {
		
		boolean result = true;
		
		if( !emulated ) {
			try {
				// write PrzReq
				if( tagPrzReq.hasWriteValue() ) {
					int value = tagPrzReq.getWriteValInt();
					result = 
							protocol.requestCmd6(7, (value >> 16) & 0xFFFF)  &&
							protocol.requestCmd6(8, value & 0xFFFF); 
					if( !result ) 
						tagPrzReq.setInt(value);
				}
				
				// write cmd
				if( result  &&  tagState.getInt() < 2  &&  tagState.getInt() != tagCmd.getInt() ) {
					result = protocol.requestCmd6(0, tagCmd.getInt());
				}

				// reset
				if( result  &&  tagReset.getInt() > 0 ) {
					int reset = 0;
					if( tagState.getInt() == 2) {
						result = protocol.requestCmd6(0, tagCmd.getInt());
						if( !result ) 
							reset = 1;
					}
					tagReset.setInt(reset);
				}
					
				// read
				result &= protocol.requestCmd3(0, 17);
				if( result ) {
					tagState.setInt(		Numbers.bytesToWord(protocol.buffin, 3));
					tagPrzCur.setInt(		calcCurOutputAverage( Numbers.bytesToInt(protocol.buffin, 5)) );
					tagNeto.setInt(			Numbers.bytesToInt(protocol.buffin, 9));
					tagWeightMainH.setInt(	Numbers.bytesToWord(protocol.buffin, 13));
					tagWeightMainL.setInt(	Numbers.bytesToWord(protocol.buffin, 15));
					tagPrzReq.setReadValInt(	Numbers.bytesToInt(protocol.buffin, 17));
					tagErrorCode.setInt(	Numbers.bytesToWord(protocol.buffin, 21));
					tagIn.setInt(			Numbers.bytesToWord(protocol.buffin, 23));
					tagOut.setInt(			Numbers.bytesToWord(protocol.buffin, 25));
					tagADCInit.setInt(		Numbers.bytesToWord(protocol.buffin, 27));
					tagADCSetGr.setInt(		Numbers.bytesToWord(protocol.buffin, 29));
					tagADCCtrl.setInt(		Numbers.bytesToWord(protocol.buffin, 31));
					tagADCMain.setInt(		Numbers.bytesToWord(protocol.buffin, 33));
					tagADCRequest.setInt(	Numbers.bytesToWord(protocol.buffin, 35));
				}
				
			} catch (Exception e) {
				env.printError(logger, e, name);
				result = false;
			}
		} else {
			tagPrzReq.acceptWriteValue();
		}
		
		calcTagCrc(crcTags, tagCrc, buffCalcCrc);
		wesSvrStateTimer = updateWesSvrState(tagWesSvrState, wesSvrStateTimer);
		
		return result;
	}
	

	private int calcCurOutputAverage(int value) {
		if( curOutputAvgSize == 0 )
			return value;
		
		if( value == 0 ) {
			if( curOutputAvgCache[0] != -1 )
			{
				curOutputAvgIdx = 0;
				for(int i=0; i<curOutputAvgSize; ++i)
					curOutputAvgCache[i] = -1;
			}
			return value;
		}
		
		int n = 0;
		int sum = 0;
		if( curOutputAvgIdx >= curOutputAvgSize )
			curOutputAvgIdx = 0;
		
		curOutputAvgCache[curOutputAvgIdx] = value;
		++curOutputAvgIdx;

		for(int i=0; i<curOutputAvgSize; ++i)
			if( curOutputAvgCache[i] >= 0 ) {
				sum = sum + curOutputAvgCache[i];
				++n;
			}
		
		return (n>0)? sum/n: 0;
	}

	
	@Override
	protected boolean reload() {
		PaGeliosFlowModule tmp = new PaGeliosFlowModule(plugin, name);
		if( !tmp.load() )
			return false;

		copySettingsFrom(tmp);
		
		if(curOutputAvgSize != tmp.curOutputAvgSize) {
			curOutputAvgSize = tmp.curOutputAvgSize;
			curOutputAvgCache = tmp.curOutputAvgCache;
			prepare();
		}
		
		return true;
	}




}
