package promauto.jroboplc.plugin.peripherial;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.Flags;
import promauto.jroboplc.core.api.Plugin;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.TagRW;

/**
 *    Attention! Unverified source code, needs to be refactored
 *
 *    Внимание! Непроверенный исходный код, необходимо провести рефакторинг
 */
public class KastBOT01Module  extends PeripherialModule {

    private final Logger logger = LoggerFactory.getLogger(KastBOT01Module.class);

    protected ProtocolModbus protocol = new ProtocolModbus(this);


    protected Tag tagState;
    protected Tag tagWorkCnt;
    protected Tag tagCurPdv;
    protected TagRW tagSearch;

    private static final int MAX_SENS_CNT = 50;
    private static final int MAX_PDV_CNT  = 8;

    private static final int[] PDVi_SENS_ADR_START  = {101, 152, 203, 254, 305, 356, 407, 458};

    public Tag[][] tagT;
    public Tag[] pdvTMax;

    private int[] PDViSensCnt;
    private int   pdvCnt;


    public KastBOT01Module(Plugin plugin, String name) {
        super(plugin, name);
    }

    @Override
    public boolean loadPeripherialModule(Object conf) {
        Configuration cm = env.getConfiguration();

        pdvCnt = cm.get(conf, "pdvCnt", MAX_PDV_CNT);
        pdvCnt = pdvCnt > MAX_PDV_CNT ? MAX_PDV_CNT : pdvCnt;



        PDViSensCnt = new int[pdvCnt];
        for(int i = 0; i < pdvCnt; ++i){
            PDViSensCnt[i] = cm.get(conf, String.format("SensCnt_PDV%02d", i+1 ), MAX_SENS_CNT);
            PDViSensCnt[i] = PDViSensCnt[i] > MAX_SENS_CNT ? MAX_SENS_CNT : PDViSensCnt[i];
        }

        tagState    = tagtable.createInt( "State",    0, Flags.STATUS);
        tagWorkCnt  = tagtable.createInt( "WorkCnt",  0, Flags.STATUS);
        tagCurPdv   = tagtable.createInt( "CurPdv",   0, Flags.STATUS);
        tagSearch    = tagtable.createRWInt("Search", 0, Flags.STATUS);

        tagT = new Tag[MAX_PDV_CNT][MAX_SENS_CNT];
        pdvTMax = new Tag[MAX_PDV_CNT];
        for(int i = 0; i < pdvCnt; ++i) {
            pdvTMax [i] = tagtable.createInt(String.format("PDV%02d.TMax", (i + 1)), 0, Flags.STATUS);
            for (int j = 0; j < PDViSensCnt[i]; ++j)
                tagT[i][j] = tagtable.createInt(String.format("PDV%02d.T%02d", (i + 1), (j + 1)), 0, Flags.STATUS);
        }

        return true;
    }


    @Override
    public boolean executePeripherialModule() {
        if(emulated) {
            tagSearch.acceptWriteValue();
            return true;
        }

        boolean result = true;

        try {

            if(tagSearch.hasWriteValue()) {
                int value = tagSearch.getWriteValInt();
                result = protocol.requestCmd6(1, value);
            }

            if( result ) {
                if(result = protocol.requestCmd3(0x0, 5)) {
                    tagState.setInt(protocol.getAnswerWord(0));
                    tagWorkCnt.setInt(protocol.getAnswerWord(2));
                    tagCurPdv.setInt(protocol.getAnswerWord(4));
                }

                if(tagState.getInt() == 2){
                for(int i = 0; i < pdvCnt; ++i){
                    short tmax = 0;
                    short val = 0;
                    if(result = protocol.requestCmd3(PDVi_SENS_ADR_START[i], PDViSensCnt[i])) {
                        for(int j = 0; j < PDViSensCnt[i]; ++j) {
                            val = (short) protocol.getAnswerWord(j);
                            if(j == 0) tmax = val;
                            tagT[i][j].setInt(val);
                            if((tmax <  val && val != 32767)  || (tmax == 32767 && val != 32767) )tmax = val;
                        }
                    }
                    pdvTMax [i].setInt ( tmax);
                }
                }


            }


        } catch (Exception e) {
            env.printError(logger, e, name);
            result = false;
        }

        return result;
    }




}
