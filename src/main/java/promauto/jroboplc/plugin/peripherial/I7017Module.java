package promauto.jroboplc.plugin.peripherial;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.Flags;
import promauto.jroboplc.core.api.Plugin;
import promauto.jroboplc.core.api.Tag;
import promauto.utils.Numbers;

import java.util.List;

public class I7017Module extends PeripherialModule {
	private final Logger logger = LoggerFactory.getLogger(I7017Module.class);

	protected ProtocolIcpcon protocol = new ProtocolIcpcon(this);

	private static final int INP_SIZE = 8;
	private static final int ANSWER_SIZE = INP_SIZE * 4 + 4;

	protected static class Input {
		public Tag value;
		public Tag scaled;
		public Tag v1;
		public Tag s1;
		public Tag v2;
		public Tag s2;
	}

	protected Tag tagTypeCode;

	protected Input[] inputs = new Input[INP_SIZE];
	
	protected boolean nominus = false; 
	protected boolean noscale = false; 
	
	private int[] buffout = new int[16]; //3];
	private int[] buffin = new int[48]; //36];

	
/*
	#0184
>0025002500250025002500240025002373
	$0280EE
	!02+04.000D0
	$0281EF
	!02+04.000D0
	$0282F0
	!02+04.000D0
	$0283F1
	!02+04.000D0
*/
	
	
	public I7017Module(Plugin plugin, String name) {
		super(plugin, name);
	}

	
	@Override
	public boolean loadPeripherialModule(Object conf) {
		Configuration cm = env.getConfiguration();
		
		nominus = cm.get(conf, "nominus", false);
		noscale = cm.get(conf, "noscale", false);
		
		initTags();

		return true;
	}


	protected void initTags() {
//		String flagAutosave = getFlagOverload(Flags.AUTOSAVE);
		
		for (int i = 0; i < INP_SIZE; i++)
		{
			String inp = "inp" + i + ".";
			inputs[i] = new Input();
			inputs[i].value = tagtable.createInt( inp + "value", 0, Flags.STATUS);
			if( !noscale ) {
				inputs[i].scaled = tagtable.createDouble( inp + "scaled", 0.0, Flags.STATUS);
				inputs[i].v1 = tagtable.createInt(    inp + "v1", 0, Flags.AUTOSAVE);
				inputs[i].s1 = tagtable.createDouble( inp + "s1", 0, Flags.AUTOSAVE);
				inputs[i].v2 = tagtable.createInt(    inp + "v2", 0, Flags.AUTOSAVE);
				inputs[i].s2 = tagtable.createDouble( inp + "s2", 0, Flags.AUTOSAVE);
			}
		}

		tagTypeCode = tagtable.createInt("TypeCode", 0, Flags.STATUS);
	}


	@Override
	protected void initChannelMap(List<String> chtags) {
		for( int i=0; i<INP_SIZE; ++i ) {
			addChannelMapTag(chtags, inputs[i].value, "" + i);
		}
	}


	@Override
	public boolean executePeripherialModule() {
		
		if( emulated ) 
			return true;
			
		
		boolean result = false;
		
		try {
			
//			port.writeString("#0184\r");
//			System.out.println( port.readBytesDelim(buffin, 13) + ": " + Arrays.toString(buffin) );
//			
//			port.writeString("#0184\r");
//			System.out.println("a: " +  port.readString( ANSWER_SIZE));
//			
//			port.writeString("#0184\r");
//			System.out.println("b: " + port.readStringDelim(13));
			
//			#0184
//			>0025002500250025002500240025002373

			if( firstPass  ||  tagError.getBool() ) {
				buffout[0] = '$';
				buffout[3] = '2';
				int n = protocol.request(buffout, 4, buffin, 12);
				if( n == 12 ) {
					tagTypeCode.setInt( (short)Numbers.asciiByteToInt(buffin, 3) );
				}
			}


			buffout[0] = '#';
//			Numbers.intToHexByte(netaddr, buffout, 1);
			int n = protocol.request(buffout, 3, buffin, ANSWER_SIZE);
			
			if( n == ANSWER_SIZE )
			{
				int idx=1;
				for(int i=0; i<INP_SIZE; ++i) {
	
					short value = (short)Numbers.asciiWordToInt(buffin, idx);
					
					if( !noscale ) {
						double v1 = inputs[i].v1.getInt();
						double s1 = inputs[i].s1.getDouble();
						double vsub = (double)(inputs[i].v2.getInt()) - v1;
						double ssub = inputs[i].s2.getDouble() - s1;
						if( (inputs[i].v2.getInt() - inputs[i].v1.getInt()) > 0) {
							double scaled = (value - v1) * ssub / vsub + s1; 
							inputs[i].scaled.setDouble(scaled);
						} else {
							inputs[i].scaled.setDouble(0.0d);
						}
					}
	
					if( nominus  &&  value < 0 )
						value = 0;
	
					inputs[i].value.setInt(value);
						
					idx += 4;
				}
				
				result = true;
			}


		} catch (Exception e) {
			env.printError(logger, e, name);
		}
		
		return result;
	}

	
	@Override
	protected boolean reload() {
		I7017Module tmp = new I7017Module(plugin, name);
		if( !tmp.load() )
			return false;

		copySettingsFrom(tmp);
		
		for(int i=0; i<INP_SIZE; ++i) {
			if( noscale  &&  !tmp.noscale ) {
				tagtable.add( inputs[i].scaled = tmp.inputs[i].scaled ); 
				tagtable.add( inputs[i].v1     = tmp.inputs[i].v1     );
				tagtable.add( inputs[i].s1     = tmp.inputs[i].s1     );
				tagtable.add( inputs[i].v2     = tmp.inputs[i].v2     );
				tagtable.add( inputs[i].s2     = tmp.inputs[i].s2     );
			} else
			
			if( !noscale  &&  tmp.noscale ) {
				tagtable.remove(inputs[i].scaled);
				tagtable.remove(inputs[i].v1    );
				tagtable.remove(inputs[i].s1    );
				tagtable.remove(inputs[i].v2    );
				tagtable.remove(inputs[i].s2    );
				
				inputs[i].scaled = null;
				inputs[i].v1     = null;
				inputs[i].s1     = null;
				inputs[i].v2     = null;
				inputs[i].s2     = null;
			}
		}
		
		nominus = tmp.nominus; 
		noscale = tmp.noscale; 
		
		return true;
	}

	

}


















