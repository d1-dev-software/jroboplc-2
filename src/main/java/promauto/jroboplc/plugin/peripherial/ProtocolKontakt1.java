package promauto.jroboplc.plugin.peripherial;

import promauto.utils.CRC;

public class ProtocolKontakt1 {

    private int TMPR_CNT_MAX = 30;
    private int buffinSize = calcTmprPackageLen(TMPR_CNT_MAX);
    private int buffoutSize = 6;

    protected int[] buffin = new int[buffinSize];
    protected int[] buffout = new int[buffoutSize];

    private PeripherialModule module;

    public ProtocolKontakt1(PeripherialModule module) {
        this.module = module;
    }

    public void setupBuffIn(int tmprCnt) {
        int n = calcTmprPackageLen(tmprCnt);
        if( n > buffinSize) {
            buffinSize = n;
            buffin = new int[buffinSize];
        }
    }

    public int calcTmprPackageLen(int tmprCnt) {
        return 5 + 2 * tmprCnt;
    }


    public boolean request(int sizeout, int sizein) throws Exception {

        if (sizeout > buffoutSize || sizein > buffinSize)
            return false;

        buffout[0] = module.netaddr;

        int crc = CRC.getCrc16(buffout, sizeout - 2);
        buffout[sizeout - 2] = crc & 0xFF;
        buffout[sizeout - 1] = (crc >> 8) & 0xFF;

        for (int trynum = 0; trynum < module.retrial; trynum++) {

            module.port.discard();
            module.delayBeforeWrite();
            module.port.writeBytes(buffout, sizeout);

            int n = module.port.readBytes(buffin, sizein);
            boolean badcrc = false;

            if (n == sizein) {
                int crc1 = CRC.getCrc16(buffin, sizein - 2);
                int crc2 = (buffin[sizein - 1] << 8) | buffin[sizein - 2];
                if (crc1 == crc2)
                    return true;
                badcrc = true;
            }

            if (module.canLogError()) {
                int cntRead = Math.abs(n);
                module.logError(trynum, badcrc, buffout, sizeout, buffin, n, "");
            }

            module.delayAfterError();
        }
        module.tagErrorCnt.setInt(module.tagErrorCnt.getInt() + 1);

        return false;
    }


}
