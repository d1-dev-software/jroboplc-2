package promauto.jroboplc.plugin.peripherial;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.Flags;
import promauto.jroboplc.core.api.Plugin;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.TagRW;

import java.util.List;


public class PaPdatModule extends PeripherialModule {
	private final Logger logger = LoggerFactory.getLogger(PaPdatModule.class);

	protected ProtocolAA55 protocol = new ProtocolAA55(this);

	private static final int INP_SIZE = 64;	
	private static final int PACKS_AMOUNT = 4;	
	private static final int PACK_SIZE = 16;	
	private static final int RPT = 3;
	private static final float RPTA = 1000000F * (RPT + 1F);
	private static final int DEL = 141;
	private static final int SUB = 4000;	



	
	protected Tag[] inps = new Tag[INP_SIZE];
	protected Tag[] frqs = new Tag[INP_SIZE];
	private int[] buffin = new int[17];
	private int[] buffout = new int[2];
	private int[] tmp = new int[INP_SIZE];

	private TagRW tagFirmware;
	private Tag tagResetCnt;
	private int resetCntPeriod;

	public PaPdatModule(Plugin plugin, String name) {
		super(plugin, name);
	}




	@Override
	public boolean loadPeripherialModule(Object conf) {
		Configuration cm = env.getConfiguration();
		resetCntPeriod = cm.get(conf, "resetCntPeriod_s", 0) * 1000;


		for (int i = 0; i < INP_SIZE; i++) {
			inps[i] = tagtable.createInt(((i < 10) ? "inp0" : "inp") + i, 0, Flags.STATUS);
			frqs[i] = tagtable.createInt(((i < 10) ? "frq0" : "frq") + i, 0, Flags.STATUS);
		}

		tagFirmware = tagtable.createRWString("firmware", "");
		tagResetCnt = tagtable.createInt("resetcnt", 0);

		return true;
	}


	@Override
	protected void initChannelMap(List<String> chtags) {
		for( int i=0; i<INP_SIZE; ++i ) {
			addChannelMapTag(chtags, inps[i], "" + i);
			addChannelMapTag(chtags, frqs[i], i + "a");
		}
	}


	@Override
	public boolean executePeripherialModule() {

		if(emulated) 
			return true;

		boolean result = true;
		try {
			buffout[0] = 0x55;
			
			// request logic data
			buffout[1] = 0xA0 + netaddr;
			result = protocol.request(buffout, 2, buffin, 9, ProtocolAA55.CrcAA55);
			if( result ) {
				int mask = 1;
				for (int i = 0; i < INP_SIZE; i++, mask <<= 1) {
					if( mask > 0x80)
						mask = 1;
					tmp[i] = (buffin[i / 8] & mask) > 0? 1: 0;
				}
				
				for( int i = 0; i < INP_SIZE; i++) {
					inps[i].setInt( tmp[ ProtocolAA55.pRksDatConv[i] ] );
				}
			}


			// request frequency data
			if( result ) {
				int cmd = 0x20;
				buffout[1] = cmd + netaddr;
				int idx = 0;
				
				for (int pack = 0; pack < PACKS_AMOUNT; ++pack) {
					
					if( protocol.request(buffout, 2, buffin, 17, ProtocolAA55.CrcAA55) ) {
						for (int i = 0; i < PACK_SIZE; ++i, ++idx) {
							tmp[idx] = Math.round( RPTA/(float)(buffin[i] * DEL + SUB));
						}
					} else {
						result = false;
						break;
					}

					cmd += 0x20;
					buffout[1] = cmd + netaddr;
				}
				
				if( result ) {
					for( int i = 0; i < INP_SIZE; i++) {
						frqs[i].setInt( tmp[ ProtocolAA55.pRksDatConv[i] ] );
					}
				}
				
			}

			if( result  &&  (firstPass  || tagError.getBool())) {
				result = protocol.requestFirmware( tagFirmware );
			}

			result = result & protocol.requestResetCnt( tagResetCnt, resetCntPeriod);

		} catch (Exception e) {	
			env.printError(logger, e, name);
			result = false;
		}
		
		
		return result;
	}

	

}
