package promauto.jroboplc.plugin.peripherial;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.Flags;
import promauto.jroboplc.core.api.Plugin;
import promauto.jroboplc.core.api.Tag;

import java.util.List;


public class OwenTrm138Module extends PeripherialModule {

	private static final int SIZE = 8;
	private final Logger logger = LoggerFactory.getLogger(OwenTrm138Module.class);

	protected ProtocolModbus protocol = new ProtocolModbus(this);

	public static class Sensor {
		protected Tag tagValue;
		protected int mul;
	}

	protected Sensor[] sensors = new Sensor[SIZE];


	public OwenTrm138Module(Plugin plugin, String name) {
		super(plugin, name);
	}


	@Override
	public boolean loadPeripherialModule(Object conf) {
		Configuration cm = env.getConfiguration();

		int mul = cm.get(conf, "mul", 100);

		for(int i=0; i<SIZE; ++i) {
			sensors[i] = new Sensor();
			sensors[i].mul = cm.get(conf, "mul." + i, mul);
			sensors[i].tagValue = tagtable.createInt("d" + i + ".value", 0, Flags.STATUS);
		}

		return true;
	}


	@Override
	protected void initChannelMap(List<String> chtags) {
		for( int i=0; i<SIZE; ++i ) {
			addChannelMapTag(chtags, sensors[i].tagValue, ""+i);
		}
	}



	@Override
	public boolean executePeripherialModule() {
		
		if(emulated) 
			return true;
		
		boolean result = true;
		
		try {
			if( result = protocol.requestCmd4(0, SIZE * 5) ) {
				for(int i=0; i<SIZE; ++i)
					sensors[i].tagValue.setDouble(
							protocol.getAnswerFloat(i * 5 + 3) * (double)sensors[i].mul );
			}
		} catch (Exception e) {
			env.printError(logger, e, name);
			result = false;
		}
	
		return result;
	}


	@Override
	protected boolean reload() {
		OwenTrm138Module tmp = new OwenTrm138Module(plugin, name);
		if( !tmp.load() )
			return false;

		copySettingsFrom(tmp);

		for(int i=0; i<SIZE; ++i)
			sensors[i].mul = tmp.sensors[i].mul;

		return true;
	}


}
