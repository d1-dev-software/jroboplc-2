package promauto.jroboplc.plugin.peripherial;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.api.Flags;
import promauto.jroboplc.core.api.Plugin;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.TagRW;

/**
 *    Attention! Unverified source code, needs to be refactored
 *
 *    Внимание! Непроверенный исходный код, необходимо провести рефакторинг
 */
public class LsisSvIp5aModule extends PeripherialModule {

    private final Logger logger = LoggerFactory.getLogger(LsisSvIp5aModule.class);

    private static int C_EOC  = 0x04;
    private static int C_ENQ  = 0x05;
    private static int C_ACK  = 0x06;
    private static int C_NACK = 0x15;

    private static int CMD_READ  = 'R';
    private static int CMD_WRITE = 'W';


    private static String S_addrFreqRefer  = "0005";
    private static String S_addrRunCmd     = "0006";
    private static String S_addrAccelTime  = "0007";
    private static String S_addrDecelTime  = "0008";

    private static String S_addrOutCurrent = "0009";
    private static String S_addrOutFreq    = "000A";
    private static String S_addrOutVoltage = "000B";
    private static String S_addrStatus     = "000E";
    private static String S_addrTripInfo   = "000F"; // 16 bits



    protected TagRW tagReg0005;  //freq reference
    protected TagRW tagReg0006;  //run command
    protected TagRW tagReg0007;  //accel time
    protected TagRW tagReg0008;  //deccel time


    protected Tag tagReg0009;  // output current
    protected Tag tagReg000A;  // output freq
    protected Tag tagReg000B;  // output voltage
    protected Tag tagReg000C;  // dc link voltage
    protected Tag tagReg000D;  //
    protected Tag tagReg000E;
    protected Tag tagReg000F;


    private static int BUFF_SIZE = 64;

    protected int[] buffin = new int[BUFF_SIZE];
    protected int[] buffout = new int[BUFF_SIZE];




    public LsisSvIp5aModule(Plugin plugin, String name) {
        super(plugin, name);
    }

    @Override
    public boolean loadPeripherialModule(Object conf) {

        tagReg0005 = tagtable.createRWInt("Reg.R005", 0, Flags.STATUS);
        tagReg0006 = tagtable.createRWInt("Reg.R006", 0, Flags.STATUS);
        tagReg0007 = tagtable.createRWInt("Reg.R007", 0, Flags.STATUS);
        tagReg0008 = tagtable.createRWInt("Reg.R008", 0, Flags.STATUS);

        tagReg0009 = tagtable.createInt("Reg.R009", 0, Flags.STATUS);
        tagReg000A = tagtable.createInt("Reg.R00A", 0, Flags.STATUS);
        tagReg000B = tagtable.createInt("Reg.R00B", 0, Flags.STATUS);
        tagReg000C = tagtable.createInt("Reg.R00C", 0, Flags.STATUS);
        tagReg000D = tagtable.createInt("Reg.R00D", 0, Flags.STATUS);
        tagReg000E = tagtable.createInt("Reg.R00E", 0, Flags.STATUS);
        tagReg000F = tagtable.createInt("Reg.R00F", 0, Flags.STATUS);

        return true;
    }

    @Override
    public boolean executePeripherialModule() {
        if(emulated) {
            tagReg0005.acceptWriteValue();
            tagReg0006.acceptWriteValue();
            tagReg0007.acceptWriteValue();
            tagReg0008.acceptWriteValue();
            return true;
        }

        boolean result = true;


        try {
            int value = 0;
            if(tagReg0005.hasWriteValue()) {
                value = tagReg0005.getWriteValInt();
                tagReg0005.setReadValInt(value);
                result = request_write(S_addrFreqRefer, value);
            };
            if(tagReg0006.hasWriteValue()) {
                value = tagReg0006.getWriteValInt();
                tagReg0006.setReadValInt(value);
                result = request_write(S_addrRunCmd, value);
            };
            if(tagReg0007.hasWriteValue()) {
                value = tagReg0007.getWriteValInt();
                tagReg0007.setReadValInt(value);
                result = request_write(S_addrAccelTime, value);
            };
            if(tagReg0008.hasWriteValue()) {
                value = tagReg0008.getWriteValInt();
                tagReg0008.setReadValInt(value);
                result = request_write(S_addrDecelTime, value);
            };
            if(!result) return result;

            //for example reading
            //from 0x0007 to 0x000C - 6 registers
            if(result = request_read(S_addrAccelTime,6)){
//                Numbers
                for(int i = 0; i < 4; ++i){
                    String str = "";
                    str+=Character.toString((char)buffin[4 + i * 4]);
                    str+=Character.toString((char)buffin[5 + i * 4]);
                    str+=Character.toString((char)buffin[6 + i * 4]);
                    str+=Character.toString((char)buffin[7 + i * 4]);
                    int ival = Integer.parseInt(str, 16);

                    switch (i) {
                        case 0:   tagReg0007.setReadValInt(ival);
                        break;
                        case 1:   tagReg0008.setReadValInt(ival);
                        break;
                        case 2:   tagReg0009.setInt(ival);
                        break;
                        case 3:   tagReg000A.setInt(ival);
                        break;
                        case 4:   tagReg000B.setInt(ival);
                        break;
                        case 5:   tagReg000C.setInt(ival);
                        break;
                    }
                }
                if(result = request_read(S_addrStatus,2)) {
                    for(int i = 0; i < 2; ++i){
                        String str = "";
                        str+=Character.toString((char)buffin[4 + i * 4]);
                        str+=Character.toString((char)buffin[5 + i * 4]);
                        str+=Character.toString((char)buffin[6 + i * 4]);
                        str+=Character.toString((char)buffin[7 + i * 4]);
                        int ival = Integer.parseInt(str, 16);
                        switch (i) {
                            case 0:   tagReg000E.setInt(ival);
                                break;
                            case 1:   tagReg000F.setInt(ival);
                                break;
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }



    private boolean request_read(String strAddr, int  iBytes) throws Exception {
        buffout[3] = CMD_READ;
        buffout[8] = String.format("%01x", iBytes).charAt(0);
        int iAns = 7 + iBytes*4;
        return request(strAddr, iAns,  iBytes, 9);
    }

    private boolean request_write(String strAddr,  int iValue) throws Exception {
        buffout[3] = CMD_WRITE;
        buffout[8] = '1';
        String strVal =  String.format("%04x", iValue);
        for(int i = 0; i < 4; ++i){
            buffout[9 + i] = strVal.charAt(i);
        }
        return request(strAddr, 11,  1, 13);
        //write 12 + n * 4
        //ack 7 + n * 4
        //nack - 9 bytes
    }


    private boolean request(String strRegAddr, int sizeAnsw, int bytesToProcs, int iSumPos) throws Exception {
        if( port == null )
            return false;

        int crc=0;
        String sAddr = String.format("%02x", netaddr).toUpperCase();

        buffout[0] = C_ENQ;
        buffout[1] = sAddr.charAt(0);
        buffout[2] = sAddr.charAt(1);
        for(int i = 0; i < 4; ++i)
            buffout[i + 4] = strRegAddr.charAt(i);

        crc = calcCrc(buffout,1 ,iSumPos-1);
        String sCrc = String.format("%02x", crc).toUpperCase();


        buffout[iSumPos]   = sCrc.charAt(0);
        buffout[iSumPos+1]  = sCrc.charAt(1);
        buffout[iSumPos+2]  = C_EOC;

        port.discard();
        port.writeBytes(buffout, iSumPos+3);



        int n = 0;

        while (n < sizeAnsw){
            buffin[n] = port.readByte();
            if(n == 0 && (buffin[n] == C_NACK || buffin[n] != C_ACK) ){
                tagErrorCnt.setInt(tagErrorCnt.getInt() + 1);
                return false;
            }
            ++n;
        }


        crc  = calcCrc(buffin,1 ,sizeAnsw - 4);
        sCrc = String.format("%02x", crc).toUpperCase();


        String sCrcReceived = Character.toString((char)buffin[sizeAnsw-3]);
        sCrcReceived += Character.toString((char)buffin[sizeAnsw-2]);


        if(sCrc.compareTo(sCrcReceived) != 0) {
            tagErrorCnt.setInt(tagErrorCnt.getInt() + 1);
            return false;
        }
        return true;
    }



    private int calcCrc(int[] bytes, int begin, int end) {
        int crc = 0;

        for (int index = begin; index <= end; index++) {
            int data = bytes[index];
            crc = crc+data;
        }
        return (crc & 0xFF);
    }


}
