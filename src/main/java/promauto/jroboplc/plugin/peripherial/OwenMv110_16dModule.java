package promauto.jroboplc.plugin.peripherial;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.Flags;
import promauto.jroboplc.core.api.Plugin;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.TagRW;

import java.util.List;


public class OwenMv110_16dModule extends PeripherialModule {
    private final Logger logger = LoggerFactory.getLogger(OwenMv110_16dModule.class);

    protected ProtocolModbus protocol = new ProtocolModbus(this);


    protected TagRW 	 tagTOut;
    protected TagRW[]    tagInp = null;
    protected TagRW[]    tagCnt = null;

    private boolean usecnt;

    protected final int size;
    protected final int regInp;
    protected final int regCnt;
    protected final int regTOut;


    public OwenMv110_16dModule(Plugin plugin, String name) {
        super(plugin, name);

        size = 16;
        regInp = 51;
        regCnt = 64;
        regTOut = 48;
    }


    @Override
    public boolean loadPeripherialModule(Object conf) {
        Configuration cm = env.getConfiguration();
        usecnt = !cm.get(conf, "nocnt", false);

        tagTOut	= protocol.addWriteTag( 48, tagtable.createRWInt("TOut", 0, Flags.STATUS));

        if( tagInp == null )
            tagInp = new TagRW[size];

        if( tagCnt == null  &&  usecnt )
            tagCnt = new TagRW[size];

        for(int i = 0; i< size; ++i) {
            tagInp[i] = tagtable.createRWInt(String.format("inp%02d", i), 0, Flags.STATUS);
            if( usecnt )
                tagCnt[i] = protocol.addWriteTag( 64+i,
                        tagtable.createRWInt(String.format("cnt%02d", i), 0, Flags.STATUS));
        }

        return true;
    }


    @Override
    protected void initChannelMap(List<String> chtags) {
        for( int i=0; i<size; ++i ) {
            addChannelMapTag(chtags, tagInp[i], ""+i);
            addChannelMapTag(chtags, tagCnt[i], "cnt"+i);
        }
    }


    @Override
    public boolean executePeripherialModule() {

        if(emulated) {
            tagTOut.acceptWriteValue();
            for(int i = 0; i< size; ++i) {
                tagInp[i].acceptWriteValue();
                if( usecnt )
                    tagCnt[i].acceptWriteValue();
            }
            return true;
        }

        boolean result = true;

        try {
            if( result )
                result = protocol.sendWriteTags(16);

            result &= readInputs();

            if( usecnt  &&  result  &&  (result = protocol.requestCmd3(regCnt, size)) ) {
                for (int i = 0; i < size; ++i)
                    tagCnt[i].setReadValInt( protocol.getAnswerWord(i) );
            }

            if( result  &&  (firstPass  ||  tagError.getBool()) ) {
                if( result = protocol.requestCmd3(regTOut, 1) )
                    tagTOut.setReadValInt( protocol.getAnswerWord(0) );
            }

        } catch (Exception e) {
            env.printError(logger, e, name);
            result = false;
        }

        return result;
    }


    protected boolean readInputs() throws Exception {
        if( !protocol.requestCmd3(regInp, 1) )
            return false;

        int inps = protocol.getAnswerWord(0);
        for (int i = 0; i < size; ++i)
            tagInp[i].setReadValInt((inps >> i) & 1);
        return true;
    }


    @Override
    protected boolean reload() {
        OwenMv110_16dModule tmp = new OwenMv110_16dModule(plugin, name);
        if( !tmp.load() )
            return false;

        copySettingsFrom(tmp);

        if( usecnt  &&  !tmp.usecnt ) {
            for(Tag tag: tagCnt)
                tagtable.remove(tag);
            tagCnt = null;
        }

        if( !usecnt  &&  tmp.usecnt ) {
            tagCnt = tmp.tagCnt;
            for(Tag tag: tmp.tagCnt)
                tagtable.add(tag);
        }

        usecnt = tmp.usecnt;

        return true;
    }




}
