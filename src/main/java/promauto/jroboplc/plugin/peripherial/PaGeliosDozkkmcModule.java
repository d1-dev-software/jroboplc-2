package promauto.jroboplc.plugin.peripherial;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.Flags;
import promauto.jroboplc.core.api.Plugin;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.TagRW;
import promauto.utils.CRC;
import promauto.utils.Numbers;

import java.util.Arrays;
import java.util.List;

import static java.lang.Math.max;
import static java.lang.Math.min;
import static promauto.utils.Numbers.limit;

public class PaGeliosDozkkmcModule extends PeripherialModule {

    private static final long SUM_WEIGHT_MAX = 3_999_999_999L; //0xFFFF_FFFFL;

    // standard state codes (bit mask)
    private final static int STATE_IDLE             = 1;
    private final static int STATE_WAITING_FOR_TASK = 2;
    private final static int STATE_LOADING          = 4;
    private final static int STATE_UNLOADING        = 8;
    private final static int STATE_ALARM            = 16;

    // original device state codes
    private final static int STATUS_IDLE            = 0;
    private final static int STATUS_LOAD_READY      = 1;
    private final static int STATUS_LOAD            = 2;
    private final static int STATUS_UNLOAD_READY    = 3;
    private final static int STATUS_TASK_READY      = 4;
    private final static int STATUS_ALARM           = 5;
    private final static int STATUS_UNLOAD_SUSP     = 6;
    private final static int STATUS_UNLOAD          = 7;

    // original device CMD codes
    private final static int CMD_START              = 0;
    private final static int CMD_TASK               = 6;
    private final static int CMD_LOAD               = 2;
    private final static int CMD_UNLOAD             = 4;
    private final static int CMD_SUSPEND            = 8;


    private final Logger logger = LoggerFactory.getLogger(PaGeliosDozkkmcModule.class);

    protected ProtocolAA55 protocol = new ProtocolAA55(this);

    // standard tags (all multicomponental dosers must have them)
    private static class StorRec {
        protected Tag tagSumWeight;
        protected Tag tagReqWeight;
        protected Tag tagSetWeight;
        protected Tag tagActive;
    }
    protected StorRec[] recs;
    protected Tag   tagSumWeightMax;
    protected Tag   tagCurStor;
    protected Tag   tagCurWeight;
    protected Tag   tagCurPercent;
    protected Tag   tagCurPercentTotal;
    protected Tag   tagState;
    protected Tag   tagCmdStartCycle;
    protected Tag   tagCmdStartTask;
    protected Tag   tagCmdStartLoad;
    protected Tag   tagCmdStartUnload;
    protected Tag   tagCmdSuspUnload;
    protected TagRW tagSendTask;
    // SendTask:
    // write 0 - nope
    // write 1..0x7fff_ffff - send
    // if read positive written value, then accepted
    // if read negative written value, then failed


    // original device specific tags
    protected TagRW tagStorQnt;
    protected Tag   tagStorQntActual;
    protected Tag   tagTaskMode;
    protected Tag   tagStage;
    protected Tag   tagStageTime;
    protected Tag   tagInOut;
    protected Tag   tagStatus;
    protected Tag   tagErrorCode;
    protected TagRW tagCmd;

    private TagRW tagFirmware;



    private int[] buffout;

    private int version;
    private int storqnt;

    private int lastSendTaskEmuVal = 0;



    public PaGeliosDozkkmcModule(Plugin plugin, String name) {
        super(plugin, name);
    }


    @Override
    public boolean loadPeripherialModule(Object conf) {
        Configuration cm = env.getConfiguration();

        version = cm.get(conf, "version", 2);
        storqnt = cm.get(conf, "storqnt", 1);
        if( storqnt < 1)
            storqnt = 1;

        buffout = new int[8 + 5 * storqnt];

        tagStorQnt = tagtable.createRWInt("StorQnt", storqnt);
        tagStorQntActual = tagtable.createInt("StorQntActual", 0, Flags.STATUS);

        recs = new StorRec[storqnt];

        for(int i = 0; i< storqnt; ++i) {
            recs[i] = new StorRec();
            recs[i].tagSumWeight   = tagtable.createLong("SumWeight"   + (i+1), 0, Flags.STATUS);
            recs[i].tagReqWeight   = tagtable.createLong("ReqWeight"   + (i+1), 0, Flags.STATUS);
            recs[i].tagSetWeight   = tagtable.createLong("SetWeight"   + (i+1), 0, Flags.STATUS);
            recs[i].tagActive      = tagtable.createBool("Active"      + (i+1), false, Flags.STATUS);
        }

        tagSumWeightMax     = tagtable.createRWLong("SumWeightMax" , cm.get(conf, "SumWeightMax", SUM_WEIGHT_MAX));
        tagCurStor          = tagtable.createInt( "CurStor"        , 0, Flags.STATUS);
        tagCurWeight        = tagtable.createLong("CurWeight"      , 0, Flags.STATUS);
        tagCurPercent       = tagtable.createInt( "CurPercent"     , 0, Flags.STATUS);
        tagCurPercentTotal  = tagtable.createInt( "CurPercentTotal", 0, Flags.STATUS);
        tagTaskMode         = tagtable.createInt( "TaskMode"       , 0, Flags.STATUS);
        tagStage            = tagtable.createInt( "Stage"          , 0, Flags.STATUS);
        tagStageTime        = tagtable.createInt( "StageTime"      , 0, Flags.STATUS);
        tagInOut            = tagtable.createLong("InOut"          , 0, Flags.STATUS);
        tagStatus           = tagtable.createInt( "Status"         , 0, Flags.STATUS);
        tagState            = tagtable.createInt( "State"          , 0, Flags.STATUS);
        tagErrorCode        = tagtable.createInt( "ErrorCode"      , 0, Flags.STATUS);

        tagCmdStartCycle    = tagtable.createInt("CmdStartCycle"  , 0);
        tagCmdStartTask     = tagtable.createInt("CmdStartTask"   , 0);
        tagCmdStartLoad     = tagtable.createInt("CmdStartLoad"   , 0);
        tagCmdStartUnload   = tagtable.createInt("CmdStartUnload" , 0);
        tagCmdSuspUnload    = tagtable.createInt("CmdSuspUnload"  , 0);

        tagCmd              = tagtable.createRWInt("Cmd"          , 0);
        tagSendTask         = tagtable.createRWInt("SendTask"     , 0);

        tagFirmware         = tagtable.createRWString("firmware", "");

        // --- crc32 tags ---
        initCrc32Tags();
        for(int i = 0; i< storqnt; ++i)
            crc32Tags.add( recs[i].tagSumWeight );
        crc32Tags.add( tagError );

        return true;
    }


    @Override
    protected void initChannelMap(List<String> chtags) {
        for(int i = 0; i< storqnt; ++i) {
            addChannelMapTag(chtags, recs[i].tagActive, "" + (i+1) );
        }
    }


    @Override
    public boolean executePeripherialModule() {

        boolean result = true;

        if( emulated ) {
            tagCmd.acceptWriteValue();
            if( tagSendTask.hasWriteValue() ) {
                if( tagSendTask.getWriteValInt() == 0 )
                    tagSendTask.setReadValInt(0);
                else {
                    tagSendTask.setReadValInt(lastSendTaskEmuVal);
                    lastSendTaskEmuVal = tagSendTask.getWriteValInt();
                    if (tagSendTask.getInt() == 0)
                        tagSendTask.setReadValInt(lastSendTaskEmuVal);
                }
            }
            calcPercent();

            updateActives();

            return true;
        }

        try {
            buffout[0] = 0x55;

            // todo: do refactor "result = ..."

            // read status
            if (result) {
                buffout[1] = 0x20 + netaddr;
                buffout[2] = 2;
                int crc = CRC.getCrc16(buffout, 1, 2);
                Numbers.wordToBytes(crc, buffout, 3);

                result = protocol.request(buffout, 5);
                if (result && (result = protocol.bufinp[0] == netaddr)) {
                    int status = 0;
                    if( version == 1) {
                        tagTaskMode.setInt(Numbers.bytesToWord(protocol.bufinp, 2));  //# 2
                        tagCurWeight.setLong(Numbers.bytesToInt(protocol.bufinp, 4));  //# 4
                        tagStage.setInt(protocol.bufinp[8]);  //# 1
                        tagStageTime.setInt(Numbers.bytesToWord(protocol.bufinp, 9));  //# 2
                        tagInOut.setInt(Numbers.bytesToWord(protocol.bufinp, 11));  //# 2
                        status = protocol.bufinp[13]; //# 1
                        tagStatus.setInt(status);
                        tagErrorCode.setInt(protocol.bufinp[14]);  //# 1

                        if( status==STATUS_IDLE  ||  status==STATUS_ALARM )
                            tagCurStor.setInt(0);
                        else
                            tagCurStor.setInt(1);
                    }

                    if( version == 2) {
                        tagTaskMode.setInt(Numbers.bytesToWord(protocol.bufinp, 2));  //# 2
                        tagCurStor.setInt(protocol.bufinp[4]);  //# 2
                        tagCurWeight.setLong(Numbers.bytesToInt(protocol.bufinp, 6));  //# 4
                        tagStage.setInt(protocol.bufinp[10]);  //# 1
                        tagStageTime.setInt(Numbers.bytesToWord(protocol.bufinp, 11));  //# 2
                        tagInOut.setLong(Numbers.bytesToDWord(protocol.bufinp, 13));  //# 4
                        status = protocol.bufinp[17]; //# 1
                        tagStatus.setInt(status);
                        tagErrorCode.setInt(protocol.bufinp[18]);  //# 1
                    }

                    updateActives();

                    tagState.setInt(
                        (status == STATUS_IDLE? STATE_IDLE: 0) +
                        (status == STATUS_TASK_READY? STATE_WAITING_FOR_TASK : 0) +
                        (status == STATUS_LOAD_READY
                                || status == STATUS_LOAD? STATE_LOADING: 0) +
                        (status == STATUS_UNLOAD_READY
                                || status == STATUS_UNLOAD
                                || status == STATUS_UNLOAD_SUSP? STATE_UNLOADING: 0) +
                        (status == STATUS_ALARM? STATE_ALARM: 0)
                    );
                }
            }


            // read sum counters
            if (result) {
                buffout[1] = 0x40 + netaddr;
                buffout[2] = 3;
                buffout[3] = storqnt;
                int crc = CRC.getCrc16(buffout, 1, 3);
                Numbers.wordToBytes(crc, buffout, 4);

                result = protocol.request(buffout, 6);
                if (result && (result = protocol.bufinp[0] == netaddr)) {
                    int qnt = min(protocol.bufinp[2], storqnt);
                    int k = 3;
                    for (int i = 0; i < qnt; ++i) {
                        recs[i].tagSumWeight.setLong(Numbers.bytesToDWord(protocol.bufinp, k + 1));
                        k += 5;
                    }
                    calcCrc32();
                }
            }

            // read task weight
            if (result) {
                buffout[1] = 0x60 + netaddr;
                buffout[2] = 2;
                int crc = CRC.getCrc16(buffout, 1, 2);
                Numbers.wordToBytes(crc, buffout, 3);

                result = protocol.request(buffout, 5);
                if (result && (result = protocol.bufinp[0] == netaddr)) {
                    tagStorQntActual.setInt(protocol.bufinp[2]);
                    int qnt = min(tagStorQntActual.getInt(), storqnt);
                    int k = 3;
                    for (int i = 0; i < qnt; ++i) {
                        recs[i].tagReqWeight.setLong(Numbers.bytesToDWord(protocol.bufinp, k + 1));
                        k += 5;
                    }
                    calcPercent();
                }
            }

            // set task weight
            if( result  &&  tagSendTask.hasWriteValue() ){

                int sendtask = tagSendTask.getWriteValInt();

                if( sendtask <= 0 ) {
                    tagSendTask.setReadValInt(0);
                } else {

                    buffout[1] = 0xA0 + netaddr;
                    buffout[2] = 5 + 5 * storqnt;
                    buffout[3] = 0;
                    buffout[4] = 3;
                    buffout[5] = storqnt;

                    int k = 6;
                    for (int i = 0; i < storqnt; ++i) {
                        buffout[k] = i+1;
                        Numbers.dwordToBytes(recs[i].tagSetWeight.getLong(), buffout, k + 1);
                        k += 5;
                    }

                    int crc = CRC.getCrc16(buffout, 1, k-1);
                    Numbers.wordToBytes(crc, buffout, k);

                    result = protocol.request(buffout, 8 + 5*storqnt);
                    if (result && (result = protocol.bufinp[0] == netaddr)) {
                        tagSendTask.setReadValInt( protocol.bufinp[2] == 0? sendtask: -sendtask );
                    } else {
                        tagSendTask.raiseWriteValue();
                    }
                }
            }



            if( result) {
                processCmd(tagCmdStartCycle, CMD_START,
                        tagStatus.getInt() == STATUS_IDLE ||
                              tagStatus.getInt() == STATUS_ALARM);

                processCmd(tagCmdStartTask, CMD_TASK,
                        tagStatus.getInt() == STATUS_TASK_READY);

                processCmd(tagCmdStartLoad, CMD_LOAD,
                        tagStatus.getInt() == STATUS_LOAD_READY);

                processCmd(tagCmdStartUnload, CMD_UNLOAD,
                        tagStatus.getInt() == STATUS_UNLOAD_READY);

                processCmd(tagCmdSuspUnload, CMD_SUSPEND,
                        tagStatus.getInt() == STATUS_UNLOAD);
            }

            // cmd
            if (result && tagCmd.hasWriteValue()) {
                buffout[1] = 0xC0 + netaddr;
                buffout[2] = 4;
                int value = tagCmd.getWriteValInt();
                buffout[3] = value;
                buffout[4] = 0;
                int crc = CRC.getCrc16(buffout, 1, 4);
                Numbers.wordToBytes(crc, buffout, 5);

                result = protocol.request(buffout, 7);
                if (result && (result = protocol.bufinp[0] == netaddr)) {
                    if (protocol.bufinp[2] == 0)
                        tagCmd.setReadValInt(value);
                } else
                    tagCmd.raiseWriteValue();
            }


            if( result  &&  (firstPass  || tagError.getBool())) {
                result = protocol.requestFirmware( tagFirmware );
            }

//            tagError.setBool(!result);

        } catch (Exception e) {
            result = false;
            env.printError(logger, e, name);
        }

        return result;
    }


    private void updateActives() {
        for(int i = 0; i< storqnt; ++i) {
            recs[i].tagActive.setBool( i+1 == tagCurStor.getInt() );
        }
    }


    private void processCmd(Tag tag, int value, boolean active) {
        if( tag.getInt() > 0 ) {
            if( active )
                tagCmd.setInt(value);
            else
                tag.setInt(0);
        }
    }

    private void calcPercent() {
        int curStor = tagCurStor.getInt();

        long curWeight = tagCurWeight.getLong();
        long reqWeight = 0;
        long curWeightTotal = curStor == 0? curWeight: 0;
        long reqWeightTotal = 0;

        for (int i = 0; i < recs.length; ++i) {
            long w = recs[i].tagReqWeight.getLong();
            reqWeightTotal += w;
            if( curStor > 0 ) {
                int cmp = i + 1 - curStor;
                if (cmp < 0) {
                    curWeightTotal += w;
                } else if (cmp == 0) {
                    reqWeight = w;
                    curWeightTotal += curWeight;
                }
            }
        }

        int percent = reqWeight > 0? (int)(curWeight * 100 / reqWeight): 0;
        tagCurPercent.setInt(limit(percent, 0, 100));

        percent = reqWeightTotal > 0? (int)(curWeightTotal * 100 / reqWeightTotal): 0;
        tagCurPercentTotal.setInt(limit(percent, 0, 100));


    }


    @Override
	protected boolean reload() {
		PaGeliosDozkkmcModule tmp = new PaGeliosDozkkmcModule(plugin, name);
		if( !tmp.load() )
			return false;

		copySettingsFrom(tmp);

		// size decreased
        if( tmp.storqnt < storqnt ) {
            for(int i=tmp.storqnt; i<storqnt; ++i) {
                tagtable.remove(recs[i].tagSumWeight);
                tagtable.remove(recs[i].tagReqWeight);
                tagtable.remove(recs[i].tagSetWeight);
            }
            recs = Arrays.copyOf(recs, tmp.storqnt);
        } else

        // size increased
        if( tmp.storqnt > storqnt ) {
            recs = Arrays.copyOf(recs, tmp.storqnt);
            for(int i=storqnt; i<tmp.storqnt; ++i) {
                recs[i] = tmp.recs[i];
                tagtable.add(recs[i].tagSumWeight);
                tagtable.add(recs[i].tagReqWeight);
                tagtable.add(recs[i].tagSetWeight);
            }
        }

        version = tmp.version;
        storqnt = tmp.storqnt;
        buffout = tmp.buffout;

        tagStorQnt.setReadValInt(storqnt);

		return true;
	}


    // d1*State;d1*CmdStartCycle;d1*CmdStartTask;d1*ReqWeight*;d1*SumWeight*


}
