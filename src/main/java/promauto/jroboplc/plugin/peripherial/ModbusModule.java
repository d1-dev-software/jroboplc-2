package promauto.jroboplc.plugin.peripherial;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.Flags;
import promauto.jroboplc.core.api.Plugin;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.TagRW;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ModbusModule extends PeripherialModule {
	
	private final Logger logger = LoggerFactory.getLogger(ModbusModule.class);

	protected ProtocolModbus protocol = new ProtocolModbus(this);
	
	protected int maxWriteSizeCoil = 128;
	protected int maxWriteSizeReg = 16;
    
	protected boolean hasReadCoils;
	protected boolean hasReadDscinps;
	protected boolean hasReadHldregs;
	protected boolean hasReadInpregs;

	protected List<ModbusTag> modbustags = new ArrayList<>();
	protected List<ModbusBitTag> bittags = new ArrayList<>();

	private int[] buff = new int[2];

	
	public ModbusModule(Plugin plugin, String name) {
		super(plugin, name);
	}


	@Override
	public boolean loadPeripherialModule(Object conf) {
		modbustags.clear();
		
		Configuration cm = env.getConfiguration();

		String cs = cm.get(conf, "charset", "");
		if( !cs.isEmpty() )
			protocol.charset = Charset.forName(cs);

		maxWriteSizeCoil = cm.get(conf, "maxWriteSizeInp", maxWriteSizeCoil);
		maxWriteSizeReg = cm.get(conf, "maxWriteSizeReg", maxWriteSizeReg);

		if( !loadModbusTags(conf) )
			return false;

		init();
		return true;
	}


	protected boolean loadModbusTags(Object conf) {
		Configuration cm = env.getConfiguration();
		try {
			for(Object conf_tag: cm.toList( cm.get(conf, "tags"))) {
				ModbusTag mtag = new ModbusTag();
				mtag.name = cm.get(conf_tag, "name", mtag.name);
				mtag.type = ModbusTag.Type.valueOf(cm.get(conf_tag, "type", mtag.type.toString()).toUpperCase());
				mtag.inverted = cm.get(conf_tag, "inverted", mtag.inverted);
				mtag.region = ModbusTag.Region.valueOf(cm.get(conf_tag, "region", mtag.region.toString()).toUpperCase());
				mtag.access = ModbusTag.Access.valueOf(cm.get(conf_tag, "access", mtag.access.toString()).toUpperCase());
				mtag.address = cm.get(conf_tag, "address", mtag.address);
				mtag.littleEndian = cm.get(conf_tag, "littleEndian", mtag.littleEndian);
				mtag.readEnd = cm.get(conf_tag, "readEnd", mtag.readEnd);
				mtag.writeSingle = cm.get(conf_tag, "writeSingle", mtag.writeSingle);
				mtag.writeMultiple = cm.get(conf_tag, "writeMultiple", mtag.writeMultiple);
				mtag.enable = cm.get(conf_tag, "enable", mtag.enable);
				mtag.size = cm.get(conf_tag, "size", mtag.size);
				mtag.tracktagName = cm.get(conf_tag, "tracktag", mtag.tracktagName);

				Object confBits = cm.get(conf_tag, "bits");
				if (confBits != null) {
					cm.toMap(confBits).forEach((bitname, mask) ->
							addModbusBitTag(mtag, bitname, Integer.parseInt(mask.toString())));
				}

				addModbusTag(mtag);
			}

		} catch (IllegalArgumentException e) {
			env.printError(logger, e, name);
			return false;
		}
		return true;
	}


	protected ModbusTag addModbusTag(ModbusTag mtag) {
		mtag.init();
		tagtable.add(mtag.tag);
		modbustags.add(mtag);
		return mtag;
	}


	protected TagRW addModbusBitTag(ModbusTag mtag, String bitname, int mask) {
		TagRW tag = null;
		if (mtag.type == ModbusTag.Type.UINT16) {
			bittags.add(new ModbusBitTag(
					mtag.name,
					mask,
					tag = tagtable.createRWInt(mtag.name + '.' + bitname, 0, Flags.STATUS)
			));
		}
		return tag;
	}


	private void init() {
		modbustags.sort((a, b) -> a.address - b.address);

		hasReadCoils = false;
		hasReadDscinps = false;
		hasReadHldregs = false;
		hasReadInpregs = false;
		for( ModbusTag mtag: modbustags) {
			if( mtag.access == ModbusTag.Access.WO )
				continue;
			hasReadCoils   |= mtag.enable  &&  mtag.region == ModbusTag.Region.COIL;
			hasReadDscinps |= mtag.enable  &&  mtag.region == ModbusTag.Region.DSCINP;
			hasReadHldregs |= mtag.enable  &&  mtag.region == ModbusTag.Region.HLDREG;
			hasReadInpregs |= mtag.enable  &&  mtag.region == ModbusTag.Region.INPREG;
		}

		try {
			Map<String, ModbusTag> map = modbustags.stream().collect(Collectors.toMap(c -> c.name, c -> c));
			for (ModbusTag mt : modbustags)
				if (!mt.tracktagName.isEmpty()) {
					mt.tracktag = map.get(mt.tracktagName);
				}
		} catch (IllegalStateException e) {
			env.printError(logger, name, "Duplicate tagname");
		}

		bittags.forEach(btag -> btag.reftag = (TagRW)tagtable.get(btag.refTagname));
		bittags.removeIf(btag -> btag.reftag == null);
	}


	@Override
	public boolean executePeripherialModule() {
		boolean result = true;

		processBittagsWrites();

		if(emulated) {
			for( ModbusTag mtag: modbustags)
				mtag.tag.acceptWriteValue();
		} else {

			boolean needWriteReg = false;
			boolean needWriteCoil = false;
			for (ModbusTag mtag : modbustags) {
				if (mtag.access == ModbusTag.Access.RO)
					continue;

				mtag.needWrite = mtag.enable &&
						(mtag.tag.hasWriteValue() ||
								(mtag.tracktag != null && !mtag.tag.equalsValue(mtag.tracktag.tag)));

				needWriteCoil |= mtag.needWrite && (mtag.region == ModbusTag.Region.COIL);
				needWriteReg |= mtag.needWrite && (mtag.region == ModbusTag.Region.HLDREG);
			}

			if (needWriteReg) {
				result = writeMultiple(ModbusTag.Region.HLDREG);
				result &= writeSingle(ModbusTag.Region.HLDREG);
			}

			if (needWriteCoil) {
				result &= writeMultiple(ModbusTag.Region.COIL);
				result &= writeSingle(ModbusTag.Region.COIL);
			}

			if (hasReadHldregs)
				result &= readRegisters(ModbusTag.Region.HLDREG);

			if (hasReadInpregs)
				result &= readRegisters(ModbusTag.Region.INPREG);

			if (hasReadCoils)
				result &= readRegisters(ModbusTag.Region.COIL);

			if (hasReadDscinps)
				result &= readRegisters(ModbusTag.Region.DSCINP);
		}

		processBittagsReads();

		return result;
	}


	private void processBittagsWrites() {
		bittags.stream()
				.filter(btag -> btag.tag.hasWriteValue())
				.forEach(ModbusBitTag::writeValue);
	}


	private void processBittagsReads() {
		bittags.forEach(ModbusBitTag::readValue);
	}


	private boolean writeMultiple(ModbusTag.Region region) {
		int maxWriteSize = (region == ModbusTag.Region.HLDREG)? maxWriteSizeReg: maxWriteSizeCoil;
		if( maxWriteSize <= 0 )
			return true;
		
		int beg = 0;
		int size = 0;
		int buffsize = 0;
		int nextaddr = 0;

		ModbusTag mtag;
		for(int i=0; i<modbustags.size(); ++i) {
			mtag = modbustags.get(i);
			
			if( size >= maxWriteSize  ||  (size > 0  &&  mtag.address != nextaddr) ) {
				if( !writeMultiple1(region, beg, size, buffsize) )
					return false;
				size = 0;
				buffsize = 0;
			}
			
			if( mtag.needWrite  &&  
				mtag.writeMultiple  &&  
				mtag.region == region )
			{
				if( size == 0 )
					beg = i;
				size++;
				buffsize += mtag.size;
				nextaddr = mtag.address + mtag.size; 
			} 
		}
		
		if( size > 0 )
			if( !writeMultiple1(region, beg, size, buffsize) )
				return false;
		
		return true;
	}


	private boolean writeMultiple1(ModbusTag.Region region, int beg, int size, int buffsize) {
		if( size == 0  ||  (buffsize == 1  &&  modbustags.get(beg).writeSingle) )
			return true;
		
		adjustBuff(buffsize);
		int pbuff = 0;
		int end = beg + size;

		ModbusTag mtag;
		for(int j=beg; j<end; ++j) {
			mtag = modbustags.get(j);
			mtag.putValueIntoBuff(buff, pbuff);
			if( mtag.access == ModbusTag.Access.WO )
				mtag.tag.copyLastWriteToRead();
			mtag.needWrite = false;
			pbuff += mtag.size;
		}
		
		boolean result = false;
		try {
			result = (region == ModbusTag.Region.HLDREG)?
					protocol.requestCmd10(modbustags.get(beg).address, buffsize, buff) :
					protocol.requestCmd0F(modbustags.get(beg).address, buffsize, buff);
			
			if( !result )
				for(int i=beg; i<end; ++i)
					modbustags.get(i).tag.raiseWriteValue();
				
		} catch (Exception e) {
			env.printError(logger, e, name);
			result = false;
		}
		
		return result;
	}

	
	
	
	private boolean writeSingle(ModbusTag.Region region) {
		int i, size;
		boolean result = false;
		try {
			for(ModbusTag mtag: modbustags) {
				if( mtag.needWrite  &&  mtag.writeSingle  &&  mtag.region == region ) {
					mtag.needWrite = false;
					if( mtag.access == ModbusTag.Access.WO )
						mtag.tag.copyLastWriteToRead();
					
					if( region == ModbusTag.Region.HLDREG ) {
						size = mtag.size;
						adjustBuff(size);
						mtag.putValueIntoBuff(buff, 0);
						for(i=0; i<size; ++i) 
							result = protocol.requestCmd6(mtag.address + i, buff[i]);
					} else
						result = protocol.requestCmd5(mtag.address, mtag.tag.getWriteValInt() );
					
					if(!result) {
						mtag.tag.raiseWriteValue();
						return false;
					}
				}
			}
		} catch (Exception e) {
			env.printError(logger, e, name);
		}
		
		return true;
	}



	private boolean readRegisters(ModbusTag.Region region) {
		
		try {
			boolean isReg = region == ModbusTag.Region.HLDREG  ||  region == ModbusTag.Region.INPREG;
			boolean result = false;
			
			int beg = -1;
			int end = -1;
					
			ModbusTag mtag;
			for(int i=0; i<modbustags.size(); ++i) {
				mtag = modbustags.get(i);
				
				if( mtag.region == region  &&  mtag.access != ModbusTag.Access.WO ) {
					if( beg == -1 )
						beg = i;
					end = i;
				}
				
				if( end >= 0  &&  (modbustags.get(end).readEnd  ||  i == (modbustags.size()-1) )) {
					int addrbeg = modbustags.get(beg).address;
					int size = modbustags.get(end).address + modbustags.get(end).size - addrbeg;
					
					switch(region) {
					case HLDREG:
						result = protocol.requestCmd3(addrbeg, size); break;
					case INPREG:
						result = protocol.requestCmd4(addrbeg, size); break;
					case COIL:
						result = protocol.requestCmd1(addrbeg, size); break;
					case DSCINP:
						result = protocol.requestCmd2(addrbeg, size); break;
					}

					if( !result )
						return false;
					
					for(int j=beg; j<=end; ++j) { 
						mtag = modbustags.get(j);
						if( mtag.region == region  &&  mtag.access != ModbusTag.Access.WO ) {
							if( isReg )
								mtag.fetchValueFromProtocolBuffin(protocol, mtag.address - addrbeg);
							else
								mtag.tag.setReadValBool( (protocol.getAnswerBit(mtag.address - addrbeg) > 0) ^ mtag.inverted );
						}
					}
					
					beg = -1;
					end = -1;
				}
			}
		} catch (Exception e) {
			env.printError(logger, e, name);
			return false;
		}
		
		return true;
	}

	
    public void adjustBuff(int size) {
		if( buff.length < size )
			buff = new int[size];
    }


	@Override
	protected boolean reload() {
		ModbusModule tmp = new ModbusModule(plugin, name);
		return reloadFrom(tmp);
	}


	protected boolean reloadFrom(ModbusModule tmp) {
		if( !tmp.load() )
			return false;

		copySettingsFrom(tmp);

		maxWriteSizeCoil = tmp.maxWriteSizeCoil;
		maxWriteSizeReg = tmp.maxWriteSizeReg;

		for(ModbusTag mtag: modbustags) {
			Tag found = tmp.tagtable.get(mtag.tag.getName());
			if( found == null  ||  found.getType() != mtag.tag.getType()  ||  !(found instanceof TagRW) ) 
				tagtable.remove(mtag.tag);
		}
		
		for(ModbusTag mtag: tmp.modbustags) {
			TagRW tag = (TagRW)tagtable.get(mtag.tag.getName());
			if( tag == null )
				tagtable.add( mtag.tag );
			else {
				mtag.tag = tag;
			}
		}
		modbustags = tmp.modbustags;


		bittags.stream()
				.map(btag -> btag.tag.getName())
				.filter(tagname -> tmp.tagtable.get(tagname) == null)
				.forEach(tagname -> tagtable.remove(tagname));

		for(ModbusBitTag btag: tmp.bittags) {
			TagRW tag = (TagRW)tagtable.get(btag.tag.getName());
			if( tag == null )
				tagtable.add( btag.tag );
			else {
				btag.tag = tag;
			}
		}

		bittags = tmp.bittags;


		init();
		
		return true;
	}

}
