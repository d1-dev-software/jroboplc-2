package promauto.jroboplc.plugin.peripherial;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.Flags;
import promauto.jroboplc.core.api.Plugin;
import promauto.jroboplc.core.api.Tag;
import promauto.utils.Numbers;

public class PaTermo5Module extends PeripherialModule {

	private final Logger logger = LoggerFactory.getLogger(PaTermo5Module.class);
	
	protected ProtocolModbus protocol = new ProtocolModbus(this); 
	
	public static final int PDV_COUNT = 12;
	public static final int SENSOR_COUNT = 6;
	
	public enum PdvType {
		Classic,
		Compensate
	}

	public static class Pdv {
		public Sensor[] sensors = new Sensor[SENSOR_COUNT];
		public Tag Umin;
		public Tag Umax;
		public Tag Time;
		public Tag Tmax;

		public float Rzero;
	    public float Rwire;
	    public PdvType pdvType;
	}

	public static class Sensor {
		public Tag T;
		public Tag U;
		public Tag R;
	}
	public Pdv[] pdvs = new Pdv[PDV_COUNT];
	
	
	protected Tag tagAnswer;


	 
	
	public PaTermo5Module(Plugin plugin, String name) {
		super(plugin, name);
	}


	@Override
	public boolean loadPeripherialModule(Object conf) {
		Configuration cm = env.getConfiguration();
		
		Object pdvconf = cm.get(conf, "params");
		
		Pattern p = Pattern.compile("^\\s*([c,p])\\s+(\\d+\\.*\\d*)\\s+(\\d+\\.*\\d*)\\s*$");
		
		for(int i=0; i<PDV_COUNT; ++i) {
			pdvs[i] = new Pdv();
			
			String s = "Pdv" + i + ".";
			for(int j=0; j<SENSOR_COUNT; ++j) {
				pdvs[i].sensors[j] = new Sensor();
				pdvs[i].sensors[j].T = tagtable.createInt(s + "T" + j, 0, Flags.STATUS);
				pdvs[i].sensors[j].U = tagtable.createInt(s + "U" + j, 0, Flags.STATUS);
				pdvs[i].sensors[j].R = tagtable.createInt(s + "R" + j, 0, Flags.STATUS);
			}
			pdvs[i].Umin = tagtable.createInt(s + "Umin", 0, Flags.STATUS);
			pdvs[i].Umax = tagtable.createInt(s + "Umax", 0, Flags.STATUS);
			pdvs[i].Time = tagtable.createInt(s + "Time", 0, Flags.STATUS);
			pdvs[i].Tmax = tagtable.createInt(s + "TMax", 0, Flags.STATUS);
			
			s = cm.get(pdvconf, "p"+i, "c 53 0"); // type Rzero Rwire
			Matcher m = p.matcher(s);
			if( m.find() )
			{
				pdvs[i].pdvType = m.group(1).equals("c")? PdvType.Classic: PdvType.Compensate;
				pdvs[i].Rzero = Float.parseFloat( m.group(2));
				pdvs[i].Rwire = Float.parseFloat( m.group(3));
			}
			else {
				env.printError(logger, name, "pdv" + i + ":", s);
				return false;
			}

		}

		return true;
	}


	@Override
	protected void initChannelMap(List<String> chtags) {
		for( int i=0; i<PDV_COUNT; ++i )
			for(int j=0; j<SENSOR_COUNT; ++j) {
				addChannelMapTag(chtags, pdvs[i].sensors[j].T, i + "." + j);
			}
	}


	@Override
	public boolean executePeripherialModule() {
		if(emulated)
			return true;
		
		boolean result = true;
		
		try {
			// read time
			if( result = protocol.requestCmd3(0x1060, 12) ) {
				int ofs = 1;
				for(Pdv pdv: pdvs)
					pdv.Time.setInt( Numbers.bytesToWord(protocol.buffin, ofs+=2));
			}
			
			// read the rest (by two pdv per request)
			if( result ) {
				int reqofs = 0x1000;
				int reqsize = (SENSOR_COUNT + 2) * 2; // sensors + Umin + Umax
				int answerOfs1 = 3;
				int answerOfs2 = answerOfs1 + reqsize;
				int idxPdv = 0;
				for(int i=0; i<PDV_COUNT/2; ++i) {
					if( result = protocol.requestCmd3(reqofs, reqsize) ) {
						fetchPdvData(idxPdv++, answerOfs1);
						fetchPdvData(idxPdv++, answerOfs2);
						reqofs += reqsize;
					} else
						break;
				}
			}
		} catch (Exception e) {
			env.printError(logger, e, name);
			result = false;
		}
		
		return result;
	}


	private void fetchPdvData(int idxPdv, int ofs) {
		Pdv pdv = pdvs[idxPdv];
		ofs -=2;
		
		for(Sensor sensor: pdv.sensors) 
			sensor.U.setInt( Numbers.bytesToWord(protocol.buffin, ofs+=2));
	
		int Umin = Numbers.bytesToWord(protocol.buffin, ofs+=2);
		int Umax = Numbers.bytesToWord(protocol.buffin, ofs+=2);
		pdv.Umin.setInt( Umin );
		pdv.Umax.setInt( Umax );

		
		int i = 0;
		float Tmax = Float.MIN_VALUE;  
		for(Sensor sensor: pdv.sensors) {
			float r = calcR( sensor.U.getInt(), Umin, Umax);
			sensor.R.setInt( (int)Math.round(r * 10.0) );
			
			float t = calcT( r, pdv, i++ );
			sensor.T.setInt( (int)Math.round(t * 10.0 ) );
			
			if( t > Tmax )
				Tmax = t;
		}

		pdv.Tmax.setDouble( Tmax * 10.0 );
	}


	private static final float Rmin = 41.2f;
	private static final float Rmax = 80.6f;
	private static final float Rdel = 475f;
	private static final float Dmin = Rmin / (Rmin + Rdel);
	private static final float Dmax = Rmax / (Rmax + Rdel);
	private static final float Dsub = Dmax - Dmin;
	
	private float calcR(float Ur, float Umin, float Umax) {
		float v = (Umax - Umin) / Dsub;
		float dv = Umax - (Dmax * v);
		return Rdel * (Ur - dv) / (v - Ur + dv);
	}
	
	
	
    private static final float[] Rld = new float[]{0.21f, 0.46f, 0.71f, 0.96f, 1.21f, 1.46f};
	private static final float A = 4.3e-3f;

	private float calcT(float Rall, Pdv pdv, int sensorIdx) {
		float r;
		if(pdv.pdvType == PdvType.Classic)
			r = Rall - Rld[sensorIdx] - pdv.Rwire;
		else
			r = Rall - pdv.Rwire;
			
		float t = ((r/pdv.Rzero)-1)/A;
		
		return t;
	}


	
	@Override
	protected boolean reload() {
		PaTermo5Module tmp = new PaTermo5Module(plugin, name);
		if( !tmp.load() )
			return false;

		copySettingsFrom(tmp);
		
		for(int i=0; i<PDV_COUNT; ++i) {
			pdvs[i].Rzero   = tmp.pdvs[i].Rzero;  
		    pdvs[i].Rwire   = tmp.pdvs[i].Rwire;  
		    pdvs[i].pdvType = tmp.pdvs[i].pdvType;
		}
		
		return true;
	}


}
