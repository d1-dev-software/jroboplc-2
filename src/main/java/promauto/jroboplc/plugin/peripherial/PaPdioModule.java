package promauto.jroboplc.plugin.peripherial;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.api.*;
import promauto.jroboplc.core.tags.TagRW;

import java.util.List;

public class PaPdioModule extends PeripherialModule {
	protected final Logger logger = LoggerFactory.getLogger(PaPdioModule.class);

	protected ProtocolAA55 protocol = new ProtocolAA55(this);

	protected static final int INP_SIZE = 64;
	protected static final int OUT_SIZE = 64;
	protected static final int BRD_SIZE = 4;

	protected Tag[] inputs = null;
	protected Tag[] outputs = null;
	protected Tag[] boards = null;

	protected int[] buffin = new int[9];
	protected int[] buffout = new int[11];

	protected int readcmd;
	protected int writecmd;

	private enum Mode {
		ReadWrite, ReadOnly, WriteOnly
	}

	protected Mode mode;

	protected String dependsOnModuleName;
	protected PeripherialModule dependsOnModule;

	private TagRW tagFirmware;
	private Tag tagResetCnt;
	private int resetCntPeriod;


	public PaPdioModule(Plugin plugin, String name) {
		super(plugin, name);
		readcmd = 0x40;
		writecmd = 0x20;
	}

	@Override
	public boolean loadPeripherialModule(Object conf) {
		Configuration cm = env.getConfiguration();

		resetCntPeriod = cm.get(conf, "resetCntPeriod_s", 0) * 1000;

		String smode = cm.get(conf, "mode", "").toLowerCase();
		if (smode.equals("readonly"))
			mode = Mode.ReadOnly;
		else if (smode.equals("writeonly"))
			mode = Mode.WriteOnly;
		else
			mode = Mode.ReadWrite;

		if (mode != Mode.WriteOnly) {
			inputs = new Tag[INP_SIZE];
			for (int i = 0; i < INP_SIZE; i++)
				inputs[i] = tagtable.createInt(((i < 10) ? "inp0" : "inp") + i, 0, Flags.STATUS);

			boards = new Tag[BRD_SIZE];
			for (int i = 0; i < BRD_SIZE; i++)
				boards[i] = tagtable.createInt("brd" + i, 0, Flags.STATUS);
		}

		if (mode != Mode.ReadOnly) {
			outputs = new Tag[OUT_SIZE];
			for (int i = 0; i < OUT_SIZE; i++)
				outputs[i] = tagtable.createInt(((i < 10) ? "out0" : "out") + i, 0, Flags.STATUS);
		}

		dependsOnModuleName = cm.get(conf, "depends", "");

		tagFirmware = tagtable.createRWString("firmware", "");
		tagResetCnt = tagtable.createInt("resetcnt", 0);

		return true;
	}


	@Override
	protected void initChannelMap(List<String> chtags) {
		switch (mode) {
			case ReadOnly:
				for (int i = 0; i < INP_SIZE; ++i) {
					addChannelMapTag(chtags, inputs[i], "" + i);
				}
				break;
			case WriteOnly:
				for (int i = 0; i < OUT_SIZE; ++i) {
					addChannelMapTag(chtags, outputs[i], "" + i);
				}
				break;
			case ReadWrite:
				for (int i = 0; i < OUT_SIZE; ++i) {
					addChannelMapTag(chtags, inputs[i], i + "r");    // 1-1r - example of short notation
					addChannelMapTag(chtags, outputs[i], i + "w");   // 1-1w
				}
				break;
		}
	}


	@Override
	public boolean preparePeripherialModule() {
		if (dependsOnModuleName.isEmpty())
			dependsOnModule = null;
		else {
			Module m = env.getModuleManager().getModule(dependsOnModuleName);
			if (m == null) {
				env.printError(logger, name, "Dependency module not found:", dependsOnModuleName);
				return false;
			}

			if (!PeripherialModule.class.isInstance(m)) {
				env.printError(logger, name, "Dependency module is not a PeripherialModule:",
						dependsOnModuleName);
				dependsOnModule = null;
				return false;
			}

			dependsOnModule = (PeripherialModule) m;
		}

		return true;
	}

	@Override
	public boolean executePeripherialModule() {

		if (emulated)
			return true;

		boolean result = true;
		try {

			// read
			if (mode != Mode.WriteOnly) {
				result = false;
				buffout[0] = 0x55;
				buffout[1] = readcmd + netaddr;

				if (protocol.request(buffout, 2, buffin, 9, ProtocolAA55.CrcAA55)) {
					decodeBuffinToInputs();

					for (int i = 0; i < BRD_SIZE; i++)
						boards[i].setInt((buffin[i * 2 + 1] << 8) + (buffin[i * 2]));

					result = true;
				}
			}

			// write
			if (dependsOnModule != null && dependsOnModule.tagError.getBool())
				result = false;

			if (result && mode != Mode.ReadOnly) {
				buffout[0] = 0x55;
				buffout[1] = writecmd + netaddr;

				encodeOutputsToBuffout();

				protocol.setCrcAA55(buffout, 11);
				protocol.writeBytes(buffout, 11);
			}


			if( result  &&  (firstPass  || tagError.getBool())) {
				result = protocol.requestFirmware( tagFirmware );
			}

			result = result & protocol.requestResetCnt( tagResetCnt, resetCntPeriod);


		} catch (Exception e) {
			env.printError(logger, e, name);
		}

		return result;
	}

	protected void decodeBuffinToInputs() {
		int mask = 1;
		for (int i = 0; i < INP_SIZE; i++) {
			inputs[i].setInt((buffin[i / 8] & mask) > 0 ? 1 : 0);
			if ((mask = mask << 1) > 0x80)
				mask = 1;
		}
	}

	protected void encodeOutputsToBuffout() {
		int b = 0;
		int mask = 1;
		for (int i = 0; i < 64; i++) {
			if (outputs[i].getInt() > 0)
				b = b + mask;
			mask = mask << 1;

			if ((i + 1) % 8 == 0) {
				buffout[i / 8 + 2] = b;
				b = 0;
				mask = 1;
			}
		}
	}

	@Override
	protected boolean reload() {
		PaPdioModule tmp = new PaPdioModule(plugin, name);
		if (!tmp.load() || !tmp.prepare())
			return false;

		copySettingsFrom(tmp);

		mode = tmp.mode;
		dependsOnModule = tmp.dependsOnModule;
		dependsOnModuleName = tmp.dependsOnModuleName;

		syncTags(inputs, tmp.inputs);
		syncTags(outputs, tmp.outputs);
		syncTags(boards, tmp.boards);

		inputs = tmp.inputs;
		outputs = tmp.outputs;
		boards = tmp.boards;

		return true;
	}

	private void syncTags(Tag[] mine, Tag[] theirs) {
		if (mine == null && theirs != null)
			for (Tag tag : theirs)
				tagtable.add(tag);

		if (mine != null && theirs == null)
			for (Tag tag : mine)
				tagtable.remove(tag);
	}

}
