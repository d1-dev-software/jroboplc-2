package promauto.jroboplc.plugin.peripherial;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.api.Flags;
import promauto.jroboplc.core.api.Plugin;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.TagRW;
import promauto.utils.CRC;


public class PaMPTModule extends PeripherialModule {
    private final Logger logger = LoggerFactory.getLogger(PaPdatModule.class);

    protected ProtocolAA55 protocol = new ProtocolAA55(this);


    protected TagRW tagReqPos;
    protected TagRW   tagReqZdv;
    protected Tag        tagError;
    protected Tag        tagAlarm;
    protected Tag        tagCurPos;
    protected Tag        tagLastPos;
    protected Tag        tagSost;
    protected Tag        tagSensors;
    protected Tag        tagCurZdv;

    private TagRW     tagFirmware;

    protected int cmdPos;
    protected int cmdZdv;

    private int[] buffout = new int[5];
    private int[] buffin = new int[10];

    public PaMPTModule(Plugin plugin, String name) {
        super(plugin, name);
    }




    @Override
    public boolean loadPeripherialModule(Object conf) {

        tagReqPos = tagtable.createRWInt("ReqPos", 0, Flags.STATUS);
        tagReqZdv = tagtable.createRWInt("ReqZdv", 0, Flags.STATUS);

        tagError   = tagtable.createInt("Error", 0, Flags.STATUS);
        tagAlarm   = tagtable.createInt("Alarm", 0, Flags.STATUS);
        tagCurPos  = tagtable.createInt("CurPos", 0, Flags.STATUS);
        tagLastPos = tagtable.createInt("LastPos", 0, Flags.STATUS);
        tagSost    = tagtable.createInt("Sost", 0, Flags.STATUS);
        tagSensors = tagtable.createInt("Sensors", 0, Flags.STATUS);
        tagCurZdv  = tagtable.createInt("CurZdv", 0, Flags.STATUS);

        tagFirmware = tagtable.createRWString("firmware", "");

        cmdPos = 0;
        cmdZdv = 0;

        return true;
    }



    @Override
    public boolean executePeripherialModule() {

        if(emulated)
            return true;

        boolean result = true;
        try {

            if( tagReqPos.hasWriteValue() )
                cmdPos = tagReqPos.getWriteValInt();

            if( tagReqZdv.hasWriteValue() )
                cmdZdv = tagReqZdv.getWriteValInt();


            buffout[0] = 0x55;
            buffout[1] = 0x20 + netaddr;
            buffout[2] = cmdPos;
            buffout[3] = cmdZdv;
            buffout[4] = CRC.getCrc8(buffout, 1, 3);
            if( result = protocol.request(buffout, 5, buffin, 10, ProtocolAA55.Crc8) ) {

                tagReqPos .setReadValInt(buffin[4]);
                tagReqZdv .setReadValInt(buffin[8]);
                tagError  .setInt(buffin[0]);
                tagAlarm  .setInt(buffin[1]);
                tagCurPos .setInt(buffin[3]);
                tagLastPos.setInt(buffin[2]);
                tagSost   .setInt(buffin[5]);
                tagSensors.setInt(buffin[6]);
                tagCurZdv .setInt(buffin[7]);
            }


            if( cmdPos == tagReqPos.getInt() )
                cmdPos = 0;

            if( cmdZdv == tagReqZdv.getInt() )
                cmdZdv = 0;

            if( result  &&  (firstPass  || tagError.getBool())) {
                result = protocol.requestFirmware( tagFirmware );
            }


        } catch (Exception e) {
            env.printError(logger, e, name);
            result = false;
        }


        return result;
    }



}
