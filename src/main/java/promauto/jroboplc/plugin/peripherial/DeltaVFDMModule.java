package promauto.jroboplc.plugin.peripherial;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.api.Flags;
import promauto.jroboplc.core.api.Plugin;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.TagRW;

public class DeltaVFDMModule extends PeripherialModule {
	private final Logger logger = LoggerFactory.getLogger(DeltaVFDMModule.class);
	
	protected ProtocolModbus protocol = new ProtocolModbus(this);

//    protected TagIntRW tagReg0001;  
//    protected TagIntRW tagReg0002; 
//    
//    protected Tag tagReg0020;
//    protected Tag tagReg0021;
//    protected Tag tagReg0022;
//    protected Tag tagReg0023;
//    protected Tag tagReg0024;
//    protected Tag tagReg0027; 
//    protected Tag tagReg0028;
//    protected Tag tagReg0029; 
//    protected Tag tagReg002A; 

    protected TagRW tagReg2000;
    protected TagRW tagReg2001;
    protected TagRW tagReg2002;
    
    protected Tag tagReg2100;
    protected Tag tagReg2101;
    protected Tag tagReg2102;
    protected Tag tagReg2103;
    protected Tag tagReg2104;
    protected Tag tagReg2105; 
    protected Tag tagReg2106;
    protected Tag tagReg2107; 
    protected Tag tagReg2108;
    protected Tag tagReg2109;
    protected Tag tagReg210A;
    protected Tag tagReg210B;
    protected Tag tagReg210C;
    protected Tag tagReg210D;
    
    
//    idxReg2000   = 0;
//    idxReg2001   = idxReg2000+1;
//    idxReg2002   = idxReg2001+1;
//   //idxReg2000 to idxReg2002 rw
//
//    idxReg2100   = idxReg2002+1;      // Reg2100   Регистр Error Code:
//    idxReg2101   = idxReg2100+1;      // Reg2101   Status
//    idxReg2102   = idxReg2101+1;      // Reg2102   Опорная часторта
//    idxReg2103   = idxReg2102+1;      // Reg2103   Текущая частота
//    idxReg2104   = idxReg2103+1;      // Reg2104   Текущий ток
//    idxReg2105   = idxReg2104+1;
//    idxReg2106   = idxReg2105+1;
//    idxReg2107   = idxReg2106+1;
//    idxReg2108   = idxReg2107+1;
//    idxReg2109   = idxReg2108+1;
//    idxReg210A   = idxReg2109+1;
//    idxReg210B   = idxReg210A+1;
//    idxReg210C   = idxReg210B+1;
//    idxReg210D   = idxReg210C+1;     // Reg210D   Температура

    
    
	
	public DeltaVFDMModule(Plugin plugin, String name) {
		super(plugin, name);
	}


	@Override
	public boolean loadPeripherialModule(Object conf) {

//		tagReg0001 = protocol.addWriteTag( 0x0001, tagtable.createIntRW("Reg.R0001", 0)) );
//		tagReg0002 = protocol.addWriteTag( 0x0002, tagtable.createIntRW("Reg.R0002", 0)) );
//		
//		tagReg0020 = tagtable.createInt("Reg.R0020", 0);
//		tagReg0021 = tagtable.createInt("Reg.R0021", 0);
//		tagReg0022 = tagtable.createInt("Reg.R0022", 0);
//		tagReg0023 = tagtable.createInt("Reg.R0023", 0);
//		tagReg0024 = tagtable.createInt("Reg.R0024", 0);
//		tagReg0027 = tagtable.createInt("Reg.R0027", 0);
//		tagReg0028 = tagtable.createInt("Reg.R0028", 0);
//		tagReg0029 = tagtable.createInt("Reg.R0029", 0);
//		tagReg002A = tagtable.createInt("Reg.R002A", 0);
		
		tagReg2000 = protocol.addWriteTag( 0x2000, tagtable.createRWInt("Reg.R2000", 0, Flags.STATUS));
		tagReg2001 = protocol.addWriteTag( 0x2001, tagtable.createRWInt("Reg.R2001", 0, Flags.STATUS));
		tagReg2002 = protocol.addWriteTag( 0x2002, tagtable.createRWInt("Reg.R2002", 0, Flags.STATUS));
		
		tagReg2100 = tagtable.createInt("Reg.R2100", 0, Flags.STATUS);
		tagReg2101 = tagtable.createInt("Reg.R2101", 0, Flags.STATUS);
		tagReg2102 = tagtable.createInt("Reg.R2102", 0, Flags.STATUS);
		tagReg2103 = tagtable.createInt("Reg.R2103", 0, Flags.STATUS);
		tagReg2104 = tagtable.createInt("Reg.R2104", 0, Flags.STATUS);
		tagReg2105 = tagtable.createInt("Reg.R2105", 0, Flags.STATUS);
		tagReg2106 = tagtable.createInt("Reg.R2106", 0, Flags.STATUS);
		tagReg2107 = tagtable.createInt("Reg.R2107", 0, Flags.STATUS);
		tagReg2108 = tagtable.createInt("Reg.R2108", 0, Flags.STATUS);
		tagReg2109 = tagtable.createInt("Reg.R2109", 0, Flags.STATUS);
		tagReg210A = tagtable.createInt("Reg.R210A", 0, Flags.STATUS);
		tagReg210B = tagtable.createInt("Reg.R210B", 0, Flags.STATUS);
		tagReg210C = tagtable.createInt("Reg.R210C", 0, Flags.STATUS);
		tagReg210D = tagtable.createInt("Reg.R210D", 0, Flags.STATUS);

		return true;
	}


	
	@Override
	public boolean executePeripherialModule() {
		
		if(emulated) {
//			tagReg0001.acceptWriteValue();
//			tagReg0002.acceptWriteValue();
			
			tagReg2000.acceptWriteValue();
			tagReg2001.acceptWriteValue();
			tagReg2002.acceptWriteValue();
			return true;
		}
		
		boolean result = true;
		
		try {
			result = protocol.sendWriteTags(16);
			
			if( result )
				if( result = protocol.requestCmd3(0x2000, 3) ) {
//					tagReg0001.setReadValue( protocol.getAnswerWord(0) ); 
//					tagReg0002.setReadValue( protocol.getAnswerWord(1) );
					
					tagReg2000.setReadValInt( protocol.getAnswerWord(0) );
					tagReg2001.setReadValInt( protocol.getAnswerWord(1) );
					tagReg2002.setReadValInt( protocol.getAnswerWord(2) );
				}
				            
			if( result )
				if( result = protocol.requestCmd3(0x2100, 14) ) {
//					tagReg0020.setInt( protocol.getAnswerWord(0) );		
//					tagReg0021.setInt( protocol.getAnswerWord(1) );		
//					tagReg0022.setInt( protocol.getAnswerWord(2) );		
//					tagReg0023.setInt( protocol.getAnswerWord(3) );		
//					tagReg0024.setInt( protocol.getAnswerWord(4) );
					//      25										   5
					//      26										   6
//					tagReg0027.setInt( protocol.getAnswerWord(7) );		
//					tagReg0028.setInt( protocol.getAnswerWord(8) );		
//					tagReg0029.setInt( protocol.getAnswerWord(9) );		
//					tagReg002A.setInt( protocol.getAnswerWord(10));
					
					tagReg2100.setInt( protocol.getAnswerWord(0) );
					tagReg2101.setInt( protocol.getAnswerWord(1) );
					tagReg2102.setInt( protocol.getAnswerWord(2) );
					tagReg2103.setInt( protocol.getAnswerWord(3) );
					tagReg2104.setInt( protocol.getAnswerWord(4) );
					tagReg2105.setInt( protocol.getAnswerWord(5) );
					tagReg2106.setInt( protocol.getAnswerWord(6) );
					tagReg2107.setInt( protocol.getAnswerWord(7) );
					tagReg2108.setInt( protocol.getAnswerWord(8) );
					tagReg2109.setInt( protocol.getAnswerWord(9) );
					tagReg210A.setInt( protocol.getAnswerWord(10) );
					tagReg210B.setInt( protocol.getAnswerWord(11) );
					tagReg210C.setInt( protocol.getAnswerWord(12) );
					tagReg210D.setInt( protocol.getAnswerWord(13) );
					
				}
			
		} catch (Exception e) {
			env.printError(logger, e, name);
			result = false;
		}
	
		return result;
	}


}
