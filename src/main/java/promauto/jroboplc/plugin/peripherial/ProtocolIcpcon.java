package promauto.jroboplc.plugin.peripherial;

import promauto.utils.Numbers;
import promauto.utils.Strings;

public class ProtocolIcpcon {

	private PeripherialModule module;
	
	public ProtocolIcpcon(PeripherialModule module) {
		this.module = module;
	}
	

	private int calcCrc(int[] data, int size) {
		int crc = 0;
		
		for(int i = 0; i < size; i++)  
		    crc += data[i]; 
		
		return crc & 0xFF;
	}

	
    public int request(int[] buffout, int sizeout, int[] buffin, int sizein) throws Exception {

		Numbers.intToHexByte(module.netaddr, buffout, 1);

		int crcout = calcCrc(buffout, sizeout);
		Numbers.intToHexByte(crcout, buffout, sizeout);
		sizeout += 3;
		buffout[sizeout-1] = 13;
		
		for (int trynum = 0; trynum < module.retrial; trynum++) {

			module.port.discard();
			module.delayBeforeWrite();
			module.port.writeBytes(buffout, sizeout);

			boolean badcrc = false;
			int n = module.port.readBytes(buffin, sizein);

			if( n >= 3  ) {
				int crc1 = Numbers.asciiByteToInt(buffin, n-3);
				int crc2 = calcCrc( buffin, n-3 );
				if( crc1 == crc2 )
					return n;
				
				badcrc = true;
			}
			
			if( module.canLogError() ) {
				int cntRead = Math.abs(n);
				module.logError(trynum, badcrc, buffout, sizeout, buffin, cntRead, 
						new String(Strings.intsToBytes(buffin, cntRead)) );
			}

			module.delayAfterError();
		}
		module.tagErrorCnt.setInt( module.tagErrorCnt.getInt()+1 );

		return -1;
    }	
	

}
