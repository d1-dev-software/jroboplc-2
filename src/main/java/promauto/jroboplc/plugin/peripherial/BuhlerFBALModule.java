package promauto.jroboplc.plugin.peripherial;

import java.nio.charset.StandardCharsets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.api.Flags;
import promauto.jroboplc.core.api.Plugin;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.TagRW;
import promauto.utils.CRC;
import promauto.utils.Numbers;

// working with
// MZAH (FBAL) dosing devices

public class BuhlerFBALModule extends PeripherialModule {
        private final Logger logger = LoggerFactory.getLogger(BuhlerFBALModule.class);

        private static int CMD_START = 2;
        private static int CMD_STOP  = 0;
        
        private static int BUFF_SIZE = 64;
        
        protected int[] buffin = new int[BUFF_SIZE];
        protected int[] buffout = new int[BUFF_SIZE];
        protected int[] buffcmd = new int[BUFF_SIZE];
        
        
        protected Tag tagCrc;
        protected Tag tagSumWesHigh;
        protected Tag tagSumWesLow;
        protected Tag tagWNum;
        protected Tag tagLastWes;
        protected Tag tagCurWes;
        protected Tag tagStageNum;
        protected Tag tagErrCode;
        protected Tag tagWCycle;
        protected TagRW tagReqOutput;
        protected Tag tagReqDose;
        protected Tag tagCmdStop;
        protected TagRW tagCmdStart;
        protected Tag tagCmdSetOutput;
        protected Tag tagCmdSetDose;
        protected Tag tagTotalWeight;   // bhlTotalWeight   = 24;
        protected Tag tagAlarm;         // bhlAlarm          = 16;
        protected Tag tagStatus;        // bhlStatus         = 17;
        protected Tag tagCapacity;      // bhlCapacityHigh   = 18;
        protected Tag tagErzWeightLow;  // bhlErzWeightLow   = 22;
        protected Tag tagDecimalPoint;

        protected TagRW tagMinOutput;
        protected TagRW tagMaxOutput;
        protected TagRW tagResetErr;

        protected TagRW tagWesSvrState;

        private long wesSvrStateTimer;
    
        
        public BuhlerFBALModule(Plugin plugin, String name) {
                super(plugin, name);
        }


        @Override
        public boolean loadPeripherialModule(Object conf) {

                tagCrc          = tagtable.createInt("Crc",     0);

                tagSumWesHigh 	= tagtable.createInt("SumWesHigh",     0, Flags.STATUS);
                tagSumWesLow  	= tagtable.createInt("SumWesLow",     0, Flags.STATUS);
                tagWNum       	= tagtable.createInt("WNum",     		 0, Flags.STATUS);
                tagLastWes    	= tagtable.createInt("LastWes",    		 0, Flags.STATUS);
                tagCurWes     	= tagtable.createInt("CurWes",     	 	 0, Flags.STATUS);
                tagStageNum   	= tagtable.createInt("StageNum",     	 0, Flags.STATUS);
                tagErrCode    	= tagtable.createInt("ErrCode",     	 0, Flags.STATUS);
                tagWCycle     	= tagtable.createInt("WCycle",     		 0, Flags.STATUS);
                tagReqOutput  	= tagtable.createRWInt("ReqOutput", 0, Flags.STATUS);
                
                tagReqDose    	= tagtable.createInt("ReqDose",     	 0, Flags.STATUS);
                tagCmdStop    	= tagtable.createInt("CmdStop",     	 0, Flags.STATUS);
                tagCmdStart  	= tagtable.createRWInt("CmdStart", 0, Flags.STATUS);
//                tagCmdStart   	= tagtable.createInt("CmdStart",     	 0);
                tagCmdSetOutput = tagtable.createInt("CmdSetOutput",     0, Flags.STATUS);
                tagCmdSetDose   = tagtable.createInt("CmdSetDose",       0, Flags.STATUS);
                tagTotalWeight  = tagtable.createInt("TotalWeight",      0, Flags.STATUS);
                tagAlarm        = tagtable.createInt("Alarm",    		 0, Flags.STATUS);
                tagStatus       = tagtable.createInt("Status",     		 0, Flags.STATUS);
                tagCapacity     = tagtable.createInt("Capacity",    	 0, Flags.STATUS);
                tagErzWeightLow = tagtable.createInt("ErzWeightLow",     0, Flags.STATUS);
                tagDecimalPoint = tagtable.createInt("DecimalPoint",     0, Flags.STATUS);
                
                tagMinOutput  	= tagtable.createRWInt("MinOutput", 0, Flags.STATUS);
                tagMaxOutput  	= tagtable.createRWInt("MaxOutput", 0, Flags.STATUS);
                tagResetErr  	= tagtable.createRWInt("ResetErr", 0, Flags.STATUS);
                             
                tagWesSvrState  = tagtable.createRWInt("WesSvrState",  0);
                return true;
        }


        
        @Override
        public boolean executePeripherialModule() {
                
                boolean result = true;
                
                if( !emulated ) {
                        try {
                        		//h resutl size 39
                                if( result = request('h',40,0) ) { //39 bytes + code 13
//                                  >01hC9#$D
//                                  A 	10    01  	13 		000000		A00007			00000000				00000000				00		DE 	//39 symbols + code 13
//                           			state step  alm num fact proiz  curr whgting  	total whgt eraseble		total whgt noneraseble	nused	crc


/*
                                    StringBuilder strBuf=new StringBuilder();
                                	for(int i = 0; i < (39); i++){
                                        strBuf.append((char)buffin[i]);
                                    }

                                	tagStatus.setInt(Integer.parseInt(strBuf.substring(1,3)));
                                	tagStageNum.setInt(Numbers.asciiBCDToInt(strBuf.substring(3, 5)));
                                	tagErrCode.setInt(Numbers.asciiBCDToInt(strBuf.substring(5, 7)));

                                	tagCapacity.setInt(Numbers.asciiBCDToInt(strBuf.substring(7, 13)));
                                	tagCurWes.setInt(Numbers.asciiBCDToInt(strBuf.substring(13, 19)));

                                	tagErzWeightLow.setInt(Numbers.asciiBCDToInt(strBuf.substring(19, 27)));
                                	int valWes=Numbers.asciiBCDToInt(strBuf.substring(27, 35));
*/

                                    tagStatus.setInt(       Numbers.asciiToInt( buffin, 1, 3));
                                    tagStageNum.setInt(     Numbers.asciiToInt( buffin, 3, 5) );
                                    tagErrCode.setInt(      Numbers.asciiToInt( buffin, 5, 7) );

                                    tagCapacity.setInt(     Numbers.asciiToInt( buffin, 7, 13));
                                    tagCurWes.setInt(       Numbers.asciiToInt( buffin, 13,19));

                                    tagErzWeightLow.setInt( Numbers.asciiToInt( buffin, 19,27));
                                    int valWes =            Numbers.asciiToInt( buffin, 27,35);



                                	tagSumWesHigh.setInt(valWes / 0x10000);
                                	tagSumWesLow.setInt(valWes % 0x10000);
                                	tagTotalWeight.setInt(valWes);
                                	
                                }

                                //setting reqthroughput
                                if (tagReqOutput.hasWriteValue()){
                                	int posPrepend=6;
                                	int value = tagReqOutput.getWriteValInt();
                                	String strCmd=String.valueOf(value);
                                	// 001234 for example
                                	posPrepend-=strCmd.length();
                                	for(int i=0;i<posPrepend;i++){
                                		buffcmd[i]='0';
                                	}
                                	for(int i=0;i<strCmd.length();i++){
                                		buffcmd[i+posPrepend]=strCmd.charAt(i);
                                	}
                                	if( !(result = request('Q',2,6))) {
                                		tagReqOutput.setInt(value);
                                	}else{
                                		tagReqOutput.setReadValInt(value);
                                	}
                                }                                
                                
                                
                                // 's' starting stopping
                                if ((tagCmdStart.hasWriteValue())){
                                	tagCmdStop.setInt(0);
                                	int value=tagCmdStart.getWriteValInt();
                                	buffcmd[0]=(String.valueOf(value)).charAt(0);// start
//                                	tagCmdStart.setReadValInt(value); // debug script remove then release
                                	if( result = request('S',2,1)) {
                                		//something here?
//                                		StringBuilder strBuf=new StringBuilder();
//                                		for(int i = 0; i < (2); i++){
//	                                        strBuf.append((char)buffin[i]);
//                                		}
//                                		System.out.println(strBuf);
                                		tagCmdStart.setReadValInt(value);
                                	}
                                	tagCmdStart.setInt(value);
                                }
//
                                if (tagCmdStop.getInt()==1){
                                	tagCmdStart.setInt(0);
                                	buffcmd[0]='0';// stop
                                	if( result = request('S',2,1)) {
                                		//something here?
                                		tagCmdStop.setInt(0);
                                	}
                                }     
                                
                                if(tagResetErr.hasWriteValue()){
                                	buffcmd[0]='9';// reset err
                                	buffcmd[1]='0';// nu
                                	buffcmd[2]='0';// nu
                                	buffcmd[3]='0';// cmd
                                	
                                	buffcmd[4]='0';// reset err
                                	buffcmd[5]='0';// nu
                                	buffcmd[6]='0';// nu
                                	buffcmd[7]='0';// cmd
                                	buffcmd[8]='0';// reset err
                                	buffcmd[9]='0';// nu
                                	buffcmd[10]='0';// nu
                                	buffcmd[11]='0';// cmd
                                	buffcmd[12]='0';// reset err
                                	buffcmd[13]='0';// nu
                                	buffcmd[14]='0';// nu
                                	buffcmd[15]='0';// cmd
                                	buffcmd[16]='0';// reset err
                                	buffcmd[17]='0';// nu
                                	buffcmd[18]='0';// nu
                                	buffcmd[19]='0';// cmd
                                	if( result = request('A',2,20) ) {
//                                		System.out.println("A processed");
                                	}
                                }


//                              //getting minthroughput
//                                if( result = request('n',10,0) ) { //6+1(A)+2(CRC) bytes + code 13
//
//                                	StringBuilder strBuf=new StringBuilder();
//                                	for(int i = 0; i < (9); i++){
//                                        strBuf.append((char)buffin[i]);
//                                    }
//                                	tagMinOutput.setInt(Numbers.asciiBCDToInt(strBuf.substring(1, 7)));
//                                }
//                                //getting maxthroughput, need to be tested
//                                if( result = request('x',10,0) ) { //6+1(A)+2(CRC) bytes + code 13
//
//                                	StringBuilder strBuf=new StringBuilder();
//                                	for(int i = 0; i < (9); i++){
//                                        strBuf.append((char)buffin[i]);
//                                    }
//                                	tagMaxOutput.setInt(Numbers.asciiBCDToInt(strBuf.substring(1, 7)));
//                                }

                        } catch (Exception e) {
                                env.printError(logger, e, name);
                                result = false;
                        }
                }
                
                String crcstr = tagTotalWeight.getString() + ';' + tagWNum.getString();
                tagCrc.setInt( CRC.getCrc8(crcstr) );
                
                wesSvrStateTimer = updateWesSvrState(tagWesSvrState, wesSvrStateTimer);
                
                return result;
        }
        

        private boolean request(char param,int sizeAnsw,int sizeAsk) throws Exception {
                if( port == null )
                        return false;               
                
                int posPrm=3; // address off parameter
                int crc=0;
                buffout[0] = '>';                
    			Numbers.intToHexByte(netaddr, buffout, 1);// 1,2
    			buffout[posPrm] = param;
    			for(int i=0;i<sizeAsk;i++){
    				buffout[++posPrm]=buffcmd[i];
    			}
    			crc = calcCrc(buffout,1 ,posPrm);
    			posPrm++;
    			Numbers.intToHexByte(crc, buffout, posPrm);
    			posPrm++;
    			posPrm++;
    			buffout[posPrm++] = 13;     			
                
                
                for (int trynum = 0; trynum < retrial; trynum++) {

                        StringBuilder strBuf1=new StringBuilder();
                    	for(int i = (0); i < posPrm; i++){
                            strBuf1.append((char)buffout[i]);
                        }
//                      System.out.println("asking for dev "+netaddr+" Sending params "+posPrm);	// debugaga
//                    	System.out.println("asking strBuf1:"+posPrm+" "+strBuf1.toString());
                        port.discard();
                        port.writeBytes(buffout, posPrm);
                        int n = port.readBytes(buffin, sizeAnsw);
                        
                        if( n == sizeAnsw) {
                        	if(buffin[0]!='A')continue; //not acknowledged
                        	if(sizeAnsw<=2)return true;
                        	StringBuilder strBuf=new StringBuilder();
                        	for(int i = (sizeAnsw-3); i < sizeAnsw-1; i++){
                                strBuf.append((char)buffin[i]);
                            }
                        	crc=calcCrc(buffin,1 ,sizeAnsw-4);                        	
                        	int crcRcvd=Integer.parseInt(strBuf.toString(),16);
//                        	System.out.println("strBuf:"+strBuf.toString());
//                        	System.out.println("CRC calcetd "+crc);
//                        	System.out.println("CRC rcvd "+crcRcvd);
                        	if(crc!=crcRcvd) continue;	// need to be tested
                            return true;
                        }
                        try {
                                Thread.sleep(20);
                        } catch (InterruptedException e) {
                        }
                        
                }
                tagErrorCnt.setInt( tagErrorCnt.getInt() + 1 );

        return false;
        }
        
        
        
        private int calcCrc(int[] bytes, int begin, int end) {
    		int crc = 0;

    		for (int index = begin; index <= end; index++) {
    			int data = bytes[index];
    			crc = crc+data;
    		}
    		return (crc & 0xFF);
    	}        
        

}
