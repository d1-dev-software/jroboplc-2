package promauto.jroboplc.plugin.task;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.AbstractModule;
import promauto.jroboplc.core.api.*;

public class TaskModule extends AbstractModule implements Runnable, Task {
	private final Logger logger = LoggerFactory.getLogger(TaskModule.class);

	private Thread thread;

	protected Tag tagRunning;
	protected Tag tagTimecounter;
	protected Tag tagTimetotal_s;
	protected Tag tagTimecycle;
	protected Tag tagStatBaton;
	
	protected volatile boolean running = false;
	protected volatile boolean isStopped = true;
	protected volatile int stateBaton;

	protected int period;
//	private boolean enable = true;
	

	protected AtomicLong timecounter = new AtomicLong();
	private volatile long timetotal;
	private volatile long timecycle;

	protected LinkedList<TaskItem> items = new LinkedList<>();

	private Set<String> modules = new HashSet<>();

	private int delayStart;



	
	
	public TaskModule(Plugin plugin, String name){
		super(plugin, name);
		taskable = false;
	}
	
	

	public boolean loadModule(Object conf) {
		
		tagRunning = tagtable.createBool("running", false);
		tagTimecounter = tagtable.createInt("timecounter", 0);
		tagTimetotal_s = tagtable.createInt("timetotal", 0);
		tagTimecycle = tagtable.createInt("timecycle", 0);
		tagStatBaton = tagtable.createInt("statebaton", 0);
		
		Configuration cm = env.getConfiguration();
		
//		enable = cm.get(conf, "enable", true);

		delayStart = cm.get(conf, "delayStart", 0);
		period = cm.get(conf, "period", 1000);
		List<Object> records = cm.toList( cm.get(conf, "modules"));
		
		TaskManager tm = env.getTaskManager();
		Pattern p = Pattern.compile("(\\w+)\\((.*)\\)");
		
		try {
			for(Object rec: records) {
				TaskItem ti = null;
				
				Matcher m = p.matcher(rec.toString());
				if(m.find()) {
					String func = m.group(1);
					String args = m.group(2);
					
					if( func.equals("_wait") )
						ti = new TaskItemFlagWait(this, args);
					else
					
					if( func.equals("_set") )
						ti = new TaskItemFlagSet(this, args);
					else 
						
					if( func.equals("_sleep") )
						ti = new TaskItemSleep(this, args);
					else 
							
					{
						// error - unknown func
						return false;
					}
					
				} 
				else
				{
					String modname = rec.toString();
					Task task = tm.getTask( modname );
					if( task != null  &&  !task.getName().equals(name) ) {
						env.printError(logger, name, "Module is already usen in another task:", modname, task.getName());
						return false;
					}
					modules.add( modname );
					ti = new TaskItem(this, modname );
				}
				items.add( ti );
				
			}
		} catch(Exception e) {
			env.printError(logger, e, name);
			return false;
		}
		
		return true;
	}

	
	
	@Override
	public boolean prepareModule() {
//		System.out.println("prepare " + name);
		for (TaskItem ti : items) 
			if( !ti.init() )
				return false;

		return true;
	}

	
	
	@Override
	public boolean executeModule() {
		if( running )
			return true;
		
		synchronized(this) {
			if( !running ) {
				isStopped = false;
				thread = new Thread(this);
//				System.out.println("execute " + name);
				thread.setName("thread-" + name);
				running = true;
				tagRunning.setBool(true);
				stateBaton = 0;
				tagStatBaton.setInt( stateBaton );
				thread.start();
			}
		}
		
		return true;
	}

	
	
	@Override
	public boolean closedownModule() {
//		System.out.println("close " + name);

		isStopped = true;

		if( thread != null ) {
			thread.interrupt();
			try {
				thread.join();
			} catch (InterruptedException e) {
				env.printError(logger, e, name);
			}
		}

		try {
			while (running) {
				isStopped = true;
				Thread.sleep(1);
 			}
		} catch (InterruptedException e) {
			env.printError(logger, e, name);
		}
		
		return true;
	}



	@Override
	public String getInfo() {
		String s;
		if( enable )
			s = 
				"cnt=" + timecounter + 
				" total=" + (timetotal) +
				" cycle=" + (timecycle) + "/" + period;
		else
			s = "disabled";
		return s;
	}
	
	
	
	@Override
	public void run() {
		try {
			Thread.sleep(delayStart);

			long timetotalbeg = System.currentTimeMillis();
			long timedelta = 0;
			long timebeg;
			long timeend = timetotalbeg;
			long timeendReq = 0;
			long maxTimeDelta = period/4; 
			timecounter.set(0);

			while ( env.isRunning() && !env.isTerminated() &&  !Thread.currentThread().isInterrupted() ) {
				
				// need to invoke execute in order to reload (if it is requested)
				execute();
				
				// check isStopped after execute, because if task got reloaded isStopped could be changed  
				if( isStopped )
					break;
				
				timebeg = timeend;
				
				if( period > 0)
					timeendReq = timebeg + (period - timedelta);
				
				try {
					for (TaskItem item : items) {
						item.execute(timeendReq);
						if( isStopped )
							break;
					}
				} catch (InterruptedException e) {
					break;
				} catch (Exception e) {
					env.printError(logger, e, name, "Exception in the task");
				}
				
				timecycle = System.currentTimeMillis() - timebeg;
				tagTimecycle.setInt((int)(timecycle));
//				System.out.print( (System.currentTimeMillis() - timeend) + "   ");
				timeend = System.currentTimeMillis();
				
				if( !isStopped  &&  period > 0 ) {
					
					if( (timeend - timeendReq) < 0 ) {
						long timesleep = timeendReq - timeend;
						Thread.sleep(timesleep);
						timeend = System.currentTimeMillis();
					}
					
					timedelta = (timeend - timebeg) - (period - timedelta);
					
					if (timedelta < 0) 
						timedelta = 0;
					else
						if (timedelta > maxTimeDelta) 
							timedelta = maxTimeDelta;
				}

				
				tagTimecounter.setInt( (int)(timecounter.addAndGet(1)) );
				
				timetotal = timeend - timetotalbeg;
				tagTimetotal_s.setInt((int)(timetotal/1000));
			}

		} catch (InterruptedException e) {
//			env.printInfo(logger, name, "Task has been interrupted");
		}

		running = false;
		tagRunning.setBool(false);
		isStopped = true;
	}


	protected boolean reload() {
		boolean res = false;
		TaskModule tmp = new TaskModule( plugin, name );
		
		if( tmp.load() ) {
			if( tmp.prepare() ) {
				copySettingsFrom(tmp);
				enable = tmp.enable;
				period = tmp.period;
				modules = tmp.modules;
				
				items = tmp.items;
				for(TaskItem item: items)
					item.parentModule = this;
				
				if( !enable  &&  running )
					isStopped = true;
				else if( enable  &&  !running )
					execute();
				
				res = true;
			}
		}

		return res;
	}



	@Override
	public boolean isRunning() {
		return running;
	}



	@Override
	public boolean hasModule(String module) {
		return modules.contains(module);
	}



	@Override
	public Set<String> getModules() {
		return modules;
	}

    @Override
    public int getPeriod() {
        return period;
    }


}