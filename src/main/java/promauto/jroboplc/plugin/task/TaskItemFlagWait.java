package promauto.jroboplc.plugin.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TaskItemFlagWait extends TaskItem {
	final Logger logger = LoggerFactory.getLogger(TaskItemFlagWait.class);

	protected int value;
	protected TaskModule targetTaskModule;
	private boolean timeoutEnabled;
	private boolean targetToItself;

	public TaskItemFlagWait(TaskModule parent, String args) {
		super(parent, args);
		String[] ss = args.split(",");
		if(ss.length == 2) {
			targetModuleName = ss[0];
			value = Integer.parseInt(ss[1].trim());
		} else
			
		if(ss.length == 1) {
			targetModuleName = parentModule.getName();
			value = Integer.parseInt(ss[0].trim());
		} else
			
		{
			targetModuleName = "";
		}
		
		timeoutEnabled = parent.period > 0;
		targetToItself = targetModuleName.equals( parent.getName() ); 
	}
	
	@Override
	public boolean init() {
		if( super.init() ) { 
			if( targetModule.getClass() == TaskModule.class ) {
				targetTaskModule = (TaskModule)targetModule;
				return true;
			}
			env.printError(logger, parentModule.getName(), "Module is not a task:", targetModuleName);
		}
		return false;
	}

	@Override
	public void execute(long maxTime) throws InterruptedException {
		
//		System.out.println(
//				String.format("%s: wait(%s,%d) now is %d, begin", 
//					parentTask.getModule().getName(), 
//					targetTaskModule.getName(), 
//					value,
//					targetTaskModule.stateBaton 
//					));

		if( targetTaskModule == null  ||  parentModule == null )
			return;
		
		while( targetTaskModule.stateBaton != value  &&	!parentModule.isStopped ) {
			
			if( targetToItself ) {
				if( targetTaskModule.stateBaton == -1 )
					break;
			} else {
				
				if( timeoutEnabled  &&  (System.currentTimeMillis() - maxTime) > 0 ) {
					if( targetTaskModule.stateBaton != -1 ) {
						
//						parentTask.getModule().errMes(logger, null, 
//								String.format("Timeout %s: wait(%s,%d)", 
//										parentTask.getModule().getName(), 
//										targetModule.getName(), 
//										value));
						
						targetTaskModule.stateBaton = -1;
						targetTaskModule.tagStatBaton.setInt( targetTaskModule.stateBaton );
					}
				}
			}
			
			
			Thread.sleep(1);
		}
		
//		System.out.println(
//				String.format("%s: wait(%s,%d) now is %d, end", 
//					parentTask.getModule().getName(), 
//					targetTaskModule.getName(), 
//					value,
//					targetTaskModule.stateBaton 
//					));
		
	}

}
