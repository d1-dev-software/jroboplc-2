package promauto.jroboplc.plugin.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.api.Environment;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.Module;

public class TaskItem {
	final Logger logger = LoggerFactory.getLogger(TaskItem.class);

	protected Environment env;
	protected TaskModule parentModule;
	protected Module targetModule = null;
	protected String targetModuleName;

	public TaskItem(TaskModule parent, String args) {
		this.parentModule = parent;
		this.env = EnvironmentInst.get();
		this.targetModuleName = args;
	}
	
	public boolean init() {
		targetModule = env.getModuleManager().getModule( targetModuleName );
		if(targetModule == null) { 
			env.printError(logger, parentModule.getName(), "Module not found:", targetModuleName);
			return false;
		}
		return true;
	}

	public void execute(long maxTime) throws InterruptedException {
//		System.out.println(parentTask.getInfo().name + ": execute " + targetModuleName + " begin");
		
		if( targetModule != null)
			targetModule.execute();
		
//		System.out.println(parentTask.getInfo().name + ": execute " + targetModuleName + " end");
	}




}
