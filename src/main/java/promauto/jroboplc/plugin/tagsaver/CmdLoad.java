package promauto.jroboplc.plugin.tagsaver;

import java.nio.file.Files;
import java.nio.file.Path;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.AbstractCommand;
import promauto.jroboplc.core.api.*;

public class CmdLoad extends AbstractCommand {

	private final Logger logger = LoggerFactory.getLogger(CmdLoad.class);



	@Override
	public String getName() {
		return "load";
	}

	@Override
	public String getUsage() {
		return "filename";
	}

	@Override
	public String getDescription() {
		return "load tag values from a file";
	}

	
	
	
	@Override
	public String execute(Console console, Module module, String args) {
		if( args.trim().isEmpty() )
			return "Use format:\n" + module.getName() + ":" + getName() + " " + getUsage();

		module.postCommand(this, console, module, args);
		return "";
	}



	@Override
	public void executePosted(Console console, Module module, String args) {
		Environment env = EnvironmentInst.get();
		TagsaverModule m = (TagsaverModule) module;

//		m.merge();

		Path path = env.getConfiguration().getConfDir().resolve(args);
		if( !Files.exists(path) ) {
			env.printError(logger, module.getName(), "File is not found: \"" + args + "\"");
			return;
		}

		if( !m.readValues(path, false) ) {
			env.printError(logger, module.getName(), "Load failed!" );
			return;
		}

		m.executeModule();
		m.merge();

        env.printInfo(logger, module.getName(), "Successfully loaded: \"" + args + "\"");
	}


	}
