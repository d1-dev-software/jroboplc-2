package promauto.jroboplc.plugin.roboplant;

public class Input extends Comp /*implements Comparable<Input>*/ {
	public int refaddr = 0;
	public int refnum = 0;
	public boolean invertor = false;
	public boolean isTagOwner = false;

	public Input() {
	}

	public Input(String name) {
		this.name = name;
	}

	@Override
	public void setAttribute(String attr, String value) {
		switch (attr) {
		case "RefAddr":
			refaddr = Integer.parseInt(value, 16);
			break;
		case "RefNum":
			refnum = Integer.parseInt(value, 16);
			break;
		case "Invertor":
			invertor = value.equals("1");
			break;
		default:
			super.setAttribute(attr, value);
		}
	}

	public boolean isEmpty() {
		return refaddr == 0x7000  &&  refnum == 0;
	}

	public boolean isConst() {
		return refaddr == 0x7000 || refaddr == 0xFFFF;
	}

	public int getInt() {
		if (invertor)
			return (tag.getInt() == 0) ? 1 : 0;
		else
			return tag.getInt();
	}


}
