package promauto.jroboplc.plugin.roboplant;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.Environment;
import promauto.jroboplc.core.api.Module;

public class ConnectManager {
	private final Logger logger = LoggerFactory.getLogger(ConnectManager.class);
	
	public static class State {
		public Map<String,String> deviceModule = new HashMap<>();
		public Set<RoboplantModule> roboplantModules = new HashSet<>();
		public Set<String> externalModuleNames = new HashSet<>();
		public Set<String> notConnectedModuleNames = new HashSet<>();
	}
	
	private State state = new State();
	private State backup = null;
	
	
	private Environment env;
	
	public ConnectManager(Environment env) {
		this.env = env;
	}
	

	public boolean loadConnectors(RoboplantModule _module, Object conf) {
		boolean result = true;
		removeConnectors(_module);
		
		
		Configuration cm = env.getConfiguration();
		Map<String,Object> conf_connect = cm.toMap( cm.get(conf, "connect") );
		for(Map.Entry<String, Object> ent: conf_connect.entrySet() ) {
			String devicename = _module.getName() + ':' + ent.getKey();
			String modulename = ent.getValue().toString();
			
			if( modulename.equals("---") ) {
				state.notConnectedModuleNames.add(devicename);
			} else if( state.externalModuleNames.contains(modulename) ) {
				// show warning
				env.printError(logger, _module.getName(), "Double use of module:", modulename);
				result = false;
			} else
				state.externalModuleNames.add(modulename);
			
			state.deviceModule.put(devicename, modulename);
		}
		
		if( conf_connect.size() > 0  ||  state.notConnectedModuleNames.size() > 0 )
			state.roboplantModules.add(_module);
		
		return result;
	}
	
	public void removeConnectors(RoboplantModule _module) {
		if( state.roboplantModules.contains(_module) ) {
			String modname = _module.getName() + ":";
			state.deviceModule.entrySet().removeIf( entry -> entry.getKey().startsWith(modname) );
			state.notConnectedModuleNames.removeIf( devmodname -> devmodname.startsWith(modname) );
			
			state.externalModuleNames.clear();
			state.externalModuleNames.addAll( state.deviceModule.values() );
		}
	}
	
	public Module getConnectedModule(RoboplantModule module, Device device) {
		String modulename = state.deviceModule.get( module.getName() + ':' + device.devtype + '.' + device.tagname );
		if( modulename == null)
			return null;
		
		return env.getModuleManager().getModule( modulename );
	}
	
	public boolean isNotConnected(RoboplantModule module, Device device) {
		return state.notConnectedModuleNames.contains( 
				module.getName() + ':' + device.devtype + '.' + device.tagname );
	}
	
	public void backupState() {
		backup = new State();
		backup.deviceModule.putAll( 			state.deviceModule );
		backup.externalModuleNames.addAll( 		state.externalModuleNames );
		backup.roboplantModules.addAll( 		state.roboplantModules );
		backup.notConnectedModuleNames.addAll( 	state.notConnectedModuleNames );
	}

	public void restoreState() {
		if( backup == null )
			return;
		
		state.deviceModule.putAll( 				backup.deviceModule );
		state.externalModuleNames.addAll( 		backup.externalModuleNames );
		state.roboplantModules.addAll( 			backup.roboplantModules );
		state.notConnectedModuleNames.addAll( 	backup.notConnectedModuleNames );
		
		resetState();
	}


	public void resetState() {
		backup = null;
	}

	
}





















