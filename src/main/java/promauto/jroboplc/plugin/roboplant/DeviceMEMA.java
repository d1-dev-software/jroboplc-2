package promauto.jroboplc.plugin.roboplant;

import promauto.jroboplc.core.State;
import promauto.jroboplc.core.api.Tag;

import java.util.List;
import java.util.Map;

public class DeviceMEMA extends Device {

	Input inpInput1;
	Input inpInput2;
	Input inpValue;

	Tag tagOutput;
	Tag tagOper;

	int trig;


    @Override
	public void prepareTags(RefBool res) {

		inpInput1  	= getInput("Input1"  	, res);
		inpInput2  	= getInput("Input2"  	, res);
		inpValue  	= getInput("Value"  	, res);

    	tagOutput 	= getOutputTag("Output"	, res);
    	tagOper 	= getOutputTag("Oper"	, res);

		if (res.value)
			resetState(); 
	}


	protected void resetState() {
        tagOutput.setInt(0);
        trig = 0;
	}
	
	
	@Override
	public boolean execute() {
        int oper = tagOper.getInt();

        if( oper == 0 ) {
            if( trig == 0  &&  inpInput1.getInt() >= inpInput2.getInt() ) {
                tagOutput.setInt(inpValue.getInt());
                trig = 1;
            } else

            if( trig == 1  &&  inpInput1.getInt() < inpInput2.getInt() ) {
                trig = 0;
            }
        }
		
		return true;
	}



	@Override
	public void saveStateExtra(State state) {
		state.saveVar("trig",  trig );
	}

	@Override
	public void loadStateExtra(State state) {
        trig = state.loadVar("trig", trig );
	}

}