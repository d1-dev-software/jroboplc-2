package promauto.jroboplc.plugin.roboplant;

import promauto.jroboplc.core.AbstractPlugin;
import promauto.jroboplc.core.api.Module;

public class RoboplantPlugin extends AbstractPlugin {
//	private final Logger logger = LoggerFactory.getLogger(RoboplantPlugin.class);

	private static final String PLUGIN_NAME = "roboplant";
	
	private ConnectManager connectManager;
	

	@Override
	public void initialize() {
//		logger.debug("init: {}", PLUGIN_NAME);
		super.initialize();
		
		connectManager = new ConnectManager(env);
    }

	public ConnectManager getConnectManager() {
		return connectManager;
	}


	@Override
    public String getPluginName() {
    	return PLUGIN_NAME;
    }

	@Override
    public String getPluginDescription(){
    	return "Roboplant logic";
    }
	
//	@Override
//    public Class<?> getModuleClass(){
//    	return RoboplantModule.class;
//    }
	

	@Override
	public Module createModule(String name, Object conf) {
		RoboplantModule m = new RoboplantModule(this, name);
    	modules.add(m);
    	return m.load(conf)? m: null;
	}


	
	
}
