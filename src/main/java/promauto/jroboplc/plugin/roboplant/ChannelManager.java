package promauto.jroboplc.plugin.roboplant;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.Flags;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.TagRWInt;

public class ChannelManager {



	private static class ChannelProvider {
		Device device;
		Tag tagDipNetAddress;
		Integer dipNetAddress = null;
		Channel.Type type;
	}
	
	private List<Channel> channels = new LinkedList<>();
	private List<ChannelProvider> providers = new LinkedList<>();
	private Map<Integer,ChannelProvider> cacheIn = new HashMap<>();
	private Map<Integer,ChannelProvider> cacheOut = new HashMap<>();

	private RoboplantModule roboplantModule;
	private Tag tagEmpty = new TagRWInt("", 0, 0);

	public ChannelManager(RoboplantModule roboplantModule) {
		this.roboplantModule = roboplantModule;
	}


	public void registerChannelProvider(Device device, Tag tagAddr, Channel.Type type) {
		ChannelProvider provider = new ChannelProvider();
		provider.device = device;
		provider.tagDipNetAddress = tagAddr;
		provider.type = type;
		providers.add(provider);
	}
	
	
	public Channel createChannel(Tag tagAddrNum, Channel.Type type) {
		if( tagAddrNum == null )
			return null;
		Channel ch = new Channel();
		ch.tagAddrNum = tagAddrNum;
		ch.classic = tagAddrNum.getType() == Tag.Type.INT;
		ch.type = type;
		channels.add( ch );
		return ch;
	}
	
	
	public void prepare() {
		channels.clear();
		providers.clear();
		cacheIn.clear();
		cacheOut.clear();
	}

	
	private int extractAddr(int addrnum) {
		return (addrnum >> 8) & 0xFF;
	}
	
	
	private int extractNum(int addrnum) {
		return addrnum & 0xFF;
	}
	
	
	private void cache(ChannelProvider provider) {
		provider.dipNetAddress = provider.tagDipNetAddress.getInt();
		if( provider.type == Channel.Type.In  ||  provider.type == Channel.Type.InOut )
			cacheIn.put(provider.dipNetAddress & 0xFF, provider);
		
		if( provider.type == Channel.Type.Out  ||  provider.type == Channel.Type.InOut )
			cacheOut.put(provider.dipNetAddress & 0xFF, provider);
	}
	
	private void uncache(ChannelProvider provider) {
		if( provider.type == Channel.Type.In  ||  provider.type == Channel.Type.InOut )
			cacheIn.remove( provider.dipNetAddress & 0xFF);
		
		if( provider.type == Channel.Type.Out  ||  provider.type == Channel.Type.InOut )
			cacheOut.remove( provider.dipNetAddress & 0xFF);
	}


	
	public void execute() {

		// classic providers
		for(ChannelProvider provider: providers)
			if( provider.dipNetAddress == null ) {
				cache(provider);
			} else 
				if( provider.dipNetAddress != provider.tagDipNetAddress.getInt() ) {
					uncache(provider);
					
					int oldProviderNetAddress = provider.dipNetAddress & 0xFF; 
					int newProviderNetAddress = provider.tagDipNetAddress.getInt() & 0xFF; 
					for(Channel ch: channels) {
						int chaddr = extractAddr(ch.addrnum); 
						if( chaddr == oldProviderNetAddress  ||  chaddr == newProviderNetAddress ) {
							ch.state = Channel.State.NeedBinding;
						}
					}
					
					cache(provider);
					
					for(ChannelProvider p: providers)
						if( p != provider  &&  (p.dipNetAddress & 0xFF) == oldProviderNetAddress ) {
							cache(p);
						}
				}
		
		
		for(Channel ch: channels) {

			// classic channel
			if (ch.classic) {
				if (ch.addrnum != ch.tagAddrNum.getInt()) {
					ch.addrnum = ch.tagAddrNum.getInt();
					ch.state = Channel.State.NeedBinding;
					ch.tagValue = null;
				}

				if (ch.state == Channel.State.NeedBinding) {

					ChannelProvider provider =
							(ch.type == Channel.Type.In) ?
									cacheIn.get(extractAddr(ch.addrnum)) :
									cacheOut.get(extractAddr(ch.addrnum));

					if (provider == null) {
						ch.state = Channel.State.Error;
						continue;
					}

					Output output = provider.device.getOutput(extractNum(ch.addrnum));
					if (output == null) {
						ch.state = Channel.State.Error;
						continue;
					}

					ch.tagValue = output.tag;
					ch.state = Channel.State.Ok;
				}
			}

			// foreign channel
			else {
				if( ch.foreignModuleTagnameText == null
						||  !ch.foreignModuleTagnameText.equals(ch.tagAddrNum.getString()))
				{
					ch.foreignModuleTagnameText = ch.tagAddrNum.getString();

					if( ch.foreignModuleTagnameText.isEmpty() ) {
						ch.state = Channel.State.Ok;
						ch.tagValue = tagEmpty;
					} else {
						int k = ch.foreignModuleTagnameText.indexOf('.');
						if (k < 0) {
							ch.state = Channel.State.Error;
							ch.foreignModule = null;
						} else {
							ch.state = Channel.State.NeedBinding;
							String modname = ch.foreignModuleTagnameText.substring(0, k);
							ch.foreignModule = EnvironmentInst.get().getModuleManager().getModule(modname);
							if (ch.foreignModule == null)
								ch.foreignTagname = ch.foreignModuleTagnameText;
							else
								ch.foreignTagname = ch.foreignModuleTagnameText.substring(k + 1);
						}

						ch.tagValue = null;
					}
				}

				if(ch.state == Channel.State.Ok  &&  ch.tagValue != null  && ch.tagValue.getStatus() == Tag.Status.Deleted) {
					ch.state = Channel.State.NeedBinding;
					ch.tagValue = null;
				}


				if (ch.state == Channel.State.NeedBinding) {
					if( ch.foreignModule == null ) {
						ch.tagValue = EnvironmentInst.get().getModuleManager().searchExternalTag(ch.foreignTagname);
					} else {
						ch.tagValue = ch.foreignModule.getTagTable().get( ch.foreignTagname );
					}

					if (ch.tagValue != null  &&  ch.tagValue.getStatus() == Tag.Status.Deleted)
						ch.tagValue = null;

					if( ch.tagValue == null )
						ch.state = Channel.State.Error;
					else
						ch.state = Channel.State.Ok;

//					System.out.println( "bind: " + ch.foreignModuleTagnameText + " = " + ch.state);
				}
			}

		}
	}


	public void refreshErrorForeignChannels() {
		channels.stream()
				.filter(ch -> !ch.classic  &&  ch.state == Channel.State.Error)
				.forEach( ch -> ch.state = Channel.State.NeedBinding);
	}


}
