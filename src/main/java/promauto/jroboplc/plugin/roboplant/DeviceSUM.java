package promauto.jroboplc.plugin.roboplant;

/**
 * @author Denis Lobanov
 */
public class DeviceSUM extends DeviceSimpleLogic {

	@Override
	public boolean execute() {
		
		int output = 0; 
		for (Input inp: inpInput)
			output += inp.getInt();
		
		tagOutput.setInt(output);
		
		return true;
	}

}