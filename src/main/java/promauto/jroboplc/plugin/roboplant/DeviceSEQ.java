package promauto.jroboplc.plugin.roboplant;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import promauto.jroboplc.core.State;
import promauto.jroboplc.core.api.Tag;

public class DeviceSEQ extends Device {
	Input inpInput;
	Input inpStop1;
	Input inpStop2;
	Input inpReset;
	Input inpSuspend;
	
    Tag tagOutput;
    Tag tagStop1;
    Tag tagStop2;
    Tag tagMode;
    Tag tagCurOut;
    Tag tagCnt;

    int outAmount;
	List<Tag> tagOut  		= new LinkedList<>();
	List<Tag> tagTimeStart  = new LinkedList<>();
	List<Tag> tagTimeStopA  = new LinkedList<>();
	List<Tag> tagTimeStopB  = new LinkedList<>();
	
	int mode;
	int curOut;
	int cnt;
	int output;

	
	@Override
	public void prepareTags(RefBool res) {
		inpInput     	= getInput("Input"	, res);
		inpStop1     	= getInput("Stop1"	, res);
		inpStop2     	= getInput("Stop2"	, res);
		inpReset     	= getInput("Reset"	, res);
		inpSuspend    	= getInput("Suspend", res);

		tagOutput    	= getOutputTag("Output"	, res);
		tagStop1    	= getOutputTag("Stop1"	, res);
		tagStop2    	= getOutputTag("Stop2"	, res);
		tagMode     	= getOutputTag("Mode"	, res);
		tagCurOut    	= getOutputTag("CurOut"	, res);
		tagCnt      	= getOutputTag("Cnt"	, res);
	
    	prepareOutputArrays();
    	
		if (res.value)
			resetState();
	}

	
    private void prepareOutputArrays() {
		tagOut.clear();
		tagTimeStart.clear();
		tagTimeStopA.clear();
		tagTimeStopB.clear();
		
		Output output;
		String s;
    	for (int i=0; i<128; i++) {
    		s = String.format("%03d", i);

    		if ((output = getOutput( "TimeStart" + s )) == null)	break;
    		tagTimeStart.add(output.tag);
    		
    		if ((output = getOutput( "TimeStopA" + s )) == null)	break;
    		tagTimeStopA.add(output.tag);
    		
    		if ((output = getOutput( "TimeStopB" + s )) == null)	break;
    		tagTimeStopB.add(output.tag);
    		
    		if ((output = getOutput( "Out_" + s )) == null)	break;
    		tagOut.add(output.tag);
    	}
    	
    	outAmount = tagOut.size();
	}

	
	protected void resetState() {
        
		tagOutput.setInt(0);  
		tagStop1 .setInt(0);  
		tagStop2 .setInt(0);  
		tagMode  .setInt(0);  
		tagCurOut.setInt(0);  
		tagCnt   .setInt(0);  
		  
		mode = 0;
		curOut = -1;
		cnt = 0;
		output = 0;
	}

	
	

	protected void setZeros() {
	  output = 0;
	  tagStop1	.setInt(0);
	  tagStop2  .setInt(0);
	  cnt = 0;
	}


	/*
	 * Warning! The source code of this method was ported from C++ original
	 * RoboplantMtr sources. Needs get refactored! Do not repeat this style!
	 */
	// @Override
	public boolean execute() {

		boolean tmpInput = inpInput.getInt() > 0; // tag.getBool();
		boolean tmpStop1 = inpStop1.getInt() > 0; // tag.getBool();
		boolean tmpStop2 = inpStop2.getInt() > 0; // tag.getBool();
		boolean tmpReset = inpReset.getInt() > 0; // tag.getBool();
		boolean tmpSuspend = inpSuspend.getInt() > 0; // tag.getBool();

		if (tmpReset) {
			if ((mode == 5) || (mode == 6)) {
				setZeros();
				mode = 0;
			}
		}
		if ((mode == 5) || (mode == 6))
			return true;

		if ((mode != 2) && (tmpInput) && (!tmpStop1) && (!tmpStop2)
				&& (curOut < outAmount) && (!tmpSuspend)) {
			setZeros();
			mode = 2;
			curOut--;
		}

		if ((mode != 7) && (tmpSuspend) && (!tmpStop1) && (!tmpStop2)
				&& (curOut < outAmount) && (curOut >= 0)) {
			mode = 7;
		}

		if ((mode != 3) && (tmpStop1) && (!tmpStop2) && (curOut != -1)) {
			setZeros();
			mode = 3;
			curOut++;
		}

		if ((mode != 4) && (tmpStop2) && (curOut != -1)) {
			setZeros();
			mode = 4;
			curOut++;
		}


		if ((mode == 2) && (tmpInput)) {

			while (true) {
				if (cnt == 0)
					curOut++;
				if (curOut < 0)
					curOut = 0;

				if (curOut < outAmount) {
					if (cnt >= tagTimeStart.get(curOut).getInt()) {
						cnt = 0;
						tagOut.get(curOut).setInt(1);
						continue;
					} else {
						cnt++;
					}
				} else {
					mode = 1;
					output = 1;
				}
				break;
			}

		}

		// Останов
		if ((mode == 3) || (mode == 4)) {
			while (true) {
				if (cnt == 0)
					curOut--;
				if (curOut >= outAmount)
					curOut = outAmount - 1;

				if (curOut > -1) {
					if (cnt >= ((mode == 3) ? tagTimeStopA.get(curOut).getInt()
							: tagTimeStopB.get(curOut).getInt())) {
						cnt = 0;
						tagOut.get(curOut).setInt(0);
						continue;
					} else {
						cnt++;
					}
				} else {
					mode = (mode == 3) ? 5 : 6;
					tagStop1.setBool(mode == 5);
					tagStop2.setBool(mode == 6);
				}
				break;
			}
		}

		tagMode.setInt(mode);
		tagCurOut.setInt(curOut);
		tagCnt.setInt(cnt);
		tagOutput.setInt(output);

		return true;
	}

	@Override
	public void saveStateExtra(State state) {
		state.saveVar("mode",	 	mode   );
		state.saveVar("curOut", 	curOut );
		state.saveVar("cnt", 		cnt);
		state.saveVar("output", 	output);
	}

	@Override
	public void loadStateExtra(State state) {
		mode   	= state.loadVar("mode", 	mode  );
		curOut 	= state.loadVar("curOut", 	curOut);
		cnt 	= state.loadVar("cnt", 		cnt);
		output 	= state.loadVar("output", 	output);
	}

}