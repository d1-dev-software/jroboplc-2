package promauto.jroboplc.plugin.roboplant;

import java.util.Map;

import promauto.jroboplc.core.State;
import promauto.jroboplc.core.api.Tag;

public class DeviceZDVA extends Device {
	Input inpEnable;
	Input inpDoOpen;
	
	Tag tagOutput;
	Tag tagSost;
    Tag tagOpened;
    Tag tagClosed;
    Tag tagControl;
    Tag tagBlok;
    Tag tagFlags;
    Tag tagTimeOut;
//    Tag tagParam2;
//    Tag tagParam3;
    Tag tagDlyAlarm;
    Channel chnl1out;
    Channel chnl2out;
    Channel chnl1in;
    Channel chnl2in;
    Tag tagAlarm;
    
	int sost;
    int almcnt;

    boolean cur_ch1out;
    boolean cur_ch2out;
    int cntChOut;

    boolean cmd_op_old;
    boolean idle_state;

	
	

    @Override
	public void prepareTags(RefBool res) {
		createTagDescr();

    	inpEnable		= getInput("Enable", res);
    	inpDoOpen		= getInput("DoOpen", res);
    	
    	tagOutput   	= getOutputTag(		"Output"	, res);
    	tagSost    		= getOutputTag(		"Sost"		, res);
    	tagOpened   	= getOutputTag(		"Opened"	, res);
    	tagClosed   	= getOutputTag(		"Closed"	, res);
    	tagControl  	= getOutputTag(		"Control"	, res);
    	tagBlok     	= getOutputTag(		"Blok"		, res);
    	tagFlags        = getOutputTag(		"Flags"		, res);
    	tagTimeOut      = getOutputTag(		"TimeOut"	, res);
    	tagDlyAlarm     = getOutputTag(		"DlyAlarm"	, res);
    	chnl1out		= createChannel(	"Chnl1Out"	, Channel.Type.Out, res);
    	chnl2out        = createChannel(	"Chnl2Out"	, Channel.Type.Out, res);
    	chnl1in         = createChannel(	"Chnl1In"	, Channel.Type.In, res);
    	chnl2in         = createChannel(	"Chnl2In"	, Channel.Type.In, res);
    	tagAlarm    	= getOutputTag(		"Alarm"		, res);
    	
		if (res.value)
			resetState(); 
	}


	protected void resetState() {
		tagOutput.setInt(0);
		tagSost.setInt(0); 	
		tagOpened.setInt(0); 	
		tagClosed.setInt(0); 	
		tagAlarm.setInt(0); 	
		
		sost = 0;
	    almcnt = 0;

	    cur_ch1out = false;
	    cur_ch2out = false;
	    cntChOut = 0;

	    cmd_op_old = false;
	    idle_state = false;

	}
	
	
	/* Warning!
	 * Code of this method was ported from C++ original RoboplantMtr sources. 
	 * Needs get refactored! Do not repeat this style!
	 */
	@Override
	public boolean execute() {
		
		boolean ch1out = false;
		boolean ch2out = false;
		boolean ch1in = false;
		boolean ch2in = false;
		boolean ch2in_use;
		boolean ch1out_use;
		boolean ch2out_use;
		boolean cmd_op;

		int flags = tagFlags.getInt();
		int control = tagControl.getInt();

		ch1out_use = (flags & 0xC) != 8;
		ch2out_use = (flags & 0xC) == 0;
		ch2in_use = (flags & 0x30) != 0x20;

		boolean chnl_ok = chnl1in.isOk();

		if (ch1out_use && chnl_ok)
			chnl_ok = chnl1out.isOk();

		if (ch2out_use && chnl_ok)
			chnl_ok = chnl2out.isOk();

		if (ch2in_use && chnl_ok)
			chnl_ok = chnl2in.isOk();

		if (!chnl_ok) {
			tagSost.setInt(8);
			return false;
		}

		// определения управления
		boolean enable = inpEnable.getInt() > 0;

		int ctrl;
		int ctrl1 = control & 0x0F;
		int ctrl2 = (control >> 4) & 0x0F;
		if ((ctrl1 == 0) || ((ctrl2 > 0) && ((flags & 0x40) > 0)))
			ctrl = ctrl2;
		else
			ctrl = ctrl1;

		if (ctrl > 0) {
			cmd_op = ctrl == 3;
		} else {
			cmd_op = (inpDoOpen.getInt() > 0) && enable;
		}

		// Определение положения
		boolean opened = tagOpened.getBool();
		boolean closed = tagClosed.getBool();
		if (tagBlok.getBool()) {
			opened = cmd_op;
			closed = !cmd_op;
		} else {
			ch1in = chnl1in.tagValue.getBool() == ((flags & 0x80) > 0);

			if (ch2in_use)
				ch2in = chnl2in.tagValue.getBool() == ((flags & 0x80) > 0);

			switch (flags & 0x30) {
			case 0x00:
				opened = ch1in;
				closed = ch2in;
				break;
			case 0x10:
				opened = (ch1in) && (ch2in);
				closed = (!ch1in) && (!ch2in);
				break;
			case 0x20:
				opened = ch1in;
				closed = !ch1in;
			}
		}

		ch1out = cmd_op;
		ch2out = !cmd_op;

		if (((flags & 3) < 2) && ((flags & 0xC) == 0)) {
			if (opened)
				ch1out = false;
			if (closed)
				ch2out = false;
		}

		// Enable...
		boolean disableOutput = ctrl == 1;
		if (ctrl < 2) {
			if ((!enable) || disableOutput) {
				ch1out = false;
				ch2out = false;
				cmd_op = false;
			}
		}

		if (tagTimeOut.getBool())
			if ((flags & 3) <= 1) {

				if ((ch1out != cur_ch1out) || (ch2out != cur_ch2out))
					cntChOut = 0;

				cur_ch1out = ch1out;
				cur_ch2out = ch2out;

				if ((!ch1out) && (!ch2out))
					cntChOut = 0;
				else if (cntChOut >= tagTimeOut.getInt()) {
					ch1out = false;
					ch2out = false;
				} else
					cntChOut++;
			}

		// Param2 = cmd_op_old*4 + cmd_op*2 + idle_state;

		// Одиночный импульс до сработки
		if (((flags & 3) == 0) && !disableOutput) {

			if ((!ch1out) && (!ch2out))
				idle_state = true;

			if (cmd_op_old != cmd_op)
				idle_state = false;

			if (idle_state) {
				ch1out = false;
				ch2out = false;
			}
			cmd_op_old = cmd_op;
		}

		// Param3 = cmd_op_old*4 + cmd_op*2 + idle_state;

		boolean output = ((!cmd_op) && (closed)) || ((cmd_op) && (opened));

		boolean alarm = tagAlarm.getBool();
		if ((!output) && (enable)) {
			if (almcnt < tagDlyAlarm.getInt()) {
				alarm = false;
				almcnt++;
			} else
				alarm = true;
		} else {
			alarm = false;
			almcnt = 0;
		}

		// состояние
		tagSost.setInt(((cmd_op ? 1 : 0) << 2) + ((opened ? 1 : 0) << 1)
				+ (closed ? 1 : 0));

		if (ch1out_use)
			chnl1out.tagValue.setBool(ch1out);
		if (ch2out_use)
			chnl2out.tagValue.setBool(ch2out);

		tagOutput.setBool(output);
		tagAlarm.setBool(alarm);
		tagOpened.setBool(opened);
		tagClosed.setBool(closed);

		return true;
	}
	
	
	
	@Override
	public void saveStateExtra(State state) {
		state.saveVar("sost"      , sost       );
		state.saveVar("almcnt"    , almcnt     );
		state.saveVar("cur_ch1out", cur_ch1out );
		state.saveVar("cur_ch2out", cur_ch2out );
		state.saveVar("cntChOut"  , cntChOut   );
		state.saveVar("cmd_op_old", cmd_op_old );
		state.saveVar("idle_state", idle_state );
	}

	@Override
	public void loadStateExtra(State state) {
	    sost       = state.loadVar("sost"      , sost       );
	    almcnt     = state.loadVar("almcnt"    , almcnt     );
	    cur_ch1out = state.loadVar("cur_ch1out", cur_ch1out );
	    cur_ch2out = state.loadVar("cur_ch2out", cur_ch2out );
	    cntChOut   = state.loadVar("cntChOut"  , cntChOut   );
	    cmd_op_old = state.loadVar("cmd_op_old", cmd_op_old );
	    idle_state = state.loadVar("idle_state", idle_state );
	}

	
}