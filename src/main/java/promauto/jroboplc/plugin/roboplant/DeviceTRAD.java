package promauto.jroboplc.plugin.roboplant;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import promauto.jroboplc.core.State;
import promauto.jroboplc.core.api.Tag;

public class DeviceTRAD extends Device {
	Input 	inpMasterIn	;
	Input 	inpTrigSet	;
	Tag 	tagOutput	;
	Tag		tagReady	;
	List<Input> inpEnable = new LinkedList<>();
	
	boolean output;
	boolean ready;
	boolean trigger;
	
    @Override
	public void prepareTags(RefBool res) {

    	inpMasterIn	 = getInput( "MasterIn", res);
    	inpTrigSet	 = getInput( "TrigSet", res);
    	
    	tagOutput	 = getOutputTag( "Output"	, res);
    	tagReady	 = getOutputTag( "Ready"	, res);

		inpEnable.clear(); 
    	for(Input inp: inputs) 
    		if (inp.name.startsWith("Enable")) 
    			inpEnable.add( inp ); 	

		if (res.value) 
			resetState();
	}


	protected void resetState() {
		tagOutput.setInt(0);    
		tagReady.setInt(0);    
		output 	= false; 
		ready 	= false;  
		trigger = false;
	}

	@Override
	public boolean execute() {

		boolean inpensum = true;
		for (Input inp : inpEnable)
			inpensum &= inp.getInt() > 0;

		if (trigger) {
			trigger = inpensum;
		} else {
			if (inpensum)
				trigger = inpTrigSet.getInt() > 0;
		}

		if (trigger) {
			output = inpMasterIn.getInt() > 0;
			ready = false;
		} else {
			output = false;
			ready = inpensum && (inpMasterIn.getInt() > 0);
		}

		tagOutput.setBool(output);
		tagReady.setBool(ready);

		return true;
	}

	
	
	@Override
	public void saveStateExtra(State state) {
		state.saveVar("trigger", 	trigger);
		state.saveVar("output", 	output);
		state.saveVar("ready", 	ready);
	}
	
	@Override
	public void loadStateExtra(State state) {
		trigger  = state.loadVar("trigger", 	trigger);
		output	 = state.loadVar("output", 	output);
		ready	 = state.loadVar("ready", 	ready);
	}

}