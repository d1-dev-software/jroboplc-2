package promauto.jroboplc.plugin.roboplant;

import promauto.jroboplc.core.State;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.TagTable;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


public class Reloader {

	protected RoboplantPlugin plg;
	protected RoboplantModule mod;
	protected RoboplantModule tmp;
	protected State state = new State();
	
	public Reloader(RoboplantModule mod) {
		this.mod = mod;
		this.plg = (RoboplantPlugin)mod.getPlugin();
	}

	public boolean reload() {
		
		saveState();
		plg.getConnectManager().backupState();
		plg.getConnectManager().removeConnectors(mod);
		
		tmp = new RoboplantModule(
				(RoboplantPlugin) mod.getPlugin(),
				mod.getName() );

		Object conf = EnvironmentInst.get().getConfiguration().getModuleConf(
				mod.getPlugin().getPluginName(), 
				mod.getName());
		
		if(conf == null) 
			return false;
		
		if( tmp.load(conf) ) {
			mod.copySettingsFrom(tmp);
			transferTags();
			
			if (tmp.prepare()) {
				mod.channelManager = tmp.channelManager;
				
				assignNewDevices();
	
				loadState();
				
				plg.getConnectManager().resetState();
				
				return true;
			} 
		}
		
		plg.getConnectManager().restoreState();
		tmp = null;
		
		return false;
	}


	
	protected void saveState() {
		for (Device d: mod.devicesByOrder) {
			d.saveState(state);
		}
	}

	
	
	protected void loadState() {
		for (Device d: mod.devicesByOrder) {
			d.loadState(state);
		}
	}

	
	
	protected void transferTags() {
		for (Device d: tmp.devicesByOrder) {
			int constvalue;
			for (Input inp: d.inputs)
				if (inp.isConst()) {
					constvalue = inp.tag.getInt();
					replaceCompTag(inp);
					inp.tag.setInt(constvalue);
				}
				
			for (Output out: d.outputs) {
				replaceCompTag(out);
			}
		}
	}
	
	

	private void replaceCompTag(Comp comp) {
		Tag tag = mod.getTagTable().get( comp.tag.getName() );
		if (tag != null  &&  tag.getType() == comp.tag.getType()) {
			tag.copyFlagsFrom(comp.tag);
			tmp.getTagTable().remove(comp.tag);
			tmp.getTagTable().add(tag);
			comp.tag = tag;
		}
	}

	
	
	protected void assignNewDevices() {
		mod.devicesByAddr = tmp.devicesByAddr;
		mod.devicesByOrder = tmp.devicesByOrder;
		mod.devicesByAddrCount = tmp.devicesByAddrCount;

		Set<String> oldtags = mod.getTagTable().values().stream()
				.map(Tag::getName)
				.collect(Collectors.toSet());

		Set<String> newtags = tmp.getTagTable().values().stream()
				.map(Tag::getName)
				.collect(Collectors.toSet());

		oldtags.removeAll(newtags);
		oldtags.forEach(tagname -> mod.getTagTable().remove(tagname));

		mod.getTagTable().clear();
		tmp.getTagTable().values().forEach(tag -> mod.getTagTable().add(tag));
		tmp.getTagTable().clear();
	}




}





