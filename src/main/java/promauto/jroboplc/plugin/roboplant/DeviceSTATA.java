package promauto.jroboplc.plugin.roboplant;

import promauto.jroboplc.core.State;
import promauto.jroboplc.core.api.Tag;

import java.time.LocalDateTime;
import java.util.Map;

public class DeviceSTATA extends Device {

	protected final static class Time {
		Tag tag;
		long value;
	}


	protected Input inpInput;

	protected Tag tagCount;
	protected Tag tagBegDate;
	protected Tag tagBegTime;
	protected Tag tagTimeTotal;
	protected Tag tagTimeCur;

	protected Tag tagMin;
	protected Tag tagMax;
	protected Tag tagDiv;
	protected Tag tagFlags;


	protected static final int TIMES_SIZE = 200;
	protected Time[] times = new Time[TIMES_SIZE];

	int startdate;
	int starttime;
	long startms;
	long lastms;
	int lastinp;

	

    @Override
	public void prepareTags(RefBool res) {

		inpInput  	= getInput("Input"  	, res);

		tagCount 	= getOutputTag("Count"		, res);
    	tagBegDate 	= getOutputTag("BegDate"	, res);
    	tagBegTime 	= getOutputTag("BegTime"	, res);
		tagTimeTotal= getOutputTag("TimeTotal"	, res);
		tagTimeCur  = getOutputTag("TimeCur"	, res);
    	tagMin 		= getOutputTag("Min"		, res);
		tagMax 		= getOutputTag("Max"		, res);
		tagDiv 		= getOutputTag("Div"		, res);
		tagFlags	= getOutputTag("Flags"		, res);
    	for(int i = 0; i< TIMES_SIZE; ++i) {
    		times[i] = new Time();
			times[i].tag = getOutputTag("Time_" + i, res);
		}


		if (res.value)
			resetState(); 
	}


	protected void resetState() {
		tagCount.setInt(0);
		tagBegDate.setInt(0);
		tagBegTime.setInt(0);
		tagTimeTotal.setInt(0);
		tagTimeCur.setInt(0);
		for(Time time: times) {
			time.tag.setInt(0);
			time.value = 0;
		}

		startms = 0;
		lastms = 0;
		lastinp = 0;
	}
	
	
	@Override
	public boolean execute() {
    	int input = inpInput.getInt();
    	int min = tagMin.getInt();
    	int max = tagMax.getInt();

		if( min < 0 )
			tagMin.setInt( min = 0 );

		if( max >= TIMES_SIZE)
			tagMin.setInt( max = TIMES_SIZE -1 );


		if( lastms != 0 ) {
			// new time
			long curms = System.currentTimeMillis();
			int div = tagDiv.getInt() <= 0? 1000: tagDiv.getInt();

			if( lastinp != input ) {
				times[lastinp].value += curms - lastms;
				lastms = curms;
			} else
				tagTimeCur.setInt( (int)Math.round(((double)(curms - lastms)) / div ) );

			// final
			if( input < min  ||  input > max  ||  (input == min  &&  input < lastinp) ) {

				for (Time time : times) {
					time.tag.setInt( (int)Math.round((double)(time.value) / div ));
					time.value = 0;
				}
				tagBegDate.setInt( startdate );
				tagBegTime.setInt( starttime );
				tagTimeTotal.setInt( (int)Math.round(((double)(lastms - startms)) / div ));
				tagCount.setInt( tagCount.getInt() + 1 );
				tagTimeCur.setInt(0);
				lastms = 0;
			}

		} else {
			// start
			if (input >= min && input <= max) {
				startms = System.currentTimeMillis();
				lastms = startms;
				LocalDateTime ldt = LocalDateTime.now();
				startdate = ldt.getYear()*10000 + ldt.getMonth().getValue()*100 + ldt.getDayOfMonth();
				starttime = ldt.getHour()*10000 + ldt.getMinute()*100 + ldt.getSecond();
			}
		}

		lastinp = input;

		return true;
	}



	@Override
	public void saveStateExtra(State state) {
		state.saveVar("startms"     , this.startms     );
		state.saveVar("lastms"      , this.lastms      );
		state.saveVar("lastinp"     , this.lastinp     );
		state.saveVar("startdate"   , this.startdate   );
		state.saveVar("starttime"   , this.starttime   );
		for(int i=0; i<TIMES_SIZE; ++i)
			state.saveVar("value_" + i, times[i].value );

	}

	@Override
	public void loadStateExtra(State state) {
		this.startms   = state.loadVar("startms"   	 , this.startms    );
		this.lastms    = state.loadVar("lastms"   	 , this.lastms     );
		this.lastinp   = state.loadVar("lastinp"   	 , this.lastinp    );
		this.startdate = state.loadVar("startdate"   , this.startdate  );
		this.starttime = state.loadVar("starttime"   , this.starttime  );
		for(int i=0; i<TIMES_SIZE; ++i)
			times[i].value = state.loadVar("value_" + i, times[i].value );
	}

}