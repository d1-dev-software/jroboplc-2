package promauto.jroboplc.plugin.roboplant;

import promauto.jroboplc.core.api.Tag;

public class DeviceOPER extends Device {
	Input 	inpInput1;
	Input 	inpInput2;
	Tag 	tagOutput;
	Tag 	tagOper;
	
	
    @Override
	public void prepareTags(RefBool res) {

    	inpInput1 = getInput( "Input1", res);
    	inpInput2 = getInput( "Input2", res);
    	
    	tagOutput	 = getOutputTag( "Output"		, res);
    	tagOper		 = getOutputTag( "Oper"			, res);

		if (res.value) 
			resetState();
	}


	protected void resetState() {
		tagOutput.setInt(0);    
	}

	@Override
	public boolean execute() {

		int inp1 = inpInput1.getInt();
		int inp2 = inpInput2.getInt();
		int value = 0;

		switch (tagOper.getInt()) {

		case 0:
			value = (inp1 == inp2) ? 1 : 0;
			break; // cmp =
		case 1:
			value = (inp1 >= inp2) ? 1 : 0;
			break; // cmp >=
		case 2:
			value = (inp1 <= inp2) ? 1 : 0;
			break; // cmp <=
		case 3:
			value = (inp1 > inp2) ? 1 : 0;
			break; // cmp >
		case 4:
			value = (inp1 < inp2) ? 1 : 0;
			break; // cmp <
		case 5:
			value = inp1 + inp2;
			break; // sum
		case 6:
			value = ((inp1 < inp2) ? 0 : (inp1 - inp2));
			break; // sub
		case 7:
			value = inp1 ^ inp2;
			break; // xor
		case 8:
			value = inp1 * inp2;
			break; // mul
		case 9:
			value = ((inp2 == 0) ? (0) : (inp1 / inp2));
			break; // div
		case 10:
			value = ((inp2 == 0) ? (0) : (inp1 % inp2));
			break; // mod
		case 11:
			value = inp1 & inp2;
			break; // and
		case 12:
			value = inp1 | inp2;
			break; // or
		case 13:
			value = inp1 * inp2 / 100;
			break; // %
		case 14:
			value = ((inp1 & inp2) == inp2) ? 1 : 0;
			break; 
		}

		tagOutput.setInt(value);

		return true;
	}

}