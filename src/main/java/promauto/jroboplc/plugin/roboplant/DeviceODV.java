package promauto.jroboplc.plugin.roboplant;

import java.util.Map;

import promauto.jroboplc.core.State;
import promauto.jroboplc.core.api.Tag;

public class DeviceODV extends Device {
	Input 	inpInput;
	Tag 	tagOutput;
	Tag 	tagTime;
	Tag		tagCnt;
	
	int cnt;
	boolean output;
	
    @Override
	public void prepareTags(RefBool res) {

    	inpInput = getInput( "Input", res);
    	
    	tagOutput	 = getOutputTag( "Output"		, res);
    	tagTime		 = getOutputTag( "Time"			, res);
    	tagCnt		 = getOutputTag( "Cnt"			, res);

		if (res.value) 
			resetState();
	}


	protected void resetState() {
		tagOutput.setInt(0);    
		cnt = 0;   
		output = false;
	}

	@Override
	public boolean execute() {

		if (output) {
			cnt++;
			if (cnt >= tagTime.getInt())
				output = false;
		}

		if (!output) {
			if (inpInput.getInt() > 0)
				output = (cnt == 0);
			else
				cnt = 0;
		}

		tagOutput.setBool(output);
		tagCnt.setInt(cnt);

		return true;
	}

	
	
	@Override
	public void saveStateExtra(State state) {
		state.saveVar("cnt", 		cnt);
		state.saveVar("output", 	output);
	}
	
	@Override
	public void loadStateExtra(State state) {
		cnt    = state.loadVar("cnt", 		cnt);
		output = state.loadVar("output", 	output);
	}

}