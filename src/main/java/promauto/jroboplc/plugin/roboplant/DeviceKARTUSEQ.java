package promauto.jroboplc.plugin.roboplant;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.Tag;

public class DeviceKARTUSEQ extends Device {
	/**
	 * Допустимы состояния: STOPED, FILLTODVU - последовательное от 1 до число используемых входов (макс 8) заполение
	 * FILLTOPDP - заполнение до подпора,если все ДВУ уже заполнены и CONTROL.b0 = 1, порядок обратный FILLTODVU
	 * NEXTSEQ - если используется CONTROL.b0 = 1 - по заполнению всех ДВУ
	 * @author Crazy UVELKA
	 * @version 1.0
	 */
	//  consts, enums and so on
	private static int 	 KART_SEQ_LNG = 8;
	private static int 	 KART_SEQ_NOOUT = -1;

	//~ consts, enums and so on

	Input inpEnable;
	//Input inpNextSEQFin;
	Input tagTransportOk;

	List<Input> inpWork	 	= new ArrayList<>(KART_SEQ_LNG);
	List<Input> inpClean	= new ArrayList<>(KART_SEQ_LNG);
	List<Input> inpDVU	 	= new ArrayList<>(KART_SEQ_LNG);
	List<Input> inpPDP	 	= new ArrayList<>(KART_SEQ_LNG);
	List<Input> inpZDVOK 	= new ArrayList<>(KART_SEQ_LNG);
	List<Tag> Outs 			= new ArrayList<>(KART_SEQ_LNG);
	List<Tag> MaxTime 		= new ArrayList<>(KART_SEQ_LNG);

	Tag tagFlow;
	Tag tagState;
	Tag tagNextSeq;
	Tag tagConrtol;
	Tag tagMaxNumber;



	// helpers
	int			iCurOutput; 		// current product consumer
	int			iMaxOutputs;		// total consumers
	int			iLastOutput;		// consumer of the not fallen product



	//~ helpers
	
    @Override
	public void prepareTags(RefBool res) {
		iCurOutput		= KART_SEQ_NOOUT;
		iLastOutput		= KART_SEQ_NOOUT;

	    inpEnable  			= getInput(	"Enable", res);
		tagTransportOk		= getInput( "TransportOk", res);


		for(Input inp: inputs){

			boolean bFound = false;

			for(int pos=1; pos <= KART_SEQ_LNG; pos++) {
				if (inp.name.compareTo("Work" + pos) == 0) {
					inpWork.add(inp);
					bFound = true;
				}
				if (inp.name.compareTo("Clean" + pos) == 0) {
					inpClean.add(inp);
					bFound = true;
				}
				if (inp.name.compareTo("DVU" + pos) == 0) {
					inpDVU.add(inp);
					bFound = true;
				}
				if (inp.name.compareTo("PDP" + pos) == 0) {
					inpPDP.add(inp);
					bFound = true;
				}
				if (inp.name.compareTo("ZDVOK" + pos) == 0) {
					inpZDVOK.add(inp);
					bFound = true;
				}
				if(bFound) break;
			}

		};

		for(int i=1; i <= KART_SEQ_LNG; i++){
			Outs.add(i-1,getOutputTag( "_Out"+i, res));
			MaxTime.add(i-1,getOutputTag( "Time"+i, res));
		}

		tagConrtol	    = getOutputTag( "Control", res);

		tagNextSeq	    = getOutputTag( "NextSeq", res);
		tagMaxNumber 	= getOutputTag( "MaxNumb", res);
		tagFlow         = getOutputTag( "Flow", res);


		tagState    	= getOutputTag( "State", res);
		if (res.value)
			resetState(); 
	}

	protected void resetState() {
	    tagState.setInt(0);
	}

    
	@Override
	public boolean execute() {
		boolean bEnable = inpEnable.getInt() == 1;
		if(!bEnable){
			turnOffOuts();
		}else {
			boolean bFillTransportOk = tagTransportOk.getInt() == 1;
			iMaxOutputs = tagMaxNumber.getInt() == 0 ? KART_SEQ_LNG : tagMaxNumber.getInt() ;
			//~last processing
			int tmpLast = iLastOutput;
			iLastOutput = getLastToFill(iMaxOutputs);
			if(tmpLast != iLastOutput) {
				turnOffOut(tmpLast);
			}
			//~last processing

			if(canMakeFlow(iCurOutput)){
				if(bFillTransportOk)
					turnOnOut(iCurOutput);
				else
					if(iCurOutput != iLastOutput) turnOffOut(iCurOutput);
				turnOnOut(iLastOutput);
			}else {
				int iLastCnt = iLastOutput == KART_SEQ_NOOUT ? iMaxOutputs : iLastOutput;
				boolean bFound = false;
				//checking if some can work next to it
				for (int i = 0; i <= iLastCnt; i++) {
					if (canMakeFlow(i)) {
						if(bFillTransportOk) switchOuts(i);
						else
							turnOffOutsAndSwap(i);
						bFound = true;
						break;
					}
				}

				// we are here so nonone can work in flow
				// looking for someone who can work without flow
				if (!bFound)
					bFound = canJustWork(iCurOutput) && bFillTransportOk;

				if (!bFound) {
					for (int i = 0; i <= iLastCnt; i++) {
						if (canJustWork(i)) {
							if(bFillTransportOk) switchOuts(i);
							else
								turnOffOutsAndSwap(i);
							bFound = true;
							break;
						}
					}
				}

				if(! bFound) {
					turnOffOuts();
					turnOnOut(iLastOutput);
				}
			}
		}
		if(iCurOutput == iLastOutput){
			tagFlow.setInt(canMakeFlow(iLastOutput) ? 1 : 0);
		}else
			tagFlow.setInt(((canMakeFlow(iCurOutput) && canJustWork(iLastOutput))) ? 1 : 0);
		return finish();
	}

	private void switchOuts(int i){
		turnOffOut(iCurOutput);
		iCurOutput = i;
		turnOnOut(iCurOutput);
	}

	private void turnOffOutsAndSwap(int i){
		turnOffOut(iCurOutput);
		iCurOutput = i;
	}

	private int getLastToFill(int iMaxOuts){
    	iMaxOuts -=1;
		for (int i = iMaxOuts; i >= iCurOutput; i--)
			if (canUseDirection(i)) return i;
    	return KART_SEQ_NOOUT;
	}

    private boolean canMakeFlow(int curPos){
		if ((curPos < 0) || (curPos >=inpPDP.size())) return false;
		return (((inpWork.get(curPos).getInt() + inpClean.get(curPos).getInt()) > 0) && (inpDVU.get(curPos).getInt() == 0) && (inpPDP.get(curPos).getInt() == 0));
	}

	private boolean canJustWork(int curPos){
		if ((curPos < 0) || (curPos >=inpPDP.size())) return false;
		return (((inpWork.get(curPos).getInt() + inpClean.get(curPos).getInt()) > 0) && (inpPDP.get(curPos).getInt() == 0));
	}

	private boolean canUseDirection(int curPos){
		if ((curPos < 0) || (curPos >=inpPDP.size())) return false;
		return (((inpWork.get(curPos).getInt() + inpClean.get(curPos).getInt()) > 0));
	}



	private void turnOnOut(int iOut){
		if(iOut<0)return;
    	Outs.get(iOut).setInt(1);
	}



	private void turnOffOut(int iOut){
		if(iOut<0)return;
		Outs.get(iOut).setInt(0);
	}

	private void turnOffOuts(){
		iCurOutput = KART_SEQ_NOOUT;
    	for(Tag tgOut: Outs){
    		tgOut.setInt(0);
		}
		tagState.setInt(0);
	}


	
	private boolean finish() {
		return true;
	}

	private void setBool(Tag tag, boolean value) {
		if(tag.getBool() != value )
			tag.setBool(value);
	}

	private void setInt(Tag tag, int value) {
		if(tag.getInt() != value )
			tag.setInt(value);
	}


	// operative sequencer, no need to save load states, each time only inputs
//	@Override
//	public void saveState(Map<String, Integer> state) {
//		super.saveState(state);
//	}
//
//	@Override
//	public void loadState(Map<String, Integer> state) {
//		super.loadState(state);
//	}

}
