package promauto.jroboplc.plugin.roboplant;

import promauto.jroboplc.core.api.Module;
import promauto.jroboplc.core.api.Tag;

public class Channel {


	public enum Type {
		In,
		Out,
		InOut
	}
	
	public enum State {
		Ok,
		Error,
		NeedBinding
	}
	
	public Type type;
	public Tag tagValue = null;
	public State state = State.NeedBinding;


	//TODO to be refactored: split to separate classes
	public Tag tagAddrNum;
	public int addrnum = 0;
	public boolean classic = true;

	public String foreignModuleTagnameText = null;
	public Module foreignModule = null;
	public String foreignTagname = null;


	public boolean isOk() {
		return state == State.Ok;
	}


}
