package promauto.jroboplc.plugin.roboplant;

import java.util.Map;

import promauto.jroboplc.core.State;
import promauto.jroboplc.core.api.Tag;

public class DeviceGNR extends Device {
	Input 	inpEnabled;
	Tag 	tagOutput	;
	Tag 	tagLowTime	;
	Tag 	tagHighTime	;
	Tag 	tagCnt		;
	
	int cnt;
	int output;
	
    @Override
	public void prepareTags(RefBool res) {

    	inpEnabled = getInput( "Enabled", res);
    	
    	tagOutput	 = getOutputTag( "Output"		, res);
    	tagLowTime	 = getOutputTag( "LowTime"		, res);
    	tagHighTime	 = getOutputTag( "HighTime"		, res);
    	tagCnt		 = getOutputTag( "Cnt"			, res);

		if (res.value) 
			resetState();
	}


	protected void resetState() {
		tagOutput.setInt(0);    
		cnt = 0;   
		output = 0;
	}

	@Override
	public boolean execute() {

		if (inpEnabled.getInt() > 0) {
			cnt++;
			if (output > 0) {
				if (cnt >= tagHighTime.getInt()) {
					cnt = 0;
					output = 0;
				}
			} else {
				if (cnt >= tagLowTime.getInt()) {
					cnt = 0;
					output = 1;
				}
			}

		} else {
			output = 0;
			cnt = 0;
		}

		tagOutput.setInt(output);
		tagCnt.setInt(cnt);

		return true;
	}

	
	
	@Override
	public void saveStateExtra(State state) {
		state.saveVar("cnt", 		cnt);
		state.saveVar("output", 	output);
	}
	
	@Override
	public void loadStateExtra(State state) {
		cnt    = state.loadVar("cnt", 		cnt);
		output = state.loadVar("output", 	output);
	}

}