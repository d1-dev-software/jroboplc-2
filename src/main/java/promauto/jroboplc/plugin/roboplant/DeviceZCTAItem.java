package promauto.jroboplc.plugin.roboplant;

import promauto.jroboplc.core.api.Tag;

public class DeviceZCTAItem {
    int state;
    int address;
    Tag tagAddress;
    Tag tagMode;
    private Tag tagOutput;
    private Tag tagControl;

    public DeviceZCTAItem() {
        state = 0;
        address = -1;
        tagOutput = null;
        tagControl = null;
    }

    public void setOutput(Tag output) {
        this.tagOutput = output;
    }

    public void setControl(Tag control) {
        this.tagControl = control;
    }

    public int getOutputValue() {
        return (tagOutput==null)? 0: tagOutput.getInt();
    }

    public int getControlValue() {
        return (tagControl==null)? 0: tagControl.getInt();
    }

    public void setControlValue(int value) {
        if (tagControl!=null)
            tagControl.setInt(value);
    }

    public void reset() {
        tagOutput = null;
        tagControl = null;
    }
}
