package promauto.jroboplc.plugin.roboplant;

public class DevicePLCI extends ConnectDevice {
	
    @Override
	public void prepareTags(RefBool res) {
    	
    	for(int i=0; i<64; ++i)
    		connector.connect( 
    				"Channel_" + i, 
    				(i<10? "inp0": "inp") + i, 
    				Connector.Mode.READ, res);
    	
    	connector.connect("ErrorFlag", "SYSTEM.ErrorFlag", Connector.Mode.READ, res);
    	connector.connect("ErrorCounter", "SYSTEM.ErrorCount", Connector.Mode.READ, res);
    	
    	registerAsChannelProvider( "DipNetAddress", Channel.Type.In, res);
    }

    
}