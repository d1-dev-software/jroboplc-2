package promauto.jroboplc.plugin.roboplant;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import promauto.jroboplc.core.api.Tag;

public class DeviceSYS extends Device {
	Tag 	tagDateYear;
	Tag 	tagDateMonth;
	Tag 	tagDateDay;

	
	
    @Override
	public void prepareTags(RefBool res) {

    	tagDateYear 	= getOutputTag( "Date.Year", res);
    	tagDateMonth 	= getOutputTag( "Date.Month", res);
    	tagDateDay 		= getOutputTag( "Date.Day", res);

		if (res.value) 
			resetState();
	}


	protected void resetState() {
		tagDateYear.setInt(0);
		tagDateMonth.setInt(0);
		tagDateDay.setInt(0);
	}

	@Override
	public boolean execute() {
		int date = Integer.parseInt( LocalDate.now().format( DateTimeFormatter.BASIC_ISO_DATE ));
		
		tagDateYear.setInt( date / 10000);
		date %= 10000;
		tagDateMonth.setInt( date / 100);
		date %= 100;
		tagDateDay.setInt( date );

		return true;
	}
}