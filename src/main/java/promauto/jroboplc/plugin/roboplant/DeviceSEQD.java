package promauto.jroboplc.plugin.roboplant;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import promauto.jroboplc.core.State;
import promauto.jroboplc.core.api.Tag;

public class DeviceSEQD extends Device {
	
	public static class SeqdInput {
		Input input;
		int value;
		
		public SeqdInput(Input input) {
			this.input = input;
			this.value = 0;
		}
	}
	
	Input inpInput;
	
	List<SeqdInput> inpStartBan 	= new ArrayList<>();
	List<SeqdInput> inpStop 		= new ArrayList<>();
	List<SeqdInput> inpStopStart 	= new ArrayList<>();
	List<SeqdInput> inpStopRun 		= new ArrayList<>();
	List<SeqdInput> inpStopFlow 	= new ArrayList<>();
	List<SeqdInput> inpStopDly 		= new ArrayList<>();
	List<SeqdInput> inpSusp 		= new ArrayList<>();

	Tag tagBtnStart;
	Tag tagBtnStop;
	Tag tagBtnSusp;
	Tag tagBtnEmrg;
	Tag tagBtnCancel;
	Tag tagTimeBtnStart;
	Tag tagFlags;
	Tag tagExclStartBan; 
	Tag tagExclStop; 	
	Tag tagExclStopStart;
	Tag tagExclStopRun; 	
	Tag tagExclStopFlow;
	Tag tagExclStopDly; 	
	Tag tagExclSusp; 	
	Tag tagOutputOn;
	Tag tagOutputRun;
	Tag tagSost;
	Tag tagCounter;
	Tag tagMesReady;
	Tag tagMesAutoStop;
	Tag tagStopFlow;
	Tag tagStopDlyCnt;
	
	List<Tag> tagStopDlyTime 		= new ArrayList<>();
	List<Tag> tagMesStartBan 		= new ArrayList<>();
	List<Tag> tagMesStop 			= new ArrayList<>();
	List<Tag> tagMesStopStart		= new ArrayList<>();
	List<Tag> tagMesStopRun 		= new ArrayList<>();
	List<Tag> tagMesStopFlow 		= new ArrayList<>();
	List<Tag> tagMesStopDly 		= new ArrayList<>();
	List<Tag> tagMesSusp 			= new ArrayList<>();
	List<Tag> tagTimeStart 			= new ArrayList<>();
	List<Tag> tagTimeStop 			= new ArrayList<>();
	List<Tag> tagOut	 			= new ArrayList<>();

	int SEQD_INP_AMOUNT;
	int outAmount;
    int	curOut;                                
    int cnt;
    int cntBtnStart;                              
    int stopDlyCancel;                              
    int sost;                              

	
    @Override
	public void prepareTags(RefBool res) {
		createTagDescr();

    	inpInput     	= getInput(		"Input"			, res);

		tagBtnStart    	= getOutputTag(	"BtnStart"  	, res);
		tagBtnStop     	= getOutputTag(	"BtnStop"   	, res);
		tagBtnSusp     	= getOutputTag(	"BtnSusp"   	, res);
		tagBtnEmrg     	= getOutputTag(	"BtnEmrg"   	, res);
		tagBtnCancel    = getOutputTag(	"BtnCancel"   	, res);
		tagTimeBtnStart	= getOutputTag(	"TimeBtnStart"  , res);
		tagFlags      	= getOutputTag(	"Flags"   		, res);
		tagExclStartBan	= getOutputTag(	"ExclStartBan"  , res);
		tagExclStop    	= getOutputTag(	"ExclStop"   	, res);
		tagExclStopStart= getOutputTag(	"ExclStopStart" , res);
		tagExclStopRun	= getOutputTag(	"ExclStopRun"	, res);
		tagExclStopFlow	= getOutputTag(	"ExclStopFlow"	, res);
		tagExclStopDly 	= getOutputTag(	"ExclStopDly"	, res);
		tagExclSusp   	= getOutputTag(	"ExclSusp"   	, res);
		tagOutputOn    	= getOutputTag(	"OutputOn"   	, res);
		tagOutputRun  	= getOutputTag(	"OutputRun"  	, res);     
		tagSost       	= getOutputTag(	"Sost"       	, res);    
		tagCounter    	= getOutputTag(	"Counter"    	, res);    
		tagMesReady   	= getOutputTag(	"MesReady"   	, res);      
		tagMesAutoStop	= getOutputTag(	"MesAutoStop"	, res);  
		tagStopFlow   	= getOutputTag(	"StopFlow"   	, res);  
		tagStopDlyCnt 	= getOutputTag(	"StopDlyCnt" 	, res);

		prepareInputArrays();

		prepareOutputArrays();
		
		if (res.value) 
			resetState();
	}


	private void prepareInputArrays() {
		inpStartBan.clear(); 
		inpStop.clear(); 	
		inpStopStart.clear();
		inpStopRun.clear(); 	
		inpStopFlow.clear(); 
		inpStopDly.clear(); 	
		inpSusp.clear(); 	
		
		String s;
		Pattern p = Pattern.compile("\\d*");
    	for(Input inp: inputs) {
    		s = p.matcher(inp.name).replaceAll("");
    		switch (s) {
    		case "StartBan"	: inpStartBan.add( 	new SeqdInput(inp)  ); break;
    		case "Stop"		: inpStop.add(		new SeqdInput(inp)  ); break;
    		case "StopStart": inpStopStart.add(	new SeqdInput(inp)  ); break;
    		case "StopRun"	: inpStopRun.add(	new SeqdInput(inp)  ); break;
    		case "StopFlow"	: inpStopFlow.add(	new SeqdInput(inp)  ); break;
    		case "StopDly"	: inpStopDly.add(	new SeqdInput(inp)  ); break;
    		case "Susp"		: inpSusp.add(		new SeqdInput(inp)  ); break;
    		}
    	}
	}


	
	private void prepareOutputArrays() {
		tagStopDlyTime.clear(); 	
		tagMesStartBan.clear(); 	
		tagMesStop.clear(); 		
		tagMesStopStart.clear();	
		tagMesStopRun.clear(); 	
		tagMesStopFlow.clear(); 	
		tagMesStopDly.clear(); 	
		tagMesSusp.clear(); 		
		tagTimeStart.clear(); 		
		tagTimeStop.clear(); 		
		tagOut.clear();	 		
		
		String s;
		Pattern p = Pattern.compile("\\d*");
    	for(Output out: outputs) {
    		s = p.matcher(out.name).replaceAll("");
    		switch (s) {
    		case "StopTime"		: tagStopDlyTime.add(out.tag); break;
    		case "MesStartBan"	: tagMesStartBan.add(out.tag); break;
    		case "MesStop"		: tagMesStop.add(out.tag); break;
    		case "MesStopStart"	: tagMesStopStart.add(out.tag); break;
    		case "MesStopRun"	: tagMesStopRun.add(out.tag); break;
    		case "MesStopFlow"	: tagMesStopFlow.add(out.tag); break;
    		case "MesStopDly"	: tagMesStopDly.add(out.tag); break;
    		case "MesSusp"		: tagMesSusp.add(out.tag); break;
    		                                     
    		case "TimeStart"	: tagTimeStart.add(out.tag); break;
    		case "TimeStop"		: tagTimeStop.add(out.tag); break;
    		case "Out"			: tagOut.add(out.tag); break;
    		}
    	}
    	
    	outAmount = Math.min( 
						Math.min(tagOut.size(), tagTimeStart.size()), 
						tagTimeStop.size());
    	
    	SEQD_INP_AMOUNT = inpStartBan.size();
    			
	}

	
	protected void resetState() {
		tagOutputOn.setInt(0);   
		tagOutputRun.setInt(0);  
		tagSost.setInt(0);       
		tagCounter.setInt(0);    
		tagMesReady.setInt(0);   
		tagMesAutoStop.setInt(0);
		tagStopFlow.setInt(0);   
		tagStopDlyCnt.setInt(0); 

		curOut = -1;                                
	    cnt = 0;
	    cntBtnStart = 0;                              
	    stopDlyCancel = 0;                              
	    sost = 0;                              
		
	}
	
	
	/*
	 * Warning! This source code of this method was ported from C++ original
	 * RoboplantMtr sources. Needs get refactored! Do not repeat this style!
	 */
	// @Override
	public boolean execute() {

	  // Sost
	  // 0 - off
	  // 1 - Start ban wait
	  // 2 - Start ready
	  // 3 - starting
	  // 4 - running
	  // 5 - stopping
	  // 6 - suspended

//		int sost = tagSost.getInt();
		int flags = tagFlags.getInt();
		int stopDlyCnt = tagStopDlyCnt.getInt();

		// inputs
		int input = inpInput.getInt();
	
		int startBan  = evalSeqdInput(inpStartBan,  tagExclStartBan.getInt(),  sost>0 && sost<4);
		int stop      = evalSeqdInput(inpStop,      tagExclStop.getInt(),      sost==3 || sost==4 || sost==6);
		int stopStart = evalSeqdInput(inpStopStart, tagExclStopStart.getInt(), sost==3 || sost==6);
		int stopRun   = evalSeqdInput(inpStopRun,   tagExclStopRun.getInt(),   sost==4);
		int stopFlow  = evalSeqdInput(inpStopFlow,  tagExclStopFlow.getInt(),  true); //sost==4);
		int stopDly   = evalSeqdInput(inpStopDly,   tagExclStopDly.getInt(),   sost==4 );
		int susp      = evalSeqdInput(inpSusp,      tagExclSusp.getInt(),      sost==3  || ((sost==6) && ((flags&1)==1)) );


		// stop with delay
		int stopDlyEvent = 0;
		if (stopDly > 0) {

			if (stopDlyCnt > 0) {

				if (stopDlyCancel != stopDly) {
					int mask = 1;
					for (int i = 0; i < SEQD_INP_AMOUNT; i++) {
						if ((stopDly & mask) > 0)
							if (stopDlyCnt > tagStopDlyTime.get(i).getInt())
								stopDlyCnt = tagStopDlyTime.get(i).getInt();

						mask = mask << 1;
					}

					if (stopDlyCnt > 0)
						stopDlyCnt--;
				}

				if (tagBtnCancel.getBool()) {
					stopDlyCancel = stopDly;
					stopDlyCnt = 0xFFFF;
					tagBtnCancel.setInt(0);
				}

			} else
				stopDlyEvent = 1;

		} else {
			stopDlyCnt = 0xFFFF;
			stopDlyCancel = 0;
		}

		// start button
		int btnStartEvent = 0;
		if (tagBtnStart.getBool()) {
			if (cntBtnStart >= tagTimeBtnStart.getInt()) {
				tagBtnStart.setInt(0);
				sost = 2;
				cntBtnStart = 0;
				btnStartEvent = 1;
			} else
				cntBtnStart++;
		} else {
			cntBtnStart = 0;
		}

		// emrg button
		if (tagBtnEmrg.getBool()  ||  ((flags&2)>0) &&  (inpStop.get(inpStop.size()-1).value>0) ) {
			sost = 0;
			btnStartEvent = 0;
			cnt = 0;
			curOut = -1;
		} else {

			// State 0 - off
			if (sost == 0) {
				if ((btnStartEvent > 0) || (input > 0)) {
					startBan = evalSeqdInput(inpStartBan, tagExclStartBan.getInt(), true);
					if (startBan > 0)
						sost = 1;
					else
						sost = 3;
				}
				curOut = -1;
			}
		}

		// State 1 - start ban wait
		if (sost == 1) {
			if (tagBtnStop.getBool())
				sost = 0;
			else if (startBan == 0)
				sost = 2;
		}

		// State 2 - start ready
		if (sost == 2) {
			startBan = evalSeqdInput(inpStartBan, tagExclStartBan.getInt(),	true);
			if (tagBtnStop.getBool())
				sost = 0;
			else if (startBan > 0)
				sost = 1;
			else if (btnStartEvent > 0)
				sost = 3;
		}

		// State 3 - starting
		if (sost == 3) {
			if ((stop > 0) || (stopStart > 0))
				tagMesAutoStop.setInt(1);

			if (tagBtnStop.getBool() || (stop > 0) || (stopStart > 0))
				sost = 5;
			else if (tagBtnSusp.getBool() || (susp > 0))
				sost = 6;
			else if (curOut >= outAmount)
				sost = 4;
			else
				evalStartCounter();
		}

		// State 4 - running
		if (sost == 4) {
			if ((stop > 0) || (stopRun > 0) || (stopDlyEvent > 0))
				tagMesAutoStop.setInt(1);

			if (tagBtnStop.getBool() || (stop > 0) || (stopRun > 0)
					|| (stopDlyEvent > 0))
				sost = 5;
		}

		// State 5 - stopping
		if (sost == 5) {
			if (curOut == -1) {
				sost = 0;
				tagMesAutoStop.setInt(0);
			} else
				evalStopCounter();
		}

		// State 6 - suspended
		if (sost == 6) {
			if (tagBtnStop.getBool() || (stop > 0) || (stopStart > 0))
				sost = 5;
			else if( (flags&1) == 0? 
						btnStartEvent > 0 :  
						(!tagBtnSusp.getBool()) && susp == 0 )
				sost = 3;
		}

		tagBtnStop.setInt(0);
		tagBtnEmrg.setInt(0);
		if( (flags & 1) == 0 )
			tagBtnSusp.setInt(0);
		
		tagOutputOn.setBool(sost > 0);
		tagOutputRun.setBool(sost == 4);
		tagMesReady.setBool(sost == 2);
		tagStopFlow.setBool(stopFlow > 0);

		tagSost.setInt(sost);
		tagStopDlyCnt.setInt(stopDlyCnt);
		tagCounter.setInt(cnt);
		
		
		for (int i=0; i<outAmount; i++)
			tagOut.get(i).setBool( curOut >= i);

		for (int i=0; i<SEQD_INP_AMOUNT; i++) {
			tagMesStartBan.get(i).setInt(inpStartBan.get(i).value);
			tagMesStop.get(i).setInt(inpStop.get(i).value);
			tagMesStopDly.get(i).setInt(inpStopDly.get(i).value);
			tagMesStopFlow.get(i).setInt(inpStopFlow.get(i).value);
			tagMesStopRun.get(i).setInt(inpStopRun.get(i).value);
			tagMesStopStart.get(i).setInt(inpStopStart.get(i).value);
			tagMesSusp.get(i).setBool( inpSusp.get(i).input.getInt() > 0 && (sost==3 || sost==6) );
		}
			
		return true;
	}

	
	


	protected int evalSeqdInput(List<SeqdInput> si, int excl, boolean enable) {
		int i;

		if (enable) {
			int mask1 = 0x0001;
			int mask2 = 0x0100;
			int inpsum = 0;

			for (i = 0; i < SEQD_INP_AMOUNT; i++) {
				
				si.get(i).value = 
						((excl & mask1) > 0) ? 0 : si.get(i).input.getInt();
				
				inpsum += 
						(((excl & mask2) > 0) || (si.get(i).value == 0)) ? 0: mask1;
				
				mask1 = mask1 << 1;
				mask2 = mask2 << 1;
			}
			return inpsum;
		} else {
			for (i = 0; i < SEQD_INP_AMOUNT; i++)
				si.get(i).value = 0;
			return 0;
		}

	}







	protected void evalStartCounter() {
		//  CurOut    FFFF  0  1  2  3  4  5  6  OutAmount
		//	           .    .  .  .  .  .  .  .     .
		if (curOut < 0) {
			curOut = 0;
			cnt = 0;
		}

		while (curOut < outAmount) 
			if (cnt++ >= tagTimeStart.get(curOut).getInt() ) {
				curOut++;
				cnt = 0;
			} else
				break;
	}

	


	protected void evalStopCounter() {
		if (curOut >= outAmount) {
			curOut = outAmount - 1;
			cnt = 0;
		}

		while (curOut >= 0) 
			if (cnt++ >= tagTimeStop.get(curOut).getInt()) {
				curOut--;
				cnt = 0;
			} else
				break;
	}
	

	
	@Override
	public void saveStateExtra(State state) {
		state.saveVar("curOut"       , curOut        );
		state.saveVar("cnt"          , cnt           );
		state.saveVar("cntBtnStart"  , cntBtnStart   );
		state.saveVar("stopDlyCancel", stopDlyCancel );
		state.saveVar("sost"         , sost          );
		
	    saveInputValues(state, inpStartBan 	, "inpStartBan"	);
	    saveInputValues(state, inpStop 		, "inpStop"		);
	    saveInputValues(state, inpStopStart	, "inpStopStart");
	    saveInputValues(state, inpStopRun 	, "inpStopRun"	);
	    saveInputValues(state, inpStopFlow 	, "inpStopFlow"	);
	    saveInputValues(state, inpStopDly 	, "inpStopDly"	);
	    saveInputValues(state, inpSusp 		, "inpSusp"		);
	}

	private void saveInputValues(State state, List<SeqdInput> list, String listname) {
		for (int i=0; i<list.size(); i++) 
			state.saveVar(listname + "_value_" + i, list.get(i).value );
	}


	@Override
	public void loadStateExtra(State state) {
	    curOut        = state.loadVar("curOut"       , curOut        );
	    cnt           = state.loadVar("cnt"          , cnt           );
	    cntBtnStart   = state.loadVar("cntBtnStart"  , cntBtnStart   );
	    stopDlyCancel = state.loadVar("stopDlyCancel", stopDlyCancel );
	    sost          = state.loadVar("sost"         , sost          );

	    loadInputValues(state, inpStartBan 	, "inpStartBan"	);
	    loadInputValues(state, inpStop 		, "inpStop"		);
	    loadInputValues(state, inpStopStart	, "inpStopStart");
	    loadInputValues(state, inpStopRun 	, "inpStopRun"	);
	    loadInputValues(state, inpStopFlow 	, "inpStopFlow"	);
	    loadInputValues(state, inpStopDly 	, "inpStopDly"	);
	    loadInputValues(state, inpSusp 		, "inpSusp"		);
	}

	private void loadInputValues(State state, List<SeqdInput> list, String listname) {
		for (int i=0; i<list.size(); i++) 
			list.get(i).value = state.loadVar(listname + "_value_" + i, list.get(i).value );
	}

	
	
}