package promauto.jroboplc.plugin.roboplant;

import java.util.Map;

import promauto.jroboplc.core.State;
import promauto.jroboplc.core.api.Tag;

public class DeviceZDVB extends Device {
	Input inpEnable;
	Input inpDoOpen;
	Input inpImp;
	Input inpIsFullOpen;
	Input inpCurVal;
	Input inpInProc;

	Tag tagOutput;
	Tag tagSost;
	Tag tagOpened;
	Tag tagClosed;
	Tag tagAlarm;
	Tag tagControl;
	Tag tagBlok;
	Tag tagFlags;
	Tag tagTimeOpen;
	Tag tagMaxCloseTime;
	Tag tagMaxOpenTime;
	Tag tagTime100Proc;
	Tag tagProc;
	Channel chnl1out;
	Channel chnl2out;
	Channel chnl1in;
	Channel chnl2in;
	Tag tagRealProc;
	Tag tagCloseVal;
	Tag tagOpenVal;
	Tag tagValue;

	int cur_imp;
	int cur_en;
	int cur_op;
	int cur_optime;
	int old_control;
	long begin_open;
	long current_time;

	@Override
	public void prepareTags(RefBool res) {
		createTagDescr();

		inpEnable = getInput("Enable", res);
		inpDoOpen = getInput("DoOpen", res);
		inpImp = getInput("Imp", res);
		inpIsFullOpen = getInput("IsFullOpen", res);
		inpCurVal = getInput("CurVal", res);
		inpInProc = getInput("InProc", res);

		tagOutput = getOutputTag("Output", res);
		tagSost = getOutputTag("Sost", res);
		tagOpened = getOutputTag("Opened", res);
		tagClosed = getOutputTag("Closed", res);
		tagAlarm = getOutputTag("Alarm", res);
		tagControl = getOutputTag("Control", res);
		tagBlok = getOutputTag("Blok", res);
		tagFlags = getOutputTag("Flags", res);
		tagTimeOpen = getOutputTag("TimeOpen", res);
		tagMaxCloseTime = getOutputTag("MaxCloseTime", res);
		tagMaxOpenTime = getOutputTag("MaxOpenTime", res);
		tagTime100Proc = getOutputTag("Time100Proc", res);
		tagProc = getOutputTag("Proc", res);
		chnl1out = createChannel("Chnl1Out", Channel.Type.Out, res);
		chnl2out = createChannel("Chnl2Out", Channel.Type.Out, res);
		chnl1in = createChannel("Chnl1In", Channel.Type.In, res);
		chnl2in = createChannel("Chnl2In", Channel.Type.In, res);
		tagRealProc = getOutputTag("RealProc", res);
		tagCloseVal = getOutputTag("CloseVal", res);
		tagOpenVal = getOutputTag("OpenVal", res);
		tagValue = getOutputTag("Value", res);

		if (res.value)
			resetState();
	}

	protected void resetState() {
		tagOutput.setInt(0);
		tagSost.setInt(30);
		tagOpened.setInt(0);
		tagClosed.setInt(0);
		tagControl.setInt(0);
		tagAlarm.setInt(0);
		tagRealProc.setInt(0);
		tagValue.setInt(0);

		cur_imp = 0;
		cur_en = 0;
		cur_op = 0;
		cur_optime = 0;
		old_control = 0;
		begin_open = 0;
		current_time = 0;

	}

	/*
	 * Warning! Code of this method was ported from C++ original RoboplantMtr
	 * sources. Needs get refactored! Do not repeat this style!
	 */
	@Override
	public boolean execute() {

		int Output      = 0;
		int Sost        = tagSost.getInt();
		int Alarm       = 0;
		int Control     = tagControl.getInt();
		int Blok        = tagBlok.getInt();
		int Flags       = tagFlags.getInt();
		int TimeOpen    = tagTimeOpen.getInt();
		int MaxCloseTime = tagMaxCloseTime.getInt();
		int MaxOpenTime = tagMaxOpenTime.getInt();
		int Time100Proc = tagTime100Proc.getInt();
		int Proc        = tagProc.getInt();
		int RealProc    = tagRealProc.getInt();
		int CloseVal    = tagCloseVal.getInt();
		int OpenVal     = tagOpenVal.getInt();
		int Value       = tagValue.getInt();
		
		boolean is_opened = false;
		boolean is_closed = false;
		int CloseOut = 0;
		int OpenOut = 0;

	  
		boolean chnl_ok = false;
		
		if ((Flags & 0xC) == 0) {
			chnl_ok = chnl1in.isOk() && chnl2in.isOk();
			if (chnl_ok) {
				is_closed = chnl1in.tagValue.getBool();
				is_opened = chnl2in.tagValue.getBool();
			}
		}

		if ((Flags & 0xC) == 4) {
			chnl_ok = chnl1in.isOk() && chnl2in.isOk();
			if (chnl_ok) {
				is_closed = chnl2in.tagValue.getBool();
				is_opened = chnl1in.tagValue.getBool();
			}
		}

		if ((Flags & 0xC) == 8) {
			chnl_ok = chnl1in.isOk();
			if (chnl_ok) {
				is_closed = chnl1in.tagValue.getBool();
				is_opened = !is_closed;
			}
		}
		

		if ((Flags & 0xC) == 12) {
			chnl_ok = true;
		}


		if (((Flags & 3) == 0) || ((Flags & 3) == 1))
			chnl_ok = chnl_ok && chnl1out.isOk() && chnl2out.isOk();
		else {
			if ((Flags & 3) == 2)
				chnl_ok = chnl_ok && chnl1out.isOk();
		}

		boolean Flag6 = (Flags & 0x40) == 0x40;

		if (((Flags & 0x80) == 0x80) && (Control == 0)) {
			Proc = inpInProc.getInt();
			tagProc.setInt(Proc);
		} else 
			if( Proc > 100 ) 
				tagProc.setInt( Proc = 100 );


		Value = inpCurVal.getInt();
		tagValue.setInt(Value);
		
		boolean ValClosedIsLowThenValOpened = CloseVal < OpenVal;

		chnl_ok = chnl_ok && ((!Flag6) || (Flag6 && (CloseVal != OpenVal)));

		if (ValClosedIsLowThenValOpened) {
			if (Value <= CloseVal)
				RealProc = 0;
			else if (OpenVal <= Value)
				RealProc = 100;
			else
				RealProc = 100 * (Value - CloseVal) / (OpenVal - CloseVal); 
		} else {
			if (Value >= CloseVal)
				RealProc = 0;
			else if (OpenVal >= Value)
				RealProc = 100;
			else
				RealProc = 100 - (100 * (Value - OpenVal) / (CloseVal - OpenVal)); 
		}
		tagRealProc.setInt(RealProc);

		// Если ошибка каналов
		if (!chnl_ok) {
			tagSost.setInt(88);
			tagAlarm.setInt(1);
			tagOutput.setInt(Blok == 0 ? 0 : 1);
			return true;
		}


		// Ошибка каналов была, но она прошла
		if (Sost == 88)
			Sost = 30;

		// Конвертируем датчики в универсальный формат
		// теперь 1 это всегда сработка
		if ((Flags & 0x20) == 0) {
			is_opened = !is_opened;
			is_closed = !is_closed;
		}

		cur_imp = 0;

		int old_imp = cur_imp;
		int old_optime = cur_optime;
		int NeedFullOpen;

		// датчики всегда отображают текущее положение
		tagOpened.setBool(is_opened);
		tagClosed.setBool(is_closed);

		// Смотрим откуда брать врем открытия (или процент)
		if (Flag6)
			cur_optime = Proc;
		else if (Time100Proc > 0)
			cur_optime = Time100Proc * Proc / 100;
		else
			cur_optime = TimeOpen;

		// Вот тут фишка, если ручное, то подменяем вх сигналы
		switch (Control) {
		case 0: // автоуправление
			// считываем со входов
			cur_imp = inpImp.getInt();
			cur_en = inpEnable.getInt();
			cur_op = inpDoOpen.getInt();
			NeedFullOpen = inpIsFullOpen.getInt();
			break;

		case 1: // закрыть
			cur_imp = 0;
			cur_en = 1;
			cur_op = 0;
			NeedFullOpen = 0;
			break;

		case 2: // открыть частично
			cur_imp = 0;
			cur_en = 1;
			cur_op = 1;
			NeedFullOpen = 0;
			break;

		case 3: // открыть полностью
			cur_imp = 0;
			cur_en = 1;
			cur_op = 1;
			NeedFullOpen = 1;
			break;
		default: // выключить
			cur_imp = 0;
			cur_en = 0;
			cur_op = 0;
			NeedFullOpen = 0;
		}
		
		// Подмена речного режима на автоматический
		if ((Sost >= 20) && (Sost <= 29))
			Sost = Sost - 10;

		long CurCPUTime = System.currentTimeMillis(); // GetTimeTicks();

		// Первый цикл на инициализацию
		while (true) {
			if (Sost == 30) {
				Sost = 0;
				break; // goto exitplace;
			}
			;

			// { // это автоматическое управление

			// перешли с ручного в автомат или назад
			if (((old_control != 0) && (Control == 0)) || ((old_control == 0) && (Control != 0))) { 
				Sost = 0;
				current_time = 0;
				begin_open = current_time;
			}

			if (cur_en == 0) // выключено, нечего и думать
			{
				Sost = 0;
				current_time = 0;
				begin_open = 0;
				break; // goto exitplace;
			}

			// поменяли время открытия, все заново надо делать
			if (cur_optime != old_optime) {
				Sost = 0;
			}

			// все сделали, а тут импульс прилетел
			if (((Sost == 16) || (Sost == 18) || (Sost == 11) || (Sost == 19) || (Sost == 13)) && (cur_imp != old_imp)
					&& (old_imp == 0)) {
				Sost = 0;
			}


			// Если мы были в состоянии закрытия, а теперь нам надо открываться...
			if ((cur_op > 0) && ((Sost == 15) || (Sost == 18))) {
				Sost = 0;
			}

			// Надо открывать, а мы закрыты сейчас
			if ((cur_op > 0) && ((Sost == 17))) {
				// если уже закрыто, тогда тогда можно сразу открывать
				if (is_closed || (Blok != 0))
					Sost = 10;
				else
					Sost = 0;
			}

			// Мы были в состоянии открывания, а теперь сигнал на закрытие пришел
			if ((cur_op == 0) && ((Sost == 10) || (Sost == 11) || (Sost == 19) || (Sost == 12) || (Sost == 13)
					|| (Sost == 14) || (Sost == 16))) {
				Sost = 0;
			}

			// Открыли успешно на нужный угол, теперь ждем
			if ((Sost == 16)) {
				if (NeedFullOpen > 0) {
					Sost = 12;
					current_time = CurCPUTime;
					begin_open = current_time;
				} else
					Output = 1;

			}

			// открылась нормально
			if ((Sost == 17)) {
				// Проверяем, вдруг отошла ?
				if (is_closed || (Blok != 0)) { 
					// Нет, не отошла, дальше ждем
					Output = 1;
				} else {
					// Отошла, надо заново пускать импульс
					Sost = 15;
					CloseOut = 1;
					current_time = CurCPUTime;
					begin_open = current_time;
				}
			}

			// Ошибка закрытия, все теперь ждем пока не сбросят
			if ((Sost == 18)) {
				Alarm = 1;
			}

			// Ошибка закрытия, все теперь ждем пока не сбросят
			if ((Sost == 19)) {
				Alarm = 1;
			}

			// Открыли до конечника успешно
			if ((Sost == 14)) {

				if (NeedFullOpen == 0)
					Sost = 0;

				if (((Blok == 0) && is_opened) || (Blok != 0))
					Output = 1;
				else {
					current_time = CurCPUTime;
					begin_open = current_time;
					Sost = 12;
				}

			}

			// Открытие до конечника закончилось провалом
			if ((Sost == 13)) {
				Alarm = 1;
			}


			// Если закрываем...
			if (Sost == 15) {
				// Приехали ?
				if (is_closed || (Blok != 0)) {
					Sost = 17;
					Output = 1;
					current_time = 0;
					begin_open = 0;
				} else {
					// Неа, не приехали

					// Надо ли время закрытия контролировать ?
					if (MaxCloseTime != 0) {
						current_time = CurCPUTime;
					}

					// Если время закрытия еще не истекло или контроль времени вообще отключен
					if ((((current_time - begin_open) / 1000) <= MaxCloseTime) || (MaxCloseTime == 0)) {
						CloseOut = 1;
					} else { 
						// Время вышло и контроль включен
						Sost = 18;
						Alarm = 1;
						current_time = 0;
						begin_open = 0;
					}
				}
			}

			// Это если открывали, но в этот момент поменялся тип открытия
			if ((Sost == 11) && (NeedFullOpen > 0))
				Sost = 12;
			
			if ((Sost == 12) && (NeedFullOpen == 0))
				Sost = 0;

			// Открываем по времени... или по датчику
			if (Sost == 11) {
				if (Flag6) { // Открытие
					if ((is_opened || (Blok != 0)) || (((RealProc >= Proc) && (ValClosedIsLowThenValOpened))
							|| ((RealProc >= Proc) && (!ValClosedIsLowThenValOpened)))) {
						// Открыли, и слава Богу
						Sost = 16;
						Output = 1;
					} else {

						if (MaxOpenTime != 0)
							current_time = CurCPUTime;
						// Если время закрытия еще не истекло или контроль
						// времени вообще отключен
						if ((((current_time - begin_open) / 1000) <= MaxOpenTime) || (MaxOpenTime == 0)) {
							OpenOut = 1;
						} else {
							// Время вышло и контроль включен
							Sost = 19;
							Alarm = 1;
							current_time = 0;
							begin_open = 0;
						}
					}
				} else {
					current_time = CurCPUTime;

					// уже открыли
					if (((Blok == 0) && (is_opened || (((current_time - begin_open) / 1000) >= cur_optime)))
							|| ((Blok != 0) && (((current_time - begin_open) / 1000) >= cur_optime))) {
						// время вышло
						// приехали
						Sost = 16;
						Output = 1;
					} else {
						// еще едем
						OpenOut = 1;
					}
				}
			}

			// открываем до предела
			if (Sost == 12) {
				// надо ли контролировать по времени
				if (MaxCloseTime > 0)
					current_time = CurCPUTime;

				if (((Blok == 0) && is_opened) || (Blok != 0)) {
					// приехали
					Sost = 14;
					Output = 1;
				} else {
					// еще едем
					// а не пора ли заканчивать ?
					if ((((current_time - begin_open) / 1000) <= MaxOpenTime) || (MaxOpenTime == 0)) {
						// Еще ждем...
						OpenOut = 1;
					} else {
						// Все время вышло
						Sost = 13;
						Alarm = 1;
					}
				}
			}


			if (Sost == 10) {
				if (Flag6) { 
					// Закрытие
					if ((is_closed || (Blok != 0)) || (((RealProc <= Proc) && (ValClosedIsLowThenValOpened))
							|| ((RealProc <= Proc) && (!ValClosedIsLowThenValOpened)))) { 
						// Закрыли, и слава Богу
						Sost = 16;
						Output = 1;
					} else {

						if (MaxCloseTime != 0)
							current_time = CurCPUTime;
						// Если время закрытия еще не истекло или контроль времени вообще отключен
						if ((((current_time - begin_open) / 1000) <= MaxCloseTime) || (MaxCloseTime == 0)) {
							CloseOut = 1;
						} else { 
							// Время вышло и контроль включен
							Sost = 19;
							Alarm = 1;
							current_time = 0;
							begin_open = 0;
						}
					}
				} else { 
					// Предварительное закрытие...
					if (is_closed || (Blok != 0)) { 
						// Закрыли до конца, теперь открываем
						OpenOut = 1;

						// Полностью открывать или нет?
						if (NeedFullOpen > 0)
							Sost = 12;
						else
							Sost = 11;

						current_time = CurCPUTime;
						begin_open = current_time;
					} else { 
						// Продолжаем закрывать
						CloseOut = 1;
						// Надо ли время закрытия контролировать ?
						if (MaxCloseTime != 0)
							current_time = CurCPUTime;

						// Если время закрытия еще не истекло или контроль времени вообще отключен
						if ((((current_time - begin_open) / 1000) <= MaxCloseTime) || (MaxCloseTime == 0)) {
							CloseOut = 1;
						} else { 
							// Время вышло и контроль включен
							Sost = 19;
							Alarm = 1;
							current_time = 0;
							begin_open = 0;
						}
					}
				}
			}

			// это начальное состояние
			if (Sost == 0) {
				if (cur_op > 0) { // надо открывать
					if (NeedFullOpen > 0) {
						// если полностью открывать, то сразу открываем
						if (((Blok == 0) && is_opened) || (Blok != 0)) {
							Sost = 14;
							Output = 1;
						} else {
							Sost = 12;
							OpenOut = 1;
							current_time = CurCPUTime;
							begin_open = current_time;
						}
					} else { // если частично, то сначала надо закрыть
						if (Flag6) {

							if (RealProc == Proc) {
								// ничего не делаем, все готово

								Sost = 16;
								Output = 1;
							} else {
								if (((RealProc < Proc) && (ValClosedIsLowThenValOpened))
										|| ((RealProc < Proc) && (!ValClosedIsLowThenValOpened))) 
								{
									
									// а надо ли открывать вообще ?
									if ((Blok == 0) && !is_opened) {
										// открываем
										OpenOut = 1;
										Sost = 11;
										current_time = CurCPUTime;
										begin_open = current_time;
									} else {
										Sost = 16;
										Output = 1;
									}
								} else {
									// надо ли закрывать ???

									if ((Blok == 0) && !is_closed) {
										// закрываем
										CloseOut = 1;
										Sost = 10;
										current_time = CurCPUTime;
										begin_open = current_time;
									} else {
										Sost = 16;
										Output = 1;
									}
								}
							}
						} else {
							Sost = 10;
							CloseOut = 1;
							current_time = CurCPUTime;
							begin_open = current_time;
						}
					}
				} else {
					if (is_closed || (Blok != 0)) {
						Sost = 17;
						Output = 1;
						current_time = 0;
						begin_open = 0;
					} else {
						// Неа, не приехали
						Sost = 15; // надо закрывать
						CloseOut = 1;
						current_time = CurCPUTime;
						begin_open = current_time;
					}
				}
			}

			// exitplace:
			break;
		}

		if (Control != 0)
			Output = 1;
		// ну теперь делаем Sost больше
		if ((Control != 0) && ((Sost >= 10) && (Sost <= 19)))
			Sost = Sost + 10;

		// сохраним на память
		old_control = Control;

		if ((Flags & 0x10) == 0) {
			CloseOut = CloseOut == 0 ? 1 : 0;
			OpenOut = OpenOut == 0 ? 1 : 0;
		}

		if ((Flags & 0x3) == 0) {
			chnl1out.tagValue.setInt(CloseOut);
			chnl2out.tagValue.setInt(OpenOut);
		}

		if ((Flags & 0x3) == 1) {
			chnl2out.tagValue.setInt(CloseOut);
			chnl1out.tagValue.setInt(OpenOut);
		}

		if ((Flags & 0x3) == 2) {
			chnl1out.tagValue.setInt(CloseOut);
		}
		
		tagOutput      .setInt( Output      );
		tagSost        .setInt( Sost        );        
		tagAlarm       .setInt( Alarm       );

		return true;
	}

	@Override
	public void saveStateExtra(State state) {
		state.saveVar("cur_imp", 		cur_imp     );
		state.saveVar("cur_en", 		cur_en      );
		state.saveVar("cur_op", 		cur_op      );
		state.saveVar("cur_optime", 	cur_optime  );
		state.saveVar("old_control", 	old_control );
		state.saveVar("begin_open", 	begin_open  );
		state.saveVar("current_time", 	current_time );
	}

	@Override
	public void loadStateExtra(State state) {
		cur_imp	 	 = state.loadVar("cur_imp", 	 cur_imp		);
		cur_en		 = state.loadVar("cur_en", 	 	cur_en			);
		cur_op		 = state.loadVar("cur_op", 	 	cur_op			);
		cur_optime	 = state.loadVar("cur_optime",  cur_optime		);
		old_control  = state.loadVar("old_control", old_control		);
		begin_open 	 = state.loadVar("begin_open",  begin_open		);
		current_time = state.loadVar("current_time",current_time	);
	}

}