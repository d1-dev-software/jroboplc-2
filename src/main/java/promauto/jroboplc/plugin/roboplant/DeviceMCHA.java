package promauto.jroboplc.plugin.roboplant;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import promauto.jroboplc.core.State;
import promauto.jroboplc.core.api.Tag;

/**
 * Deprecated class stub
 */
public class DeviceMCHA extends Device {
	List<Input> 		inpTrigres 	= new LinkedList<>();
	List<Input> 		inpSuspend 	= new LinkedList<>();
	List<List<Input>> 	inpInput 	= new LinkedList<>();

	Tag tagMasterOut;
	Tag tagSost;
    Tag tagAlarm;
    Tag tagPlata;
    Tag tagCnt;
    Tag tagTrigRes;
    Tag tagControl;
    Tag tagBlok;
    Tag tagTimeStart;
    Tag tagTimeStop;
    Channel channelOut;
    Channel channelIn;
    
	int oldControl;
	int cntManAlarm;
	int sost;
	int cnt;

	
	

    @Override
	public void prepareTags(RefBool res) {
		createTagDescr();

		tagMasterOut 	= getOutputTag(		"MasterOut"	, res);
		tagSost 		= getOutputTag(		"Sost" 		, res);     
		tagAlarm 		= getOutputTag(		"Alarm" 	, res);    
		tagPlata 		= getOutputTag(		"Plata" 	, res);    
		tagCnt 			= getOutputTag(		"Cnt" 		, res);      
		tagTrigRes 		= getOutputTag(		"TrigRes" 	, res);  
		tagControl 		= getOutputTag(		"Control" 	, res);  
		tagBlok 		= getOutputTag(		"Blok" 		, res);     
		tagTimeStart 	= getOutputTag(		"TimeStart"	, res);
		tagTimeStop 	= getOutputTag(		"TimeStop" 	, res); 
		channelOut 		= createChannel(	"Channel"	, Channel.Type.Out, res); 
		channelIn 		= createChannel(	"Channel"	, Channel.Type.In, res); 
		
		prepareInputArrays();
		
		if (res.value) 
			resetState();
	}
	
    
	protected void resetState() {
		tagMasterOut.setInt(0);
		tagSost.setInt(0); 	
		tagAlarm.setInt(0); 	
		tagPlata.setInt(0); 	
		tagCnt.setInt(0); 		
		tagTrigRes.setInt(0); 	
		
		sost = 0;
		cnt = 0;
		oldControl = 0;
	}

	
    protected void prepareInputArrays() {
    	inpTrigres.clear(); 	
    	inpSuspend.clear(); 	
    	inpInput.clear(); 	
    	
    	String curInpName = "";
    	List<Input> curList = null;
    	
    	for(Input inp: inputs) 
    		switch (inp.name) {
    		case "TrigRes":	inpTrigres.add(inp); break;
    		case "Suspend":	inpSuspend.add(inp); break;
    		default:
	       		if (inp.name.startsWith("Input")) {
	       			if (!inp.name.equals(curInpName)) {
	       				curList = null;
	       				curInpName = inp.name;
	       			}
	       			
	       			if (curList == null) {
	       				curList = new LinkedList<>();
	       				inpInput.add(curList);
	       			}
	       			curList.add(inp);
	       		}
    		}
	}
    
    
    public boolean getValueAnd(List<Input> list) {
    	for (Input inp: list) 
    		if (inp.getInt() == 0)
    			return false;
    	return list.size() > 0;
    }

    public boolean getValueOr(List<Input> list) {
    	for (Input inp: list) 
    		if (inp.getInt() > 0)
    			return true;
    	return false;
    }


	/* Warning!
	 * Code of this method was ported from C++ original RoboplantMtr sources. 
	 * Needs get refactored! Do not repeat this style!
	 */
	@Override
	public boolean execute() {
    	
		  boolean pTrig = false;
		  boolean pSusp = false;
		  boolean pInput = false;
		  int sostNew = 0;
		  boolean mstin;
		  boolean mstin_nosusp;
		  boolean pin;
		  boolean hand_stop = false;
		  boolean plataIn = false;
		  boolean plataOut = false;
//		  int flags_mode;
		  
		  int control = tagControl.getInt();
	
	
	
		  tagAlarm.setBool( sost == 4 ); 
		  
		  
		  if (!linkChannels())
			  return true;
		  
    	  
    	  // 2. Определение pTrig
    	  pTrig = getValueOr(inpTrigres);
    	  if ((control & 4) > 0) {
    		  pTrig = true;
    		  control &= 0xFB;
    		  tagControl.setInt(control);
      	  }

    	  if (control != oldControl) {
	    	  if (((control & 3)==3) && (((control ^ oldControl)&3) > 0)) {
	    		  pTrig = true;
	    	  }
			  oldControl = control;
    	  }
    	  
    	  
    	  // 3. Определение pSusp
    	  pSusp = getValueOr(inpSuspend);
    	        	  
    	  
    	  // 4. Определение pInput
		  for (List<Input> list: inpInput)
			  if (getValueAnd(list)) {
				  pInput = true;
				  break;
			  }
		  
				  
		  
		  
    	  // 5. Расчет Sost
		  if ((control & 3) > 0) {
			  mstin = (control & 2) > 0;
			  hand_stop = !mstin;
		  } else
			  mstin = pInput;
		  mstin_nosusp = mstin;
		  
		  
    	  if (pSusp)
      	    mstin = false;
      	  else 
      	    if (sost==5)   
      	    	sost=0;
      	  
		  
		plataIn = channelIn.tagValue.getInt() > 0;

    	  if (tagBlok.getInt() > 0)
    		  pin = mstin;
    	  else
    		  pin = plataIn;
    	  
		  
		switch (sost) {
		case 0: // Выключено
			if (mstin) {
				sostNew = 2;
				plataOut = true;
				cnt = 1;
			} else if (pin) {
				sostNew = 7;
				plataOut = false;
			} else {
				sostNew = 0; 
				plataOut = false;
			}
			break;

		case 1: // Включено
			if (mstin) {
				if (pin) {
					sostNew = 1;
					plataOut = true;
					cnt = 0;
				} else {
					sostNew = 4;
					plataOut = false;
					cnt = 0;
				}
			} else {
				sostNew = 3;
				plataOut = tagTimeStop.getInt() > 0;
				cnt = 1;
			}
			break;

		case 2: // Запуск
			if (mstin)
				if (pin)
					if (cnt >= tagTimeStart.getInt()) {
						sostNew = 1;
						plataOut = true;
						cnt = 0;
					} else {
						sostNew = 2;
						plataOut = true;
						cnt++;
					}
				else if (cnt >= tagTimeStart.getInt()) {
					sostNew = 4;
					plataOut = false;
					cnt = 0;
				} 
				else {
					sostNew = 2;
					plataOut = true;
					cnt++;
				}
			else {
				sostNew = 0;
				plataOut = false;
				cnt = 0;
			}
			break;

		case 3: // Останов
			if (mstin) {
				sostNew = 2;
				plataOut = true;
				cnt = 1;
			} else if ((cnt >= tagTimeStop.getInt()) || (hand_stop)) {
				if (pin) {
					sostNew = 7;
					plataOut = false;
					cnt = 0;
				} else {
					sostNew = 0;
					plataOut = false;
					cnt = 0;
				}
			} else {
				sostNew = 3;
				plataOut = true;
				cnt++;
			}
			break;

		case 4: // Авария
			if (mstin) {
				if (pTrig) {
					sostNew = 0;
					plataOut = false;
				} else {
					sostNew = 4;
					plataOut = false;
				}
			} else {
				plataOut = false;
				if (pin)
					sostNew = 7;
				else
					sostNew = 0;
			}
			break;


		case 7: // Вкл. без команды
			if (mstin) {
				sostNew = 2;
				plataOut = true;
				cnt = 1;
			} else if (pin) {
				sostNew = 7;
				plataOut = false;
				cnt = 0;
			} else {
				sostNew = 0;
				plataOut = false;
			}
			break;
		}  
    	  
    	  
    	  
		if (pSusp && (sost != 4) && (mstin_nosusp)) {
			sostNew = 5;
			cnt = 0;
		}


		sost = sostNew;

		boolean masterOut = (sost == 1) && (pInput);
		
		if ((((control & 3) == 1) || ((control & 3) == 3)) && (pInput))
			masterOut = true;

		channelOut.tagValue.setBool(plataOut);

		tagTrigRes.setBool(pTrig);

		tagAlarm.setBool( sost == 4 ); 
		
		
		// post
        tagSost.setInt(sost);
       	tagPlata.setBool(plataIn);
       	tagCnt.setInt(cnt);
        tagMasterOut.setBool(masterOut);
       
		return true;
	}


	
	protected boolean linkChannels() {
		if (channelIn.tagAddrNum.getInt() != channelOut.tagAddrNum.getInt())
//			channelIn.set(channelOut);
			channelOut.tagAddrNum.copyValueTo( channelIn.tagAddrNum );

		boolean chnl_ok = true;
		chnl_ok = chnl_ok && channelIn.isOk();
		chnl_ok = chnl_ok && channelOut.isOk();

		if (!chnl_ok) {
			tagSost.setInt(8);
			return false;
		}

		return chnl_ok;
	}
	

	
	
	@Override
	public void saveStateExtra(State state) {
		state.saveVar("oldControl", 	oldControl );
		state.saveVar("cntManAlarm", 	cntManAlarm);
		state.saveVar("sost", 			sost       );
		state.saveVar("cnt", 			cnt        );
	}

	@Override
	public void loadStateExtra(State state) {
	    oldControl  = state.loadVar("oldControl", 	oldControl 	);
	    cntManAlarm = state.loadVar("cntManAlarm", 	cntManAlarm	);
	    sost        = state.loadVar("sost", 		sost       	);
	    cnt         = state.loadVar("cnt", 			cnt        	);
	}


}