package promauto.jroboplc.plugin.roboplant;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import promauto.jroboplc.core.State;
import promauto.jroboplc.core.api.Tag;

public class DeviceLOADA extends Device {
	Input 		inpEnable	;
	
	List<Input> inpInput 	= new ArrayList<>();
	
	List<Tag> 	tagOutput 	= new ArrayList<>();
	List<Tag> 	tagOrder 	= new ArrayList<>();

	Tag 		tagCurrent	;
	Tag 		tagNoOutput	;
	Tag 		tagChanging	;
	Tag 		tagDlyChange;
	
	int cnt;
	int set_current;
	int loadANum;
	int[] order;
	int[] fl;
	int[] idx;
	private int current;


	@Override
	public void prepareTags(RefBool res) {

    	inpEnable = getInput( "Enable", res);
    	
    	tagCurrent		 = getOutputTag( "Current"		, res);
    	tagNoOutput		 = getOutputTag( "NoOutput"		, res);
    	tagChanging		 = getOutputTag( "Changing"		, res);
    	tagDlyChange	 = getOutputTag( "DlyChange"	, res);

    	
    	
//		inpInput.clear(); 
//    	for(Input inp: inputs) 
//    		if (inp.name.startsWith("Input")) 
//    			inpInput.add( inp ); 	
//    			
//		tagOutput.clear(); 
//		tagOrder.clear(); 
//    	for(Output out: outputs) 
//    		if (out.name.startsWith("Output")) 
//    			tagOutput.add( out.tag );
//    		else
//    		if (out.name.startsWith("Order")) 
//    			tagOrder.add( out.tag );
//    	
//    	loadANum = tagOrder.size();
    	
    	
    	
		Input  inp;
		Output out;
		Output ord;
    	int i=0;
    	while (true) {
    		inp = getInput( "Input" +i );
    		out = getOutput( "Output" +i );
    		ord = getOutput( "Order" +i );
    		if (inp==null || ord==null || out==null) break;
    		
    	    inpInput.add( inp );
    	    tagOutput.add( out.tag );
    	    tagOrder.add( ord.tag );
    	    
    	    i++;
    	}
    	
    	loadANum = inpInput.size();

    	
    	
    	
    	order = new int[ loadANum ];
		fl = new int[loadANum];
		idx = new int[loadANum];

		if (res.value)
			resetState(); 
	}

	protected void resetState() {
		tagCurrent.setInt(0);
		tagNoOutput.setInt(0);
		
		for (Tag tag : tagOutput)
			tag.setInt(0);
		
		for (int i=0; i<tagOrder.size(); i++)
			order[i] = tagOrder.get(i).getInt();

		cnt = 0;
		current = 0;
		set_current = -1;
	}
	
	
	/*
	 * Warning! The source code of this method was ported from C++ original
	 * RoboplantMtr sources. Needs get refactored! Do not repeat this style!
	 */
	@Override
	public boolean execute() {

		rebuildOrder();

		int i;
		int flag = 0;
		int tmp_changing = 0;

		if( current != tagCurrent.getInt()  && tagCurrent.getInt() >= 0  &&  tagCurrent.getInt() < 16)
			set_current = tagCurrent.getInt();

		int dlyChange 	= tagDlyChange.getInt();
		int noOutput 	= tagNoOutput.getInt();

		if (set_current != -1) {
			if (cnt >= dlyChange) {
				current = set_current;
				set_current = -1;
			} else {
				cnt++;
				tmp_changing = 1;
			}
		}

		int new_current = current;

		if (inpEnable.getInt() > 0  &&  !isAllOrdersZero() ) {

			for (i = 0; i < loadANum; i++) {
				fl[i] = 0;
				idx[i] = 0;
			}

			for (i = 0; i < loadANum; i++)
				if (order[i] > 0)
					if (inpInput.get(i).getInt() == 0)
						if (order[i] <= loadANum) {
							fl[order[i] - 1] = 1;
							idx[order[i] - 1] = i;
						}

			if ((new_current < 0) || (new_current >= loadANum))
				new_current = 0;

			for (i = order[new_current] - 1; i < loadANum; i++)
				if ((i >= 0) && (i < loadANum))
					if (fl[i] > 0) {
						new_current = idx[i];
						flag = 1;
						break;
					}

			if (flag == 0)
				for (i = 0; i < order[new_current] - 1; i++)
					if ((i >= 0) && (i < loadANum))
						if (fl[i] > 0) {
							new_current = idx[i];
							flag = 1;
							break;
						}

			if (flag == 0) {

				if (cnt >= dlyChange) {
					noOutput = 1;
				} else {
					cnt++;
					tmp_changing = 1;
				}

			} else {

				if (new_current != current) {
					if (cnt >= dlyChange) {
						current = new_current;
						for (i = 0; i < 16; i++)
							tagOutput.get(i).setBool(i == current);
						noOutput = 0;
					} else {
						cnt++;
						tmp_changing = 1;
					}
				} else {
					if (set_current == -1)
						cnt = 0;
					for (i = 0; i < 16; i++)
						tagOutput.get(i).setBool(i == current);
					noOutput = 0;
				}
			}

		} else {
			noOutput = inpEnable.getInt() > 0? 1: 0;
			current = 0;
			for (i = 0; i < 16; i++) {
				tagOutput.get(i).setInt(0);
				if (order[i] == 1)
					current = i;
			}
		}

		tagChanging.setInt(tmp_changing);

		tagDlyChange.setInt(dlyChange);
		tagCurrent	.setInt(current	 );
		tagNoOutput	.setInt(noOutput );

		return true;
	}

	private boolean isAllOrdersZero() {
		for (int i = 0; i < 16; i++)
			if( tagOrder.get(i).getInt() > 0 )
				return false;

		return true;
	}


	protected void rebuildOrder() {
		int idx;
		for (idx = 0; idx < loadANum; idx++)
			if (order[idx] != tagOrder.get(idx).getInt())
				break;

		if (idx == loadANum)
			return;

		int ord1 = order[idx];
		int ord2 = tagOrder.get(idx).getInt();

		for (int i = 0; i < loadANum; i++)
			if (order[i] > 0) {

				if ((ord1 > 0) && (ord2 > 0)) {
					if (ord1 < ord2) {
						if ((order[i] >= ord1)
								&& (order[i] <= ord2))
							order[i]--;
					}

					if (ord1 > ord2) {
						if ((order[i] >= ord2)
								&& (order[i] <= ord1))
							order[i]++;
					}
				}

				if ((ord1 > 0) && (ord2 == 0)) {
					if (order[i] > ord1)
						order[i]--;
				}

				if ((ord1 == 0) && (ord2 > 0)) {
					if (order[i] >= ord2)
						order[i]++;
				}

			}

		order[idx] = tagOrder.get(idx).getInt();

		int k = 0;
		int i;
		for (int j = 1; j <= loadANum; j++) {

			for (i = 0; i < loadANum; i++)
				if (order[i] == j) {
					order[i] = order[i] - k;
					break;
				}

			if (i > loadANum)
				k++;
		}

		for (idx = 0; idx < loadANum; idx++)
			tagOrder.get(idx).setInt( order[idx] );

	}
	
	
	
	@Override
	public void saveStateExtra(State state) {
		state.saveVar("cnt", 			cnt        	);
		state.saveVar("set_current", 	set_current	);
		state.saveVar("current", 	    current	);
		for (int i=0; i<loadANum; i++)
			state.saveVar("order_" + i, order[i]);
			
	}

	@Override
	public void loadStateExtra(State state) {
	    cnt         = state.loadVar("cnt",		 	cnt        	);
		set_current = state.loadVar("set_current", 	set_current	);
		current 	= state.loadVar("current", 		current	);
		for (int i=0; i<loadANum; i++)
			order[i] = state.loadVar("order_" + i, order[i]);
	}
}