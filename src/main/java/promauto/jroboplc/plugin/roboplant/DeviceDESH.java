package promauto.jroboplc.plugin.roboplant;

import java.util.ArrayList;
import java.util.List;

import promauto.jroboplc.core.api.Tag;

public class DeviceDESH extends Device {
	Input inpInput;
	List<Tag> tagOutput = new ArrayList<>();
	
//	int inputval;
	
	
    @Override
	public void prepareTags(RefBool res) {
		inpInput = getInput( "Input", res);

		tagOutput.clear(); 
    	for(Output out: outputs) 
    		if (out.name.startsWith("Output"))
    			tagOutput.add( out.tag ); 

		if (res.value) 
			resetState();
	}


	protected void resetState() {
		for (Tag tag: tagOutput)
			tag.setInt(0);    
		
	}
	
	@Override
	public boolean execute() {
		
		int inp = inpInput.getInt();
		
		for (int i=0; i<tagOutput.size(); i++)
			if( tagOutput.get(i).getBool() ^ (inp == i) )
				tagOutput.get(i).setBool( inp == i );

		return true;
	}


//	@Override
//	public void saveState(Map<String, Integer> state) {
//		super.saveState(state);
//		saveVar(state, 	"inputval", inputval);
//	}
//	
//	@Override
//	public void loadState(Map<String, Integer> state) {
//		super.loadState(state);
//		inputval    = loadVar(state, "inputval", inputval);
//	}

}