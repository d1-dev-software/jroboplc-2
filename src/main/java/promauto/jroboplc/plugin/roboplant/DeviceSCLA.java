package promauto.jroboplc.plugin.roboplant;

import java.util.Map;

import promauto.jroboplc.core.State;
import promauto.jroboplc.core.api.Tag;

public class DeviceSCLA extends Device {
	Input 	inpValue;
	
	Tag 	tagValue		;
	Tag 	tagPrimeval		;
	Tag 	tagPercent		;
	Tag 	tagMax			;
	Tag 	tagMin			;
	Tag 	tagPointV1      ;
	Tag 	tagPointP1      ;
	Tag 	tagPointV2      ;
	Tag 	tagPointP2      ;
	Tag 	tagSPmax        ;
	Tag 	tagSPmin        ;
	Tag 	tagBlok         ;
	Tag 	tagBlokValue	;
	Tag 	tagDlyMax       ;
	Tag 	tagDlyMin       ;
	
    int   cntmax;
    int   cntmin;
	
    @Override
	public void prepareTags(RefBool res) {

    	inpValue = getInput( "Value", res);
    	
    	tagValue		 = getOutputTag( "Value"		, res);
    	tagPrimeval		 = getOutputTag( "Primeval"		, res);
    	tagPercent		 = getOutputTag( "Percent"		, res);
    	tagMax			 = getOutputTag( "Max"			, res);
    	tagMin			 = getOutputTag( "Min"			, res);
    	tagPointV1  	 = getOutputTag( "PointV1"		, res);
    	tagPointP1  	 = getOutputTag( "PointP1"		, res);
    	tagPointV2  	 = getOutputTag( "PointV2"		, res);
    	tagPointP2  	 = getOutputTag( "PointP2"		, res);
    	tagSPmax    	 = getOutputTag( "SPmax"		, res);
    	tagSPmin    	 = getOutputTag( "SPmin"		, res);
    	tagBlok     	 = getOutputTag( "Blok"			, res);
    	tagBlokValue	 = getOutputTag( "BlokValue"	, res);
    	tagDlyMax   	 = getOutputTag( "DlyMax"		, res);
    	tagDlyMin   	 = getOutputTag( "DlyMin"		, res);

		if (res.value) 
			resetState();
	}


	protected void resetState() {
		tagValue	.setInt(0);
		tagPrimeval	.setInt(0);
		tagPercent	.setInt(0);
		tagMax		.setInt(0);
		tagMin		.setInt(0);
		
		cntmax = 0;
		cntmin = 0;
	}

	@Override
	public boolean execute() {

		int tmpVal = 0;

		if (tagBlok.getInt() == 2)
			tagPrimeval.setInt(tagBlokValue.getInt());
		else
			tagPrimeval.setInt(inpValue.getInt());

		float r;
		float r1 = tagPointP2.getInt() - tagPointP1.getInt();

		float r2 = tagPrimeval.getInt() - tagPointP1.getInt();

		float r3 = tagPointV2.getInt() - tagPointV1.getInt();

		if (tagBlok.getInt() == 1) {
			tmpVal = tagBlokValue.getInt();
			r = tmpVal;
		} else {

			if (r1 == 0) {
				tmpVal = 0;
				r = 0;
			} else {
				r = r2 / r1;
				r = r * r3 + tagPointV1.getInt();

				if (r < 0) {
					tmpVal = 0;
					r = 0;
				} else {
					tmpVal = Math.round(r);
				}
			}
		}

		int spmax = tagSPmax.getInt();
		int spmin = tagSPmin.getInt();


		if (tmpVal >= spmax) {
			if (cntmax < tagDlyMax.getInt())
				cntmax++;
			else
				tagMax.setInt(1);

			tmpVal = spmax;
		} else {
			tagMax.setInt(0);
			cntmax = 0;
		}

		if (tmpVal <= spmin) {
			if (cntmin < tagDlyMin.getInt())
				cntmin++;
			else
				tagMin.setInt(1);

			tmpVal = spmin;
		} else {
			tagMin.setInt(0);
			cntmin = 0;
		}

		if ((spmax - spmin) > 0) {
			r = tmpVal;
			r = (r - spmin) * 100 / (spmax - spmin);
			if (r < 0)
				r = 0;
			tagPercent.setInt(Math.round(r));
		}

		tagValue.setInt(tmpVal);

		return true;
	}

	
	
	@Override
	public void saveStateExtra(State state) {
		state.saveVar("cntmax", cntmax );
		state.saveVar("cntmin", cntmin );
	}
	
	@Override
	public void loadStateExtra(State state) {
		cntmax = state.loadVar("cntmax", cntmax );
		cntmin = state.loadVar("cntmin", cntmin );
	}

}