package promauto.jroboplc.plugin.roboplant;

import promauto.jroboplc.core.State;
import promauto.jroboplc.core.api.Tag;

import java.util.ArrayList;
import java.util.List;

public class DeviceLOADB extends Device {
	private static final int SIZE = 16;

	protected static class Item {
		Tag tagOutput;
		Tag tagSelect;
		int index;
		boolean select;
	}

	protected List<Item> items = new ArrayList<>();

	Input inpEnable;
	Tag tagCurrent;
	Tag tagChanging;
	Tag tagDlyChange;
	Tag tagFlags;
	Tag tagOutput0;

	int current = 0;
	int changecnt = 0;


    @Override
	public void prepareTags(RefBool res) {
		inpEnable  	= getInput("Enable", res);
		tagCurrent 		= getOutputTag( "Current", 	res);
		tagChanging 	= getOutputTag( "Changing", 	res);
    	tagDlyChange 	= getOutputTag( "DlyChange", 	res);
		tagFlags 		= getOutputTag( "Flags", 		res);
		tagOutput0 		= getOutputTag( "Output0",	res);

		items.clear();

		for(int i=1; i<=SIZE; ++i) {
    		Item it = new Item();
			it.tagOutput = getOutputTag( "Output" + i,	res);
			it.tagSelect = getOutputTag( "Select" + i,	res);
			it.index = i;
			it.select = false;
    		items.add(it);
    	}

	}


	@Override
	public boolean execute() {

		if( tagCurrent.getInt() < 0  ||  tagCurrent.getInt() > SIZE )
			tagCurrent.setInt(current);

		int oldcur = current;

    	if( current != tagCurrent.getInt() ) {
			current = tagCurrent.getInt();
		} else {
    		for(Item it: items) {
    			boolean select = it.tagSelect.getBool();
    			if( select ) {
					if( !it.select )
						current = it.index;
				} else {
    				if( current == it.index )
						current = 0;
				}
			}
		}

		tagCurrent.setInt(current);

    	if( current != oldcur  &&  current > 0 )
    		changecnt = tagDlyChange.getInt();

		tagChanging.setBool( changecnt > 0);

		for(Item it: items) {
			boolean select = it.index == current;
			it.tagSelect.setBool(select);
			it.select = select;

			if (inpEnable.getInt() == 0) {
				it.tagOutput.setInt(0);
			} else {
				if (changecnt == 0)
					it.tagOutput.setBool(select);
			}
		}


		if (changecnt == 0)
			tagOutput0.setBool( current == 0  &&  inpEnable.getInt() > 0);
		else
			--changecnt;




		return true;
	}


	@Override
	public void saveStateExtra(State state) {
		state.saveVar("current", 	current);
		state.saveVar("changecnt", 	changecnt);
		for(Item it: items)
			state.saveVar("select" + it.index, it.select);
	}

	@Override
	public void loadStateExtra(State state) {
		current   = state.loadVar("current",	current);
		changecnt = state.loadVar("changecnt", 	changecnt);
		for(Item it: items)
			it.select = state.loadVar("select" + it.index, it.select);
	}



}