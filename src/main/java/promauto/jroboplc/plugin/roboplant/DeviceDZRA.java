package promauto.jroboplc.plugin.roboplant;

import promauto.jroboplc.core.api.Tag;


public class DeviceDZRA extends Device {
	Input 	inpInput		;
	
	Tag 	tagState		;
	Tag 	tagZagr			;	
	Tag 	tagRazgr		;
	Tag 	tagError		;
	Tag 	tagIsNull		;
	Tag 	tagNotEqu		;
	Tag 	tagComplited	;
	Tag 	tagIState		;
	Tag 	tagIZagrRdy		;
	Tag 	tagIRazgrRdy	;
	Tag 	tagIErrorFlag	;
	Tag 	tagITaskIsNull	;
	Tag 	tagITaskNotEqu	;
	Tag 	tagITaskCompl	;
	
	
    @Override
	public void prepareTags(RefBool res) {
    	inpInput = getInput( "Input", res);
    	
    	tagState			 = getOutputTag( "State"		, res);
    	tagZagr				 = getOutputTag( "Zagr"			, res);
    	tagRazgr			 = getOutputTag( "Razgr"		, res);
    	tagError			 = getOutputTag( "Error"		, res);
    	tagIsNull			 = getOutputTag( "IsNull"		, res);
    	tagNotEqu			 = getOutputTag( "NotEqu"		, res);
    	tagComplited		 = getOutputTag( "Complited"	, res);
    	tagIState			 = getOutputTag( "iState"		, res);
    	tagIZagrRdy			 = getOutputTag( "iZagrRdy"		, res);
    	tagIRazgrRdy		 = getOutputTag( "iRazgrRdy"	, res);
    	tagIErrorFlag		 = getOutputTag( "iErrirFlag"	, res);
    	tagITaskIsNull		 = getOutputTag( "iTaskIsNull"	, res);
    	tagITaskNotEqu		 = getOutputTag( "iTaskNotEqu"	, res);
    	tagITaskCompl		 = getOutputTag( "iTaskCompl"	, res);
	}


	@Override
	public boolean execute() {

		 boolean inp = inpInput.getInt() > 0;

		  tagError.setBool( inp && tagIErrorFlag.getBool() );
		  if ((inp) && (!tagError.getBool())) {
		    tagState.setInt( tagIState.getInt());
		    tagZagr.setBool( tagIZagrRdy.getBool() && !tagITaskNotEqu.getBool() );
		    tagRazgr.setInt( tagIRazgrRdy.getInt() );
		    tagIsNull.setInt( tagITaskIsNull.getInt() );
		    tagNotEqu.setInt( tagITaskNotEqu.getInt() );
		    tagComplited.setInt( tagITaskCompl.getInt() );
		  } else {
		    tagState    .setInt(0);
		    tagZagr     .setInt(0);
		    tagRazgr    .setInt(0);
		    tagIsNull   .setInt(0);
		    tagNotEqu   .setInt(0);
		    tagComplited.setInt(0); 
		  }
		  
		  return true;
	}
}