package promauto.jroboplc.plugin.roboplant;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import promauto.jroboplc.core.State;
import promauto.jroboplc.core.api.Tag;

public class DeviceZDVT extends Device {
	
	static class Direction {
		Input inp;
		Channel channelIn;
		Channel channelOut;
	}

	Input inpEnable;
	List<Direction> dirs = new ArrayList<>();
	
	Tag tagOutput;
	Tag tagSost;
    Tag tagControl;
    Tag tagBlok;
    Tag tagFlags;
    Tag tagDelay;
    Tag tagParam;

	int sost;
	int curdirNum;                            
    int curdirSost;                           
    int delayCnt;  
    int dirAmount;

	

    @Override
	public void prepareTags(RefBool res) {
		createTagDescr();

    	inpEnable		= getInput("Enable", res);
    	
    	tagOutput 		= getOutputTag(0, res);
    	tagSost   		= getOutputTag(1, res);
    	tagControl		= getOutputTag(2, res);
    	tagBlok   		= getOutputTag(3, res);
    	tagFlags		= getOutputTag(4, res);
    	tagDelay      	= getOutputTag(5, res);
    	tagParam      	= getOutputTag(6, res);
    	
    	prepareDirections(res);
    	
		if (res.value)
			resetState(); 
	}
    
    
    protected void prepareDirections(RefBool res) {
    	dirs.clear();
    	Input inp;
    	Direction dir;
    	for (int i=1; ; i++) {
        	inp = getInput("Dir" + i);
        	if (inp == null)
        		break;
        	
        	dir = new Direction();
        	dir.inp = inp;
        	dir.channelIn = createChannel( "Chnl" + i + "In", Channel.Type.In, res);
        	dir.channelOut = createChannel( "Chnl" + i + "Out", Channel.Type.Out, res);
        	
        	dirs.add(dir);
    	}
    	dirAmount = dirs.size();
	}



	protected void resetState() {
		tagOutput.setInt(0);
		tagSost.setInt(0); 	
		
		sost = 0;
		curdirNum = 0xFF;                            
	    curdirSost = 0;                           
	    delayCnt = 0;                             
	}
	

    
	/* Warning!
	 * Code of this method was ported from C++ original RoboplantMtr sources. 
	 * Needs get refactored! Do not repeat this style!
	 */
	@Override
	public boolean execute() {

		int i, dr, first_inp;
		boolean dat_in = false;

		for (Direction dir : dirs)
			if (!dir.channelIn.isOk() || !dir.channelOut.isOk()) {
				tagOutput.setInt(0);
				tagSost.setInt(0xFE);
				return false;
			}

		int flags = tagFlags.getInt();
		int control = tagControl.getInt();
		int blok = tagBlok.getInt();

		boolean en = inpEnable.getInt() > 0;

		first_inp = -1;
		for (i = 0; (i < dirAmount) && (first_inp == -1); i++)
			if (dirs.get(i).inp.getInt() > 0)
				first_inp = i;

		int ctrl = control & 0x7F;
		if (ctrl == 0) {
			ctrl = (control >> 8) & 0x7F;
			if (ctrl > 0) {
				first_inp = ctrl - 1;
				en = true;
			}
		}

		dr = (ctrl & 0x7F) - 1;
		if ((dr == -1) && (en)) {

			// проверка активности предыдуще-выбранного направления
			if (curdirNum < dirAmount)
				if (dirs.get(curdirNum).inp.getInt() > 0)
					dr = curdirNum;

			// если предыдущ.напр.уже не активно, то ищем новое активное
			if (dr == -1)
				dr = first_inp;

		}
		if ((dr >= dirAmount) || (dr == -1))
			dr = 0xFF;

		// если направление сменилось:
		if (dr != 0xFF) {
			if (dr != curdirNum) {
				curdirSost = 0;
			}
		} else {
			// если не одно напр.не выбрано, погасить все выходы
			curdirSost = 2;
			for (i = 0; i < dirAmount; i++)
				dirs.get(i).channelOut.tagValue.setInt(0);
		}
		curdirNum = dr;

		// получение состояния датчика положения
		if (tagBlok.getBool()) {
			dat_in = true;
		} else {
			if (curdirNum < dirAmount) {
				dat_in = getDataDirChnlIn(curdirNum, flags) > 0;
			}
		}

		// команда повторной подачи сигнала на выход
		if ((control & 0x80) > 0) {
			dat_in = false;
			curdirSost = 0;
			control &= 0xFF7F;
			tagControl.setInt(control);
		}

		// подача импульса:
		if (curdirSost == 0) {
			delayCnt = 0;
			for (i = 0; i < dirAmount; i++)
				dirs.get(i).channelOut.tagValue.setBool((i == curdirNum)
						&& ((!dat_in) || (blok > 0) || ((flags & 3) == 2)));

			curdirSost = 1;
		} else

		// снятие импульса
		if (curdirSost == 1) {
			int flgs = flags & 3;
			if ((flgs == 0) || ((flgs == 1) && (dat_in))) {
				if (curdirNum < dirAmount)
					dirs.get(curdirNum).channelOut.tagValue.setInt(0);

				curdirSost = 2;
			}
		} else

		// ожидание
		if (curdirSost == 2) {
			// ничего не делать
		}

		// определение Sost
		dr = -1;
		int dr_kolvo = 0;

		for (i = 0; i < dirAmount; i++)
			if (getDataDirChnlIn(i, flags) > 0)
				dr_kolvo++;

		for (i = 0; (i < dirAmount) && (dr == -1); i++)
			if (getDataDirChnlIn(i, flags) > 0)
				dr = i;

		int sst = dr + 1;

		if (blok > 0) {
			if (ctrl > 0)
				sst = ctrl;
			else
				sst = curdirNum + 1;
		}

		// установка бита аварии на Sost'e
		boolean alm = false;
		if ((curdirNum != 0xFF) && (!dat_in)) {
			if (delayCnt < tagDelay.getInt())
				delayCnt++;
			else {
				alm = true;
				if ((flags & 0x10) > 0) {
					if ((delayCnt++) > (tagDelay.getInt() * 2)) {
						control |= 0x80;
						tagControl.setInt(control);
					}
//					System.out.println("" + delayCnt);
				}
			}
		} else {
			delayCnt = 0;
		}

		sost = (alm) ? (curdirNum + 1) | 0x80 : sst & 0x7F;
		if (dr_kolvo > 1)
			sost = sost | 0x80;
		tagSost.setInt(sost);
//		System.out.println("" + alm + "  " + curdirNum + "  " + sst + "  " + dr_kolvo + "  " + sost);

		// опредление output'a
		tagOutput.setBool((first_inp >= 0)
				&& (((flags & 4) > 0) ? (en && dat_in) : en));

		int tmpParam = 0;
		for (i = 0; i < dirAmount; i++) {
			tmpParam = tmpParam << 1;
			tmpParam |= getDataDirChnlIn(i, flags);
			tmpParam = tmpParam << 1;
			tmpParam |= dirs.get(i).channelOut.tagValue.getInt();
		}
		tagParam.setInt(tmpParam);

		return true;
	};

	
	
	protected int getDataDirChnlIn(int dirnum, int flags) {
		if (((flags >> (dirnum + 8)) & 1) > 0)
			return 1 - (((dirs.get(dirnum).channelIn.tagValue.getBool()) == ((flags & 0x80) > 0)) ? 1
					: 0);
		else
			return ((dirs.get(dirnum).channelIn.tagValue.getBool()) == ((flags & 0x80) > 0)) ? 1
					: 0;

	};

	
	@Override
	public void saveStateExtra(State state) {
		state.saveVar("sost"      , sost       );
		state.saveVar("curdirNum" , curdirNum  );
		state.saveVar("curdirSost", curdirSost );
		state.saveVar("delayCnt"  , delayCnt   );
	}

	@Override
	public void loadStateExtra(State state) {
	    sost       = state.loadVar("sost"      , sost       );
	    curdirNum  = state.loadVar("curdirNum" , curdirNum  );
	    curdirSost = state.loadVar("curdirSost", curdirSost );
	    delayCnt   = state.loadVar("delayCnt"  , delayCnt   );
	}
	

}