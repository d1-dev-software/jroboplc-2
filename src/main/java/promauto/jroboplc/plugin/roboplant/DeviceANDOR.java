package promauto.jroboplc.plugin.roboplant;

import promauto.jroboplc.core.api.Tag;

public class DeviceANDOR extends Device {
	Input   inpInpNum;
	Input   inpInput0;
	Input   inpInput1;
	Tag 	tagOutput;
	
    @Override
	public void prepareTags(RefBool res) {
    	inpInpNum		= getInput(			"InpNum"	, res);
    	inpInput0		= getInput(			"Input0"	, res);
    	inpInput1		= getInput(			"Input1"	, res);
    	tagOutput   	= getOutputTag(		"Output"	, res);
	}

	@Override
	public boolean execute() {
		if (inpInpNum.getInt() == 0)
			tagOutput.setInt( inpInput0.getInt() );
		else
			tagOutput.setInt( inpInput1.getInt() );
		return true;
	}
    
    

}