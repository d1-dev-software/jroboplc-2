package promauto.jroboplc.plugin.roboplant;

import promauto.jroboplc.core.api.Tag;

public class DeviceI7024 extends ConnectDevice {
	Input[] inpInput 	= new Input[4];
	Tag[] tagOutput 	= new Tag[4];

    @Override
	public void prepareTags(RefBool res) {
    	
    	for(int i=0; i<4; ++i) {
    		connector.connect( 
    				"Output_" + i, 
    				"out" + i + ".value", 
    				Connector.Mode.READ, res);
    		
    		connector.connect( 
    				"setOutput_" + i, 
    				"out" + i + ".valsp", 
    				Connector.Mode.WRITE, res);
    		
        	inpInput[i] = getInput( "setOutput_" + i, res);
        	tagOutput[i] = getOutputTag( "setOutput_" + i, res);
    	}
    	
    	connector.connect("ErrorFlag", "SYSTEM.ErrorFlag", Connector.Mode.READ, res);
    	connector.connect("ErrorCounter", "SYSTEM.ErrorCount", Connector.Mode.READ, res);

    	registerAsChannelProvider( "DipNetAddress", Channel.Type.InOut, res);
    }

    
	@Override
	public boolean execute() {
		super.execute();
		
    	for(int i=0; i<4; ++i) {
//			if( inpInput[i].tag != null )
			if( !inpInput[i].isEmpty() )
    			tagOutput[i].setInt( inpInput[i].getInt() );
    	}

		return true;
	}


}