package promauto.jroboplc.plugin.roboplant;

import java.util.Map;

import promauto.jroboplc.core.State;
import promauto.jroboplc.core.api.Tag;

public class DeviceVALA extends Device {
	private static final int ALWAYS_ENABLED = 1;
	private static final int VALIN_IGNORED = 2;
	private static final int VALIN_TO_VALSET_DISABLED = 4;

	Input 	inpValIn;
	Input 	inpEnable;
	Tag 	tagValOut;
	Tag 	tagValSet;
	Tag 	tagDlyEnOn;
	Tag 	tagDlyEnOff;
	Tag 	tagFlags;
	
	int enable;
	int timer;
	
    @Override
	public void prepareTags(RefBool res) {

    	inpValIn   = getInput( "ValIn", res);
    	inpEnable  = getInput( "Enable", res);
    	
    	tagValOut 	 = getOutputTag( "ValOut"		, res);
    	tagValSet 	 = getOutputTag( "ValSet"		, res);
    	tagDlyEnOn	 = getOutputTag( "DlyEnOn"		, res);
    	tagDlyEnOff	 = getOutputTag( "DlyEnOff"		, res);
    	tagFlags  	 = getOutputTag( "Flags"		, res);

		if (res.value) 
			resetState();
	}


	protected void resetState() {
		tagValOut.setInt(0);
		enable = 0;
		timer = 0;
	}

	@Override
	public boolean execute() {
		int flags = tagFlags.getInt();

		int valout = tagValSet.getInt();

		if( !inpValIn.isEmpty()  &&  (flags & VALIN_IGNORED) == 0 ) {
			valout = inpValIn.getInt();
			if( (flags & VALIN_TO_VALSET_DISABLED) == 0 )
				tagValSet.setInt(valout);
		}
		
		if( (flags & ALWAYS_ENABLED) > 0 )
			enable = 1;
		else
			if( enable != inpEnable.getInt() ) {
				if( timer <= 0 )
					timer = inpEnable.tag.getBool()?  tagDlyEnOn.getInt():  tagDlyEnOff.getInt();
				else
					timer--;
				
				if( timer <= 0 )
					enable = inpEnable.getInt();
			} else
				timer = 0;
		
		
		tagValOut.setInt( enable > 0?  valout:  0 );

		return true;
	}

	
	
	@Override
	public void saveStateExtra(State state) {
		state.saveVar("enable",	enable);
		state.saveVar("timer", 	timer);
	}
	
	@Override
	public void loadStateExtra(State state) {
		enable = state.loadVar("enable", 	enable);
		timer  = state.loadVar("timer", 	timer);
	}

}