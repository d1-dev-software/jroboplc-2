package promauto.jroboplc.plugin.roboplant;

import promauto.jroboplc.core.api.Tag;

public class DeviceDZR12 extends ConnectDevice {
    public static final int MAXSIZE = 12;
    Input inpInput;
    Input inpSuspend;
    Input inpTrigRes;
    Input inpLoad;
    Input inpUnload;

    Tag tagNetAddress;
    Tag tagComPort;
    Tag tagMaxShnkNum;
    Tag tagErrorFlag;
    Tag tagErrorCounter;
    Tag tagEnable;
    Tag tagStatus;
    Tag tagTaskCode;
    Tag tagCurStorNum;
    Tag tagResStorNum;
    Tag tagCurStorWesHigh;
    Tag tagCurStorWesLow;
    Tag tagCurStage;
    Tag tagInputs;
    Tag tagOutputs;
    Tag tagCmd;
    Tag tagErrCode;

    Tag[] tagsCounterHigh       = new Tag[MAXSIZE];
    Tag[] tagsCounterLow        = new Tag[MAXSIZE];
    Tag[] tagsNominalCurHigh    = new Tag[MAXSIZE];
    Tag[] tagsNominalCurLow     = new Tag[MAXSIZE];


    @Override
    public void prepareTags(RefBool res) {
        inpInput 		= getInput( "Input"		, res);
        inpSuspend 		= getInput( "Suspend"		, res);
        inpTrigRes 	    = getInput( "TrigRes"	    , res);
        inpLoad 	    = getInput( "Load"	    , res);
        inpUnload 	    = getInput( "Unload"	    , res);

        tagNetAddress	 = getOutputTag( "NetAddress"	, res);
        tagComPort		 = getOutputTag( "ComPort"	, res);
        tagMaxShnkNum	 = getOutputTag( "MaxShnkNum"	, res);
        tagErrorFlag	 = getOutputTag( "ErrorFlag"	, res);
        tagErrorCounter	 = getOutputTag( "ErrorCounter"	, res);
        tagEnable		 = getOutputTag( "Enable"	    , res);
        tagStatus		 = getOutputTag( "Status"	    , res);
        tagTaskCode		 = getOutputTag( "TaskCode"	, res);
        tagCurStorNum	 = getOutputTag( "CurStorNum"	, res);
        tagResStorNum	 = getOutputTag( "ResStorNum"	, res);
        tagCurStorWesHigh= getOutputTag( "CurStorWesHigh"	, res);
        tagCurStorWesLow = getOutputTag( "CurStorWesLow"	, res);
        tagCurStage		 = getOutputTag( "CurStage"	, res);
        tagInputs		 = getOutputTag( "Inputs"	    , res);
        tagOutputs		 = getOutputTag( "Outputs"	, res);
        tagCmd		     = getOutputTag( "Cmd"	    , res);
        tagErrCode		 = getOutputTag( "ErrCode"	, res);

        for(int i=0; i<MAXSIZE; ++i) {
            tagsCounterHigh[i]  = getOutputTag( String.format("Counter%02dHigh", i), res);
            tagsCounterLow[i]   = getOutputTag( String.format("Counter%02dLow", i), res);
            tagsNominalCurHigh[i]  = getOutputTag( String.format("NominalCur%02dHigh", i), res);
            tagsNominalCurLow[i]  = getOutputTag( String.format("NominalCur%02dLow", i), res);

        }

        connector.connect("NetAddress",     "SYSTEM.NetAddress",Connector.Mode.READ, res);
        connector.connect("ComPort",        "SYSTEM.ComPort",   Connector.Mode.READ, res);
        connector.connect("MaxShnkNum",     "Size",             Connector.Mode.READ, res);

        connector.connect("ErrorFlag",      "SYSTEM.ErrorFlag", Connector.Mode.READ, res);
        connector.connect("ErrorCounter",   "SYSTEM.ErrorCount",Connector.Mode.READ, res);

        connector.connect("Enable",         "Enable",           Connector.Mode.WRITE, res);
        connector.connect("Status",         "Status",           Connector.Mode.READ, res);
        connector.connect("TaskCode",       "Task",             Connector.Mode.READ, res);
        connector.connect("CurStorNum",     "CurStorNum",       Connector.Mode.READ, res);
        connector.connect("ResStorNum",     "RestStorNum",      Connector.Mode.READ, res);
        connector.connect("CurStorWesHigh", "CurStorWesHigh",   Connector.Mode.READ, res);
        connector.connect("CurStorWesLow",  "CurStorWesLow",    Connector.Mode.READ, res);
        connector.connect("CurStage",       "CurStage",         Connector.Mode.READ, res);
        connector.connect("Inputs",         "Inputs",           Connector.Mode.READ, res);
        connector.connect("Outputs",        "Outputs",          Connector.Mode.READ, res);
        connector.connect("Cmd",            "Cmd",              Connector.Mode.MANUAL, res);
        connector.connect("ErrCode",        "ErrorCode",        Connector.Mode.READ, res);


        for(int i=0; i<MAXSIZE; ++i) {
            connector.connect(String.format("Counter%02dHigh", i),     "SumWesHigh"+i,     Connector.Mode.READ, res);
            connector.connect(String.format("Counter%02dLow",  i),     "SumWesLow"+i,      Connector.Mode.READ, res);
            connector.connect(String.format("NominalCur%02dHigh", i),  "TaskWesHigh"+i,    Connector.Mode.READ, res);
            connector.connect(String.format("NominalCur%02dLow", i),   "TaskWesLow"+i,     Connector.Mode.READ, res);
        }

        if (res.value)
            resetState();
    }


    protected void resetState() {
        tagCmd.setInt(100);
    }

    @Override
    public boolean execute() {
        super.execute();

        int cmd = tagCmd.getInt();

        if( inpLoad.tag.getBool()  &&  tagStatus.getInt() == 1 )
            cmd = 2;
        else

        if( inpUnload.tag.getBool()  &&  tagStatus.getInt() == 3 )
            cmd = 4;

        if( cmd < 100) {
            Tag tagCmdBind = connector.getTagBind(tagCmd);
            if( tagCmdBind != null ) {
                tagCmdBind.setInt(cmd);
                tagCmd.setInt(cmd + 100);
            }
        }

        return true;
    }

}