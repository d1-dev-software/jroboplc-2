package promauto.jroboplc.plugin.roboplant;


/**
 * @author Denis Lobanov
 */
public class DeviceAND extends DeviceSimpleLogic {

	@Override
	public boolean execute() {
		
		boolean output = true; 
		for (Input inp: inpInput)
			output &= (inp.getInt() > 0);
		
		tagOutput.setBool(output);
		
		return true;
	}

	
}