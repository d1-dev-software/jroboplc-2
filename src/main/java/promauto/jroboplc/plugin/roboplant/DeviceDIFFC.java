package promauto.jroboplc.plugin.roboplant;

import java.util.Map;

import promauto.jroboplc.core.State;
import promauto.jroboplc.core.api.Tag;

public class DeviceDIFFC extends Device {
	
	Input inpEnable;
	Input inpSusp;
	Input inpCurTmp;
	Input inpMaxTmp;
	Input inpMinTmp;
	Input inpAlrmTmpHigh;
	Input inpAlrmTmpLow;
	Input inpError;
	
	Tag tagOutVal;
	Tag tagState;
	Tag tagMinOut;
	Tag tagMaxOut;
	Tag tagStartPwr;
	Tag tagPwrStep;
	Tag tagPwrMult;
	Tag tagTimePreHeat;
	Tag tagTimeWrkHigh;
	Tag tagTimeWrkLow;
	Tag tagOldTmp;
	Tag tagFlSetHigh;     
	Tag tagTimeWrkLowCnt; 
	Tag tagTimeWrkHighCnt;
	Tag tagTimePreCnt;    
	
	
	int enable;
	int time;
	int curTmp;
	int maxTmp;
	int minTmp;
	int alrmTmpHigh;
	int alrmTmpLow;
	int error;
//    int pwrMult;    

	
    @Override
	public void prepareTags(RefBool res) {

    	inpEnable		= getInput( "Enable", res);     
    	inpSusp			= getInput( "Susp", res);
    	inpCurTmp		= getInput( "CurTmp", res);
    	inpMaxTmp		= getInput( "MaxTmp", res);
    	inpMinTmp		= getInput( "MinTmp", res);
    	inpAlrmTmpHigh	= getInput( "AlrmTmpH", res);
    	inpAlrmTmpLow	= getInput( "AlarmTmpL", res);
    	inpError		= getInput( "Error", res);
    	
    	tagOutVal		  = getOutputTag( "OutVal"		, res);
    	tagState		  = getOutputTag( "State"			, res);
    	tagMinOut		  = getOutputTag( "MinOut_mA"		, res);
    	tagMaxOut         = getOutputTag( "MaxOut_mA"		, res);
    	tagStartPwr       = getOutputTag( "StartPwr"		, res);
    	tagPwrStep        = getOutputTag( "PwrStep"		, res);
    	tagPwrMult        = getOutputTag( "PwrMult"		, res);
    	tagTimePreHeat    = getOutputTag( "TimePreHeat"	, res);
    	tagTimeWrkHigh    = getOutputTag( "TimeWrkHigh"	, res);
    	tagTimeWrkLow     = getOutputTag( "TimeWrkLow"	, res);
    	tagOldTmp	      = getOutputTag(  "OldTmp"		, res);
    	tagFlSetHigh      = getOutputTag( "FlSetLow"		, res);
    	tagTimeWrkLowCnt  = getOutputTag( "TimeWorkLowCnt"	, res);
    	tagTimeWrkHighCnt = getOutputTag( "TimeWorkHighCnt"	, res);
    	tagTimePreCnt     = getOutputTag( "TimePreCnt"		, res);

		if (res.value) 
			resetState();
	}


	protected void resetState() {
	    enable			= 0;
	    time			= 0;  
	    curTmp			= 0;
	    maxTmp			= 0;
	    minTmp			= 0;
	    alrmTmpHigh		= 0;
	    alrmTmpLow		= 0;
	    error			= 0;
	    tagPwrMult			.setInt(1);    
	    tagOutVal			.setInt(0);
	    tagState			.setInt(0);
		tagFlSetHigh		.setInt(0);
	    tagTimeWrkLowCnt	.setInt(0);
	    tagTimeWrkHighCnt	.setInt(0);
	    tagTimePreCnt		.setInt( tagTimePreHeat.getInt() );
	    tagOldTmp			.setInt(0);
	}
	
	private int state;
	private int outval;
	private int minout;
	private int maxout;
	private int timePreCnt;
	private int pwrmult;
	
	@Override
	public boolean execute() {

		int susp = inpSusp.getInt();

		enable = inpEnable.getInt();
		curTmp = inpCurTmp.getInt();
		maxTmp = inpMaxTmp.getInt();
		minTmp = inpMinTmp.getInt();
		alrmTmpHigh = inpAlrmTmpHigh.getInt();
		alrmTmpLow = inpAlrmTmpLow.getInt();
		error = inpError.getInt();
		
		state = tagState.getInt();
		outval = tagOutVal.getInt();
		minout = tagMinOut.getInt();
		maxout = tagMaxOut.getInt();
		timePreCnt = tagTimePreCnt.getInt();
		pwrmult = tagPwrMult.getInt();

		// error
		if( error != 0 ) {
			state = (enable == 1) ? 3 : 4;
			outval = minout;
			timePreCnt = tagTimePreHeat.getInt();
		} else
			
		// not enabled
		if( enable == 0 ) {
			outval = minout;
			state = 0;
			timePreCnt = tagTimePreHeat.getInt();
		} else

		// suspended
		if( susp != 0 ) {
			state = 8;
		} else
		
		// enabled
		{
			if (curTmp < minTmp && timePreCnt > 0) {
				// preheating stage
				state = 1;
				outval = minout + (int)((float)((maxout - minout) * tagStartPwr.getInt()) / 100);
				timePreCnt--;
			} else {
				timePreCnt = 0;
				// increase
				if (curTmp < minTmp) {
					setPwrUp((curTmp < alrmTmpLow) ? 1 : 0);
					state = 5;
				} else
				// decrease
				if (curTmp > maxTmp) {
					setPwrDown((curTmp > alrmTmpHigh) ? 1 : 0);
					state = 6;
				} else
				// do nothing
				{
					state = 7;
					tagTimeWrkHighCnt.setInt( 0 );
					tagTimeWrkLowCnt.setInt( 0 );
					timePreCnt = 0;
				}
				tagOldTmp.setInt( curTmp );
			}
			
			// check limits
			if (outval < minout) {
				outval = minout;
			} else
			if (outval > maxout) {
				outval = maxout;
			}

		} 
		
		tagState.setInt(state);
		tagOutVal.setInt(outval);
		tagTimePreCnt.setInt(timePreCnt);
		tagPwrMult.setInt(pwrmult);


		return true;
	}	

	
	private void setPwrUp(int isAlarm) {
		if (tagTimeWrkHighCnt.getInt() == 0) {
			if (tagFlSetHigh.getInt() == 0) {
				tagFlSetHigh.setInt( 1 );
				pwrmult = 1;
				outval += (int)((float)((pwrmult + isAlarm) * 
						tagPwrStep.getInt() * (maxout - minout)) / 100);
			} else {
				if (tagOldTmp.getInt() >= curTmp) {
					outval += (int)((float)(((pwrmult + isAlarm) & 0x03) * 
							tagPwrStep.getInt() * (maxout - minout)) / 100);
					pwrmult = (pwrmult + 1) & 0x03;
				} else {
					pwrmult = 1;
				}
			}
			tagTimeWrkHighCnt.setInt( tagTimeWrkHigh.getInt() );
		} else {
			tagTimeWrkHighCnt.setInt( tagTimeWrkHighCnt.getInt() - 1 );;
		}
	}

	
	private void setPwrDown(int isAlarm) {
		if (tagTimeWrkLowCnt.getInt() == 0) {
			if (tagFlSetHigh.getInt() == 1) {
				tagFlSetHigh.setInt( 0 );
				pwrmult = 1;
				outval -= (int)((float)((pwrmult + isAlarm) * 
						tagPwrStep.getInt() * (maxout - minout)) / 100);
			} else {
				if (tagOldTmp.getInt() <= curTmp) {
					outval -= (int)((float)((pwrmult + isAlarm) * 
							tagPwrStep.getInt() * (maxout - minout)) / 100);
				}
			}
			tagTimeWrkLowCnt.setInt( tagTimeWrkLow.getInt() );
		} else {
			tagTimeWrkLowCnt.setInt( tagTimeWrkLowCnt.getInt() - 1 );
		}
	}

	
	@Override
	public void saveStateExtra(State _state) {
		_state.saveVar("enable", 			enable);
		_state.saveVar("time", 			time);
		_state.saveVar("curTmp", 			curTmp);
		_state.saveVar("maxTmp", 			maxTmp);
		_state.saveVar("minTmp", 			minTmp);
		_state.saveVar("alrmTmpHigh", 		alrmTmpHigh	);
		_state.saveVar("alrmTmpLow", 		alrmTmpLow	);
		_state.saveVar("error", 			error);

		// все теги сохраняет super-класс
//		_state.saveVar("outVal", 			tagOutVal			.getInt());
//		_state.saveVar("state", 			tagState 			.getInt());
//		_state.saveVar("pwrMult", 			tagPwrMult			.getInt());
//		_state.saveVar("flSetHigh", 		tagFlSetHigh		.getInt());
//		_state.saveVar("timeWrkLowCnt",	tagTimeWrkLowCnt 	.getInt());
//		_state.saveVar("timeWrkHighCnt",	tagTimeWrkHighCnt 	.getInt());
//		_state.saveVar("timePreCnt", 		tagTimePreCnt		.getInt());
//		_state.saveVar("oldTmp", 			tagOldTmp			.getInt());
	}

	
	
	@Override
	public void loadStateExtra(State _state) {
		enable			= _state.loadVar("enable", 			enable			);
		time			= _state.loadVar("time", 			time			);
		curTmp			= _state.loadVar("curTmp", 			curTmp			);
		maxTmp			= _state.loadVar("maxTmp", 			maxTmp			);
		minTmp			= _state.loadVar("minTmp", 			minTmp			);
		alrmTmpHigh		= _state.loadVar("alrmTmpHigh", 	alrmTmpHigh		);
		alrmTmpLow		= _state.loadVar("alrmTmpLow", 		alrmTmpLow		);
		error			= _state.loadVar("error", 			error			);

//		tagOutVal			.setInt( loadVar(_state, 	"outVal", 			tagOutVal.getInt()));
//		tagState			.setInt(  loadVar(_state, 	"state", 			tagState.getInt()));
//		tagPwrMult			.setInt( loadVar(_state, 	"pwrMult", 			tagPwrMult			.getInt()));
//		tagFlSetHigh		.setInt( loadVar(_state, 	"flSetHigh", 		tagFlSetHigh		.getInt()));
//		tagTimeWrkLowCnt 	.setInt( loadVar(_state, 	"timeWrkLowCnt",	tagTimeWrkLowCnt 	.getInt()));
//		tagTimeWrkHighCnt 	.setInt( loadVar(_state, 	"timeWrkHighCnt",	tagTimeWrkHighCnt 	.getInt()));
//		tagTimePreCnt		.setInt( loadVar(_state, 	"timePreCnt", 		tagTimePreCnt		.getInt()));
//		tagOldTmp			.setInt( loadVar(_state, 	"oldTmp", 			tagOldTmp			.getInt()));
	}

}
