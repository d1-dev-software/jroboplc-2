package promauto.jroboplc.plugin.roboplant;

public class ConnectDevice extends Device {
	protected Connector connector = null;
	
    @Override
	public boolean prepare() {
    	connector = new Connector(this);
    	return super.prepare();
    }


	@Override
	public boolean execute() {
		if( connector != null )
			connector.execute();
		
		return true;
	}
    

	@Override
	public String checkErrors() {
		if( connector != null )
			return connector.checkErrors();
		
		return "";
	}

}