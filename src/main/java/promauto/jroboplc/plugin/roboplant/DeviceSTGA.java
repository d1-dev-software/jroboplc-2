package promauto.jroboplc.plugin.roboplant;

import promauto.jroboplc.core.api.Tag;

import java.util.ArrayList;
import java.util.List;

public class DeviceSTGA extends Device {
	protected static class Group {
		Input inpNext;
		Tag tagOutput;
	}

	protected List<Group> groups 	= new ArrayList<>();

	Input inpInput;
	Input inpReset;
	Tag tagStage;
	Tag tagCycleCnt;
	Tag tagCycleMax;
	Tag tagStageStart;
	Tag tagStageLoop;
	Tag tagFlags;

	
    @Override
	public void prepareTags(RefBool res) {
		inpInput  	= getInput("Input", res);
		inpReset  	= getInput("Reset", res);

		tagStage 	 	 = getOutputTag( "Stage", res);
		tagCycleCnt 	 = getOutputTag( "CycleCnt", res);
    	tagCycleMax      = getOutputTag( "CycleMax", res);
    	tagStageStart 	 = getOutputTag( "StageStart", res);
    	tagStageLoop     = getOutputTag( "StageLoop", res);
    	tagFlags 		 = getOutputTag( "Flags", res);


		groups.clear();

    	int i = 1;
    	while (true) {
    		Group gr = new Group();
			gr.inpNext = getInput( "Next" + i );
    		Output output = getOutput("Output" + i);

    		if( gr.inpNext==null || output==null)
    			break;

    		gr.tagOutput = output.tag;
    		groups.add(gr);
    	    i++;
    	}
    	
	}

	
	@Override
	public boolean execute() {

    	int stage = tagStage.getInt();

    	if( inpReset.getInt() > 0  ||  groups.size() == 0  ||  stage < 0  ||  stage > groups.size() ) {
    		stage = 0;

		} else if( stage == 0 ) {
    		if( inpInput.getInt() > 0  &&  canStartCycle() ) {
				stage = Math.max(tagStageStart.getInt(), 1);
			}
		} else {
			Group gr = groups.get(stage - 1);
			stage = stage + gr.inpNext.getInt();

			if( stage > groups.size() ) {
				tagCycleCnt.setInt( tagCycleCnt.getInt() + 1 );
				if( inpInput.getInt() > 0  &&  canStartCycle() ) {
					stage = Math.max(tagStageLoop.getInt(), 1);
				} else {
					stage = 0;
				}
			} else if( stage < 0 ) {
				stage = 0;
			}
		}


    	tagStage.setInt(stage);

    	int curidx = stage - 1;
    	for( int i=0; i<groups.size(); ++i) {
			groups.get(i).tagOutput.setBool(i == curidx);
		}

		return true;
	}

	private boolean canStartCycle() {
		return (tagCycleMax.getInt() <= 0  ||  tagCycleCnt.getInt() < tagCycleMax.getInt());
	}

}