package promauto.jroboplc.plugin.roboplant;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import promauto.jroboplc.core.State;
import promauto.jroboplc.core.api.Tag;

public class DeviceSEQC extends Device {
	
	private static final int MF_NONE_STOPPABLE 		= 1;
	private static final int MF_TRANSPARENT_START 	= 2;
	private static final int MF_TRANSPARENT_WORK 	= 4;
	private static final int MF_FLOW_CONTROLLER 	= 8;
	private static final int MF_FLOW 				= 0x10;
	private static final int MF_WATCHING_ONLY 		= 0x20;
	private static final int MF_SOLE_OWNER 			= 0x40;

	Input inpStart;
	Input inpStop;
	Input inpStopEmrg;
	Input inpStopFlow;
	List<DeviceSEQC> otherSeqc = new LinkedList<>();

	Tag tagSost;
	Tag tagCurrentAddr;
	Tag tagAlarm;
	Tag tagCode;
	Tag tagCheck;
	Tag tagFlags;
	List<Tag> tagAddress 		= new ArrayList<>();
	List<Tag> tagDelays 		= new ArrayList<>();
	List<Tag> tagModeFlags 		= new ArrayList<>();

	DeviceSEQCItem[] mchb;

	int addressCount;
	int cnt;
	int curDelay;
	int podachaCnt;
	int current;
	int sost;

	@Override
	public void prepareTags(RefBool res) {
		inpStart    	= getInput("Start"		, res);
		inpStop     	= getInput("Stop"		, res);
		inpStopEmrg 	= getInput("StopEmrg"	, res);
		inpStopFlow 	= getInput("StopFlow"	, res);

		tagSost    		= getOutputTag("Sost"	, res);
		tagCurrentAddr 	= getOutputTag("Current", res);
		tagAlarm   		= getOutputTag("Alarm"	, res);
		tagCode    		= getOutputTag("Code"	, res);
		tagCheck   		= getOutputTag("Check"	, res);
		tagFlags   		= getOutputTag("Flags"	, res);
	
		prepareOtherSeqc();

		prepareOutputArrays();

		if (res.value)
			resetState();
	}

	
	private void prepareOtherSeqc() {
		for (Device dev: module.devicesByOrder)
			if (dev instanceof DeviceSEQC)
				if (dev != this)
					otherSeqc.add( (DeviceSEQC)dev );
	}


    
    
    	
    private void prepareOutputArrays() {
		tagAddress.clear();
		tagDelays.clear();
		tagModeFlags.clear();

		Output addr, dly, flg;
		int i=0;
		while (true) {
			addr = getOutput("Address" + i); 
			dly  = getOutput("Delays" + i); 
			flg  = getOutput("ModeFlags" + i); 
			
			if (addr==null || dly==null || flg==null)
				break;
			
			tagAddress.add(addr.tag);
			tagDelays.add(dly.tag);
			tagModeFlags.add(flg.tag);
			
			i++;
		}
		
		addressCount = i;
		
		initMchbItems();
	}

	protected void initMchbItems() {
		mchb = new DeviceSEQCItem[addressCount];
		for (int i = 0; i < addressCount; i++)
			mchb[i] = new DeviceSEQCItem();
	}

	protected void resetState() {
		tagSost.setInt(0);
		tagCurrentAddr.setInt(0xffff);
		tagAlarm.setInt(0);
		tagCode.setInt(0);
		tagCheck.setInt(0);

		cnt = 0;
		curDelay = 0;
		podachaCnt = 0;
		current = 0;
		sost = 0;
	}

	protected void linkMchbItems() {
		int addr;
		for (int i = 0; i < addressCount; i++) {
			addr = tagAddress.get(i).getInt();

			if (mchb[i].address != addr) {

				mchb[i].address = addr;
				mchb[i].reset();

				if (addr == 0xffff)
					break;

				Device d = module.getDevice(addr);
				Output output;
				if (d != null) {
					if ((output = d.getOutput(0)) != null)
						mchb[i].setTagMasterout( output.tag );

					if ((output = d.getOutput(1)) != null)
						mchb[i].setTagSost( output.tag );

					if ((output = d.getOutput(6)) != null)
						mchb[i].setTagControl( output.tag );
				} else {
					tagAddress.get(i).setInt(0xffff);
					mchb[i].address = 0xffff;
					mchb[i].reset();
				}
			}
		}
	}

	/*
	 * Warning! The source code of this method was ported from C++ original
	 * RoboplantMtr sources. Needs get refactored! Do not repeat this style!
	 */
	// @Override
	public boolean execute() {

		linkMchbItems();

		// инициализация переменных
		boolean _start = inpStart.getInt() > 0  &&  (tagCode.getInt() > 0)
				&& (tagCode.getInt() < 0xFFFF);
		boolean _stop = inpStop.getInt() > 0;
		boolean _emrgstop = inpStopEmrg.getInt() > 0;

		// ModeFlags
		// =============
		// b0 - Неотключаемость. Запрет оставнова при аварии предшествующего по
		// цепочке оборудования
		// =0 - откл
		// =1 - вкл
		//
		// b2-b1 - Прозрачность. Останов/отключение/авария оборудования не
		// останавливают последующее поцепочке оборудование
		// =0 - откл
		// =1 - вкл при работе
		// =2 - вкл при работе и запуске
		//
		// b3 - Контролирование подачи. Работа оборудования является
		// обязательным условием для включения и работы оборудования, подающего
		// продукт
		// =0 - откл
		// =1 - вкл
		//
		// b4 - Подача продукта. Данное оборудование подает продукт и зависит от
		// работы оборудования, контролирующего подачу
		// =0 - откл
		// =1 - вкл
		//
		// b15-b8 - Задержка времени останова подающего оборудования при
		// останове оборудования, контролирующего
		// подачу
		// =0 - откл
		// =1 - вкл

		// Проверка запущенной части цепочки
		boolean _podachaEn = inpStopFlow.getInt() == 0;

		int modeflags;
		if ((sost > 0) && (current > 0)) {
			for (int i = 0; i < current; i++) {
				modeflags = tagModeFlags.get(i).getInt();
				if ((modeflags & MF_FLOW_CONTROLLER) > 0)
					_podachaEn &= getMchbMasterOut(i) > 0;

				if (!_podachaEn)
					if ((modeflags & MF_FLOW) > 0)
						if (podachaCnt >= (modeflags / 0x100)) {
							stopMchbSequence(i, current, 0);
							current = i;
							sost = 1;
							break;
						}

				if ((getMchbMasterOut(i) == 0)  &&  ((modeflags & MF_TRANSPARENT_WORK) == 0)) { 
					stopMchbSequence(i + 1, current, 0);
					current = i;
					if (checkMchbInAlarm(i))
						sost = 4;
					else
						sost = 1;
				}

			}
		}

		if (_podachaEn)
			podachaCnt = 0;
		else if (podachaCnt < 256)
			podachaCnt++;

		// Проверка требуется останов от кнопоки Stop
		if ((sost == 1) || (sost == 2) || (sost == 4) || (sost == 6))
			if (_stop)
				sost = 3;

		// Проверка требуется останов от кнопоки EmrgStop
		if ((sost == 1) || (sost == 2) || (sost == 3) || (sost == 4) || (sost == 6))
			if (_emrgstop)
				sost = 5;

		// =======
		// Выкл
		// =======
		if (sost == 0) {
			if ((_start) && (!_stop) && (!_emrgstop)) {
				sost = 1; // **22**
				current = 0;
				cnt = 0;
			} else {
				tagAlarm.setInt(0);
			}
		}

		
		// =========
		// Устройство занято (приостанов запуска)
		// =========
		if( sost == 6 ){
			if( ((mchb[current].getControlValue() & 0x10) == 0) )
				sost = 1;
		}

		
		// =========
		// Запуск
		// =========
		if (sost == 1 ) {
			modeflags = tagModeFlags.get(current).getInt();
			
			if( ((modeflags & MF_SOLE_OWNER) > 0)  
					&&  mchb[current].state == 0
					&&  ((mchb[current].getControlValue() & 0x10) > 0) )
			{
				sost = 6;
			}
			
			else if (((modeflags & MF_FLOW) > 0)  &&  (!_podachaEn)) {
				setMchbControl(current, 0);
				cnt = 0;

			} 
			
			else {
				
				if (mchb[current].state == 0) {
					setMchbControl(current, 1);
					curDelay = getMchbStartDelay(current);
					cnt = 0;
				} else {
					// поддержание бита включения
					int ctrl = mchb[current].getControlValue();
															
					if ((ctrl & 0x10) == 0)
						if ((modeflags & MF_WATCHING_ONLY) == 0)
							mchb[current].setControlValue(ctrl | 0x10);
				}

				if (cnt < curDelay)
					cnt++;

				if (cnt >= curDelay) {
					if ((getMchbMasterOut(current) == 1)  || ((modeflags & MF_TRANSPARENT_START) > 0 )) {
						current++;

						boolean flag = true;
						if (current < addressCount)
							if (tagAddress.get(current).getInt() != 0xFFFF)
								flag = false;

						if (flag)
							sost = 2;
					} else {
						if (mchb[current].getSostValue() == 4  ||  mchb[current].getSostValue() == 5)
							sost = 4;
					}
				}
			}

		}

		// ======
		// Вкл
		// ======
		if (sost == 2) {
			setMchbControl(current, 1);
		}

		// ==========
		// Останов
		// ==========
		if (sost == 3) {

			boolean flag = false;
			if (current < addressCount)
				if (tagAddress.get(current).getInt() != 0xFFFF)
					flag = true;

			if (flag) {

				if (mchb[current].state == 1) {
					setMchbControl(current, 0);
					curDelay = getMchbStopDelay(current);
					cnt = 0;
				}

				if (cnt < curDelay)
					cnt++;

				if (cnt >= curDelay) {
					if (current == 0) {
						stopAll();
						sost = 0;
					} else
						current--;
				}

			} else {

				if (current == 0) {
					stopAll();
					sost = 0;
				} else
					current--;
			}

		}

		// =========
		// Авария
		// =========
		if (sost == 4) {
			if (!checkMchbInAlarm(current))
				if (_start) {
					sost = 1; // **33**
					tagAlarm.setInt(0);
				}
		}

		// =====================
		// Экстренный останов
		// =====================
		if (sost == 5) {
			stopAll();
			sost = 0;
		}

		// Определение CurrentAddr
		if (((sost == 1) || (sost == 3) || (sost == 4) || (sost == 6))
				&& (current < addressCount))
			tagCurrentAddr.setInt(tagAddress.get(current).getInt());
		else
			tagCurrentAddr.setInt(0xFFFF);

		tagAlarm.setInt( (sost == 4  ||  sost == 6)? 1: 0 );


		tagSost.setInt(sost);

		return true;

	}

	protected int getMchbMasterOut(int idx) {
		return mchb[idx].getMasteroutValue();
	}

	protected int getMchbStartDelay(int idx) {
		return tagDelays.get(idx).getInt() >> 8;
	}

	protected int getMchbStopDelay(int idx) {
		return tagDelays.get(idx).getInt() & 0xFF;
	}

	protected boolean checkMchbInAlarm(int idx) {
		int i = mchb[idx].getSostValue();
		return ((i == 4) || (i == 5));
	}

	protected void setMchbControl(int idx, int value) {
		int ctrl = mchb[idx].getControlValue();

		if (value > 0) {
			// включение
			mchb[idx].state = 1;

			if ((tagModeFlags.get(idx).getInt() & MF_WATCHING_ONLY) == 0)
				mchb[idx].setControlValue(ctrl | 0x10);

		} else {
			// выключение
			mchb[idx].state = 0;

			// проверка на использования в других маршрутах
			boolean flag = false;
			for (DeviceSEQC seqc : otherSeqc)
				if (seqc.checkMchbState(mchb[idx].address)) {
					flag = true;
					break;
				}

			if (!flag)
				if ((tagModeFlags.get(idx).getInt() & MF_WATCHING_ONLY) == 0)
					mchb[idx].setControlValue(ctrl & 0xEF);
		}
	}

	protected void stopMchbSequence(int idxbeg, int idxend, int stoparmo) {
		for (int i = idxbeg; i <= idxend; i++)
			if (((tagModeFlags.get(i).getInt() & MF_NONE_STOPPABLE) == 0) || (stoparmo > 0)) {
				mchb[i].state = 0;
				setMchbControl(i, 0);
			}
	}

	protected void stopAll() {
		for (int i = 0; i < addressCount; i++)
			if (mchb[i].address != 0xFFFF) {
				mchb[i].state = 0;
				setMchbControl(i, 0);
			} else
				break;
	}

	protected boolean checkMchbState(int addr) {
		boolean flag = false;

		for (int i = 0; i < addressCount; i++) {
			if (tagAddress.get(i).getInt() == addr) {
				if ((tagModeFlags.get(i).getInt() & MF_WATCHING_ONLY) == 0)
					flag = mchb[i].state > 0;
				else
					flag = false;
				break;
			} else

			if (tagAddress.get(i).getInt() == 0xFFFF)
				break;
		}

		return flag;
	}
	
	
	@Override
	public void saveStateExtra(State state) {
		state.saveVar("cnt"       , cnt        );
		state.saveVar("curDelay"  , curDelay   );
		state.saveVar("podachaCnt", podachaCnt );
		state.saveVar("current"   , current    );
		state.saveVar("sost"      , sost       );
		
		for (int i=0; i<addressCount; i++) {
			state.saveVar("state_" + i,  mchb[i].state );
		}
	}

	@Override
	public void loadStateExtra(State state) {
	    cnt        = state.loadVar("cnt"       , cnt        );
	    curDelay   = state.loadVar("curDelay"  , curDelay   );
	    podachaCnt = state.loadVar("podachaCnt", podachaCnt );
	    current    = state.loadVar("current"   , current    );
	    sost       = state.loadVar("sost"      , sost       );

	    for (int i=0; i<addressCount; i++) {
	    	mchb[i].state 	= state.loadVar("state_" + i,  	mchb[i].state );
		}
	}

	
}