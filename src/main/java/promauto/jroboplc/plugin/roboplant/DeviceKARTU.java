package promauto.jroboplc.plugin.roboplant;

import java.util.Map;

import promauto.jroboplc.core.State;
import promauto.jroboplc.core.api.Tag;

public class DeviceKARTU extends Device {

	private static int KART_WAIT = 0;
	private static int KART_WRK_1 = 1;
	private static int KART_WRK_2 = 2;
	private static int KART_WRK_3 = 3;
	private static int KART_WRK_4 = 4;
	private static int KART_WRK_5 = 5;

	private static int KART_CLN_1 = 6;
	private static int KART_CLN_2 = 7;
	private static int KART_CLN_3 = 8;
	private static int KART_CLN_4 = 9;
	private static int KART_CLN_5 = 10;
	private static int KART_CLN_6 = 11;
	private static int KART_CLN_7 = 12;
//	private static int KART_CLN_8 = 13;

//	Input inpEnable;
	Input inpWork;
	Input inpClean;
	Input inpDVU;
	Input inpCls1;
	Input inpMdl1;
	Input inpOpn1;
	Input inpCls2;
	Input inpMdl2;
	Input inpOpn2;
	Input inpCls3;
	Input inpMdl3;
	Input inpOpn3;
//	Input inpError;
	Input inpEmrgStop;

	Tag tagState;
	Tag tag_Wrk;
	Tag tag_Cln;
	Tag tag_Cls1;
	Tag tag_Mdl1;
	Tag tag_Opn1;
	Tag tag_Cls2;
	Tag tag_Mdl2;
	Tag tag_Opn2;
	Tag tag_Cls3;
	Tag tag_Mdl3;
	Tag tag_Opn3;
	Tag tag_Blow1;
	Tag tag_Blow2;
	Tag tag_Blow3;
	Tag tag_Blow4;
	Tag tagTimeOpCls;
	Tag tagTimeMdlOpn;
	Tag tagTimeClean;
	Tag tagCntClean;
	Tag tagTimeBlow1;
	Tag tagTimeBlow2;
	Tag tagTimeBlow3;
	Tag tagTimeBlow4;

	// helpers for inputs
	boolean Cls1;
	boolean Mdl1;
	boolean Opn1;
	boolean Cls2;
	boolean Mdl2;
	boolean Opn2;
	boolean Cls3;
	boolean Mdl3;
	boolean Opn3;

	// helpers for outputs ro
	int State;
	boolean _Wrk;
	boolean _Cls1;
	boolean _Mdl1;
	boolean _Opn1;
	boolean _Cls2;
	boolean _Mdl2;
	boolean _Opn2;
	boolean _Cls3;
	boolean _Mdl3;
	boolean _Opn3;
	boolean _Blow1;
	boolean _Blow2;
	boolean _Blow3;
	boolean _Blow4;


	// help elements
	int iCnt;
	boolean startWrk;
	boolean startCln;
	int startBlow;
	int prevBlow;
	
    @Override
	public void prepareTags(RefBool res) {
//    	inpEnable  		= getInput(	"Enable", res);
    	inpWork    		= getInput(	"Work", res);
    	inpClean   		= getInput(	"Clean", res);
    	inpDVU	   		= getInput(	"DVU", res);
    	inpCls1    		= getInput(	"Cls1", res);
    	inpMdl1    		= getInput(	"Mdl1", res);
    	inpOpn1    		= getInput(	"Opn1", res);
    	inpCls2    		= getInput(	"Cls2", res);
    	inpMdl2    		= getInput(	"Mdl2", res);
    	inpOpn2    		= getInput(	"Opn2", res);
    	inpCls3    		= getInput(	"Cls3", res);
    	inpMdl3    		= getInput(	"Mdl3", res);
    	inpOpn3    		= getInput(	"Opn3", res);
//    	inpError   		= getInput(	"Error", res);
    	inpEmrgStop		= getInput(	"EmrgStop", res);
    	
    	tagState    	= getOutputTag( "State", res);
    	tag_Wrk     	= getOutputTag( "_Wrk", res);
    	tag_Cln     	= getOutputTag( "_Cln", res);
    	tag_Cls1    	= getOutputTag( "_Cls1", res);
    	tag_Mdl1    	= getOutputTag( "_Mdl1", res);
    	tag_Opn1    	= getOutputTag( "_Opn1", res);
    	tag_Cls2    	= getOutputTag( "_Cls2", res);
    	tag_Mdl2    	= getOutputTag( "_Mdl2", res);
    	tag_Opn2    	= getOutputTag( "_Opn2", res);
    	tag_Cls3    	= getOutputTag( "_Cls3", res);
    	tag_Mdl3    	= getOutputTag( "_Mdl3", res);
    	tag_Opn3    	= getOutputTag( "_Opn3", res);
    	tag_Blow1   	= getOutputTag( "_Blow1", res);
    	tag_Blow2   	= getOutputTag( "_Blow2", res);
    	tag_Blow3   	= getOutputTag( "_Blow3", res);
    	tag_Blow4   	= getOutputTag( "_Blow4", res);
    	tagTimeOpCls	= getOutputTag( "TimeOpCls", res);
    	tagTimeMdlOpn	= getOutputTag( "TimeMdlOpn", res);
    	tagTimeClean	= getOutputTag( "TimeClean", res);
    	tagCntClean 	= getOutputTag( "CntClean", res);
    	tagTimeBlow1	= getOutputTag( "TimeBlow1", res);
    	tagTimeBlow2	= getOutputTag( "TimeBlow2", res);
    	tagTimeBlow3	= getOutputTag( "TimeBlow3", res);
    	tagTimeBlow4	= getOutputTag( "TimeBlow4", res);
    	
		if (res.value)
			resetState(); 
	}

	protected void resetState() {
	    tagState.setInt(0);
	    tagCntClean.setInt(0);
	    
	    iCnt = 0;
	    startBlow=0;
	    prevBlow=0;
	}

    
	/*
	 * Warning! The source code of this method was ported from C++ original
	 * RoboplantMtr sources. Needs get refactored! Do not repeat this style!
	 */
	@Override
	public boolean execute() {

		State = tagState.getInt();
		_Wrk = tag_Wrk.getInt() > 0;
		_Cls1 = tag_Cls1.getInt() > 0;
		_Mdl1 = tag_Mdl1.getInt() > 0;
		_Opn1 = tag_Opn1.getInt() > 0;
		_Cls2 = tag_Cls2.getInt() > 0;
		_Mdl2 = tag_Mdl2.getInt() > 0;
		_Opn2 = tag_Opn2.getInt() > 0;
		_Cls3 = tag_Cls3.getInt() > 0;
		_Mdl3 = tag_Mdl3.getInt() > 0;
		_Opn3 = tag_Opn3.getInt() > 0;
		_Blow1 = tag_Blow1.getInt() > 0;
		_Blow2 = tag_Blow2.getInt() > 0;
		_Blow3 = tag_Blow3.getInt() > 0;
		_Blow4 = tag_Blow4.getInt() > 0;

		if (inpEmrgStop.getInt() > 0) {
			State = KART_WAIT;
			return finish();
		}

		startWrk = inpWork.getInt() > 0;
		startCln = (inpClean.getInt() > 0) && (!startWrk);

		Cls1 = inpCls1.getInt() > 0;
		Mdl1 = inpMdl1.getInt() > 0;
		Opn1 = inpOpn1.getInt() > 0;
		Cls2 = inpCls2.getInt() > 0;
		Mdl2 = inpMdl2.getInt() > 0;
		Opn2 = inpOpn2.getInt() > 0;
		Cls3 = inpCls3.getInt() > 0;
		Mdl3 = inpMdl3.getInt() > 0;
		Opn3 = inpOpn3.getInt() > 0;

		if (startWrk) {
			if (State == KART_WAIT) {
				State = KART_WRK_1;
				iCnt = 0;
				return finish();
			}

			if (State == KART_WRK_1) {
//				_Cls1 = !(Mdl1 || Cls1);
//				_Cls2 = !(Mdl2 || Cls2);
//				_Cls3 = !(Mdl3 || Cls3);
				_Cls1 = !( Cls1);
				_Cls2 = !( Cls2);
				_Cls3 = !( Cls3);
				if (!(_Cls1 || _Cls2 || _Cls3) && (iCnt >= tagTimeOpCls.getInt())) {
					iCnt = 0;
					State = KART_WRK_2;
				} else {
					iCnt++;
				}
				return finish();
			}

			if (State == KART_WRK_2) {
				_Mdl1 = true;
				_Mdl2 = true;
				_Mdl3 = true;
				if (Mdl1 && Mdl2 && Mdl3 && iCnt >= tagTimeOpCls.getInt()) {
					_Wrk = true;
					State = KART_WRK_3;
				} else {
					iCnt++;
				}
				return finish();
			}

		} else if( inpDVU.getInt() == 0 ) {
			if(startCln && (State > KART_WAIT && State < KART_CLN_1)){
				_Mdl1 = false;
				_Mdl2 = false;
				_Mdl3 = false;
				State = KART_WAIT;
				return finish();
			}
			if (State == KART_WRK_3) {
				_Mdl1 = false;
				_Mdl2 = false;
				_Mdl3 = false;
				State = KART_WRK_4;
				return finish();
			}

			if (State == KART_WRK_4) {
				_Wrk = false;
				State = KART_WRK_5;
				return finish();
			}

			if (State > KART_WAIT && State < KART_CLN_1) {
				_Mdl1 = false;
				_Mdl2 = false;
				_Mdl3 = false;
				_Cls1 = true;
				_Cls2 = true;
				_Cls3 = true;
				if (Cls1 && Cls2 && Cls3)
					State = KART_WAIT;
				return finish();
			}
		}

		if (startCln && tagCntClean.getInt() <= tagTimeClean.getInt()) {
			/*
			 * cln_1 - close zdvs 
			 * cln_2 - zdvs to open pos 
			 * cln_3 - start Blowing
			 * cln_4 - flag _Cln 
			 * cln_5 - start counting 
			 * cln_6 - unflag _Cln
			 * cln_7 - stop blowing 
			 * cln_8 - close zdvs
			 */

			if (State == KART_WAIT) {
				State = KART_CLN_1;
				iCnt = 0;
				return finish();
			}

			if (State == KART_CLN_1) {

				iCnt = 0;
				State = KART_CLN_2;
			}

			if (State == KART_CLN_2) {
				_Opn1 = true;
				_Opn2 = true;
				_Opn3 = true;
				if (Opn1 && Opn2 && Opn3) {
					if (iCnt >= tagTimeOpCls.getInt()) {
						iCnt = 0;
						State = KART_CLN_3;
					} else
						iCnt++;
				} else {
					startBlow = 0;
				}
				return finish();
			}

			if (State == KART_CLN_3) {
				startBlow = 1;
				State = KART_CLN_4;
				return finish();
			}

			if (State == KART_CLN_4) {
				tag_Cln.setInt(1);
				;
				State = KART_CLN_5;
				return finish();
			}

			if (State == KART_CLN_5) {
				tagCntClean.setInt(tagCntClean.getInt() + 1);
			}

		} else {
			if (State == KART_CLN_5) {
				_Blow1 = false;
				_Blow2 = false;
				_Blow3 = false;
				_Blow4 = false;
				_Opn1 = false;
				_Opn2 = false;
				_Opn3 = false;
				startBlow=0;
				State = KART_CLN_6;
				return finish();
			}

			if (State == KART_CLN_6) {
				tag_Cln.setInt(0);
				State = KART_CLN_7;
				return finish();
			}

//			if (State == KART_CLN_7) {
//				startBlow = 0;
//				State = KART_CLN_8;
//				return finish();
//			}

			if (State >= KART_CLN_1) {
				_Opn1 = false;
				_Opn2 = false;
				_Opn3 = false;
				_Cls1 = true;
				_Cls2 = true;
				_Cls3 = true;
				if (Cls1 && Cls2 && Cls3)
					State = KART_WAIT;
				return finish();
			}
		}

		return finish();
	}
	
	private boolean finish() {
		if (startBlow == 1) {
			if (!(_Blow1 || _Blow2 || _Blow3 || _Blow4) && prevBlow == 0) {
				_Blow1 = true;
				iCnt = 0;
			}

			else if (_Blow1) {
				if (iCnt >= tagTimeBlow1.getInt()) {
					_Blow1 = false;
					prevBlow = 1;
					iCnt = 0;
				} else
					iCnt++;
			}

			else if (_Blow2) {
				if (iCnt >= tagTimeBlow2.getInt()) {
					_Blow2 = false;
					prevBlow = 2;
					iCnt = 0;
				} else
					iCnt++;
			}

			else if (_Blow3) {
				if (iCnt >= tagTimeBlow3.getInt()) {
					_Blow3 = false;
					prevBlow = 3;
					iCnt = 0;
				} else
					iCnt++;
			}

			else if (_Blow4) {
				if (iCnt >= tagTimeBlow4.getInt()) {
					_Blow4 = false;
					prevBlow = 4;
					iCnt = 0;
				} else
					iCnt++;
			}

			else
				switch (prevBlow) {
				case 1:
					if (iCnt >= tagTimeMdlOpn.getInt()) {
						_Blow2 = true;
					} else {
						iCnt++;
					}
					break;
				case 2:
					if (iCnt >= tagTimeMdlOpn.getInt()) {
						_Blow3 = true;
					} else {
						iCnt++;
					}
					break;
				case 3:
					if (iCnt >= tagTimeMdlOpn.getInt()) {
						_Blow4 = true;
					} else {
						iCnt++;
					}
					break;
				case 4:
					if (iCnt >= tagTimeMdlOpn.getInt()) {
						_Blow1 = true;
					} else {
						iCnt++;
					}
					break;
				}

		}

		if (State == KART_WAIT) {
			iCnt = 0;
			_Wrk = false;
			tag_Cln.setInt(0);
			_Cls1 = false;
			_Mdl1 = false;
			_Opn1 = false;
			_Cls2 = false;
			_Mdl2 = false;
			_Opn2 = false;
			_Cls3 = false;
			_Mdl3 = false;
			_Opn3 = false;
			_Blow1 = false;
			_Blow2 = false;
			_Blow3 = false;
			_Blow4 = false;
			prevBlow = 0;
			if (!startCln)
				tagCntClean.setInt(0);
		}

		setInt(tagState, State);
		setBool(tag_Wrk, _Wrk);
		setBool(tag_Cls1, _Cls1);
		setBool(tag_Mdl1, _Mdl1);
		setBool(tag_Opn1, _Opn1);
		setBool(tag_Cls2, _Cls2);
		setBool(tag_Mdl2, _Mdl2);
		setBool(tag_Opn2, _Opn2);
		setBool(tag_Cls3, _Cls3);
		setBool(tag_Mdl3, _Mdl3);
		setBool(tag_Opn3, _Opn3);
		setBool(tag_Blow1, _Blow1);
		setBool(tag_Blow2, _Blow2);
		setBool(tag_Blow3, _Blow3);
		setBool(tag_Blow4, _Blow4);

		return true;
	}

	private void setBool(Tag tag, boolean value) {
		if(tag.getBool() != value )
			tag.setBool(value);
	}

	private void setInt(Tag tag, int value) {
		if(tag.getInt() != value )
			tag.setInt(value);
	}

	@Override
	public void saveStateExtra(promauto.jroboplc.core.State state) {
		state.saveVar("iCnt", 		iCnt);
		state.saveVar("startBlow",  startBlow);
		state.saveVar("prevBlow",   prevBlow);
	}
	
	@Override
	public void loadStateExtra(promauto.jroboplc.core.State state) {
		iCnt    	= state.loadVar("iCnt", 	iCnt);
		startBlow   = state.loadVar("startBlow",startBlow);
		prevBlow    = state.loadVar("prevBlow", prevBlow);
	}

}
