package promauto.jroboplc.plugin.roboplant;

import java.util.Map;

import promauto.jroboplc.core.State;
import promauto.jroboplc.core.api.Tag;

public class DeviceTRIG extends Device {
	Input 	inpSet	;
	Input 	inpReset;
	Tag 	tagOutput;
	
	int output;
	
    @Override
	public void prepareTags(RefBool res) {

    	inpSet	 = getInput( "Set", res);
    	inpReset = getInput( "Reset", res);
    	
    	tagOutput	 = getOutputTag( "Output"		, res);

		if (res.value) 
			resetState();
	}


	protected void resetState() {
		tagOutput.setInt(0);
		output = 0;
	}

	@Override
	public boolean execute() {

		if (inpReset.getInt() > 0)
			output = 0;
		else if (inpSet.getInt() > 0)
			output = 1;

		tagOutput.setInt(output);

		return true;
	}

	
	
	@Override
	public void saveStateExtra(State state) {
		state.saveVar("output", 	output);
	}
	
	@Override
	public void loadStateExtra(State state) {
		output = state.loadVar("output", 	output);
	}

}