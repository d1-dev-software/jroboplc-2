package promauto.jroboplc.plugin.wessvr;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.AbstractCommand;
import promauto.jroboplc.core.DbHelper;
import promauto.jroboplc.core.api.*;

import java.sql.*;

public class CmdImport extends AbstractCommand {
	private final Logger logger = LoggerFactory.getLogger(CmdImport.class);


	@Override
	public String getName() {
		return "import";
	}

	@Override
	public String getUsage() {
		return "db";
	}

	@Override
	public String getDescription() {
		return "imports wessvr data from database db";
	}

	
	
	
	@Override
	public String execute(Console console, Module module, String args) {
		module.postCommand(this, console, module, args);
		return "";
	}


	@Override
	public void executePosted(Console console, Module module, String args) {
		
		WessvrModule w = (WessvrModule) module;

		Database dbdst = w.getDatabase();
		if( !dbdst.isConnected() ) {
			console.print( "Database " + dbdst.getName() + " is not connected\n" );
			return;
		}

		Module m = EnvironmentInst.get().getModuleManager().getModule(args);
		if( !(m instanceof Database) ) {
			console.print( "Module " + args + " is not a database\n" );
			return;
		}
		Database dbsrc = (Database)m;
		if( !dbsrc.isConnected() ) {
			console.print( "Database " + dbsrc.getName() + " is not connected\n" );
			return;
		}

		boolean hasModulesOutcalcmode = false;
		boolean hasMainIdbatch = false;
		try(Statement st = dbsrc.getConnection().createStatement() ) {
			hasModulesOutcalcmode = dbsrc.hasColumn(st, "", "modules", "outcalcmode");
			hasMainIdbatch = dbsrc.hasColumn(st, "", "main", "idbatch");
		} catch (SQLException e) {
			EnvironmentInst.get().printError(logger, e);
		}


		DbHelper.clear(console, dbdst,
				"arcidx, arcout, arcstatus, arcval, main, mainhr, stat, stathr, usermdl, " +
				"userprev, userrep, usertrd, modules, producttags, brigada, extradatabases, " +
				"previlegs, product, reports, trends, users, mgroup, mgroupmod, " +
				"smenalist, smenas, modtypes ");


		DbHelper.copytbl(console, dbsrc, dbdst, "brigada", "*");
		DbHelper.copytbl(console, dbsrc, dbdst, "extradatabases", "*");
		DbHelper.copytbl(console, dbsrc, dbdst, "modtypes", "*");
		DbHelper.copytbl(console, dbsrc, dbdst, "previlegs", "*");
		DbHelper.copytbl(console, dbsrc, dbdst, "product", "*");
		DbHelper.copytbl(console, dbsrc, dbdst, "reports", "*");
		DbHelper.copytbl(console, dbsrc, dbdst, "smenalist", "*");
		DbHelper.copytbl(console, dbsrc, dbdst, "smenas", "*");
		DbHelper.copytbl(console, dbsrc, dbdst, "trends", "*");
		DbHelper.copytbl(console, dbsrc, dbdst, "users", "*");

		DbHelper.copytbl(console, dbsrc, dbdst, "modules",
				"IDM, IDMT, IDSL, NAME, DESCR, ENABLED, SVRNAME, ARCVALSIZE, ARCOUTSIZE, ARCOUTPER, ARCOUTTIME, WMUL, " +
						"PROTOCOLVERSION, ARCSTATUS_OFF " +
						(hasModulesOutcalcmode?", OUTCALCMODE": ""));

		DbHelper.copytbl(console, dbsrc, dbdst, "arcidx", "*", "idm", "modules");
		DbHelper.copytbl(console, dbsrc, dbdst, "arcout", "*", "idm", "modules");
		DbHelper.copytbl(console, dbsrc, dbdst, "arcstatus", "*", "idm", "modules");
		DbHelper.copytbl(console, dbsrc, dbdst, "arcval", "*", "idm", "modules");
		DbHelper.copytbl(console, dbsrc, dbdst, "mgroup", "*");
		DbHelper.copytbl(console, dbsrc, dbdst, "mgroupmod", "*", "idm", "modules");
		DbHelper.copytbl(console, dbsrc, dbdst, "producttags", "*", "idm", "modules");
		DbHelper.copytbl(console, dbsrc, dbdst, "usermdl", "*", "idm", "modules");
		DbHelper.copytbl(console, dbsrc, dbdst, "userprev", "*");
		DbHelper.copytbl(console, dbsrc, dbdst, "userrep", "*");
		DbHelper.copytbl(console, dbsrc, dbdst, "usertrd", "*");


		DbHelper.copytbl(console, dbsrc, dbdst, "main",
				"IDM, PERTYPE, DTBEG, WES, SWBEG, SWEND, WNUM, WNBEG, WNEND, OUTWES, OUTNUM, IDPROD, IDBRIG"+
						(hasMainIdbatch? ", IDBATCH": ""));

		DbHelper.copytbl(console, dbsrc, dbdst, "mainhr",
				"IDM, PERTYPE, DTBEG, WES, SWBEG, SWEND, WNUM, WNBEG, WNEND, OUTWES, OUTNUM, IDPROD, IDBRIG"+
						(hasMainIdbatch? ", IDBATCH": ""));


		DbHelper.copytbl(console, dbsrc, dbdst, "stat",
				"IDR, IDM, PERTYPE, PERIOD, DTBEG, DTEND, WES, SWBEG, SWEND, WNUM, WNBEG, WNEND, OUTWES, OUTNUM, IDPROD, IDBRIG"+
						(hasMainIdbatch? ", IDBATCH": ""));


		DbHelper.copytbl(console, dbsrc, dbdst, "stathr",
				"IDR, IDM, PERTYPE, PERIOD, DTBEG, DTEND, WES, SWBEG, SWEND, WNUM, WNBEG, WNEND, OUTWES, OUTNUM, IDPROD, IDBRIG"+
						(hasMainIdbatch? ", IDBATCH": ""));



		DbHelper.copygen(console, dbdst, "modules", 			"gen_modules_id", 		"idm");
		DbHelper.copygen(console, dbdst, "brigada", 			"gen_brigada_id", 		"idbrig");
		DbHelper.copygen(console, dbdst, "extradatabases", 	"gen_extradatabases_id","id");
		DbHelper.copygen(console, dbdst, "modtypes", 		"gen_modtypes_id", 		"idmt");
		DbHelper.copygen(console, dbdst, "product", 			"gen_product_id", 		"idprod");
		DbHelper.copygen(console, dbdst, "reports", 			"gen_reports_id", 		"idrep");
		DbHelper.copygen(console, dbdst, "smenalist", 		"gen_smenalist_id", 	"idsl");
		DbHelper.copygen(console, dbdst, "smenas", 			"gen_smenas_id", 		"idsmena");
		DbHelper.copygen(console, dbdst, "trends", 			"gen_trends_id", 		"idtrd");
		DbHelper.copygen(console, dbdst, "users", 			"gen_users_id", 		"iduser");
		DbHelper.copygen(console, dbdst, "mgroup", 			"gen_mgroup_id", 		"idgroup");
		DbHelper.copygen(console, dbdst, "producttags", 		"gen_producttags_id", 	"id");
		DbHelper.copygen(console, dbdst, "stat", 			"gen_stat_id", 			"idr");
		DbHelper.copygen(console, dbdst, "stathr", 			"gen_stathr_id", 		"idr");


		console.print("Done\n");
	}

/*
	private void clear(Console console, Database dbsrc, Database dbdst, String strTables) {
		String[] tbls = strTables.split("\\s*,\\s*");
		try(Statement st = dbdst.getConnection().createStatement() ) {
			for(String tbl: tbls) {
				console.print("clear " + tbl + ":");
				String sql = "delete from " + tbl;
				st.executeUpdate(sql);
				dbdst.commit();
				console.print(" OK\n");
			}
		} catch (Exception e) {
			EnvironmentInst.get().printError(logger, e);
			try {
				dbdst.rollback();
			} catch (Exception e1) {
			}
		}
	}

	private void copygen(Console console, Database dbdst, String tbl, String gen, String id) {
		console.print("set " + gen + ":");
		String sql = "select max(" + id + ") from " + tbl;
		try(Statement st = dbdst.getConnection().createStatement(); ResultSet rs = st.executeQuery(sql)) {
			if( rs.next() ) {
				int value = rs.getInt(1);
				sql = "ALTER SEQUENCE " + gen + " RESTART WITH " + value;
				st.executeUpdate(sql);
			}
			dbdst.commit();
		} catch (Exception e) {
			EnvironmentInst.get().printError(logger, e);
			try {
				dbdst.rollback();
			} catch (Exception e1) {
			}
		}
		console.print(" OK\n");
	}


	private void copytbl(Console console, Database dbsrc, Database dbdst, String tbl, String strColumns, boolean checkIdm) {

		console.print( "copy " + tbl + ": " );

		try(Statement stsrc = dbsrc.getConnection().createStatement() ) {

			Set<Integer> idms = new HashSet<>();;
			if( checkIdm ) {
				String sql = "select idm from modules";
				try(ResultSet rs = stsrc.executeQuery(sql)) {
					while (rs.next())
						idms.add(rs.getInt(1));
				}
			}

			String sql = "select " + strColumns + " from " + tbl;
			try(ResultSet rs = stsrc.executeQuery(sql)) {
				ResultSetMetaData md = rs.getMetaData();
				int n = md.getColumnCount();

				if (strColumns.equals("*")) {
					strColumns = "";
					for (int i = 1; i <= n; ++i)
						strColumns += (i > 1 ? ", " : "") + md.getColumnName(i);
				}

				sql = "insert into " + tbl + " (" + strColumns + ") values (";

				for (int i = 1; i <= n; ++i)
					sql += i > 1 ? ", ?" : "?";
				sql += ')';


				boolean skip = false;
				int cnt1 = 0;
				int cnt2 = tbl.length() + 7;
				PreparedStatement dst = dbdst.getConnection().prepareStatement(sql);
				while (rs.next()) {
					skip = false;
					for (int i = 1; i <= n; ++i) {
						if (checkIdm && md.getColumnName(i).equals("IDM"))
							if (!idms.contains(rs.getInt(i))) {
								skip = true;
								break;
							}

						switch (md.getColumnType(i)) {
							case Types.SMALLINT:
							case Types.INTEGER:
							case Types.BIGINT:
								dst.setLong(i, rs.getLong(i));
								break;
							case Types.TIMESTAMP:
								dst.setTimestamp(i, rs.getTimestamp(i));
								break;
							default:
								dst.setString(i, rs.getString(i));
								break;
						}
					}

					if (skip)
						continue;

					dst.executeUpdate();
					if (cnt1++ > 500) {
						cnt1 = 0;
						console.print("*");
						dbdst.commit();
						if (cnt2++ > 80) {
							cnt2 = 0;
							console.print("\n");
						}
					}
				}
			}

			dbdst.commit();
			dbsrc.commit();
			console.print(" OK\n");

		} catch (Exception e) {
			EnvironmentInst.get().printError(logger, e);
			try {
				dbsrc.rollback();
			} catch (Exception e1) {
			}
			try {
				dbdst.rollback();
			} catch (Exception e1) {
			}
		}
	}
*/

}
