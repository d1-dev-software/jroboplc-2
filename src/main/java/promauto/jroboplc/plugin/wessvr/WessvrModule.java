package promauto.jroboplc.plugin.wessvr;


import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.AbstractModule;
import promauto.jroboplc.core.api.*;
import promauto.utils.ParamsMap;

public class WessvrModule extends AbstractModule implements Signal.Listener {
	private final Logger logger = LoggerFactory.getLogger(WessvrModule.class);

	private ArchiveConfig archiveConfig = new ArchiveConfig(this);
	private Shiftsets shiftsets = new Shiftsets(this);
	private ProductTags productTags = new ProductTags(this);

	private String databaseModuleName;
	private String svrname;
	private String schema;
//	private List<String> tagSrcNames = null;

	private Database database;
//	private List<Module> tagSrcModules = new LinkedList<>();
	private List<Device> devices = new ArrayList<>();

	private Statement statement;
	private boolean singleTransaction;
	private boolean connected;
	private Tag tagConnected;
	private boolean errorOnInit = false;
	private boolean wesSvrStateEnabled;
	private String lastSql;
    private boolean modeReload;

	public WessvrModule(Plugin plugin, String name) {
        super(plugin, name);
        env.getCmdDispatcher().addCommand(this, CmdImport.class);
        env.getCmdDispatcher().addCommand(this, CmdSweep.class);
        env.getCmdDispatcher().addCommand(this, CmdWmaxlist.class);
        env.getCmdDispatcher().addCommand(this, CmdWmax.class);
    }

	public ArchiveConfig getArchiveConfig() {
		return archiveConfig;
	}

	public Shiftsets getShiftsets() {
		return shiftsets;
	}

	public ProductTags getProductTags() {
		return productTags;
	}

	public String getDatabaseModuleName() {
		return databaseModuleName;
	}

	public String getSvrname() {
		return svrname;
	}

//	public List<String> getTagSrcNames() {
//		return tagSrcNames;
//	}

	public List<Device> getDevices() {
		return devices;
	}

	public boolean isSingleTransaction() {
		return singleTransaction;
	}

	private Statement getStatement() {
		return statement;
	}

//	public List<Module> getTagSrcModules() {
//		return tagSrcModules;
//	}

	public Database getDatabase() {
		return database;
	}

	public String getSchema() {
		return schema;
	}

	public String makeTableName(String table) {
		return database.makeSchemaObjectName(schema, table);
	}



	public final boolean loadModule(Object conf) {
		Configuration cm = env.getConfiguration();
		databaseModuleName = cm.get(conf, "database", "db");
		svrname = cm.get(conf, "svrname", "");
		schema = cm.get(conf, "schema", "");
		singleTransaction = cm.get(conf, "singleTransaction", true);
		wesSvrStateEnabled = cm.get(conf, "wesSvrStateEnabled", true);

//		tagSrcNames =  cm.toList(cm.get(conf, "tagsources")).stream()
//				.map(Object::toString)
//				.collect(Collectors.toList());


		archiveConfig.load(conf);

		if( (tagConnected = tagtable.get("connected")) == null )
			tagConnected = tagtable.createBool("connected", false);

        modeReload = false;
		return true;
	}
	

	@Override
	public final boolean prepareModule() {
		
		Module module = env.getModuleManager().getModule(databaseModuleName);
		if( module == null ) {
			env.printError(logger, name, "Not found a database module:", databaseModuleName );
			return false;
		}

		if( !(module instanceof Database) ) {
			env.printError(logger, name, "Not a database:", databaseModuleName );
			return false;
		}

		if( modeReload )
			return true;

		database = (Database)module;
		database.addSignalListener(this);
//		database.registerEventNotification("SYNC_READER");
		
        String resourceName = "dbscr/dbscr.wessvr.yml";
        if( database.loadScriptFromResource(WessvrPlugin.class, resourceName)==null )
        	return false;

        // find tag sources
//		tagSrcModules.clear();
//		Module tsmod;
//        if( tagSrcNames != null ) {
//			for (String tsname : tagSrcNames)
//				if ((tsmod = env.getModuleManager().getModule(tsname)) != null)
//					tagSrcModules.add(tsmod);
//				else {
//					env.printError(logger, name, "Not found tag source module:", tsname);
//					return false;
//				}
//		}


		init();
		
		return true;
	}


	@Override
	public void onSignal(Module sender, Signal signal) {
		if( signal.type == Signal.SignalType.CONNECTED ) {
			if( !connected ) {
				init();
				postSignal(Signal.SignalType.RELOADED);
			}
		} else

		if( signal.type == Signal.SignalType.DISCONNECTED ) {
            tagConnected.setBool( connected = false );
		}
	}



	private void init() {
		errorOnInit = false;
		tagConnected.setBool( connected = false );

		if( !enable  ||  database == null  ||  !database.isConnected())
			return;

		boolean res = true;

        ParamsMap params = new ParamsMap();
        res &= database.executeScript("wessvr.init1", params ).success;
		res &= database.executeScript("wessvr.init2", params ).success;
		res &= database.executeScript("wessvr.update", params ).success;

		res &= archiveConfig.createTables();

		res &= shiftsets.init();

		res &= productTags.init();

		res &= initDevices();

		errorOnInit = !res;
        tagConnected.setBool( connected = res );

        if( errorOnInit )
            env.printError(logger, name, "Initialization error");
	}



	private boolean initDevices() {
		boolean result = true;
		devices.clear();
		
		try(Statement st = database.getConnection().createStatement() ) {
			statement = st;
			String sql = String.format(
					"select mt.name as modtype, m.* from %s m " +
							"join %s mt on m.idmt=mt.idmt " +
							"where m.svrname='%s' order by m.idm",
                    makeTableName("modules"),
					makeTableName("modtypes"),
					svrname );
			
			try(ResultSet rs = st.executeQuery(sql)) {
				while (rs.next()) {
					String modtype = rs.getString("modtype");
					Device wd = DeviceFactory.create(this, modtype);
					if (wd != null && wd.load(rs))
						devices.add(wd);
					else
						env.printError(logger, name, "Unknown MODTYPE:", modtype);
				}
			}

			for(Device wd: devices)
				if( !wd.init() ) {
					result = false;
					break;
				}

			database.commit();
		} catch (SQLException e) {
			env.printError(logger, e, name, "lastSql:", lastSql);
			database.rollback();
			result = false;
		}
		statement = null;

		return result;
	}




	@Override
	public boolean closedownModule() {
		if (!enable) return true;

		tagConnected.setBool( connected = false );

		if( database != null)
			database.removeSignalListener(this);
		
		
		return true;
	}


	@Override
	public boolean executeModule() {
//		long t = System.currentTimeMillis();

		if( !connected  ||  !database.isConnected() )
			return true;

        Connection con = database.getConnection();
		try(Statement st = con.createStatement() ) {
			statement = st;

			productTags.execute();

            for(Device wd: devices)
            	if( wd.isEnable() ) {
					wd.execute();
					if( !singleTransaction )
						database.commit();
            	}

			st.close();

			if( singleTransaction )
				database.commit();

		} catch (Exception e) {
			env.printError(logger, e, name, "lastSql:", lastSql);
			database.rollback();
		}
		statement = null;

//		System.out.print( "w=" + (System.currentTimeMillis() - t) + "   ");


		return true;
	}



    public boolean isWesSvrStateEnabled() {
        return wesSvrStateEnabled;
    }

    public void executeUpdate(String sql) throws SQLException {
        lastSql = sql;
        statement.executeUpdate(sql);
    }



    public ResultSet executeQuery(String sql) throws SQLException {
        lastSql = sql;
        return statement.executeQuery(sql);
    }



    @Override
	public String getInfo() {
		if( !enable )
			return "disabled";

		StringBuilder sb = new StringBuilder();

		sb.append(
				(errorOnInit? ANSI.RED + ANSI.BOLD + "ERROR! " + ANSI.RESET: "") +
				databaseModuleName + ", " +
				(svrname.isEmpty()? "": "svrname=" + svrname + ", ")  +
				(schema.isEmpty()? "": "schema=" + schema + ", ") +
				devices.size() + " devices, " +
				(connected? "connected": "NOT CONNECTED!") );

		for(Device device: devices) {
			sb.append("\r\n");
			sb.append( device.getInfo() );
		}

		return sb.toString();
	}


	
	@Override
	protected boolean reload() {
		WessvrModule tmp = new WessvrModule( plugin, name );
		boolean res = tmp.load();
		tmp.modeReload = true;
		res &= tmp.prepare();
		tmp.closedown();

		if( res ) {
			Object conf = env.getConfiguration().getModuleConf(	plugin.getPluginName(),	name);
			closedown();
			if( load(conf) )
				if( prepare() )
					res = true;
		}


		return res;
	}


}