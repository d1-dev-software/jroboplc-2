package promauto.jroboplc.plugin.wessvr;

import promauto.jroboplc.core.tags.RefGroup;
import promauto.jroboplc.core.tags.RefItem;

public class DeviceTenzomTb09 extends DeviceBase {
    public static final String MODTYPE = "TenzoM_TB009";

    protected RefItem refSumWeightHigh;
    protected RefItem refSumWeightLow;
    protected RefItem refSumNumHigh;
    protected RefItem refSumNumLow;
    protected RefItem refLastWeightHigh;
    protected RefItem refLastWeightLow;
    protected RefItem refLastTimeHigh;
    protected RefItem refLastTimeLow;
    protected RefItem refOutputHigh;
    protected RefItem refOutputLow;
    protected RefItem refVersion;

    int version;


    public DeviceTenzomTb09(WessvrModule module) {
        super(module);
        sizeSumWeight = 0x1_0000_0000L;
        sizeSumNum = 0x1_0000_0000L;
        version = 0;
    }

    @Override
    public String getModtype() {
        return MODTYPE;
    }


    @Override
    protected void createRefs() {
        super.createRefs();

        addVldZero( addVldFFFF( refSumWeightHigh  = refs.createItemCrc(name, "SumWeightHigh")));
        addVldZero( addVldFFFF( refSumWeightLow   = refs.createItemCrc(name, "SumWeightLow")));
        addVldZero( addVldFFFF( refSumNumHigh     = refs.createItemCrc(name, "SumNumHigh")));
        addVldZero( addVldFFFF( refSumNumLow      = refs.createItemCrc(name, "SumNumLow")));
        addVldZero( addVldFFFF( refLastWeightHigh = refs.createItemCrc(name, "LastWeightHigh")));
        addVldZero( addVldFFFF( refLastWeightLow  = refs.createItemCrc(name, "LastWeightLow")));
        addVldZero( addVldFFFF( refLastTimeHigh   = refs.createItemCrc(name, "LastTimeHigh")));
        addVldZero( addVldFFFF( refLastTimeLow    = refs.createItemCrc(name, "LastTimeLow")));
        addVldZero( addVldFFFF( refOutputHigh     = refs.createItemCrc(name, "OutputHigh")));
        addVldZero( addVldFFFF( refOutputLow      = refs.createItemCrc(name, "OutputLow")));

        refs.addItemCrc(refErrorFlag);

        refVersion      = refs.createItemCrc(name, "Version");

    }



    @Override
    protected long getSumWeight() {
        return (((long)(refSumWeightHigh.getValue().getInt())) << 16) + refSumWeightLow.getValue().getInt();
    }


    @Override
    protected long getSumNum() {
        return (((long)(refSumNumHigh.getValue().getInt())) << 16) + refSumNumLow.getValue().getInt();
    }



    @Override
    protected boolean updateValuesArcval() {
        if( arcvalValues[0] == getSumWeight()  &&  arcvalValues[1] == getSumNum() )
            return false;

        arcvalValues[0] = getSumWeight();
        arcvalValues[1] = getSumNum();
        arcvalValues[2] = (((long)(refLastWeightHigh.getValue().getInt())) << 16) + refLastWeightLow.getValue().getInt();
        arcvalValues[3] = (((long)(refLastTimeHigh.getValue().getInt())) << 16) + refLastTimeLow.getValue().getInt();
        arcvalValues[4] = 0;

        return true;
    }


    @Override
    protected double getArcoutValue() {
        return (((refOutputHigh.getValue().getInt())) << 16) + refOutputLow.getValue().getInt();
    }


    @Override
    protected void doAfterRefsRead() {
        if( refVersion.getValue().getInt() == 1 ) {
            sizeSumWeight = 1_000_000_000L;
            sizeSumNum = 1_000_000_000L;
        } else {
            sizeSumWeight = 0x1_0000_0000L;
            sizeSumNum = 0x1_0000_0000L;
        }
    }

}
