package promauto.jroboplc.plugin.wessvr;

import java.time.LocalDateTime;
import java.util.*;

public class Shiftset {

    private class Shift {
        int num;
        int timebeg;

        public Shift(int num, int timebeg) {
            this.num = num;
            this.timebeg = timebeg;
        }
    }

    private int firstTimeAfterMidnight;

    private Comparator<Shift> shiftComparator = (o1, o2) -> o2.timebeg - o1.timebeg;
    private SortedSet<Shift> shifts = new TreeSet<>(shiftComparator);


    public void addShift(int num, int timebeg) {
        shifts.add( new Shift(num, timebeg) );
        firstTimeAfterMidnight = shifts.last().timebeg;
    }

    public int getShiftNum(LocalDateTime ldt) {
        int time = ldt.getHour() * 100 + ldt.getMinute();

        for(Shift sh: shifts)
            if( time >= sh.timebeg )
                return sh.num;

        return shifts.first().num;
    }


    public boolean isAfterMidnight(LocalDateTime ldt) {
        int time = ldt.getHour() * 100 + ldt.getMinute();
        return time < firstTimeAfterMidnight;
    }

}
