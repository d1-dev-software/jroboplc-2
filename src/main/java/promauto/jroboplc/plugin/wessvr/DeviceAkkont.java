package promauto.jroboplc.plugin.wessvr;

import promauto.jroboplc.core.tags.RefGroup;
import promauto.jroboplc.core.tags.RefItem;

public class DeviceAkkont extends DeviceBase {

	public static final String MODTYPE = "Akkont 908";

	protected RefItem refCrc;
	protected RefItem refSumWesHigh;
	protected RefItem refSumWesLow;
	protected RefItem refWNum;
	protected RefItem refLastWes;
	protected RefItem refCurWes;
	protected RefItem refStageNum;
	protected RefItem refWCycle;
	protected RefItem refOutput;


	public DeviceAkkont(WessvrModule module) {
		super(module);
		sizeSumWeight = 4_000_000_000L;
		sizeSumNum = 65536;
	}

	@Override
	public String getModtype() {
		return MODTYPE;
	}


	@Override
	protected void createRefs() {
		super.createRefs();
		refCrc			 	= refs.createItemCrcSum(name, "Crc16");

		addVldZero( addVldFFFF( refSumWesHigh 	= refs.createItemCrc(name, "SumWesHigh")));
		addVldZero( addVldFFFF( refSumWesLow 	= refs.createItemCrc(name, "SumWesLow")));
		addVldZero( addVldFFFF( refWNum 		= refs.createItemCrc(name, "WNum")));
		addVldZero( addVldFFFF( refLastWes 		= refs.createItemCrc(name, "LastWes")));

		refs.addItemCrc(refErrorFlag);

		refCurWes  = refs.createItem(name, "CurWes");
		refStageNum = refs.createItem(name, "StageNum");
		refWCycle  = refs.createItem(name, "WCycle");
		refOutput  = refs.createItem(name, "Output");
	}



	@Override
	protected long getSumWeight() {
		return (((long)(refSumWesHigh.getValue().getInt())) << 16) + refSumWesLow.getValue().getInt();
	}


	@Override
	protected long getSumNum() {
		return refWNum.getValue().getInt();
	}


	@Override
	protected boolean updateValuesStatus() {
		if( !hasStatusValuesChanged(null, refStageNum) )
			return false;

		statusValues[1] = refStageNum.getValue().getInt();

		return true;
	}


	@Override
	protected boolean updateValuesArcval() {
		if( arcvalValues[0] == getSumWeight()  &&  arcvalValues[1] == getSumNum() )
			return false;

		arcvalValues[0] = getSumWeight();
		arcvalValues[1] = getSumNum();
		arcvalValues[2] = refLastWes.getValue().getInt();
		arcvalValues[3] = refWCycle.getValue().getInt();
		arcvalValues[4] = refStageNum.getValue().getInt();

		return true;
	}


	@Override
	protected double getArcoutValue() {
		return refOutput.getValue().getDouble();
	}


}
