package promauto.jroboplc.plugin.wessvr;

import promauto.jroboplc.core.tags.RefGroup;
import promauto.jroboplc.core.tags.RefItem;

public class DeviceAtara extends DeviceBase {

	public static final String MODTYPE = "Atara";

	protected RefItem refCrc;
	protected RefItem refWeightPHigh;
	protected RefItem refWeightPLow;
	protected RefItem refCurWeight;
	protected RefItem refOutput;


	public DeviceAtara(WessvrModule module) {
		super(module);
		sizeSumWeight = 0x1_0000_0000L;
		sizeSumNum = 0x1_0000_0000L;
	}

	@Override
	public String getModtype() {
		return MODTYPE;
	}


	@Override
	protected void createRefs() {
		super.createRefs();
		refCrc			 	= refs.createItemCrcSum(name, "Crc16");

		addVldZero( addVldFFFF( refs.createItemCrc(name, "WeightTHigh")));
		addVldZero( addVldFFFF( refs.createItemCrc(name, "WeightTLow")));
		addVldZero( addVldFFFF( refWeightPHigh = refs.createItemCrc(name, "WeightPHigh")));
		addVldZero( addVldFFFF( refWeightPLow = refs.createItemCrc(name, "WeightPLow")));

		refs.addItemCrc(refErrorFlag);

		refCurWeight = refs.createItem(name, "CurWeight");
		refOutput  = refs.createItem(name, "Output");
	}



	@Override
	protected long getSumWeight() {
		return (((long)(refWeightPHigh.getValue().getInt())) << 16) + refWeightPLow.getValue().getInt();
	}




	@Override
	protected boolean updateValuesArcval() {
		if( arcvalValues[0] == getSumWeight() )
			return false;

		arcvalValues[0] = getSumWeight();

		return true;
	}


	@Override
	protected double getArcoutValue() {
		return refOutput.getValue().getDouble();
	}


}
