package promauto.jroboplc.plugin.wessvr;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.AbstractCommand;
import promauto.jroboplc.core.api.Console;
import promauto.jroboplc.core.api.Database;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.Module;

import java.sql.ResultSet;
import java.sql.Statement;

public class CmdWmax extends AbstractCommand {
	private final Logger logger = LoggerFactory.getLogger(CmdWmax.class);


	@Override
	public String getName() {
		return "wmax";
	}

	@Override
	public String getUsage() {
		return "[idm|all] val";
	}

	@Override
	public String getDescription() {
		return "sets wesincmax setting for a module with idm or for all ";
	}

	
	
	
	@Override
	public String execute(Console console, Module module, String args) {
		module.postCommand(this, console, module, args);
		return "";
	}


	@Override
	public void executePosted(Console console, Module module, String args) {

        WessvrModule w = (WessvrModule) module;

        Database db = w.getDatabase();
        if (!db.isConnected()) {
            console.print("Database " + db.getName() + " is not connected\n");
            return;
        }

        String[] ss = args.split("\\s+");
        if( ss.length != 2 )
            return;

        String idm = ss[0];
        String val = ss[1];

        try( Statement st = db.getConnection().createStatement()) {
            String sql = "update modules set wesincmax=" + val;
            if( !idm.toUpperCase().equals("ALL"))
                sql += " where idm=" + ss[0];

            int cnt = st.executeUpdate(sql);
            if( cnt > 0 )
                console.print("OK\n");

            db.commit();
        } catch (Exception e) {
            EnvironmentInst.get().printError(logger, e);
            try {
                db.rollback();
            } catch (Exception e1) {
            }
        }

    }



}
