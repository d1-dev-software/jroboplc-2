package promauto.jroboplc.plugin.wessvr;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.api.ANSI;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.RefGroup;
import promauto.jroboplc.core.tags.RefItem;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

public abstract class DeviceBase implements Device {
	private final Logger logger = LoggerFactory.getLogger(DeviceBase.class);

	private static final int CRC_ERROR_CNT_MAX = 10;
	private static final int STATE_DISABLED = 100;
	private static final int STATE_START = 99;
	private static final int STATE_OK = 0;
	private static final int STATE_ERROR_CONNECT = 1;
	private static final int STATE_ERROR_CRC = 2;
	private static final int STATE_ERROR_VALID = 3;
	private static final int STATE_ERROR_LINK = 4;


	private static final class ArcoutCalcRec  implements Comparable<ArcoutCalcRec> {
		long timems;
		long weight;

		public ArcoutCalcRec(long timems, long weight) {
			this.timems = timems;
			this.weight = weight;
		}

		@Override
		public int compareTo(ArcoutCalcRec o) {
			return timems == o.timems? 0: (timems > o.timems? 1: -1);
		}
	}

	protected final WessvrModule module;

	protected Shiftset shiftset;

	protected final RefGroup refs = new RefGroup();
	protected RefItem refErrorFlag;
	protected RefItem refUpdateTime;
	protected RefItem refWesSvrState;

	protected List<RefItem> vldZero = new ArrayList<>();
	protected List<RefItem> vldFFFF = new ArrayList<>();

	protected Tag tagState;
//	protected Tag tagOutput;
	protected Tag tagDescr;

	protected int idm;
	protected int idsl;
	protected String name;
	protected String descr;
	protected boolean enable;
	protected int arcvalSize;
	protected int arcoutSize;
	protected int arcoutPer;
	protected int arcoutTime;

	protected int arcvalIdr = 0;
	protected int arcoutIdr = 0;
	protected int statusIdr = 0;

	protected LocalDateTime statidleDt;
	protected int statidleTimeSec;

	protected double wmul;
	protected int protocolVersion;
	protected int arcStatusOff;
	protected int outCalcMode;
	protected int wesIncMax;

	protected long sizeSumWeight;
	protected long sizeSumNum;

	private List<ArchiveBase> archives = null;

	protected int[] statusValues = new int[5];
	protected long[] arcvalValues = new long[5];

    private Queue<Double> arcoutAvgQueue;// = new LinkedList<>();
	private int arcoutAvgQueueSize;

	private SortedSet<ArcoutCalcRec> arcoutCalcQueue;// = new LinkedList<>();
	private long arcoutLastWeight = -1;
	private long arcoutLastMs = 0;

	private boolean firstPass = true;
	private int crcErrorCnt = 0;

	public DeviceBase(WessvrModule module) {
		this.module = module;
		sizeSumWeight = 0x1_0000_0000L;
		sizeSumNum = 0x1_0000_0000L;
	}


	@Override
	public boolean load(ResultSet rs) throws SQLException {
	    idm = rs.getInt("IDM");
		enable = rs.getBoolean("ENABLED");
		name = rs.getString("NAME");
		descr = rs.getString("DESCR");
		idsl = rs.getInt("IDSL");
	    arcvalSize = rs.getInt("ARCVALSIZE");
	    arcoutSize = rs.getInt("ARCOUTSIZE");
	    arcoutPer = rs.getInt("ARCOUTPER") * 1000;
	    arcoutTime = rs.getInt("ARCOUTTIME") * 1000;
		statidleTimeSec = rs.getInt("TIMEIDLE");

	    wmul = rs.getInt("WMUL");
	    protocolVersion = rs.getInt("PROTOCOLVERSION"); 
	    arcStatusOff = rs.getInt("ARCSTATUS_OFF");
		outCalcMode = rs.getInt("OUTCALCMODE");
		wesIncMax = rs.getInt("WESINCMAX");

	    return true;
	}


	@Override
	public boolean isEnable() {
		return enable;
	}

	@Override
	public String getInfo() {
		return
				(tagState.getInt()<=STATE_OK? "": ANSI.RED + ANSI.BOLD + getStateAsString() + ANSI.RESET + " ")
				+ idm + " " + getModtype() + "/" + name + ", " + descr
				+ (tagState.getInt()!=STATE_OK? "": ", w=" + getSumWeight() + ", n=" + getSumNum() );

	}

	private String getStateAsString() {
		switch(tagState.getInt()) {
			case STATE_DISABLED: return "DISABLED";
			case STATE_START: return "START";
			case STATE_OK: return "OK";
			case STATE_ERROR_CONNECT: return "CONNECTION ERROR";
			case STATE_ERROR_CRC: return "CRC ERROR";
			case STATE_ERROR_VALID: return "DATA IS NOT VALID";
			case STATE_ERROR_LINK: return "NOT LINKED";
		}
		return "";
	}


	protected Tag getOrCreateTag(Tag.Type type, String tagname) {
		tagname = name + '.' + tagname;
		Tag tag = module.getTagTable().get(tagname);
		if( tag == null )
			tag = module.getTagTable().createTag(type, tagname, 0);
		return tag;
	}


	@Override
	public boolean init() throws SQLException {
		EnvironmentInst.get().printInfo(logger, module.getName(), ANSI.GREEN + ANSI.BOLD +  "init:", idm + " " + name + ANSI.RESET);

		shiftset = module.getShiftsets().getShiftset(idsl);

		createTags();

		createRefs();
		refs.prepare();

		archives = module.getArchiveConfig().items.stream()
				.map(item -> new ArchiveBase(this, item))
				.collect(Collectors.toList());

		if( enable ) {
			arcvalIdr = selectMaxIdr("arcval");
			arcoutIdr = selectMaxIdr("arcout");
			statusIdr = selectMaxIdr("arcstatus");
			statidleDt = selectDtByIdr("arcval", arcvalIdr);

			applyState(STATE_START);

			if( outCalcMode == 0 ) {
				arcoutAvgQueueSize = (arcoutPer <= 0) || (arcoutTime < arcoutPer) ? 1 : arcoutTime / arcoutPer;
				arcoutAvgQueue = new LinkedList<>();
			} else {
				initArcoutCalcQueue();
			}

		} else
			tagState.setInt( STATE_DISABLED );

        return true;
	}


	private int selectMaxIdr(String table) throws SQLException {
		String sql = String.format(
				"select max(idr) from %s where idm=%d",
				module.makeTableName(table),
				idm );

		try(ResultSet rs = module.executeQuery(sql)) {
			if (rs.next()) {
				return rs.getInt(1);
			}
		}

		return 0;
	}


	private LocalDateTime selectDtByIdr(String table, int idr) throws SQLException {
		String sql = String.format(
				"select dt from %s where idr=%d and idm=%d",
				module.makeTableName(table),
				idr,
				idm );

		try(ResultSet rs = module.executeQuery(sql)) {
			if (rs.next()) {
				return rs.getTimestamp(1).toLocalDateTime();
			}
		}

		return LocalDateTime.now();
	}



	private void createTags() {
		tagState = getOrCreateTag(Tag.Type.INT, "state");
//		tagOutput = getOrCreateTag(Tag.Type.INT, "output");
		tagDescr = getOrCreateTag(Tag.Type.STRING, "descr");
		tagDescr.setString(descr);
	}



	protected void createRefs() {
		refErrorFlag = refs.createItem(name, "SYSTEM.ErrorFlag");
		refUpdateTime = refs.createItem(name, "SYSTEM.UpdateTime");
		refWesSvrState = refs.createItem(name, "WesSvrState");
	}


	protected boolean checkCrc() {
		return refs.checkCrc16();
	}


	protected boolean linkRefs() {
		return refs.link();
	}


	@Override
	public void execute() throws SQLException {
		if( !linkRefs() ) {
			applyState(STATE_ERROR_LINK);
		} else {
			refs.read();
			doAfterRefsRead();

			if (refErrorFlag.getValue().getBool()) {
				applyState(STATE_ERROR_CONNECT);
                archives.stream().forEach( ArchiveBase::onConnectError );
			} else

			if (!checkCrc()) {
				applyState(STATE_ERROR_CRC);
			} else

			if (!isRefDataValid()) {
				applyState(STATE_ERROR_VALID);
			} else {
				applyState(STATE_OK);

				for (ArchiveBase archive : archives)
					archive.execute();

				if( firstPass ) {
					firstPass = false;
					updateValuesStatus();
					updateValuesArcval();
				}

				executeArcVal();

				executeArcOut();

				executeArcStatus();
			}

			updateWesSvrState();
		}

//		updateIndexes();
	}

	protected void updateWesSvrState() {
		if( module.isWesSvrStateEnabled() ) {
			refWesSvrState.getTag().setInt(2);
		}
	}


	RefItem addVldZero(RefItem item) {
		vldZero.add( item );
		return item;
	}

	RefItem addVldFFFF(RefItem item) {
		vldFFFF.add( item );
		return item;
	}

	private boolean validateZero() {
		return vldZero.size() == 0  ||  !vldZero.stream().allMatch(a -> a.getValue().getInt() == 0);
	}

	private boolean validateFFFF() {
		return vldFFFF.size() == 0  ||  !vldFFFF.stream().allMatch(a -> a.getValue().getInt() == 0xFFFF);
	}

	private boolean isRefDataValid() {
		return validateZero()  &&  validateFFFF();
	}


	protected long getSumWeight() {
		// needs to be overridden
		return 0;
	}

	protected long getSumNum() {
		// needs to be overridden
		return 0;
	}



	private void applyState(int state) throws SQLException {
		if( state != tagState.getInt() ) {

			// ignore first time crc error
			if( state == STATE_ERROR_CRC) {
				if( crcErrorCnt < CRC_ERROR_CNT_MAX) {
					crcErrorCnt++;
					return;
				}
			} else
				crcErrorCnt = 0;


			tagState.setInt( state );

			deleteByIdr("arcstatus", ++statusIdr, arcvalSize);

			String sql = String.format(
					"insert into %s (idr, idm, dt, noconnect, nodata, status1, status2," +
							"status3, status4, status5) values (%d, %d, '%s', %d, %d, null, null," +
							" null, null, null)",
					module.makeTableName("arcstatus"),
					statusIdr,
					idm,
					LocalDateTime.now().format( module.getDatabase().getTimestampFormatter() ),
					refErrorFlag.getValue() == null? 0: refErrorFlag.getValue().getInt(),
					state);
			module.executeUpdate(sql);
		}
	}


	// ARCSTATUS
	private void executeArcStatus() throws SQLException {
		if( !updateValuesStatus() )
			return;

		deleteByIdr("arcstatus", ++statusIdr, arcvalSize);


		String sql = String.format(
				"insert into %s (idr, idm, dt, noconnect, nodata, status1, status2, status3, status4, status5) " +
						"values (%d, %d, '%s', %d, %d, %d, %d, %d, %d, %d)",
				module.makeTableName("arcstatus"),
				statusIdr,
				idm,
				refs.getDtRead().format( module.getDatabase().getTimestampFormatter() ),
				refErrorFlag.getValue().getInt(),
				tagState.getInt(),
				statusValues[0], statusValues[1], statusValues[2], statusValues[3], statusValues[4] );
		module.executeUpdate(sql);
	}


	private void deleteByIdr(String table, int idr, int size) throws SQLException {
		String sql = String.format(
				"delete from %s where idr<%d and idm=%d",
				module.makeTableName(table),
				idr - size,
				idm);
		module.executeUpdate(sql);
	}


	// ARCVAL
	private void executeArcVal() throws SQLException {
		if( !updateValuesArcval() )
			return;

		deleteByIdr("arcval", ++arcvalIdr, arcvalSize);


		String sql = String.format(
				"update or insert into %s (idr, idm, dt, sumwes, wnum, lastwes, wcycle, errcode) " +
						"values (%d, %d, '%s', %d, %d, %d, %d, %d)",
				module.makeTableName("arcval"),
				arcvalIdr,
				idm,
				refs.getDtRead().format( module.getDatabase().getTimestampFormatter() ),
				arcvalValues[0], arcvalValues[1], arcvalValues[2], arcvalValues[3], arcvalValues[4] );
		module.executeUpdate(sql);

		executeStatIdle();
	}


	private void executeStatIdle() throws SQLException {
		if( statidleTimeSec <= 0 )
			return;

		int seconds = (int)(statidleDt.until(refs.getDtRead(), ChronoUnit.SECONDS));

		if( seconds > statidleTimeSec ) {
			String sql = String.format(
					"insert into %s (idm, dtbeg, dtend, seconds) " +
							"values (%d, '%s', '%s', %d)",
					module.makeTableName("statidle"),
					idm,
					statidleDt.format(module.getDatabase().getTimestampFormatter()),
					refs.getDtRead().format(module.getDatabase().getTimestampFormatter()),
					seconds);
			module.executeUpdate(sql);
		}

		statidleDt = refs.getDtRead();
	}


	boolean hasStatusValuesChanged(RefItem... items) {
		int mask = 1;
		int n = Math.min(items.length, statusValues.length);
		for(int i=0; i<n; ++i) {
			if( (arcStatusOff & mask) == 0  &&  items[i] != null  &&  items[i].getTag().getInt() != statusValues[i] )
				return true;
			else
				mask <<= 1;
		}
		return false;
	}



	// ARCOUT
	private void executeArcOut() throws SQLException {

		if( outCalcMode != 0 )
			updateArcoutCalcValue();

		long t = System.currentTimeMillis();
		if( (t - arcoutLastMs) < arcoutPer )
			return;
		arcoutLastMs = t;

		double arcoutAvgValue = 0;

		if( outCalcMode == 0 ) {
			arcoutAvgValue = getArcoutAvgValue();
		} else {
			arcoutAvgValue = getArcoutCalcValue();
		}
//		tagOutput.setDouble(arcoutAvgValue);

		deleteByIdr("arcout", ++arcoutIdr, arcoutSize);

		String sql = String.format(Locale.ROOT,
				"insert into %s (idr, idm, dt, outwes ) " +
						"values (%d, %d, '%s', %f )",
				module.makeTableName("arcout"),
				arcoutIdr,
				idm,
				refs.getDtRead().format( module.getDatabase().getTimestampFormatter() ),
				arcoutAvgValue );
		module.executeUpdate(sql);
	}


	private double getArcoutAvgValue() {
        double arcoutValue = getArcoutValue();

        if( arcoutValue <= 0.001 ) {
            arcoutAvgQueue.clear();
            return 0;
        }

        arcoutAvgQueue.add(arcoutValue);
		while (arcoutAvgQueue.size() > arcoutAvgQueueSize)
            arcoutAvgQueue.poll();
		return arcoutAvgQueue.stream().mapToDouble(a -> a).average().getAsDouble();
	}


	private void updateArcoutCalcValue() {
		long curtimems = System.currentTimeMillis();

		while( arcoutCalcQueue.size() > 0  &&  curtimems - arcoutCalcQueue.first().timems > arcoutTime)
			arcoutCalcQueue.remove(arcoutCalcQueue.first());

		while( arcoutCalcQueue.size() > 0  &&  curtimems <= arcoutCalcQueue.last().timems)
			arcoutCalcQueue.remove( arcoutCalcQueue.last() );

		if( arcoutLastWeight < 0 )
			arcoutLastWeight = getSumWeight();


		if( arcoutLastWeight != getSumWeight() ) {
			arcoutLastWeight = getSumWeight();
			arcoutCalcQueue.add(new ArcoutCalcRec(curtimems, arcoutLastWeight));
		}
	}


	private double getArcoutCalcValue() {
		if( arcoutCalcQueue.size() == 0 )
			return 0;

		long weight = arcoutCalcQueue.last().weight - arcoutCalcQueue.first().weight;
		if( weight < 0 )
			weight = sizeSumWeight - arcoutCalcQueue.first().weight + arcoutCalcQueue.last().weight;
		long time = arcoutCalcQueue.last().timems - arcoutCalcQueue.first().timems;

		// check for idle
		long avgDoseTime = time / arcoutCalcQueue.size();
		long timeSinceLastDosing = System.currentTimeMillis() - arcoutCalcQueue.last().timems;
		if( avgDoseTime > 0  &&  timeSinceLastDosing / avgDoseTime >= 3 )
			return 0;

		double output = time > 1?
				3600_000D * (double)weight / (double)time:
				0;

		return output;
	}


	private void initArcoutCalcQueue() throws SQLException {
		arcoutCalcQueue = new TreeSet<>();

		LocalDateTime dt = LocalDateTime.now();
		long curtimems = System.currentTimeMillis();

		LocalDateTime dtbeg = dt.minusSeconds( arcoutTime / 1000);

		String sql = String.format(
				"select dt, sumwes from %s where idm=%d and dt>'%s' and dt<'%s' order by dt",
				module.makeTableName("arcval"),
				idm,
				dtbeg.format(module.getDatabase().getTimestampFormatter()),
				dt.format(module.getDatabase().getTimestampFormatter()) );

		try(ResultSet rs = module.executeQuery(sql)) {
			while (rs.next()) {
				long time = curtimems - ChronoUnit.MILLIS.between(rs.getTimestamp(1).toLocalDateTime(), dt);
				long weight = rs.getLong(2);
				arcoutCalcQueue.add(new ArcoutCalcRec(time, weight));
			}
		}
	}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	protected boolean updateValuesStatus() {
		// needs to be overridden
		return false;
	}


	protected boolean updateValuesArcval() {
		// needs to be overridden
		return false;
	}


	protected double getArcoutValue() {
		// needs to be overridden
		return 0.0D;
	}

	protected void doAfterRefsRead() {
		// needs to be overridden (optional)
	}


}
