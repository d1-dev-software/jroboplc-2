package promauto.jroboplc.plugin.wessvr;

import promauto.jroboplc.core.tags.RefGroup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.tags.RefItem;

/**
 * Created by alexeybel on 22.05.18.
 */
public class DeviceMercuryECnt extends DeviceBase{
    private final Logger logger = LoggerFactory.getLogger(DeviceMercuryECnt.class);
    public static final String MODTYPE = "MercuryECnt";
    protected RefItem refCrc;
    protected RefItem refPSumAfterResetH;     //tagPSumAfterResetH
    protected RefItem refPSumAfterResetL;     //tagPSumAfterResetL
    protected RefItem refSerialNumberH;       //tagSerialNumberH
    protected RefItem refSerialNumberL;       //tagSerialNumberL


    public DeviceMercuryECnt(WessvrModule module) {
        super(module);
        sizeSumWeight   = 0x1_0000_0000L;
        sizeSumNum      = 0x1_0000_0000L;
    }

    @Override
    public String getModtype() {
        return this.MODTYPE;
    }

    @Override
    protected void createRefs() {
        super.createRefs();
        refCrc			 	= refs.createItemCrcSum(name, "Crc16");

        addVldZero( addVldFFFF( refSerialNumberH 	 = refs.createItemCrc(name, "SerialNumberH")));
        addVldZero( addVldFFFF( refSerialNumberL 	 = refs.createItemCrc(name, "SerialNumberL")));

        addVldZero( addVldFFFF( refPSumAfterResetH   = refs.createItemCrc(name, "PSumAfterResetH")));
        addVldZero( addVldFFFF( refPSumAfterResetL   = refs.createItemCrc(name, "PSumAfterResetL")));

        refs.addItemCrc(refErrorFlag);
    }

    @Override
    protected long getSumWeight() {
        return (((long)(refPSumAfterResetH.getValue().getInt())) << 16) + refPSumAfterResetL.getValue().getInt();
    }


    @Override
    protected long getSumNum() {
        return (((long)(refSerialNumberH.getValue().getInt())) << 16) + refSerialNumberL.getValue().getInt();
    }


    @Override
    protected boolean updateValuesStatus() {
//        env.printError(logger, name, "Not a database:", databaseModuleName );
        return true;
    }



    @Override
    protected boolean updateValuesArcval() {
        if( arcvalValues[0] == getSumWeight())
            return false;

        arcvalValues[0] = getSumWeight();
        arcvalValues[1] = getSumNum();

        return true;
    }


    @Override
    protected double getArcoutValue() {
        return (((long)(refPSumAfterResetH.getValue().getInt())) << 16) + refPSumAfterResetL.getValue().getInt();
    }


}
