package promauto.jroboplc.plugin.wessvr;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.api.EnvironmentInst;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

public class Shiftsets {
    private final Logger logger = LoggerFactory.getLogger(Shiftsets.class);

    private final WessvrModule module;
    private final Map<Integer,Shiftset> shiftsets = new HashMap<>();

    public Shiftsets(WessvrModule module) {
        this.module = module;
    }



    public boolean init() {

        shiftsets.clear();

        try(Statement st = module.getDatabase().getConnection().createStatement();
            ResultSet rs = st.executeQuery(
                    "select s.idsl, s.smenanum, s.timebeg from smenalist sl join smenas s on s.idsl=sl.idsl order by idsl, timebeg desc") ) {

            while( rs.next() ) {
                int idsl = rs.getInt(1);
                int num = rs.getInt(2);
                int timebeg = rs.getInt(3);

                Shiftset sh = shiftsets.get(idsl);
                if( sh == null ) {
                    sh = new Shiftset();
                    shiftsets.put(idsl, sh);
                }

                sh.addShift(num, timebeg);
            }
            st.close();
            module.getDatabase().commit();
        } catch (SQLException e) {
            EnvironmentInst.get().printError(logger, e, module.getName());
            module.getDatabase().rollback();
            return false;
        }

        return true;
    }



    public Shiftset getShiftset(int idsl) {
        Shiftset sh = shiftsets.get(idsl);
        if( sh == null) {
            sh = new Shiftset();
            sh.addShift(0, 0);
        }
        return sh;
    }
}
