package promauto.jroboplc.plugin.wessvr;


import promauto.jroboplc.core.AbstractPlugin;
import promauto.jroboplc.core.api.Module;

public class WessvrPlugin extends AbstractPlugin {
	private static final String PLUGIN_NAME = "wessvr";
//	private final Logger logger = LoggerFactory.getLogger(WessvrPlugin.class);

	@Override
	public void initialize() {
		super.initialize();
	}

	@Override
	public String getPluginName() {
		return PLUGIN_NAME;
	}

	@Override
	public String getPluginDescription() {
		return "weight data archiver";
	}

//	@Override
//	public Class<?> getModuleClass() {
//		return WessvrModule.class;
//	}

	
	@Override
	public Module createModule(String name, Object conf) {
    	Module m = createDatabaseModule(name, conf);
    	if( m != null )
    		modules.add(m);
    	return m;
		
	}
	
	public Module createDatabaseModule(String name, Object conf) {
		WessvrModule m = new WessvrModule(this, name);
    	modules.add(m);
    	return m.load(conf)? m: null;
	}



}
