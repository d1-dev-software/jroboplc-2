package promauto.jroboplc.plugin.wessvr;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.tags.Ref;
import promauto.jroboplc.core.tags.RefGroup;
import promauto.jroboplc.core.tags.RefItem;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ProductTags {
    private final Logger logger = LoggerFactory.getLogger(ProductTags.class);

    private final WessvrModule module;
    private final RefGroup refs = new RefGroup();

    private final class Item {
        RefItem ref;
        int idm;
        int idprod;
        int value = 0;
    }

    List<Item> items = new ArrayList<>();


    public ProductTags(WessvrModule module) {
        this.module = module;
    }



    public boolean init() {

//        refs.setTagSrcModules( module.getTagSrcModules() );

        items.clear();

        String sql = String.format(
                "SELECT P.IDM, P.IDPROD, P.TAGNAME FROM %s P, MODULES M WHERE P.IDM=M.IDM AND M.SVRNAME='%s'",
                module.makeTableName("producttags"),
                module.getSvrname());

        try(Statement st = module.getDatabase().getConnection().createStatement();
            ResultSet rs = st.executeQuery( sql ) ) {

            Pattern p = Pattern.compile("(.+)\\.(.+)");
            while( rs.next() ) {
                Item item = new Item();
                item.idm = rs.getInt(1);
                item.idprod = rs.getInt(2);

                Matcher m = p.matcher( rs.getString(3) );
                if( !m.find() )
                    continue;
                item.ref = refs.createItem(m.group(1), m.group(2));

                items.add(item);
            }
            st.close();
            module.getDatabase().commit();
        } catch (SQLException e) {
            EnvironmentInst.get().printError(logger, e, module.getName());
            module.getDatabase().rollback();
            return false;
        }


        refs.prepare();

        return true;
    }



    public void execute() throws SQLException {
        refs.link();
        refs.read();

        boolean hasChanges = false;
        for(Item item: items)
            if( item.ref.isLinked()  &&  item.value != item.ref.getTag().getInt() ) {
                item.value = item.ref.getTag().getInt();

                if( item.ref.getTag().getInt() > 0 ){
                    String sql = String.format("update %s set idprod=%d where idm=%d",
                            module.makeTableName("modules"),
                            item.idprod,
                            item.idm
                    );

                    module.executeUpdate(sql);
                    hasChanges = true;
                }
            }

        if( hasChanges )
            module.getDatabase().commit();
    }


}
