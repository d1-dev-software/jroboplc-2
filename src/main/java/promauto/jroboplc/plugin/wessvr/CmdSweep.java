package promauto.jroboplc.plugin.wessvr;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.AbstractCommand;
import promauto.jroboplc.core.DbHelper;
import promauto.jroboplc.core.api.Console;
import promauto.jroboplc.core.api.Database;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.Module;

import java.sql.*;
import java.util.HashSet;
import java.util.Set;

public class CmdSweep extends AbstractCommand {
	private final Logger logger = LoggerFactory.getLogger(CmdSweep.class);


	@Override
	public String getName() {
		return "sweep";
	}

	@Override
	public String getUsage() {
		return "year";
	}

	@Override
	public String getDescription() {
		return "deletes old records from archive tables for specified year and all before (dtbeg <= 'year.12.31 23:59:59.999')";
	}

	
	
	
	@Override
	public String execute(Console console, Module module, String args) {
		module.postCommand(this, console, module, args);
		return "";
	}


	@Override
	public void executePosted(Console console, Module module, String args) {

        WessvrModule w = (WessvrModule) module;

        Database db = w.getDatabase();
        if (!db.isConnected()) {
            console.print("Database " + db.getName() + " is not connected\n");
            return;
        }

        int year;
        try {
            year = Integer.parseInt(args);
        } catch (NumberFormatException e) {
            console.print("Invalid year: " + args + "\n");
            return;
        }

        console.print("\n");
        for (ArchiveConfig.Item item : w.getArchiveConfig().items) {
            DbHelper.sweep(
                    console,
                    db,
                    db.makeSchemaObjectName(w.getSchema(), item.tblStat),
                    "dtbeg",
                    year);
        }
        console.print("OK\n");
    }



}
