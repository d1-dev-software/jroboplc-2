package promauto.jroboplc.plugin.database;

import java.util.Arrays;
import java.util.List;

import promauto.jroboplc.core.api.Console;
import promauto.jroboplc.core.api.Module;
import promauto.utils.ParamsMap;

public class CmdExec extends CmdSql {
	

	@Override
	public String getName() {
		return "exec";
	}

	@Override
	public String getUsage() {
		return "scrfile [prm=val ...]";
	}

	@Override
	public String getDescription() {
		return "executes all dbscr in a file with given parameters";
	}


	@Override
	public void executePosted(Console console, Module module, String args) {
		
		DatabaseModule m = (DatabaseModule) module;
		
		String[] ss = args.split("\\s* \\s*");
		if( ss.length < 1  ||  ss[0].isEmpty() ) {
			console.print( getRefuseAnswer(m) + "\n" );
			return;
		}

		String filename = ss[0];
		ParamsMap params = new ParamsMap( Arrays.copyOfRange(ss,  1, ss.length) );

		List<String> loaded = m.loadScriptFromFile(filename);
		if( loaded == null) {
			console.print( "Script " + filename + " is not found\r\n" );
			return;
		}
		
		for(String scr: loaded)
			m.executeScript(scr, params);
		
	}




}
