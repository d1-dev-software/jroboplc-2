package promauto.jroboplc.plugin.database;

import java.sql.SQLException;
import java.sql.Statement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.api.EnvironmentInst;

public class DbscrActionDo extends DbscrAction {
	private final Logger logger = LoggerFactory.getLogger(DbscrActionDo.class);
	
	private String sql;

	@Override
	public boolean init(Dbscr dbscr, String conf) {
		super.init(dbscr, conf);
		sql = conf;
		return true;
	}

	
	@Override
	public boolean execute(Statement statement) throws SQLException {
		
		String injected_sql = dbscr.injectParameters(sql);

		EnvironmentInst.get().printInfo(logger, dbscr.getDatabase().getName(), "Action:\n" + injected_sql);
		
		statement.executeUpdate( injected_sql );
		
		return true;
	}

}