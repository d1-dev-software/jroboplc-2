package promauto.jroboplc.plugin.database;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DbscrActionIf extends DbscrAction {

	@FunctionalInterface
	public interface BooleanSupplierThr {
	    boolean execute(Statement statement) throws SQLException;
	}

	private static Pattern pattern = Pattern.compile("((?:not)?)\\s*(\\w+)\\((.*)\\)");
	private boolean not;
	private String cmd;
	private String argline;
	private String[] args;
	private BooleanSupplierThr func;

 
	@Override
	public boolean init(Dbscr dbscr, String conf)  {
		super.init(dbscr, conf);
		
		Matcher m = pattern.matcher(conf);
		if( !m.find() ) 
			return false;

		not = !m.group(1).isEmpty();
		cmd = m.group(2).trim();
		argline = m.group(3).trim();
		args = argline.split("\\s*,\\s*");
		final DatabaseModule db = dbscr.getDatabase();
		func = null;
		
		switch( cmd ) {

		case "has_domain":
			func = (Statement st) -> db.hasDomain(st, getArg(0), getArg(1)) ^ not; break;
			
		case "has_schema":
			func = (Statement st) -> db.hasSchema(st, getArg(0)) ^ not; break;
			
		case "has_table":
			func = (Statement st) -> db.hasTable(st, getArg(0), getArg(1))  ^ not; break;

		case "has_column":
			func = (Statement st) -> db.hasColumn(st, getArg(0), getArg(1), getArg(2))  ^ not; break;

		case "has_constraint":
			func = (Statement st) -> db.hasConstraint(st, getArg(0), getArg(1), getArg(2))  ^ not; break;

		case "has_index":
			func = (Statement st) -> db.hasIndex(st, getArg(0), getArg(1))  ^ not; break;

		case "has_trigger":
			func = (Statement st) -> db.hasTrigger(st, getArg(0), getArg(1))  ^ not; break;

		case "has_procedure":
			func = (Statement st) -> db.hasProcedure(st, getArg(0), getArg(1))  ^ not; break;

		case "has_record":
			func = (Statement st) -> db.hasRecord(st, dbscr.injectParameters(argline))  ^ not; break;

			default:
			return false;
		}
		
		return true;
	}
	
	private String getArg(int idx) {
		if( idx < args.length )
			return dbscr.injectParameters( args[idx] );
		else
			return "";
	}

	@Override
	public boolean execute(Statement statement) throws SQLException {
		if( func == null )
			return false;
		
		return func.execute(statement);
	}

	@Override
	public boolean isCondition() {
		return true;
	}


}
