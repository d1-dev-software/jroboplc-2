package promauto.jroboplc.plugin.jrbustcp;

import io.netty.buffer.ByteBuf;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.TagRW;

import java.nio.charset.Charset;
import java.util.zip.CRC32;

import static promauto.jroboplc.core.api.Tag.Type.*;

public class JrbustcpProtocol {
    public final static Charset charset = Charset.forName("UTF-8");

    public  final static int FRAME_SIZE_MAX     = 16384;
    private final static int WRITE_UPPER_BOUND  = FRAME_SIZE_MAX/2;
    public  static final int STR_LEN_MAX        = FRAME_SIZE_MAX/4; // must be even!

    private static final int MAGIC_HEADER = 0xABCD;

    static final int CMD_INIT        = 1;
    static final int CMD_LIST        = 2;
    static final int CMD_UPDATE      = 3;
    static final int CMD_READ        = 4;
    static final int CMD_WRITE       = 5;
    static final int CMD_CRC         = 6;
    static final int CMD_AUTH_INIT   = 7;
    static final int CMD_AUTH_SUBMIT = 8;

    static final int ERROR_UNUATHENTICATED = 254;
    static final int ERROR_CMD_UNKNOWN     = 255;

    static final int AUTH_INIT_OK = 0;
    static final int AUTH_INIT_FAILED = 1;
    static final int AUTH_INIT_DISABLED = 2;

    static final int AUTH_SUBMIT_ACCEPTED = 0;
    static final int AUTH_SUBMIT_DENIED = 255;

    static final int INITPRM_TAGDESCR         = 1;
    static final int INITPRM_TAGSTATUS        = 2;
    static final int INITPRM_EXCLUDE_EXTERNAL = 4;
    static final int INITPRM_INCLUDE_HIDDEN   = 8;

    private static final int TYPE_BOOL   = 1;
    private static final int TYPE_INT    = 2;
    private static final int TYPE_LONG   = 3;
    private static final int TYPE_DOUBLE = 4;
    private static final int TYPE_STRING = 5;

    private static final int FLAGS_ALL        = 0xFF;
    private static final int FLAGS_STATUS     = 0x10;

    private static final int VAL_ZERO         = 0xF0;
    private static final int VAL_ONE          = 0xF1;
    private static final int VAL_BYTE         = 0xF2;
    private static final int VAL_WORD         = 0xF3;
    private static final int VAL_INT          = 0xF8;
    private static final int VAL_LONG         = 0xF9;
    private static final int VAL_DOUBLE       = 0xFA;
    private static final int VAL_STRING       = 0xFB;

    private static final int VAL_INDEX_SHORT  = 0xFE;
    private static final int VAL_INDEX_MEDIUM = 0xFF;


    private boolean useTagStatus = false;


    private CRC32 crc = new CRC32();

    public JrbustcpProtocol() {
        assert STR_LEN_MAX <= Tag.STRING_LENGTH_MAX;
    }


    public boolean isUseTagStatus() {
        return useTagStatus;
    }


    public void setUseTagStatus(boolean useTagStatus) {
        this.useTagStatus = useTagStatus;
    }


    public Message getMessage(ByteBuf buf) {
        if( buf.readableBytes() < 9 )
            return null;

        int header = buf.getUnsignedShort(0);
        long crc1 = buf.getUnsignedInt(buf.readableBytes() - 4);
        long crc2 = calcCrc(buf, 2, buf.readableBytes()-4);

        if( header != MAGIC_HEADER  ||  crc1 != crc2 )
            return null;

        int reqid = buf.getInt(2);
        int cmd = buf.getUnsignedByte(6);
        buf = buf.slice(7, buf.readableBytes() - 9);

        return new Message(cmd, reqid, buf.readableBytes(), buf);
    }


    private long calcCrc(ByteBuf buf, int begin, int len) {
        crc.reset();
        for(int i=begin; i<len; ++i) {
            crc.update(buf.getUnsignedByte(i));
        }
        return crc.getValue();
    }


    public void writeHeader(ByteBuf buf, int reqId, int cmd) {
        buf.clear();
        buf.writeShort(MAGIC_HEADER);
        buf.writeInt( reqId );
        buf.writeByte( cmd );
    }


    public void writeFooter(ByteBuf buf) {
        long crc2 = calcCrc(buf, 2, buf.writerIndex());
        buf.writeInt( (int)crc2 );
    }


    public int convertTagTypeToByte(Tag tag) {
        switch (tag.getType()) {
            case BOOL:   return TYPE_BOOL;
            case INT:    return TYPE_INT;
            case LONG:   return TYPE_LONG;
            case DOUBLE: return TYPE_DOUBLE;
            case STRING: return TYPE_STRING;
        }
        return 0;
    }

    public Tag.Type convertByteToTagType(int value) {
        switch (value) {
            case TYPE_BOOL:   return BOOL;
            case TYPE_INT:    return INT;
            case TYPE_LONG:   return LONG;
            case TYPE_DOUBLE: return DOUBLE;
            case TYPE_STRING: return STRING;
        }
        throw new IllegalArgumentException("Unknown tag type value: " + value);
    }


    public int readIndex(ByteBuf buf, int index) {
        int valcode = buf.getUnsignedByte( buf.readerIndex() );
        switch ( valcode ) {
            case VAL_INDEX_SHORT:
                buf.skipBytes(1);
                index = buf.readUnsignedShort();
                break;
            case VAL_INDEX_MEDIUM:
                buf.skipBytes(1);
                index = buf.readUnsignedMedium();
                break;
        }
        return index;
    }


    public void writeIndex(ByteBuf buf, int index) {
        if( index < 0x10000 ) {
            buf.writeByte(VAL_INDEX_SHORT);
            buf.writeShort(index);
        } else {
            buf.writeByte(VAL_INDEX_MEDIUM);
            buf.writeMedium(index);
        }
    }


    // Reading by server
    public void readValue(ByteBuf buf, Tag tag) {
        int valcode = buf.readUnsignedByte();

        // ignore tag status info if presented
        valcode |= FLAGS_STATUS;

        switch (valcode) {
            case VAL_ZERO:   tag.setInt(0); break;
            case VAL_ONE:    tag.setInt(1); break;
            case VAL_BYTE:   tag.setInt( buf.readUnsignedByte() ); break;
            case VAL_WORD:   tag.setInt( buf.readUnsignedShort() ); break;
            case VAL_INT:    tag.setInt( buf.readInt() ); break;
            case VAL_LONG:   tag.setLong( buf.readLong() ); break;
            case VAL_DOUBLE: tag.setDouble( buf.readDouble() ); break;
            case VAL_STRING: tag.setString(
                    buf.readCharSequence(buf.readUnsignedShort(), charset).toString() ); break;
            default:
                throw new IllegalArgumentException("Unknown value code: " + valcode);
        }

    }


    // Reading by client
    public void readValueAndStatus(ByteBuf buf, TagRW tag) {
        int valcode = buf.readUnsignedByte();

        Tag.Status status = Tag.Status.Good;
        if( (valcode & FLAGS_STATUS) == 0 ) {
            valcode |= FLAGS_STATUS;
            status = Tag.Status.Bad;
        }

        switch (valcode) {
            case VAL_ZERO:   tag.setReadValInt(0); break;
            case VAL_ONE:    tag.setReadValInt(1); break;
            case VAL_BYTE:   tag.setReadValInt( buf.readUnsignedByte() ); break;
            case VAL_WORD:   tag.setReadValInt( buf.readUnsignedShort() ); break;
            case VAL_INT:    tag.setReadValInt( buf.readInt() ); break;
            case VAL_LONG:   tag.setReadValLong( buf.readLong() ); break;
            case VAL_DOUBLE: tag.setReadValDouble( buf.readDouble() ); break;
            case VAL_STRING: tag.setReadValString(
                    buf.readCharSequence(buf.readUnsignedShort(), charset).toString() ); break;
            default:
                throw new IllegalArgumentException("Unknown value code: " + valcode);
        }

        tag.setStatus(status);
    }



    // Writing by server
    public void writeValueAndStatus(ByteBuf buf, Tag tag) {
        int flags = FLAGS_ALL;

        if( useTagStatus  &&  tag.getStatus() != Tag.Status.Good )
            flags ^= FLAGS_STATUS;

        switch (tag.getType()) {
            case BOOL:      writeValueBool(  buf, tag.getBool(),   flags); break;
            case INT:       writeValueInt(   buf, tag.getInt(),    flags); break;
            case LONG:      writeValueLong(  buf, tag.getLong(),   flags); break;
            case DOUBLE:    writeValueDouble(buf, tag.getDouble(), flags); break;
            case STRING:    writeValueString(buf, tag.getString(), flags); break;
        }
    }

    // Writing by client
    public void writeValue(ByteBuf buf, TagRW tag) {

        // Skipped setting MASK_FLAG_STATUS, because client must ignore tag status info while writing

        switch (tag.getType()) {
            case BOOL:      writeValueBool(  buf, tag.getWriteValBool(),   FLAGS_ALL); break;
            case INT:       writeValueInt(   buf, tag.getWriteValInt(),    FLAGS_ALL); break;
            case LONG:      writeValueLong(  buf, tag.getWriteValLong(),   FLAGS_ALL); break;
            case DOUBLE:    writeValueDouble(buf, tag.getWriteValDouble(), FLAGS_ALL); break;
            case STRING:    writeValueString(buf, tag.getWriteValString(), FLAGS_ALL); break;
        }
    }


    private void writeValueBool(ByteBuf buf, boolean value, int flags) {
        buf.writeByte(flags & (value? VAL_ONE: VAL_ZERO));
    }


    private void writeValueInt(ByteBuf buf, int value, int flags) {
        if( writeValueShortFormInt(buf, value, flags))
            return;

        buf.writeByte(flags & VAL_INT);
        buf.writeInt(value);
    }


    private void writeValueLong(ByteBuf buf, long value, int flags) {
        if( value >= 0  &&  value < 65536  &&  writeValueShortFormInt(buf, (int)value, flags))
            return;

        buf.writeByte(flags & VAL_LONG);
        buf.writeLong(value);
    }


    private boolean writeValueShortFormInt(ByteBuf buf, int value, int flags) {

        if( value == 0 ) {
            buf.writeByte(flags & VAL_ZERO);
            return true;
        }

        if(value > 0) {
            if (value == 1) {
                buf.writeByte(flags & VAL_ONE);
                return true;
            }

            if (value < 256) {
                buf.writeByte(flags & VAL_BYTE);
                buf.writeByte(value);
                return true;
            }

            if (value < 65536) {
                buf.writeByte(flags & VAL_WORD);
                buf.writeShort(value);
                return true;
            }
        }
        return false;
    }


    private void writeValueDouble(ByteBuf buf, double value, int flags) {
        buf.writeByte(flags & VAL_DOUBLE);
        buf.writeDouble(value);
    }


    private void writeValueString(ByteBuf buf, String value, int flags) {
        buf.writeByte(flags & VAL_STRING);
        writeString(buf, value);
    }

    public String readShortString(ByteBuf buf) {
        int len = buf.readUnsignedByte();
        return buf.readCharSequence(len, charset).toString();
    }

    public String readString(ByteBuf buf) {
        int len = buf.readUnsignedShort();
        return buf.readCharSequence(len, charset).toString();
    }

    public void writeShortString(ByteBuf buf, String value) {
        int posLen = buf.writerIndex();
        buf.writeByte(0);
        buf.writeCharSequence(value, charset);
        int len = buf.writerIndex() - posLen - 1;
        if(len < 256) {
            buf.setByte(posLen, len);
        } else {
            len = 254;
            buf.setByte(posLen, len);
            buf.writerIndex(posLen + 1 + len);
        }
    }

    public void writeString(ByteBuf buf, String value) {
        int posLen = buf.writerIndex();
        buf.writeShort(0);
        buf.writeCharSequence(value, charset);
        int len = buf.writerIndex() - posLen - 2;
        if(len < STR_LEN_MAX) {
            buf.setShort(posLen, len);
        } else {
            len = STR_LEN_MAX;
            buf.setShort(posLen, len);
            buf.writerIndex(posLen + 2 + len);
        }
    }




    boolean canWrite(ByteBuf buf) {
        return buf.writerIndex() < WRITE_UPPER_BOUND;
    }





}
