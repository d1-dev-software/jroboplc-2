package promauto.jroboplc.plugin.jrbustcp;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.LengthFieldPrepender;
import io.netty.handler.codec.compression.*;
import io.netty.handler.ipfilter.IpFilterRuleType;
import io.netty.handler.ipfilter.IpSubnetFilter;
import io.netty.handler.ipfilter.IpSubnetFilterRule;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.timeout.IdleStateHandler;

import java.util.ArrayList;
import java.util.List;

import static promauto.jroboplc.plugin.jrbustcp.JrbustcpProtocol.FRAME_SIZE_MAX;

public class ServerInitializer extends ChannelInitializer<SocketChannel> {
    private final JrbustcpServerModule module;
    private final SslContext sslCtx;


    public ServerInitializer(JrbustcpServerModule module, SslContext sslCtx) {
        this.module = module;
        this.sslCtx = sslCtx;
    }

    @Override
    public void initChannel(SocketChannel ch) {
        ChannelPipeline pipeline = ch.pipeline();

        pipeline.addFirst("ipfilter", module.getIpSubnetFilter() );

        pipeline.addFirst("traffic", new TrafficHandler());

        pipeline.addLast(new IdleStateHandler(0, module.getIdleTimeout(), 0));

        if (sslCtx != null) {
            pipeline.addLast(sslCtx.newHandler(ch.alloc()));
        }

//            pipeline.addLast(new FastLzFrameEncoder());
//            pipeline.addLast(new SnappyFrameEncoder());
        if (module.getCompress().equals("gzip")) {
            pipeline.addLast(ZlibCodecFactory.newZlibEncoder(ZlibWrapper.GZIP));
            pipeline.addLast(ZlibCodecFactory.newZlibDecoder(ZlibWrapper.GZIP));
        }

        pipeline.addLast(new LengthFieldBasedFrameDecoder(FRAME_SIZE_MAX, 0, 2, 0, 2));
        pipeline.addLast(new LengthFieldPrepender(2));

        pipeline.addLast(new ServerHandler(module));
    }

}
