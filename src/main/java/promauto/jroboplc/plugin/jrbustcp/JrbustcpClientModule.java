package promauto.jroboplc.plugin.jrbustcp;

import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufUtil;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import io.netty.handler.ssl.util.InsecureTrustManagerFactory;
import io.netty.util.ResourceLeakDetector;
import org.apache.log4j.BasicConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.AbstractModule;
import promauto.jroboplc.core.api.*;
import promauto.jroboplc.core.tags.TagRW;

import javax.net.ssl.SSLException;
import java.rmi.server.ExportException;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static promauto.jroboplc.plugin.jrbustcp.JrbustcpProtocol.*;


public class JrbustcpClientModule extends AbstractModule {
    private final Logger logger = LoggerFactory.getLogger(JrbustcpClientModule.class);

    protected String descr;
    private String host;
    private int port;
    public int initflags;
    private boolean ssl;
    private String compress;
    protected String filter;
    private int reconTime;
    private int timeout;
    private boolean logging;
    private boolean auth;
    private String authKeyName;
    private int writeDelay;
    private int chWdtDelay;
    private boolean chWdtEnable;

    private Tag tagConnected;
    private Tag tagReconnectCnt;
    private Tag tagDisconnectCnt;
    private Tag tagLastError;
    private Tag tagChWdtAlarm;

    protected TagRW[] tags = new TagRW[0];

    private List<Integer> writeCache = new ArrayList<>();

    private Map<String,String> alarmValues = new HashMap<>();

    private boolean modeSetHidden;

    private long cooldownTime = 0;
    private long connectTime = 0;
    private long chWdtDisconnectTime = 0;

    private JrbustcpProtocol prot = new JrbustcpProtocol();
    private Bootstrap bootstrap;
    private EventLoopGroup workerGroup = null;
    protected Channel channel = null;
    protected ByteBuf outbuf = null;
    protected ByteBuf inbuf = null;
    private int reqid;
    private int inboundValues;
    private int outboundValues;

    final BlockingQueue<ByteBuf> answer = new LinkedBlockingQueue<>(10);

    public JrbustcpClientModule(Plugin plugin, String name) {
        super(plugin, name);
    }

    @Override
    public boolean canHaveExternalTags() {
        return true;
    }

    @Override
    protected boolean loadModule(Object conf) {
        Configuration cm = env.getConfiguration();

        port = cm.get(conf, "port", 0);
        host = cm.get(conf, "host", "localhost");
        descr = cm.get(conf, "descr", name);
        ssl = cm.get(conf, "ssl", false);
        compress = cm.get(conf, "compress", "");
        filter = cm.get(conf, "filter", ".*");
        reconTime = cm.get(conf, "recon_s", 5) * 1000;
        timeout = cm.get(conf, "timeout_ms", 3000);
        logging = cm.get(conf, "logging", false);

        auth = cm.get(conf, "auth", false);
        authKeyName = cm.get(conf, "authKey", "");
        if( auth  &&  authKeyName.isEmpty() ) {
            authKeyName = env.getKeyManager().getDefaultPrivateKeyName();
            if( authKeyName.isEmpty() ) {
                auth = false;
                env.printError(logger, name, "No default private key found. Authentication disabled!");
            }
        }


        writeDelay = cm.get(conf, "writeDelay_s", 0) * 1000;
        chWdtDelay = cm.get(conf, "chWdtDelay_s", 0) * 1000;
        chWdtEnable   = cm.get(conf, "chWdtEnable", false);

        initflags = (cm.get(conf, "tagdescr", false)?1:0) * INITPRM_TAGDESCR;
        initflags += INITPRM_TAGSTATUS; // no choice for client, always has to ask server to send tag status info
        initflags += (cm.get(conf, "excludeExternal", false)?1:0) * INITPRM_EXCLUDE_EXTERNAL;
        initflags += (cm.get(conf, "includeHidden", false)?1:0) * INITPRM_INCLUDE_HIDDEN;

        modeSetHidden = cm.get(conf, "setHidden", false);

        alarmValues.clear();
        for(Object obj: cm.toList(cm.get(conf, "alarm.values")))
            alarmValues.put( cm.get(obj, "tag", ""), cm.get(obj, "value", ""));

        createSystemTags();

        return true;
    }

    protected void createSystemTags() {
        tagConnected	 = tagtable.createBool("client.connected", false);
        tagReconnectCnt  = tagtable.createInt("client.reconnect.cnt", 0);
        tagDisconnectCnt = tagtable.createInt("client.disconnect.cnt", 0);
        tagLastError = tagtable.createString("client.last.error", "");
        if( chWdtEnable ) {
            tagChWdtAlarm = tagtable.createBool("client.chwdt.alarm", false);
        }
    }

    public String getCompress() {
        return compress;
    }

    void setAuth(boolean auth) {
        this.auth = auth;
    }

    void setAuthKeyName(String authKeyName) {
        this.authKeyName = authKeyName;
    }

    @Override
    public String getInfo() {
        long counterRead = 0, counterWritten = 0;
        TrafficHandler th;
        if( channel != null  &&  (th = (TrafficHandler) channel.pipeline().get("traffic")) != null) {
            counterRead = th.getCounterRead();
            counterWritten = th.getCounterWritten();
        }

        return !enable? "disabled":
                String.format("%s%s:%d %s %s%s%s, tags=%d, %s%d/%d B/s%s, values rd/wr=%d/%d"
                        , !tagConnected.getBool()? ANSI.RED + ANSI.BOLD + "ERROR! " + ANSI.RESET: ""
                        , host
                        , port
                        , descr
                        , filter
                        , (ssl? " SSL": "")
                        , (auth? " AUTH": "")
                        , (tags==null? 0: tags.length)
                        , ANSI.GREEN
                        , counterRead, counterWritten
                        , ANSI.RESET
                        , inboundValues, outboundValues
                );

    }

    @Override
    protected boolean prepareModule() {
        // debug only
        if(logging) {
            BasicConfigurator.configure();
            ResourceLeakDetector.setLevel(ResourceLeakDetector.Level.PARANOID);
        }
        // debug only


        reqid = ThreadLocalRandom.current().nextInt();
        tagReconnectCnt.setInt(-1);
        tagDisconnectCnt.setInt(0);

        if( workerGroup == null)
            workerGroup = new NioEventLoopGroup(1);

        if( bootstrap == null ) {
            SslContext sslCtx = null;
            if (ssl) {
                try {
                    sslCtx = SslContextBuilder.forClient()
                            .trustManager(InsecureTrustManagerFactory.INSTANCE).build();
                } catch (SSLException e) {
                    env.printError(logger, e, name);
                    return false;
                }
            }

            bootstrap = new Bootstrap();
            bootstrap.group(workerGroup);
            bootstrap.channel(NioSocketChannel.class);
            bootstrap.option(ChannelOption.SO_KEEPALIVE, true);
            bootstrap.handler(new ClientInitializer(this, sslCtx));
        }

        connect();

        if( !isConnected() )
            env.printInfo(logger, name, "No connection");

        return true;
    }

    @Override
    protected boolean closedownModule() {
        disconnect();

        try {
            if( workerGroup != null )
                workerGroup.shutdownGracefully().sync();
        } catch (InterruptedException e) {
            env.printError(logger, e, name);
        }

        workerGroup = null;
        bootstrap = null;

        return true;
    }


    private void connect() {
        disconnect();

        tagReconnectCnt.setInt( tagReconnectCnt.getInt() + 1 );

        try {
            channel = bootstrap.connect(host, port).sync().channel();
        } catch (Exception e) {
            setLastError(e);
        }

        if( isConnected()  &&  authenticate()  &&  initList()  &&  doRead()) {
            for(TagRW tag: tags) {
                tag.hasWriteValue();
            }

            tagConnected.setBool(true);
            if( tagReconnectCnt.getInt() > 0 )
                env.printInfo(logger, name, "Connection restored");
            resetCooldown();

            connectTime = System.currentTimeMillis();
        } else {
            disconnect();
            setupCooldown();
        }
    }



    private void setLastError(Exception e) {
        tagLastError.setString(LocalDateTime.now().toString() + ": " + e.getMessage() );
    }


    private void disconnect() {
        if( channel == null )
            return;

        for(TagRW tag: tags) {
            tag.setStatus(Tag.Status.Bad);
        }

        try {
            if(outbuf != null) {
                outbuf.release();
                outbuf = null;
            }
            channel.close().sync();
        } catch (Exception e) {
            env.printError(logger, e, name);
        }

        channel = null;
        tagConnected.setBool(false);
    }


    @Override
    protected boolean executeModule() {
        if (cooldownTime > 0) {
            if (isCooldownOver()) {
                resetCooldown();
                connect();
            } else {
                executeChWdt();
            }
        } else {
            try {
                if( isConnected()  &&  doWrite()  &&  doRead() )
                    return true;
            } catch (Exception e) {
                env.printError(logger, e, name);
            } finally {
                releaseInbuf();
            }

            disconnect();
            applyAlarmValues();
            setupCooldown();

            chWdtDisconnectTime = System.currentTimeMillis();

            tagDisconnectCnt.setInt( tagDisconnectCnt.getInt() + 1 );
            env.printInfo(logger, name, "Connection lost, cnt: " + tagDisconnectCnt.getInt());
        }

        return true;
    }


    private boolean isCooldownOver() {
        return cooldownTime < System.currentTimeMillis();
    }

    private void setupCooldown() {
        cooldownTime = System.currentTimeMillis() + reconTime;
    }

    private void resetCooldown() {
        cooldownTime = 0;
    }

    protected boolean isConnected() {
        return channel != null  &&  channel.isActive();
    }

    private void applyAlarmValues() {
        for(String tagexpr: alarmValues.keySet()) {
            Pattern p = Pattern.compile(tagexpr);
            String value = alarmValues.get(tagexpr);

            for (TagRW tag : tags)
                if( p.matcher(tag.getName()).matches() )
                    tag.setReadValString( value );

        }
    }


    private Message readMessage(int cmd) throws Exception {
        for(;;) {
            releaseInbuf();
            inbuf = answer.poll(timeout, TimeUnit.MILLISECONDS);

            if( logging  &&  inbuf != null )
                env.logInfo(logger, "Client got answer:\r\n" + ByteBufUtil.prettyHexDump(inbuf) + "\r\n");

            if (inbuf == null)
                throw new TimeoutException();

            Message msg = prot.getMessage(inbuf);
            if (msg.reqId == reqid) {
                if( msg.cmd != (cmd | 0x80)) {
                    switch (msg.cmd) {
                        case ERROR_CMD_UNKNOWN:
                            throw new CommandUnknownException("Unknown command: " + cmd);
                        case ERROR_UNUATHENTICATED:
                            env.printError(logger, name, "Server required authentication!");
                            throw new UnauthenticatedException("Unauthenticated command: " + cmd);
                        default:
                            throw new IllegalServerAnswerException("Illegal server answer: " + msg.cmd + ", while command: " + cmd);
                    }
                }
                return msg;
            }
        }
    }


    private void releaseInbuf() {
        if( inbuf != null ) {
            inbuf.release();
            inbuf = null;
        }
    }


    private void createOutbuf() {
        if( outbuf == null ) {
            outbuf = channel.alloc().buffer();// directBuffer();
        }
    }


    private void writeAndFlash() {
        if(logging)
            env.logInfo(logger, "Client send request:\r\n" + ByteBufUtil.prettyHexDump(outbuf) + "\r\n");

        channel.writeAndFlush(outbuf);
        outbuf = null;

    }


    private int getNextReqId() {
        return ++reqid;
    }


    protected boolean authenticate() {
        if( !auth )
            return true;


        try {
            Message msg = sendCmdAuthInit();
            int statusInit = msg.body.readUnsignedByte();
            if (statusInit == AUTH_INIT_FAILED) {
                String failDescr = prot.readString(msg.body);
                env.printError(logger, name, "Authentication failed: " + failDescr);
                return false;
            }

            if (statusInit == AUTH_INIT_OK) {
                String encryptedNonce = prot.readString(msg.body);
                String nonce = env.getKeyManager().decryptPrivate(authKeyName, encryptedNonce);
                msg = sendCmdAuthSubmit(nonce);
                int statusSubmit = msg.body.readUnsignedByte();
                if( statusSubmit != AUTH_SUBMIT_ACCEPTED ) {
                    env.printError(logger, name, "Authentication rejected!");
                    return false;
                }
            }

        } catch (CommandUnknownException e) {
            env.printInfo(logger, name, "Server doesn't support authentication. Server is insecure!");
            setLastError(e);
        } catch (Exception e) {
            env.logError(logger, e, name, "authenticate error");
            setLastError(e);
            return false;
        } finally {
            releaseInbuf();
        }

        return true;
    }


    private Message sendCmdAuthInit() throws Exception {
        return sendString(CMD_AUTH_INIT, authKeyName);
    }


    private Message sendCmdAuthSubmit(String nonce) throws Exception {
        return sendString(CMD_AUTH_SUBMIT, nonce);
    }


    private Message sendString(int cmd, String data) throws Exception {
        createOutbuf();
        prot.writeHeader(outbuf, getNextReqId(), cmd);
        prot.writeString(outbuf, data);
        prot.writeFooter(outbuf);
        writeAndFlash();
        return readMessage(cmd);
    }


    protected boolean initList() {
        TagRW[] newtags = null;
        try {
            Message msg = sendCmdInit();

            int size = msg.body.readUnsignedMedium();
            newtags = new TagRW[size];

            int next = 0;
            do {
                msg = sendCmdList(next);
                int index = msg.body.readUnsignedMedium();
                int qnt = msg.body.readUnsignedMedium();
                next = msg.body.readUnsignedMedium();

                for(int i=0; i<qnt; ++i) {
                    Tag.Type type = prot.convertByteToTagType(msg.body.readUnsignedByte());

                    int len = msg.body.readUnsignedByte();
                    String tagname = msg.body.readCharSequence(len, charset).toString();

                    len = msg.body.readUnsignedByte();
                    String tagdescr = msg.body.readCharSequence(len, charset).toString();

                    if( index < 0  ||  index >= size )
                        throw new IndexOutOfBoundsException();

                    newtags[index] = TagRW.create(type, tagname,
                            Flags.EXTERNAL + Flags.STATUS + (modeSetHidden? Flags.HIDDEN: 0));
//                  todo:  newtags[index].setDescr( tagdescr );
                    newtags[index].setStatus( Tag.Status.Uninitiated);
                    index++;
                }
            } while (next > 0);

        } catch (Exception e) {
            env.logError(logger, e, name, "initList error");
            setLastError(e);
            return false;
        } finally {
            releaseInbuf();
        }

        boolean hasChanges = false;

        // remove irrelevant tags from old
        Map<String,Tag> newmap = Arrays.stream(newtags).collect(
                Collectors.toMap(Tag::getName, tag -> tag, (a1, a2) -> a1));

        for(Tag oldtag: tags) {
            Tag newtag = newmap.get(oldtag.getName());
            if( newtag==null  ||  newtag.getType() != oldtag.getType() ) {
                tagtable.remove(oldtag);
                hasChanges = true;
            }
        }

        // move relevant from old to new
        for(int i=0; i<newtags.length; ++i) {
            Tag oldtag = tagtable.get(newtags[i].getName());

            if( oldtag == null ) {
                tagtable.add(newtags[i]);
                hasChanges = true;
            } else
                newtags[i] = (TagRW)oldtag;

            newtags[i].setStatus(Tag.Status.Good);
        }

        if( hasChanges ) {
            tags = newtags;
            postSignal(Signal.SignalType.RELOADED);
        }

        return true;
    }


    private Message sendCmdInit() throws Exception {
        createOutbuf();
        prot.writeHeader(outbuf, getNextReqId(), CMD_INIT);
        prot.writeShortString(outbuf, filter);
        prot.writeShortString(outbuf, descr);
        outbuf.writeShort( initflags );
        prot.writeFooter(outbuf);
        writeAndFlash();
        return readMessage(CMD_INIT);
    }


    private Message sendCmdList(int index) throws Exception {
        createOutbuf();
        prot.writeHeader(outbuf, getNextReqId(), CMD_LIST);
        outbuf.writeMedium( index );
        prot.writeFooter(outbuf);
        writeAndFlash();
        return readMessage(CMD_LIST);
    }


    protected boolean doWrite() {
        if( writeDelay > 0  &&  connectTime > 0 ) {
            long time = System.currentTimeMillis() - connectTime;
            if( time < 0  ||  time > writeDelay) {
                connectTime = 0;
            } else {
                return true;
            }
        }

        int qnt = 0;
        for(int i=0; i<tags.length; ++i) {
            if( tags[i].hasWriteValue() ) {
                if( qnt < writeCache.size() )
                    writeCache.set(qnt, i) ;
                else
                    writeCache.add(i);
                qnt++;
            }
        }

        outboundValues = qnt;

        if( qnt == 0 )
            return true;

        try {
            int i = 0;
            while( i < qnt ) {
                int curindex = writeCache.get(i);
                createOutbuf();
                prot.writeHeader(outbuf, getNextReqId(), CMD_WRITE);
                outbuf.writeMedium( curindex );
                int qntPos = outbuf.writerIndex();
                outbuf.writeMedium(0);

                int n = 0;
                for(;i<qnt; ++i, ++n, ++curindex) {
                    if( !prot.canWrite(outbuf) )
                        break;

                    int index = writeCache.get(i);
                    boolean gap = curindex != index;

                    if( gap ) {
                        prot.writeIndex(outbuf, index);
                        curindex = index;
                    }

                    prot.writeValue(outbuf, tags[curindex]);
                }

                outbuf.setMedium(qntPos, n);
                prot.writeFooter(outbuf);
                writeAndFlash();

                readMessage(CMD_WRITE);
            }
        } catch (Exception e) {
            env.logError(logger, e, name, "write error");
            setLastError(e);
            return false;
        } finally {
            releaseInbuf();
        }

        return true;
    }


    protected boolean doRead() {
        try {
            Message msg = sendCmdUpdate();
            int qnt = msg.body.readUnsignedMedium();
            int next = msg.body.readUnsignedMedium();
            int liststate = msg.body.readUnsignedByte();

            if( liststate == 0xFF ) {
                env.printInfo(logger, name, "Server signal: reinitialize");
                return initList();
            }

            inboundValues = qnt;

            if( qnt == 0 )
                return true;

            int index;
            do {
                msg = sendCmdRead(next);

                index = msg.body.readUnsignedMedium();
                qnt = msg.body.readUnsignedMedium();
                next = msg.body.readUnsignedMedium();

                for (int i = 0; i < qnt; ++i, ++index) {
                    index = prot.readIndex(msg.body, index);
                    if (index >= 0 && index < tags.length)
                        prot.readValueAndStatus(msg.body, tags[index]);
                    else
                        throw new IndexOutOfBoundsException();
                }
            } while (next > 0);

        } catch (Exception e) {
            env.logError(logger, e, name, "read error");
            setLastError(e);
            return false;
        } finally {
            releaseInbuf();
        }

        return true;
    }


    private Message sendCmdUpdate() throws Exception {
        createOutbuf();
        prot.writeHeader(outbuf, getNextReqId(), CMD_UPDATE);
        prot.writeFooter(outbuf);
        writeAndFlash();
        return readMessage(CMD_UPDATE);
    }


    private Message sendCmdRead(int index) throws Exception {
        createOutbuf();
        prot.writeHeader(outbuf, getNextReqId(), CMD_READ);
        outbuf.writeMedium( index );
        prot.writeFooter(outbuf);
        writeAndFlash();
        return readMessage(CMD_READ);
    }


    private void executeChWdt() {
        if( !chWdtEnable  ||  chWdtDisconnectTime <= 0 )
            return;

        tagChWdtAlarm.setInt(1);

        long time = System.currentTimeMillis() - chWdtDisconnectTime;
        if( time >= 0  &&  time <= chWdtDelay)
            return;

        chWdtDisconnectTime = 0;

        Set<Module> modules = EnvironmentInst.get().getModuleManager().getModules();
        for(Tag tag: tags) {
            String value = tag.getString();
            if (tag.getType() == Tag.Type.STRING) {
                Optional<Module> foundModule = modules.stream()
                        .filter(m -> value.startsWith( m.getName() + '.' ))
                        .findFirst();

                if( !foundModule.isPresent() )
                    continue;

                String tagname = value.substring( foundModule.get().getName().length() + 1 );
                Tag foundTag = foundModule.get().getTagTable().get( tagname );
                if( foundTag == null )
                    continue;

                foundTag.setInt(0);
                System.out.println("chwdt: " + value); // todo: REMOVE THIS DEBUG!!
            }
        }

        tagChWdtAlarm.setInt(0);
    }


    @Override
    protected boolean reload() {
        JrbustcpClientModule tmp = new JrbustcpClientModule(plugin, name);
        if( !tmp.load() )
            return false;

        closedownModule();

        copySettingsFrom(tmp);

        descr             = tmp.descr;
        host              = tmp.host;
        port              = tmp.port;
        initflags         = tmp.initflags;
        ssl               = tmp.ssl;
        compress          = tmp.compress;
        filter            = tmp.filter;
        reconTime         = tmp.reconTime;
        timeout           = tmp.timeout;
        logging           = tmp.logging;
        auth              = tmp.auth;
        authKeyName       = tmp.authKeyName;


        writeDelay = tmp.writeDelay;
        chWdtDelay = tmp.chWdtDelay;
        chWdtEnable       = tmp.chWdtEnable;

        if( tagChWdtAlarm == null  &&  tmp.tagChWdtAlarm != null ) {
            tagChWdtAlarm = tmp.tagChWdtAlarm;
            getTagTable().add( tagChWdtAlarm );
        }

        if( enable  &&  env.isRunning() )
            prepareModule();

        return true;
    }


}


class CommandUnknownException extends Exception {
    public CommandUnknownException(String message) {
        super(message);
    }
}

class UnauthenticatedException extends Exception {
    public UnauthenticatedException(String message) {
        super(message);
    }
}

class IllegalServerAnswerException extends Exception {
    public IllegalServerAnswerException(String message) {
        super(message);
    }
}
