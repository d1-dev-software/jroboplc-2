package promauto.jroboplc.plugin.rpsvrtcp;

import promauto.jroboplc.core.AbstractCommand;
import promauto.jroboplc.core.api.Console;
import promauto.jroboplc.core.api.Module;

public class CmdDebugInfoWrite extends AbstractCommand {
	
	


	@Override
	public String getName() {
		return "debugInfoWrite";
	}

	@Override
	public String getUsage() {
		return "on|off";
	}

	@Override
	public String getDescription() {
		return "turns on/off info on writing tags";
	}

	
	
	
	@Override
	public String execute(Console console, Module module, String args) {
		
		RpsvrtcpModule m = (RpsvrtcpModule) module;
		
		if (args.equals("on")) { 
			m.debugConsole = console;
			m.debugInfoWrite = true;
		} else
		
		if (args.equals("off")) { 
			m.debugConsole = null;
			m.debugInfoWrite = false;
		} else

		if (!args.isEmpty()) 
			return getUsage();
		
		return "OK";
	}


}
