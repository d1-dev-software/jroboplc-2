package promauto.jroboplc.plugin.rpsvrtcp;

import promauto.jroboplc.core.AbstractCommand;
import promauto.jroboplc.core.api.Console;
import promauto.jroboplc.core.api.Module;

public class CmdInfofSetFlag extends AbstractCommand {
	
	


	@Override
	public String getName() {
		return "infosetflag";
	}

	@Override
	public String getUsage() {
		return "on|off";
	}

	@Override
	public String getDescription() {
		return "turns on/off showing SETFLAG command firing";
	}

	
	
	
	@Override
	public String execute(Console console, Module module, String args) {
		
		RpsvrtcpModule m = (RpsvrtcpModule) module;
		
		if (args.equals("on")) 
			m.infoSetFlag = true;
		else
		
		if (args.equals("off")) 
			m.infoSetFlag = false;
		else

		if (!args.isEmpty()) 
			return getUsage();
		
		return "infosetflag is " + (m.infoSetFlag? "ON": "OFF");
	}


}
