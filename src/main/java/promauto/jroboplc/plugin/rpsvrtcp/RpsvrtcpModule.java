package promauto.jroboplc.plugin.rpsvrtcp;

import static promauto.utils.Strings.getFilterPattern;

import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.AbstractModule;
import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.Console;
import promauto.jroboplc.core.api.Plugin;
import promauto.jroboplc.core.api.TcpServerChannel;
import promauto.jroboplc.core.api.TcpServerPort;
import promauto.jroboplc.core.api.TcpSubscriber;

public class RpsvrtcpModule extends AbstractModule implements TcpSubscriber {
	private final Logger logger = LoggerFactory.getLogger(RpsvrtcpModule.class);
	
	private int portnum;
	protected boolean hiddentags;
	protected Pattern incl;
	protected Pattern excl;
	
	private Set<RpsvrtcpClient> clients = new HashSet<>();
	protected Set<String> transparentModules = new HashSet<>();

	public boolean infoSetFlag = false;

	protected Console debugConsole = null;
	protected boolean debugInfoWrite = false;

	
	public RpsvrtcpModule(Plugin plugin, String name) {
		super(plugin, name);
		taskable = false;
		env.getCmdDispatcher().addCommand(this, CmdHiddenTags.class);
		env.getCmdDispatcher().addCommand(this, CmdInfofSetFlag.class);
		env.getCmdDispatcher().addCommand(this, CmdDebugInfoWrite.class);
	}

	
	
	public boolean loadModule(Object conf) {
		Configuration cm = env.getConfiguration();

		portnum = cm.get(conf, "portnum", 0);
		enable = cm.get(conf, "enable", true);
		hiddentags = cm.get(conf, "hiddentags", false);
		incl 	= getFilterPattern( cm.get(conf, "incl", "") );
		excl 	= getFilterPattern( cm.get(conf, "excl", "") );
		
		cm.toList( cm.get(conf, "transparent")).stream().forEach(obj -> transparentModules.add(obj.toString()));
		
		return true;
	}

	
	
	@Override
	public boolean prepareModule() {
		if( env.getTcpServer().subscribe(portnum, this) )
			return true;

		env.printError(logger, name);
		return false;
	}

	
	

	
	
	@Override
	public String getInfo() {
		StringBuilder sb = new StringBuilder();
		if( enable ) {
			sb.append("p" + portnum + ", ");
			sb.append("hidden=" + hiddentags);
			sb.append("\r\n");
			int i = 0;
			for(RpsvrtcpClient client: clients) {
				sb.append("  " + ++i +": " );
				sb.append(client.getInfo());
				sb.append("\r\n");
			}
			return sb.toString();
		} else
			return "disabled";
	}


	
	@Override
	public boolean onTcpServerClientConnected(TcpServerChannel channel) {
		String clientAddress = channel.getRemoteAddress().getAddress().getHostAddress();
		env.printInfo(logger, name, "Connected:", clientAddress );
		
		RpsvrtcpClient client = new RpsvrtcpClient(this, clientAddress);
		channel.setAttachedObject(client);
		clients.add(client);
		
		channel.write( "100-Roboplant OPC Server (TCP)\r\n100 RpSvrTcp ver3\r\n" );

		return true;
	}



	@Override
	public boolean onTcpServerClientDisconnected(TcpServerChannel channel) {
		env.printInfo(logger, name, "Disconnected:", channel.getRemoteAddress().getAddress().getHostAddress() );

		Object obj = channel.getAttachedObject();
		if (obj!=null  &&  obj instanceof RpsvrtcpClient) {
			RpsvrtcpClient client = (RpsvrtcpClient)obj;
			client.release();
			return clients.remove(client);
		} else
			return false;
	}



	@Override
	public boolean onTcpServerRequest(TcpServerChannel channel, String request) {
		if (channel.getAttachedObject()!=null  &&  
				channel.getAttachedObject() instanceof RpsvrtcpClient)
			((RpsvrtcpClient)channel.getAttachedObject()).onRequest(channel, request);
		return true;
	}



	@Override
	protected boolean reload() {
		RpsvrtcpModule tmp = new RpsvrtcpModule(plugin, name);
		if( !tmp.load() )
			return false;

		TcpServerPort port_old = env.getTcpServer().getPortBySubscriber(this);
		TcpServerPort port_new = env.getTcpServer().getPortByNum(tmp.portnum); 
		if( tmp.enable  &&  port_new == null ) {
			env.printError(logger, name, "Port not found: " + tmp.portnum);
			return false;
		}

		copySettingsFrom(tmp);
		
		int portnum_old = portnum;
		portnum = tmp.portnum;
		hiddentags = tmp.hiddentags;
		incl = tmp.incl;
		excl = tmp.excl;
		transparentModules = tmp.transparentModules;

		
		if( enable  &&  env.isRunning()  &&  port_new != null  &&  port_old != port_new )
			port_new.setSubscriber(this);


		if( port_old != null  &&  port_old != port_new ) { 
			env.getTcpServer().disconnect(portnum_old);
			port_old.setSubscriber(null);
		}
		
		return true;
	}


	
	

}