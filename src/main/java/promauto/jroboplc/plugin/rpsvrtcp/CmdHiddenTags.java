package promauto.jroboplc.plugin.rpsvrtcp;

import promauto.jroboplc.core.AbstractCommand;
import promauto.jroboplc.core.api.Console;
import promauto.jroboplc.core.api.Module;

public class CmdHiddenTags extends AbstractCommand {
	
	


	@Override
	public String getName() {
		return "hidden";
	}

	@Override
	public String getUsage() {
		return "on|off";
	}

	@Override
	public String getDescription() {
		return "turns on/off hidden tags";
	}

	
	
	
	@Override
	public String execute(Console console, Module module, String args) {
		
		RpsvrtcpModule m = (RpsvrtcpModule) module;
		
		if (args.equals("on")) 
			m.hiddentags = true;
		else
		
		if (args.equals("off")) 
			m.hiddentags = false;
		else

		if (!args.isEmpty()) 
			return getUsage();
		
		return "hidden tags mode is " + (m.hiddentags? "ON": "OFF");
	}


}
