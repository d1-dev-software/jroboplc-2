package promauto.jroboplc.plugin.kkmansvr;

import promauto.jroboplc.core.api.ANSI;
import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.tags.RefGroup;
import promauto.jroboplc.core.tags.RefItem;

public class Doser {
    public static final int DOZMAN_STATUS_CODE_MASK = 0x7f;
    public static final int DOZMAN_STATUS_STOP = 0;

    private int doserId;
    private String name;
    private String descr;
    private boolean manual;
    private boolean canControl;

    public final RefGroup refgr = new RefGroup();
    private RefItem refIsEmptyCur;
    private RefItem refIsEmptyTot;
    private RefItem refSetEmptyCur;
    private RefItem refSetEmptyTot;
    private RefItem refWeightReq;
    private RefItem refWeightCur;
    private RefItem refWeightTot;
    private RefItem refStable;
    private RefItem refFinished;
    private RefItem refRun;
    private RefItem refStatus;
    private RefItem refErrorFlag;



    public boolean load(Object conf) {
        Configuration cm = EnvironmentInst.get().getConfiguration();
        name        = cm.get(conf, "name", "");
        descr       = cm.get(conf, "descr", name);
        manual      = cm.get(conf, "manual", false);
        canControl  = cm.get(conf, "canControl", false);

        refgr.clear();
        refIsEmptyCur  = refgr.createItem(   name, "IsEmptyCur");
        refIsEmptyTot  = refgr.createItem(   name, "IsEmptyTot");
        refSetEmptyCur = refgr.createItem(   name, "SetEmptyCur");
        refSetEmptyTot = refgr.createItem(   name, "SetEmptyTot");
        refWeightReq   = refgr.createItem(   name, "WeightReq");
        refWeightCur   = refgr.createItemCrc(name, "WeightCur");
        refWeightTot   = refgr.createItemCrc(name, "WeightTot");
        refStable      = refgr.createItemCrc(name, "Stable");
        refFinished    = refgr.createItemCrc(name, "Finished");
        refRun         = refgr.createItemCrc(name, "Run");
        refStatus      = refgr.createItemCrc(name, "Status");
        refErrorFlag   = refgr.createItemCrc(name, "SYSTEM.ErrorFlag");

        refgr.createItemCrcSum(name, "Crc32");

        return true;
    }


    public void init() {
        refgr.prepare();
    }

    public String getName() {
        return name;
    }

    public void setDoserId(int doserId) {
        this.doserId = doserId;
    }

    public String getDescr() {
        return descr;
    }

    public boolean isManual() {
        return manual;
    }

    public boolean isCanControl() {
        return canControl;
    }

    public int getWeightCur() {
        return refWeightCur.getValue().getInt();
    }

    public int getWeightTot() {
        return refWeightTot.getValue().getInt();
    }

    public boolean getStable() {
        return refStable.getValue().getBool();
    }

    public void setEmptyTot(boolean value) {
        refSetEmptyTot.getTag().setBool(value);
    }

    public boolean getIsEmptyTot() {
        return refIsEmptyTot.getValue().getBool();
    }

    public boolean isFinished() {
        return refFinished.getValue().getBool();
    }

    public void setWeightReq(int value) {
        refWeightReq.getTag().setInt(value);
    }

    public void setEmptyCur(boolean value) {
        refSetEmptyCur.getTag().setBool(value);
    }

    public void setRun(boolean value) {
        refRun.getTag().setBool(value);
    }

    public boolean isEmptyCur() {
        return refIsEmptyCur.getValue().getBool();
    }

    public boolean isStatusStop() {
        return (refStatus.getValue().getInt() & DOZMAN_STATUS_CODE_MASK) == DOZMAN_STATUS_STOP;
    }

    public boolean isStable() {
        return refStable.getValue().getBool();
    }

    public boolean isConnected() {
        return !refErrorFlag.getValue().getBool();
    }


    public String getInfo() {
        String error = "";
        if( !refgr.link() )
            error = "NO LINK ";
        else if( !refgr.checkCrc32() )
            error = "CRC ";
        else if( refErrorFlag.getValue().getBool() )
            error = "NOT CONNECTED ";

        return String.format("  %s%s (%d) %s%s%s",
                ANSI.redBold(error),
                name,
                doserId,
                descr,
                manual? ", manual": "",
                canControl? ", canControl": ""
        );
    }

    public String check() {
        return refgr.check();
    }

}
