package promauto.jroboplc.plugin.kkmansvr;


import promauto.jroboplc.core.AbstractPlugin;
import promauto.jroboplc.core.api.Module;

public class KkmansvrPlugin extends AbstractPlugin {
	private static final String PLUGIN_NAME = "kkmansvr";

	@Override
	public void initialize() {
		super.initialize();
	}

	@Override
	public String getPluginName() {
		return PLUGIN_NAME;
	}

	@Override
	public String getPluginDescription() {
		return "manual component doser server";
	}


	@Override
	public Module createModule(String name, Object conf) {
    	Module m = createDatabaseModule(name, conf);
    	if( m != null )
    		modules.add(m);
    	return m;
		
	}
	
	private Module createDatabaseModule(String name, Object conf) {
		KkmansvrModule m = new KkmansvrModule(this, name);
    	modules.add(m);
    	return m.load(conf)? m: null;
	}



}
