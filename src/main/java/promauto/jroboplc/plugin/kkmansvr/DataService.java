package promauto.jroboplc.plugin.kkmansvr;

import promauto.jroboplc.core.api.Database;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.Set;

public interface DataService {
    void setDb(Database db);

    Statement createStatement() throws SQLException;

    void commit() throws SQLException;

    void rollback();

    void syncLine(int lineId, String lineName) throws SQLException;

    int syncDoser(int lineId, String name, String descr, boolean canControl) throws SQLException;

    void deleteOtherDosers(int lineId, Set<Integer> doserIds) throws SQLException;

    Task findTask(int taskId) throws SQLException;

    void saveCycle(Cycle cycle) throws SQLException;
}
