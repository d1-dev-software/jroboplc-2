package promauto.jroboplc.plugin.kkmansvr;

import java.util.ArrayList;
import java.util.List;

public class Task {
    public static class Product {
        public int productId;
        public String productName;
        public int doserId;
        public int weightReq;
    }

    public int lineId;
    public int taskId;
    public int cycleReq;
    public int cycleCnt;
    public int controlId;
    public int recipeId;
    public String recipeName;
    public List<Product> products = new ArrayList<>();

}
