package promauto.jroboplc.plugin.kkmansvr;

import java.time.LocalDateTime;
import java.util.List;

public class Cycle {
    public static class Product {
        public int productId;
        public int weightReq;
        public int weightFin;
    }

    public int taskId;
    public LocalDateTime dt;
    public int cycleCnt;
    public int weightReq;
    public int weightFin;
    public int weightCtl;

    public List<Product> products;

}

