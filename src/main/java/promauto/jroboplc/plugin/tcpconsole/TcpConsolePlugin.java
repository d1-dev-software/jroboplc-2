package promauto.jroboplc.plugin.tcpconsole;

import promauto.jroboplc.core.AbstractPlugin;
import promauto.jroboplc.core.api.Module;

public class TcpConsolePlugin extends AbstractPlugin {

	private static final String PLUGIN_NAME = "tcpconsole";
	
	@Override
    public String getPluginName() {
    	return PLUGIN_NAME;
    }

	@Override
    public String getPluginDescription(){
    	return "Remote console over tcp";
    }
	
//	@Override
//    public Class<?> getModuleClass(){
//    	return TcpConsoleModule.class;
//    }
	

	@Override
	public Module createModule(String name, Object conf) {
		TcpConsoleModule m = new TcpConsoleModule(this, name);
    	modules.add(m);
    	return m.load(conf)? m: null;
	}

}
