package promauto.jroboplc.plugin.tcpconsole;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.AbstractConsole;
import promauto.jroboplc.core.api.Environment;
import promauto.jroboplc.core.api.TcpServerChannel;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class TcpConsoleClient extends AbstractConsole {
    private final Logger logger = LoggerFactory.getLogger(TcpConsoleClient.class);

//	private final static java.nio.charset.Charset chUTF8 = java.nio.charset.Charset.forName("UTF-8");


	private TcpServerChannel consoleChannel;

	protected TcpConsoleModule module;
//	private boolean active;

	public TcpConsoleClient(Environment env, TcpConsoleModule module, TcpServerChannel channel) {
		super(env);
		this.module = module;
		this.consoleChannel = channel;
//		this.active = true;
	}

	public void onRequest(TcpServerChannel channel, String request) {
		String answer;

		if( module.needPassword ) {
			if( request.equals( module.password ) ) {
				answer = "\r\n" + getPrompt();
				module.needPassword = false;
			} else {
				answer = "\r\nIncorrect password!\r\n" + TcpConsoleModule.ENTER_PASSWORD;
			}
		} else {
			boolean forbidden = false;
			if( module.regexForbidden != null ) {
				forbidden = module.regexForbidden.matcher(request).matches();
			}
			
			if( module.listForbidden != null ) {
				forbidden = module.listForbidden.stream().anyMatch( forbcmd -> request.startsWith(forbcmd) ); 
			}
			
			if( forbidden )
				answer = "Forbidden command!";
			else
				answer = env.getCmdDispatcher().execute(this, request);
			answer = answer + "\r\n" + getPrompt();

			afterRequestExecuted(request);
		}

		channel.write( prepareText(answer) );

		if( !request.trim().isEmpty() )
		    logger.info("[INPUT-" + channel.getRemoteAddress().getAddress().getHostAddress() + "] " + request);
	}


	private String prepareText(String text) {
		return new String(parseAnsi(text).getBytes(StandardCharsets.UTF_8), Charset.defaultCharset());
	}

	
	@Override
	public synchronized void print(String text) {
		try {
			consoleChannel.write( prepareText(text) );
		} catch (Exception e) {
			
		}
	}


	public void onDisconnect() {
		stopRepeatingLastRequest();
	}


	@Override
	public boolean initialize() {
		return true;
	}

	@Override
	public void close() {
	}

	@Override
	public void process() {
	}


}
