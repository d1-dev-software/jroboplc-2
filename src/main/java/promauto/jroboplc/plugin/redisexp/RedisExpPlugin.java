package promauto.jroboplc.plugin.redisexp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.AbstractPlugin;
import promauto.jroboplc.core.api.Module;

public class RedisExpPlugin extends AbstractPlugin {
	private final Logger logger = LoggerFactory.getLogger(RedisExpPlugin.class);

	private static final String PLUGIN_NAME = "redisexp";
	
	@Override
	public void initialize() {
		super.initialize();
    }
	
	@Override
    public String getPluginName() {
    	return PLUGIN_NAME;
    }

	@Override
    public String getPluginDescription(){
    	return "Redis export client";
    }
	

	@Override
	public Module createModule(String name, Object conf) {
		RedisExpModule m = new RedisExpModule(this, name);
		modules.add(m);
		return m.load(conf)? m: null;
	}

}
