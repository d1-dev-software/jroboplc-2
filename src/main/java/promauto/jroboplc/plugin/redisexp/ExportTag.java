package promauto.jroboplc.plugin.redisexp;

import io.lettuce.core.RedisFuture;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.TagPlain;

public class ExportTag {


    public enum State {
        IDLE,
        NEED_SET,
        HAS_FUTURE
    };

//    String tagname;
    String encodedName;
    String encodedData;
    Tag tag;
    Tag curval;
    char type;
    RedisFuture<Boolean> future;
    long timeSet;
    boolean relevant;
    State state;
//    long updateTime;


    public ExportTag(String tagname, Tag tag) {
        this.tag = tag;
        this.curval = TagPlain.create(tag);
        this.curval.setStatus( tag.getStatus() );
        this.state = State.NEED_SET;
        this.type = tag.getType().toString().charAt(0);

        this.encodedName = TagInfoCodec.encodeName(tagname);
        prepareEncodedData();
    }


    private void prepareEncodedData() {
        encodedData = TagInfoCodec.encodeData(
                curval.getString(),
                System.currentTimeMillis(),
                type,
                curval.getStatus() == Tag.Status.Good);
    }


    public void update() {
        tag.copyValueTo(curval);
        curval.setStatus( tag.getStatus() );
        prepareEncodedData();
    }


    public boolean isOutdated() {
        return !tag.equalsValue( curval )  ||  tag.getStatus() != curval.getStatus();
    }


    public String getEncodedData() {
        return encodedData;
    }

    public String getEncodedMessageUpdate() {
        return TagInfoCodec.encodeMessageUpdate(encodedName, encodedData);
    }


}
