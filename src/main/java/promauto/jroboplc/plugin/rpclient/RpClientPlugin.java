package promauto.jroboplc.plugin.rpclient;

import promauto.jroboplc.core.AbstractPlugin;
import promauto.jroboplc.core.api.Module;

public class RpClientPlugin extends AbstractPlugin {
//	private final Logger logger = LoggerFactory.getLogger(RpClientPlugin.class);

	private static final String PLUGIN_NAME = "rpclient";
	
	@Override
	public void initialize() {
		super.initialize();
    }
	
	@Override
    public String getPluginName() {
    	return PLUGIN_NAME;
    }

	@Override
    public String getPluginDescription(){
    	return "Client connection to RpSvrTcp server";
    }
	
//	@Override
//    public Class<?> getModuleClass(){
//    	return RpClientModule.class;
//    }
	

	@Override
	public Module createModule(String name, Object conf) {
		RpClientModule m = new RpClientModule(this, name);
    	modules.add(m);
    	return m.load(conf)? m: null;
	}

}
