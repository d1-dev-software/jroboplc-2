package promauto.jroboplc.plugin.rpclient;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.AbstractModule;
import promauto.jroboplc.core.api.*;
import promauto.jroboplc.core.tags.TagInt;
import promauto.jroboplc.core.tags.TagRWInt;
import promauto.utils.CRC;
import promauto.utils.Numbers;
import promauto.utils.Strings;

public class RpClientModule extends AbstractModule {
	private static final int BUFF_SIZE = 32768;
	private final Logger logger = LoggerFactory.getLogger(RpClientModule.class);
	private final static Charset charset = Charset.forName("UTF-8");
	
	private   Socket socket = null;
//	protected BufferedReader socketReader;
	protected InputStream streamReader;
	protected BufferedWriter socketWriter;
	
	protected TagRWInt[] tags = new TagRWInt[0];
	protected Map<Tag,Integer> irrtags = new HashMap<>();
	
	protected String 	tagfilter;
	protected int 		reconnectTimeSec;
	protected int 		timeoutMs;
	protected Tag 		tagHost;
	protected Tag 		tagPort;
	protected Tag 		tagConnected;
	protected Tag 		tagReconnectCounter;
	protected Tag 		tagDisconnectCounter;

	private Map<String,String> alarmValues = new HashMap<>();
	
	
    protected boolean connected = false;
    private long cooldownTime = 0;
    protected byte[] buffin = new byte[BUFF_SIZE];
	protected int buffinBeg;
	protected int buffinEnd;
    private boolean	hasMore;
    
	private StringBuilder sb = new StringBuilder();
	private int indexRead;
//	private String flagExternal;
	private boolean answerNotSupported;
	
	private boolean needToGetAll;

	private boolean useCrcTagValues;
	private boolean useMessages;
	private long crcTheirs;
	private int[] buffCrcTagValuesH;
	private int[] buffCrcTagValuesL;
	
	protected Console debugConsole = null;
	protected boolean debugInfoWrite = false;
    private boolean hasFetchChanges;

//	protected Path pathCache;
	
	
	public RpClientModule(Plugin plugin, String name) {
		super(plugin, name);
		env.getCmdDispatcher().addCommand(this, CmdDebugInfoWrite.class);
	}


	@Override
	public boolean canHaveExternalTags() {
		return true;
	}


	public boolean loadModule(Object conf) {
		Configuration cm = env.getConfiguration();
		
		tagfilter			= cm.get(conf, "filter", "*");
		reconnectTimeSec	= cm.get(conf, "recon_s", 60);
		timeoutMs			= cm.get(conf, "timeout_ms", 3000);
		tagHost				= tagtable.createString("host.addr", cm.get(conf, "host", "localhost"));
		tagPort				= tagtable.createInt("host.port", cm.get(conf, "port", 3033));
		tagConnected		= tagtable.createBool("connected", false);
		tagReconnectCounter = tagtable.createInt("reconnect.cnt", 0);
		tagDisconnectCounter = tagtable.createInt("disconnect.cnt", 0);

//		pathCache = cm.getPath(conf, "file.cache", "cache/" + name + ".cache");

		alarmValues.clear();
		for(Object obj: cm.toList(cm.get(conf, "alarm.values")))
			alarmValues.put( cm.get(obj, "tag", ""), cm.get(obj, "value", ""));

		return true;
	}

	
	
	@Override
	public boolean prepareModule() {
		
		tagReconnectCounter.setInt(0);
		tagDisconnectCounter.setInt(0);
		
		connect();
		
		if( !connected )
			env.printInfo(logger, name, "No connection"); 
		
		return true;
	}

	
	




	@Override
	public boolean closedownModule() {
		if (!enable) return true;
		
		disconnect();
		
		return true;
	}





	@Override
	public boolean executeModule() {

		try {
			if (cooldownTime > 0) {
				if (isCooldownOver()) {
					resetCooldown();
					if( connect() ) {
//                        if (hasFetchChanges)
//                            postSignal(Signal.SignalType.RELOADED);
                    }
				}
			} else {
				boolean ok = isConnected();
				if (ok)
					try {
						ok = writeValues() && writeIrrFlags() && readValues(needToGetAll);
						readMessages();
					} catch (Exception e) {
						ok = false;
					}

				if (!ok) {
					disconnect();
					setupCooldown();

					int cnt = tagDisconnectCounter.getInt() + 1;
					tagDisconnectCounter.setInt(cnt);
					env.printInfo(logger, name, "Connection lost, cnt:", "" + cnt);

					applyAlarmValues();
				}
			}

		} catch (Exception e) {
			env.printError(logger, e, name);
		}

		return true;
	}



	private void applyAlarmValues() {
		for(String tagexpr: alarmValues.keySet()) {
			Pattern p = Pattern.compile(tagexpr);
			String value = alarmValues.get(tagexpr);

			for (TagRWInt tag : tags)
				if( p.matcher(tag.getName()).matches() )
					tag.setReadValString( value );

		}
	}


	private boolean connect() {
	    if( connected  ||  !enable )
	        return true;

	    try {
	    	int reconnectCnt = tagReconnectCounter.getInt();
            tagReconnectCounter.setInt( reconnectCnt + 1 );
	    	connectSocket();
	    	
	    	connected = isConnected();
		    if( connected ) {
                postSignal(Signal.SignalType.CONNECTED);

                if( handshake()  &&  fetchTags() ) {
	                // success
	                tagConnected.setBool(true);
	                if( reconnectCnt > 0 ) 
	                	env.printInfo(logger, name, "Connection restored");
	                resetCooldown();
	                return true;
	            }
		    }
	    } 
	    catch(IOException e) {
	    	// silent on timeouts or i/o errors
	    }
	    catch(Exception e) {
			env.printError(logger, e, name);
	    }
	    

	    disconnect();
	    setupCooldown();
	    return false;
	}



	protected void connectSocket() throws SocketException, IOException {
		socket = new Socket();
		socket.setSoTimeout(timeoutMs);
		socket.connect( new InetSocketAddress(tagHost.toString(), tagPort.getInt()), timeoutMs );
		streamReader = socket.getInputStream();
//		socketReader = new BufferedReader(new InputStreamReader(socket.getInputStream(), charset), BUFF_SIZE);
		socketWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream(), charset), BUFF_SIZE);
	}

	
	private void disconnect() {
	    if( !connected )
	        return;

	    for(Tag tag: tags)
	    	if(tag != null)
		        if( tag.getStatus() == Tag.Status.Good )
		            tag.setStatus(Tag.Status.Bad);

    	if( socket != null ) {
		    try {
	    		disconnectSocket();
		    } catch(Exception e) {
				env.printError(logger, e, name);
		    }
	    	socket = null;
    	}
	    
	    connected = false;
	    tagConnected.setBool(false);

        postSignal(Signal.SignalType.DISCONNECTED);
    }



	protected void disconnectSocket() throws IOException {
		socket.close();
	}

	
	private boolean handshake() throws IOException {
	    if( getAnswer("", false) )
	    	if( buffinStartsWith("100-Roboplant OPC Server (TCP)") )
	    		return true;

	    return false;
	}


	protected boolean isConnected() {
		if( socket != null  &&	socket.isConnected()  &&  socket.isBound()  &&  !socket.isClosed() )
			return true;
		
		return false;
	}


	
	private boolean fetchTags() throws IOException {
	    hasFetchChanges = false;
	    Set<String> tagnames = new HashSet<>();

	    if( !request("SETFILTER " + tagfilter, "101", false) )
	        return false;

	    if( !request("CREATETAGLIST", "103", false ) )
	        return false;

	    
	    int total = Numbers.hexToInt(buffin, buffinBeg, buffinEnd - buffinBeg); 
	    TagRWInt[] tmptags = new TagRWInt[total];

	    if( total > 0 ) {
	        int index = 0;
	        while( true ) {

	            if( !request("GETTAGLIST " + Numbers.toHexString(index), "104", true ) ) 
	                return false;

	            List<String> list = new LinkedList<>();
	            if( !parseListAnswer(list, index) )
	                return false;

	            // add tag 
	            for(String tagname: list) {
		       		 if( index >= tmptags.length )
		 		        break;
	
		 		    Tag tag = tagtable.get(tagname);
		 		    TagRWInt tagrw = null;
		 		    if( tag != null  &&  tag instanceof TagRWInt ) {
		 		    	tagrw = (TagRWInt)tag;
		 		    }
		 		    if( tagrw == null ) {
		 		    	tagrw = tagtable.add( new TagRWInt( tagname, 0, Flags.EXTERNAL) );
		 		    	tagrw.setStatus( Tag.Status.Uninitiated );
                        hasFetchChanges = true;
                    }
		 		    else
		 		        if( tagrw.getStatus() == Tag.Status.Deleted ) {
                            tagrw.setStatus(Tag.Status.Uninitiated);
                            hasFetchChanges = true;
                        }
	
		 		    tmptags[index++] = tagrw;	
	            }
	            
	            tagnames.addAll(list);

	            if( !hasMore )
	                break;
	        }
	        
		    if( index != total ) {
    			env.printError(logger, name, "Fetch tags error, index:" + index +" != total" + total);
    			return false;
		    }
	    }
	    

	    tags = tmptags;

	    // process deleted tags
	    List<Tag> deltags = new LinkedList<>();
	    for(Tag tag: tagtable.values())
	    	if( tag.hasFlags( Flags.EXTERNAL ) )
	    		if( !tagnames.contains(tag.getName()) )
	    			deltags.add( tag );

	    for(Tag tag: deltags) {
            tagtable.remove(tag);
            hasFetchChanges = true;
        }
	    	

	    // initial read values
	    useCrcTagValues = true;
	    useMessages = true;
	    if( total > 0 ) {
	        if( !readValues(true) )
	            return false;
	        
	        if( !readIrrTags() )
	            return false;
	    }

	    if( hasFetchChanges)
	        postSignal(Signal.SignalType.RELOADED);

	    return true;
	}



	


	private boolean isCooldownOver() {
	    return cooldownTime < System.currentTimeMillis();
	}

	
	private void setupCooldown() {
	    cooldownTime = System.currentTimeMillis() + reconnectTimeSec * 1000;
	}

	
	private void resetCooldown() {
		cooldownTime = 0;
	}


	private boolean readMessages() throws IOException {
		if( !useMessages )
			return true;

		if( !request("GETMSG", "119", false) ) {
			if(answerNotSupported)
				useMessages = false;
			return false;
		}


        if( buffinBeg < buffinEnd ) {
            String msg = new String(buffin, buffinBeg, buffinEnd - buffinBeg);
            if( msg.startsWith("RELOAD") )
                onMessageReload();
            else
                env.printInfo(logger, name, "received unknown message:", msg);
        }

        return true;
	}

	private void onMessageReload() throws IOException {
		env.printInfo(logger, name, "received message RELOAD from server");
		fetchTags();
	}


	private boolean writeValues() throws IOException {
	    int i = 0;
	    for(TagRWInt tag: tags) {
	        if( tag.hasWriteValue() ) {
	        	if( debugInfoWrite  &&  debugConsole != null) {
	        		debugConsole.print(name + " write: " + tag.getName() + " = "+ tag.getInt() + " -> " + 
	        				tag.getWriteValInt() + "\r\n");
	        	}
	            int value = tag.getWriteValInt();
	            if( !writeValue("WNM", "109", i, value) ) {
	    			env.printError(logger, name, "Write error:", tag.getName(), "= "+ value);
//	                continue;
	            }
	        }
	        ++i;
	    }
	    return true;
	}

	
	private boolean writeIrrFlags() throws IOException {
		if( irrtags.isEmpty() )
			return true;
		
	    for(Map.Entry<Tag,Integer> ent: irrtags.entrySet()) {
	    	Tag auxtag = ent.getKey().getObject();
	    	int idx = ent.getValue();
	    	if( auxtag != null  &&  auxtag.getInt() > 0 ) {
	            if( !writeValue("SETFLAG", "117", idx, auxtag.getInt()) ) {
	    			env.printError(logger, name, "Set flag error:", ent.getKey().getName(), auxtag.getName(), "= " + auxtag.getInt());
	            } else {
					auxtag.setInt(0);
				}
	        }
	    }
	    return true;
	}

	
	public boolean writeValue(String cmd, String prefix, int index, int value) throws IOException {
        sb.setLength(0);
        sb.append(Numbers.toHexChars(index));
        if(value<0)
        	sb.append(" -");
        else
        	sb.append(' ');
        sb.append(Numbers.toHexChars( Math.abs(value) ));
        
        int crc = CRC.getCrc16( sb.toString().getBytes(charset) );

        sb.insert(0, ' ');
        sb.insert(0, cmd);
        sb.append(' ');
        sb.append( Numbers.toHexChars(crc) );

        if( !request(sb.toString(), prefix, false) )
            return false;

        if( buffin[buffinBeg] != '!' )
        	return false;
        			
		return true;
	}


	
	private boolean readValues(boolean _get_all) throws IOException {
		   if( !request("FIXALL", "105", false) )
		        return false;

		    int changed_val_count = Numbers.hexToInt(buffin, buffinBeg, buffinEnd - buffinBeg);

		    if( changed_val_count == 0  &&  !_get_all )
		        return true;
		    
		    
		    
		   if( useCrcTagValues ) {
			   if( !request("GETCRC", "118", false) ) {
					if (answerNotSupported)
						useCrcTagValues = false;
					else
						return false;
			   } else {
//				   crcTagValues = answer;
				   crcTheirs = Numbers.hexToLong(buffin, buffinBeg, buffinEnd - buffinBeg);
			   }
		   }
		   


		    indexRead = 0;
		    while(true) {
		    	sb.setLength(0);
		        if( _get_all )
		        {
		            if( !request(sb.append("GETALL ").append(Numbers.toHexChars(indexRead)).toString(), "106", true ) )
		                return false;
		        }
		        else
		        {
		            if( !request(sb.append("GETCHG ").append(Numbers.toHexChars(indexRead)).toString(), "107", true ) )
		                return false;
		        }

		        if( !parseValues() )
		            return false;

		        if( !hasMore )
		            break;
		    }

		    
		    if( _get_all )
		    	needToGetAll = false;
		    
		   if( useCrcTagValues ) {
			   if( buffCrcTagValuesH == null  ||  buffCrcTagValuesH.length != tags.length ) {
				   buffCrcTagValuesH = new int[tags.length];
				   buffCrcTagValuesL = new int[tags.length];
			   }
			   
			   int n = tags.length;
			   for(int i=0; i<n; ++i) {
				   int value = tags[i].getInt();
				   buffCrcTagValuesH[i] = (value >> 8) & 0xFF;
				   buffCrcTagValuesL[i] = value & 0xFF;
			   }
			   long crcH = CRC.getCrc16(buffCrcTagValuesH, n);
			   long crcL = CRC.getCrc16(buffCrcTagValuesL, n);
			   long crcOurs = (crcH << 16) + crcL;
			   needToGetAll = crcOurs != crcTheirs;
//			   if( needToGetAll )
//				   logger.debug( "[readValues] " + name + " NeedToGetAll " + crcOurs + " != " + crcTheirs);
		   }


		    return true;	
	}
	
	
	
	private boolean readIrrTags() throws IOException {
		irrtags.clear();
		indexRead = 0;
		while (true) {
			sb.setLength(0);
			if (!request(sb.append("GETPROPS ").append(Numbers.toHexChars(indexRead)).toString(), "116", true)) {
				if (answerNotSupported)
					return true;
				else
					return false;
			}

			if (!parseProps() )
				return false;

			if (!hasMore)
				break;
		}

		return true;
	}




	private boolean request(String _command, String _answer, boolean _use_crc) throws IOException {
		clearSockerReader();
		
		socketWriter.write( _command );
		socketWriter.write( "\r\n" );
		socketWriter.flush();

	    if( !getAnswer(_answer, _use_crc) )
	    	return false;

	    return true;
	}



	protected void clearSockerReader() throws IOException {
		int n;
		while( (n = streamReader.available()) > 0 )
			streamReader.skip(n);
	}


	private boolean getAnswer(String prefix, boolean use_crc) throws IOException {
		hasMore = false;

		if( !readBuffin() )
			return false;
		
//		System.out.println( new String(buffin, buffinBeg, buffinEnd - buffinBeg) );
//		answer = new String(buffin, buffinBeg, buffinEnd - buffinBeg);

		if (!prefix.isEmpty()) {
			if( !buffinStartsWith(prefix) ) {
//				answerNotSupported = buffinStartsWith("400 NOT SUPPORTED");
				answerNotSupported = buffinStartsWith("400");
				return false;
			}
			buffinBeg = prefix.length() + 1;
		}

		// check and delete crc
		if (use_crc) {
			boolean has_more = false;
			int k = buffinGetPosReverse('=');
			if (k == -1) {
				k = buffinGetPosReverse('~');
				has_more = true;
			}

			if (k == -1)
				return false;

			int crc_their = Numbers.hexToInt(buffin, k+1, buffinEnd-k-1);
			buffinEnd = k;

			int crc_mine = CRC.getCrc16(buffin, buffinBeg, buffinEnd-1);

			if (crc_mine != crc_their)
				return false;

			hasMore = has_more;
		}
		
		return true;
	}	

	
	protected boolean readBuffin() throws IOException {
		int i = 0;
		int n;
	read:
		while( true ) {
			n = Math.max(1, streamReader.available());
			if( i + n > BUFF_SIZE )
				return false;
			
			n = streamReader.read(buffin, i, n);
			if( n == -1 )
				return false;
			
			n = i + n;
			while( i < n )
				if( buffin[i] == 13  ||  buffin[i] == 10 )
					break read;
				else
					i++;
		}
		buffinBeg = 0;
		buffinEnd = i;		
		return true;
	}



	private boolean buffinStartsWith(String substr) {
		return Strings.bytesStartsWith(substr, buffin, buffinBeg, buffinEnd);
	}

	private int buffinGetPosReverse(int ch) {
		return Strings.getPosReverse(ch, buffin, buffinBeg, buffinEnd);
	}
	
	private int buffinGetPos(int ch) {
		return Strings.getPos(ch, buffin, buffinBeg, buffinEnd);
	}
	
	

	private boolean parseListAnswer(List<String> list, int index) {
	    int p1 = buffinGetPos('#'); 
	    int p2 = buffinGetPos('!'); 

	    int size = Numbers.hexToInt(buffin, buffinBeg, p1-buffinBeg);
	    if( size == 0 )
	        return true;

	    int index_their = Numbers.hexToInt(buffin, p1+1, p2-p1-1);

	    if( index != index_their )
	        return false;
	    
	    buffinBeg = p2+1;
	    int k = 0;
	    for(int i=0; i<size; ++i) {
	    	k = buffinGetPos(';');
	    	if( k == -1 ) {
	    		list.add( new String(buffin, buffinBeg, buffinEnd - buffinBeg) );
	    		break;
	    	}
	    	list.add( new String(buffin, buffinBeg, k - buffinBeg) );
	    	buffinBeg = k+1;
	    }

	    return true;
	}



	private boolean parseValues() {
		int p = buffinGetPos('#');
		if( p == -1 )
			return false;
		int size = Numbers.hexToInt(buffin, buffinBeg, p-buffinBeg); 
		if (size == 0)
			return true;

		buffinBeg = p;
		int b = 0;
		int index = -1;
		int value = 0;
		boolean sign = false;
		int index_max = tags.length;
		while( true ) {
			if( buffinBeg >= buffinEnd) {
				if( index < 0  &&  index >= index_max )
					return false;
				tags[index++].setReadValInt(sign? -value: value);
				break;
			}
			
			b = buffin[ buffinBeg++ ] & 0xFF;
			
			if( b == ';'  ||  b == '#' ) {
				if( index >= 0 ) {
					if( index >= index_max )
						return false;
					tags[index++].setReadValInt(sign? -value: value);
					value = 0;
					sign = false;
				}
				
				if( b == '#' ) {
					p = buffinGetPos('!');
					if( p == -1 )
						return false;
					index = Numbers.hexToInt(buffin, buffinBeg, p-buffinBeg); 
					buffinBeg = p + 1;
				} 
			} else 
				if( b == '-' )
					sign = !sign;
				else
					value = (value << 4) + Numbers.asciiDigitToInt(b);
				
		}

		indexRead = index;
		return true;
	}


	private boolean parseProps() {
		int p = buffinGetPos('#');
		if( p == -1 )
			return false;
		int size = Numbers.hexToInt(buffin, buffinBeg, p-buffinBeg); 
		if (size == 0)
			return true;
		buffinBeg = p + 1;
		
		p = buffinGetPos('!');
		if( p == -1 )
			return false;
		int index = Numbers.hexToInt(buffin, buffinBeg, p-buffinBeg); 
		buffinBeg = p + 1;
		
		int b = 0;
		int value = 0;
		int index_max = tags.length;
		boolean endflag = false;
		while( !endflag ) {
			
			if( !(endflag = (buffinBeg >= buffinEnd)) )
				b = buffin[ buffinBeg++ ] & 0xFF;
			
			if( b == ';'  ||  endflag) {
				if( index >= index_max )
					return false;
				
				Tag tag = tags[index];
				
				if (value == 1 )
					irrtags.put(tag, index);

				if (value == 1 && tag.getObject() == null) {
					tag.setObject(new TagInt("IRR", 0));
				} else

				if (value == 0 && tag.getObject() != null) {
					tag.setObject(null);
				}

				index++;
				value = 0;
			} else
				value = (value << 4) + Numbers.asciiDigitToInt(b);
		}

		indexRead = index;
		return true;
	}


	
	

//	private void loadFromCache() {
//		// TODO Auto-generated method stub
//		
//	}
//
//
//	private void saveToCache() {
//		// TODO Auto-generated method stub
//		
////		for(Tag tag: tags) {
////			tag.getName()
////		}
//		
//	}



	
	@Override
	public String getInfo() {
		return enable?
				(!connected? ANSI.RED + ANSI.BOLD + "ERROR! " + ANSI.RESET: "") +
					String.format("%-16s ", tagHost.toString() + ":" + tagPort.toString() ) +
					" discon=" + tagDisconnectCounter + 
					" recon=" + tagReconnectCounter + 
					" tags=" + tags.length
				: "disabled";
	}


	
	@Override
	protected boolean reload() {
		RpClientModule tmp = new RpClientModule(plugin, name);
		if( !tmp.load() )
			return false;

		closedown();

		copySettingsFrom(tmp);

		alarmValues = tmp.alarmValues;
		tagfilter = tmp.tagfilter;
		reconnectTimeSec = tmp.reconnectTimeSec;
		timeoutMs = tmp.timeoutMs;
		cooldownTime = 0;
		tmp.tagHost.copyValueTo( tagHost );
		tmp.tagPort.copyValueTo( tagPort );
		tmp.tagConnected.copyValueTo( tagConnected );
		tmp.tagReconnectCounter.copyValueTo( tagReconnectCounter );
		tmp.tagDisconnectCounter.copyValueTo( tagDisconnectCounter );
		
		return prepare();
	}




	
	
	

}











