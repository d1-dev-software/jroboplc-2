package promauto.jroboplc.plugin.system;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.Module;
import promauto.jroboplc.core.api.Tag;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SystemCommands {
    private final Logger logger = LoggerFactory.getLogger(SystemCommands.class);

    public static class PTag {
        Pattern patern;
        Tag tag;
    }


    public static class Cmd {
        String cmdline;
        List<PTag> ptags = new LinkedList<>();
    }

    protected  List<Cmd> cmds = new LinkedList<>();

    public void load(Module module, Object conf) {
        cmds.clear();
        Configuration cm = EnvironmentInst.get().getConfiguration();
        for( Object cmd_conf: cm.toList( cm.get(conf, "syscommands") )) {
            Cmd syscmd = new Cmd();
            syscmd.cmdline = cm.get(cmd_conf, "cmdline", "");
            for( Object tag_conf: cm.toList( cm.get(cmd_conf, "tags") )) {
                String tagname = cm.get(tag_conf, "name", "");
                String tagtype = cm.get(tag_conf, "type", "int");
                String regex = cm.get(tag_conf, "regex", "(.*)");

                PTag tag = new PTag();
                tag.tag = module.getTagTable().createTag(tagtype, tagname, "");
                tag.patern = Pattern.compile(regex);
                syscmd.ptags.add(tag);
            }
            cmds.add(syscmd);
        }

    }

    public void execute() {
        String cmdline = "";
        try {
            for( Cmd syscmd: cmds) {
                cmdline = syscmd.cmdline;
                Process p = Runtime.getRuntime().exec(cmdline);
                p.waitFor(10, TimeUnit.SECONDS);

                BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
                String s = null;
                while ((s = stdInput.readLine()) != null) {
                    for(PTag cmdtag: syscmd.ptags) {
                        Matcher m = cmdtag.patern.matcher(s);
                        if( m.find() )
                            cmdtag.tag.setString( m.group(1) );
                    }
                }
            }

        } catch (IOException |InterruptedException e) {
            EnvironmentInst.get().printError(logger, e, cmdline);
        }
    }

}
