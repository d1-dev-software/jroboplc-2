package promauto.jroboplc.plugin.system;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.AbstractModule;
import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.Plugin;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.Ref;
import promauto.jroboplc.core.tags.TagCallableInt;
import promauto.jroboplc.core.tags.TagRW;

public class SystemModule extends AbstractModule {
	private final Logger logger = LoggerFactory.getLogger(SystemModule.class);

	private final Runtime runtime = Runtime.getRuntime();
	
	private SystemPlugin plugin;

	protected boolean sdEnabled;
	protected Ref refShutdown = new Ref();
	protected int sdValue;
	protected int sdDelay;
	protected TagRW tagSdCnt;
	protected TagRW tagSdState;
	protected Tag tagSdEnable;

	// tagShutdownState values:
	private static final int SD_STATE_OK = 0;			// no need to shutdown
	private static final int SD_STATE_COUNTDOWN = 1;	// counting down before shutdown
	private static final int SD_STATE_CANCELED = 2;	// disabled until next shutdown request
	private static final int SD_STATE_SHUTDOWN = 3;	// shutdown
	
	protected String sdCommand;

	protected SystemCommands syscmds = new SystemCommands();
	private Tag tagMemTotal;
	private Tag tagMemUsed;
	private Tag tagMemMax;

	protected TagRW tagLicDate;

	public SystemModule(Plugin plugin, String name) { 
		super(plugin, name);
		this.plugin = (SystemPlugin)plugin;
	}

	
	
	public boolean loadModule(Object conf) {
		Configuration cm = env.getConfiguration();
		Object shutdown_conf = cm.get(conf, "shutdown");
		sdEnabled = cm.toMap( shutdown_conf ).size() > 0;
		
		if( sdEnabled ) {
			if( !refShutdown.init(shutdown_conf, "tag", this) ) {
				env.printError(logger, name, "No shutdown tag");
				return false;
			}
				
			sdValue = cm.get(shutdown_conf, "value", 1);
			sdDelay = cm.get(shutdown_conf, "delay_s", 60);
			tagSdCnt = tagtable.createRWInt("shutdown.cnt", 0);
			tagSdState = tagtable.createRWInt("shutdown.state", 0);
			tagSdEnable = tagtable.createBool("shutdown.enable", cm.get(shutdown_conf, "enable", true) );
			
			sdCommand = cm.get(shutdown_conf, "cmd."+plugin.getOsName(), "");
			if( sdCommand.isEmpty() )
				switch( plugin.getOsType() ) {
					case Linux: 	sdCommand = "shutdown -h now"; break; 
					case Windows: 	sdCommand = "shutdown.exe -s -t 0"; break; 
				}
		}

		syscmds.load(this, conf );

		tagMemTotal = tagtable.createInt("MemTotal", 0);
		tagMemUsed = tagtable.createInt("MemUsed", 0);
		tagMemMax = tagtable.createInt("MemMax", 0);

		tagLicDate = tagtable.createRWInt("licdate", env.getLoggerMode().get());

		return true;
	}

	
	
	@Override
	public boolean prepareModule() {
		if( sdEnabled ) {
			if( !refShutdown.prepare() ) {
				env.printError(logger, name, "Bad shutdown tag ref");
				return false;
			}
			tagSdState.setReadValInt(0);
		}

		return true;
	}


	



	@Override
	public boolean executeModule() {
		if( sdEnabled )
			if( refShutdown.linkIfNotValid() ) {
				if( (refShutdown.getInt() == sdValue)  &&  (tagSdEnable.getBool()) ) {

					if( tagSdState.getInt() == SD_STATE_OK) {
						tagSdCnt.setReadValInt(sdDelay);
						tagSdState.setReadValInt(SD_STATE_COUNTDOWN);
					}

					if( tagSdState.hasWriteValue() ) {
						int v = tagSdState.getWriteValInt();
						if( v == SD_STATE_COUNTDOWN ||  v == SD_STATE_CANCELED)
							tagSdState.setReadValInt( v );
					}


					if( tagSdState.getInt() == SD_STATE_COUNTDOWN) {
						if( tagSdCnt.getInt() <= 0 ) {
							// shutdown!
							doShutdown();

						} else {
							if( tagSdCnt.hasWriteValue() )
								tagSdCnt.setReadValInt( tagSdCnt.getWriteValInt() );
							else
								tagSdCnt.setReadValInt( tagSdCnt.getInt() - 1 );
						}
					}
				} else {
					if( tagSdState.getInt() != SD_STATE_OK)
						tagSdState.setReadValInt(SD_STATE_OK);
				}
		}

		syscmds.execute();

		tagMemTotal.setInt( (int)(runtime.totalMemory() / 1024) );
		tagMemUsed.setInt( (int)((runtime.totalMemory() - runtime.freeMemory()) / 1024) );
		tagMemMax.setInt( (int)(runtime.maxMemory() / 1024) );

		return true;
	}

	private void doShutdown() {
		env.printInfo(logger, '['+name+']', "Shutting system down!!!");
		tagSdState.setReadValInt(SD_STATE_SHUTDOWN);

		try {
            Process p = Runtime.getRuntime().exec(sdCommand);
            p.waitFor(10, TimeUnit.SECONDS);

            BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
            BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));
            String s = null;

            String output = "";
            while ((s = stdInput.readLine()) != null)
                output += s;
            if( !output.isEmpty() )
                env.printInfo(logger, output);

            output = "";
            while ((s = stdError.readLine()) != null)
                output += s;
            if( !output.isEmpty() )
                env.printError(logger, name, output);

        } catch (IOException |InterruptedException e) {
            env.printError(logger, e, name);
        }
	}


	@Override
	public String getInfo() {
		return "";
	}



	
	@Override
	protected boolean reload() {

		SystemModule tmp = new SystemModule(plugin, name);
		if( !tmp.load()  ||  !tmp.prepare() )
			return false;

		copySettingsFrom(tmp);
		
		sdEnabled = tmp.sdEnabled;
		refShutdown = tmp.refShutdown;
		sdValue = tmp.sdValue;
		sdDelay = tmp.sdDelay;
		sdCommand = tmp.sdCommand;
		
		if( tagSdCnt == null  &&  tmp.tagSdCnt != null ) {
			tagtable.add( tagSdCnt = tmp.tagSdCnt );
			tagtable.add( tagSdState = tmp.tagSdState );
		} else

		if( tagSdCnt != null  &&  tmp.tagSdCnt == null ) {
			tagtable.remove(tagSdCnt);
			tagtable.remove(tagSdState);
			tagSdCnt = null;
			tagSdState = null;
		}

		// transfer syscmd tags
		for(SystemCommands.Cmd cmd: syscmds.cmds)
			for(SystemCommands.PTag ptag: cmd.ptags) {
				Tag tag = tmp.tagtable.get(ptag.tag.getName());
				if (tag == null || tag.getType() != ptag.tag.getType())
					tagtable.remove(ptag.tag);
			}

		for(SystemCommands.Cmd cmd: tmp.syscmds.cmds)
			for(SystemCommands.PTag ptag: cmd.ptags) {
				Tag tag = tagtable.get(ptag.tag.getName());
				if (tag != null) {
					ptag.tag = tag;
				} else
					tagtable.add(ptag.tag);
			}
		syscmds = tmp.syscmds;


		return true;
	}

}











