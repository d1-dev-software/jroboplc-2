package promauto.jroboplc.plugin.arcsvr;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.AbstractCommand;
import promauto.jroboplc.core.DbHelper;
import promauto.jroboplc.core.api.Console;
import promauto.jroboplc.core.api.Database;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.Module;

import java.sql.Statement;

public class CmdSweepmes extends AbstractCommand {
	private final Logger logger = LoggerFactory.getLogger(CmdSweepmes.class);


	@Override
	public String getName() {
		return "sweepmes";
	}

	@Override
	public String getUsage() {
		return "year";
	}

	@Override
	public String getDescription() {
		return "deletes old messages for specified year and all before";
	}

	
	
	
	@Override
	public String execute(Console console, Module module, String args) {
		module.postCommand(this, console, module, args);
		return "";
	}


	@Override
	public void executePosted(Console console, Module module, String args) {

        ArcsvrModule w = (ArcsvrModule) module;

        Database db = w.getDatabase();
        if (!db.isConnected()) {
            console.print("Database " + db.getName() + " is not connected\n");
            return;
        }

        int year;
        try {
            year = Integer.parseInt(args);
        } catch (NumberFormatException e) {
            console.print("Invalid year: " + args + "\n");
            return;
        }

        console.print("\n");
        String tbl = db.makeSchemaObjectName(w.getSchema(), "messages");
        DbHelper.sweep(console, db, tbl,"dt", year);
        console.print("OK\n");
    }



}
