package promauto.jroboplc.plugin.arcsvr;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.AbstractModule;
import promauto.jroboplc.core.api.*;

public class ArcsvrModule extends AbstractModule implements Signal.Listener {
	private final Logger logger = LoggerFactory.getLogger(ArcsvrModule.class);

	private String databaseModuleName;
	private String schema;
	private Database database;

    protected ArcvalHolder arcvalHolder = new ArcvalHolder( this);
    protected MesHolder mesHolder = new MesHolder( this);


    private boolean connected;
	private Tag tagConnected;
	private boolean errorOnInit = false;
	private boolean reloading;
	private long lastTimeCycle;
    private volatile boolean needLink;
    private volatile boolean needInit;
	private String placename;


	public ArcsvrModule(Plugin plugin, String name) {
        super(plugin, name);
		env.getCmdDispatcher().addCommand(this, CmdPack.class);
		env.getCmdDispatcher().addCommand(this, CmdSweepmes.class);
		env.getCmdDispatcher().addCommand(this, CmdNolink.class);

	}


	public Database getDatabase() {
		return database;
	}

	public String getSchema() {
		return schema;
	}


	public String makeTableName(String table) {
		return database.makeSchemaObjectName(schema, table);
	}



	public final boolean loadModule(Object conf) {
		Configuration cm = env.getConfiguration();
		databaseModuleName = cm.get(conf, "database", "db");
		schema = cm.get(conf, "schema", "");
		placename = cm.get(conf, "place", name);

		if( (tagConnected = tagtable.get("connected")) == null )
			tagConnected = tagtable.createBool("connected", false);

		// Warning! Use existing tags and not recreate them inside the holders.
        boolean res = arcvalHolder.load(conf);
        res &= mesHolder.load(conf);

		reloading = false;

		return res;
	}
	

	@Override
	public final boolean prepareModule() {
		
		Module db = env.getModuleManager().getModule(databaseModuleName);
		if( db == null ) {
			env.printError(logger, name, "Database module is not found:", databaseModuleName );
			return false;
		}

		if( !(db instanceof Database) ) {
			env.printError(logger, name, "Not a database:", databaseModuleName );
			return false;
		}

		if(reloading)
			return true;


		database = (Database)db;

        env.getModuleManager().getModules().stream()
                .filter( m -> m != this )
                .forEach( m -> m.addSignalListener(this) );


        if( arcvalHolder.isEnable()  &&  !database.hasScript("arcsvr_arcval_init") ) {
            String resourceName = "dbscr/dbscr.arcsvr_arcval.yml";
            if (database.loadScriptFromResource(ArcsvrPlugin.class, resourceName) == null) {
                return false;
            }
        }

        if( mesHolder.isEnable()  &&  !database.hasScript("arcsvr_arcmes_init") ) {
            String resourceName = "dbscr/dbscr.arcsvr_arcmes.yml";
            if (database.loadScriptFromResource(ArcsvrPlugin.class, resourceName) == null) {
                return false;
            }
        }

        needLink = true;
        needInit = true;

		return true;
	}


    @Override
    public void onSignal(Module sender, Signal signal) {
        if( signal.type == Signal.SignalType.RELOADED ) {
            needLink = true;
        } else

        if( sender == database ) {
            if (signal.type == Signal.SignalType.CONNECTED) {
                needInit = true;
            } else

            if( signal.type == Signal.SignalType.DISCONNECTED) {
                tagConnected.setBool(connected = false);
            }
        }
    }


    private void link() {
        arcvalHolder.link();
        mesHolder.link();
		needLink = false;
    }


	private void init() {
		errorOnInit = false;
		tagConnected.setBool( connected = false );

		if( !enable  ||  database == null  ||  !database.isConnected())
			return;

        boolean res = true;

        res &= arcvalHolder.init();
        res &= mesHolder.init();

		errorOnInit = !res;
        tagConnected.setBool( connected = res );

		if( errorOnInit )
			env.printError(logger, name, "Initialization error");

		needInit = false;
	}



	@Override
	public boolean closedownModule() {
		if (!enable  || reloading ) return true;

		tagConnected.setBool( connected = false );

		env.getModuleManager().getModules().stream()
                .forEach( m -> m.removeSignalListener(this) );

        arcvalHolder.closedown();
        mesHolder.closedown();

		return true;
	}



	@Override
	public boolean executeModule() {
		long t = System.currentTimeMillis();

        if(needLink)
            link();

        if(needInit)
            init();

        if( !connected )
			return true;

        try {
            arcvalHolder.execute();
            mesHolder.execute();

            database.commit();
        } catch (Exception e) {
            env.printError(logger, e, name);
            database.rollback();
        }

        lastTimeCycle = System.currentTimeMillis() - t;

		return true;
	}



	@Override
	public String getInfo() {
		if( !enable )
			return "disabled";

		StringBuilder sb = new StringBuilder();

		sb.append(
				(errorOnInit? ANSI.RED + ANSI.BOLD + "ERROR! " + ANSI.RESET: "") +
				databaseModuleName + ", " +
				(schema.isEmpty()? "": "schema=" + schema + ", ") +
				(connected? "connected": "NOT CONNECTED!") );

		if(connected) {
			sb.append(", cycle=" + lastTimeCycle + ":");
            sb.append(arcvalHolder.getInfo());
            sb.append(mesHolder.getInfo());
		}

		return sb.toString();
	}


	
	@Override
	protected boolean reload() {
		ArcsvrModule tmp = new ArcsvrModule( plugin, name );
		boolean res = tmp.load();
		tmp.reloading = true;
		res &= tmp.prepare();
		tmp.closedown();

		if( res ) {
			Object conf = env.getConfiguration().getModuleConf(	plugin.getPluginName(),	name);
			closedown();
			if( load(conf) )
				if( prepare() )
					res = true;
		}

		return res;

	}

	public String pack(String arcname) {
    	return arcvalHolder.pack(arcname);
	}

	public String showNolinks() {
		String s1 = arcvalHolder.showNolinks();
		String s2 = mesHolder.showNolinks();
		String s = (s1.isEmpty()? "": s1 + "\r\n") + (s2.isEmpty()? "": s2);
		return (s.isEmpty()? "": s);
	}

    public String getPlaceName() {
		return placename;
    }
}