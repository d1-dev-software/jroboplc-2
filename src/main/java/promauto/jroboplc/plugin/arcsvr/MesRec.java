package promauto.jroboplc.plugin.arcsvr;

import promauto.jroboplc.core.api.Module;
import promauto.jroboplc.core.api.Tag;

public class MesRec {
    Tag tag = null;
    int idtag;
    int value;
    String descr;
    boolean active = false;

    Module mod = null;
    boolean phantom = false;
    boolean tobeRemoved = false;

    String placename;
    int placeid;
}
