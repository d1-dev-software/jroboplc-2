package promauto.jroboplc.plugin.kkormsvr;


import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.AbstractModule;
import promauto.jroboplc.core.api.*;

public class KkormsvrModule extends AbstractModule implements Signal.Listener {
	private final Logger logger = LoggerFactory.getLogger(KkormsvrModule.class);


	private Database database;
	private String databaseModuleName;
	private List<KkLine> lines = new ArrayList<>();

	private Statement statement;
	private boolean connected;
	private Tag tagConnected;

	public final LiveData livedata;
	private DateTimeFormatter formatter;
	private boolean emulated;

	public KkormsvrModule(Plugin plugin, String name) {
        super(plugin, name);
		env.getCmdDispatcher().addCommand(this, CmdImport.class);
		env.getCmdDispatcher().addCommand(this, CmdSweep.class);

		livedata = new LiveData(this);
	}

	public Database getDatabase() {
		return database;
	}

	public Statement getStatement() {
		return statement;
	}



	public final boolean loadModule(Object conf) {
		Configuration cm = env.getConfiguration();
		emulated 	= cm.get(conf, "emulate", false);
		databaseModuleName = cm.get(conf, "database", "db");

		for(Object lineconf: cm.toList(cm.get(conf, "lines"))) {
			KkLine line = new KkLine(this);
			if( !line.load(lineconf) )
				return false;

			lines.add(line);
		}
		tagConnected = tagtable.createBool("connected", false);

		return true;
	}
	

	@Override
	public final boolean prepareModule() {
		if( emulated )
			return true;

		Module module = env.getModuleManager().getModule(databaseModuleName);
		if( module == null ) {
			env.printError(logger, name, "Not found a database module:", databaseModuleName );
			return false;
		}

		if( !(module instanceof Database) ) {
			env.printError(logger, name, "Not a database:", databaseModuleName );
			return false;
		}

		database = (Database)module;
		database.addSignalListener(this);

		formatter = database.getTimestampFormatter();

		String resourceName = "dbscr/dbscr.kkormsvr.yml";
        if( database.loadScriptFromResource(KkormsvrPlugin.class, resourceName)==null )
        	return false;

		lines.forEach(KkLine::prepare);

		init();

		return true;
	}


	@Override
	public void onSignal(Module sender, Signal signal) {
		if( emulated )
			return;

		if( signal.type == Signal.SignalType.CONNECTED ) {
			if( !connected ) {
				init();
				postSignal(Signal.SignalType.RELOADED);
			}
		} else

		if( signal.type == Signal.SignalType.DISCONNECTED ) {
            tagConnected.setBool( connected = false );
		}
	}



	private void init() {

		tagConnected.setBool( connected = false );

		if( !enable  ||  database == null  ||  !database.isConnected())
			return;

		boolean res = true;

        res &= database.executeScript("kkormsvr.init1" ).success;
		res &= database.executeScript("kkormsvr.init2" ).success;
		res &= initLines();

        tagConnected.setBool( connected = res );

        if( !res )
            env.printError(logger, name, "Initialization error");
	}



	private boolean initLines() {
		boolean result = true;

		try(Statement st = database.getConnection().createStatement() ) {
			statement = st;

			livedata.load();
			for (KkLine line : lines)
				line.loadLiveData();

			for (KkLine line : lines)
				line.init();

			database.commit();
		} catch (SQLException e) {
//			env.printError(logger, e, name, "lastSql:", lastSql);
			env.printError(logger, e, name);
			database.rollback();
			result = false;
		}
		statement = null;

		return result;
	}




	@Override
	public boolean executeModule() {
		if( emulated )
			return true;

		if( !connected  ||  !database.isConnected() )
			return true;

        Connection con = database.getConnection();
		try(Statement st = con.createStatement() ) {
			statement = st;

			for(KkLine line: lines)
				line.execute();

			livedata.update();

			st.close();
			database.commit();

		} catch (Exception e) {
			env.printError(logger, e, name);
			database.rollback();
		}
		statement = null;

		return true;
	}



	@Override
	public boolean closedownModule() {
		if (enable) {
			tagConnected.setBool(connected = false);

			if (database != null)
				database.removeSignalListener(this);
		}
		return true;
	}


	public DateTimeFormatter getFormatter() {
		return formatter;
	}


	@Override
	public String getInfo() {
		if( !enable )
			return "disabled";

		if( emulated )
			return "emulated";

		return "database=" + databaseModuleName + " "
				+ (connected? "": ANSI.redBold("NOT CONNECTED! ")) + "\r\n"
				+ lines.stream()
					.map(KkLine::getInfo)
					.collect(Collectors.joining("\r\n"));
	}


	@Override
	public String check() {
		return lines.stream()
				.map(KkLine::getProblems)
				.filter(s -> !s.isEmpty())
				.collect(Collectors.joining("\r\n \r\n"));
	}


	@Override
	protected boolean reload() {
		KkormsvrModule tmp = new KkormsvrModule( plugin, name );
		if( !tmp.load() )
			return false;

		closedown();

		copySettingsFrom(tmp);

		tagtable.removeAll();
		tmp.tagtable.values().forEach(tag -> tagtable.add(tag));
		tmp.tagtable.clear();

		tagConnected = tmp.tagConnected;
		lines = tmp.lines;

		prepare();

		return true;
	}


}