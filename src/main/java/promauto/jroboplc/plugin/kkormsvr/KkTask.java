package promauto.jroboplc.plugin.kkormsvr;

import java.util.HashMap;
import java.util.Map;

public class KkTask {

    int taskId;
    int receiptId;
    String receiptName;
    int cycleReq;

    KkTask(int taskId, int receiptId, String receiptName, int cycleReq) {
        this.taskId = taskId;
        this.receiptId = receiptId;
        this.receiptName = receiptName;
        this.cycleReq = cycleReq;
    }


    static class Detail {
        String productName;
        int productId;
        int storageId;
        long weight;
        boolean applied = false;

        Detail(String productName, int productId, int storageId, long weight) {
            this.productName = productName;
            this.productId = productId;
            this.storageId = storageId;
            this.weight = weight;
        }
    }
    public Map<Integer, Detail> details = new HashMap<>();


    void addDetail(String productName, int productId, int storageId, long weight) {
        Detail detail = new Detail(productName, productId, storageId, weight);
        details.put(storageId, detail);
    }


    boolean applied() {
        return details.values().stream().allMatch(detail -> detail.applied);
    }
}
