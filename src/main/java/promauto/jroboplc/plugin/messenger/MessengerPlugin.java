package promauto.jroboplc.plugin.messenger;

import promauto.jroboplc.core.AbstractPlugin;
import promauto.jroboplc.core.api.Module;

public class MessengerPlugin extends AbstractPlugin {

	private static final String PLUGIN_NAME = "messenger";
	
	@Override
	public void initialize() {
		super.initialize();
    }
	
	@Override
    public String getPluginName() {
    	return PLUGIN_NAME;
    }

	@Override
    public String getPluginDescription(){
    	return "Send messages via email";
    }
	
//	@Override
//    public Class<?> getModuleClass(){
//    	return MessengerModule.class;
//    }
	

	@Override
	public Module createModule(String name, Object conf) {
		MessengerModule m = new MessengerModule(this, name);
    	modules.add(m);
    	return m.load(conf)? m: null;
	}

}
