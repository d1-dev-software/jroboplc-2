package promauto.jroboplc.plugin.serial;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.SerialPort;
import promauto.jroboplc.core.tags.TagRW;

import java.io.IOException;
import java.net.*;

public class SerialPortFinsUdp implements SerialPort {
	final Logger logger = LoggerFactory.getLogger(SerialPortFinsUdp.class);

	private DatagramSocket socket;
	private DatagramPacket packetout;
	private DatagramPacket packetin;

	private volatile boolean valid = true;
	private volatile boolean opened = false;

	private String host;
	private int port;
	private InetAddress addr;

	private static final int BUFFOUT_SIZE = 2048;
	private static final int BUFFIN_SIZE = 2048;
	private byte[] buffin = new byte[BUFFIN_SIZE];
	private byte[] buffout = new byte[BUFFOUT_SIZE];
	private int buffinPos;
	private int buffinLast;

	private int id;
	private int timeout_ms;

	private boolean connectionLost = false;
	private long connectionLostTime = 0;
	private long recon_ms = 3000;
	private long liveness = 0;
	private SerialManagerModule module;
	private TagRW tagOpened;
	private int transactSent;




	public SerialPortFinsUdp(SerialManagerModule module) {
		this.module = module;
		transactSent = 0;
	}

	public synchronized boolean load(Object conf) {
		Configuration cm = EnvironmentInst.get().getConfiguration();
		
		id 			= cm.get(conf, "id",			0);
		recon_ms 	= cm.get(conf, "recon_ms", 		3000);
		timeout_ms 	= cm.get(conf, "timeout", 		500);

		host = cm.get(conf, "host", "");
		port = cm.get(conf, "port", 0);
		try {
			addr = InetAddress.getByName(host);
		} catch (UnknownHostException e) {
			EnvironmentInst.get().printError(logger, e, module.getName(), "host = " + host);
			return false;
		}

		tagOpened = module.getTagOpened(id);
		
		return true;
	}
	
	
	
	@Override
	public int getId() {
		return id;
	}

	@Override
	public synchronized boolean setParams(int baud, int databits, int parity, int stopbits, int timeout) {
		this.timeout_ms = timeout;
		boolean res = true;
		if( opened ) {
			try {
				socket.setSoTimeout(timeout_ms);
			} catch (SocketException e) {
				res = false;
			}
		}
		return res;
	}

	@Override
	public synchronized boolean open() {
		return open(false);
	}

	public synchronized boolean open(boolean silent) {
		if( opened ) 
			return true;
		
		opened = connectSocket(silent);
		if( !opened ) {
			connectionLost = true;
			disconnectSocket();
		} else {
			connectionLost = false;
			tagOpened.setReadValInt(1);
		}
	
		return opened;
	}

	
	
	
	protected boolean connectSocket(boolean silent) {
		resetBuffin();
		try {
			socket = new DatagramSocket();
			socket.setSoTimeout(timeout_ms);
			packetout = new DatagramPacket(buffout, 0, addr, port);
			packetin = new DatagramPacket(buffin, buffin.length);
		} catch (Exception e) {
			if( !silent )
				EnvironmentInst.get().printError(logger, e, module.getName(), "Connect socket:", getDescr());
			return false;
		}
		return true;
	}

	
	private void disconnectSocket() {
		resetBuffin();
		if( socket != null ) {
			try {
				socket.close();
			} catch (Exception e) {
				EnvironmentInst.get().printError(logger, e, module.getName(), "Disconnect socket:", getDescr());
			}
			socket = null;
		}
		opened = false;
		tagOpened.setReadValInt( 0 );
	}

	

	
	@Override
	public synchronized void close() {
		if( !opened ) 
			return;

		disconnectSocket();
		connectionLost = false;
	}

	
	private void checkLiveness() {
		
		boolean ok = socket.isBound();
		ok &= !socket.isClosed();

		if( ok ) {
			if( liveness == 0 ) {
				liveness  = System.currentTimeMillis();
				return;
			}
			
			if( System.currentTimeMillis() - liveness < recon_ms )
				return;
		}
		
		liveness = 0;
		
		if( opened ) {
			disconnectSocket();
			if( open(true) ) 
				return;
			
			EnvironmentInst.get().printError(logger, module.getName(), getDescr(), "Connection lost");
			connectionLost  = true;
			connectionLostTime = System.currentTimeMillis();
		}
	}
	
	private void affirmLiveness() {
		liveness = 0;
	}


	
	private String getDescr() {
		return "Serial port " + id + " (FinsUdp " + host + ":" + port +  ")";
	}

	private boolean restoreConnectionIfLost() {
		if( !connectionLost )
			return true;
		
		if( System.currentTimeMillis() - connectionLostTime < recon_ms )
			return false;
		
		if( open(true) ) {
			EnvironmentInst.get().printInfo(logger, getDescr(), "Connection restored");
			return true;
		}
		
		connectionLostTime = System.currentTimeMillis();
		return false;
	}

	


	@Override
	public boolean isOpened() {
		return opened;
	}

	@Override
	public boolean isValid() {
		return valid;
	}

	@Override
	public void setInvalid() {
		valid = false;
	}

	
	
	@Override
	public int getAvailable() throws IOException {
		return buffinLast - buffinPos + 1;
	}

	
	
	
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<	

	private boolean updateAnswer() {
		if( !restoreConnectionIfLost()  ||  !opened )
			return false;

		if( isBuffinEmpty() )
			if( !receiveAnswer() ) {
				checkLiveness();
				return false;
			}
		
		affirmLiveness();

		return true;
	}


	private boolean receiveAnswer() {
		try {
			socket.receive(packetin);
		} catch (IOException e) {
			// error: timeout
			return false;
		}

		buffinLast = packetin.getLength() - 1;
		buffinPos = 10;

		if( buffinLast <= 9 ) {
			// error: format
			return false;
		}

		if( (buffin[9] & 0xff) != transactSent ) {
			// error: transactID
			return false;
		}

		return true;
	}
	

	private int getByteFromBuffin() {
		return buffin[ buffinPos++ ] & 0xFF;
	}
	
	private boolean isBuffinAvailable() {
		return buffinPos >= buffinLast;
	}

	private void resetBuffin() {
		buffinPos = 0;
		buffinLast = 0;
	}

	private boolean isBuffinEmpty() {
		return buffinLast == 0;
	}

//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

	
	@Override
	public synchronized int readByte() {
		if( !updateAnswer() )
			return -1;
		
		if( isBuffinAvailable() )
			return -1;
		
		return getByteFromBuffin();
	}


	@Override
	public synchronized int readBytes(int[] buff, int size) {
		if( !updateAnswer() )
			return 0;
		
		if( isBuffinAvailable() )
			return 0;

		int n = Math.min(buffinLast - buffinPos + 1, size);
		for( int i=0; i<n; i++)
			buff[i] = getByteFromBuffin(); 
				
		return n;
	}


	
	
	
	
	@Override
	public synchronized int readBytesDelim(int[] buff, int delim) {
		// not implemented
		return 0;
	}


	
	@Override
	public synchronized String readString(int size) {
		// not implemented
		return "";
	}

	
	@Override
	public synchronized String readStringDelim(int delim) {
		// not implemented
		return "";
	}

	
	@Override
	public synchronized boolean writeByte(int data) {
		// not implemented
		return false;
	}

	
	@Override
	public synchronized boolean writeBytes(int[] data, int size) {
		if( !restoreConnectionIfLost() )
			return false;
		
		if( !opened )
			return false;
		
		resetBuffin();
		
		transactSent = (++transactSent) & 0xFF;
		
		int bufflen = size+10;
		if( bufflen > BUFFOUT_SIZE) {
			EnvironmentInst.get().printError(logger, module.getName(), "writeBytes: data size is too big " +
					bufflen + " > " + BUFFOUT_SIZE);
			return false;
		}

		buffout[0] = (byte)0x80;
		buffout[1] = 0;
		buffout[2] = 3;
		buffout[3] = 0;
		buffout[4] = 0;
		buffout[5] = 0;
		buffout[6] = 0;
		buffout[7] = 9;
		buffout[8] = 0;
		buffout[9] = (byte)(transactSent & 0xFF);

		for(int i=10, j=0; j<size; ++i, ++j)
			buffout[i] = (byte)data[j];

		packetout.setLength(bufflen);

		try {
			socket.send(packetout);
		} catch (IOException e) {
			checkLiveness();
			return false;
		}

		return true;
	}

	
	@Override
	public synchronized boolean writeString(String data) {
		// not implemented
		return false;
	}



	@Override
	public synchronized boolean discard() {
		if( !opened )
			return false;
		
		resetBuffin();

		return true;
	}

	
	@Override
	public String getInfo() {
		return String.format("%d: finsudp %s:%d %d %s",
				id,
				host,
				port,
				timeout_ms,
				(opened? "opened": "closed"));
	}

}
