package promauto.jroboplc.core.api;

import promauto.jroboplc.core.PatternEx;

import java.nio.file.Path;
import java.util.List;
import java.util.Map;

public interface Configuration {
	
	Path getConfDir();
	
	boolean load();
	
	
	Map<String,Object> getRoot();
	
	Object getModuleConf(String plugin, String module);

	Path getPath(Object conf, String key, String defval);
	
	
	Object get(Object conf, String key);

	int get(Object conf, String key, int defval);

	long get(Object conf, String key, long defval);

	double get(Object conf, String key, double defval);
	
	boolean get(Object conf, String key, boolean defval);
	
	String get(Object conf, String key, String defval);

	List<String> getStringList(Object conf, String key);
	
	
	Map<String,Object> toMap(Object conf);

	List<Object> toList(Object conf);

	<K,V> Map<K,V> toGenericMap(Object conf);

}
