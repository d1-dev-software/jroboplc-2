package promauto.jroboplc.core.api;

public interface TcpSubscriber {

	boolean onTcpServerRequest(TcpServerChannel channel, String request);

	boolean onTcpServerClientConnected(TcpServerChannel channel);

	boolean onTcpServerClientDisconnected(TcpServerChannel channel);

}
