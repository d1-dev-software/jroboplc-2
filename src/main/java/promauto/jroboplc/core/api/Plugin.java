package promauto.jroboplc.core.api;

import java.nio.file.Path;
import java.util.List;

import org.slf4j.Logger;

public interface Plugin { 
	
	
	void initialize();
	
	
	String getPluginName();


	String getPluginDescription();
	
	
//	Class<?> getModuleClass();
	

	Module createModule(String name, Object conf);
	

	List<Module> getModules();
	


}
