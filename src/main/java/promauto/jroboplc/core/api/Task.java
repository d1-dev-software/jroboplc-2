package promauto.jroboplc.core.api;

import java.util.Set;

public interface Task {
	
	String getName();
	
	boolean isRunning();
	
	boolean hasModule(String module);
	
	Set<String> getModules();

	int getPeriod();

}
