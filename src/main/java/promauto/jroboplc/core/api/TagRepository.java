package promauto.jroboplc.core.api;

import java.sql.SQLException;
import java.util.List;

public interface TagRepository {

    boolean init();

    void load(String group, Tag tag);
    void load(String group, List<Tag> tags);

    void save(String group, Tag tag);
    void save(String group, List<Tag> tags);

    void persist() throws SQLException;
}
