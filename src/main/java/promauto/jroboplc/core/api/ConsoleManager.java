package promauto.jroboplc.core.api;

import java.util.Set;

public interface ConsoleManager {
	
	boolean addConsole(Console console);
	
	boolean removeConsole(Console console);
	
	Set<Console> getConsoles();

	void printToAll(String text);
	
}
