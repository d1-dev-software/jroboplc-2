package promauto.jroboplc.core.api;

/**
 * This class is a definition and a handler for one text-based command.
 *
 */
public interface Command {
	
	String getName();

	String getUsage();
	
	String getDescription();
	
	String execute(Console console, String args);
	
	String execute(Console console, Plugin plugin, String args);
	
	String execute(Console console, Module module, String args);

	void executePosted(Console console, Module module, String args);
	
}
