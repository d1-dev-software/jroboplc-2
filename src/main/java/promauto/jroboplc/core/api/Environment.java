package promauto.jroboplc.core.api; 

import javax.xml.stream.XMLInputFactory;

import org.slf4j.Logger;
import promauto.jroboplc.core.LoggerMode;

public interface Environment {


    LoggerMode getLoggerMode();

    Configuration getConfiguration();
	void setConfiguration(Configuration conf);


	KeyManager getKeyManager();

	
	CmdDispatcher getCmdDispatcher();

	void setCmdDispatcher(CmdDispatcher cmdDispatcher);

	
	
	ModuleManager getModuleManager();
	
	void setModuleManager(ModuleManager moduleManager);


	
	TaskManager getTaskManager();
	
	void setTaskManager(TaskManager taskManager);

	
	
	ConsoleManager getConsoleManager();

	void setConsoleManager(ConsoleManager consoleManager);
	
	Console getConsole();

	void setConsole(Console console);

	
	
	SerialManager getSerialManager();
	
	void setSerialManager(SerialManager serialManager);

	
	
	TcpServer getTcpServer();
	
	void setTcpServer(TcpServer tcpserver);
	
		
	
	boolean isRunning();
	
	void setRunning(boolean status);

	
	
	boolean isTerminated();
	
	void setTerminated();
	
	
	

	XMLInputFactory getXMLInputFactory();

	
	void printError(Logger logger, String... text);
	void printError(Logger logger, Throwable e, String... text);

    void logError(Logger logger, String... text);

    void logError(Logger logger, Throwable e, String... text);

    void printInfo(Logger logger, String... text);

    void logInfo(Logger logger, String... text);

    void logStatus(Logger logger, String... text);

    void print(String text);

}
