package promauto.jroboplc.core.api;

public class Signal {

    public enum SignalType {
        RELOADED,
        CONNECTED,
        DISCONNECTED,
        NOTIFICATION
    }

    public interface Listener {
        void onSignal(Module sender, Signal signal);
    }

    public SignalType type;
    public Object data = null;

    public Signal(SignalType type) {
        this.type = type;
    }

    public Signal(SignalType type, Object data) {
        this.type = type;
        this.data = data;
    }

    public String getDataAsString() {
        if( data != null )
            return data.toString();
        return "";
    }

    @Override
    public String toString() {
        return type.toString() + (data==null? "": data.toString());
    }
}
