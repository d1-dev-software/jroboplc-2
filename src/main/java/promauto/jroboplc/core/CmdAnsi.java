package promauto.jroboplc.core;

import promauto.jroboplc.core.api.Console;

public class CmdAnsi extends AbstractCommand {

	@Override
	public String getName() {
		return "ansi";
	}
	
	@Override
	public String getUsage() {
		return "off|on";
	}

	@Override
	public String getDescription() {
		return "turns off/on ansi colors";
	}


	@Override
	public String execute(Console console, String args) {
		console.setProperty("ansi", args);

		return "OK";
	}


}
