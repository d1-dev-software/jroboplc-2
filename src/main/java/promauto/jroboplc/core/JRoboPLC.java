package promauto.jroboplc.core;

import java.net.URL;
import java.nio.file.Paths;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.log4j.helpers.LogLog;
import org.apache.log4j.xml.DOMConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.api.ANSI;
import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.EnvironmentInst;



public class JRoboPLC { 
	final Logger logger = LoggerFactory.getLogger(JRoboPLC.class); 
	private EnvironmentImpl env;

	private static final String LOGGER_CONF = "/logger.xml";
	private static final String DEFAULT_LOGDIR = "log";
	
	private String plugDir = "jar/plugin";
	private String confDir = "conf";
	private boolean quiet = false;
	private String startCommands = "";

	
	private boolean parseCommandLine(String[] args) {
		Options options = new Options();
		options.addOption("cfg", true, "configuration directory, default is '" + confDir + "'");
		options.addOption("plg", true, "plugins directory, default is '" + plugDir + "'");
		options.addOption("cmd", true, "start up commands ('|' - separator)");
		options.addOption("quiet", false, "disable output and interactive console");
		try {
			CommandLineParser parser = new DefaultParser();
			CommandLine cmd = parser.parse( options, args);
			
			plugDir = cmd.hasOption("plg")? cmd.getOptionValue("plg"): plugDir;
			confDir = cmd.hasOption("cfg")? cmd.getOptionValue("cfg"): confDir;
			startCommands = cmd.hasOption("cmd")? cmd.getOptionValue("cmd"): startCommands;
			quiet = cmd.hasOption("quiet");
			
			return true;
		} catch (ParseException e) {
			new HelpFormatter().printHelp( "jroboplc", options );
		}
		return false;
	}

	
	private boolean initialize() {
		env = new EnvironmentImpl();
		EnvironmentInst.set(env);
		
		Configuration conf = new ConfigurationYaml( confDir );
		if( !conf.load() ) {
			return false;
		}
		
		env.setConfiguration(conf);

		
		if( !setupLogger() )
			return false;

		env.logStatus(logger, "*** INIT v" + Version.getVersionStr() + " ***");

		env.setConsoleManager( new ConsoleManagerImpl() );
		if (quiet)
			env.setConsole( new ConsoleDaemon() );
		else
			env.setConsole( new ConsoleInteract(env) );
		env.getConsole().print("JRoboPLC v" + Version.getVersionStr() + " loading...\r\n");

		env.setCmdDispatcher( new CmdDispatcherImpl() );
		env.setTaskManager(new TaskManagerImpl(env));
		
		addGlobalCommands();
		
		env.setModuleManager( new ModuleManagerImpl() );

		boolean result = 
//				env.getModuleManager().loadPlugins(plugDir) &&
				env.getModuleManager().loadModules();

		if( env.getSerialManager() == null )
			env.setSerialManager( new SerialManagerStub() );

		if( env.getTcpServer() == null )
			env.setTcpServer( new TcpServerStub() );

		if (!result)
			env.getConsole().print("Fatal configuration error!\r\n");
		
		attachShutDownHook();

		return result;
	}

	
	private boolean setupLogger() {
		Configuration cm = env.getConfiguration();

		String logdir = cm.getPath(cm.getRoot(), "log.dir", DEFAULT_LOGDIR ).resolve( Paths.get(confDir).getFileName() ).toString();
		System.setProperty("jroboplc.logdir", logdir );
		LogLog.setQuietMode(true);

        URL resourceUrl = getClass().getResource(LOGGER_CONF);
        DOMConfigurator.configure(resourceUrl);
		LogLog.setQuietMode(false);

		env.setLoggerMode( new LoggerMode() );
		return env.getLoggerMode().isOk();
	}
	

	private void addGlobalCommands() {
		env.getCmdDispatcher().addCommand( CmdQuit.class );
		env.getCmdDispatcher().addCommand( CmdHelp.class );
		env.getCmdDispatcher().addCommand( CmdStart.class);
		env.getCmdDispatcher().addCommand( CmdStop.class);
		env.getCmdDispatcher().addCommand( CmdTaglist.class);
		env.getCmdDispatcher().addCommand( CmdList.class);
		env.getCmdDispatcher().addCommand( CmdListAgain.class);
		env.getCmdDispatcher().addCommand( CmdRepeat.class);
		env.getCmdDispatcher().addCommand( CmdListFwd.class);
		env.getCmdDispatcher().addCommand( CmdListBack.class);
		env.getCmdDispatcher().addCommand( CmdSettag.class);
		env.getCmdDispatcher().addCommand( CmdStx.class);
		env.getCmdDispatcher().addCommand( CmdModinfo.class);
		env.getCmdDispatcher().addCommand( CmdAnsi.class);
		env.getCmdDispatcher().addCommand( CmdReload.class);
		env.getCmdDispatcher().addCommand( CmdAppend.class);
		env.getCmdDispatcher().addCommand( CmdRemove.class);
		env.getCmdDispatcher().addCommand( CmdState.class);
		env.getCmdDispatcher().addCommand( CmdCheck.class);
		env.getCmdDispatcher().addCommand( CmdSusp.class);
		env.getCmdDispatcher().addCommand( CmdVersion.class);
		env.getCmdDispatcher().addCommand( CmdGc.class);
		env.getCmdDispatcher().addCommand( CmdKeygen.class);
	}

	
	
	private void run() {
        env.logStatus(logger, "*** READY ***");
		if (!quiet) {
			env.getConsole().print("OK\r\n(use 'h' for help)\r\n");
		} else { 
			env.getConsole().print("Starting..." + env.getTaskManager().start() + "\r\n");
			if (!env.isRunning())
				return;
		}

		executeStartCommands();
		
		try {
			while (!env.isTerminated()) {
				env.getConsole().process();
				Thread.sleep(100);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	

	private void executeStartCommands() {
		if (!startCommands.isEmpty()) {
			String[] cmds = startCommands.split("\\s*\\|\\s*");
			for (String cmd: cmds) {
				env.getConsole().print(
						ANSI.BLUE + "cmd: " + cmd + "\r\n" +
						env.getCmdDispatcher().execute(
								env.getConsole(),
								cmd) + "\r\n\r\n" + ANSI.RESET );
			}
		}
	}


	public void attachShutDownHook() {
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				if (env.isRunning()) {
					env.getConsole().print(ANSI.BLUE + "Stop PLC!\r\n" + ANSI.RESET);
					env.getTaskManager().stop();
				}
                env.logStatus(logger, "*** EXIT ***\r\n\r\n");
			}
		});
	}


	
	
	private void closedown() {
        env.logStatus(logger, "*** CLOSE ***");
	}



	
	public static void main(String[] args) {
		
		JRoboPLC app = new JRoboPLC();
		
		if( app.parseCommandLine(args)  &&  app.initialize() ) {
			app.run();
			app.closedown();
		}
		
		System.exit(0);
	}


	

}
