package promauto.jroboplc.core;

import io.lettuce.core.*;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.api.async.RedisAsyncCommands;
import io.lettuce.core.api.sync.RedisCommands;
import io.lettuce.core.dynamic.Commands;
import io.lettuce.core.dynamic.RedisCommandFactory;
import io.lettuce.core.dynamic.batch.BatchExecutor;
import io.lettuce.core.dynamic.batch.BatchSize;
import io.lettuce.core.dynamic.batch.CommandBatching;
import io.lettuce.core.output.KeyStreamingChannel;
import io.lettuce.core.pubsub.RedisPubSubListener;
import io.lettuce.core.pubsub.StatefulRedisPubSubConnection;
import io.lettuce.core.pubsub.api.async.RedisPubSubAsyncCommands;
import promauto.jroboplc.plugin.redisexp.TagInfo;
import promauto.utils.Strings;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import static promauto.jroboplc.plugin.redisexp.TagInfoCodec.decodeMessageWrite;

public class Probe {
    public static void main(String[] args) {
        int i = 12999;
        double d = i/1000;
        System.out.println( String.format("%.3f", d));
        System.out.println( String.format("%.3g", d));

        d = (double)i/1000;
        System.out.println( String.format("%.3f", d));
        System.out.println( String.format("%.3g", d));

    }

    public static void main2(String[] args) throws InterruptedException, ExecutionException, IOException, IOException, ExecutionException {
//        String host = "3.67.6.195";
        String host = "localhost";


        // Enabled keep alive
        SocketOptions socketOptions = SocketOptions.builder()
                .connectTimeout(Duration.ofSeconds(5))
                .keepAlive(true)
                .build();
        ClientOptions clientOptions = ClientOptions.builder()
                .socketOptions(socketOptions)
                .build();

//        RedisClient redisClient = RedisClient.create("redis://" + host + ":6379/0");
        RedisClient redisClient = RedisClient.create();
        RedisURI uri = RedisURI.builder()
//                .withTimeout(Duration.ofSeconds(1))
                .withHost(host).withPort(6379).build();

        redisClient.setOptions(clientOptions);

        StatefulRedisConnection<String, String> connection = redisClient.connect(uri);
        connection.addListener(System.out::println);

//        ConnectionFuture<StatefulRedisConnection<String, String>> ca = redisClient.connectAsync(StringCodec.UTF8, RedisURI.create("localhost", 6379));
//        StatefulRedisConnection<String, String> connection = ca.get();
        RedisCommands<String, String> syncCommands = connection.sync();
        syncCommands.set("keyB", "Hello, Redis!");
        String value = syncCommands.get("key");
        syncCommands.hset("recordName", "FirstName", "John");
        syncCommands.hset("recordName", "LastName", "Smith");
        Map<String, String> record = syncCommands.hgetall("recordName");
        record.forEach((k, v) -> System.out.println(k + ":" + v));

        // async
        final long t1 = System.nanoTime();
        RedisAsyncCommands<String, String> asyncCommands = connection.async();
        connection.addListener(message -> System.out.println("sync " + message));
        RedisFuture<String> res = asyncCommands.get("SHDSH.MDTA_510_RKS.descr");
        res.thenApply(s -> {
            long t2 = System.nanoTime();
            System.out.println(s);
            System.out.println("" + (t2 - t1));
            return true;
        });


//        RedisStringReactiveCommands<String, String> reactiveCommands = connection.reactive();

        // lists
        asyncCommands.lpush("tasks", "firstTask1");
        asyncCommands.lpush("tasks", "secondTask1");
        res = asyncCommands.rpop("tasks");
        res.whenComplete((s, thr) -> System.out.println(s));
        res.await(500, TimeUnit.MILLISECONDS);

        asyncCommands.del("tasks");
        asyncCommands.lpush("tasks", "firstTask2");
        asyncCommands.lpush("tasks", "secondTask2");
        res = asyncCommands.lpop("tasks");
        res.whenComplete((s, thr) -> System.out.println(s));

        // sets
        asyncCommands.sadd("pets", "dog");
        asyncCommands.sadd("pets", "cat");
        asyncCommands.sadd("pets", "cat");
        RedisFuture<Set<String>> pets = asyncCommands.smembers("pets");
        RedisFuture<Boolean> exists = asyncCommands.sismember("pets", "dog");
        pets.get().forEach(System.out::println);
        System.out.println(exists.get());

        // hashes
        asyncCommands.hset("recordName", "FirstName", "John");
        asyncCommands.hset("recordName", "LastName", "Smith");
        RedisFuture<String> lastName = asyncCommands.hget("recordName", "LastName");
        RedisFuture<Map<String, String>> record1 = asyncCommands.hgetall("recordName");
        System.out.println(lastName.get());
        record1.get().forEach((k, v) -> System.out.println(k + ":" + v));

        // sorted sets
        asyncCommands.zadd("sortedset", 1, "one");
        asyncCommands.zadd("sortedset", 4, "zero");
        asyncCommands.zadd("sortedset", 2, "two");
//        RedisFuture<List<String>> valuesForward = asyncCommands.zrange(key, 0, 3);
//        RedisFuture<List<String>> valuesReverse = asyncCommands.zrevrange(key, 0, 3);

        // transactions
        asyncCommands.multi();

        RedisFuture<String> result1 = asyncCommands.set("key1", "value1");
        RedisFuture<String> result2 = asyncCommands.set("key2", "value2");
        RedisFuture<String> result3 = asyncCommands.set("key3", "value3");

        RedisFuture<TransactionResult> execResult = asyncCommands.exec();
        TransactionResult transactionResult = execResult.get();

        String firstResult = transactionResult.get(0);
        String secondResult = transactionResult.get(1);
        String thirdResult = transactionResult.get(2);
        System.out.println(firstResult + " " + secondResult + " " + thirdResult);


        // batching
//        asyncCommands.setAutoFlushCommands(false);
//        List<RedisFuture<?>> futures = new ArrayList<>();
//        for (int i = 0; i < 10; i++) {
//            futures.add(asyncCommands.set("key-" + i, "value-" + i));
//        }
//        asyncCommands.flushCommands();
//        boolean result = LettuceFutures.awaitAll(5, TimeUnit.SECONDS, futures.toArray(new RedisFuture[0]));
//        System.out.println(result);

        // subscribe
        StatefulRedisPubSubConnection<String, String> subconn = redisClient.connectPubSub(uri);
        subconn.addListener(new MyListener());

//        subconn.setAutoFlushCommands(false);
        RedisPubSubAsyncCommands<String, String> async = subconn.async();
//        async.subscribe("update");
//        async.psubscribe("__keyevent@0__:del", "__keyevent@0__:expired", "__keyevent@0__:set");
//        async.psubscribe("*");
//        subconn.flushCommands();

        long t0 = System.currentTimeMillis();
        String channel = "hello-channel";
        for(int i=0; i<10000; ++i) {
//				template.convertAndSend(channel, msg + i);
            String msg = "Hello World! - " + i;
            syncCommands.publish(channel, msg);
        }
        long t2 = System.currentTimeMillis();
        System.out.println("time: " + (t2-t0));


        System.out.println(Strings.bytesToHexString("123Hello".getBytes(StandardCharsets.UTF_8)));
        System.out.println(Strings.bytesToHexString("Привет".getBytes(StandardCharsets.UTF_8)));



//        Thread.sleep(500);
        RedisFuture<String> future = null;
        for (; ; ) {
            int inp = System.in.read();
            if (inp == 48) //0
                break;

            if (inp == 49) { //1
                System.out.println("connection.isOpen() = " + connection.isOpen());
                System.out.println("connection = " + connection.toString());
                System.out.println("subconn.isOpen() = " + subconn.isOpen());
                System.out.println("subconn = " + subconn.toString());
            }

            if (inp == 50) { // 2
                asyncCommands.lpush("tasks", "firstTask1");
                asyncCommands.lpush("tasks", "secondTask1");
                res = asyncCommands.rpop("tasks");
                System.out.println(res.get());
//                res.whenComplete((s, thr) -> System.out.println(s));
//                res.await(500, TimeUnit.MILLISECONDS);
            }

            if (inp == 51) { // 3
                t2 = System.nanoTime();
                asyncCommands.setAutoFlushCommands(false);
                List<RedisFuture<?>> futures = new ArrayList<>();
                for (int i = 0; i < 100000; i++) {
                    futures.add(asyncCommands.set("ABCDE.MCHB_12345_Channel-" + i, "value-" + i));
                }

                long t3 = System.nanoTime();
                asyncCommands.flushCommands();
                long t4 = System.nanoTime();
                boolean result = LettuceFutures.awaitAll(30, TimeUnit.SECONDS, futures.toArray(new RedisFuture[0]));
                long t5 = System.nanoTime();
                System.out.println(result);
                asyncCommands.setAutoFlushCommands(true);
                System.out.println("time: " + ((t3-t2)/1000000.0));
                System.out.println("time: " + ((t4-t3)/1000000.0));
                System.out.println("time: " + ((t5-t4)/1000000.0));
                System.out.println("total: " + ((t5-t2)/1000000.0));
            }

            if (inp == 54) { // 6
                syncCommands.setTimeout(Duration.ofSeconds(5));
                t2 = System.nanoTime();
                for (int i = 0; i < 100000; i++) {
                    syncCommands.set("ABCDE.MCHB_12345_Channel-" + i, "value-" + i);
                }
                long t3 = System.nanoTime();
                System.out.println("time: " + ((t3-t2)/1000000.0));
            }

            if (inp == 52) { // 4
                t2 = System.nanoTime();
                for (int i = 0; i < 100000; i++) {
                    syncCommands.get("key-" + i);
                }
                long t3 = System.nanoTime();
                System.out.println("time: " + ((t3-t2)/1000000.0));
            }

            if (inp == 53) { // 5
                RedisFuture<List<String>> resKeys = asyncCommands.keys("*");
                List<String> keys = resKeys.get();
                System.out.println("===== keys =====");
                keys.forEach(System.out::println);
                System.out.println("================");
            }

            if (inp == 55) { // 7
                asyncCommands.setTimeout(Duration.ofSeconds(5));
                future = asyncCommands.set("aaa", "111");
                inp = 56;
            }
            if (inp == 56) { // 8
                if( future != null) {
                    System.out.println("isDone: " + future.isDone());
                    System.out.println("isCancelled: " + future.isCancelled());
                    System.out.println("error: " +  future.getError() );
                    System.out.println();

                }
            }
            if (inp == 57) { // 9
//                if( future != null) {
//                    future.cancel(true);
//                    inp = 56;
//                }
                asyncCommands.setAutoFlushCommands(false);

                RedisCommandFactory rcf = new RedisCommandFactory(connection);
                RedisBatchQuery batchQuery = rcf.getCommands(RedisBatchQuery.class);
                t2 = System.nanoTime();
                List<RedisFuture<?>> futures = new ArrayList<>();
                for (int i = 0; i < 1000000; i++) {
                    futures.add(batchQuery.set("ABCDE.MCHB_12345_Channel-" + i, "value-" + i, CommandBatching.queue()));
                }
                batchQuery.get("xxx", CommandBatching.flush());
//                batchQuery.flush();

                long t3 = System.nanoTime();
//                asyncCommands.flushCommands();
                long t4 = System.nanoTime();
                boolean result = LettuceFutures.awaitAll(600, TimeUnit.SECONDS, futures.toArray(new RedisFuture[0]));
                long t5 = System.nanoTime();
                System.out.println(result);
//                asyncCommands.setAutoFlushCommands(true);
                System.out.println("time: " + ((t3-t2)/1000000.0));
                System.out.println("time: " + ((t4-t3)/1000000.0));
                System.out.println("time: " + ((t5-t4)/1000000.0));

            }

            if( inp == 'a' ) {
                int n = 1000000;
                String[] keys = new String[n];
                for (int i = 0; i < n; i++) {
                    keys[i] = "ABCDE.MCHB_12345_Channel-" + i;
                }
                async.punsubscribe("__keyevent@0__:del", "__keyevent@0__:expired")
                        .thenCompose(unused -> {
                            System.out.println("deleting...");
                            return asyncCommands.del(keys);
                        })
                        .thenAccept(cnt -> {
                            async.psubscribe("__keyevent@0__:del", "__keyevent@0__:expired");
                            System.out.println("removed: " + cnt);
                        });
                asyncCommands.flushCommands();
            }

            if( inp == 'd' ) {
                int n = 1000000;
                CompletionStage<Long> futCombined = CompletableFuture.completedFuture(0L);
                for (int i = 0; i < n; i++) {
                    RedisFuture<Long> futDel = asyncCommands.del("ABCDE.MCHB_12345_Channel-" + i);
                    futCombined = futCombined.thenCombine(futDel, (a1, a2) -> a1 + a2);
                }
                asyncCommands.flushCommands();
                futCombined.thenAccept(a -> System.out.println(">>> removed: " + a));
            }

            if( inp == 's' ) {
                asyncCommands.keys("*")
                        .thenApply(keys -> keys.stream()
//                                .filter(key -> !etags.containsKey(key))
                                .collect(Collectors.toList()))
                        .thenCompose(keys -> {
                            CompletionStage<Long> result = CompletableFuture.completedFuture(0L);
                            for(String key: keys)
                                result = result.thenCombine(asyncCommands.del(key), Long::sum);
                            asyncCommands.flushCommands();
                            return result;
                        })
                        .thenAccept(removedCount -> {
                            if( removedCount > 0)
                                System.out.println("Removed unknown values: " + removedCount);
                            else
                                System.out.println("Nothing to remove");
                        });
            }

            if( inp == 'c' ) {
                connection.setAutoFlushCommands(false);

                List<CompletionStage<Long>> futures = new ArrayList<>();

                KeyStreamingChannel<String> stringKeyStreamingChannel = key -> {
                    futures.add( asyncCommands.del(key) );
                    System.out.println("key: " + key);
                };

                ScanArgs scanArgs = ScanArgs.Builder.limit(3);
                ((mycursor == null)?
                        asyncCommands.scan(stringKeyStreamingChannel, scanArgs) :
                        asyncCommands.scan(stringKeyStreamingChannel, mycursor, scanArgs))
                .thenCompose(cursor -> {
                    mycursor = cursor.isFinished()? null: cursor;
                    System.out.println("cursor.getCount() = " + cursor.getCount());
                    System.out.println("cursor.isFinished() = " + cursor.isFinished());

                    asyncCommands.flushCommands();
                    return futures.stream().reduce(
                            CompletableFuture.completedFuture(0L),
                            (future1, future2) -> future1.thenCombine(future2, Long::sum));
                })
                .thenAccept(cnt -> {
                    System.out.println("removed: " + cnt + " from " + futures.size());
                });
                asyncCommands.flushCommands();
            }

            if (inp == 'h') { // h
                t2 = System.nanoTime();
                asyncCommands.setAutoFlushCommands(false);
                List<RedisFuture<?>> futures = new ArrayList<>();
                for (int i = 0; i < 100000; i++) {
                    futures.add(asyncCommands.hset("jtag", "ABCDE.MCHB_12345_Channel-" + i, "value-" + i));
                }

                long t3 = System.nanoTime();
                asyncCommands.flushCommands();
                long t4 = System.nanoTime();
                boolean result = LettuceFutures.awaitAll(30, TimeUnit.SECONDS, futures.toArray(new RedisFuture[0]));
                long t5 = System.nanoTime();
                System.out.println(result);
                asyncCommands.setAutoFlushCommands(true);
                System.out.println("time: " + ((t3-t2)/1000000.0));
                System.out.println("time: " + ((t4-t3)/1000000.0));
                System.out.println("time: " + ((t5-t4)/1000000.0));
                System.out.println("total: " + ((t5-t2)/1000000.0));
            }


        }
    }
    private static StreamScanCursor mycursor = null;


    @BatchSize(50)
    interface RedisBatchQuery extends Commands, BatchExecutor {
        RedisFuture<String> set(String key, String value, CommandBatching batching);
        RedisFuture<String> get(String key, CommandBatching batching);
//        RedisFuture<String> get(String key, CommandBatching batching);
    }


    public static class MyListener implements RedisPubSubListener<String, String> {

        @Override
        public void message(String channel, String message) {
            System.out.println(channel + " -> " + message);
        }

        @Override
        public void message(String pattern, String channel, String message) {
            System.out.println(pattern + " -> " + channel + " -> " + message);
        }

        @Override
        public void subscribed(String channel, long count) {
            System.out.println("subscribed " + channel + ", " + count);
        }

        @Override
        public void psubscribed(String pattern, long count) {
            System.out.println("psubscribed " + pattern + ", " + count);
        }

        @Override
        public void unsubscribed(String channel, long count) {
            System.out.println("unsubscribed " + channel + ", " + count);
        }

        @Override
        public void punsubscribed(String pattern, long count) {
            System.out.println("punsubscribed " + pattern + ", " + count);
        }
    }

    public static void main1(String[] args)  {
        AtomicReference<Object> ar = new AtomicReference<>();
        Thread t = new Thread();
    }

}