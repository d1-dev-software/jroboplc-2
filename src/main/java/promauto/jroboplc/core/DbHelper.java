package promauto.jroboplc.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.api.Console;
import promauto.jroboplc.core.api.Database;
import promauto.jroboplc.core.api.EnvironmentInst;

import java.sql.*;
import java.util.HashSet;
import java.util.Set;

public class DbHelper {
    private static final Logger logger = LoggerFactory.getLogger(DbHelper.class);



    public static void clear(Console console, Database dbdst, String strTables) {
        String[] tbls = strTables.split("\\s*,\\s*");
        try(Statement st = dbdst.getConnection().createStatement() ) {
            for(String tbl: tbls) {
                console.print("clear " + tbl + ":");
                String sql = "delete from " + tbl;
                st.executeUpdate(sql);
                dbdst.commit();
                console.print(" OK\n");
            }
        } catch (Exception e) {
            EnvironmentInst.get().printError(logger, e);
            try {
                dbdst.rollback();
            } catch (Exception e1) {
            }
        }
    }



    public static void copygen(Console console, Database dbdst, String tbl, String gen, String id) {
        console.print("set " + gen + ":");
        String sql = "select max(" + id + ") from " + tbl;
        try(Statement st = dbdst.getConnection().createStatement(); ResultSet rs = st.executeQuery(sql)) {
            if( rs.next() ) {
                int value = rs.getInt(1);
                sql = "ALTER SEQUENCE " + gen + " RESTART WITH " + value;
                st.executeUpdate(sql);
            }
            dbdst.commit();
        } catch (Exception e) {
            EnvironmentInst.get().printError(logger, e);
            try {
                dbdst.rollback();
            } catch (Exception e1) {
            }
        }
        console.print(" OK\n");
    }



    public static void copytbl(Console console, Database dbsrc, Database dbdst, String tbl, String strColumns) {
        copytbl(console, dbsrc, dbdst, tbl, strColumns, "", "");
    }



    public static void copytbl(Console console, Database dbsrc, Database dbdst, String tbl, String strColumns, String checkIdField, String checkIdTable) {

        console.print( "copy " + tbl + ": " );

        try(Statement stsrc = dbsrc.getConnection().createStatement() ) {

            if( !dbsrc.hasTable(stsrc, "", tbl) ) {
                console.print( "source table not found!\n" );
                return;
            }

            boolean checkId = !checkIdField.isEmpty()  &&  !checkIdTable.isEmpty();

            Set<Integer> ids = new HashSet<>();;
            if( checkId ) {
                String sql = "select " + checkIdField + " from " + checkIdTable;
                try(ResultSet rs = stsrc.executeQuery(sql)) {
                    while (rs.next())
                        ids.add(rs.getInt(1));
                }
            }

            String sql = "select " + strColumns + " from " + tbl;
            try(ResultSet rs = stsrc.executeQuery(sql)) {
                ResultSetMetaData md = rs.getMetaData();
                int n = md.getColumnCount();

                if (strColumns.equals("*")) {
                    strColumns = "";
                    for (int i = 1; i <= n; ++i)
                        strColumns += (i > 1 ? ", " : "") + md.getColumnName(i);
                }

                sql = "insert into " + tbl + " (" + strColumns + ") values (";

                for (int i = 1; i <= n; ++i)
                    sql += i > 1 ? ", ?" : "?";
                sql += ')';

                boolean skip = false;
                int cnt1 = 0;
                int cnt2 = tbl.length() + 7;
                PreparedStatement dst = dbdst.getConnection().prepareStatement(sql);
                while (rs.next()) {
                    skip = false;
                    for (int i = 1; i <= n; ++i) {
                        if (checkId  &&  md.getColumnName(i).equals(checkIdField))
                            if (!ids.contains(rs.getInt(i))) {
                                skip = true;
                                break;
                            }

                        switch (md.getColumnType(i)) {
                            case Types.SMALLINT:
                            case Types.INTEGER:
                            case Types.BIGINT:
                                dst.setLong(i, rs.getLong(i));
                                break;
                            case Types.TIMESTAMP:
                                dst.setTimestamp(i, rs.getTimestamp(i));
                                break;
                            default:
                                dst.setString(i, rs.getString(i));
                                break;
                        }
                    }

                    if (skip)
                        continue;

                    dst.executeUpdate();
                    if (cnt1++ > 500) {
                        cnt1 = 0;
                        console.print("*");
                        dbdst.commit();
                        if (cnt2++ > 80) {
                            cnt2 = 0;
                            console.print("\n");
                        }
                    }
                }
            }

            dbdst.commit();
            dbsrc.commit();
            console.print(" OK\n");

        } catch (Exception e) {
            EnvironmentInst.get().printError(logger, e);
            try {
                dbsrc.rollback();
            } catch (Exception e1) {
            }
            try {
                dbdst.rollback();
            } catch (Exception e1) {
            }
        }
    }



    public static void sweep(Console console, Database db, String table, String dtfield, int year) {
        String dt = year + ".12.31 23.59.59.999";
        String sql = "delete from " + table + " where " + dtfield+ " <= '" + dt + "'";

        try (Statement st = db.getConnection().createStatement()) {
            console.print("delete from " + table + "...");
            int cnt = st.executeUpdate(sql);
            db.commit();
            console.print(" " + cnt + " records\n");
        } catch (Exception e) {
            EnvironmentInst.get().printError(logger, e);
            try {
                db.rollback();
            } catch (Exception e1) {
            }
        }
    }


}
