package promauto.jroboplc.core;

import promauto.jroboplc.core.api.*;

import java.util.*;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import static promauto.jroboplc.core.api.Flags.*;

public class CmdList extends AbstractCommand {

	protected static final int LIMIT = 1000;
	public static final String STATUS_GOOD = "Good";
	public static final String STATUS_BAD = "Bad";

	@Override
	public String getName() {
		return "l";
	}

	@Override
	public String getUsage() {
		return "[mod|filter]";
	}

	@Override
	public String getDescription() {
		return "list tags filtered with regex";
	}



	@Override
	public String execute(Console console, String args) {
		return list(console, args, 0);
	}


	protected String list(Console console, String filter, int pos) {
		Environment env = EnvironmentInst.get();

		console.setProperty("cmd_l_filter", filter);

		Module m = env.getModuleManager().getModule(filter);
		if( m != null )
			filter += ":.*";


		int cntbeg = Math.max(0, pos);


		Pattern p = null;
		try {
			if (!filter.isEmpty())
				p = Pattern.compile(filter);
		} catch (PatternSyntaxException e) {
		}





		int cnt = 0;

		Set<Module> mods = env.getModuleManager().getModules();
		Map<String, String[]> map = new TreeMap<>();

		int len1 = 1;
		int len2 = 1;
		int len3 = 1;
		int len4 = 1;

		for(Module mod: mods) {
			for (Tag tag : mod.getTagTable().values()) {
				String tagname = mod.getName() +':'+ tag.getName();
				if( p!=null )
					if( !p.matcher( tagname).matches() )
						continue;

				String value = tag.getString();
				if( tag.getType() == Tag.Type.STRING )
					value = ANSI.BOLD + ANSI.YELLOW + value + ANSI.RESET;

				String[] s = new String[]{
						tag.getType().name(),
						getFlagsText(tag),
						getStatusText(tag),
						value
				};

				map.put(tagname, s);
				cnt++;

				len1 = Math.max(len1, tagname.length());
				len2 = Math.max(len2, s[0].length());
				len3 = Math.max(len3, s[1].length());
				len4 = Math.max(len4, s[2].length());
			}
		}

		int cntend = cntbeg + LIMIT;
		int cntlast = 0;
		int i = -1;
		StringBuilder sb = new StringBuilder();
		for(String tagname: map.keySet()) {
			++i;
			if( i < cntbeg  ||  i >= cntend )
				continue;

			String[] s = map.get(tagname);

			String row = String.format("%-"+len1+"s  " +
					ANSI.GREEN +
					"%-" + len2 + "s " +
					"%-" + len3 + "s " +
					"%s" +
					"%-" + len4 + "s " +
					ANSI.RESET +
					" = %s\r\n",

					tagname,
					s[0],
					s[1],
					s[2].equals(STATUS_BAD)?
							ANSI.BOLD + ANSI.RED:
							ANSI.BOLD + ANSI.BLACK,
					s[2],
					s[3]);



			sb.append(row);

			cntlast = i;
		}


		console.setProperty("cmd_l_pos", "" + pos);
		console.setProperty("cmd_l_last", "" + ++cntlast);

		sb.append("\r\nListed from " + (++cntbeg) + " to " + cntlast + ", total is " + cnt + ".");

		if( cntlast < cnt )
			sb.append(" Use \"lf\" for next page.");

		sb.append("\r\n" + ANSI.BOLD + ANSI.BLACK);
		sb.append("Legend: S - autosave, E - external, H - hidden, Good/Bad - status");
		sb.append(ANSI.RESET);

		return sb.toString();
	}


	private String getFlagsText(Tag tag) {
		return (
				(tag.hasFlags(AUTOSAVE)? " S": "")
			  + (tag.hasFlags(HIDDEN)?   " H": "")
			  + (tag.hasFlags(EXTERNAL)? " E": "")).trim();
	}

	private String getStatusText(Tag tag) {
		if( tag.hasFlags(STATUS) ) {
			if (tag.getStatus() == Tag.Status.Good)
				return STATUS_GOOD;
			else if (tag.getStatus() == Tag.Status.Bad)
				return STATUS_BAD;
		}
		return "";
	}


}
