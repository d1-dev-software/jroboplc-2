package promauto.jroboplc.core;

import promauto.jroboplc.core.api.Tag;

import java.util.HashMap;

public class State extends HashMap<String,String> {
    private String varprefix = "";

    public void setVarPrefix(String prefix) {
        this.varprefix = prefix;
    }

    public void loadTag(Tag tag) {
        String value = get(tag.getName());
        if( value != null )
            tag.setString(value);
    }

    public void saveTag(Tag tag) {
        put(tag.getName(), tag.getString());
    }

    public boolean loadVar(String key, boolean defvalue) {
        String value = get(varprefix +  key);
        if( value != null )
            return value.equals("on");
        return defvalue;
    }

    public int loadVar(String key, int defvalue) {
        String value = get(varprefix +  key);
        try {
            if( value != null )
                return Integer.parseInt(value);
        } catch (NumberFormatException e) {
        }
        return defvalue;
    }

    public long loadVar(String key, long defvalue) {
        String value = get(varprefix +  key);
        try {
            if( value != null )
                return Long.parseLong(value);
        } catch (NumberFormatException e) {
        }
        return defvalue;
    }

    public double loadVar(String key, double defvalue) {
        String value = get(varprefix +  key);
        try {
            if( value != null )
                return Double.parseDouble(value);
        } catch (NumberFormatException e) {
        }
        return defvalue;
    }

    public String loadVar(String key, String defvalue) {
        String value = get(varprefix +  key);
        if( value != null )
            return value;

        return defvalue;
    }



    public void saveVar(String key, boolean value) {
        put(varprefix +  key, value? "on": "off");
    }

    public void saveVar(String key, int value) {
        put(varprefix +  key, ""+value);
    }

    public void saveVar(String key, long value) {
        put(varprefix +  key, ""+value);
    }

    public void saveVar(String key, double value) {
        put(varprefix +  key, ""+value);
    }

    public void saveVar(String key, String value) {
        put(varprefix +  key, value);
    }


    public boolean hasVar(String key) {
        return containsKey(varprefix +  key);
    }
}
