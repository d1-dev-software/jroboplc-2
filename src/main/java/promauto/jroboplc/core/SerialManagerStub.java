package promauto.jroboplc.core;

import promauto.jroboplc.core.api.SerialManager;
import promauto.jroboplc.core.api.SerialPort;

public class SerialManagerStub implements SerialManager {

	@Override
	public SerialPort getPort(int portid) {
		return null;
	}
}
