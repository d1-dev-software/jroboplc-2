package promauto.jroboplc.core;

import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.function.Consumer;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.api.*;

public abstract class AbstractModule implements Module {
	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	protected final Environment env;
	protected final Plugin plugin;
	protected final String name;
	
	
	protected TagTable tagtable;
	protected boolean enable;

	protected volatile boolean hasReloadRequest = false;
	protected Consumer<Boolean> reloader = null;
	protected boolean taskable = true;
	
	protected Map<String,String> flags = null;

	private boolean suspended;

	protected PostedCommands postedCommands = null;

//	protected long loadTimeMillis;

    protected Queue<Signal> signals = null; //new ConcurrentLinkedQueue<>();
    protected Set<Signal.Listener> signalListeners = null; // = new HashSet<>();


	protected AbstractModule(Plugin plugin, String name){
		this.env = EnvironmentInst.get();
		this.plugin = plugin;
		this.name = name;
		this.tagtable = new TagTable();
	}
	
	public boolean isEnabled() {
		return enable;
	}
	
	public boolean isTaskable() {
		return taskable;
	}

	public boolean canHaveExternalTags() {
		return false;
	}

	@Override
	public Plugin getPlugin() {
		return plugin;
	}

	@Override
	public TagTable getTagTable() {
		return tagtable;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getInfo() {
		return "";
	}



	public boolean load() {
		Object conf = env.getConfiguration().getModuleConf(	plugin.getPluginName(),	name);
		if( conf == null  ||  !load(conf) )
			return false;
		return true;
	}
	
	
	public final boolean load(Object conf) {
//		loadTimeMillis = System.currentTimeMillis();

		Configuration cm = env.getConfiguration();
		enable = cm.get(conf, "enable",	true);
		
		clearFlags();
		cm.toMap(conf).entrySet().stream()
				.filter( ent -> ent.getKey().startsWith("flag.") )
				.forEach( ent -> {
					if( flags == null )
						flags = new HashMap<>();
					flags.put(
							ent.getKey().substring(5).toUpperCase(),
							ent.getValue().toString().toUpperCase() );
				});

		boolean res = loadModule(conf);

		initTagValues(cm.toMap( cm.get(conf, "tag.values")));

		initTagFlags(cm.toMap( cm.get(conf, "tag.flags")));

		return res;
	}


	private void initTagValues(Map<String, Object> initmap) {
		initmap.entrySet().stream().forEach(ent -> {
			Tag tag = tagtable.get(ent.getKey());
			if( tag != null)
				initTagValue(tag, ent.getValue());
			else
				tagtable.getTags(ent.getKey(), false).stream()
						.forEach(tag1 -> initTagValue(tag1, ent.getValue()));
		});
	}

	private void initTagValue(Tag tag, Object value) {
		if( value instanceof  Boolean )
			tag.setBool((Boolean)value);
		else
			tag.setString(value.toString());
	}


	private void initTagFlags(Map<String, Object> initmap) {
		initmap.entrySet().stream().forEach(ent -> {
			Tag tag = tagtable.get(ent.getKey());
			if( tag != null)
				tag.setFlags( Flags.parseFlags(ent.getValue().toString()) );
			else
				tagtable.getTags(ent.getKey(), false).stream()
						.forEach(tag1 -> tag1.setFlags( Flags.parseFlags(ent.getValue().toString()) ));
		});
	}


	protected abstract boolean loadModule(Object conf);


	public void copySettingsFrom(AbstractModule src) {
//		this.loadTimeMillis = src.loadTimeMillis;
		this.enable = src.enable;
		
		clearFlags();
		this.flags = src.flags;
	}

	private void clearFlags() {
		if( flags != null ) {
			flags.clear();
			flags = null;
		}
	}
	
	@Override
	public String getFlag(String flag) {
		if(flags == null)
			return "";
		String value = flags.get(flag.toUpperCase());
		return (value == null)? "": value;
	}
	
	@Override
	public boolean isFlagCompatibleWith(String flag, Module module) {
		return getFlag(flag.toUpperCase()).equals( module.getFlag(flag.toUpperCase()) );
	}




	@Override
	public final boolean prepare() {
		if(!enable)
			return true;
		
		hasReloadRequest = false;

		clearSignals();

		return prepareModule();
	}

	protected abstract boolean prepareModule();


	
	@Override
	public final boolean execute() {
		if(!enable)
			return true;
		
		if( hasReloadRequest )
			executeReload();
		
		if( suspended )
			return true;

		if( postedCommands != null )
			postedCommands.execute();

		boolean res = executeModule();

        processSignals();
		
		return res;
	}

	protected boolean executeModule() {
		return true;
	}




	@Override
	public final boolean closedown() {
		if(!enable)
			return true;

		boolean res = closedownModule();
		processSignals();
		return res;
	}

	protected boolean closedownModule() {
		return true;
	}
	


	@Override
	public final void requestReload(Consumer<Boolean> reloader) {
		this.reloader  = reloader;
		
		if( taskable  ||  this instanceof Task ) {
			boolean running = enable  &&  EnvironmentInst.get().isRunning();
			if( running  &&  taskable ) {
				Task task = EnvironmentInst.get().getTaskManager().getTask(this);
				running =  task!=null  &&  task.isRunning(); 
			}
			
			if( running ) {
				hasReloadRequest = true;
				return;
			}
		}
		executeReload();
	}

	
	private void executeReload() {
		hasReloadRequest = false;
		boolean res = reload();
		if( res ) {
			env.printInfo(logger, name, "Successfully reloaded");
			postSignal(Signal.SignalType.RELOADED);
		} else
			env.printError(logger, name, "Failed to reload");
		
		if( reloader != null) {
			reloader.accept(res);
			reloader = null;
		}
	}

	
	protected boolean reload() {
		EnvironmentInst.get().printError(logger, name, "Reloading is not supported");
		return false;
	}
	


//	@Override
//	public void release() {
//		enable = false;
//		tagtable.clear();
//	}

	
	@Override
	public void loadState(State state) {
	}
	
	
	@Override
	public void saveState(State state) {
	}
	
	
	@Override
	public String check() {
		return "";
	}

	@Override
	public boolean isSuspended() {
		return suspended;
	}

	@Override
	public void setSuspended(boolean value) {
		suspended = value;
	}


	@Override
	public void postCommand(Command cmd, Console console, Module module, String args) {
		if( postedCommands == null )
			postedCommands = new PostedCommands();

		postedCommands.post(cmd, console, module, args);
	}



	//////////// SIGNALS ////////////
    @Override
    public void addSignalListener(Signal.Listener signalListener) {
        if( signalListeners == null )
            signalListeners = new HashSet<>();

        synchronized (signalListeners) {
			signalListeners.add(signalListener);
		}
    }

    @Override
    public void removeSignalListener(Signal.Listener signalListener) {
        if( signalListeners != null )
			synchronized (signalListeners) {
				signalListeners.remove(signalListener);
			}
    }

	@Override
	public void postSignal(Signal.SignalType id) {
		postSignal(id, null);
	}

	@Override
	public void postSignal(Signal.SignalType id, Object data) {
		if( signalListeners == null )
			return;

        if( signals == null )
            signals = new ConcurrentLinkedQueue<>();

        signals.add( new Signal(id, data) );
    }

    private void processSignals() {
        if( signals != null )
            while( !signals.isEmpty() ) {
                Signal signal = signals.poll();
                if( signalListeners != null )
					synchronized (signalListeners) {
						for (Signal.Listener listener : signalListeners) {
							listener.onSignal(this, signal);
						}
					}
        }
    }

	private void clearSignals() {
		if( signals != null )
			signals.clear();
	}




}
