package promauto.jroboplc.core;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.util.io.pem.PemObject;
import org.bouncycastle.util.io.pem.PemReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.KeyManager;
import promauto.jroboplc.plugin.tagsaver.TagsaverModule;

import javax.crypto.Cipher;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.*;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class KeyManagerImpl implements KeyManager {
    private final Logger logger = LoggerFactory.getLogger(KeyManagerImpl.class);

    public static final String PUBLIC_EXTENSION = ".pub";

    private Path keysDir;
    private Map<String, Key> keys = new HashMap<>();

    public KeyManagerImpl() {
        Security.addProvider(new BouncyCastleProvider());
        Configuration cm = EnvironmentInst.get().getConfiguration();
        this.keysDir = cm.getPath(cm.getRoot(), "keys.dir", "keys");
    }


    @Override
    public String encryptPublic(String keyName, String decryptedText) throws Exception {
        RSAPublicKey publicKey = readPublicKey(keyName);
        Cipher encryptCipher = Cipher.getInstance("RSA");
        encryptCipher.init(Cipher.ENCRYPT_MODE, publicKey);
        byte[] encryptedTextBytes = encryptCipher.doFinal(decryptedText.getBytes(StandardCharsets.UTF_8));
        return Base64.getEncoder().encodeToString(encryptedTextBytes);
    }

    @Override
    public String decryptPrivate(String keyName, String enryptedText) throws Exception {
        RSAPrivateKey privateKey = readPrivateKey(keyName);
        Cipher decryptCipher = Cipher.getInstance("RSA");
        decryptCipher.init(Cipher.DECRYPT_MODE, privateKey);
        byte[] encryptedTextBytes = Base64.getDecoder().decode(enryptedText);
        byte[] decryptedTextBytes = decryptCipher.doFinal(encryptedTextBytes);
        return new String(decryptedTextBytes, StandardCharsets.UTF_8);
    }

    @Override
    public String generateAndSaveKeyPair(String keyName) {
        try {
            KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA");
            generator.initialize(1024);
            KeyPair kp = generator.generateKeyPair();

            String privateKeyText =
                    "-----BEGIN PRIVATE KEY-----\r\n" +
                    Base64.getMimeEncoder().encodeToString( kp.getPrivate().getEncoded()) +
                    "\r\n-----END PRIVATE KEY-----";

            String publicKeyText =
                    "-----BEGIN PUBLIC KEY-----\r\n" +
                    Base64.getMimeEncoder().encodeToString( kp.getPublic().getEncoded()) +
                    "\r\n-----END PUBLIC KEY-----";

            if( keyName.isEmpty() ) {
                do {
                    keyName = "key" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS"));
                } while (Files.exists(keysDir.resolve(keyName))  ||  Files.exists(keysDir.resolve(keyName + PUBLIC_EXTENSION)));
            }

            Files.createDirectories(keysDir);
            Files.write(keysDir.resolve(keyName), privateKeyText.getBytes(StandardCharsets.UTF_8));
            Files.write(keysDir.resolve(keyName + PUBLIC_EXTENSION), publicKeyText.getBytes(StandardCharsets.UTF_8));

        } catch (Exception e) {
            EnvironmentInst.get().printError(logger, e, "Failed to generate keys: " + keyName);
        }
        return keyName;
    }

    @Override
    public String getDefaultPrivateKeyName() {
        try {
            List<Path> found = Files.find(keysDir, 1,
                            (path, attr) -> !path.toString().endsWith(PUBLIC_EXTENSION)  &&  attr.isRegularFile())
                    .collect(Collectors.toList());
            if( found.size() == 1)
                return found.get(0).getFileName().toString();
        } catch (IOException e) {
        }

        return "";
    }


    private RSAPublicKey readPublicKey(String keyName) throws Exception {
        keyName += PUBLIC_EXTENSION;
        Key key = keys.get(keyName);
        if( key != null )
            return (RSAPublicKey)key;

        Path path = keysDir.resolve(keyName);
        KeyFactory factory = KeyFactory.getInstance("RSA");
        try (FileReader keyReader = new FileReader(path.toFile()); PemReader pemReader = new PemReader(keyReader)) {
            PemObject pemObject = pemReader.readPemObject();
            byte[] content = pemObject.getContent();
            X509EncodedKeySpec pubKeySpec = new X509EncodedKeySpec(content);
            RSAPublicKey rsaPublicKey = (RSAPublicKey) factory.generatePublic(pubKeySpec);
            keys.put(keyName, rsaPublicKey);
            return rsaPublicKey;
        }
    }

    private RSAPrivateKey readPrivateKey(String keyName) throws Exception {
        Key key = keys.get(keyName);
        if( key != null )
            return (RSAPrivateKey)key;

        Path path = keysDir.resolve(keyName);
        KeyFactory factory = KeyFactory.getInstance("RSA");
        try (FileReader keyReader = new FileReader(path.toFile()); PemReader pemReader = new PemReader(keyReader)) {
            PemObject pemObject = pemReader.readPemObject();
            byte[] content = pemObject.getContent();
            PKCS8EncodedKeySpec privKeySpec = new PKCS8EncodedKeySpec(content);
            RSAPrivateKey rsaPrivateKey = (RSAPrivateKey) factory.generatePrivate(privKeySpec);
            keys.put(keyName, rsaPrivateKey);
            return rsaPrivateKey;
        }
    }

}
