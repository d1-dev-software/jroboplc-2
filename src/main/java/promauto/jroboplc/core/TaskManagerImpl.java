package promauto.jroboplc.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.api.*;

public class TaskManagerImpl implements TaskManager {
	private final Logger logger = LoggerFactory.getLogger(TaskManagerImpl.class);
	private final Environment env;
	
	
	public TaskManagerImpl(Environment env) {
		this.env = env;
	}

	
	
	
	@Override
	public String start() {
		if (env.isRunning())
			return "Already started";
		
		env.logStatus(logger, "*** START ***");

		boolean status = true;
		for (Module m: env.getModuleManager().getModules())
			try {
				if (!(status &= m.prepare()))
					break;
			} catch (Exception e) {
				env.printError(logger, e, "Unhandled exception while preparing module:", m.getName());
				status = false;
				break;
			}

		env.setRunning(status);

		if (status) {
			// Starting task modules
			for (Module m: env.getModuleManager().getModules())
				if( m instanceof Task )
					m.execute();

            env.logStatus(logger, "*** RUN ***");
		} else {
			env.printError(logger, "Start failed, rolling back...");
			stop();
		}
//			for (Module m: env.getModuleManager().getModules())
//				status &= m.closedown();
	
		return (status)? "OK": "FAILED";
	}

	
	
	@Override
	public String stop() {
        env.logStatus(logger, "*** STOP ***");
	
		env.setRunning(false);

		// close down task modules only
		for(Module m: env.getModuleManager().getModules())
			if( m instanceof Task )
				m.closedown();
		

		// close down all the rest
		for(Module m: env.getModuleManager().getModules())
			if( !(m instanceof Task) )
				m.closedown();

        env.logStatus(logger, "*** IDLE ***");
		return "OK";
	}

	
	

	@Override
	public Task getTask(Module module) {
		return getTask(module.getName());
	}




	@Override
	public Task getTask(String modname) {
		for(Module m: env.getModuleManager().getModules())
			if( m instanceof Task ) {
				Task task = (Task)m;
				if( task.hasModule(modname) )
					return task;
			}
		return null;
	}



}
