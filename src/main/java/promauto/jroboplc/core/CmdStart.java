package promauto.jroboplc.core;

import promauto.jroboplc.core.api.Console;
import promauto.jroboplc.core.api.EnvironmentInst;

public class CmdStart extends AbstractCommand {

	@Override
	public String getName() {
		return "start";
	}
	
	@Override
	public String getUsage() {
		return "";
	}


	@Override
	public String getDescription() {
		return "start tasks";
	}

	
	@Override
	public String execute(Console console, String args) {
		return EnvironmentInst.get().getTaskManager().start(); 
	}

	


}
