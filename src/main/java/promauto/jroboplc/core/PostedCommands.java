package promauto.jroboplc.core;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import promauto.jroboplc.core.api.Command;
import promauto.jroboplc.core.api.Console;
import promauto.jroboplc.core.api.Module;

public class PostedCommands {
	private static class Rec {
		private Command cmd;
		private Console console;
		private Module module;
		private String args;
		
		public Rec(Command cmd, Console console, Module module, String args) {
			super();
			this.cmd = cmd;
			this.console = console;
			this.module = module;
			this.args = args;
		}
		
		
	}
	
	Queue<PostedCommands.Rec> recs = new ConcurrentLinkedQueue<>();
	
	public void post(Command cmd, Console console, Module module, String args) {
		recs.offer( new Rec(cmd, console, module, args) );
	}
	
	public void execute() {
		while( !recs.isEmpty() ) {
			Rec rec = recs.poll();
			if( rec != null )
				rec.cmd.executePosted(rec.console, rec.module, rec.args);
		}
	}

}
