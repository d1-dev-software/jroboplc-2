package promauto.jroboplc.core;

import promauto.jroboplc.core.api.Console;

public class CmdRepeat extends CmdList {

	@Override
	public String getName() {
		return "rpt";
	}

	@Override
	public String getUsage() {
		return "[period]";
	}

	@Override
	public String getDescription() {
		return "execute last command repeatedly with time period (ms)";
	}


	@Override
	public String execute(Console console, String args) {
		long period = 1000;
		if( !args.isEmpty() ) {
			try {
				period = Long.parseLong(args);
			} catch (NumberFormatException e) {
				return "Period must be an integer value";
			}
		}
		return console.startRepeatingLastRequest(period);
	}

}
