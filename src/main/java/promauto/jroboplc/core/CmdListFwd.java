package promauto.jroboplc.core;

import promauto.jroboplc.core.api.Console;

public class CmdListFwd extends CmdList {


	@Override
	public String getName() {
		return "lf";
	}

	@Override
	public String getUsage() {
		return "";
	}

	@Override
	public String getDescription() {
		return "list the next page";
	}



	@Override
	public String execute(Console console, String args) {
		return list(console,
				console.getProperty("cmd_l_filter", ""),
				console.getProperty("cmd_l_last", 0) );
	}



}
