package promauto.jroboplc.core;

import promauto.jroboplc.core.api.Console;

public class CmdVersion extends AbstractCommand {

	@Override
	public String getName() {
		return "version";
	}

	@Override
	public String getUsage() {
		return "";
	}

	@Override
	public String getDescription() {
		return "shows version of the core";
	}

	@Override
	public String execute(Console console, String args) {
		return Version.getVersionStr();
	}


}
