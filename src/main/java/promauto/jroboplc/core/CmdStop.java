package promauto.jroboplc.core;

import promauto.jroboplc.core.api.Console;
import promauto.jroboplc.core.api.EnvironmentInst;

public class CmdStop extends AbstractCommand {

	@Override
	public String getName() {
		return "stop";
	}
	
	@Override
	public String getUsage() {
		return "";
	}


	@Override
	public String getDescription() {
		return "stop tasks";
	}


	@Override
	public String execute(Console console, String args) {
		return EnvironmentInst.get().getTaskManager().stop();
	}
	
	


}
