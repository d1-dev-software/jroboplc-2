package promauto.jroboplc.core;

import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import promauto.jroboplc.core.api.Environment;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.Module;
import promauto.jroboplc.core.api.Plugin;

public abstract class AbstractPlugin implements Plugin {

	protected Environment env;
	protected final List<Module> modules = new LinkedList<>();

	@Override
	public void initialize() {
		this.env = EnvironmentInst.get();
	}


	@Override
	public List<Module> getModules() {
		return modules;
	}
	

	@Override
	public Module createModule(String name, Object conf) {
		return null;
	}




}
