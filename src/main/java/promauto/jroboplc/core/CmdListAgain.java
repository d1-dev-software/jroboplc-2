package promauto.jroboplc.core;

import promauto.jroboplc.core.api.*;

public class CmdListAgain extends CmdList {


	@Override
	public String getName() {
		return "ll";
	}

	@Override
	public String getUsage() {
		return "";
	}

	@Override
	public String getDescription() {
		return "list the last page again";
	}



	@Override
	public String execute(Console console, String args) {
		return list(console,
				console.getProperty("cmd_l_filter", ""),
				console.getProperty("cmd_l_pos", 0) );
	}



}
