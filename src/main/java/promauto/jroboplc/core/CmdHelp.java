package promauto.jroboplc.core;

import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.text.WordUtils;

import promauto.jroboplc.core.api.CmdDispatcher;
import promauto.jroboplc.core.api.ANSI;
import promauto.jroboplc.core.api.Command;
import promauto.jroboplc.core.api.Console;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.Plugin;

public class CmdHelp extends AbstractCommand {
	private static final String LINE_SEP = "\r\n";
	private final int WIDTH_DESCR = 180;
	private int cmdWidth;

	@Override
	public String getName() {
		return "h";
	}
	
	@Override
	public String getUsage() {
		return "";
	}


	@Override
	public String getDescription() {
		return "shows this help";
	}

	
	private static class PluginCmdInfo {
		public Plugin plugin;
		public Map<String,Command> commands = new TreeMap<>();
	}
	
	@Override
	public String execute(Console console, String args) {
		StringBuilder sb = new StringBuilder();
		
		Map<String,CmdDispatcher.Entry> commands = EnvironmentInst.get().getCmdDispatcher().getCommands();
		Map<String,Command> commons = new TreeMap<>();
		Map<String,PluginCmdInfo> plugins = new TreeMap<>();
		
		cmdWidth = 10;
		// prepare data
		for (CmdDispatcher.Entry entry: commands.values()) {
			String cmdname = entry.command.getName();
			if (entry.plugin==null)
				commons.put(cmdname, entry.command);
			else {
				String plugname = entry.plugin.getPluginName();
				PluginCmdInfo pluginfo = plugins.get(plugname);
				if (pluginfo==null) {
					pluginfo = new PluginCmdInfo();
					pluginfo.plugin = entry.plugin;
					plugins.put(plugname, pluginfo);
				}
				
				pluginfo.commands.put( (entry.module==null? "p:": "m:") + cmdname, entry.command);
			}
			
			int w = entry.command.getName().length() + entry.command.getUsage().length();
			if( w > cmdWidth )
				cmdWidth = w;
		}
		cmdWidth += 4;	
		
		// write main intro
		sb.append("General format:\r\n");
		sb.append("  <comand>     " + "common command\r\n");
		sb.append("  p:<command>  " + "plugin command, where 'p' is a plugin name\r\n");
		sb.append("  m:<command>  " + "module command, where 'm' is a module name\r\n\r\n"); 
		
		// write common commands if there any
		if (!commons.isEmpty()) {
			sb.append(ANSI.BOLD);
			sb.append("Common commands:\r\n");
			sb.append(ANSI.RESET);
			addCmdLines(sb, commons);
		}

		// write plugin description
		for (PluginCmdInfo pluginfo: plugins.values()) {
			sb.append("\r\n");
			sb.append(ANSI.BOLD);
			sb.append( pluginfo.plugin.getPluginName() );
			sb.append(ANSI.RESET);
			sb.append(":");
//			sb.append( pluginfo.plugin.getPluginDescription() );
			sb.append("\r\n");

			addCmdLines(sb, pluginfo.commands);
		}

		return sb.toString();
	}

	
	
	private void addCmdLines(StringBuilder sb, Map<String,Command> commands) {
		for (Map.Entry<String,Command> cmd: commands.entrySet()) {
			// command name and usage
			sb.append("  "); 
			sb.append(ANSI.BOLD);
			sb.append(ANSI.GREEN);
			sb.append(cmd.getKey()); 
			sb.append(ANSI.RESET);
			sb.append(ANSI.GREEN);
			sb.append(String.format(" %-" + (cmdWidth-cmd.getKey().length()) + "s", cmd.getValue().getUsage()));
			sb.append(ANSI.RESET);
			
			
			// first line of a description

			String[] s = WordUtils.wrap(cmd.getValue().getDescription(), WIDTH_DESCR, LINE_SEP, true).split(LINE_SEP);
			
			// the rest of the description
			sb.append(s[0] + LINE_SEP);
			for(int i=1; i<s.length; i++) {
				sb.append( String.format("%" + (cmdWidth + 3) + "s%s\r\n", " ", s[i]) );
			}
		}
	}

	

}

