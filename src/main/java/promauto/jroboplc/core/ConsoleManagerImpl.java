package promauto.jroboplc.core;

import java.util.HashSet;
import java.util.Set;

import promauto.jroboplc.core.api.Console;
import promauto.jroboplc.core.api.ConsoleManager;

public class ConsoleManagerImpl implements ConsoleManager {
	
	Set<Console> consoles = new HashSet<>();

	@Override
	public synchronized boolean addConsole(Console console) {
		return consoles.add(console);
	}

	@Override
	public synchronized boolean removeConsole(Console console) {
		return consoles.remove(console);
	}

	@Override
	public synchronized Set<Console> getConsoles() {
		return consoles;
	}

	@Override
	public synchronized void printToAll(String text) {
		consoles.forEach( console -> console.print(text) );
	}

}
