package promauto.jroboplc.core;

import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.EnvironmentInst;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;

public class PatternList {
    private List<PatternEx> list = new LinkedList<>();


    public void load(Object conf, String key) {
        Configuration cm = EnvironmentInst.get().getConfiguration();

        list.clear();
        cm.toList( cm.get(conf, key)).stream().map(o -> o.toString().trim() ).forEach(regex -> {
            PatternEx pe = new PatternEx();
            if( regex.startsWith("~") ) {
                pe.exclude = true;
                regex = regex.substring(1).trim();
            }
            pe.pattern = Pattern.compile(regex);
            list.add(pe);
        });
    }


    public boolean match(String s) {
        boolean res = false;
        for (PatternEx pe : list)
            if (pe.pattern.matcher(s).find())
                res = !pe.exclude;
        return res;
    }

}
