package promauto.jroboplc.core;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import promauto.jroboplc.core.api.Console;
import promauto.jroboplc.core.api.Environment;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.Module;

public class CmdSusp extends AbstractCommand {
//	private final Logger logger = LoggerFactory.getLogger(CmdSusp.class);

	@Override
	public String getName() {
		return "susp";
	}

	@Override
	public String getUsage() {
		return "[off|on] mod";
	}

	@Override
	public String getDescription() {
		return "switches suspend mode for a module";
	}

	@Override
	public String execute(Console console, String args) {
		Environment env = EnvironmentInst.get();
		
		if( args.trim().isEmpty() )
			return getAllSuspendedModules();
		
		Matcher m = Pattern.compile("^(on|off) +(\\w+)$").matcher(args.trim());
		if( !m.find() )
			return getRefuseAnswer();

		String mode = m.group(1);
		String modname = m.group(2);
		boolean suspend = m.group(1).equals("on");
		Module module = env.getModuleManager().getModule(modname);
		if( module != null ) {
			module.setSuspended(suspend);
			return "Suspend mode for " + modname + " is " + mode; 
		}
		
		return getAnswerModuleNotFound(modname);
	}

	private String getAllSuspendedModules() {
		Environment env = EnvironmentInst.get();
		String result = "";
		for(Module module: env.getModuleManager().getModules())
			if( module.isSuspended() )
				result += module.getName() + "\r\n";

		return result;
	}


}
