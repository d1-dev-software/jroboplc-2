package promauto.jroboplc.core;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.api.ANSI;
import promauto.jroboplc.core.api.Console;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.Module;
import promauto.jroboplc.core.api.Task;
import promauto.jroboplc.core.api.TaskManager;

public class CmdModinfo extends AbstractCommand {
	final Logger logger = LoggerFactory.getLogger(CmdModinfo.class);


	@Override
	public String getName() {
		return "i";
	}

	@Override
	public String getUsage() {
		return "[mod]";
	}

	@Override
	public String getDescription() {
		return "module info";
	}

	@Override
	public String execute(Console console, String args) {
		
		Set<Module> mods = EnvironmentInst.get().getModuleManager().getModules();

		Pattern pattern = null;
		try {
			if (!args.isEmpty())
				pattern = Pattern.compile(args);
		} catch (PatternSyntaxException e) {
			EnvironmentInst.get().printError(logger, e, "Pattern compilation error:", args);
		}

		Map<String,Module> map = new TreeMap<>();
		int moduleWidth = 6;
		int pluginWidth = 6;
		int taskWidth = 6;
		for(Module m: mods) {
			int w = m.getName().length();
			if( moduleWidth < w )
				moduleWidth = w;
			
			if( m instanceof Task )
				if( taskWidth < w )
					taskWidth = w;
			
			w = m.getPlugin().getPluginName().length();
			if( pluginWidth < w )
				pluginWidth = w;
			
			map.put(m.getPlugin().getPluginName() + m.getName(), m);
		}
		pluginWidth += 2;
		moduleWidth += 2;
		taskWidth += 2;
		int totalWidth = moduleWidth + pluginWidth + taskWidth;
		
		StringBuilder sb = new StringBuilder();
		String fmtplg = "%-"+pluginWidth+"s";
		String fmtmod = "%-"+moduleWidth+"s";
		String fmttsk = "%-"+taskWidth+"s";
		String fmt2 = "%-"+totalWidth+"s%s\r\n";

		// header
		sb.append(ANSI.BOLD);
		sb.append( String.format(fmtmod, "MODULE") );
		sb.append( String.format(fmtplg, "PLUGIN") );
		sb.append( String.format(fmttsk, "TASK") );
		sb.append( "INFORMATION" );
		sb.append("\r\n");
		sb.append(ANSI.RESET);

		// representing info as a table
		TaskManager tm = EnvironmentInst.get().getTaskManager();
		for(Module m: map.values()) {
			
			if (pattern!=null)
				if (!pattern.matcher(m.getName()).matches())
					continue;
			
			String[] info = m.getInfo().split("\r\n");

			sb.append(ANSI.YELLOW);
			sb.append( String.format(fmtmod, m.getName()) );
			sb.append(ANSI.RESET);
			
			sb.append(ANSI.GREEN);
			sb.append( String.format(fmtplg, m.getPlugin().getPluginName()) );
			sb.append(ANSI.RESET);
			
			sb.append(ANSI.YELLOW);
			String taskname = "";
			Task task = tm.getTask(m);
			if( task != null ) {
				taskname = task.getName();
				if( task.isRunning() )
					sb.append(ANSI.BOLD);
			} else
				if( m.isTaskable() )
					taskname = "---";
			sb.append( String.format(fmttsk, taskname ) );
			sb.append(ANSI.RESET);
			
			
			sb.append(info[0]);
			sb.append("\r\n");
			
			for(int i=1; i<info.length; i++)
				sb.append( String.format(fmt2, " ", info[i]) );
		}
		
		if( args.isEmpty() ) {
			sb.append(ANSI.BOLD);
			sb.append( String.format(fmtmod, "\r\nTOTALS:") );
			sb.append("\r\n");
			sb.append(ANSI.RESET);
			
			Map<String,String> totals = new LinkedHashMap<>();
			totals.put("modules", ""+mods.size() );
			int totaltags = 0;
			for(Module mod: mods)
				totaltags += mod.getTagTable().getSize();
			totals.put("tags", ""+totaltags );
			
			String fmt = "%-12s";
			for(Map.Entry<String,String> ent: totals.entrySet()) {
				sb.append(ANSI.GREEN);
				sb.append( String.format(fmt, ent.getKey()) );
				sb.append(ANSI.RESET);
				sb.append( ent.getValue() );
				sb.append("\r\n");
			}
		}
		
		
		return sb.toString();
	}
	
	
}
