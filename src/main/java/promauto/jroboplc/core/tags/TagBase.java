package promauto.jroboplc.core.tags;

import promauto.jroboplc.core.api.Tag;


public abstract class TagBase implements Tag {

	private final String name;
	private volatile Status status = Status.Good;
	private Object object = null;
	private int flags;


	public static Tag create(Type type, String tagname, int flags) {
		switch (type) {
			case BOOL:   return new TagBool(tagname, false, flags);
			case INT:    return new TagInt(tagname, 0, flags);
			case LONG:   return new TagLong(tagname, 0L, flags);
			case DOUBLE: return new TagDouble(tagname, 0.0, flags);
			case STRING: return new TagString(tagname, "", flags);
			default:
				throw new IllegalArgumentException("Not supported tag type: " + type.name() );
		}
	}


	public TagBase(String name) {
		this.name = name;
	}


	public TagBase(String name, int flags) {
		this.name = name;
		this.flags = flags;
	}


	@Override
	public Status getStatus() {
		return status;
	}
	
	@Override
	public void setStatus(Status status) {
		if( this.status != status )
			this.status = status;
	}


	
	@Override
	public String getName() {
		return name;
	}


	@Override
	public String toString() {
		return getString();
	}


	@Override
	@SuppressWarnings("unchecked")
	public <T> T getObject() {
		return (T)object;
	}

	@Override
	public void setObject(Object object) {
		this.object = object;
	}




	@Override
	public int getFlags() {
		return flags;
	}

	@Override
	public void setFlags(int flags) {
		this.flags = flags;
	}

	@Override
	public void addFlag(int flag) {
		flags |= flag;
	}

	@Override
	public boolean hasFlags(int flags) {
		return (this.flags & flags) == flags;
	}

	@Override
	public void copyFlagsFrom(Tag tag) {
		flags = tag.getFlags();
	}


}
