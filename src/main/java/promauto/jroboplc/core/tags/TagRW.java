package promauto.jroboplc.core.tags;

public abstract class TagRW extends TagBase {
	protected volatile boolean valueWrChanged = false;
	

	public static TagRW create(Type type, String tagname, int flags) {
		switch (type) {
			case BOOL:   return new TagRWBool(tagname, false, flags);
			case INT:    return new TagRWInt(tagname, 0, flags);
			case LONG:   return new TagRWLong(tagname, 0L, flags);
			case DOUBLE: return new TagRWDouble(tagname, 0.0, flags);
			case STRING: return new TagRWString(tagname, "", flags);
			default:
				throw new IllegalArgumentException("Not supported tag type: " + type.name() );
		}
	}


	public TagRW(String name, int flags) {
		super(name, flags);
	}


	public void raiseWriteValue() {
		valueWrChanged = true;
	}

	public boolean hasWriteValue() {
		if( valueWrChanged )
			synchronized(this) {
				if( !valueWrChanged )
					return false;

				copyWriteToLastWrite();
				return true;
			}
		else
			return false;
	}
	
	
	public boolean acceptWriteValue() {
		if( valueWrChanged )
			synchronized(this) {
				if( !valueWrChanged )
					return false;

				copyWriteToLastWrite();
				copyWriteToRead();
				return true;
			}
		else
			return false;
	}

	abstract public void copyLastWriteToRead();
	abstract protected void copyWriteToLastWrite();
	abstract protected void copyWriteToRead();

	
	abstract public boolean getWriteValBool();
	abstract public int getWriteValInt();
	abstract public long getWriteValLong();
	abstract public double getWriteValDouble();
	abstract public String getWriteValString();

	
	abstract public void setReadValBool(boolean value);
	abstract public void setReadValInt(int value);
	abstract public void setReadValLong(long value);
	abstract public void setReadValDouble(double value);
	abstract public void setReadValString(String value) throws NumberFormatException ;
	

	


}
