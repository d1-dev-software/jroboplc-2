package promauto.jroboplc.core.tags;

import promauto.jroboplc.core.api.Tag;

// Thread UNSAFE. Cannot be used in tagtables.
public abstract class TagPlain extends TagBase {

	public static TagPlain create(Type type, String tagname) {
		switch (type) {
			case BOOL:   return new TagPlainBool(tagname, false);
			case INT:    return new TagPlainInt(tagname, 0);
			case LONG:   return new TagPlainLong(tagname, 0L);
			case DOUBLE: return new TagPlainDouble(tagname, 0.0);
			case STRING: return new TagPlainString(tagname, "");
			default:
				throw new IllegalArgumentException("Not supported tag type: " + type.name() );
		}
	}


	public static Tag create(Tag source) {
		Tag tag = create(source.getType(), source.getName());
		source.copyValueTo(tag);
		return tag;
	}

	public TagPlain(String name) {
		super(name);
	}

	@Override
	public String getName() {
		return super.getName() == null? "": super.getName();
	}


}
