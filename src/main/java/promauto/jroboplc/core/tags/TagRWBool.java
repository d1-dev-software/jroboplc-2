package promauto.jroboplc.core.tags;

import promauto.jroboplc.core.api.Tag;

public class TagRWBool extends TagRW {

	// can be accessed from multiple threads
	private volatile boolean valueRd;

	// must be accessed from one thread by tag owner module only!
	private boolean valueWr;
	private boolean valueWrLast = false;
	
	

	public TagRWBool(String name, boolean value, int flags) {
		super(name, flags);
		this.valueRd = value;
	}

	@Override
	public Type getType() {
		return Type.BOOL;
	}



///// general methods for multithreading operations /////

	@Override
	public boolean getBool() {
		return valueRd;
	}

	@Override
	public int getInt() {
		return (valueRd)? 1: 0;
	}

	@Override
	public long getLong() {
		return (valueRd)? 1L: 0L;
	}

	@Override
	public double getDouble() {
		return (valueRd)? 1.0: 0.0;
	}

	@Override
	public String getString() {
		return (valueRd)? "on": "off";
	}
	


	
	
	@Override
	public synchronized void setBool(boolean value) {
		valueWr = value;
		valueWrChanged = true;
	}

	@Override
	public synchronized void setInt(int value) {
		valueWr = value!=0;
		valueWrChanged = true;
	}

	@Override
	public synchronized void setLong(long value) {
		valueWr = value!=0L;
		valueWrChanged = true;
	}

	@Override
	public synchronized void setDouble(double value) {
		valueWr = value!=0.0;
		valueWrChanged = true;
	}

	@Override
	public synchronized void setString(String value) {
		valueWr = value.equals("on");
		valueWrChanged = true;
	}





///// Methods below are suppossed to be used by tag owner module only!
///// Must be invoked from single thread!

	@Override
	public boolean getWriteValBool() {
		return valueWrLast;
	}

	@Override
	public int getWriteValInt() {
		return (valueWrLast)? 1: 0;
	}

	@Override
	public long getWriteValLong() {
		return (valueWrLast)? 1L: 0L;
	}

	@Override
	public double getWriteValDouble() {
		return (valueWrLast)? 1.0: 0.0;
	}
	
	@Override
	public String getWriteValString() {
		return (valueWrLast)? "on": "off";
	}
	
	
	
	
	@Override
	public void setReadValBool(boolean value) {
		valueRd = value;
	}

	@Override
	public void setReadValInt(int value) {
		valueRd = value!=0;
	}

	@Override
	public void setReadValLong(long value) {
		valueRd = value!=0L;
	}

	@Override
	public void setReadValDouble(double value) {
		valueRd = value!=0.0;
	}

	@Override
	public void setReadValString(String value) throws NumberFormatException {
		valueRd = value.equals("on");
	}



	
	
	@Override
	public boolean equalsValue(Tag tag) {
		return tag.getBool() == valueRd;
	}

	@Override
	public void copyValueTo(Tag tag) {
		tag.setBool(valueRd);
	}



	
	@Override
	protected void copyWriteToLastWrite() {
		valueWrChanged = false;
		valueWrLast = valueWr;
	}
	
	@Override
	protected void copyWriteToRead() {
		valueRd = valueWr;
	}

	@Override
	public void copyLastWriteToRead() {
		valueRd = valueWrLast;
	}

	

}
