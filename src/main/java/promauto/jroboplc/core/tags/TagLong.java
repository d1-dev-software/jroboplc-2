package promauto.jroboplc.core.tags;

import promauto.jroboplc.core.api.Tag;

public class TagLong extends TagBase {

	private volatile long value;


	public TagLong(String name, long value) {
		super(name);
		setLong(value);
	}

	public TagLong(String name, long value, int flags) {
		super(name, flags);
		setLong(value);
	}


	@Override
	public Type getType() {
		return Type.LONG;
	}
	
	


	@Override
	public boolean getBool() {
		return value != 0L;
	}

	@Override
	public int getInt() {
		return (int)value;
	}

	@Override
	public long getLong() {
		return value;
	}

	@Override
	public double getDouble() {
		return (double)value;
	}

	@Override
	public String getString() {
		return ""+value;
	}
	


	
	
	@Override
	public void setBool(boolean value) {
		if( this.value != (value? 1L: 0L) )
			this.value = value? 1L: 0L;
	}

	@Override
	public void setInt(int value) {
		if( this.value != value )
			this.value = value;
	}

	@Override
	public void setLong(long value) {
		if( this.value != value )
			this.value = value;
	}

	@Override
	public void setDouble(double value) {
		if( this.value != (long)value )
			this.value = (long)value;
	}

	@Override
	public void setString(String value) {
		long parsed;
		try {
			parsed = Long.parseLong(value.trim());
		} catch (NumberFormatException e) {
			parsed = 0L;
		}

		if( this.value != parsed )
			this.value = parsed;
	}

	
	@Override
	public boolean equalsValue(Tag tag) {
		return tag.getLong() == value;
	}

	@Override
	public void copyValueTo(Tag tag) {
		tag.setLong(value);
	}

	


}
