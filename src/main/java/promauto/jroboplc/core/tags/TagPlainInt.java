package promauto.jroboplc.core.tags;

import promauto.jroboplc.core.api.Tag;

public class TagPlainInt extends TagPlain {
	
	int value;


	public TagPlainInt(String name, int value) {
		super(name);
		this.value = value;
	}

	@Override
	public Type getType() {
		return Type.INT;
	}

	@Override
	public boolean equalsValue(Tag tag) {
		return tag.getInt() == value;
	}

	@Override
	public void copyValueTo(Tag tag) {
		tag.setInt(value);
	}

	
	
	
	
	@Override
	public boolean getBool() {
		return value!=0;
	}

	@Override
	public int getInt() {
		return value;
	}

	@Override
	public long getLong() {
		return (long)value;
	}

	@Override
	public double getDouble() {
		return (double)value;
	}

	@Override
	public String getString() {
		return ""+value;
	}
	


	
	
	@Override
	public void setBool(boolean value) {
		this.value = value? 1: 0;
	}

	@Override
	public void setInt(int value) {
		this.value = value;
	}

	@Override
	public void setLong(long value) {
		this.value = (int)value;
	}

	@Override
	public void setDouble(double value) {
		this.value = (int)value;
	}

	@Override
	public void setString(String value) {
		try {
			this.value = Integer.parseInt(value.trim());
		} catch (NumberFormatException e) {
			this.value = 0;
		}
	}

}
