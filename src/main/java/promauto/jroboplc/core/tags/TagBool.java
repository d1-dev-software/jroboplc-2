package promauto.jroboplc.core.tags;

import promauto.jroboplc.core.api.Tag;

public class TagBool extends TagBase {
	
	private volatile boolean value = false;  
	
	
	public TagBool(String name, boolean value) {
		super(name);
		setBool(value);
	}

	public TagBool(String name, boolean value, int flags) {
		super(name, flags);
		setBool(value);
	}


	@Override
	public Type getType() {
		return Type.BOOL;
	}
	
	


	@Override
	public boolean getBool() {
		return value;
	}

	@Override
	public int getInt() {
		return (value)? 1: 0;
	}

	@Override
	public long getLong() {
		return (value)? 1L: 0L;
	}

	@Override
	public double getDouble() {
		return (value)? 1.0: 0.0;
	}

	@Override
	public String getString() {
		return (value)? "on": "off";
	}



	
	@Override
	public void setBool(boolean value) {
		if( this.value != value )
			this.value = value;
	}

	@Override
	public void setInt(int value) {
		if( this.value != (value!=0) )
			this.value = value!=0;
	}

	@Override
	public void setLong(long value) {
		if( this.value != (value!=0) )
			this.value = value!=0;
	}

	@Override
	public void setDouble(double value) {
		if( this.value != (value!=0.0) )
			this.value = value!=0.0;
	}
	
	@Override
	public void setString(String value) {
		boolean parsed = value.equals("on");
		if( this.value != parsed )
			this.value = parsed;
	}

	

	
	
	
	@Override
	public boolean equalsValue(Tag tag) {
		return tag.getBool() == value;
	}

	
	@Override
	public void copyValueTo(Tag tag) {
		tag.setBool(value);
	}

	

}
