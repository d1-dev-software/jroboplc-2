package promauto.jroboplc.core.tags;

import promauto.jroboplc.core.api.Tag;

public class TagPlainString extends TagPlain {

	String value;
	

	public TagPlainString(String name, String value) {
		super(name);
		setString(value);
	}

	@Override
	public Type getType() {
		return Type.STRING;
	}

	@Override
	public boolean equalsValue(Tag tag) {
		return tag.getString().equals( value );
	}

	@Override
	public void copyValueTo(Tag tag) {
		tag.setString(value);
	}

	
	
	
	@Override
	public boolean getBool() {
		return value.equals("on");
	}

	@Override
	public int getInt() {
		try {
			return Integer.parseInt(value);
		} catch (NumberFormatException e) {
			return 0;
		}
	}

	@Override
	public long getLong() {
		try {
			return Long.parseLong(value);
		} catch (NumberFormatException e) {
			return 0L;
		}
	}

	@Override
	public double getDouble() {
		try {
			return Double.parseDouble(value);
		} catch (NumberFormatException e) {
			return 0.0;
		}
	}

	@Override
	public String getString() {
		return value;
	}

	

	
	
	@Override
	public void setBool(boolean value) {
		this.value = value? "on": "off";
	}

	@Override
	public void setInt(int value) {
		this.value = ""+value;
	}

	@Override
	public void setLong(long value) {
		this.value = ""+value;
	}

	@Override
	public void setDouble(double value) {
		this.value = ""+value;
	}

	@Override
	public void setString(String value) {
		if( value.length() <= STRING_LENGTH_MAX )
			this.value = value;
		else
			this.value = value.substring(0, STRING_LENGTH_MAX);
	}

	
}
