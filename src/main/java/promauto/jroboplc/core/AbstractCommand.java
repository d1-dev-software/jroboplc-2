package promauto.jroboplc.core;

import promauto.jroboplc.core.api.Command;
import promauto.jroboplc.core.api.Console;
import promauto.jroboplc.core.api.Module;
import promauto.jroboplc.core.api.Plugin;

public abstract class AbstractCommand implements Command {

	@Override
	public String execute(Console console, String args) {
		return "";
	}

	@Override
	public String execute(Console console, Plugin plugin, String args) {
		return "";
	}

	@Override
	public String execute(Console console, Module module, String args) {
		return "";
	}
	
	@Override
	public void executePosted(Console console, Module module, String args) {
	}
	
	
	private String getRefuseAnswer(String name) {
		return 
				"Invalid parameters. Use format:\r\n  " + 
				(name.isEmpty()? "": name + ":")  + getName() + " " + getUsage();
	}

	public String getRefuseAnswer() {
		return getRefuseAnswer("");
	}

	public String getRefuseAnswer(Module module) {
		return getRefuseAnswer(module.getName());
	}
	
	public String getRefuseAnswer(Plugin plugin) {
		return getRefuseAnswer(plugin.getPluginName());
	}
	
	public String getAnswerModuleNotFound(String modname) {
		return "Module not found: " + modname;
	}
	


}
