# What is JRobo
JRobo is a console java application that allows you to interact with peripheral devices through external ports (serial, tcp, udp), using various industrial and non-standard data exchange protocols, and control these devices in accordance with the specified logic.

It is used to create PLCs, data collection servers that work directly with various DBMS (relational and NoSQL), and can also be used as a framework for creating other applications.

This is the basis for building flexible, scalable and secure distributed control systems, allowing you to achieve maximum efficiency in process control.

Runs on any hardware with JVM (Oracle, OpenJDK) version 8 or higher or from a Docker container. 

# References

http://prom-auto.ru/wiki/doku.php?id=doc:jroboplc:index (russian)

https://gitlab.com/d1-dev-software/jroboplc/ (sources)


# Supported docker tags
  * `1.6.1`, `latest`


# How to use this image

## start a jrobo instance
```
docker run -it -p 4000:4000 -p 4900:4900 --name jrobo-demo promauto/jrobo
```

This will start `jrobo` with the default configuration "Traffic Light Demonstration" where TCP port `4000` is used for `jrbustcp` connections and `4900` is the port for `telnet`. 

Read more about the `jrbustcp` protocol [here](http://prom-auto.ru/wiki/doku.php?id=doc:jroboplc:modules:jrbustcp-protocol). 

You can use `jrviewer` as a simple client application to check the `jrbustcp` connection and view data. Download here: [jrviewer_win-x32](http://prom-auto.ru/download/jroboplc/jrviewer_win-x32.zip), [jrviewer_linux-x64](http://prom-auto.ru/download/jroboplc/jrviewer_linux-x64.tar.gz).




## ...via docker-compose
```
version: '3.8'

networks:
  default:

services:
  redis:
    image: promauto/jrobo
    container_name: jrobo-demo
    ports:
      - 4000:4000
      - 4900:4900
```

## ...with custom configuration
```
version: '3.8'

networks:
  default:

services:
  redis:
    image: promauto/jrobo
    container_name: jrobo-demo
    restart: always
    ports:
      - 4000:4000
      - 4900:4900
    volumes:
      - ${PWD}/conf:/usr/jrobo/conf
```
Make sure the `conf` folder is created and writable.

# Contacts
If you have any questions, feel free to ask: d1-dev@mail.ru Denis Lobanov
