<?xml version='1.0' encoding='ISO-8859-1' ?>
<root>
  <SCHEMAS>
    <s groupid='1' sizex='1280' sizey='1024' dbuf='1' prn='1'>
      <alias>Traffic Lights Logic</alias>
      <BRICKS>
        <b addr='0' x='312' y='0' ugpf='1'>
          <pinincl/>
          <pinexcl/>
        </b>
        <b addr='1' x='508' y='336' ugpf='1'>
          <pinincl/>
          <pinexcl/>
        </b>
        <b addr='2' x='508' y='404' ugpf='1'>
          <pinincl/>
          <pinexcl/>
        </b>
        <b addr='3' x='508' y='472' ugpf='1'>
          <pinincl/>
          <pinexcl/>
        </b>
        <b addr='4' x='508' y='540' ugpf='1'>
          <pinincl/>
          <pinexcl/>
        </b>
        <b addr='5' x='508' y='608' ugpf='1'>
          <pinincl/>
          <pinexcl/>
        </b>
        <b addr='6' x='776' y='112' ugpf='1'>
          <pinincl/>
          <pinexcl/>
        </b>
        <b addr='7' x='776' y='360' ugpf='1'>
          <pinincl/>
          <pinexcl/>
        </b>
        <b addr='8' x='984' y='16' ugpf='1'>
          <pinincl/>
          <pinexcl/>
        </b>
        <b addr='12' x='984' y='192' ugpf='1'>
          <pinincl/>
          <pinexcl/>
        </b>
        <b addr='10' x='984' y='364' ugpf='1'>
          <pinincl/>
          <pinexcl/>
        </b>
      </BRICKS>
      <COMMENTS/>
      <LINES>
        <l oa='0' oi='9' ia='1' ii='0'>
          <lp x='410' y='122'/>
          <lp x='476' y='122'/>
          <lp x='476' y='362'/>
          <lp x='508' y='362'/>
        </l>
        <l oa='0' oi='11' ia='2' ii='0'>
          <lp x='410' y='138'/>
          <lp x='464' y='138'/>
          <lp x='464' y='430'/>
          <lp x='508' y='430'/>
        </l>
        <l oa='0' oi='13' ia='3' ii='0'>
          <lp x='410' y='154'/>
          <lp x='452' y='154'/>
          <lp x='452' y='498'/>
          <lp x='508' y='498'/>
        </l>
        <l oa='0' oi='15' ia='4' ii='0'>
          <lp x='410' y='170'/>
          <lp x='440' y='170'/>
          <lp x='440' y='566'/>
          <lp x='508' y='566'/>
        </l>
        <l oa='0' oi='17' ia='5' ii='0'>
          <lp x='410' y='186'/>
          <lp x='428' y='186'/>
          <lp x='428' y='634'/>
          <lp x='508' y='634'/>
        </l>
        <l oa='2' oi='1' ia='0' ii='10'>
          <lp x='589' y='430'/>
          <lp x='648' y='430'/>
          <lp x='648' y='732'/>
          <lp x='220' y='732'/>
          <lp x='220' y='74'/>
          <lp x='312' y='74'/>
        </l>
        <l oa='3' oi='1' ia='0' ii='12'>
          <lp x='589' y='498'/>
          <lp x='632' y='498'/>
          <lp x='632' y='716'/>
          <lp x='236' y='716'/>
          <lp x='236' y='90'/>
          <lp x='312' y='90'/>
        </l>
        <l oa='4' oi='1' ia='0' ii='14'>
          <lp x='589' y='566'/>
          <lp x='620' y='566'/>
          <lp x='620' y='700'/>
          <lp x='252' y='700'/>
          <lp x='252' y='106'/>
          <lp x='312' y='106'/>
        </l>
        <l oa='5' oi='1' ia='0' ii='16'>
          <lp x='589' y='634'/>
          <lp x='609' y='634'/>
          <lp x='609' y='684'/>
          <lp x='268' y='684'/>
          <lp x='268' y='122'/>
          <lp x='312' y='122'/>
        </l>
        <l oa='1' oi='1' ia='0' ii='8'>
          <lp x='589' y='362'/>
          <lp x='664' y='362'/>
          <lp x='664' y='748'/>
          <lp x='200' y='748'/>
          <lp x='200' y='58'/>
          <lp x='312' y='58'/>
        </l>
        <l oa='0' oi='11' ia='6' ii='0'>
          <lp x='410' y='138'/>
          <lp x='597' y='138'/>
          <lp x='597' y='138'/>
          <lp x='776' y='138'/>
        </l>
        <l oa='0' oi='15' ia='7' ii='0'>
          <lp x='410' y='170'/>
          <lp x='716' y='170'/>
          <lp x='716' y='386'/>
          <lp x='776' y='386'/>
        </l>
        <l oa='0' oi='9' ia='8' ii='16'>
          <lp x='410' y='122'/>
          <lp x='540' y='122'/>
          <lp x='540' y='42'/>
          <lp x='984' y='42'/>
        </l>
        <l oa='6' oi='1' ia='12' ii='16'>
          <lp x='879' y='138'/>
          <lp x='931' y='138'/>
          <lp x='931' y='218'/>
          <lp x='984' y='218'/>
        </l>
        <l oa='0' oi='13' ia='10' ii='16'>
          <lp x='410' y='154'/>
          <lp x='740' y='154'/>
          <lp x='740' y='332'/>
          <lp x='947' y='332'/>
          <lp x='947' y='390'/>
          <lp x='984' y='390'/>
        </l>
        <l oa='0' oi='11' ia='8' ii='17'>
          <lp x='410' y='138'/>
          <lp x='564' y='138'/>
          <lp x='564' y='58'/>
          <lp x='984' y='58'/>
        </l>
        <l oa='0' oi='17' ia='12' ii='17'>
          <lp x='410' y='186'/>
          <lp x='697' y='186'/>
          <lp x='697' y='234'/>
          <lp x='984' y='234'/>
        </l>
        <l oa='7' oi='1' ia='10' ii='17'>
          <lp x='879' y='386'/>
          <lp x='931' y='386'/>
          <lp x='931' y='406'/>
          <lp x='984' y='406'/>
        </l>
      </LINES>
    </s>
  </SCHEMAS>
</root>